VERSION 5.00
Begin VB.Form Frm_About 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3585
   ClientLeft      =   3960
   ClientTop       =   3090
   ClientWidth     =   4785
   Icon            =   "Fm_About.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   3585
   ScaleWidth      =   4785
   Begin VB.Label lblCopyright 
      Alignment       =   2  'Center
      BackStyle       =   0  'Transparent
      Caption         =   "� 2013 Stockletter AB"
      ForeColor       =   &H00C0C0C0&
      Height          =   315
      Left            =   120
      TabIndex        =   2
      Top             =   3120
      Width           =   4575
   End
   Begin VB.Label lblType 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "BETA"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   615
      Left            =   180
      TabIndex        =   1
      Top             =   60
      Width           =   1935
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Version X"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   225
      Left            =   3480
      TabIndex        =   0
      Top             =   1260
      Width           =   735
   End
End
Attribute VB_Name = "Frm_About"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Frm_About.left = (MainMDI.Width - Frm_About.Width) / 2
  Frm_About.top = (MainMDI.Height - Frm_About.Height) / 4
  Frm_About.Picture = frmSplash.Picture
  lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision & " " & " r1"
  lblType.Caption = "Pro"
    If PRO = False Then
      lblType.Caption = "Demo"
    Else
      #If BETA = 1 Then
        lblType.Caption = "Beta"
      #Else
        lblType.Caption = ""
      #End If
    End If
    lblCopyright.Caption = Chr(169) & " " & App.LegalCopyright
End Sub

