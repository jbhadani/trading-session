Attribute VB_Name = "Init"
Option Explicit

Global menu As MenuBitmaps

Declare Function FindWindowEx Lib "user32" Alias "FindWindowExA" ( _
    ByVal hWnd1 As Long, ByVal hWnd2 As Long, _
    ByVal lpsz1 As String, ByVal lpsz2 As String) As Long

Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long

Declare Function SetClassLong Lib "user32" Alias "SetClassLongA" ( _
                ByVal hWnd As Long, ByVal nIndex As Long, ByVal dwnewlong As Long) As Long

'Declare Function InvalidateRect Lib "user32" _
                (ByVal hwnd As Long, lpRect As Long, ByVal bErase As Long) As Long

Declare Function CreateSolidBrush Lib "gdi32" (ByVal crColor As Long) As Long

Declare Function CreatePatternBrush Lib "gdi32" (ByVal hBitmap As Long) As Long

Public Enum enuTBType
    enuTB_FLAT = 1
    enuTB_STANDARD = 2
End Enum

Const GCL_HBRBACKGROUND = (-10)

Sub InitHelp()
  App.HelpFile = Path & "TradingSession2.chm"
End Sub


Sub InitMainMDI()
  If DEMO Then
    MainMDI.Caption = App.Title & " Demo"
  Else
    MainMDI.Caption = "Trading Session 2"
  End If
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          InitEnviroment
'Input:
'Output:
'
'Description:   Initialize environment variables
'Author:        Fredrik Wendel
'Revision:      97XXXX  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InitEnvironment()
  Dim Version As String
  
  Version = GetSetting(App.Title, "Identification", "Version", "")
  ActivePortfolio = False
  
  ' "Convert" from pre-processor to public variable
  'If PRO Then
  'IsProVersion = True
  'Else
  'IsProVersion = False
  'End If
  Call GetVersion
  Path = GetSetting(App.Title, "Directories", "Path", "Empty")     ' Added in 1.0.3
  ' TODO: Inkludera igen?
  'If Path = "Empty" Or Path = "" Then                             ' Added in 1.0.3
  '  Path = App.Path & "\"                                         ' Added in 1.0.3
  '  SaveSetting App.Title, "Directories", "Path", Path            ' Added in 1.0.3
  'End If
  
  DataPath = GetSetting(App.Title, "Directories", "DataPath", "Empty")
  'If DataPath = "Empty" Or DataPath = "" Then
  '  DataPath = App.Path & "\Data\"
  '  SaveSetting App.Title, "Directories", "DataPath", DataPath
  'End If
  
  'If GetSetting(App.Title, "Directories", "Path", "") = "" Then  ' Removed in 1.0.3
  '  SaveSetting App.Title, "Directories", "Path", App.Path & "\" ' Removed in 1.0.3
  'End If
  
  If GetSetting(App.Title, "Indicator", "UseGeneralIndicatorParameters", "False") = "True" Then
    UseGeneralIndicatorParameters = True
  Else
    UseGeneralIndicatorParameters = False
  End If
  
  blnUseSavedToolbarSettings = GetSetting(App.Title, "Misc", "UseSavedToolbarSettings", "False")
  Dim graphshow
  GraphInfoChanged = True
  graphshow = False
  
  IndicatortestVisible = False
  TestOK = True
  
  ActiveStockLabelsHandle = Empty
  
  ' TODO: Add error handling?
  On Error GoTo Err:
  InitStockLabels Path & "TextLabels.dat"
  
  'Bj�rn provar
  ZIAPointer = 0
  Exit Sub
Err:
  'Slut hack
End Sub

' Mikael Wallin 2013-05-13
' Validates whether MA is installed on the system and in that case if the MA user account is still valid.
' Called in Form_Load in MainMDI form.
'
' Mikael Wallin 2013-07-10
' Changed server and validation procedure
Sub ValidateMarketAccess(bAlertUser As Boolean)

    'Dim reg As Registry
    Dim key As RegKey, sUserId As String, sUserPass As String
    
    On Error Resume Next
    'AddRootRegKey HKEY_CURRENT_USER, ""
    Set key = RegKeyFromString("\HKEY_CURRENT_USER\Software\VB and VBA Program Settings\Trading Session 2\Market Access\User")
    
    'Set key = RegKeyFromString("\HKEY_CURRENT_USER\Software\Stockletter AB\Market Access\User")
    Select Case Err.Number
    Case 35006
        If bAlertUser Then MsgBox "Market Access not installed!", vbExclamation, "Market Access Validation"
        ' End program
        End
    Case 0
        sUserId = key.Values.Item("Id").Value
        
        sUserPass = key.Values.Item("Password").Value  '"!demo001"
        
        Dim scUserAgent As String, sURL As String
        
        scUserAgent = App.Title
        
        ' -- URLs ---
        sURL = "www.axier.se/includes/validate_quotes_user.php?uid=" & sUserId & "&pw=" & sUserPass & "&do="
        If IsProVersion Then 'Pro
          sURL = sURL & "validate-pro"
        Else
          sURL = sURL & "validate-usr"
        End If
        'MsgBox "sURL: " & sURL, vbInformation, "Market Access Validation"
        
        ' Old URL
        'sURL = "http://service.tendens.se/cgi-aktie/ma_server4.exe?op=auth&id=" & sUserId & "&pwd=" & sUserPass & "&client=TS2"
        
        Dim hOpen As Long, hFile As Long, sBuffer As String, Ret As Long
        
        ' Create a fixed-length string consisting of x number of spaces
        sBuffer = Space(4)
        
        ' Create an internet connection
        hOpen = InternetOpen(scUserAgent, INTERNET_OPEN_TYPE_DIRECT, vbNullString, vbNullString, 0)
        'Dim H As String
        'sURL = "webapps.sirote.com/foreclosure/default.aspx?__EVENTTARGET=&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUIMzE5MzEwMDAPZBYCZg9kFgICAw9kFgICAw9kFgICAQ9kFgICAQ9kFgoCAQ9kFgICAQ8QZGQWAWZkAgUPEA8WAh4LXyFEYXRhQm91bmRnZBAVRANBbGwHQXV0YXVnYQdCYWxkd2luB0JhcmJvdXIEQmliYgZCbG91bnQHQn"
        'sURL = sURL + "VsbG9jawZCdXRsZXIHQ2FsaG91bghDaGFtYmVycwhDaGVyb2tlZQdDaGlsdG9uB0Nob2N0YXcGQ2xhcmtlBENsYXkIQ2xlYnVybmUGQ29mZmVlB0NvbGJlcnQHQ29uZWN1aAVDb29zYQlDb3Zpbmd0b24IQ3JlbnNoYXcHQ3VsbG1hbgREYWxlBkRhbGxhcwZEZUthbGIGRWxtb3JlCEVzY2FtYmlhBkV0b3dhaAdGYXlldHRlCEZyYW5rbGluBkdlbmV2YQZHcmVlbmUESGFsZQVIZW5yeQdIb3VzdG9uB0phY2tzb24JSmVmZmVyc29uBUxhbWFyCkxhdWRlcmRhbGUITGF3cmVuY2UDTGVlCUxpbWVzdG9uZQdMb3duZGVzBU1hY29uB01hZGlzb24HTWFyZW5nbwZNYXJpb24ITWFyc2hhbGwGTW9iaWxlBk1vbnJvZQpNb250Z29tZXJ5Bk1vcmdhbgVQZXJyeQdQaWNrZW5zBFBpa2UIUmFuZG9scGgHUnVzc2VsbAZTaGVsYnkJU3QuIENsYWlyBlN1bXRlcglUYWxsYWRlZ2EKVGFsbGFwb29zYQpUdXNjYWxvb3NhBldhbGtlcgpXYXNoaW5ndG9uBldpbGNveAdXaW5zdG9uFUQDQWxsB0F1dGF1Z2EHQmFsZHdpbgdCYXJib3VyBEJpYmIGQmxvdW50B0J1bGxvY2sGQnV0bGVyB0NhbGhvdW4IQ2hhbWJlcnMIQ2hlcm9rZWUHQ2hpbHRvbgdDaG9jdGF3BkNsYXJrZQRDbGF5CENsZWJ1c"
        'sURL = sURL + "m5lBkNvZmZlZQdDb2xiZXJ0B0NvbmVjdWgFQ29vc2EJQ292aW5ndG9uCENyZW5zaGF3B0N1bGxtYW4ERGFsZQZEYWxsYXMGRGVLYWxiBkVsbW9yZQhFc2NhbWJpYQZFdG93YWgHRmF5ZXR0ZQhGcmFua2xpbgZHZW5ldmEGR3JlZW5lBEhhbGUFSGVucnkHSG91c3RvbgdKYWNrc29uCUplZmZlcnNvbgVMYW1hcgpMYXVkZXJkYWxlCExhd3JlbmNlA0xlZQlMaW1lc3RvbmUHTG93bmRlcwVNYWNvbgdNYWRpc29uB01hcmVuZ28GTWFyaW9uCE1hcnNoYWxsBk1vYmlsZQZNb25yb2UKTW9udGdvbWVyeQZNb3JnYW4FUGVycnkHUGlja2VucwRQaWtlCFJhbmRvbHBoB1J1c3NlbGwGU2hlbGJ5CVN0LiBDbGFpcgZTdW10ZXIJVGFsbGFkZWdhClRhbGxhcG9vc2EKVHVzY2Fsb29zYQZXYWxrZXIKV2FzaGluZ3RvbgZXaWxjb3gHV2luc3RvbhQrA0RnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZ2dnZxYBZmQCBw88KwAKAQAPFgQeC1Zpc2libGVEYXRlBgAAboCGddAIHgJTRBYBBgAAboCGddAIZGQCCQ8PFgIeBFRleHQFE1RvZGF5OiBTZXAgMDIsIDIwMTNkZAINDxYCHwMFaTxiIHN0eWxlPSdwYWRkaW5nLWxlZnQ6IDVweCc%2BTm8gZm9yZWNsb3N1cmUgc2FsZXMgYXJlIGF2YWlsYWJsZSBmb3IgdGhlIGNvdW50eSBhbmQgZGF0ZSB5b3Ugc2VsZWN0ZWQuPC9iPmQYAQUlY3RsMDAkQ29udGVudFBsY"
        'sURL = sURL + "WNlSG9sZGVyMSR1aU11bHRpVmlldw8PZGZkUJ8v0g4fjYdLOnfvyZ8IL%2Bu5ldo%3D&__EVENTVALIDATION=%2FwEWBgLjy%2FOGCwKOkrn2CAKSkrn2CAKGkrn2CALN0rfDBQKJuczqBCqs5LY3P1HwjRsHSTlkwco9e65f&ctl00%24ContentPlaceHolder1%24uiAgree=Agree"
        ' Open the url
        hFile = InternetOpenUrl(hOpen, sURL, vbNullString, ByVal 0&, INTERNET_FLAG_RELOAD, ByVal 0&)
        
        ' Read x bytes of response string from server
        InternetReadFile hFile, sBuffer, Len(sBuffer), Ret
        
        'MsgBox "sBuffer: " & sBuffer, vbInformation, "Market Access Validation"
        
        ' Validate
        Dim sResponse As String
        sResponse = sBuffer
        'MsgBox "Response: " & sResponse, vbInformation, "Market Access Validation"
        If sResponse = "FAIL" Then
          If bAlertUser Then MsgBox "Market Access user account no longer valid!", vbExclamation, "Market Access Validation"
          ' End program
          End
        Else
          ' Everything good! Don't need to tell user =)
          'If bAlertUser Then MsgBox "Market Access installed and user account valid!", vbInformation, "Market Access Validation"
        End If
        
        ' --- Old validation (service.tendens.se) ---
        'Dim lines() As String
        'lines = Split(sBuffer, vbNewLine)
        
        ''Dim i As Integer
        ''For i = 0 To UBound(lines)
        '    'MsgBox "Line " & i + 1 & ": " & lines(i), vbInformation, "Market Access Validation"
        ''Next i
        
        'Dim sUID As String, iFlag1 As Integer, iFlag2 As Integer
        'sUID = lines(0)
        ''MsgBox "User Id: " & sUID, vbInformation, "Market Access Validation"
        'If sUID = "ERROR" Then
        '    If bAlertUser Then MsgBox "Market Access user account no longer valid!", vbExclamation, "Market Access Validation"
        '    ' End program
        '    End
        'Else
        '    iFlag1 = lines(1)
        '    iFlag2 = lines(2)
        '    If iFlag1 = iFlag2 Then
        '        ' Everything good! Don't need to tell user =)
        '        'If bAlertUser Then MsgBox "Market Access installed and user account valid!", vbInformation, "Market Access Validation"
        '    End If
        'End If
        ' --- /Old validation
        
        ' Clean up
        InternetCloseHandle hFile
        InternetCloseHandle hOpen
    Case Else
        MsgBox "An unexpected error occurred when validating Market Access:" & vbNewLine & Err.Description, vbExclamation, "Market Access Validation"
        ' End program
        End
    End Select
    
End Sub

'InitStatusbar
'Last modified: 961230

Sub InitStatusBar()
  
  Dim I As Long
  Dim pnlX As Panel
    
  MainMDI.StatusBar.Panels.Clear
  For I = 1 To 6      ' Add 7 panels.
    Set pnlX = MainMDI.StatusBar.Panels.Add()
    pnlX.Alignment = sbrCenter
  Next I

  ' Set the prooperties of each panel.
  With MainMDI.StatusBar.Panels
    .Item(1).Alignment = sbrLeft
    .Item(1).Style = sbrText
    .Item(1).Width = 2000
    .Item(1).AutoSize = sbrSpring
    .Item(1).Text = CheckLang("Portf�lj: Ingen")
    .Item(2).Style = sbrCaps
    .Item(2).Width = 500
    .Item(3).Style = sbrNum
    .Item(3).Width = 500
    .Item(4).Style = sbrIns
    .Item(4).Width = 500
    .Item(5).Alignment = sbrLeft
    .Item(5).Style = sbrText
    .Item(5).Width = 3500
    .Item(6).Style = sbrDate
    .Item(6).Width = 1000
  End With
    
End Sub

' InitPortFolioArray
' Last modified 961230
Sub InitPortfolioArray()
  PortfolioArraySize = 0
  TransactionArraySize = 0
  'MainMDI.M_Report.Enabled = False
End Sub

' InitInvestmentArray
' Last modified 961230
Sub InitInvestmentArray()
  If DEMO = 0 Then
    R_InvestInfo_All DataPath + "InvInfo.dat"
    FixInvInfo InvestInfoArray
  Else
    
    InvestInfoArraySize = 5
    ReDim InvestInfoArray(1 To InvestInfoArraySize)
    
    With InvestInfoArray(1)
    .FileName = "ABB B"
    .TypeOfInvest = 1
    .Name = "ABB B"
    End With
    
    With InvestInfoArray(2)
    .FileName = "Astra A"
    .TypeOfInvest = 1
    .Name = "Astra A"
    End With
    
    With InvestInfoArray(3)
    .FileName = "Ericsson B"
    .TypeOfInvest = 1
    .Name = "Ericsson B"
    End With
    
    With InvestInfoArray(4)
    .FileName = "Skanska B"
    .TypeOfInvest = 1
    .Name = "Skanska B"
    End With
    
    With InvestInfoArray(5)
    .FileName = "Volvo B"
    .TypeOfInvest = 1
    .Name = "Volvo B"
    End With

  End If

End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          InitIncludeInStockList
'Input:
'Output:
'
'Description:   Initialize gblnIncludeInStockList
'Author:        Fredrik Wendel
'Revision:      980928  Created
'               980929  Reading from registry was added
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub Init�ncludeInStockList()

  Dim I As Long
  ReDim gblnIncludeInStockList(CategoryArraySize)
  For I = 1 To CategoryArraySize
    If Trim(CategoryArray(I)) <> "" Then
      gblnIncludeInStockList(I) = GetSetting(App.Title, "StockList", "Include" & Trim(Str(I)), "TRUE")
    Else
      gblnIncludeInStockList(I) = GetSetting(App.Title, "StockList", "Include" & Trim(Str(I)), "FALSE")
    End If
  Next I
  
End Sub

Sub InitObjectCombo()
Dim I As Long
Dim ActiveIndexFound As Boolean
Dim CategoryIndex As Byte
Static k As Boolean
k = False
With MainMDI.ID_Object
  .Clear
  
  'If MainMDI.C_Category.ListIndex = 0 Then
  If MainMDI.ID_Group.ListIndex = 0 Then
    For I = 1 To InvestInfoArraySize
      .AddItem Trim(InvestInfoArray(I).Name)
      DoEvents
    Next I
   k = True
  End If
  
  If MainMDI.ID_Group.ListIndex = 1 Then
    For I = 1 To PortfolioArraySize
      .AddItem Trim(PortfolioArray(I).Name)
      DoEvents
    Next I
    k = True
  End If
  
  If MainMDI.ID_Group.ListIndex = 2 Then
    For I = 1 To SearchResultArraySize
      .AddItem Trim(SearchResultArray(I))
      DoEvents
    Next I
    k = True
  End If
  If k = False Then
  If MainMDI.ID_Group.ListIndex >= 3 Then
    'CategoryIndex = GetCategoryIndex(MainMDI.C_Category.Text)
    CategoryIndex = GetCategoryIndex(MainMDI.ID_Group.List(MainMDI.ID_Group.ListIndex))
    For I = 1 To InvestInfoArraySize
      If InvestInfoArray(I).TypeOfInvest = CategoryIndex Then
        .AddItem Trim(InvestInfoArray(I).Name)
      End If
      DoEvents
    Next I
  End If
   k = True
   End If
  If (.ListCount) > 0 And (ActiveIndex = 0) Then
    ActiveIndex = 1
  End If
  
  
    
  ActiveIndexFound = False
  For I = 0 To .ListCount - 1
    If Trim(.List(I)) = Trim(InvestInfoArray(ActiveIndex).Name) Then
      R_PriceDataArray DataPath + Trim(InvestInfoArray(ActiveIndex).FileName) + ".prc", PriceDataArray, PriceDataArraySize
      If ShowGraph = True Then
        GraphInfoChanged = True
        UpdateGraph ActiveIndex
      End If
      .ListIndex = I
      ActiveIndexFound = True
    End If
    DoEvents
  Next I
  
  If .ListCount = 0 Then
    ActiveIndex = 0
    If ShowGraph Then
      ShowGraph = False
      Unload Frm_OwnGraph
    End If
  Else
    If Not ActiveIndexFound Then
      ActiveIndex = GetInvestIndex(.List(0))
      R_PriceDataArray DataPath + Trim(InvestInfoArray(ActiveIndex).FileName) + ".prc", PriceDataArray, PriceDataArraySize
      .ListIndex = 0
      ' Ta bort alla trendlinjer fr�n diagrammet
      FreeTrendLineArray GraphSettings.DWM
      HideAllTrendlines
      
      If ShowGraph = True Then
        GraphInfoChanged = True
        UpdateGraph ActiveIndex
      End If
    End If
  End If
  
  
  Dim j As Long
  If ActiveIndex > 0 Then
    For I = 1 To TrendLineSeries
      For j = 1 To TrendlineSerieSize
        TrendLineArray(I, j) = WorkspaceSettingsArray(ActiveIndex).TrendLine(I, j)
      Next j
    Next I
  End If
  '.Locked = True
  End With
End Sub

Sub InitIndicatorToolBar()
  With MainMDI
  Dim TempStr1 As String
  Dim TempStr2 As String
  Dim strTemp3 As String
    
  Dim I
  For I = 1 To IndicatorArraySize
    .ID_Indicator1.AddItem IndicatorArray(I), I - 1
    .ID_Indicator2.AddItem IndicatorArray(I), I - 1
  Next I
  
  For I = 1 To UBound(ColorIndicatorArray)
    .ID_ColorSignallingIndicator.AddItem ColorIndicatorArray(I), I - 1 '
  Next I
  
  TempStr1 = GraphSettings.strIndicator1
  TempStr2 = GraphSettings.strIndicator2
    
  For I = 1 To IndicatorArraySize
    If TempStr1 = IndicatorArray(I) Then
      .ID_Indicator1.ListIndex = I - 1
    End If
    If TempStr2 = IndicatorArray(I) Then
      .ID_Indicator2.ListIndex = I - 1
    End If
  Next I
  For I = 1 To UBound(ColorIndicatorArray)
    If GraphSettings.strColorSignallingIndicator = ColorIndicatorArray(I) Then
      .ID_ColorSignallingIndicator.ListIndex = I - 1
    End If
  Next I
  End With
End Sub

Sub InitActiveIndex()
ActiveIndex = 0
End Sub

Sub InitCategories()
  R_Category_All DataPath + "Category.dat"
  RemoveUnusedCategories
End Sub

Sub InitCategoryToolbar(ComboBox As ComboBox)
' This Sub must be called after InitCategories
  Dim I As Long
  
  'With MainMDI.SSActiveToolBars1.Tools("ID_Group").ComboBox
  With ComboBox
  .Clear
  .AddItem "Alla"
  .AddItem "Portf�lj"
  .AddItem "S�kresultat"
  For I = 1 To CategoryArraySize
    If Trim(CategoryArray(I)) <> "" Then
      .AddItem CategoryArray(I)
    End If
  Next I
  .ListIndex = 0
  End With
End Sub

Sub InitSearchResultArray()
  SearchResultArraySize = 0
End Sub



Public Sub InitParaSet()
  If GetSetting(App.Title, "Indicator", "UseGeneralIndicatorParameters", "True") = "True" Then
    UseGeneralIndicatorParameters = True
  Else
    UseGeneralIndicatorParameters = False
  End If
  If UseGeneralIndicatorParameters Then
    ' TODO:
    'MsgBox "GetParameterSettingsFromRegistry"
    GetParameterSettingsFromRegistry
If PRO = True Then
    GetFishParameterSettingsFromRegistry
End If
  Else
    ' TODO:
    'MsgBox "GetParameterSettingsFromInvest (ActiveIndex: " & ActiveIndex & ")"
    GetParameterSettingsFromInvest ActiveIndex
If PRO = True Then
    GetFishParameterSettingsFromInvest ActiveIndex
End If
  End If
  
End Sub




Sub InitTrendline()
  TLDrawMode = dmNone
  AttachMode = dmNone
  HideAllTrendlines
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          InitWorkSpaceSettings
'Input:
'Output:
'
'Description:   Initialize WorkspaceSettings
'Author:        Fredrik Wendel
'Revision:      98XXXX  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Sub InitWorkspaceSettings()
  ReadWorkspaceSettings
  If InvestInfoArraySize <> 0 Then
    FixAddAndRemove
  End If
  WriteWorkspaceSettings
End Sub

Public Sub InitIndicatorTestOptions()
With IndicatorTestOptions
  
  If GetSetting(App.Title, "Indicatortest", "UseBreakevenLongPositions", "False") = "True" Then
    .UseBreakevenLongPositions = True
  Else
    .UseBreakevenLongPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseBreakevenShortPositions", "False") = "True" Then
    .UseBreakevenShortPositions = True
  Else
    .UseBreakevenShortPositions = False
  End If

  If GetSetting(App.Title, "Indicatortest", "BreakevenMethodIsPercent", "True") = "True" Then
    .BreakevenMethodIsPercent = True
  Else
    .BreakevenMethodIsPercent = False
  End If
  
  .BreakevenLevel = Val(GetSetting(App.Title, "Indicatortest", "BreakevenLevel", "0"))
  
  If GetSetting(App.Title, "Indicatortest", "UseInactivityLongPositions", "False") = "True" Then
    .UseInactivityLongPositions = True
  Else
    .UseInactivityLongPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseInactivityShortPositions", "False") = "True" Then
    .UseInactivityShortPositions = True
  Else
    .UseInactivityShortPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "InactivityMethodIsPercent", "True") = "True" Then
    .InactivityMethodIsPercent = True
  Else
    .InactivityMethodIsPercent = False
  End If
  
  .InactivityMinChange = Val(GetSetting(App.Title, "Indicatortest", "InactivityMinChange", "5"))
  .InactivityPeriods = Val(GetSetting(App.Title, "Indicatortest", "InactivityPeriods", "20"))
  
  If GetSetting(App.Title, "Indicatortest", "UseMaxLossLongPositions", "False") = "True" Then
    .UseMaxLossLongPositions = True
  Else
    .UseMaxLossLongPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseMaxLossShortPositions", "False") = "True" Then
    .UseMaxLossShortPositions = True
  Else
    .UseMaxLossShortPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "MaxLossMethodIsPercent", "True") = "True" Then
    .MaxLossMethodIsPercent = True
  Else
    .MaxLossMethodIsPercent = False
  End If
  
  .MaxLossLevel = Val(GetSetting(App.Title, "Indicatortest", "MaxLossLevel", "5"))
  
  If GetSetting(App.Title, "Indicatortest", "UseProfitTakingLongPositions", "False") = "True" Then
    .UseProfitTakingLongPositions = True
  Else
    .UseProfitTakingLongPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseProfitTakingShortPositions", "False") = "True" Then
    .UseProfitTakingShortPositions = True
  Else
    .UseProfitTakingShortPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "ProfitTakingMethodIsPercent", "True") = "True" Then
    .ProfitTakingMethodIsPercent = True
  Else
    .ProfitTakingMethodIsPercent = False
  End If
  
  .ProfitTarget = Val(GetSetting(App.Title, "Indicatortest", "ProfitTarget", "20"))
  
  If GetSetting(App.Title, "Indicatortest", "UseTrailingLongPositions", "False") = "True" Then
    .UseTrailingLongPositions = True
  Else
    .UseTrailingLongPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseTrailingShortPositions", "False") = "True" Then
    .UseTrailingShortPositions = True
  Else
    .UseTrailingShortPositions = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "TrailingMethodIsPercent", "True") = "True" Then
    .TrailingMethodIsPercent = True
  Else
    .TrailingMethodIsPercent = False
  End If
  
  .TrailingProfitRisk = Val(GetSetting(App.Title, "Indicatortest", "TrailingProfitRisk", "5"))
  .TrailingPeriods = Val(GetSetting(App.Title, "Indicatortest", "TrailingPeriods", "5"))
  
  .TradePrice = Val(GetSetting(App.Title, "Indicatortest", "TradePrice", "3"))
  .Delay = Val(GetSetting(App.Title, "Indicatortest", "Delay", "0"))
  
  If GetSetting(App.Title, "Indicatortest", "LongOnly", "True") = "True" Then
    .LongOnly = True
  Else
    .LongOnly = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "ShortOnly", "False") = "True" Then
    .ShortOnly = True
  Else
    .ShortOnly = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "BothLongAndShort", "False") = "True" Then
    .BothLongAndShort = True
  Else
    .BothLongAndShort = False
  End If
  
  .InitialEquity = Val(GetSetting(App.Title, "Indicatortest", "InitialEquity", "10000"))
  .MarginRequirement = Val(GetSetting(App.Title, "Indicatortest", "MarginRequirement", "100"))
  .AnnualInterestRate = Val(GetSetting(App.Title, "Indicatortest", "AnnualInterestRate", "0"))
  '.StartDate = Val(GetSetting(App.Title, "Indicatortest", "AnnualInterestRate", "0"))
  '.StoppDate = Val(GetSetting(App.Title, "Indicatortest", "AnnualInterestRate", "0"))
  
  If GetSetting(App.Title, "Indicatortest", "UseCommission", "False") = "True" Then
    .UseCommission = True
  Else
    .UseCommission = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UsePercentageCommission", "True") = "True" Then
    .UsePercentageCommission = True
  Else
    .UsePercentageCommission = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "UseMinimumCommission", "False") = "True" Then
    .UseMinimumCommission = True
  Else
    .UseMinimumCommission = False
  End If
  
  .Percentage = Val(GetSetting(App.Title, "Indicatortest", "Percentage", "0"))
  .Capital = Val(GetSetting(App.Title, "Indicatortest", "Capital", "10000"))
  .MinCommission = Val(GetSetting(App.Title, "Indicatortest", "MinCommission", "200"))
  .BuyColor = Val(GetSetting(App.Title, "Indicatortest", "BuyColor", "10"))
  .SellColor = Val(GetSetting(App.Title, "Indicatortest", "SellColor", "4"))
  .StopColor = Val(GetSetting(App.Title, "Indicatortest", "StopColor", "4"))
  
  If GetSetting(App.Title, "Indicatortest", "ShowArrows", "False") = "True" Then
    .ShowArrows = True
  Else
    .ShowArrows = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "ShowArrowLabels", "False") = "True" Then
    .ShowArrowLabels = True
  Else
    .ShowArrowLabels = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "RemoveExistingArrows", "False") = "True" Then
    .RemoveExistingArrows = True
  Else
    .RemoveExistingArrows = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "PlotEquityLine", "False") = "True" Then
    .PlotEquityLine = True
  Else
    .PlotEquityLine = False
  End If
  
  If GetSetting(App.Title, "Indicatortest", "RemoveExistingLines", "False") = "True" Then
    .RemoveExistingLines = True
  Else
    .RemoveExistingLines = False
  End If
End With
End Sub

Sub InitTools()

  ' TODO: Add error handling?
  'On Error Resume Next
  On Error GoTo Err:
  If blnUseSavedToolbarSettings Then
    If FileExists(Path & ACTIVETOOLBARS_LAYOUT_FILE) Then
      'MainMDI.SSActiveToolBars1.LoadLayout Path & ACTIVETOOLBARS_LAYOUT_FILE
MainMDI.Toolbar1.RestoreToolbar "", "Software\VB and VBA Program Settings\Trading Session 2\market Access", "TL1"
MainMDI.Toolbar2.RestoreToolbar "", "Software\VB and VBA Program Settings\Trading Session 2\market Access", "TL2"
MainMDI.Toolbar4.RestoreToolbar "", "Software\VB and VBA Program Settings\Trading Session 2\market Access", "TL4"
Frm_ToolMove.Toolbar4.RestoreToolbar "", "Software\VB and VBA Program Settings\Trading Session 2\market Access", "TL4"
    End If
  End If
  'On Error GoTo 0
Lang = GetSetting(App.Title, "Misc", "Lang", 1)
  If Lang = 0 Then
    Lang = 1
  End If
  With MainMDI

    ' Enable certain controls if Pro version...
    If IsProVersion Then
      
      .ID_NewPortfolio.Enabled = True
      .ID_OpenPortfolio.Enabled = True
      .ID_SavePortfolio.Enabled = True
      .ID_SavePortfolioAs.Enabled = True
      
      .ID_Porfolio.Enabled = True
      .ID_StockTransaction.Enabled = True
      .ID_DepositWithdrawal.Enabled = True
      .ID_Dividend.Enabled = True
      .ID_AddObjectToPortfolio.Enabled = True
      .ID_Report.Enabled = True
      
      .ID_Option.Enabled = True
      
      .Toolbar1.Buttons(3).Enabled = True
      
      '.ID_Ind2).checked = true
      .ID_Ind2.Enabled = True
      .ID_Indicator2.Enabled = True
      .Toolbar2.Buttons(13).Enabled = True
        
      ' Set registry settings
      GraphSettings.ShowIndicator2 = True
    
    Else
      
      ' ...and disable certain controls if not
      .ID_NewPortfolio.Enabled = False
      .ID_OpenPortfolio.Enabled = False
      .ID_SavePortfolio.Enabled = False
      .ID_SavePortfolioAs.Enabled = False
      
      '.ID_Portfolio.Enabled = False
      .ID_StockTransaction.Enabled = False
      .ID_DepositWithdrawal.Enabled = False
      .ID_Dividend.Enabled = False
      .ID_AddObjectToPortfolio.Enabled = False
      .ID_Report.Enabled = False
      
      .ID_Option.Enabled = False
      
      .Toolbar1.Buttons(3).Enabled = False
      
      .ID_Ind1.Checked = True
      .ID_Ind1.Enabled = True
      .ID_Indicator1.Enabled = True
      .ID_Ind2.Checked = False
      .ID_Ind2.Enabled = False
      .ID_Indicator2.Enabled = False
      '.Toolbar2.Buttons(12).ToolTipText = .Toolbar1.Buttons(12).ToolTipText
      .Toolbar2.Buttons(12).Enabled = True
      '.Toolbar2.Buttons(13).ToolTipText = .Toolbar1.Buttons(13).ToolTipText
      .Toolbar2.Buttons(13).Enabled = False
      
      .Toolbar4.Buttons(11).Enabled = False     ' save  graph

      ' Set registry settings
      GraphSettings.ShowIndicator2 = False
      .ID_Search.Enabled = False            '   search menu
      .Toolbar1.Buttons(9).Enabled = False  '   search toolbar button
      
      .ID_FishNetEnabled.Checked = False
      .ID_FishNetEnabled.Enabled = False
      .ID_FishNetSettings.Checked = False
      .ID_FishNetSettings.Enabled = False
      .Toolbar1.Buttons(14).Value = tbrUnpressed
      .Toolbar1.Buttons(14).Enabled = False
     
     .Toolbar1.Buttons(1).Enabled = False
     .Toolbar1.Buttons(2).Enabled = False
     .Toolbar1.Buttons(13).Enabled = False
     
     
     
      ' Add name text
      .ID_Porfolio.Enabled = 1 '.ID_Porfolio.Caption & " " & MESSAGE_ONLY_PRO
      .ID_NewPortfolio.Enabled = 0 ' .ID_NewPortfolio.Caption & " " & MESSAGE_ONLY_PRO
      .ID_OpenPortfolio.Enabled = 0 ' .ID_OpenPortfolio.Caption & " " & MESSAGE_ONLY_PRO
      .ID_SavePortfolio.Enabled = 0 '.ID_SavePortfolio.Caption & " " & MESSAGE_ONLY_PRO
      .ID_SavePortfolioAs.Enabled = 0 '.ID_SavePortfolioAs.Caption & " " & MESSAGE_ONLY_PRO
      
      .ID_StockTransaction.Enabled = 0 '.ID_StockTransaction.Caption & " " & MESSAGE_ONLY_PRO
      .ID_DepositWithdrawal.Enabled = 0 '.ID_DepositWithdrawal.Caption & " " & MESSAGE_ONLY_PRO
      .ID_Dividend.Enabled = 0 '.ID_Dividend.Caption & " " & MESSAGE_ONLY_PRO
      .ID_AddObjectToPortfolio.Enabled = 0 '.ID_AddObjectToPortfolio.Caption & " " & MESSAGE_ONLY_PRO
      .ID_Report.Enabled = 0 '.ID_Report.Caption & " " & MESSAGE_ONLY_PRO
      
      .ID_Option.Enabled = 0 '.ID_Option.Caption & " " & MESSAGE_ONLY_PRO
      
      .Toolbar1.Buttons(3).Enabled = 0 '.Toolbar1.Buttons(3).ToolTipText & " " & MESSAGE_ONLY_PRO
      .Toolbar1.Buttons(3).Enabled = False

      ' Add tooltip text
      '.ID_Porfolio.ToolTipText = .ID_Porfolio.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_NewPortfolio.ToolTipText = .ID_NewPortfolio.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_OpenPortfolio.ToolTipText = .ID_OpenPortfolio.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_SavePortfolio.ToolTipText = .ID_SavePortfolio.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_SavePortfolioAs.ToolTipText = .ID_SavePortfolioAs.ToolTipText & " " & MESSAGE_ONLY_PRO
      
      '.ID_StockTransaction.ToolTipText = .ID_StockTransaction.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_DepositWithdrawal.ToolTipText = .ID_DepositWithdrawal.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_Dividend.ToolTipText = .ID_Dividend.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_AddObjectToPortfolio.ToolTipText = .ID_AddObjectToPortfolio.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_Report.ToolTipText = .ID_Report.ToolTipText & " " & MESSAGE_ONLY_PRO
      
      '.ID_Option.ToolTipText = .ID_Option.ToolTipText & " " & MESSAGE_ONLY_PRO
      
      '.ID_TLSave.ToolTipText = .ID_TLSave.ToolTipText & " " & MESSAGE_ONLY_PRO
      
      '.ID_Ind2.ToolTipText = .ID_Ind2.ToolTipText & " " & MESSAGE_ONLY_PRO
      '.ID_Indicator2.ToolTipText = .ID_Indicator2.ToolTipText & " " & MESSAGE_ONLY_PRO
    
    End If

    .ID_Print.Enabled = False
    .ID_Preview.Enabled = False
    .Toolbar1.Buttons(5).Enabled = False
    .Toolbar1.Buttons(4).Enabled = False
  
    If GraphSettings.ShowIndicator1 Then
      .ID_Ind1.Checked = True
      .ID_Indicator1.Enabled = True
      .Toolbar2.Buttons(12).Value = tbrPressed
    Else
      .ID_Ind1.Checked = False
      .ID_Indicator1.Enabled = False
      .Toolbar2.Buttons(12).Value = tbrUnpressed
    End If
    If GraphSettings.ShowIndicator2 Then
      .ID_Ind2.Checked = True
      .ID_Indicator2.Enabled = True
      .Toolbar2.Buttons(13).Value = tbrPressed
    Else
      .ID_Ind2.Checked = False
      .ID_Indicator2.Enabled = False
      .Toolbar2.Buttons(13).Value = tbrUnpressed
    End If
    If GraphSettings.ShowVolume Then
      .ID_ShowVolume.Checked = True
      .Toolbar2.Buttons(11).Value = tbrPressed
    Else
      .ID_ShowVolume.Checked = False
      .Toolbar2.Buttons(11).Value = tbrUnpressed
    End If
    If GraphSettings.ShowBollinger Then
      .ID_ShowBollinger.Checked = True
      .Toolbar2.Buttons(20).Value = tbrPressed
    Else
      .ID_ShowBollinger.Checked = False
      .Toolbar2.Buttons(20).Value = tbrUnpressed
    End If
    If GraphSettings.ShowParabolic Then
      .ID_ShowParabolic.Checked = True
      .Toolbar2.Buttons(21).Value = tbrPressed
    Else
      .ID_ShowParabolic.Checked = False
      .Toolbar2.Buttons(21).Value = tbrUnpressed
    End If
    If GraphSettings.ShowSignalColor Then
      .ID_ShowColorSignalling.Checked = True
      .Toolbar2.Buttons(16).Visible = True
    Else
      .ID_ShowColorSignalling.Checked = False
      .Toolbar2.Buttons(16).Visible = False
    End If
    If GraphSettings.ShowHLC Then
      .ID_ShowHLC.Checked = True
      .Toolbar2.Buttons(15).Value = tbrPressed
    Else
      .ID_ShowHLC.Checked = False
      .Toolbar2.Buttons(15).Value = tbrUnpressed
    End If
If PRO = True Then
    If GraphSettings.blnShowFish Then
      .ID_FishNetEnabled.Checked = True
      '.ID_ShowHighlight.Enabled = True
      .Toolbar1.Buttons(14).Value = tbrPressed
    Else
      .ID_FishNetEnabled.Checked = False
      '.ID_ShowHighlight.Enabled = False
      .Toolbar1.Buttons(14).Value = tbrUnpressed
    End If
    If GraphSettings.blnShowHighlight Then
      '.ID_ShowHighlight.Checked = True
    Else
      '.ID_ShowHighlight.Checked = False
    End If
End If
    Select Case GraphSettings.lngCloseWidth
    Case 0
      .ID_GraphCloseWidthOnBar.Checked = True
    Case 1
      .ID_GraphCloseWidthOne.Checked = True
    Case 2
      .ID_GraphCloseWidthTwo.Checked = True
    End Select
    If GraphSettings.ShowYGrid Then
      .ID_ScaleLineVertical.Checked = True
    Else
      .ID_ScaleLineVertical.Checked = False
    End If
    If GraphSettings.ShowXGrid Then
      .ID_ScaleLineHorizontal.Checked = True
    Else
      .ID_ScaleLineHorizontal.Checked = False
      .ID_ScaleLineMinHorizontal.Enabled = False
    End If
    If GraphSettings.ShowMinXGrid Then
      .ID_ScaleLineMinHorizontal.Checked = True
    Else
      .ID_ScaleLineMinHorizontal.Checked = False
    End If
    If GraphSettings.ShowPriceMAV1 Then
      .ID_ShowMAV1.Checked = True
      .Toolbar2.Buttons(18).Value = tbrPressed
    Else
      .ID_ShowMAV1.Checked = False
      .Toolbar2.Buttons(18).Value = tbrUnpressed
    End If
    If GraphSettings.ShowPriceMAV2 Then
      .ID_ShowMAV2.Checked = True
      .Toolbar2.Buttons(19).Value = tbrPressed
    Else
      .ID_ShowMAV2.Checked = False
      .Toolbar2.Buttons(19).Value = tbrUnpressed
    End If
    
    'Setup
    If GraphSettings.Setup(supKeyReversalDay).blnShow Then
      .ID_ShowKeyReversalDay.Checked = True
    Else
      .ID_ShowKeyReversalDay.Checked = False
    End If
    If GraphSettings.Setup(supOneDayReversal).blnShow Then
      .ID_ShowOneDayReversal.Checked = True
    Else
      .ID_ShowOneDayReversal.Checked = False
    End If
    If GraphSettings.Setup(supPatternGap).blnShow Then
      .ID_ShowPatternGap.Checked = True
    Else
      .ID_ShowPatternGap.Checked = False
    End If
    If GraphSettings.Setup(supReversalDay).blnShow Then
      .ID_ShowReversalDay.Checked = True
    Else
      .ID_ShowReversalDay.Checked = False
    End If
    If GraphSettings.Setup(supReversalGap).blnShow Then
      .ID_ShowReversalGap.Checked = True
    Else
      .ID_ShowReversalGap.Checked = False
    End If
    If GraphSettings.Setup(supTwoDayReversal).blnShow Then
      .ID_ShowTwoDayReversal.Checked = True
    Else
      .ID_ShowTwoDayReversal.Checked = False
    End If
    If GraphSettings.blnShowSupportResistance Then
      .ID_ShowSupportResistance.Checked = True
    Else
      .ID_ShowSupportResistance.Checked = False
    End If
    If GraphSettings.ExponentialPrice Then
      .ID_GraphScaleExponential.Checked = True
    Else
      .ID_GraphScaleExponential.Checked = False
    End If
  End With
Exit Sub
Err:
End Sub

Sub InitCommand()
  If Trim(Command) <> "" Then
    DoOpenPortfolio Trim(Command)
  End If
End Sub

Public Sub ChangeTBBack(TB As Object, PNewBack As Long, pType As enuTBType)
Dim lTBWnd      As Long

    Select Case pType
        
        Case enuTB_FLAT     'FLAT Button Style Toolbar
            DeleteObject SetClassLong(TB.hWnd, GCL_HBRBACKGROUND, PNewBack) 'Its Flat, Apply directly to TB Hwnd
        
        Case enuTB_STANDARD 'STANDARD Button Style Toolbar
            lTBWnd = FindWindowEx(TB.hWnd, 0, "msvb_lib_toolbar", vbNullString) 'Standard, find Hwnd first
            DeleteObject SetClassLong(lTBWnd, GCL_HBRBACKGROUND, PNewBack)      'Set new Back
    End Select
End Sub
'=================================================================================================================
' If you want to use Win Common Control 5 Toolbars, use "ToolbarWindow32" instead of "msvb_lib_toolbar"
' Win Common Control 5 Toolbars can't be FLAT, they are always STANDARD, so use enuTB_STANDARD when you call this
'=================================================================================================================


Sub InitGraphicsMenu()
Dim I As Integer
Dim hMenu As Long
Dim hSubMenu As Long
Dim Ret As Long
'hMenu = GetMenu(hWnd)
' FILE MENU
With MainMDI
'hSubMenu = GetSubMenu(hMenu, 0)
Set menu = New MenuBitmaps
'menu.GetHBmpUacShield MainMDI
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(2).ExtractIcon)
menu.Assign MainMDI, Ret, 0, 0  'new
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(3).ExtractIcon)
menu.Assign MainMDI, Ret, 0, 1  'open
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(1).ExtractIcon)
menu.Assign MainMDI, Ret, 0, 2  'save

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(66).ExtractIcon)
menu.Assign MainMDI, Ret, 0, 4  'preview
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(65).ExtractIcon)
menu.Assign MainMDI, Ret, 0, 5  'print
menu.Assign MainMDI, HBMMENU_POPUP_CLOSE, 0, 8      'print

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(22).ExtractIcon)
menu.Assign MainMDI, Ret, 1, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(23).ExtractIcon)
menu.Assign MainMDI, Ret, 1, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(24).ExtractIcon)
menu.Assign MainMDI, Ret, 1, 2
'25 69
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(25).ExtractIcon)
menu.Assign MainMDI, Ret, 1, 3, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(69).ExtractIcon)
menu.Assign MainMDI, Ret, 1, 3, 2

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(41).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 0, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(33).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 0, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(34).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 0, 2

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(17).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(18).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(64).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 2

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(58).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(59).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(60).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 2
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(61).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 3
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(62).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 4
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(63).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 3, 5

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(56).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 5
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(57).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 6
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(39).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 7
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(40).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 1, 8

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(35).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 2, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(36).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 2, 1

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(53).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 3, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(37).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 3, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(38).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 3, 2

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(50).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 4, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(51).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 4, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(52).ExtractIcon)
menu.Assign MainMDI, Ret, 2, 4, 2

'13, 11, 12, 31, 32, 19
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(13).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 0
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(11).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 1
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(12).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 2
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(31).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 4
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(32).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 5
Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(19).ExtractIcon)
menu.Assign MainMDI, Ret, 3, 6

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(26).ExtractIcon)
menu.Assign MainMDI, Ret, 4, 4

Ret = menu.HIconToMenuHBitmap(.Picture1, .ImageList2.ListImages(27).ExtractIcon)
menu.Assign MainMDI, Ret, 5, 0

End With
End Sub

Public Function GetVersion()
Dim MD5 As New MD5Module
Dim MD5str As String
Dim userid As String
Dim regke As RegKey
Dim buf As String * 3
buf = GetSetting("Trading Session 2", "MISC", "Version")
If InStr(1, buf, "std") Then
    buf = "std"
Else
Set regke = RegKeyFromString("\HKEY_CURRENT_USER\Software\VB and VBA Program Settings\Trading Session 2\Market Access\User")
MD5str = MD5.MD5("pro" & regke.Values.Item("Id").Value)
Set regke = RegKeyFromString("\HKEY_CURRENT_USER\Software\VB and VBA Program Settings\Trading Session 2\Misc")
userid = regke.Values.Item("Version").Value
If MD5str = (regke.Values("Version").Value) Then
    buf = "pro"
Else
    buf = "std"
End If
End If
GetVersion = buf
End Function
