VERSION 5.00
Begin VB.Form frmRegisterTrigger 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nytt bevakningsobjekt"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3270
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3645
   ScaleWidth      =   3270
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   2880
      Width           =   3255
      Begin VB.CommandButton cmbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2040
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmbOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Larmdata"
      Height          =   2775
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3255
      Begin VB.TextBox txtLowerLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         TabIndex        =   13
         Top             =   2280
         Width           =   1815
      End
      Begin VB.TextBox txtUpperLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         TabIndex        =   12
         Top             =   1800
         Width           =   1815
      End
      Begin VB.TextBox txtClose 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   1320
         Width           =   1815
      End
      Begin VB.TextBox txtDate 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         TabIndex        =   9
         Top             =   840
         Width           =   1815
      End
      Begin VB.ComboBox cboObject 
         Height          =   315
         Left            =   1200
         TabIndex        =   5
         Top             =   360
         Width           =   1815
      End
      Begin VB.Label Label5 
         Caption         =   "Senast betalt"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Undre gr�ns"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "�vre gr�ns"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Datum"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Objekt"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmRegisterTrigger"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
  Dim i As Integer
  
  For i = 1 To Upperbound(InvestInfoArraySize)
    cboObject.AddItem Trim(InvestInfoArray(i).Name)
  Next i
  
  txtDate = Date
  
  
  
End Sub
