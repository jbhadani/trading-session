Attribute VB_Name = "Highlight"
Option Explicit

Public Enum ToggleHighlightEnum
  theAdded = 1
  theRemoved = 2
  theFull = 3
End Enum

Public Type HighlightType
  blnValid As Boolean
  dtmStart As Date
  dwmType As DWMEnum
End Type

Const MAX_NBR_OF_HIGHLIGHTS As Long = 30
Dim mHighlighArray(1 To MAX_NBR_OF_HIGHLIGHTS) As HighlightType
Dim mNbrOfValidHighlights As Long

'#If FISH = 1 Then
Function ToggleHighlight(theHighlight As HighlightType) As ToggleHighlightEnum
If PRO = False Then Exit Function
  Dim Found As Boolean
  Dim I As Long
  Dim LowestFree As Long
  '''''''''''
  ' Temporary fix
  ''''''''''''''
  
  ToggleHighlight = theAdded
  Exit Function
  ''''''''''''
   
   
  'Search for the highlight
  LowestFree = MAX_NBR_OF_HIGHLIGHTS + 1
  Found = False
  I = 1
  Do While (Not Found) And I <= MAX_NBR_OF_HIGHLIGHTS
    If mHighlighArray(I).blnValid And mHighlighArray(I).dtmStart = theHighlight.dtmStart Then
      Found = True
    Else
      If mHighlighArray(I).blnValid = False And I < LowestFree Then
        LowestFree = I
      End If
      I = I + 1
    End If
  Loop
  If Found Then
    mHighlighArray(I).blnValid = False
    ToggleHighlight = theRemoved
    mNbrOfValidHighlights = mNbrOfValidHighlights - 1
    Debug.Print "Removed " & CStr(I)
  Else
    If LowestFree <= MAX_NBR_OF_HIGHLIGHTS Then
      theHighlight.blnValid = True
      mHighlighArray(LowestFree) = theHighlight
      mNbrOfValidHighlights = mNbrOfValidHighlights + 1
      ToggleHighlight = theAdded
      Debug.Print "Added " & CStr(LowestFree)
    Else
      ToggleHighlight = theFull
      Debug.Print "Full"
    End If
  End If
  
End Function

Sub FreeHighlights()
If PRO = False Then Exit Sub
Dim I As Long
  For I = 1 To MAX_NBR_OF_HIGHLIGHTS
    mHighlighArray(I).blnValid = False
  Next I
  mNbrOfValidHighlights = 0
End Sub

Sub GetHighlights(ByRef Highlights() As HighlightType)
If PRO = False Then Exit Sub
Dim Index As Long
Dim I As Long
ReDim Highlights(1 To mNbrOfValidHighlights)
  Index = 1
  For I = 1 To MAX_NBR_OF_HIGHLIGHTS
    If mHighlighArray(I).blnValid Then
      Highlights(Index) = mHighlighArray(I)
      Index = Index + 1
    End If
  Next I
End Sub







'#End If


