Attribute VB_Name = "ReportProc"
Option Explicit

Sub Report_PortfolioMixInvest(LatestDate As Date)
Dim i As Integer

Dim TotalInvested As Single
Dim Interpolated As Boolean
Dim TotalValue
Dim TmpString As String
With Frm_RepList
.Show
.Caption = "Sammans�ttning - Objekt"
.Width = 4950
End With
With Frm_RepList.Grid1
.Clear
.Cols = 3
.Rows = PortfolioArraySize + 1
.Row = 0
.Col = 0
.ColWidth(0) = 2000
.Text = "Objekt"
.Col = 1
.ColWidth(1) = 1400
.Text = "Andel investerat"
.Col = 2
.ColWidth(2) = 1400
.Text = "Andel v�rde"
TotalInvested = GetTotalInvested(LatestDate)
TotalValue = GetTotalValue(LatestDate)
For i = 1 To PortfolioArraySize
.Col = 0
.Row = i
.Text = PortfolioArray(i).Name
.Col = 1
 TmpString = Str(GetInvested(PortfolioArray(i).Name, LatestDate) / TotalInvested * 100)
    
     
.Text = Format$(PointToComma(TmpString), ".0")
.Col = 2
TmpString = Str(GetVolume(PortfolioArray(i).Name, LatestDate) * GetPrice(LatestDate, PortfolioArray(i).Name) / TotalValue * 100)
.Text = Format$(PointToComma(TmpString), ".0")
Next i
End With
End Sub
Sub Report_PortfolioMixCategory(LatestDate As Date)

End Sub

Sub Report_PortfolioPerformance(LatestDate As Date)
Dim Invested As Single
Dim TotalInv As Single
Dim i As Integer
Dim Price As Single
Dim Interpolated As Boolean
Dim volume As Single
Dim TotalValue As Single
Dim GainPer  As Single
Dim TmpString As String
Dim TotalInsatt As Double
Dim Saldo As Double


    
    DoEvents
    With Frm_RepList.Grid1
    .Redraw = False
    .Clear
    .GridLines = False
    .Cols = 5
    .Rows = PortfolioArraySize + 3
    DoEvents
    .Row = 0
    .Col = 0
    .ColWidth(0) = 2000
    .Text = "Objekt"
    .Col = 1
    .ColWidth(1) = 1800
    .Text = "Investerat"
    .Col = 2
    .ColWidth(2) = 1800
    .Text = "V�rde"
    .Col = 3
    .ColWidth(3) = 1800
    .Text = "Resultat"
    .Col = 4
    .ColWidth(4) = 1200
    .Text = "Avkastning"
    
    TotalInsatt = 0
    Saldo = 0
    For i = 1 To TransactionArraySize
      If TransactionArray(i).IsDeposit And TransactionArray(i).Date <= LatestDate Then
        If Trim(TransactionArray(i).Name) = "Ins�ttning" Then
          TotalInsatt = TotalInsatt + TransactionArray(i).Cash
        Else
          TotalInsatt = TotalInsatt - TransactionArray(i).Cash
        End If
      End If
      If TransactionArray(i).IsBuySell And TransactionArray(i).Date <= LatestDate Then
        If TransactionArray(i).IsBuy Then
          Saldo = Saldo - TransactionArray(i).Price * TransactionArray(i).volume - TransactionArray(i).Courtage
        Else
          Saldo = Saldo + TransactionArray(i).Price * TransactionArray(i).volume - TransactionArray(i).Courtage
        End If
      End If
    Next i
    Saldo = Saldo + TotalInsatt
    .Row = 1
    .Col = 0
    .Text = "Insatt kapital"
    .Col = 1
    TmpString = Str(TotalInsatt)
    .Text = Format$(PointToComma(TmpString), ".00")
    
    TotalInv = 0
    TotalValue = GetTotalValue(LatestDate)
    TotalValue = TotalValue + Saldo
    
    For i = 1 To PortfolioArraySize
    .Row = i + 1
    .Col = 0
    .Text = PortfolioArray(i).Name
    .Col = 1
    Invested = GetInvested(PortfolioArray(i).Name, LatestDate)
    TmpString = Str(Invested)
    .Text = Format$(PointToComma(TmpString), ".00")
    TotalInv = TotalInv + Invested
    .Col = 2
    Price = GetPrice(LatestDate, PortfolioArray(i).Name)
    volume = GetVolume(PortfolioArray(i).Name, LatestDate)
    TmpString = Str(volume * Price)
    .Text = Format$(PointToComma(TmpString), ".00")
    If Interpolated Then
  .Text = .Text + "*"
End If
  

.Col = 3
TmpString = Str(volume * Price - Invested)
If Val(TmpString) > 0 Then
  .CellForeColor = RGB(0, 0, 255)
Else
  .CellForeColor = RGB(255, 0, 0)
End If
.Text = Format$(PointToComma(TmpString), ".00")
.Col = 4
If Invested > 0 Then
  GainPer = (volume * Price / Invested - 1) * 100
  TmpString = Str(GainPer)
  If Val(TmpString) > 0 Then
  .CellForeColor = RGB(0, 0, 255)
  Else
  .CellForeColor = RGB(255, 0, 0)
  End If

  .Text = Format$(PointToComma(TmpString), ".0")
Else
  .Text = ""
  End If
Next i
.Row = PortfolioArraySize + 2
.Col = 0
.CellFontBold = True
.Text = "Total:"
.Col = 1
.CellFontBold = True
TmpString = Str(TotalInsatt)
.Text = Format$(PointToComma(TmpString), ".00")
.Col = 2
.CellFontBold = True
TmpString = Str(TotalValue)
.Text = Format$(PointToComma(TmpString), ".00")
.Col = 3
.CellFontBold = True
TmpString = Str(TotalValue - TotalInsatt)
.Text = Format$(PointToComma(TmpString), ".00")
If Val(TmpString) > 0 Then
  .CellForeColor = RGB(0, 0, 255)
Else
  .CellForeColor = RGB(255, 0, 0)
End If

.Col = 4
.CellFontBold = True
If TotalInv > 0 Then
  TmpString = Str(((TotalValue - TotalInsatt) / TotalInsatt) * 100)
  If Val(TmpString) > 0 Then
  .CellForeColor = RGB(0, 0, 255)
Else
  .CellForeColor = RGB(255, 0, 0)
End If

  .Text = Format$(PointToComma(TmpString), ".0")
Else
  .Text = ""
End If


.Redraw = True
End With

Frm_RepList.Show 0
End Sub

