Attribute VB_Name = "Window_Proc"
Sub ShowFrm_Graph()
  ShowGraph = True
  MainMDI.MousePointer = 11
  Load Frm_Graph
  Caption = MainMDI.C_StockList.Text + "   " + Str$(PriceDataArray(1).Date) + " to " + Str$(PriceDataArray(PriceDataArraySize).Date)
  Frm_Graph.Caption = Caption
  Graph
  Frm_Graph.Show
  MainMDI.MousePointer = 0
End Sub
