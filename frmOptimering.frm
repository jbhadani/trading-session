VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmOptimize 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Optimering"
   ClientHeight    =   6765
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7995
   Icon            =   "frmOptimering.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6765
   ScaleWidth      =   7995
   Begin VB.Frame Frame7 
      Caption         =   "Optimeringsobjekt"
      Height          =   855
      Left            =   0
      TabIndex        =   45
      Top             =   3000
      Width           =   3375
      Begin VB.Label lblObject 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   300
         TabIndex        =   46
         Top             =   300
         Width           =   2655
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "V�lj objekt"
      Height          =   2175
      Left            =   0
      TabIndex        =   43
      Top             =   720
      Width           =   3375
      Begin MSComctlLib.TreeView treobjects 
         Height          =   1815
         Left            =   120
         TabIndex        =   44
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   3201
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   680
      Left            =   0
      Picture         =   "frmOptimering.frx":014A
      ScaleHeight     =   615
      ScaleWidth      =   7875
      TabIndex        =   42
      Top             =   0
      Width           =   7935
   End
   Begin VB.Frame Frame1 
      Caption         =   "Optimeringsintervall"
      Height          =   975
      Left            =   0
      TabIndex        =   30
      Top             =   3960
      Width           =   3375
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   300
         Left            =   1920
         TabIndex        =   34
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   62390273
         CurrentDate     =   36388
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   300
         Left            =   240
         TabIndex        =   33
         Top             =   480
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Enabled         =   0   'False
         Format          =   62390273
         CurrentDate     =   36388
      End
      Begin VB.Line Line9 
         X1              =   1560
         X2              =   1800
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Label Label15 
         Caption         =   "Slutdatum"
         Height          =   255
         Left            =   2160
         TabIndex        =   32
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Startdatum"
         Height          =   255
         Left            =   360
         TabIndex        =   31
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Courtage"
      Height          =   2175
      Left            =   3480
      TabIndex        =   20
      Top             =   3720
      Width           =   4455
      Begin VB.OptionButton OptionDontUseCourtage 
         Caption         =   "Anv�nd ej Courtage"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         TabStop         =   0   'False
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.OptionButton OptionMinCourtage 
         Caption         =   "Min"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         TabStop         =   0   'False
         Top             =   960
         Width           =   615
      End
      Begin VB.OptionButton OptionPercentCourtage 
         Caption         =   " %"
         Height          =   255
         Left            =   360
         TabIndex        =   24
         TabStop         =   0   'False
         Top             =   1800
         Width           =   615
      End
      Begin VB.TextBox tbMinCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         MultiLine       =   -1  'True
         TabIndex        =   23
         TabStop         =   0   'False
         Text            =   "frmOptimering.frx":4E80
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox tbInvBelopp 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2880
         MultiLine       =   -1  'True
         TabIndex        =   22
         TabStop         =   0   'False
         Text            =   "frmOptimering.frx":4E83
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox tbCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2040
         MultiLine       =   -1  'True
         TabIndex        =   21
         TabStop         =   0   'False
         Text            =   "frmOptimering.frx":4E89
         Top             =   1800
         Width           =   975
      End
      Begin VB.Line Line8 
         X1              =   960
         X2              =   4080
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line7 
         X1              =   960
         X2              =   4200
         Y1              =   600
         Y2              =   600
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Minimicourtage"
         Height          =   255
         Left            =   1080
         TabIndex        =   29
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Investeringsbelopp"
         Height          =   255
         Left            =   2640
         TabIndex        =   28
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         Caption         =   "Courtage, %"
         Height          =   255
         Left            =   1920
         TabIndex        =   27
         Top             =   1560
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Start- och slutv�rden f�r parametrar"
      Height          =   2895
      Left            =   3480
      TabIndex        =   12
      Top             =   720
      Width           =   4455
      Begin MSComCtl2.UpDown UpDownStep 
         Height          =   285
         Left            =   2520
         TabIndex        =   41
         Top             =   2475
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   2
         BuddyControl    =   "tbSteglangd"
         BuddyDispid     =   196634
         OrigLeft        =   3360
         OrigTop         =   3120
         OrigRight       =   3600
         OrigBottom      =   3375
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStop3 
         Height          =   285
         Left            =   3720
         TabIndex        =   40
         Top             =   2040
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   50
         BuddyControl    =   "tbSlutvarde3"
         BuddyDispid     =   196636
         OrigLeft        =   3720
         OrigTop         =   2400
         OrigRight       =   3960
         OrigBottom      =   2655
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStart3 
         Height          =   285
         Left            =   1320
         TabIndex        =   39
         Top             =   2040
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "tbStartvarde3"
         BuddyDispid     =   196635
         OrigLeft        =   1320
         OrigTop         =   2400
         OrigRight       =   1560
         OrigBottom      =   2775
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStop2 
         Height          =   285
         Left            =   3720
         TabIndex        =   38
         Top             =   1320
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   50
         BuddyControl    =   "tbSlutvarde2"
         BuddyDispid     =   196633
         OrigLeft        =   3600
         OrigTop         =   1560
         OrigRight       =   3840
         OrigBottom      =   1935
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStop1 
         Height          =   285
         Left            =   3720
         TabIndex        =   37
         Top             =   600
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   50
         BuddyControl    =   "tbSlutvarde1"
         BuddyDispid     =   196632
         OrigLeft        =   3480
         OrigTop         =   720
         OrigRight       =   3720
         OrigBottom      =   975
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStart2 
         Height          =   285
         Left            =   1320
         TabIndex        =   36
         Top             =   1320
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "tbStartvarde2"
         BuddyDispid     =   196631
         OrigLeft        =   1320
         OrigTop         =   1440
         OrigRight       =   1560
         OrigBottom      =   1725
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin MSComCtl2.UpDown UpDownStart1 
         Height          =   285
         Left            =   1320
         TabIndex        =   35
         Top             =   600
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "tbStartvarde1"
         BuddyDispid     =   196630
         OrigLeft        =   3120
         OrigTop         =   360
         OrigRight       =   3360
         OrigBottom      =   615
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
      Begin VB.TextBox tbStartvarde1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   3
         Text            =   "frmOptimering.frx":4E8E
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox tbStartvarde2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   4
         Text            =   "frmOptimering.frx":4E90
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox tbSlutvarde1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2760
         MultiLine       =   -1  'True
         TabIndex        =   5
         Text            =   "frmOptimering.frx":4E93
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox tbSlutvarde2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2760
         MultiLine       =   -1  'True
         TabIndex        =   6
         Text            =   "frmOptimering.frx":4E96
         Top             =   1320
         Width           =   975
      End
      Begin VB.TextBox tbSteglangd 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1560
         MultiLine       =   -1  'True
         TabIndex        =   9
         Text            =   "frmOptimering.frx":4E99
         Top             =   2475
         Width           =   975
      End
      Begin VB.TextBox tbStartvarde3 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   7
         Text            =   "frmOptimering.frx":4E9B
         Top             =   2040
         Width           =   975
      End
      Begin VB.TextBox tbSlutvarde3 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   2760
         MultiLine       =   -1  'True
         TabIndex        =   8
         Text            =   "frmOptimering.frx":4E9D
         Top             =   2040
         Width           =   975
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00FF0000&
         X1              =   1680
         X2              =   2640
         Y1              =   2160
         Y2              =   2160
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00FF0000&
         X1              =   120
         X2              =   4320
         Y1              =   2400
         Y2              =   2400
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00FF0000&
         X1              =   1680
         X2              =   2640
         Y1              =   1440
         Y2              =   1440
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FF0000&
         X1              =   120
         X2              =   4320
         Y1              =   1680
         Y2              =   1680
      End
      Begin VB.Line Line2 
         BorderColor     =   &H00FF0000&
         X1              =   120
         X2              =   4320
         Y1              =   960
         Y2              =   960
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00FF0000&
         X1              =   1680
         X2              =   2640
         Y1              =   720
         Y2              =   720
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Startv�rde parameter 1"
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Startv�rde parameter 2"
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "Slutv�rde parameter1"
         Height          =   255
         Left            =   2520
         TabIndex        =   17
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "Slutv�rde parameter 2"
         Height          =   255
         Left            =   2520
         TabIndex        =   16
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Caption         =   "Stegl�ngd"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   2520
         Width           =   1095
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Caption         =   "Startv�rde parameter 3"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         Caption         =   "Slutv�rde parameter 3"
         Height          =   255
         Left            =   2520
         TabIndex        =   13
         Top             =   1800
         Width           =   1695
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "V�lj indikator"
      Height          =   855
      Left            =   0
      TabIndex        =   11
      Top             =   5040
      Width           =   3375
      Begin VB.ComboBox cMethod 
         Height          =   315
         ItemData        =   "frmOptimering.frx":4E9F
         Left            =   600
         List            =   "frmOptimering.frx":4EA1
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame Frame5 
      Height          =   735
      Left            =   0
      TabIndex        =   10
      Top             =   6000
      Width           =   7935
      Begin VB.CommandButton Command4 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   6360
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Optimera"
         Default         =   -1  'True
         Enabled         =   0   'False
         Height          =   375
         Left            =   600
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmOptimize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cMethod_Click()
Dim Method As String

Method = Trim(cMethod.Text)

If StrComp(Method, "Stochastic Oscillator") = 0 Then
  tbStartvarde1.Enabled = True
  UpDownStart1.Enabled = True
  tbSlutvarde1.Enabled = True
  UpDownStop1.Enabled = True
  tbStartvarde2.Enabled = True
  UpDownStart2.Enabled = True
  tbSlutvarde2.Enabled = True
  UpDownStop2.Enabled = True
  tbStartvarde3.Enabled = True
  UpDownStart3.Enabled = True
  tbSlutvarde3.Enabled = True
  UpDownStop3.Enabled = True
ElseIf StrComp(Method, "Parabolic") = 0 Then
  tbStartvarde1.Enabled = False
  UpDownStart1.Enabled = False
  tbSlutvarde1.Enabled = False
  UpDownStop1.Enabled = False
  tbStartvarde2.Enabled = False
  UpDownStart2.Enabled = False
  tbSlutvarde2.Enabled = False
  UpDownStop2.Enabled = False
  tbStartvarde3.Enabled = False
  UpDownStart3.Enabled = False
  tbSlutvarde3.Enabled = False
  UpDownStop3.Enabled = False
Else
  tbStartvarde1.Enabled = True
  UpDownStart1.Enabled = True
  tbSlutvarde1.Enabled = True
  UpDownStop1.Enabled = True
  tbStartvarde2.Enabled = True
  UpDownStart2.Enabled = True
  tbSlutvarde2.Enabled = True
  UpDownStop2.Enabled = True
  tbStartvarde3.Enabled = False
  UpDownStart3.Enabled = False
  tbSlutvarde3.Enabled = False
  UpDownStop3.Enabled = False
End If
End Sub

Private Sub Command1_Click()
  'Control frmOptimering
  Optimering
End Sub

Private Sub Command4_Click()
  Unload frmOptimize
End Sub

Private Sub Form_Load()
  Dim i As Long
  Dim j As Long
  Dim strObjectName As String
  Dim udtObject As InvestInfoType
  Dim nodX As Node
  
  With treObjects
    .LineStyle = tvwRootLines
    With .Nodes
      For j = 1 To CategoryArraySize
        If Trim(CategoryArray(j)) <> "" Then
          Set nodX = .Add(, , Trim(CategoryArray(j)), Trim(CategoryArray(j)))
          For i = 1 To InvestInfoArraySize
            If InvestInfoArray(i).TypeOfInvest = j Then
              Set nodX = .Add(Trim(CategoryArray(j)), tvwChild, Trim(InvestInfoArray(i).Name), Trim(InvestInfoArray(i).Name))
            End If
          Next i
        End If
      Next j
    End With
  End With
  
  'DTPicker1.MaxDate = GetDateString(PriceDataArray(PriceDataArraySize).Date)
  'DTPicker2.MinDate = GetDateString(PriceDataArray(1).Date)
 
  'DTPicker1.Value = GetDateString(PriceDataArray(1).Date)
  'DTPicker2.Value = GetDateString(PriceDataArray(PriceDataArraySize).Date)
  
  top = 100
  left = 600
  
  OptionDontUseCourtage_Click '??????
  
  With cMethod
    .AddItem "Ease Of Movement"
    .AddItem "Momentum"
    .AddItem "Money Flow Index"
    .AddItem "Moving Average"
    .AddItem "Parabolic"
    .AddItem "Price oscillator"
    .AddItem "Rate-of-change"
    .AddItem "Relative Strength Index"
    .AddItem "Stochastic Oscillator"
    .AddItem "TRIX"
    .ListIndex = 0
  End With

  
    
  UpDownStart1.Value = 5
  UpDownStart2.Value = 5
  UpDownStart3.Value = 5
  UpDownStop1.Value = 50
  UpDownStop2.Value = 50
  UpDownStop3.Value = 50
  UpDownStep.Value = 2
  
  tbStartvarde3.Enabled = False
  UpDownStart3.Enabled = False
  tbSlutvarde3.Enabled = False
  UpDownStop3.Enabled = False
  ChangeFormLang Me
End Sub



Private Sub OptionDontUseCourtage_Click()
  tbMinCourtage.Enabled = False
  tbInvBelopp.Enabled = False
  tbCourtage.Enabled = False
End Sub

Private Sub OptionMinCourtage_Click()
  tbCourtage.Enabled = False
  tbMinCourtage.Enabled = True
  tbInvBelopp.Enabled = True
End Sub

Private Sub OptionPercentCourtage_Click()
  tbMinCourtage.Enabled = False
  tbInvBelopp.Enabled = False
  tbCourtage.Enabled = True

End Sub

Private Sub tbCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbCourtage_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySinglePara tbCourtage, Errorfound
  If Errorfound Then
    Cancel = True
  End If
End Sub

Private Sub tbInvBelopp_GotFocus()
  TextSelected
End Sub

Private Sub tbInvBelopp_Validate(Cancel As Boolean)
  Dim A As Long
  Dim Error As Boolean
  
  On Error Resume Next
  A = CLng(tbInvBelopp.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), 16, CheckLang("Inget heltal")
    Err.Clear
    Error = True
  End If
  If Error Then
    Cancel = True
  Else
    Cancel = False
  End If
  tbInvBelopp.SetFocus
End Sub

Private Sub tbMinCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbMinCourtage_Validate(Cancel As Boolean)
Dim A As Long
  Dim Error As Boolean
  
  On Error Resume Next
  A = CLng(tbMinCourtage.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), 16, CheckLang("Inget heltal")
    Err.Clear
    Error = True
  End If
  If Error Then
    Cancel = True
  Else
    Cancel = False
  End If
  tbMinCourtage.SetFocus
End Sub

Private Sub tbSlutvarde1_GotFocus()
  TextSelected
End Sub

Private Sub tbSlutvarde1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbSlutvarde1, UpDownStop1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbSlutvarde2_GotFocus()
  TextSelected
End Sub

Private Sub tbSlutvarde2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbSlutvarde2, UpDownStop2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbSlutvarde3_GotFocus()
  TextSelected
End Sub

Private Sub tbSlutvarde3_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbSlutvarde3, UpDownStop3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbStartvarde1_GotFocus()
  TextSelected
End Sub

Private Sub tbStartvarde1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbStartvarde1, UpDownStart1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbStartvarde2_GotFocus()
  TextSelected
End Sub

Private Sub tbStartvarde2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbStartvarde2, UpDownStart2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbStartvarde3_GotFocus()
  TextSelected
End Sub

Private Sub tbStartvarde3_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbStartvarde3, UpDownStart3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbSteglangd_GotFocus()
  TextSelected
End Sub

Private Sub tbSteglangd_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbSteglangd, UpDownStep, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub treObjects_NodeClick(ByVal Node As MSComctlLib.Node)
  Dim Filename1 As String
  Dim PDArray() As PriceDataType
  Dim PDArraySize As Long
  If Node.Children = 0 Then
    lblObject.Caption = Node.key
    OptimizeObject = Trim(lblObject.Caption)
    Command1.Enabled = True
    Filename1 = DataPath + Trim(InvestInfoArray(GetInvestIndex(OptimizeObject)).FileName) + ".prc"
    R_PriceDataArray Filename1, PDArray(), PDArraySize
    DTPicker1.MaxDate = GetDateString(PDArray(PDArraySize).Date)
    DTPicker2.MinDate = GetDateString(PDArray(1).Date)
    DTPicker1.Value = GetDateString(PDArray(1).Date)
    DTPicker2.Value = GetDateString(PDArray(PDArraySize).Date)
    DTPicker1.Enabled = True
    DTPicker2.Enabled = True
  End If
End Sub
