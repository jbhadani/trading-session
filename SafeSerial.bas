Attribute VB_Name = "SafeSerial"
Option Explicit

Function LicenseCheck() As Boolean
With MainMDI.SS1
Dim IR As Long
    Debug.Print "Install date:" & CStr(CDate(.ReadInstallDate))
    Debug.Print "License type:" & CStr(.ReadLicenseType)
    Debug.Print "Max executions:" & CStr(.ReadMaxExecs)
    Debug.Print "Executions:" & CStr(.ReadExecs)
    Debug.Print "Counter:" & CStr(.readcounter)
    Debug.Print "Days allowed:" & CStr(.ReadDaysAllowed)
    
    'MsgBox "Install date:" & CStr(CDate(.GetInstallDate))
    'MsgBox "License type:" & CStr(.GetLicenseType)
    'MsgBox "Max executions:" & CStr(.GetMaxExecs)
    'MsgBox "Executions:" & CStr(.GetExecs)
    'MsgBox "Counter:" & CStr(.GetCounter)
    'MsgBox "Days allowed:" & CStr(.GetDaysAllowed)
    'IR = .IsRegistered
    'If IR = 420 Then
    '  MsgBox "Is registered (Code: " & CStr(IR) & ")"
    'Else
    '  MsgBox "Is NOT registered (Code: " & CStr(IR) & ")"
    'End If
    '.Execution
    'If .Checklicense <> 420 Then End
    '
    .Execution
    .Checklicense
End With
End Function

Sub MaxExecsDone()
  InformationalMessage "Antalet exekveringar f�r en olicensierad produkt har nu �verskridits. F�r att forts�tta anv�nda produkten var v�nlig att registrera denna."
  'MainMDI.SS1.RegisterDlg
  End
  'Debug.Print "MaxExecsDone"
End Sub

Sub NotRegistered()
  'InformationalMessage "Antalet exekveringar f�r en olicensierad produkt har nu �verskridits. F�r att forts�tta anv�nda produkten var v�nlig att registrera denna."
  MainMDI.SS1.RegisterDlg
  'Debug.Print "Not registered"
End Sub

Sub TimeExpired()
  InformationalMessage "Antalet dagar f�r en olicensierad produkt har nu �verskridits. F�r att forts�tta anv�nda produkten var v�nlig att registrera denna."
  MainMDI.SS1.RegisterDlg
  End
  'Debug.Print "TimeExpired"
End Sub

' Return: -1  if the encoded sum is false
'         0   if not registered
'         1   if registered
          
Function ProductRegistered() As Long
  If MainMDI.SS1.IsRegistered = 0 Then
    ProductRegistered = 0
  ElseIf MainMDI.SS1.IsRegistered = 420 Then
    ProductRegistered = 1
  Else
    ProductRegistered = -1
End If
End Function



Function LicensedDays() As Long
  LicensedDays = CLng(MainMDI.SS1.ReadDaysAllowed)
End Function

Function LicensedExecutions() As Long
  LicensedExecutions = CLng(MainMDI.SS1.ReadMaxExecs)
End Function

Function NbrOfExecutions() As Long
  NbrOfExecutions = CLng(MainMDI.SS1.ReadExecs)
End Function

Function NbrOFDays() As Long
  NbrOFDays = CLng(Date) - CLng(MainMDI.SS1.ReadInstallDate)
End Function
