VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmOptimize 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Optimering"
   ClientHeight    =   5430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7830
   Icon            =   "frmOptimize.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5430
   ScaleWidth      =   7830
   Begin VB.Frame Frame2 
      Caption         =   "Courtage"
      Height          =   2895
      Index           =   1
      Left            =   0
      TabIndex        =   7
      Top             =   1800
      Width           =   2775
      Begin VB.HScrollBar HScroll_Courtage 
         Height          =   135
         LargeChange     =   10
         Left            =   1320
         Max             =   200
         TabIndex        =   16
         TabStop         =   0   'False
         Top             =   2640
         Value           =   65
         Width           =   975
      End
      Begin VB.TextBox tbCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         MultiLine       =   -1  'True
         TabIndex        =   15
         TabStop         =   0   'False
         Text            =   "frmOptimize.frx":014A
         Top             =   2280
         Width           =   735
      End
      Begin VB.HScrollBar HScroll_InvBelopp 
         Height          =   135
         LargeChange     =   100
         Left            =   1320
         Max             =   10000
         Min             =   1
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   1800
         Value           =   1000
         Width           =   975
      End
      Begin VB.TextBox tbInvBelopp 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         MultiLine       =   -1  'True
         TabIndex        =   13
         TabStop         =   0   'False
         Text            =   "frmOptimize.frx":014F
         Top             =   1440
         Width           =   735
      End
      Begin VB.HScrollBar HScroll_MinCourtage 
         Height          =   135
         LargeChange     =   25
         Left            =   1320
         Max             =   2000
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   1080
         Value           =   50
         Width           =   975
      End
      Begin VB.TextBox tbMinCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1440
         MultiLine       =   -1  'True
         TabIndex        =   11
         TabStop         =   0   'False
         Text            =   "frmOptimize.frx":0155
         Top             =   720
         Width           =   735
      End
      Begin VB.OptionButton OptionPercentCourtage 
         Caption         =   " %"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         TabStop         =   0   'False
         Top             =   2280
         Width           =   615
      End
      Begin VB.OptionButton OptionMinCourtage 
         Caption         =   "Min"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         TabStop         =   0   'False
         Top             =   1080
         Width           =   615
      End
      Begin VB.OptionButton OptionDontUseCourtage 
         Caption         =   "Anv�nd ej Courtage"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         TabStop         =   0   'False
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.Label Label5 
         Caption         =   "Courtage, %"
         Height          =   255
         Left            =   1320
         TabIndex        =   19
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label3 
         Caption         =   "Investerings Belopp"
         Height          =   255
         Left            =   1080
         TabIndex        =   18
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label2 
         Caption         =   "Min Courtage"
         Height          =   255
         Left            =   1320
         TabIndex        =   17
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Parameterintervall"
      Height          =   2895
      Left            =   2880
      TabIndex        =   6
      Top             =   1800
      Width           =   4935
      Begin VB.Label Label10 
         Caption         =   "Slutv�rde Parameter 3"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   2160
         Width           =   2295
      End
      Begin VB.Label Label9 
         Caption         =   "Startv�rde Parameter 3"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1800
         Width           =   2295
      End
      Begin VB.Label Label8 
         Caption         =   "Stegl�ngd"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   2520
         Width           =   1695
      End
      Begin VB.Label Label7 
         Caption         =   "Slutv�rde Parameter 2"
         Height          =   255
         Left            =   240
         TabIndex        =   23
         Top             =   1440
         Width           =   2295
      End
      Begin VB.Label Label6 
         Caption         =   "Slutv�rde Parameter1"
         Height          =   255
         Left            =   240
         TabIndex        =   22
         Top             =   1080
         Width           =   2295
      End
      Begin VB.Label Label4 
         Caption         =   "Startv�rde Parameter 2"
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   720
         Width           =   2295
      End
      Begin VB.Label Label1 
         Caption         =   "Startv�rde Parameter 1"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   360
         Width           =   2295
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Optimeringsintervall"
      Height          =   975
      Index           =   0
      Left            =   2880
      TabIndex        =   3
      Top             =   720
      Width           =   4935
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   300
         Left            =   3120
         TabIndex        =   5
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   529
         _Version        =   393216
         Format          =   24444929
         CurrentDate     =   36388
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   300
         Left            =   600
         TabIndex        =   4
         Top             =   360
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   529
         _Version        =   393216
         Format          =   24444929
         CurrentDate     =   36388
      End
      Begin VB.Line Line1 
         X1              =   1920
         X2              =   3000
         Y1              =   480
         Y2              =   480
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "V�lj indikator som skall optimeras"
      Height          =   975
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   2775
      Begin VB.ComboBox cMethod 
         Height          =   315
         ItemData        =   "frmOptimize.frx":0158
         Left            =   360
         List            =   "frmOptimize.frx":0174
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   680
      Left            =   0
      Picture         =   "frmOptimize.frx":01FA
      ScaleHeight     =   615
      ScaleWidth      =   7755
      TabIndex        =   0
      Top             =   0
      Width           =   7815
   End
End
Attribute VB_Name = "frmOptimize"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
