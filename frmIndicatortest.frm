VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmIndicatortest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Indikatortest"
   ClientHeight    =   5340
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6915
   Icon            =   "frmIndicatortest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5340
   ScaleWidth      =   6915
   Begin VB.PictureBox Picture1 
      Height          =   615
      Left            =   50
      Picture         =   "frmIndicatortest.frx":014A
      ScaleHeight     =   555
      ScaleWidth      =   6795
      TabIndex        =   11
      Top             =   60
      Width           =   6855
   End
   Begin VB.Frame Frame3 
      Caption         =   "Inst�llningar"
      Height          =   855
      Left            =   3480
      TabIndex        =   9
      Top             =   3600
      Width           =   3375
      Begin VB.CommandButton Command1 
         Caption         =   "Inst�llningar..."
         Height          =   375
         Left            =   1680
         TabIndex        =   10
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Testobjekt"
      Height          =   855
      Left            =   3480
      TabIndex        =   7
      Top             =   720
      Width           =   3375
      Begin VB.Label lbltestobject 
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         Height          =   315
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   2895
      End
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   4560
      Width           =   6855
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   5160
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdStart 
         Caption         =   "Starta"
         Height          =   375
         Left            =   480
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "V�lj objekt"
      Height          =   3735
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   3375
      Begin MSComctlLib.TreeView treObjects 
         Height          =   3375
         Left            =   120
         TabIndex        =   5
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   5953
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "V�lj indikator"
      Height          =   975
      Left            =   3480
      TabIndex        =   0
      Top             =   1680
      Width           =   3375
      Begin VB.ComboBox cmbIndicator 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   6
         Top             =   360
         Width           =   2895
      End
   End
End
Attribute VB_Name = "frmIndicatortest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdStart_Click()
  If IndicatortestObject <> "" Then
    TestHandler
  End If
End Sub

Private Sub Command1_Click()
  If frmIndicatortest.lbltestobject <> "" Then
    frmIndicatortestOptions.Show
  End If
End Sub

Private Sub Form_Load()
  
  Dim i As Long
  Dim j As Long
  Dim strObjectName As String
  Dim udtObject As InvestInfoType
  Dim nodX As Node
  
  top = 500
  left = 500
  
  With treObjects
    .LineStyle = tvwRootLines
    With .Nodes
      For j = 1 To CategoryArraySize
        If Trim(CategoryArray(j)) <> "" Then
          Set nodX = .Add(, , Trim(CategoryArray(j)), Trim(CategoryArray(j)))
          For i = 1 To InvestInfoArraySize
            If InvestInfoArray(i).TypeOfInvest = j Then
              Set nodX = .Add(Trim(CategoryArray(j)), tvwChild, Trim(InvestInfoArray(i).Name), Trim(InvestInfoArray(i).Name))
            End If
          Next i
        End If
      Next j
    End With
  End With
  
  With cmbIndicator
    .Clear
    .AddItem "Ease Of Movement"
    .AddItem "MACD"
    .AddItem "Momentum"
    .AddItem "Money Flow Index"
    .AddItem "Moving Average"
    .AddItem "Parabolic"
    .AddItem "Price Oscillator"
    .AddItem "Rate-of-change"
    .AddItem "Relative Strength Index"
    .AddItem "Stochastic Oscillator"
    .AddItem "TRIX"
    .ListIndex = 0
  End With
  ChangeFormLang Me
End Sub

Private Sub treObjects_NodeClick(ByVal Node As MSComctlLib.Node)
  If Node.Children = 0 Then
    lbltestobject.Caption = Node.key
    IndicatortestObject = Node.key
  End If
End Sub
