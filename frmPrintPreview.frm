VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmPrintPreview 
   Caption         =   "Preview"
   ClientHeight    =   7695
   ClientLeft      =   915
   ClientTop       =   1380
   ClientWidth     =   6795
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   7.5
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmPrintPreview.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7695
   ScaleWidth      =   6795
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox bBar 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   300
      Left            =   0
      ScaleHeight     =   300
      ScaleWidth      =   6795
      TabIndex        =   17
      Top             =   0
      Width           =   6795
      Begin VB.ComboBox zVal 
         Height          =   288
         ItemData        =   "frmPrintPreview.frx":0442
         Left            =   3384
         List            =   "frmPrintPreview.frx":045E
         Style           =   2  'Dropdown List
         TabIndex        =   30
         ToolTipText     =   "zoom factor"
         Top             =   0
         Width           =   768
      End
      Begin VB.ComboBox ePag 
         Height          =   288
         ItemData        =   "frmPrintPreview.frx":0487
         Left            =   4212
         List            =   "frmPrintPreview.frx":0489
         Style           =   2  'Dropdown List
         TabIndex        =   29
         ToolTipText     =   "select (selected) page"
         Top             =   0
         Width           =   768
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   10
         Left            =   3024
         Picture         =   "frmPrintPreview.frx":048B
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "close"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   9
         Left            =   2700
         Picture         =   "frmPrintPreview.frx":058D
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "about..."
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   8
         Left            =   2376
         Picture         =   "frmPrintPreview.frx":068F
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "last page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   7
         Left            =   2088
         Picture         =   "frmPrintPreview.frx":0791
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "next page (PgDn)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   6
         Left            =   1800
         Picture         =   "frmPrintPreview.frx":0893
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "previous page (PgUp)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   5
         Left            =   1512
         Picture         =   "frmPrintPreview.frx":0995
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "first page"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   4
         Left            =   1188
         Picture         =   "frmPrintPreview.frx":0A97
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "select printer"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   3
         Left            =   900
         Picture         =   "frmPrintPreview.frx":0B99
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "print"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   2
         Left            =   576
         Picture         =   "frmPrintPreview.frx":0C9B
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "zoom out (-)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   1
         Left            =   288
         Picture         =   "frmPrintPreview.frx":0D9D
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "zoom in (+)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
      Begin VB.CommandButton Cmd 
         Height          =   280
         Index           =   0
         Left            =   0
         Picture         =   "frmPrintPreview.frx":0E9F
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "refresh (CR)"
         Top             =   0
         UseMaskColor    =   -1  'True
         Width           =   290
      End
   End
   Begin VB.PictureBox sBar 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   216
      Left            =   0
      ScaleHeight     =   210
      ScaleWidth      =   6795
      TabIndex        =   10
      Top             =   7485
      Width           =   6795
      Begin VB.Label zPag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   3096
         TabIndex        =   16
         Top             =   0
         Width           =   1668
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "zoom:"
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   2304
         TabIndex        =   15
         Top             =   0
         Width           =   768
      End
      Begin VB.Label tPag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   1764
         TabIndex        =   14
         Top             =   0
         Width           =   516
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "of"
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   1368
         TabIndex        =   13
         Top             =   0
         Width           =   372
      End
      Begin VB.Label aPag 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   828
         TabIndex        =   12
         Top             =   0
         Width           =   516
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         Caption         =   "page:"
         ForeColor       =   &H80000008&
         Height          =   264
         Left            =   36
         TabIndex        =   11
         Top             =   0
         Width           =   768
      End
   End
   Begin VB.FileListBox TmpList 
      Height          =   1650
      Left            =   4860
      TabIndex        =   9
      Top             =   1680
      Width           =   1632
      Visible         =   0   'False
   End
   Begin VB.PictureBox Img 
      Height          =   408
      Left            =   5148
      ScaleHeight     =   345
      ScaleWidth      =   345
      TabIndex        =   8
      Top             =   684
      Width           =   408
      Visible         =   0   'False
   End
   Begin MSComDlg.CommonDialog Apre 
      Left            =   5760
      Top             =   900
      _ExtentX        =   688
      _ExtentY        =   688
      _Version        =   393216
   End
   Begin VB.HScrollBar hBar 
      Height          =   156
      Left            =   108
      TabIndex        =   7
      Top             =   6768
      Width           =   4476
   End
   Begin VB.VScrollBar vBar 
      Height          =   6240
      Left            =   4608
      TabIndex        =   6
      Top             =   468
      Width           =   156
   End
   Begin VB.CommandButton Command6 
      Caption         =   "ref grid"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   264
      Left            =   3348
      TabIndex        =   5
      Top             =   8100
      Width           =   1164
      Visible         =   0   'False
   End
   Begin VB.CommandButton Command4 
      Caption         =   "Imposta A3"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   264
      Left            =   252
      TabIndex        =   4
      Top             =   8064
      Width           =   1164
      Visible         =   0   'False
   End
   Begin VB.CommandButton Command3 
      Caption         =   "Imposta B5"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   264
      Left            =   252
      TabIndex        =   3
      Top             =   8640
      Width           =   1164
      Visible         =   0   'False
   End
   Begin VB.CommandButton Command2 
      Caption         =   "Imposta A4"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   264
      Left            =   252
      TabIndex        =   2
      Top             =   8352
      Width           =   1164
      Visible         =   0   'False
   End
   Begin VB.CommandButton Command1 
      Caption         =   "prova"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   408
      Left            =   1656
      TabIndex        =   1
      Top             =   8172
      Width           =   1452
      Visible         =   0   'False
   End
   Begin VB.PictureBox Prv 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ClipControls    =   0   'False
      ForeColor       =   &H80000008&
      Height          =   6240
      Left            =   72
      ScaleHeight     =   6240
      ScaleMode       =   0  'User
      ScaleWidth      =   4476
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   468
      Width           =   4476
   End
End
Attribute VB_Name = "frmPrintPreview"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Function DoTest() As Integer

    Static Dove As Integer
    Dove = ANTEPRIMA

    Prv.Cls
        
    PrintBox 1, 1, 20, 28.7, Dove
   
    PrintInLef 5, 5, "carattere: Arial", "Arial", 20, False, Dove
    
    PrintInLef 5, 10, "carattere: Britannic Bold", "Britannic Bold", 20, False, Dove

    PrintInLef 5, 15, "carattere: Castanet", "Castanet", 20, False, Dove

    PrintInLef 5, 20, "carattere: Ms Sans Serif", "Ms Sans Serif", 20, False, Dove
    
    PrintInLef 5, 21, "Ecco una prova di anteprima di stampa 'VIRTUALE'", "Ms Sans Serif", 20, False, Dove
    
End Function

Sub SetA4()

    Prv.Width = Prv.Height * (21 / 29.7)
    Prv.ScaleWidth = mm * 21
    Prv.ScaleHeight = mm * 29.7
    
    Scala = Prv.Height / Prv.ScaleHeight

End Sub

Sub MostraPagina()

        Prv.ScaleMode = SistemaCoordinate
        Prv.Width = (mm * 21) * (Val(zVal.Text) / 100) 'frmPrintPreview.ScaleWidth
        Prv.Height = (mm * 29.7) * (Val(zVal.Text) / 100) 'frmPrintPreview.ScaleHeight
        Prv.ScaleWidth = (mm * 21) 'frmPrintPreview.ScaleWidth
        Prv.ScaleHeight = (mm * 29.7) 'frmPrintPreview.ScaleHeight
        Prv.Refresh
        Scala = frmPrintPreview.Prv.Height / frmPrintPreview.Prv.ScaleHeight
        TempShow 0, 0, Prv.Width, Prv.Height

End Sub



Sub SistemaHBAR()
    Static X1 As Single, Y1 As Single
    Static X2 As Single, Y2 As Single
    TempShow hBar.Value, vBar.Value, hBar.Value + hBar.Width, vBar.Value + vBar.Height

End Sub

Sub SistemaVBAR()

    Static X1 As Single, Y1 As Single
    Static X2 As Single, Y2 As Single
    TempShow hBar.Value, vBar.Value, hBar.Value + hBar.Width, vBar.Value + vBar.Height

End Sub

Private Sub Cmd_Click(Index As Integer)

    Select Case Index
    
    Case 0 'refresh
        MostraPagina
    Case 1 'zoom in
        If zVal.ListIndex < zVal.ListCount - 1 Then
            zVal.ListIndex = zVal.ListIndex + 1
            MostraPagina
        End If
    Case 2 'zoom out
        If zVal.ListIndex > 0 Then
            zVal.ListIndex = zVal.ListIndex - 1
            MostraPagina
        End If
    Case 3 'sep
        frmPrint.Show
    Case 4 'setup prn
        Apre.Flags = cdlPDPrintSetup
        Apre.Action = &H5
    Case 5 'fst pg
        If ePag.ListIndex > 0 Then
            ePag.ListIndex = 0
            MostraPagina
        End If
    Case 6 'prv pg
        If ePag.ListIndex > 0 Then
            ePag.ListIndex = ePag.ListIndex - 1
            MostraPagina
        End If
    Case 7 'nxt pg
        If ePag.ListIndex < ePag.ListCount - 1 Then
            ePag.ListIndex = ePag.ListIndex + 1
            MostraPagina
        End If
    Case 8 'lst pg
        If ePag.ListIndex < ePag.ListCount - 1 Then
            ePag.ListIndex = ePag.ListCount - 1
            MostraPagina
        End If
    Case 9 'about
        frmPrintInfo.Show vbModal
    Case 10 'unload
        Unload Me
    End Select

End Sub


Private Sub Command1_Click()
    Static A%
    A% = DoTest()
End Sub

Private Sub Command2_Click()

    Prv.Cls

    Prv.Width = Prv.Height * (21 / 29.7)
    Prv.ScaleWidth = mm * 21
    Prv.ScaleHeight = mm * 29.7
    
    Scala = Prv.Height / Prv.ScaleHeight
    
End Sub


Private Sub Command3_Click()

    Prv.Cls

    Prv.Width = Prv.Height * (15 / 21)
    Prv.ScaleWidth = mm * 15
    Prv.ScaleHeight = mm * 21
    
    Scala = Prv.Height / Prv.ScaleHeight


End Sub

Private Sub Command4_Click()

    Prv.Cls

    Prv.Width = Prv.Height * (29.7 / 42)
    Prv.ScaleWidth = mm * 29.7
    Prv.ScaleHeight = mm * 42
    
    Scala = Prv.Height / Prv.ScaleHeight

End Sub




Private Sub Command6_Click()
    PrintRefGrid ANTEPRIMA
End Sub

Private Sub Form_Activate()
    MostraPagina
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    
    If KeyCode = vbKeyEscape Then
        Unload Me
    ElseIf KeyCode = vbKeyReturn Then
        MostraPagina
    ElseIf KeyCode = vbKeyHome Then
        vBar.Value = vBar.Min
    ElseIf KeyCode = vbKeyEnd Then
        vBar.Value = vBar.Max
    ElseIf KeyCode = vbKeyUp Then
        If vBar.Value - vBar.LargeChange < vBar.Min Then
            vBar.Value = vBar.Min
        Else
            vBar.Value = vBar.Value - vBar.LargeChange
        End If
    ElseIf KeyCode = vbKeyDown Then
        If vBar.Value + vBar.LargeChange > vBar.Max Then
            vBar.Value = vBar.Max
        Else
            vBar.Value = vBar.Value + vBar.LargeChange
        End If
    ElseIf KeyCode = vbKeyPageUp Then
        If ePag.ListIndex > 0 Then
            ePag.ListIndex = ePag.ListIndex - 1
            MostraPagina
        End If
    ElseIf KeyCode = vbKeyPageDown Then
        If ePag.ListIndex < ePag.ListCount - 1 Then
            ePag.ListIndex = ePag.ListIndex + 1
            MostraPagina
        End If
    ElseIf KeyCode = vbKeyRight Then
        If hBar.Value + hBar.LargeChange > hBar.Max Then
            hBar.Value = hBar.Max
        Else
            hBar.Value = hBar.Value + hBar.LargeChange
        End If
    ElseIf KeyCode = vbKeyLeft Then
        If hBar.Value - hBar.LargeChange < hBar.Min Then
            hBar.Value = hBar.Min
        Else
            hBar.Value = hBar.Value - hBar.LargeChange
        End If
    ElseIf KeyCode = vbKeyAdd Then
        If zVal.ListIndex < zVal.ListCount - 1 Then
            zVal.ListIndex = zVal.ListIndex + 1
            MostraPagina
        End If
    ElseIf KeyCode = vbKeySubtract Then
        If zVal.ListIndex > 0 Then
            zVal.ListIndex = zVal.ListIndex - 1
            MostraPagina
        End If
    End If

End Sub

Private Sub Form_Load()

    LocPerc = CurDir
    If Right(LocPerc, 1) <> "\" Then LocPerc = LocPerc + "\"

    frmPrintPreview.Height = Screen.Height * 0.9
    frmPrintPreview.Width = Screen.Width * 0.9
    Prv.Width = frmPrintPreview.ScaleWidth - (vBar.Width + (NM_PP_Ofs * 2))
    Prv.Height = frmPrintPreview.ScaleHeight - (hBar.Height + Cmd(0).Height + sBar.Height)
    
    Prv.ScaleMode = SistemaCoordinate
    Prv.Top = Cmd(0).Height
    Prv.Left = NM_PP_Ofs
    Prv.Width = frmPrintPreview.ScaleWidth - (vBar.Width + (NM_PP_Ofs * 2))
    Prv.Height = frmPrintPreview.ScaleHeight - (hBar.Height + Cmd(0).Height + sBar.Height)
    
    vBar.Top = Prv.Top
    vBar.Left = Prv.Left + Prv.Width
    vBar.Height = Prv.Height
    hBar.Left = Prv.Left
    hBar.Top = Prv.Top + Prv.Height
    hBar.Width = Prv.Width
    
    frmPrintPreview.Refresh
    frmPrintPreview.Left = (Screen.Width / 2) - (frmPrintPreview.Width / 2)
    frmPrintPreview.Top = (Screen.Height / 2) - (frmPrintPreview.Height / 2)
    
    zVal.ListIndex = 3
    
    Prv.Cls

    frmPrintPreview.Show vbModeless

End Sub

Private Sub Form_Resize()

  If frmPrintPreview.ScaleWidth > 1000 And frmPrintPreview.ScaleHeight > 1000 Then

    Prv.Top = Cmd(0).Height
    Prv.Left = NM_PP_Ofs
    'Prv.Width = frmPrintPreview.ScaleWidth - (vBar.Width + (NM_PP_Ofs * 2))
    'Prv.Height = frmPrintPreview.ScaleHeight - (hBar.Height + bBar.Height + sBar.Height)
    
    vBar.Top = Cmd(0).Height
    vBar.Left = frmPrintPreview.ScaleWidth - (vBar.Width + NM_PP_Ofs)
    vBar.Height = frmPrintPreview.ScaleHeight - (Cmd(0).Height + sBar.Height + hBar.Height)
    
    hBar.Top = frmPrintPreview.ScaleHeight - (sBar.Height + hBar.Height)
    hBar.Left = NM_PP_Ofs
    hBar.Width = frmPrintPreview.ScaleWidth - (vBar.Width + (NM_PP_Ofs * 2))
    
  End If
  
End Sub


Private Sub hBar_Change()
    SistemaHBAR
End Sub

Private Sub vBar_Change()
    SistemaVBAR
End Sub


