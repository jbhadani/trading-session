VERSION 5.00
Begin VB.Form frmReplace 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ers�tt/ta bort portf�ljobjekt"
   ClientHeight    =   4020
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3165
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4020
   ScaleWidth      =   3165
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Caption         =   "Objekt"
      Height          =   3135
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   3135
      Begin VB.ListBox List_Investments 
         Height          =   2595
         Left            =   120
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   4
         Top             =   360
         Width           =   2895
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   3240
      Width           =   3135
      Begin VB.CommandButton cbDelete 
         Caption         =   "Ta bort"
         Default         =   -1  'True
         Height          =   375
         Left            =   1680
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbChoose 
         Caption         =   "V�lj"
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmReplace"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbChoose_Click()
  ReplaceSem.str1 = List_Investments.List(List_Investments.ListIndex)
  ReplaceSem.int1 = 1 'Replace investment in portfolio
  ReplaceSem.wait = False
End Sub

Private Sub cbDelete_Click()
  ReplaceSem.int1 = 2 'Delete investment in portfolio
  ReplaceSem.wait = False
End Sub

Private Sub Form_Load()
  With frmReplace
  .Left = (MainMDI.Width - .Width) / 2
  .Top = (MainMDI.Height - .Height) / 4
  End With
  Dim i As Long
  For i = 1 To InvestInfoArraySize
    List_Investments.AddItem InvestInfoArray(i).Name
  Next i
End Sub

