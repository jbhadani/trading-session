Attribute VB_Name = "Optimize"


Public Sub Optimering()
  Dim Start1 As Long
  Dim Start2 As Long
  Dim Start3 As Long
  Dim Stop1 As Long
  Dim Stop2 As Long
  Dim Stop3 As Long
  Dim Inkrement As Long
  Dim PercentCurtage As Single
  Dim Mincourtage As Long
  Dim InvSum As Long
  Dim Courtage As Single
  Dim i As Long
  Dim OptimizeArray() As OptimizeType
  Dim Optimal As OptimizeType
  Dim Method As String
  Dim ParametersOK As Boolean
  Dim PDArray() As PriceDataType
  Dim Size As Long
  Dim A As Long
  Dim Svar As Boolean
  Dim BestResult As Single
  Dim BestResultActionNbr As Long
  Dim j As Long
  Dim Startdatum As Long
  Dim Slutdatum As Long
  Dim Continue As Boolean
  Dim Filename1 As String
  Dim nStartDate As Long
  Dim nStopDate As Long
  
  Filename1 = DataPath + Trim(InvestInfoArray(GetInvestIndex(OptimizeObject)).FileName) + ".prc"
  R_PriceDataArray Filename1, PDArray(), Size
   
  With frmOptimize
    Method = Trim(.cMethod.Text)
    Start1 = Val(.tbStartvarde1.Text)
    Start2 = Val(.tbStartvarde2.Text)
    Stop1 = Val(.tbSlutvarde1.Text)
    Stop2 = Val(.tbSlutvarde2.Text)
    Start3 = Val(.tbStartvarde3.Text)
    Stop3 = Val(.tbSlutvarde3.Text)
    Inkrement = Val(.tbSteglangd.Text)
    If .OptionDontUseCourtage Then
      Courtage = 0
    ElseIf .OptionMinCourtage Then
      Courtage = Val((.tbMinCourtage / .tbInvBelopp))
    Else
      Courtage = CSng(PointToComma(.tbCourtage)) / 100
    End If
  End With
  
  If Start1 > Stop1 Then
    MsgBox CheckLang("Slutv�rde 1 m�ste vara st�rre �n eller lika med startv�rde 1."), vbOKOnly, CheckLang("Felmeddelande")
    frmOptimize.tbSlutvarde1.SetFocus
    Exit Sub
  End If
  
  If Start2 > Stop2 Then
    MsgBox CheckLang("Slutv�rde 2 m�ste vara st�rre �n eller lika med startv�rde 2."), vbOKOnly, CheckLang("Felmeddelande")
    frmOptimize.tbSlutvarde2.SetFocus
    Exit Sub
  End If
  
  If Start3 > Stop3 Then
    MsgBox CheckLang("Slutv�rde 3 m�ste vara st�rre �n eller lika med startv�rde 3."), vbOKOnly, CheckLang("Felmeddelande")
    frmOptimize.tbSlutvarde1.SetFocus
    Exit Sub
  End If
  
  If Stop1 > Size Then
    Stop1 = Size
  End If
  
  If Stop2 > Size Then
    Stop2 = Size
  End If
  
  If StrComp(Trim(Method), "Stochastic oscillator") = 0 Then
    If Stop3 > Size Then
      Stop3 = Size
    End If
  End If
  
  nStartDate = CDate(frmOptimize.DTPicker1)
  nStopDate = CDate(frmOptimize.DTPicker2)
  
  If CDate(PDArray(1).Date) >= CDate(nStartDate) Then
    Startdatum = 1
  Else
    For i = 1 To Size
      If CDate(Int(PDArray(i).Date)) >= CDate(nStartDate) Then
        Startdatum = i
        i = Size
      End If
    Next i
  End If
  
  If Int(PDArray(PDArraySize).Date) <= CDate(nStopDate) Then
    Slutdatum = Size
  Else
    i = Size
    Do While i > 0
      If Int(PDArray(i).Date) <= CDate(nStopDate) Then
        Slutdatum = i
        i = 0
      End If
      i = i - 1
    Loop
  End If
  
  Select Case Method
     Case "Moving Average"
       OptimizeMAV Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Momentum"
       OptimizeMOM Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Price oscillator"
       OptimizeOSC Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Relative Strength Index"
       OptimizeRSI Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Stochastic Oscillator"
       OptimizeSTO Start1, Start2, Start3, Stop1, Stop2, Stop3, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Money Flow Index"
       OptimizeMFI Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Parabolic"
       OptimizeParabolic OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Ease Of Movement"
       OptimizeEOM Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "Rate-of-change"
       OptimizeROC Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
     Case "TRIX"
       OptimizeTRIX Start1, Start2, Stop1, Stop2, Inkrement, OptimizeArray(), PDArray(), Size, Courtage, Startdatum, Slutdatum
  End Select
  
  UpdateOptimizeGrid OptimizeArray
  
End Sub

Public Sub CopyToParameterSettings(Optimal As OptimizeType)
Dim Method As String
  Method = Trim(frmOptimize.cMethod.Text)
  With ParameterSettings
  Select Case Method
    
    Case "Ease Of Movement"
      ParameterSettings.EaseOfMovementLength = Optimal.Parameter1
      If .PlotEaseOfMovementAndMAV Then
        .EaseOfMovementMAVLength = Optimal.Parameter2
      End If
    
    Case "Momentum"
      .MOM = Optimal.Parameter1
      If .MOMSignalUseMAV Then
        .MAVMOM = Optimal.Parameter2
      End If
    
    Case "Money Flow Index"
      .MFILength = Optimal.Parameter1
      If .MFISignalUseMAV Then
        .MAVMFI = Optimal.Parameter2
      End If
    
    Case "Moving Average"
      .MAV1 = Optimal.Parameter1
      .MAV2 = Optimal.Parameter2
    
    Case "Parabolic"
      .AccelerationFactor = Optimal.sngParameter4
    
    Case "Price Ocillator"
      .PRIOSC1 = Optimal.Parameter1
      .PRIOSC2 = Optimal.Parameter2
      
    Case "Rate-of-change"
      .RateOfChangeLength = Optimal.Parameter1
      If .PlotRateOfChangeAndMAV Then
        .RateOfChangeMAVLength = Optimal.Parameter2
      End If
      
    Case "Relative Strength Index"
      .RSI = Optimal.Parameter1
      If .RSISignalUseMAV Then
        .MAVRSI = Optimal.Parameter2
      End If
      
    Case "Stochastic Oscillator"
      .STO = Optimal.Parameter1
      .MAVSTO = Optimal.Parameter3
      .STOSlowing = Optimal.Parameter2
     
    Case "TRIX"
      .TRIXLength = Optimal.Parameter1
      If .PlotTRIXAndMAV Then
        .TRIXMAVLength = Optimal.Parameter2
      End If
      
  End Select
  End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeMAV
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeMAV(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim MAV1() As Single
  Dim MAV2() As Single
  Dim buylast As Boolean
  Dim Last As Long
  Dim Result As Single
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim MAV1(Size)
  ReDim MAV2(Size)
  
  NbrOfActions = 0
  
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    Do While Length1 > Length2
      Length2 = Length2 + Inkrement
    Loop
    
    If ParameterSettings.MAVUseExp Then
      ExpMAVCalc PDArray(), MAV1(), Length1, Size
    Else
      SimpleMAVCalc PDArray(), MAV1(), Length1, Size, True
    End If
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
    
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
  
      If ParameterSettings.MAVUseExp Then
        ExpMAVCalc PDArray(), MAV2(), Length2, Size
      Else
        SimpleMAVCalc PDArray(), MAV2(), Length2, Size, True
      End If
        
      MAVSignalArray Signalarray, MAV1, MAV2, Size
        
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeMOM
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 R�ttat fel med restriktioner f�r parametrar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeMOM(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim Signalarray() As Long
  Dim MOM() As Single
  Dim MOMMAV() As Single
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim MOM(Size)
  ReDim MOMMAV(Size)
  ReDim Signalarray(Size)
  
  NbrOfActions = 0
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  NbrOfActions = 0
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    
    MomentumCalc PDArray, MOM, Length1, Size, True
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      ExpCalc MOM(), MOMMAV(), Length2, Size
      MomentumSignalArray Signalarray(), MOM(), MOMMAV(), Size
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeOSC
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeOSC(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim MAV1() As Single
  Dim MAV2() As Single
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim MAV1(Size)
  ReDim MAV2(Size)
  NbrOfActions = 0
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  NbrOfActions = 0
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    Do While Length2 < Length1
      Length2 = Length2 + Inkrement
    Loop
    
    If ParameterSettings.OSCMAVExp Then
      ExpMAVCalc PDArray, MAV1, Length1, Size
    Else
      SimpleMAVCalc PDArray, MAV1, Length1, Size, True
    End If
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      If ParameterSettings.OSCMAVExp Then
        ExpMAVCalc PDArray, MAV2, Length2, Size
      Else
        SimpleMAVCalc PDArray, MAV2, Length2, Size, True
      End If
              
      MAVSignalArray Signalarray, MAV1, MAV2, Size
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeRSI
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
'               980928 R�ttat fel med restriktioner f�r parametrar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeRSI(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim RSI() As Single
  Dim MAVRSI() As Single
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim RSI(Size)
  ReDim MAVRSI(Size)
  NbrOfActions = 0
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  NbrOfActions = 0
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
        
    RSICalc PDArray, RSI, Length1, Size, True
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      If ParameterSettings.RSISignalUseMAV Then
        ExpCalc RSI, MAVRSI, Length2, Size
      Else
        Length2 = Stop2
      End If
      
      RSISignalArray Signalarray, RSI, MAVRSI, Size
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      If ParameterSettings.RSISignalUseMAV Then
        UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      Else
        UpdateOptimizeArray Length1, 0, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      End If
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeSTO
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
'               980928 R�ttat fel med restriktioner f�r parametrar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeSTO(Start1 As Long, Start2 As Long, Start3 As Long, Stop1 As Long, Stop2 As Long, Stop3 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim Length3 As Long
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim STO() As Single
  Dim STOMAV() As Single
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim STO(Size)
  ReDim STOMAV(Size)
  NbrOfActions = 0
  If Stop1 > Size Or Stop2 > Size Or Stop3 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  Length3 = Start3
  
  buylast = False
  Last = 1
  NbrOfActions = 0
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1) * (((Stop3 - Start3) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
        
    Do While Length2 <= Stop2
      
      StochasticOscCalc PDArray, STO, Length1, Size, True, Length2
      
      Do While Length3 <= Stop3
        Loops = Loops + 1
        Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
        Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
        DoEvents
      
        TotalTime = 0
        Result = 1
        NbrOfTrades = 0
        NbrOfWinningTrades = 0
        NbrOfLosingTrades = 0
        BiggestGain = 0
        
        ExpCalc STO, STOMAV, Length3, Size
        
        STOSignalArray Signalarray, STO, STOMAV, Size
        
        CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
        
        Result = Result - 1
        NbrOfActions = NbrOfActions + 1
        
        UpdateOptimizeArray Length1, Length2, Length3, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
        
        Length3 = Length3 + Inkrement
      Loop
      Length3 = Start3
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeMFI
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
'               980928 R�ttat fel med restriktioner f�r parametrar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeMFI(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim MFI() As Single
  Dim MFIMAV() As Single
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim MFI(Size)
  ReDim MFIMAV(Size)
  
  NbrOfActions = 0
  
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast ") & Size & CheckLang(" kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  NbrOfActions = 0
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
  
    MFICalc PDArray, MFI, Length1, Size, True
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      ExpCalc MFI(), MFIMAV(), Length2, Size
      
      MFISignalArray Signalarray(), MFI, MFIMAV(), Size
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeParabolic
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till multiplikativ resultatber�kning
'               980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeParabolic(OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim buylast As Boolean
  Dim Result As Single
  Dim Last As Long
  Dim Parabolic() As Single
  Dim ParabolicSignal() As Boolean
  Dim i As Long
  Dim OldAccelerationFactor As Long
  Dim Signalarray() As Long
  Dim AF As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  
  ReDim Signalarray(Size)
  ReDim Parabolic(Size)
  ReDim ParabolicSignal(Size)
  
  NbrOfActions = 0
  
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  For i = 2 To 20
    
    Frm_Progress.PB_1.Value = Int(i / 20 * 100)
    Frm_Progress.Label1.Caption = Str$(Int(i / 20 * 100)) + " % �r klart."
    DoEvents
    
    Result = 1
    TotalTime = 0
    NbrOfTrades = 0
    NbrOfWinningTrades = 0
    NbrOfLosingTrades = 0
    BiggestGain = 0
  
    ParabolicCalc PDArray(), Parabolic(), ParabolicSignal(), Size, True, i / 100
    
    ParabolicSignalArray Signalarray, ParabolicSignal, Size
        
    CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
        
    Result = Result - 1
    NbrOfActions = NbrOfActions + 1
    
    UpdateOptimizeArray i, 0, 0, Result, OptimizeArray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
           
  Next i
  
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

Public Sub UpdateOptimizeArray(Length1 As Long, Length2 As Long, Length3 As Long, Result As Single, OptimizeArray() As OptimizeType, NbrOfWinningTrades As Long, NbrOfLosingTrades As Long, NbrOfTrades As Long, BiggestGain As Single)
  ReDim Preserve OptimizeArray(NbrOfActions)
  OptimizeArray(NbrOfActions).Parameter1 = Length1
  OptimizeArray(NbrOfActions).Parameter2 = Length2
  OptimizeArray(NbrOfActions).Parameter3 = Length3
  OptimizeArray(NbrOfActions).NbrOfTrades = NbrOfTrades
  OptimizeArray(NbrOfActions).NbrOfWinningTrades = NbrOfWinningTrades
  OptimizeArray(NbrOfActions).NbrOfLosingTrades = NbrOfLosingTrades
  OptimizeArray(NbrOfActions).BiggestGain = BiggestGain
  OptimizeArray(NbrOfActions).Result = Result
End Sub
Public Sub ControlParameters(Method As String, Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OK As Boolean)
  OK = True
  Select Case Method
    Case "Moving Average"
      If Start1 >= Start2 Then
        MsgBox CheckLang("Startv�rde 1 m�ste vara mindre �n startv�rde 2."), 48, CheckLang("Felmeddelande")
        OK = False
      ElseIf Stop1 > Stop2 Then
        MsgBox CheckLang("Slutv�rde 1 m�ste vara mindre �n slutv�rde 2."), 48, CheckLang("Felmeddelande")
        OK = False
      End If
    Case "Price Oscillator"
      If Start1 >= Start2 Then
        MsgBox CheckLang("Startv�rde 1 m�ste vara mindre �n startv�rde 2."), 48, CheckLang("Felmeddelande")
        OK = False
      ElseIf Stop1 > Stop2 Then
        MsgBox CheckLang("Slutv�rde 1 m�ste vara mindre �n slutv�rde 2."), 48, CheckLang("Felmeddelande")
        OK = False
      End If
  End Select
End Sub

Sub UpdateOptimizeGrid(OptimizeArray() As OptimizeType)
Dim i As Integer
Dim Size As Long
Dim Method As String
  
  Method = Trim(frmOptimize.cMethod.Text)
  MainMDI.MousePointer = 11
  With frmOptimizeResults.MSFlexGrid1
    .Redraw = False
    .Rows = NbrOfActions + 1
    On Error Resume Next
      Size = UBound(OptimizeArray)
      If Err.Number <> 0 Then
        MsgBox CheckLang("De inst�llningar du valt genererade inga aff�rer. D�rf�r finns inget optimeringsresultat."), vbOKOnly, CheckLang("Meddelande")
        Exit Sub
      Else
      For i = 1 To UBound(OptimizeArray)
      .Row = i
      .Col = 0
      .CellAlignment = flexAlignCenterCenter
      .Text = Str$(i)
      .Col = 1
      .CellAlignment = flexAlignCenterCenter
      If StrComp(Method, "Parabolic") = 0 Then
        .Text = (OptimizeArray(i).Parameter1 / 100)
      Else
        .Text = OptimizeArray(i).Parameter1
      End If
      .Col = 2
      .CellAlignment = flexAlignCenterCenter
      .Text = OptimizeArray(i).Parameter2
      .Col = 3
      .CellAlignment = flexAlignCenterCenter
      .Text = OptimizeArray(i).Parameter3
      .Col = 4
      .CellAlignment = flexAlignCenterCenter
      .Text = OptimizeArray(i).NbrOfTrades
      .Col = 5
      .CellAlignment = flexAlignCenterCenter
      .Text = OptimizeArray(i).NbrOfWinningTrades
      .Col = 6
      .CellAlignment = flexAlignCenterCenter
      .Text = OptimizeArray(i).NbrOfLosingTrades
      .Col = 7
      .CellAlignment = flexAlignCenterCenter
      .Text = FormatNumber(OptimizeArray(i).BiggestGain * 100, 1)
      .Col = 8
      .CellAlignment = flexAlignCenterCenter
      .Text = FormatNumber(OptimizeArray(i).Result * 100, 1)
      Next i
      End If
    On Error GoTo 0
    
    .Col = 8
    .Sort = 4
    .Redraw = True
    .Col = 0
    .Row = 1
  End With
  MainMDI.MousePointer = 1
  frmOptimizeResults.Show
  DoEvents
  frmOptimizeResults.MSFlexGrid1.SetFocus
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeEOM
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeEOM(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim EOM() As Single
  Dim EOMMAV() As Single
  Dim buylast As Boolean
  Dim Last As Long
  Dim Result As Single
  Dim TotalTime As Long
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim EOM(Size)
  ReDim EOMMAV(Size)
  
  NbrOfActions = 0
    
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast " & Size & " kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    
    EaseOfMovementCalc PDArray(), EOM(), Length1, Size, True
    
    Do While Length2 <= Stop2
      
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      If ParameterSettings.PlotEaseOfMovementAndMAV Then
        SimpleCalc EOM, EOMMAV, Length2, Size, True
        EaseOfMovementSignalArray Signalarray(), EOMMAV(), Size
      Else
        EaseOfMovementSignalArray Signalarray(), EOM(), Size
        Length2 = Stop2
      End If
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
      
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      If ParameterSettings.PlotEaseOfMovementAndMAV Then
        UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      Else
        UpdateOptimizeArray Length1, 0, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      End If
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeROC
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeROC(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim ROC() As Single
  Dim ROCMAV() As Single
  Dim buylast As Boolean
  Dim Last As Long
  Dim Result As Single
  Dim TotalTime As Long
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim ROC(Size)
  ReDim ROCMAV(Size)
  
  NbrOfActions = 0
  
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast " & Size & " kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    
    RateOfChangeCalc PDArray(), ROC(), Length1, Size, True
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      If ParameterSettings.PlotRateOfChangeAndMAV Then
        SimpleCalc ROC, ROCMAV, Length2, Size, True
        EaseOfMovementSignalArray Signalarray(), ROCMAV(), Size
      Else
        EaseOfMovementSignalArray Signalarray(), ROC(), Size
        Length2 = Stop2
      End If
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
     
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      If ParameterSettings.PlotRateOfChangeAndMAV Then
        UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      Else
        UpdateOptimizeArray Length1, 0, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      End If
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          OptimizeTRIX
'Input:         StartV�rde1, StartV�rde2, Slutv�rde1,Slutv�rde2,
'               Inkrementsats, PDArray, PDArraysize,
'               Courtage, Startdatum, Slutdatum
'Output:        OptimizeArray()
'
'Description:   Calculates the result of the indicator for all
'               possible combinations of parameters.
'Author:        Martin Lindberg
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub OptimizeTRIX(Start1 As Long, Start2 As Long, Stop1 As Long, Stop2 As Long, Inkrement As Long, OptimizeArray() As OptimizeType, PDArray() As PriceDataType, Size As Long, Courtage As Single, Startdatum As Long, Slutdatum As Long)
  Dim Length1 As Long
  Dim Length2 As Long
  Dim TRIX() As Single
  Dim TRIXMAV() As Single
  Dim buylast As Boolean
  Dim Last As Long
  Dim Result As Single
  Dim TotalTime As Long
  Dim Signalarray() As Long
  Dim NbrOfTrades As Long
  Dim NbrOfWinningTrades As Long
  Dim NbrOfLosingTrades As Long
  Dim BiggestGain As Single
  Dim Thisgain As Single
  Dim NbrOfLoops As Long
  Dim Loops As Long
  
  ReDim Signalarray(Size)
  ReDim TRIX(Size)
  ReDim TRIXMAV(Size)
  
  NbrOfActions = 0
  
  If Stop1 > Size Or Stop2 > Size Then
    MsgBox CheckLang("Det finns endast " & Size & " kurser lagrade. N�got av slutv�rdena �verstiger detta antal."), 48, CheckLang("Felmeddelande")
    Exit Sub
  End If
  
  Length1 = Start1
  Length2 = Start2
  buylast = False
  Last = 1
  
  If Startdatum <= 2 Then
    Startdatum = 2
  End If
  
  NbrOfLoops = (((Stop1 - Start1) \ Inkrement) + 1) * (((Stop2 - Start2) \ Inkrement) + 1)
  Loops = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Optimerar..."
  End With
  DoEvents
  
  Do While Length1 <= Stop1
    
    TRIXCalc PDArray(), TRIX(), Length1, Size, True
    
    Do While Length2 <= Stop2
      Loops = Loops + 1
      Frm_Progress.PB_1.Value = Int(Loops / NbrOfLoops * 100)
      Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfLoops * 100)) + " % �r klart."
      DoEvents
      
      TotalTime = 0
      Result = 1
      NbrOfTrades = 0
      NbrOfWinningTrades = 0
      NbrOfLosingTrades = 0
      BiggestGain = 0
      
      SimpleCalc TRIX, TRIXMAV, Length2, Size, True
      TRIXSignalArray Signalarray(), TRIX(), TRIXMAV(), Size
      
      CalcResult Startdatum, Slutdatum, Signalarray, NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain, Result, PDArray(), Courtage
            
      Result = Result - 1
      NbrOfActions = NbrOfActions + 1
      
      If ParameterSettings.PlotTRIXAndMAV Then
        UpdateOptimizeArray Length1, Length2, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      Else
        UpdateOptimizeArray Length1, 0, 0, Result, OptimizeArray(), NbrOfWinningTrades, NbrOfLosingTrades, NbrOfTrades, BiggestGain
      End If
      
      Length2 = Length2 + Inkrement
    Loop
    Length2 = Start2
    Length1 = Length1 + Inkrement
  Loop
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  DoEvents
End Sub

Sub CalcResult(Startdatum As Long, Slutdatum As Long, Signalarray() As Long, NbrOfWinningTrades As Long, NbrOfLosingTrades As Long, NbrOfTrades As Long, BiggestGain As Single, Result As Single, PDArray() As PriceDataType, Courtage As Single)
Dim buylast As Boolean
Dim Thisgain As Single
Dim i As Integer
Dim PortfolioValue As Single
Dim Mincourtage As Long

With frmOptimize
If .OptionMinCourtage Then
  PortfolioValue = .tbInvBelopp
  Mincourtage = .tbMinCourtage
End If

For i = Startdatum To Slutdatum
  If i <> 2 And i = Startdatum Then
    If Signalarray(i) = 1 Then
      buylast = True
      Last = i
    End If
  Else
    If ((Signalarray(i) = 1 And Signalarray(i - 1) = -1) Or (Signalarray(i) = 1 And Signalarray(i - 1) = 0)) And Not buylast Then
      buylast = True
      Last = i
    ElseIf Signalarray(i) = -1 And Signalarray(i - 1) = 1 And buylast Then
      buylast = False
      NbrOfTrades = NbrOfTrades + 1
      Thisgain = ((((PDArray(i).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - 2 * Courtage) - 1
      If Thisgain > 0 Then
        NbrOfWinningTrades = NbrOfWinningTrades + 1
      ElseIf Thisgain < 0 Then
        NbrOfLosingTrades = NbrOfLosingTrades + 1
      End If
      If Thisgain > BiggestGain Then
        BiggestGain = Thisgain
      End If
      If .OptionMinCourtage Then
        Courtage = Mincourtage / PortfolioValue + Mincourtage / (PortfolioValue * (1 + Thisgain))
      End If
      PortfolioValue = PortfolioValue * (1 + Thisgain)
      
      If .OptionMinCourtage Then
        Result = Result * ((((PDArray(i).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - Courtage)
      Else
        Result = Result * ((((PDArray(i).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - Courtage * 2)
      End If
      
    End If
    If buylast Then
      TotalTime = TotalTime + 1
    End If
  End If
Next i
If buylast Then
  NbrOfTrades = NbrOfTrades + 1
  Thisgain = ((((PDArray(i - 1).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - 2 * Courtage) - 1
  If Thisgain > 0 Then
    NbrOfWinningTrades = NbrOfWinningTrades + 1
  ElseIf Thisgain < 0 Then
    NbrOfLosingTrades = NbrOfLosingTrades + 1
  End If
  If Thisgain > BiggestGain Then
    BiggestGain = Thisgain
  End If
  If .OptionMinCourtage Then
    Courtage = Mincourtage / PortfolioValue + Mincourtage / (PortfolioValue * (1 + Thisgain))
  End If
  PortfolioValue = PortfolioValue * (1 + Thisgain)
  If .OptionMinCourtage Then
    Result = Result * ((((PDArray(i - 1).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - Courtage)
  Else
    Result = Result * ((((PDArray(i - 1).Close - PDArray(Last).Close) / PDArray(Last).Close) + 1) - Courtage * 2)
  End If
  buylast = False
End If
End With
End Sub
