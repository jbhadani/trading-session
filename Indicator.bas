Attribute VB_Name = "Indicator"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  ExpCalc
'Input:         An empty array and the period and a valuearray.
'Output:        An array that contains the MAV.
'Description:   This procedure calculates a moving average
'               of the input array.
'Written:       970414  by Martin Lindberg
'Modified:      980929  A bug was removed (FW)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub ExpCalc(ByRef inMAV() As Single, outMAV() As Single, ByVal Length As Long, Size As Long)
  Dim I
  Dim Percent As Single
  Percent = 2 / (Length + 1)
  
  outMAV(1) = inMAV(1)
  For I = 2 To Size
    outMAV(I) = outMAV(I - 1) + (Percent * (inMAV(I) - outMAV(I - 1)))
    'outMAV(i) = (inMAV(i) * ExpPercent / 100) + (outMAV(i - 1) * (1 - ExpPercent / 100))
  Next I
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  ExpMAVCalc
'Input:         An empty array an the period.
'Output:        An array that contains the MAV.
'Description:   This procedure calculates a moving average
'               of the input array.
'Written:       970327 by Martin Lindberg
'Modified:      970328 by Martin Lindberg
'               980929  A bug was removed (FW)
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub ExpMAVCalc(PriceArray() As PriceDataType, ByRef MAV() As Single, ByVal Length As Long, Size As Long)

  Dim I As Long
  Dim Percent As Single
  Percent = 2 / (Length + 1)
  MAV(1) = PriceArray(1).Close
  For I = 2 To Size
    MAV(I) = MAV(I - 1) + (Percent * (PriceArray(I).Close - MAV(I - 1)))
  Next I
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  MACDCalc
'Input:         An empty array.
'Output:        MACD valuearray.
'Description:   This procedure calculates the MACD indicator
'Written:       970414 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MACDCalc(PriceArray() As PriceDataType, MACDArray() As Single, MACDSignal() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
  Dim I As Long
  ReDim MAV26(Size) As Single
  ReDim MAV12(Size) As Single
  ReDim MACDArray(Size) As Single
  ReDim MACDSignal(Size) As Single
  
  If 26 > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna MACD"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  ExpMAVCalc PriceArray(), MAV26(), 26, Size
  ExpMAVCalc PriceArray(), MAV12(), 12, Size
  For I = 1 To Size
     MACDArray(I) = MAV12(I) - MAV26(I)
  Next I
  ExpCalc MACDArray(), MACDSignal(), 9, Size
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  MomentumCalc
'Input:         An empty array.
'Output:        An array that contains the Momentum values.
'Description:   This procedure calculates the indicator
'               Momentum.
'Written:       970406 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MomentumCalc(PriceArray() As PriceDataType, ByRef MomentumArray() As Single, ByVal Length As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  Dim I As Long
  
  If Length >= Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Momentum"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
    For I = 1 To Length + 1
      MomentumArray(I) = 100
    Next I
    For I = Length + 1 To Size
      If PriceArray(I - Length).Close <> 0 Then
        MomentumArray(I) = (PriceArray(I).Close / PriceArray(I - Length).Close) * 100
      Else
        MomentumArray(I) = MomentumArray(I - 1)
      End If
    Next I
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  MFICalc
'Input:         An empty array.
'Output:        An array that contains the MFI values.
'Description:   This procedure calculates the indicator
'               Money Flow Index.
'Written:       970411 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MFICalc(PriceArray() As PriceDataType, MFIArray() As Single, Length As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  Dim I As Long
  Dim j As Long
  Dim TypicalPrice() As Single
  Dim MoneyFlow() As Double
  Dim PositiveMoneyFlow As Double
  Dim NegativeMoneyFlow As Double
  Dim MoneyRatio As Double
  ReDim TypicalPrice(Size) As Single
  ReDim MoneyFlow(Size) As Double
  ReDim MFIIArray(Size) As Double
  
  If Length > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna MFI"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  PositiveMoneyFlow = 0
  NegativeMoneyFlow = 0.0001
  TypicalPriceCalc PriceArray(), TypicalPrice(), Size, True
  For I = 1 To Size
    MoneyFlow(I) = TypicalPrice(I) * PriceArray(I).Volume
  Next I
  For I = 1 To Length - 1
    MFIArray(I) = 50
  Next I
  For I = 2 To Length
    If TypicalPrice(I) > TypicalPrice(I - 1) Then
      PositiveMoneyFlow = PositiveMoneyFlow + MoneyFlow(I)
    ElseIf TypicalPrice(I) < TypicalPrice(I - 1) Then
      NegativeMoneyFlow = NegativeMoneyFlow + MoneyFlow(I)
    End If
  Next I
  If NegativeMoneyFlow <> 0 Then
    MoneyRatio = PositiveMoneyFlow / NegativeMoneyFlow
  Else
    MoneyRatio = PositiveMoneyFlow
  End If
  MFIArray(Length) = 100 - (100 / (1 + MoneyRatio))
  j = Length + 1
  Do Until j > Size
    If TypicalPrice(j) > TypicalPrice(j - 1) Then
      PositiveMoneyFlow = PositiveMoneyFlow + MoneyFlow(j)
    ElseIf TypicalPrice(j) < TypicalPrice(j - 1) Then
      NegativeMoneyFlow = NegativeMoneyFlow + MoneyFlow(j)
    End If
    If NegativeMoneyFlow <> 0 Then
      MoneyRatio = PositiveMoneyFlow / NegativeMoneyFlow
    Else
      MoneyRatio = PositiveMoneyFlow
    End If
    MFIArray(j) = 100 - (100 / (1 + MoneyRatio))
    If TypicalPrice(j - Length + 1) > TypicalPrice(j - Length) Then
      PositiveMoneyFlow = PositiveMoneyFlow - MoneyFlow(j - Length + 1)
    ElseIf TypicalPrice(j - Length + 1) < TypicalPrice(j - Length) Then
      NegativeMoneyFlow = NegativeMoneyFlow - MoneyFlow(j - Length + 1)
    End If
    j = j + 1
  Loop
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  RSICalc
'Input:         An empty array.
'Output:        An array that contains the RSI values.
'Description:   This procedure calculates the indicator
'               RSI.
'Written:       970331 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RSICalc(PriceArray() As PriceDataType, ByRef RSIArray() As Single, Length As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  Dim I
  Dim P As Single
  Dim N As Single
  Dim PS As Single
  Dim NS As Single
  Dim Pc As Single
  Dim NC As Single
  Dim Pp As Single
  Dim Np As Single
  
  Pp = 0
  Np = 0
  P = 0
  N = 0
  PS = 0
  NS = 0
  Pc = 0
  NC = 0
  
  If Length > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna RSI"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
 
  For I = 2 To Size
    If PriceArray(I).Close > PriceArray(I - 1).Close Then
      Pc = PriceArray(I).Close - PriceArray(I - 1).Close
      NC = 0
    ElseIf PriceArray(I).Close < PriceArray(I - 1).Close Then
      NC = PriceArray(I - 1).Close - PriceArray(I).Close
      Pc = 0
    End If
    PS = Pc + (Pp * (Length - 1))
    P = PS / Length
    NS = NC + (Np * (Length - 1))
    N = NS / Length
    Pp = P
    Np = N
    If I > Length And N <> 0 Then
      RSIArray(I) = (100 * P / N) / (1 + P / N)
    End If
  Next I
  
  For I = 1 To Length
    RSIArray(I) = 50
  Next I
      
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  SimpleMAVCalc
'Input:         An empty array an the period.
'Output:        An array that contains the MAV.
'Description:   This procedure calculates a moving average
'               of the input array.
'Written:       970327 by Martin Lindberg
'Modified:      970328 by Martin Lindberg
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SimpleMAVCalc(PriceArray() As PriceDataType, ByRef MAV() As Single, ByVal Length As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  
  Dim I, N, j As Long
  Dim Sum As Single
  ReDim MAV(Size) As Single
  
  If Length > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna medelv�rdet"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  Sum = 0
  For k = 1 To Length
    Sum = Sum + PriceArray(k).Close
    MAV(k) = Sum / k
  Next k
  I = 1
  Do Until I = Size - Length + 1
    Sum = Sum + PriceArray(Length + I).Close - PriceArray(I).Close
    MAV(Length + I) = Sum / Length
    I = I + 1
  Loop
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  StochasticOsc
'Input:         An empty array.
'Output:        An array that contains the Stochastic values.
'Description:   This procedure calculates the indicator
'               Stochastic Oscillator. The result is put
'               in two different arrays. The second array
'               is a complement two the other. The use of
'               this array is set by the user.
'Written:       970315 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub StochasticOscCalc(PriceArray() As PriceDataType, StochasticOscArrayK() As Single, Kper As Long, Size As Long, SearchOptimizeOrTest As Boolean, STOSlowing As Long)

  ReDim StochasticOscArrayK(Size) As Single
  
  Dim I, j As Long
  Dim LowestLow, HighestHigh As Single
  Dim StochasticOscArrayKf() As Single
  ReDim StochasticOscArrayKf(Size)
  
  If Kper > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Stochastic"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  
    For j = 1 To Kper - 1
      StochasticOscArrayKf(j) = 50
    Next j
    I = Kper
    Do Until I > Size
      LowestLow = 32000
      HighestHigh = 0
      For j = 0 To Kper - 1
        If PriceArray(I - j).Low < LowestLow Then
          LowestLow = PriceArray(I - j).Low
        End If
        If PriceArray(I - j).High > HighestHigh Then
          HighestHigh = PriceArray(I - j).High
        End If
      Next j
      If (HighestHigh - LowestLow) <> 0 Then
        StochasticOscArrayKf(I) = ((PriceArray(I).Close - LowestLow) / (HighestHigh - LowestLow)) * 100
      Else
        StochasticOscArrayKf(I) = StochasticOscArrayK(I - 1)
      End If
      I = I + 1
    Loop
    
    ExpCalc StochasticOscArrayKf(), StochasticOscArrayK(), STOSlowing, Size
       
End Sub

#If 0 Then
Public Sub CoppockCalc(PDArray() As PriceDataType, COP() As Single, COPMAVLength As Long, COPDist As Long, COPSUM As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  Dim I As Long
  Dim j As Long
  ReDim COP(Size)
  Dim COPMAV() As Single
  ReDim COPMAV(Size)
  Dim temp() As Single
  ReDim temp(Size)
  If COPMAVLength > Size Or COPDist > Size - 1 Or (COPSUM + COPDist) > Size - 1 Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Coppock"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  ExpMAVCalc PDArray, COPMAV, COPMAVLength, Size
  
  For I = COPDist + 1 To Size
    If COPMAV(I - COPDist + 1) <> 0 Then
      temp(I) = COPMAV(I) / COPMAV(I - (COPDist - 1))
    Else
      temp(I) = temp(I - 1)
    End If
  Next I
  For I = COPDist + COPSUM To Size
    For j = 1 To COPSUM
      COP(I) = COP(I) + (COPSUM - j + 1) * temp(I - (j - 1))
    Next j
  Next I
  For I = 1 To COPDist + COPSUM - 1
    COP(I) = COP(COPDist + COPSUM)
  Next I
End Sub
#End If

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ParabolicCalc
'Input:
'Output:
'
'Description:   Calculates Parabolic
'Author:        Martin Lindberg
'Revision:      98XXXX  Created
'               980928  �ndrat s� att acc.factor �r input till
'                       funktionen.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub ParabolicCalc(PDArray() As PriceDataType, Parabolic() As Single, ParabolicBuyOrSell() As Boolean, Size As Long, SearchOptimizeOrTest As Boolean, AccelerationFactor As Single)
   Dim I As Long
   Dim temp As Single
   Dim Short As Boolean
   Dim NewBuyLong As Boolean
   Dim NewSellShort As Boolean
   Dim HighestHighInPosition As Single
   Dim LowestLowInPosition As Single
   Dim AF As Single
   Dim StartAccelerationFactor As Single
   ReDim Parabolic(Size)
   ReDim ParabolicBuyOrSell(Size)
   
   StartAccelerationFactor = 0.02
   NewBuyLong = False
   NewSellShort = False
   Parabolic(1) = PDArray(1).Low
   ParabolicBuyOrSell(1) = True
   HighestHighInPosition = PDArray(1).High
   Short = False
   I = 2
   AF = StartAccelerationFactor
   
   Do While I <= Size
     If Not Short Then
       temp = Parabolic(I - 1) + AF * (HighestHighInPosition - Parabolic(I - 1))
       If temp >= PDArray(I).Low Then
         NewSellShort = True
         Short = True
         Parabolic(I) = HighestHighInPosition
         ParabolicBuyOrSell(I) = False
       Else
         ParabolicBuyOrSell(I) = True
         If temp > PDArray(I - 1).Low And temp > PDArray(I - 2).Low Then
           If PDArray(I - 1).Low > PDArray(I - 2).Low And I - 2 <> 0 Then
             Parabolic(I) = PDArray(I - 2).Low
            Else
             Parabolic(I) = PDArray(I - 1).Low
           End If
         Else
           Parabolic(I) = temp
         End If
       End If
       If PDArray(I).High > HighestHighInPosition Then
         HighestHighInPosition = PDArray(I).High
         AF = AF + AccelerationFactor
         If AF > 0.2 Then
           AF = 0.2
         End If
       End If
     Else
       temp = Parabolic(I - 1) + AF * (LowestLowInPosition - Parabolic(I - 1))
       If temp <= PDArray(I).High Then
         NewBuyLong = True
         Short = False
         Parabolic(I) = LowestLowInPosition
         ParabolicBuyOrSell(I) = True
       Else
         ParabolicBuyOrSell(I) = False
         If temp < PDArray(I - 1).High And temp < PDArray(I - 2).High Then
           If PDArray(I - 1).High > PDArray(I - 2).High Then
             Parabolic(I) = PDArray(I - 2).High
           Else
             Parabolic(I) = PDArray(I - 1).High
           End If
         Else
           Parabolic(I) = temp
         End If
       End If
       If PDArray(I).Low < LowestLowInPosition Then
         LowestLowInPosition = PDArray(I).Low
         AF = AF + AccelerationFactor
         If AF > 0.2 Then
           AF = 0.2
         End If
       End If
     End If
     
     If NewBuyLong Then
       HighestHighInPosition = PDArray(I).High
       AF = StartAccelerationFactor
       NewBuyLong = False
     End If
     If NewSellShort Then
       LowestLowInPosition = PDArray(I).Low
       AF = StartAccelerationFactor
       NewSellShort = False
     End If
     I = I + 1
   Loop
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          BollingerBandCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub BollingerBandCalc(PDArray() As PriceDataType, BollingerUpper() As Single, BollingerLower() As Single, MAV() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
 Dim I As Long
 Dim j As Long
 Dim Sum As Single
 Dim MAVLength As Long
 Dim STD() As Single

 ReDim BollingerUpper(Size)
 ReDim BollingerLower(Size)
 ReDim MAV(Size)
 ReDim STD(Size)
 
 MAVLength = ParameterSettings.BollingerMAVLength
 If MAVLength > Size Then
   If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
 
 SimpleMAVCalc PDArray(), MAV(), MAVLength, Size, False
 
 For I = 1 To MAVLength
   STD(I) = MAV(I)
   BollingerUpper(I) = STD(I)
   BollingerLower(I) = STD(I)
 Next I
 
 I = MAVLength
 Do While I <= Size
   Sum = 0
   For j = 1 To MAVLength
     Sum = Sum + ((PDArray(I - MAVLength + j).Close - MAV(I)) ^ 2)
   Next j
   STD(I) = Sqr((Sum / MAVLength))
   BollingerUpper(I) = MAV(I) + ParameterSettings.BollingerSTDFactor * STD(I)
   BollingerLower(I) = MAV(I) - ParameterSettings.BollingerSTDFactor * STD(I)
   I = I + 1
 Loop
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          AccDistCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub AccDistCalc(PDArray() As PriceDataType, AccDistArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim AccDistArray(Size)

If Size < 2 Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

AccDistArray(1) = 0
For I = 2 To Size
  With PDArray(I)
  If .High <> .Low Then
    AccDistArray(I) = AccDistArray(I - 1) + (((.Close - .Low) - (.High - .Close)) / (.High - .Low)) * .Volume
  Else
    AccDistArray(I) = AccDistArray(I - 1)
  End If
  End With
Next I
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ATRCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ATRCalc(PDArray() As PriceDataType, ATRArray() As Single, MAVLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim j As Long
Dim TrueRange() As Single
Dim temp As Single
ReDim TrueRange(Size)
ReDim ATRArray(Size)

If Size <= MAVLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

TrueRange(1) = 0
For I = 2 To Size
  If (PDArray(I).High - PDArray(I).Low) > Abs((PDArray(I - 1).Close - PDArray(I - 1).High)) And (PDArray(I).High - PDArray(I).Low) > (PDArray(I - 1).Close - PDArray(I - 1).Low) Then
    TrueRange(I) = (PDArray(I).High - PDArray(I).Low)
  ElseIf Abs((PDArray(I - 1).Close - PDArray(I - 1).High)) > (PDArray(I - 1).Close - PDArray(I - 1).Low) Then
    TrueRange(I) = Abs((PDArray(I - 1).Close - PDArray(I - 1).High))
  Else
    TrueRange(I) = (PDArray(I - 1).Close - PDArray(I - 1).Low)
  End If
Next I
For I = 1 To MAVLength
  ATRArray(I) = TrueRange(I)
Next I
For I = MAVLength To Size
  temp = 0
  For j = 0 To MAVLength - 1
    temp = TrueRange(I - j) + temp
  Next j
  ATRArray(I) = temp / MAVLength
Next I
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ChaikinOscCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ChaikinOscCalc(PDArray() As PriceDataType, ChaikinOscArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim AccDistArray() As Single
Dim Temp1() As Single
Dim Temp2() As Single
ReDim AccDistArray(Size)
ReDim Temp1(Size)
ReDim Temp2(Size)

If Size < 10 Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

AccDistCalc PDArray(), AccDistArray(), Size, False

ExpCalc AccDistArray(), Temp1(), 10, Size
ExpCalc AccDistArray(), Temp2(), 3, Size

For I = 1 To Size
  ChaikinOscArray(I) = Temp2(I) - Temp1(I)
Next I

End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          DPOCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub DPOCalc(PDArray() As PriceDataType, DPOArray() As Single, DPOLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim k As Long
Dim MAV() As Single
ReDim DPOArray(Size)
ReDim MAV(Size)

If Size < DPOLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

SimpleMAVCalc PDArray(), MAV(), DPOLength, Size, False

k = Int((DPOLength / 2) + 1)

For I = k + 1 To Size
  DPOArray(I) = PDArray(I).Close - MAV(I - k)
Next I

For I = 1 To k
 DPOArray(I) = PDArray(I).Close - MAV(I)
Next I
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          EaseOfMovementCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118 Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub EaseOfMovementCalc(PDArray() As PriceDataType, EOMArray() As Single, MAVLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim MidPointMove() As Single
Dim BoxRatio() As Single
Dim TempEMV() As Single
ReDim EOMArray(Size)
ReDim MidPointMove(1 To Size)
ReDim BoxRatio(1 To Size)
ReDim TempEMV(Size)

If Size < MAVLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If


For I = 2 To Size
  If PDArray(I).High <> PDArray(I).Low Then
    BoxRatio(I) = (PDArray(I).Volume) / (PDArray(I).High - PDArray(I).Low)
  Else
    BoxRatio(I) = BoxRatio(I - 1)
  End If
  MidPointMove(I) = (((PDArray(I).High - PDArray(I).Low) / 2) - ((PDArray(I - 1).High - PDArray(I - 1).Low) / 2))
  If BoxRatio(I) <> 0 Then
    TempEMV(I) = MidPointMove(I) / BoxRatio(I)
  End If
Next I

TempEMV(1) = 0

SimpleCalc TempEMV(), EOMArray(), MAVLength, Size, False

End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          MedianPriceCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MedianPriceCalc(PDArray() As PriceDataType, MedianPriceArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim MedianPriceArray(Size)

For I = 1 To Size
  MedianPriceArray(I) = (PDArray(I).High - PDArray(I).Low) / 2
Next I

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          NegativeVolumeCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      991205  Created
'               000118  Added code for avoiding an all zero array.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub NegativeVolumeCalc(PDArray() As PriceDataType, NegativeVolumeArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim NegativeVolumeArray(Size)

If Size <= 3 Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

If PDArray(1).Close <> 0 Then
  NegativeVolumeArray(2) = (((PDArray(2).Close - PDArray(1).Close) / PDArray(1).Close) * PDArray(2).Volume)
End If

If NegativeVolumeArray(2) <> 0 Then
  NegativeVolumeArray(1) = NegativeVolumeArray(2)
Else
  NegativeVolumeArray(2) = 1
  NegativeVolumeArray(1) = 1
End If

For I = 3 To Size
  If PDArray(I).Volume < PDArray(I - 1).Volume Then
    If PDArray(I - 1).Close <> 0 Then
      NegativeVolumeArray(I) = NegativeVolumeArray(I - 1) + (((PDArray(I).Close - PDArray(I - 1).Close) / PDArray(I - 1).Close) * NegativeVolumeArray(I - 1))
    Else
      Debug.Print "Fel i databasen orsakade fel i NegativeVolumeIndex"
      NegativeVolumeArray(I) = NegativeVolumeArray(I - 1)
    End If
  Else
    NegativeVolumeArray(I) = NegativeVolumeArray(I - 1)
  End If
Next I

End Sub

Public Sub OnBalanceVolumeCalc(PDArray() As PriceDataType, OBVArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim OBVArray(Size)

OBVArray(1) = 0

For I = 2 To Size
  If PDArray(I).Close > PDArray(I - 1).Close Then
    OBVArray(I) = OBVArray(I - 1) + PDArray(I).Volume
  ElseIf PDArray(I).Close < PDArray(I - 1).Close Then
    OBVArray(I) = OBVArray(I - 1) - PDArray(I).Volume
  Else
    OBVArray(I) = OBVArray(I - 1)
  End If
Next I

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          PositiveVolumeCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      991205  Created
'               000118  Added code for avoiding an all zero array.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub PositiveVolumeCalc(PDArray() As PriceDataType, PositiveVolumeArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim PositiveVolumeArray(Size)

If Size <= 3 Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

If PDArray(1).Close <> 0 Then
  PositiveVolumeArray(2) = (((PDArray(2).Close - PDArray(1).Close) / PDArray(1).Close) * PDArray(2).Volume)
End If

If PositiveVolumeArray(2) <> 0 Then
  PositiveVolumeArray(1) = PositiveVolumeArray(2)
Else
  PositiveVolumeArray(2) = 1
  PositiveVolumeArray(1) = 1
End If

For I = 3 To Size
  If PDArray(I).Volume > PDArray(I - 1).Volume Then
    If PDArray(I - 1).Close <> 0 Then
      PositiveVolumeArray(I) = PositiveVolumeArray(I - 1) + (((PDArray(I).Close - PDArray(I - 1).Close) / PDArray(I - 1).Close) * PositiveVolumeArray(I - 1))
    Else
      PositiveVolumeArray(I) = PositiveVolumeArray(I - 1)
      Debug.Print "Fel i databasen orsakade fel i PositiveVolumeCalc"
    End If
  Else
    PositiveVolumeArray(I) = PositiveVolumeArray(I - 1)
  End If
Next I

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          RateOfChangeCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      991205  Created
'               000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RateOfChangeCalc(PDArray() As PriceDataType, ROCArray() As Single, ROCLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim ROCArray(Size)

If Size <= ROCLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

For I = 1 To ROCLength
  ROCArray(I) = 100
Next I

For I = ROCLength + 1 To Size
  If PDArray(I - ROCLength).Close <> 0 Then
    ROCArray(I) = ((PDArray(I).Close - PDArray(I - ROCLength).Close) / PDArray(I - ROCLength).Close) * 100
  Else
    Debug.Print "Fel i databasen orsakade fel i Rate-Of-Change"
    ROCArray(I) = ROCArray(I - 1)
  End If
Next I
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TRIXCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TRIXCalc(PDArray() As PriceDataType, TRIXArray() As Single, TRIXLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim Temp1() As Single
Dim Temp2() As Single
Dim Temp3() As Single
ReDim TRIXArray(Size)
ReDim Temp1(Size)
ReDim Temp2(Size)
ReDim Temp3(Size)

If Size <= TRIXLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

ExpMAVCalc PDArray(), Temp1(), TRIXLength, Size
ExpCalc Temp1(), Temp2(), TRIXLength, Size
ExpCalc Temp2(), Temp3(), TRIXLength, Size

TRIXArray(1) = 0

For I = 2 To Size
  If Temp3(I - 1) <> o Then
    TRIXArray(I) = ((Temp3(I) - Temp3(I - 1)) / Temp3(I - 1)) * 100
  Else
    TRIXArray(I) = TRIXArray(I - 1)
    Debug.Print "Fel i databasen orsakade fel i TRIXCalc"
  End If
Next I
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TypicalPriceCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TypicalPriceCalc(PDArray() As PriceDataType, TypicalPriceArray() As Single, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim TypicalPriceArray(Size)

For I = 1 To Size
  With PDArray(I)
  TypicalPriceArray(I) = (.High + .Low + .Close) / 3
  End With
Next I

End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ChaikinsVolatiltyCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ChaikinsVolatilityCalc(PDArray() As PriceDataType, ChaikinsVolatilityArray() As Single, VolLength As Long, ROCLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim temp() As Single
Dim HLAverage() As Single
ReDim ChaikinsVolatilityArray(Size)
ReDim temp(Size)
ReDim HLAverage(Size)

If Size <= VolLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

For I = 1 To Size
 temp(I) = PDArray(I).High - PDArray(I).Low
Next I

ExpCalc temp(), HLAverage, VolLength, Size

For I = 1 To ROCLength
  ChaikinsVolatilityArray(I) = 0
Next I

For I = ROCLength + 1 To Size
  If HLAverage(I - ROCLength) Then
    ChaikinsVolatilityArray(I) = (((HLAverage(I) - HLAverage(I - ROCLength)) / HLAverage(I - ROCLength)) * 100)
  Else
    ChaikinsVolatilityArray(I) = ChaikinsVolatilityArray(I - 1)
    Debug.Print "Fel i databasen orsakade fel i ChaikinsVolatilityCalc"
  End If
Next I

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          VolumeOscillatorCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub VolumeOscillatorCalc(PDArray() As PriceDataType, VolumeOscArray() As Single, MAVLength1 As Long, MAVLength2 As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
Dim Temp1() As Single
Dim Temp2() As Single
Dim Temp3() As Single
ReDim VolumeOscArray(Size)
ReDim Temp1(Size)
ReDim Temp2(Size)
ReDim Temp3(Size)

If Size <= MAVLength1 Or Size <= MAVLength2 Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

For I = 1 To Size
  Temp3(I) = PDArray(I).Volume
Next I

ExpCalc Temp3(), Temp1(), MAVLength1, Size
ExpCalc Temp3(), Temp2(), MAVLength2, Size

For I = 2 To Size
  If Temp1(I) <> 0 Then
    VolumeOscArray(I) = ((Temp1(I) - Temp2(I)) / Temp1(I)) * 100
  Else
    VolumeOscArray(I) = VolumeOscArray(I - 1)
    Debug.Print "Fel i databasen orsakade fel i VolumeOscillatorCalc"
  End If
Next I

VolumeOscArray(1) = 100

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          VolumeROCCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub VolumeROCCalc(PDArray() As PriceDataType, VolumeROCArray() As Single, VolumeROCLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
Dim I As Long
ReDim VolumeROCArray(Size)

If Size <= VolumeROCLength Then
  If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
    MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
  Else
    ErrorFoundInIndicator = True
  End If
  Exit Sub
End If

For I = 1 To VolumeROCLength
  VolumeROCArray(I) = 100
Next I

For I = VolumeROCLength + 1 To Size
  If PDArray(I - VolumeROCLength).Volume <> 0 Then
    VolumeROCArray(I) = ((PDArray(I).Volume - PDArray(I - VolumeROCLength).Volume) / PDArray(I - VolumeROCLength).Volume) * 100
  Else
    VolumeROCArray(I) = VolumeROCArray(I - 1)
    Debug.Print "Fel i databasen orsakade fel i VolumeROCCalc"
  End If
Next I

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          StdDevCalc
'Input:
'Output:
'
'Description:   Calculates the indicator and returns the value in an array.
'Author:        Martin Lindberg
'Revision:      000118  Added code for avoiding problems with database containing zeros.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub StdDevCalc(PDArray() As PriceDataType, StdDev() As Single, StdDevLength As Long, Size As Long, SearchOptimizeOrTest As Boolean)
 Dim I As Long
 Dim j As Long
 Dim Sum1 As Single
 Dim Sum2 As Single
 Dim MAV() As Single
 Dim ROCArray() As Single
 Dim RelativeReturns() As Single
 Dim DailyReturns() As Single
 Dim DailyReturnsSquared() As Single
 
 ReDim MAV(Size)
 ReDim StdDev(Size)
 ReDim ROCArray(Size)
 ReDim RelativeReturns(Size)
 ReDim DailyReturnsSquared(Size)
 ReDim DailyReturns(Size)
 
 If StdDevLength > Size Then
   If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna Bollinger"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
 
 RelativeReturns(1) = 1
 DailyReturns(1) = 0
 DailyReturnsSquared(1) = 0
 For I = 2 To Size
   If PDArray(I - 1).Close <> 0 Then
     RelativeReturns(I) = PDArray(I).Close / PDArray(I - 1).Close
   Else
     RelativeReturns(I) = RelativeReturns(I - 1)
     Debug.Print "Fel i databasen orsakade fel i StdDevCalc"
   End If
   DailyReturns(I) = Log(RelativeReturns(I))
   DailyReturnsSquared(I) = (Log(RelativeReturns(I)) ^ 2)
 Next I
 
 
 I = StdDevLength
 Do While I <= Size
   Sum1 = 0
   Sum2 = 0
   For j = 1 To StdDevLength
     Sum1 = Sum1 + DailyReturnsSquared(I - StdDevLength + j)
     Sum2 = Sum2 + DailyReturns(I - StdDevLength + j)
   Next j
   StdDev(I) = Sqr(((Sum1 / (StdDevLength - 1)) - ((Sum2 ^ 2) / (StdDevLength * (StdDevLength - 1)))))
   I = I + 1
 Loop

 For I = 1 To StdDevLength
   StdDev(I) = StdDev(StdDevLength)
 Next I

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  SimpleCalc
'Input:         An empty array an the period.
'Output:        An array that contains the MAV.
'Description:   This procedure calculates a moving average
'               of the input array.
'Written:       970327 by Martin Lindberg
'Modified:      970328 by Martin Lindberg
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SimpleCalc(inArray() As Single, ByRef outArray() As Single, ByVal Length As Long, Size As Long, SearchOptimizeOrTest As Boolean)
  
  Dim I, N, j As Long
  Dim Sum As Single
  ReDim outArray(Size) As Single
  
  If Length > Size Then
    If Not SearchOptimizeOrTest Or ShowIndicatorErrorMessage Then
      MsgBox CheckLang("Det finns f�r f� lagrade kurser f�r att ber�kna medelv�rdet"), 48, CheckLang("Felmeddelande")
    Else
      ErrorFoundInIndicator = True
    End If
    Exit Sub
  End If
  
  Sum = 0
  For k = 1 To Length
    Sum = Sum + inArray(k)
    outArray(k) = Sum / k
  Next k
  I = 1
  Do Until I = Size - Length + 1
    Sum = Sum + inArray(Length + I) - inArray(I)
    outArray(Length + I) = Sum / Length
    I = I + 1
  Loop
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  KeyReversalDayCalc
'Input:         PriceDataArray
'Output:        KeyReversalDay
'Description:   Creates a Key Reversal Day array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub KeyReversalDayCalc(PDArray() As PriceDataType, KeyReversalDay() As Boolean)
Dim I As Integer

KeyReversalDay(1) = False
For I = 2 To UBound(PDArray)
  If PDArray(I).Low < PDArray(I - 1).Low And PDArray(I).Close > PDArray(I - 1).High Then
    KeyReversalDay(I) = True
  Else
    KeyReversalDay(I) = False
  End If
Next I
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  ReversalDayCalc
'Input:         PriceDataArray
'Output:        ReversalDay
'Description:   Creates a Reversal Day array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReversalDayCalc(PDArray() As PriceDataType, ReversalDay() As Boolean)
Dim I As Integer

ReversalDay(1) = False
For I = 2 To UBound(PDArray)
  If PDArray(I).Low < PDArray(I - 1).Low And PDArray(I).Close > PDArray(I - 1).Close Then
    ReversalDay(I) = True
  Else
    ReversalDay(I) = False
  End If
Next I

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  TwoDayReversalCalc
'Input:         PriceDataArray
'Output:        TwoDayReversal
'Description:   Creates a Two Day Reversal array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub TwoDayReversalCalc(PDArray() As PriceDataType, TwoDayReversal() As Boolean)
Dim I As Integer
Dim MAV10() As Single
Dim MAV50() As Single
ReDim MAV10(UBound(PDArray))
ReDim MAV50(UBound(PDArray))

ExpMAVCalc PDArray, MAV10, 10, UBound(PDArray)
ExpMAVCalc PDArray, MAV50, 50, UBound(PDArray)

TwoDayReversal(1) = False
For I = 2 To UBound(PDArray)
  If PDArray(I - 1).High <> PDArray(I - 1).Low And PDArray(I).High <> PDArray(I).Low Then
    If (((PDArray(I - 1).Close - PDArray(I - 1).Low) / (PDArray(I - 1).High - PDArray(I - 1).Low)) < 0.2) And (((PDArray(I).High - PDArray(I).Close) / (PDArray(I).High - PDArray(I).Low)) < 0.2) And PDArray(I).Close > MAV10(I) And PDArray(I).Close > MAV50(I) Then
      TwoDayReversal(I) = True
    Else
      TwoDayReversal(I) = False
    End If
  Else
    TwoDayReversal(I) = False
  End If
Next I

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  OneDayReversalCalc
'Input:         PriceDataArray
'Output:        OneDayReversal
'Description:   Creates a One Day Reversal array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub OneDayReversalCalc(PDArray() As PriceDataType, OneDayReversal() As Boolean)
Dim I As Integer

OneDayReversal(1) = False
For I = 2 To UBound(PDArray)
  If PDArray(I).High <> PDArray(I).Low Then
    If PDArray(I).Low < PDArray(I - 1).Low And ((PDArray(I).High - PDArray(I).Close) / (PDArray(I).High - PDArray(I).Low)) < 0.2 Then
      OneDayReversal(I) = True
    Else
      OneDayReversal(I) = False
    End If
  Else
    OneDayReversal(I) = False
  End If
Next I

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  PatternGapCalc
'Input:         PriceDataArray
'Output:        PatternGap
'Description:   Creates a Pattern Gap array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub PatternGapCalc(PDArray() As PriceDataType, PatternGap() As Boolean)
Dim I As Integer

PatternGap(1) = False
PatternGap(2) = False

If UBound(PDArray) > 2 Then
  For I = 3 To UBound(PDArray)
    If PDArray(I).High <> PDArray(I).Low Then
      If PDArray(I).Low > PDArray(I - 1).Close And ((PDArray(I).High - PDArray(I).Close) / (PDArray(I).High - PDArray(I).Low)) <= 0.5 And PDArray(I).Close > PDArray(I - 1).High And PDArray(I).Close > PDArray(I - 2).Close Then
        PatternGap(I) = True
      Else
        PatternGap(I) = False
      End If
    Else
      PatternGap(I) = False
    End If
  Next I
End If

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  ReversalGapCalc
'Input:         PriceDataArray
'Output:        ReversalGap
'Description:   Creates a Reversal Gap array for use as setup signaling
'Written:       990808
'Modified:      990808
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReversalGapCalc(PDArray() As PriceDataType, ReversalGap() As Boolean)
Dim I As Integer

ReversalGap(1) = False
ReversalGap(2) = False

For I = 3 To UBound(PDArray)
  If PDArray(I).High <> PDArray(I).Low Then
    If PDArray(I).Low > PDArray(I - 1).High And ((PDArray(I).High - PDArray(I).Close) / (PDArray(I).High - PDArray(I).Low)) <= 0.5 And PDArray(I).Close > PDArray(I - 2).Close Then
      ReversalGap(I) = True
    Else
      ReversalGap(I) = False
    End If
  Else
    ReversalGap(I) = False
  End If
Next I

End Sub

Sub VolatilityCalc(PDArray() As PriceDataType, Volatility() As Single)
Dim I As Long
Dim StdDev() As Single

ReDim Volatility(UBound(PDArray))

StdDevCalc PDArray, StdDev, ParameterSettings.StandardDeviationLength, UBound(PDArray), False

If GraphSettings.DWM = 0 Then
  For I = 1 To UBound(PDArray)
    Volatility(I) = StdDev(I) * Sqr(250)
  Next I
ElseIf GraphSettings.DWM = 1 Then
  For I = 1 To UBound(PDArray)
    Volatility(I) = StdDev(I) * Sqr(52)
  Next I
  
Else
  For I = 1 To UBound(PDArray)
    Volatility(I) = StdDev(I) * Sqr(12)
  Next I
End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          SimpleMAVIncrementalCalc
'Input:         PriceDataArray, StartIndex
'Output:        IncrementalArray
'
'Description:   Creates a incremental array for FishNet.
'Author:        Martin Lindberg
'Revision:      990829  Created
'               990829  Changed indexing of IncrementalMAVArray.
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SimpleMAVIncrementalCalc(PDArray() As PriceDataType, IncrementalMAVArray() As Single, StartIndex As Long)
Dim Sum As Single
Dim I As Long
Dim j As Long
Dim Size As Long

Size = UBound(PDArray) - StartIndex + 1

ReDim IncrementalMAVArray(StartIndex To UBound(PDArray))

j = 1
Sum = 0

  For k = StartIndex To UBound(PDArray)
    Sum = Sum + PDArray(k).Close
    IncrementalMAVArray(k) = Sum / j
    j = j + 1
  Next k
End Sub

Public Sub InitIndicatorArray()
    ReDim IndicatorArray(1 To 22)
    IndicatorArray(1) = "Accumulation/Distribution"
    IndicatorArray(2) = "Average True Range"
    IndicatorArray(3) = "Chaikin Oscillator"
    IndicatorArray(4) = "Chaikin's Volatility"
    IndicatorArray(5) = "Detrended Price Oscillator"
    IndicatorArray(6) = "Ease Of Movement"
    IndicatorArray(7) = "Moving Average"
    IndicatorArray(8) = "MACD"
    IndicatorArray(9) = "Momentum"
    IndicatorArray(10) = "Money Flow Index"
    IndicatorArray(11) = "Negative Volume Index"
    IndicatorArray(12) = "On Balance Volume"
    IndicatorArray(13) = "Positive Volume Index"
    IndicatorArray(14) = "Price Oscillator"
    IndicatorArray(15) = "Rate-Of-Change"
    IndicatorArray(16) = "Relative Strength Index"
    IndicatorArray(17) = "Standard Deviation"
    IndicatorArray(18) = "Stochastic"
    IndicatorArray(19) = "TRIX"
    IndicatorArray(20) = "Volatilitet"
    IndicatorArray(21) = "Volume Oscillator"
    IndicatorArray(22) = "Volume Rate-Of-Change"
    IndicatorArraySize = 22
End Sub

Public Sub InitColorIndicatorArray()
  ReDim ColorIndicatorArray(1 To 11)

    ColorIndicatorArray(1) = "Ease Of Movement"
    ColorIndicatorArray(2) = "MACD"
    ColorIndicatorArray(3) = "Momentum"
    ColorIndicatorArray(4) = "Money Flow Index"
    ColorIndicatorArray(5) = "Moving Average"
    ColorIndicatorArray(6) = "Parabolic"
    ColorIndicatorArray(7) = "Price Oscillator"
    ColorIndicatorArray(8) = "Rate-Of-Change"
    ColorIndicatorArray(9) = "Relative Strength Index"
    ColorIndicatorArray(10) = "Stochastic"
    ColorIndicatorArray(11) = "TRIX"
    
End Sub
