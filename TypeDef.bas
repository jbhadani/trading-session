Attribute VB_Name = "TypeDef"
' DataTypes
' Last modified: 970323 (FW)
' Comments:
'


Type FishParameterSettingsType
  lngLongest As Long
  lngShortest As Long
  lngStep As Long
End Type


Type ParameterSettingsType
  MAV1 As Long
  MAV2 As Long
  RSI As Long
  MAVRSI As Long
  STO As Long '%K
  STOSlowing As Long 'Slowing
  MAVSTO As Long '%D
  MOM As Long
  MAVMOM As Long
  PRIOSC1 As Long
  PRIOSC2 As Long
  MAVUseExp As Boolean
  MAVNbr As Long
  PlotRSIandMAV As Boolean
  RSISignalUseMAV As Boolean
  PlotSTOandMAV As Boolean
  STOSignalUseSignalLevels As Boolean
  PlotMOMandMAV As Boolean
  OSCMAVExp As Boolean
  MACDMAV As Boolean
  MOMSignalUseMAV As Boolean
  MOMSignalUseZeroLine As Boolean
  MFIShowMAV As Boolean
  MFILength As Long
  MAVMFI As Long
  MFISignalUseMAV As Boolean
  COPMAV As Long
  COPDist As Long
  COPSUM As Long
  BollingerMAVLength As Long
  BollingerSTDFactor As Single
  AccelerationFactor As Single
  AverageTrueRangeLength As Long
  ChaikinVolatility As Long
  ChaikinVolatilityROC As Long
  DetrendedPriceOscillatorLength As Long
  EaseOfMovementLength As Long
  EaseOfMovementMAVLength As Long
  PlotEaseOfMovementAndMAV As Boolean
  NegativeVolumeMAVLength As Long
  PlotNegativeVolumeAndMAV As Boolean
  PositiveVolumeMAVLength As Long
  PlotPositiveVolumeAndMAV As Boolean
  RateOfChangeLength As Long
  RateOfChangeMAVLength As Long
  PlotRateOfChangeAndMAV As Boolean
  StandardDeviationLength As Long
  StandardDeviationMAVLength As Long
  PlotStandardDeviationAndMAV As Boolean
  TRIXLength As Long
  TRIXMAVLength As Long
  PlotTRIXAndMAV As Boolean
  VolumeOscillatorMAV1Length As Long
  VolumeOscillatorMAV2Length As Long
  VolumeRateOfChangeLength As Long
  MFILowerSignalLevel As Long
  MFIUpperSignalLevel As Long
  RSILowerSignalLevel As Long
  RSIUpperSignalLevel As Long
  STOLowerSignalLevel As Long
  STOUpperSignalLevel As Long
  VolatilityMAVLength As Long
  Volatility As Long
  PlotVolatilityAndMAV As Boolean
End Type

Type TrendLineType
  InUse As Boolean
  StartDate As Date
  EndDate As Date
  StartPrice As Single
  EndPrice As Single
  Extend As Boolean
  LineType As Byte
  LineWidth As Byte
  LineColor As Long
End Type

Type WorkspaceSettingsType
  Name As String * 20
  ParameterSettings As ParameterSettingsType
  TrendLine(1 To 20) As TrendLineType
End Type

Type WorkspaceSettings_v2_Type
  Name As String * 20
  ParameterSettings As ParameterSettingsType
  FishParameterSettings As FishParameterSettingsType
  TrendLine(1 To TrendLineSeries, 1 To TrendlineSerieSize) As TrendLineType
End Type

'Type PriceDataType
'  Date As Date
'  Close As Single
'  High As Single
'  Low As Single
'  volume As Single
'Shares As Single
'End Type

Type PriceDataType
  Date As Date
  Open As Single
  Close As Single
  High As Single
  Low As Single
  Volume As Single
  OpenInterest As Single
End Type


'Type InvestInfoTypeOld
'  FileName As String * 20
'  Name As String * 20
'  Symbol1 As String * 10  '
'  Symbol2 As String * 10  '
'  TypeOfInvest As Byte
'  ParameterSettings As ParameterSettingsType '
'  TrendLine(1 To 20) As TrendLineType '
'End Type

Type InvestInfoType
  FileName As String * 20
  Name As String * 20
  TypeOfInvest As Byte
End Type

Type PortfolioType
  Name As String * 20
End Type

Type TransactionType
  Name As String * 20
  DepositType As String * 20
  Date As Date
  Price As Single
  Courtage As Single
  Volume As Single
  Cash As Double
  IsBuySell As Boolean
  IsBuy As Boolean
  IsDeposit As Boolean
End Type

Type DividendType
  Date As Date
  Amount As Single
End Type

'Type SetupSettingsType
'  ShowKeyReversalDay As Boolean
'  ShowReversalDay As Boolean
'  ShowOneDayReversal As Boolean
'  ShowTwoDayReversal As Boolean
'  ShowPatternGap As Boolean
'  ShowReversalGap As Boolean
'End Type

Type SetupSettingsType
  blnShow As Boolean
  lngColor As Long
End Type
Type GraphSettingsType
  ShowHLC As Boolean
  PriceColor As Long
  BackGroundColor As Long
  AxisColor As Long
  GridColor As Long
  Serie1Color As Long
  Serie2Color As Long
  ShowPriceMAV1 As Boolean
  PriceMAV1Color As Long
  ShowPriceMAV2 As Boolean
  PriceMAV2Color As Long
  ShowXGrid As Boolean
  ShowYGrid As Boolean
  ExponentialPrice As Boolean
  
  ShowSignalColor As Boolean
 
  
  SignalIndicator As Long
  SignalColorBuy As Long
  SignalColorSell As Long
  DayWidth As Byte

  ShowIndicator1 As Boolean
  ShowIndicator2 As Boolean
  
  ShowVerticalColorLines As Boolean
  
  ShowBollinger As Boolean
  BollingerColor As Long
  
  ShowParabolic As Boolean
  ParabolicColor As Long
  ShowZero As Boolean
  
  
End Type

Type GraphSettings_v2_Type
  ShowHLC As Boolean
  PriceColor As Long
  lngCloseColor As Long
  lngCloseWidth As Long
  BackGroundColor As Long
  lngDrawAreaColor As Long
  AxisColor As Long
  GridColor As Long
  Serie1Color As Long
  Serie2Color As Long
  ShowPriceMAV1 As Boolean
  PriceMAV1Color As Long
  ShowPriceMAV2 As Boolean
  PriceMAV2Color As Long
  lngFishColor As Long
  lngSupportResistanceColor As Long
  lngVolumeColor As Long
  ShowXGrid As Boolean
  ShowMinXGrid As Boolean
  ShowYGrid As Boolean
  
  ExponentialPrice As Boolean
  
  ShowSignalColor As Boolean
  strColorSignallingIndicator As String
  
  SignalIndicator As Long
  SignalColorBuy As Long
  
  lngSignalColorSell As Long
  
  lngDayWidth As Long
  lngDaySpace As Long
  
  ShowIndicator1 As Boolean
  ShowIndicator2 As Boolean
  strIndicator1 As String
  strIndicator2 As String
  
  ShowVerticalColorLines As Boolean
  
  ShowBollinger As Boolean
  BollingerColor As Long
  
  ShowParabolic As Boolean
  ParabolicColor As Long
  ShowZero As Boolean
  
  DWM As DWMEnum
  ShowVolume As Boolean
  
  blnShowCompare As Boolean
  lngCompareIndex As Long
  
  blnShowFish As Boolean
  blnShowHighlight As Boolean
  
  blnShowSupportResistance As Boolean
  'Show setup
  Setup(1 To 6) As SetupSettingsType
End Type


Type GraphPropertiesType
  GraphLeft As Long
  GraphBottom As Long
  GraphWidth As Long
  GraphHeight As Long
  Name As String * 20
  
  ShowHLC As Boolean
  ShowPriceMAV1 As Boolean
  ShowPriceMAV2 As Boolean
  PriceMAV1Color As Long
  PriceMAV2Color As Long
  PriceColor As Long
  BackGroundColor As Long
  AxisColor As Long
  GridColor As Long
  ShowXGrid As Boolean
  ShowMinXGrid As Boolean
  ShowYGrid As Boolean
  ExponentialPrice As Boolean
  ShowSignalColor As Boolean
  SignalColorBuy As Long
  SignalColorSell As Long
  ShowBollinger As Boolean
  BollingerColor As Long
  ShowParabolic As Boolean
  ParabolicColor As Long
  ShowZero As Boolean
  strLatest As String
  strGainDay As String
  strGainWeek As String
  strGainMonth As String
  strGainYear As String
  
  blnShowFish As Boolean
  blnShowSupportResistance As Boolean
  strColorSignallingIndicator As String
  
  lngCloseColor As Long
  lngFishColor As Long
  lngSupportResistanceColor As Long
  lngDrawAreaColor As Long
  lngCloseWidth As Long
  Setup(1 To 6) As SetupSettingsType
End Type

Type IndicatorPropertiesType
  'Left As Long
  'Width As Long
  'Bottom As Long
  'Height As Long
  
  ShowSerie1 As Boolean
  ShowSerie2 As Boolean
  
  Serie1Name As String
  Serie2name As String
  
  Serie1Color As Long
  Serie2Color As Long
  
  FixMaxMin As Boolean
  Max As Long
  Min As Long
  
  ShowFixLine1 As Boolean
  ShowFixLine2 As Boolean
  FixLine1Value As Long
  FixLine2Value As Long
  
  ShowScale As Boolean
  DayWidth As Byte
  
  AxisColor As Long
  GridColor As Long
  lngColorDrawArea As Long
End Type

Type VolumePropertiesType
  ColorVolume As Long
  ColorAxis As Long
  ColorGrid As Long
  lngColorDrawArea As Long
End Type

Type OptimizeType
  Parameter1 As Long
  Parameter2 As Long
  Parameter3 As Long
  sngParameter4 As Single
  NbrOfTrades As Long
  NbrOfWinningTrades As Long
  NbrOfLosingTrades As Long
  BiggestGain As Single
  Result As Single
End Type

Enum DrawModeType
  dmNone = 0
  dmDelete = 1
  dmDraw = 2
  dmDrawHorizontal = 3
  dmDrawVertical = 4
  dmMove = 5
  dmText = 6
  dmDrawParallel = 7
  'Bj�rn hackar
  dmDrawDates = 8
  dmDrawZ = 9
  dmMoveZ = 10
  'Slut hack
End Enum
 
Enum AttachModeType
  amNone = 0
  amHigh = 1
  amLow = 2
End Enum
 
Type DrawSettingsType
  LineWidth As Byte
  LineColor As Long
  LineType As Byte
End Type


Type SignalType
  Name As String * 20
  LastDate As Date
  LastPrice As Single
  LastType As Boolean
  NbrOfBuySignalsForObject As Long
  NbrOfSellSignalsForObject As Long
  NbrOfCOP As Long
  NbrOfMACD As Long
  NbrOfMAV As Long
  NbrOfMFI As Long
  NbrOfMOM As Long
  NbrOfOSC As Long
  NbrOfREV As Long
  NbrOfRSI As Long
  NbrOfSTO As Long
  NbrOfSAR As Long
  NbrOfEOM As Long
  NbrOfROC As Long
  NbrOfTRIX As Long
  NbrOfReversalDay As Long
  NbrOfTwoDayReversal As Long
  NbrOfOneDayReversal As Long
  NbrOfPatternGap As Long
  NbrOfReversalGap As Long
  NbrOfCOPSell As Long
  NbrOfMACDSell As Long
  NbrOfMAVSell As Long
  NbrOfMFISell As Long
  NbrOfMOMSell As Long
  NbrOfOSCSell As Long
  NbrOfREVSell As Long
  NbrOfRSISell As Long
  NbrOfSTOSell As Long
  NbrOfSARSell As Long
  NbrOfEOMSell As Long
  NbrOfROCSell As Long
  NbrOfTRIXSell As Long
  NbrOfReversalDaySell As Long
  NbrOfTwoDayReversalSell As Long
  NbrOfOneDayReversalSell As Long
  NbrOfPatternGapSell As Long
  NbrOfReversalGapSell As Long
End Type

Type IndicatorSerieType
    Serie1() As Single
    Serie2() As Single
End Type

Type PortfolioOverheadType
  TotalCash As Single
  MarketValue As Single
  PortfolioValue As Single
  PortfolioProfit As Single
End Type


Type StockListType
  Name As String * 20
  UppDownToday As Single
  PercentToday As Single
  Close As Single
  High As Single
  Low As Single
  PercentThisYear As Single
  Volume As Single
  Date As Date
End Type

Type StartAndStopIndexes
  StartIndex As Long
  StopIndex As Long
  Year As Long
End Type

 Type TLWholeInViewType
   End1 As Boolean
   End2 As Boolean
 End Type


Type SemaphoreType
  wait As Boolean
  int1 As Long
  int2 As Long
  str1  As String
  str2 As String
End Type

Type GraphDrawInfoType
  Start As Long
  Stop As Long
  DaySpace As Long
  DayWidth As Long
  DWM As DWMEnum
End Type

Type GraphDrawRectType
  left As Long
  bottom As Long
  Height As Long
  Width As Long
End Type

Type IndicatortestOptionsType
  'Stops
  UseBreakevenLongPositions As Boolean
  UseBreakevenShortPositions As Boolean
  BreakevenMethodIsPercent As Boolean
  BreakevenLevel As Single
  UseInactivityLongPositions As Boolean
  UseInactivityShortPositions As Boolean
  InactivityMethodIsPercent As Boolean
  InactivityMinChange As Single
  InactivityPeriods As Long
  UseMaxLossLongPositions As Boolean
  UseMaxLossShortPositions As Boolean
  MaxLossMethodIsPercent As Boolean
  MaxLossLevel As Single
  UseProfitTakingLongPositions As Boolean
  UseProfitTakingShortPositions As Boolean
  ProfitTakingMethodIsPercent As Boolean
  ProfitTarget As Single
  UseTrailingLongPositions As Boolean
  UseTrailingShortPositions As Boolean
  TrailingMethodIsPercent As Boolean
  TrailingProfitRisk As Single
  TrailingPeriods As Long
  
  'General
  TradePrice As Integer '0=Open ,1=High, 2=Low, 3=Close
  Delay As Integer
  LongOnly As Boolean
  ShortOnly As Boolean
  BothLongAndShort As Boolean
  InitialEquity As Single
  MarginRequirement As Single
  AnnualInterestRate As Single
  
  'Timeperiod
  StartDate As Long
  StopDate As Long
  
  'Commissions
  UseCommission As Boolean
  UsePercentageCommission As Boolean
  UseMinimumCommission As Boolean
  Percentage As Single
  Capital As Single
  MinCommission As Single
   
  'Report
  BuyColor As Integer
  SellColor As Integer
  StopColor As Integer
  ShowArrows As Boolean
  ShowArrowLabels As Boolean
  RemoveExistingArrows As Boolean
  PlotEquityLine As Boolean
  RemoveExistingLines As Boolean
  
End Type

'''''
'Added 99-03-24
'ML
'New types for IndicatorTest
'''''
Type IndicatortestStatisticsType
  TotalNetProfit As Single
  PercentGainLoss As Single
  InitialInvestment As Single
  OpenPositionValue As Single
  AnnualPercentGainLoss As Single
  InterestEarned As Single
  CurrentPosition As String * 5
  DateCurrentPositionEntered As Date
  BuyHoldProfit As Single
  BuyHoldPercentageProfit As Single
  DaysInTest As Long
  AnnualBuyHoldPercentage As Single
  TotalClosedTrades As Integer
  AverageProfitPerTrade As Single
  TotalLongTrades As Integer
  WinningLongTrades As Integer
  CommissionsPaid As Single
  AverageWinLossRatio As Single
  TotalShortTrades As Integer
  WinningShortTrades As Integer
  TotalWinningTrades As Integer
  AmountOfWinningTrades As Single
  AverageWin As Single
  LargestWin As Single
  AverageLengthOfWin As Single
  LongestWinningTrade As Integer
  MostConsecutiveWins As Integer
  TotalLosingTrades As Integer
  AmountOfLosingTrades As Single
  AverageLoss As Single
  LargestLoss As Single
  AverageLengthOfLoss As Single
  LongestLosingTrade As Integer
  MostConsecutiveLosses As Integer
  TotalBarsOut As Long
  LongestOutPeriod As Long
  AverageLengthOut As Single
  SystemCloseDrawDown As Single
  SystemOpenDrawDown As Single
  MaxOpenTradeDrawDown As Single
  ProfitLossIndex As Single
  RewardRiskIndex As Single
  BuyHoldIndex As Single
End Type

'''''
'Added 99-03-24
'ML
'New types for IndicatorTest
'''''
Type IndicatortestResultType
  DateOfPositionEntry As Date
  DateOfPositionClose As Date
  TradePriceAtEntry As Single
  TradePriceAtClose As Single
  TypeOfTrade As String * 5
  ReasonForClose As Integer
  StartIndex As Integer
  StoppIndex As Integer
End Type

'Ny typ f�r triggers.
'Inlagd 1999-07-07
'Martin
Type TriggerType
  Name As String * 20
  Date As Date
  UpperTriggerLevel As Single
  LowerTriggerLevel As Single
  Position As Long
  Close As Single
End Type



  


' Used in GetScale
Type RoundArrayType
  sngScale As Single
  lngSmallStep As Long
End Type



Type TextsType
  strText As String * 30
  strFont As String * 20
  lngColor As Long
  sngPrice As Single
  dtmDate As Date
End Type

Type RECT
  left As Long
  top As Long
  right As Long
  bottom As Long
End Type

#If 0 Then
Type PortfolioValueType
  Date As Date
  Value As Single
End Type
#End If

