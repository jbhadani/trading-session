VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmIndicatortestOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Inst�llningar f�r indikatortest"
   ClientHeight    =   4380
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7500
   Icon            =   "frmIndicatortestOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4380
   ScaleWidth      =   7500
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   735
      Index           =   1
      Left            =   0
      TabIndex        =   21
      Top             =   3600
      Width           =   7455
      Begin VB.CommandButton Command1 
         Caption         =   "Avancerade..."
         Height          =   375
         Left            =   5760
         TabIndex        =   24
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   3120
         TabIndex        =   23
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   600
         TabIndex        =   22
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Courtage"
      Height          =   1815
      Index           =   0
      Left            =   2640
      TabIndex        =   7
      Top             =   1680
      Width           =   4815
      Begin VB.Frame Frame9 
         Caption         =   "Typ av courtage"
         Height          =   1455
         Left            =   2280
         TabIndex        =   9
         Top             =   240
         Width           =   2415
         Begin VB.TextBox txtMinCommission 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   27
            Text            =   "200"
            Top             =   840
            Width           =   615
         End
         Begin VB.OptionButton optPercentage 
            Caption         =   "Procentsats"
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   360
            Width           =   1335
         End
         Begin VB.OptionButton optMinimum 
            Caption         =   "Minimicourtage"
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   840
            Width           =   1455
         End
         Begin VB.TextBox txtPercentageCommission 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1680
            MaxLength       =   5
            TabIndex        =   12
            Text            =   "0,2"
            Top             =   320
            Width           =   615
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Allm�nt"
         Height          =   1455
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   2055
         Begin VB.OptionButton optDontUseCommission 
            Caption         =   "Anv�nd ej courtage"
            Height          =   255
            Left            =   120
            TabIndex        =   11
            Top             =   840
            Width           =   1695
         End
         Begin VB.OptionButton optUseCommission 
            Caption         =   "Anv�nd courtage"
            Height          =   255
            Left            =   120
            TabIndex        =   10
            Top             =   360
            Width           =   1695
         End
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Testets tidsomfattning"
      Height          =   1575
      Left            =   2640
      TabIndex        =   3
      Top             =   0
      Width           =   4815
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   3240
         TabIndex        =   26
         Top             =   1080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61603841
         CurrentDate     =   36385
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   360
         TabIndex        =   25
         Top             =   1080
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61603841
         CurrentDate     =   36367
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Databasen omfattar 1993-01-04 - 1999-07-02"
         Height          =   255
         Left            =   360
         TabIndex        =   6
         Top             =   360
         Width           =   4095
      End
      Begin VB.Line Line1 
         X1              =   1680
         X2              =   3120
         Y1              =   1200
         Y2              =   1200
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Caption         =   "Startdatum"
         Height          =   255
         Left            =   480
         TabIndex        =   5
         Top             =   840
         Width           =   975
      End
      Begin VB.Label Label9 
         Alignment       =   2  'Center
         Caption         =   "Slutdatum"
         Height          =   255
         Left            =   3360
         TabIndex        =   4
         Top             =   840
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Allm�nt"
      Height          =   3495
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2535
      Begin VB.TextBox txtDelay 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MaxLength       =   1
         TabIndex        =   18
         Text            =   "0"
         Top             =   1200
         Width           =   1095
      End
      Begin VB.OptionButton optLongOnly 
         Caption         =   "Endast l�ng"
         Height          =   255
         Left            =   600
         TabIndex        =   17
         Top             =   2400
         Width           =   1575
      End
      Begin VB.OptionButton optShortOnly 
         Caption         =   "Endast kort"
         Height          =   255
         Left            =   600
         TabIndex        =   16
         Top             =   2640
         Width           =   1575
      End
      Begin VB.OptionButton optBothLongAndShort 
         Caption         =   "B�de och"
         Height          =   255
         Left            =   600
         TabIndex        =   15
         Top             =   2880
         Width           =   1575
      End
      Begin VB.TextBox txtInitialEquity 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MaxLength       =   12
         TabIndex        =   1
         Text            =   "1000"
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label5 
         Caption         =   "Vilka positioner skall anv�ndas"
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   2040
         Width           =   2175
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "F�rdr�jning, dagar:"
         Height          =   255
         Left            =   600
         TabIndex        =   19
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   2  'Center
         Caption         =   "Startkapital"
         Height          =   255
         Left            =   840
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmIndicatortestOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click()
  Unload Me
End Sub

Private Sub cmdOk_Click()
  UpdateIndicatortestOptions
  Me.Hide
End Sub

Private Sub Command1_Click()
  frmIndicatortestStops.Show
End Sub

Private Sub Form_Load()
Dim Filename1 As String

Filename1 = DataPath + Trim(InvestInfoArray(GetInvestIndex(IndicatortestObject)).FileName) + ".prc"
    
R_PriceDataArray Filename1, PDIndicatortestArray(), PDIndicatortestArraySize

Width = 7620
Height = 4785
top = 1000
left = 500
     

With IndicatorTestOptions
 txtDelay = .Delay
 If .LongOnly Then
   optLongOnly = True
 ElseIf .ShortOnly Then
   optShortOnly = True
 Else
   optBothLongAndShort = True
 End If
 
 txtInitialEquity = .InitialEquity
 
 If .StartDate <> 0 Then
   DTPicker1.Value = CDate(.StartDate)
   DTPicker2.Value = CDate(.StopDate)
 End If
  
 If IndicatortestObject <> "" Then
    Label3.Caption = "Databasen omfattar " & CDate(Int(PDIndicatortestArray(1).Date)) & " - " & CDate(Int(PDIndicatortestArray(PDIndicatortestArraySize).Date))
 End If

 DTPicker1.MaxDate = CDate(Int(PDIndicatortestArray(PDIndicatortestArraySize).Date))
 DTPicker2.MinDate = CDate(Int(PDIndicatortestArray(1).Date))
 DTPicker1.Value = GetDateString(PDIndicatortestArray(1).Date)
 DTPicker2.Value = GetDateString(PDIndicatortestArray(PDIndicatortestArraySize).Date)
 
 If .UseCommission Then
   optUseCommission = True
 Else
   optDontUseCommission = True
 End If
 
 If .UsePercentageCommission Then
   optPercentage = True
   optMinimum = False
   
 Else
   optMinimum = True
   optPercentage = False
   
 End If
 
 txtPercentageCommission.Text = .Percentage
 txtMinCommission.Text = .MinCommission
 
End With
End Sub


Private Sub optMinimum_Click()
  optPercentage.Value = False
  txtPercentageCommission.Enabled = False
  txtMinCommission.Enabled = True
End Sub

Private Sub optPercentage_Click()
  optMinimum.Value = False
  txtMinCommission.Enabled = False
  txtPercentageCommission.Enabled = True
End Sub

Private Sub txtDelay_GotFocus()
  TextSelected
End Sub

Private Sub txtDelay_Validate(Cancel As Boolean)
  Dim A As Long
  Dim Error As Boolean
  
  On Error Resume Next
  A = CLng(txtDelay.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), 16, CheckLang("Inget heltal")
    Err.Clear
    Error = True
  End If
  If Error Then
    Cancel = True
  Else
    Cancel = False
  End If
  txtDelay.SetFocus
End Sub

Private Sub txtInitialEquity_GotFocus()
  TextSelected
End Sub

Private Sub txtInitialEquity_Validate(Cancel As Boolean)
  Dim A As Long
  Dim Error As Boolean
  
  On Error Resume Next
  A = CLng(txtInitialEquity.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), 16, CheckLang("Inget heltal")
    Err.Clear
    Error = True
  End If
  If Error Then
    Cancel = True
  Else
    Cancel = False
  End If
  txtInitialEquity.SetFocus
End Sub

Private Sub txtMinCommission_GotFocus()
  TextSelected
End Sub

Private Sub txtMinCommission_Validate(Cancel As Boolean)
  Dim A As Long
  Dim Error As Boolean
  
  On Error Resume Next
  A = CLng(txtMinCommission.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), 16, CheckLang("Inget heltal")
    Err.Clear
    Error = True
  End If
  If Error Then
    Cancel = True
  Else
    Cancel = False
  End If
  txtMinCommission.SetFocus
End Sub

Private Sub txtPercentageCommission_GotFocus()
  TextSelected
End Sub

Private Sub txtPercentageCommission_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySinglePara txtPercentageCommission, Errorfound
  If Errorfound Then
    Cancel = True
  End If
End Sub
