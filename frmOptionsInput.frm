VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmOptionsInput 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " Inmatning optionsdata"
   ClientHeight    =   5235
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   Icon            =   "frmOptionsInput.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5235
   ScaleWidth      =   3765
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   19
      Top             =   4440
      Width           =   3735
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2400
         TabIndex        =   21
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   20
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Optionsdata"
      Height          =   4335
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      Begin MSComCtl2.DTPicker DTPicker2 
         Height          =   315
         Left            =   1800
         TabIndex        =   23
         Top             =   1920
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61997057
         CurrentDate     =   36402
      End
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   1800
         TabIndex        =   22
         Top             =   1560
         Width           =   1575
         _ExtentX        =   2778
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61997057
         CurrentDate     =   36402
      End
      Begin VB.TextBox txtVolatility 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   18
         Top             =   3840
         Width           =   1575
      End
      Begin VB.TextBox txtOptionPrice 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   17
         Top             =   3480
         Width           =   1575
      End
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "frmOptionsInput.frx":058A
         Left            =   1800
         List            =   "frmOptionsInput.frx":0594
         TabIndex        =   14
         Text            =   "K�p"
         Top             =   3000
         Width           =   1575
      End
      Begin VB.TextBox txtObjectPrice 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   12
         Top             =   2640
         Width           =   1575
      End
      Begin VB.TextBox txtStrikePrice 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   11
         Top             =   2280
         Width           =   1575
      End
      Begin VB.TextBox txtInterestRate 
         Alignment       =   2  'Center
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   10
         Top             =   1200
         Width           =   1575
      End
      Begin VB.TextBox txtOptionName 
         Alignment       =   2  'Center
         CausesValidation=   0   'False
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   9
         Top             =   840
         Width           =   1575
      End
      Begin VB.TextBox txtObjectName 
         Alignment       =   2  'Center
         CausesValidation=   0   'False
         Height          =   285
         Index           =   0
         Left            =   1800
         TabIndex        =   8
         Top             =   480
         Width           =   1575
      End
      Begin VB.Line Line3 
         X1              =   1560
         X2              =   1560
         Y1              =   3600
         Y2              =   3960
      End
      Begin VB.Line Line2 
         X1              =   1680
         X2              =   1560
         Y1              =   3960
         Y2              =   3960
      End
      Begin VB.Line Line1 
         X1              =   1680
         X2              =   1560
         Y1              =   3600
         Y2              =   3600
      End
      Begin VB.Label Label1 
         Caption         =   "Volatilitet"
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   16
         Top             =   3840
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Optionspris"
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   15
         Top             =   3480
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Optionstyp"
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   13
         Top             =   3000
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Underliggande (kr)"
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   7
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "L�senpris (kr)"
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   6
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "L�sendag"
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   5
         Top             =   1920
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Ber�kningsdag"
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   4
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "R�nta (%)"
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   3
         Top             =   1200
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Option (namn,l�sen)"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   840
         Width           =   1455
      End
      Begin VB.Label Label1 
         Caption         =   "Aktie (namn)"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmOptionsInput"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
  Dim AllDataOk As Boolean
  
  CheckIfAllNecessaryData AllDataOk
  
  If AllDataOk Then
    'L�gg till fr�ga om vad som skall ber�knas om b�da angivits.
    If txtOptionPrice(0).Text <> "" Then
      CalcOptionVolatility
      Unload Me
    Else
      CalcOptionValue
      Unload Me
    End If
  Else
    MsgBox CheckLang("Datumangivelsen �r fel eller s� saknas data f�r att ber�knigen skall kunna genomf�ras. Komplettera."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  
  
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Height = 5610
  Width = 3885
  top = 300
  left = 3000
 ' txtCalculationDate(0).Text = Date
  DTPicker1.Value = Date
  DTPicker2.Value = Date
End Sub

Private Sub txtCalculationDate_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtInterestRate_Validate(Index As Integer, Cancel As Boolean)
  Dim OK As Boolean
  If frmOptionsInput.txtInterestRate(0).Text <> "" Then
  VerifySingleInput OK, frmOptionsInput.txtInterestRate(0).Text
  If Not OK Then
    MsgBox CheckLang("R�ntan du matat in �r inget korrekt tal."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  Else
    Cancel = False
  End If
  End If
End Sub

Private Sub txtObjectName_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtObjectPrice_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtObjectPrice_Validate(Index As Integer, Cancel As Boolean)
  Dim OK As Boolean
  If frmOptionsInput.txtObjectPrice(0).Text <> "" Then
  VerifySingleInput OK, frmOptionsInput.txtObjectPrice(0).Text
  If Not OK Then
    MsgBox CheckLang("Aktiekursen du matat in �r inget korrekt tal."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  Else
    Cancel = False
  End If
  End If
End Sub

Private Sub txtOptionName_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtOptionPrice_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtOptionPrice_Validate(Index As Integer, Cancel As Boolean)
  Dim OK As Boolean
  If frmOptionsInput.txtOptionPrice(0).Text <> "" Then
  VerifySingleInput OK, frmOptionsInput.txtOptionPrice(0).Text
  If Not OK Then
    MsgBox CheckLang("Optionspriset du matat in �r inget korrekt tal."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  Else
    Cancel = False
  End If
  End If
End Sub

Private Sub txtStrikePrice_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtStrikePrice_Validate(Index As Integer, Cancel As Boolean)
  Dim OK As Boolean
  If frmOptionsInput.txtStrikePrice(0).Text <> "" Then
  VerifySingleInput OK, frmOptionsInput.txtStrikePrice(0).Text
  If Not OK Then
    MsgBox CheckLang("L�senpriset du matat in �r inget korrekt tal."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  Else
    Cancel = False
  End If
  End If
End Sub


Private Sub txtVolatility_GotFocus(Index As Integer)
  TextSelected
End Sub

Private Sub txtVolatility_Validate(Index As Integer, Cancel As Boolean)
  Dim OK As Boolean
  If frmOptionsInput.txtVolatility(0).Text <> "" Then
  VerifySingleInput OK, frmOptionsInput.txtVolatility(0).Text
  If Not OK Then
    MsgBox CheckLang("Volatiliteten du matat in �r inget korrekt tal."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  Else
    Cancel = False
  End If
  End If
End Sub
