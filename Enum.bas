Attribute VB_Name = "Enum"
Option Explicit

Public Enum DWMEnum
  dwmDaily = 1
  dwmWeekely = 2
  dwmMonthly = 3
End Enum

' Used in function GetGain (Module: PriceDataOperations)
Public Enum GetGainPeriodEnum
  ggpDay = 0
  ggpWeek = 1
  ggpMonth = 2
  ggpYear = 3
End Enum

Public Enum SetupEnum
  supNone = 0
  supKeyReversalDay = 1
  supReversalDay = 2
  supOneDayReversal = 3
  supTwoDayReversal = 4
  supPatternGap = 5
  supReversalGap = 6
End Enum
