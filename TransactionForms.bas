Attribute VB_Name = "TransactionForms"
Option Explicit
Public Sub UpdateReportGrid()

Dim temp As String
Dim LatestDate As Date

  On Error Resume Next
  LatestDate = Frm_RepList.DTPicker1.Value
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum."
    Frm_RepList.DTPicker1.SetFocus
    Err.Clear
    Exit Sub
  End If
  On Error GoTo 0

Frm_RepList.Grid1.Redraw = False

If TransactionArraySize >= 1 Then
  If TransactionArray(1).Date <= LatestDate Then
    temp = Trim(Frm_RepList.Combo1.Text)
    Select Case temp
      Case "Innehav"
        InitInnehav
      Case "Transaktioner"
        InitTransaktioner
      'Case "Realiserade V/F"
      '  InitRealiseradeVF
      'Case "Resultat"
      '  InitResultat
      Case "Deklarationsunderlag"
        InitDeklaration
    End Select
    CalcOverhead
  End If
Else
  Frm_RepList.Grid1.Rows = 1
  CalcOverhead
End If

With Frm_RepList
If .Combo1.Text = "Transaktioner" And TransactionArraySize > 0 Then
  .cbChangeTransaction.Enabled = True
  .cbDeleteTransaction.Enabled = True
Else
  .cbChangeTransaction.Enabled = False
  .cbDeleteTransaction.Enabled = False
End If
End With

Frm_RepList.Grid1.Redraw = True

End Sub

Public Sub InitTransaktioner()

  With Frm_RepList.Grid1
    .Cols = 7
    .ColWidth(0) = 500
    .ColWidth(1) = 1000
    .ColWidth(2) = 1300
    .ColWidth(3) = 2300
    .ColWidth(4) = 1100
    .ColWidth(5) = 1500
    .ColWidth(6) = 1150
    .Rows = 1
    .Row = 0
    .Col = 0
    .Text = "Nr"
    .Col = 1
    .Text = "Datum"
    .Col = 2
    .Text = "Typ"
    .Col = 3
    .Text = "Objekt"
    .Col = 4
    .Text = "Antal"
    .Col = 5
    .Text = "Belopp"
    .Col = 6
    .Text = "Courtage"
    
    UpdateGridTransaktioner
    
    'CalcOverhead
    
    If .Rows > 10 Then
      .ColWidth(6) = 895
    End If
  End With
End Sub
Public Sub InitInnehav()
  With Frm_RepList.Grid1
    .Cols = 7
    .ColWidth(0) = 1700
    .ColWidth(1) = 950
    .ColWidth(2) = 1200
    .ColWidth(3) = 1200
    .ColWidth(4) = 1200
    .ColWidth(5) = 1300
    .ColWidth(6) = 1000
    .Rows = 1
    .Row = 0
    .Col = 0
    .Text = "Objekt"
    .Col = 1
    .Text = "Antal"
    .Col = 2
    .Text = "K�pkurs"
    .Col = 3
    .Text = "Dagskurs"
    .Col = 4
    .Text = "Markn.v�rde"
    .Col = 5
    .Text = "Oreal. V/F"
    .Col = 6
    .Text = "Tillv�xt"
    
    UpdateGridInnehav
    
    'CalcOverhead
    
    If .Rows > 10 Then
      .ColWidth(6) = 695
    End If
  End With
End Sub

Public Sub InitDeklaration()
  With Frm_RepList.Grid1
    .Cols = 8
    .ColWidth(0) = 1700
    .ColWidth(1) = 1300
    .ColWidth(2) = 800
    .ColWidth(3) = 1000
    .ColWidth(4) = 1000
    .ColWidth(5) = 800
    .ColWidth(6) = 1000
    .ColWidth(7) = 1250
    .Rows = 1
    .Row = 0
    .Col = 0
    .Text = "Objekt"
    .Col = 1
    .Text = "Antal"
    .Col = 2
    .Text = "K�pkurs"
    .Col = 3
    .Text = "Investerat"
    .Col = 4
    .Text = "S�ljdatum"
    .Col = 5
    .Text = "S�ljkurs"
    .Col = 6
    .Text = "Fsg.int�kt"
    .Col = 7
    .Text = "Resultat"
    
    UpdateGridDeklaration
    
    'CalcOverhead
    
    If .Rows > 10 Then
      .ColWidth(7) = 995
    End If
  End With
End Sub

Public Sub UpdateGridTransaktioner()
Dim i As Long
Dim TotalCash As Single
Dim LatestDate As Date
Dim j As Long
Dim ExistsInPortfolio As Boolean

  On Error Resume Next
  LatestDate = Frm_RepList.DTPicker1.Value
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum."
    Frm_RepList.DTPicker1.SetFocus
    Err.Clear
    Exit Sub
  End If
  On Error GoTo 0

With Frm_RepList.Grid1
  For i = 1 To TransactionArraySize
  If TransactionArray(i).Date <= LatestDate Then
    .Rows = .Rows + 1
    .Row = i
    .Col = 0
    .Text = .Rows - 1
    .Col = 1
    .Text = TransactionArray(i).Date
    .Col = 2
    If TransactionArray(i).IsBuySell Then
      If TransactionArray(i).IsBuy Then
        .Text = "K�p"
        TotalCash = TotalCash - TransactionArray(i).Price * TransactionArray(i).Volume - TransactionArray(i).Courtage
      Else
        .Text = "S�lj"
        TotalCash = TotalCash + TransactionArray(i).Price * TransactionArray(i).Volume - TransactionArray(i).Courtage
      End If
      .Col = 3
      ExistsInPortfolio = False
      For j = 1 To PortfolioArraySize
        If PortfolioArray(j).Name = TransactionArray(i).Name Then
          ExistsInPortfolio = True
        End If
      Next j
      If ExistsInPortfolio Then
        .Text = TransactionArray(i).Name
      Else
        .Text = Trim(TransactionArray(i).Name) + "*"
      End If
      '.Col = 3
      '.Text = TransactionArray(i).Name
      .Col = 4
      .Text = TransactionArray(i).Volume
      .Col = 5
      .Text = Trim(Str$(TransactionArray(i).Price * TransactionArray(i).Volume))
      .Col = 6
      .Text = FormatNumber(TransactionArray(i).Courtage, 2)
    Else
      .Text = TransactionArray(i).DepositType
      If TransactionArray(i).DepositType = "Utdelning" Then
        .Col = 3
        .Text = TransactionArray(i).Name
      End If
      .Col = 5
      If TransactionArray(i).IsDeposit Then
        .Text = FormatNumber(TransactionArray(i).Cash, 2)
        TotalCash = TotalCash + TransactionArray(i).Cash
      Else
        .Text = "-" + FormatNumber(TransactionArray(i).Cash, 2)
        TotalCash = TotalCash - TransactionArray(i).Cash
      End If
    End If
  Else
    i = TransactionArraySize
  End If
  Next i
End With
End Sub

Public Sub UpdateGridInnehav()
Dim i As Long
Dim Volume As Long
Dim AveragePrice As Single
Dim Price As Single
Dim Value As Single
Dim Result As Single
Dim TmpString As String
Dim LatestDate As Date
Dim Profit As Single

  On Error Resume Next
  LatestDate = Frm_RepList.DTPicker1.Value
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum."
    Frm_RepList.DTPicker1.SetFocus
    Err.Clear
    Exit Sub
  End If
  On Error GoTo 0

With Frm_RepList.Grid1
For i = 1 To PortfolioArraySize
  Volume = GetVolume(PortfolioArray(i).Name, LatestDate)
  If Volume <> 0 Then
    AveragePrice = GetAveragePrice(PortfolioArray(i).Name, LatestDate, False)
    Price = GetPrice(LatestDate, PortfolioArray(i).Name)
    Value = Price * Volume
    Result = Value - Volume * AveragePrice
    If AveragePrice > 0 And Volume > 0 Then
      Profit = Result / (AveragePrice * Volume) * 100
    Else
      Profit = 0
    End If
    .Rows = .Rows + 1
    .Row = .Rows - 1
    .Col = 0
    .Text = PortfolioArray(i).Name
    .Col = 1
    .Text = FormatNumber(Volume, 2)
    .Col = 2
    .Text = FormatNumber(AveragePrice, 2)
    .Col = 3
    .Text = FormatNumber(Price, 2)
    .Col = 4
    .Text = FormatNumber(Value, 2)
    .Col = 5
    .Text = FormatNumber(Result, 2)
    .Col = 6
    .Text = FormatNumber(Profit, 2)
  End If
  Next i
End With
End Sub

Public Sub UpdateGridDeklaration()
Dim i As Long
Dim j As Long
Dim Volume As Long
Dim AveragePrice As Single
Dim Price As Single
Dim Value As Single
Dim Result As Single
Dim TmpString As String
Dim LatestDate As Date
Dim FirstDate As Long
Dim TransactionYear As Long
Dim TotalPositiveResult As Single
Dim TotalNegativeResult As Single


  On Error Resume Next
  LatestDate = Frm_RepList.DTPicker1.Value
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum."
    Frm_RepList.DTPicker1.SetFocus
    Err.Clear
    Exit Sub
  End If
  On Error GoTo 0
  
With Frm_RepList.Grid1
FirstDate = Year(LatestDate)
For i = 1 To PortfolioArraySize
  For j = 1 To TransactionArraySize
    If Trim(PortfolioArray(i).Name) = Trim(TransactionArray(j).Name) Then
      TransactionYear = Year(TransactionArray(j).Date)
      If Not TransactionArray(j).IsBuy And TransactionYear = FirstDate And TransactionArray(j).Date <= LatestDate And Not Trim(TransactionArray(j).DepositType) = "Utdelning" Then
        AveragePrice = GetAveragePrice(PortfolioArray(i).Name, TransactionArray(j).Date, True)
        Result = (TransactionArray(j).Price - AveragePrice) * TransactionArray(j).Volume - TransactionArray(j).Courtage
        If Result >= 0 Then
          TotalPositiveResult = TotalPositiveResult + Result
        Else
          TotalNegativeResult = TotalNegativeResult + Result
        End If
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .Text = PortfolioArray(i).Name
        .Col = 1
        TmpString = Str$(TransactionArray(j).Volume)
        .Text = Format(PointToComma(TmpString), ".00")
        .Col = 2
        TmpString = Str$(AveragePrice)
        .Text = Format$(PointToComma(TmpString), ".00")
        .Col = 3
        TmpString = Str$(AveragePrice * TransactionArray(j).Volume)
        .Text = Format$(PointToComma(TmpString), ".00")
        .Col = 4
        .Text = TransactionArray(j).Date
        .Col = 5
        TmpString = Str$(TransactionArray(j).Price - TransactionArray(j).Courtage / TransactionArray(j).Volume)
        .Text = Format$(PointToComma(TmpString), ".00")
        .Col = 6
        TmpString = Str$(TransactionArray(j).Price * TransactionArray(j).Volume - TransactionArray(j).Courtage)
        .Text = Format$(PointToComma(TmpString), ".00")
        .Col = 7
        TmpString = Str$(Result)
        .Text = Format$(PointToComma(TmpString), ".00")
      End If
    End If
  Next j
Next i

.Rows = .Rows + 5
.Row = .Rows - 4
.Col = 0
.CellFontBold = True
TmpString = "Totalt " + Str$(FirstDate)
.Text = TmpString
.Row = .Row + 1
.Col = 0
.CellFontBold = True
.Text = "Reavinster"
.Col = 1
.CellFontBold = True
TmpString = Str$(TotalPositiveResult)
.Text = Format$(PointToComma(TmpString), ".00")
.Row = .Row + 1
.Col = 0
.CellFontBold = True
.Text = "Reaf�rluster"
.Col = 1
.CellFontBold = True
TmpString = Str$(TotalNegativeResult)
.Text = Format$(PointToComma(TmpString), ".00")
.Row = .Row + 1
.Col = 0
.CellFontBold = True
.Text = "Totalt"
.Col = 1
.CellFontBold = True
TmpString = Str$(TotalPositiveResult + TotalNegativeResult)
.Text = Format$(PointToComma(TmpString), ".00")
End With
End Sub

Public Sub CalcOverhead()
Dim i As Long
Dim LatestDate As Date
Dim TmpString As String
Dim TotalInsatt As Single
Dim Saldo As Single

  
  LatestDate = Frm_RepList.DTPicker1.Value
  
  If TransactionArraySize = 0 Then
    Saldo = 0
    TotalInsatt = 0
  Else
    For i = 1 To TransactionArraySize
      If TransactionArray(i).Date <= LatestDate Then
        If TransactionArray(i).IsBuySell Then
          If TransactionArray(i).IsBuy Then
            Saldo = Saldo - TransactionArray(i).Price * TransactionArray(i).Volume - TransactionArray(i).Courtage
          Else
            Saldo = Saldo + TransactionArray(i).Price * TransactionArray(i).Volume - TransactionArray(i).Courtage
          End If
        Else
          If TransactionArray(i).IsDeposit Then
            If Trim(TransactionArray(i).DepositType) = "Ins�ttning" Then
              Saldo = Saldo + TransactionArray(i).Cash
              TotalInsatt = TotalInsatt + TransactionArray(i).Cash
            Else
              Saldo = Saldo + TransactionArray(i).Cash
            End If
          Else
            If Trim(TransactionArray(i).DepositType) = "Uttag" Then
              Saldo = Saldo - TransactionArray(i).Cash
              TotalInsatt = TotalInsatt - TransactionArray(i).Cash
            Else
              Saldo = Saldo - TransactionArray(i).Cash
            End If
          End If
        End If
      End If
    Next i
  End If
  
  With PortfolioOverhead
  .TotalCash = Saldo
  .MarketValue = GetTotalValue(LatestDate)
  .PortfolioValue = .TotalCash + .MarketValue
  'If TotalInsatt > 0 Then
  '  .PortfolioProfit = ((.PortfolioValue - TotalInsatt) / TotalInsatt) * 100
  'Else
  '  .PortfolioProfit = 0
  'End If
  
  'TmpString = Str$(.TotalCash)
  Frm_RepList.labTotalCash = FormatNumber(.TotalCash, 2)
  
  'TmpString = Str$(.MarketValue)
  Frm_RepList.labMarketValue = FormatNumber(.MarketValue, 2)
  
  'TmpString = Str$(.PortfolioValue)
  Frm_RepList.labPortfolioValue = FormatNumber(.PortfolioValue, 2)
  
  'TmpString = Str$(.PortfolioProfit)
  'Frm_RepList.labPortfolioProfit = Format$(PointToComma(TmpString), ".00")
  End With
End Sub
