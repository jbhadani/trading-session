VERSION 5.00
Begin VB.Form frmPrintHidden 
   Caption         =   "PrvTEST"
   ClientHeight    =   5895
   ClientLeft      =   1305
   ClientTop       =   1530
   ClientWidth     =   10350
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   5895
   ScaleWidth      =   10350
   Begin VB.ListBox List1 
      Height          =   5325
      ItemData        =   "frmPrintHidden.frx":0000
      Left            =   5760
      List            =   "frmPrintHidden.frx":0016
      TabIndex        =   3
      Top             =   360
      Width           =   4545
      Visible         =   0   'False
   End
   Begin VB.TextBox Text1 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   7.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5376
      Left            =   0
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   2
      Text            =   "frmPrintHidden.frx":00C1
      Top             =   324
      Width           =   5700
   End
   Begin VB.PictureBox MyImg 
      Height          =   1920
      Left            =   2844
      Picture         =   "frmPrintHidden.frx":05D1
      ScaleHeight     =   1860
      ScaleWidth      =   2430
      TabIndex        =   1
      Top             =   7452
      Width           =   2496
      Visible         =   0   'False
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Print (do preview)"
      Default         =   -1  'True
      Height          =   300
      Left            =   0
      TabIndex        =   0
      ToolTipText     =   "print preview"
      Top             =   0
      Width           =   4044
   End
End
Attribute VB_Name = "frmPrintHidden"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

' PrvTEST - �NewMedia 1998
' http://www.chiappori.net
' newmedia@chiappori.net
' PrvTEST is a VB5 code to make printer previews
' The code below is the event connected to
' 'print' button in main form.
'
' e-mail me to receive full (free) source code
'
Private Sub Command1_Click()

    ' local variables
    Static l As Integer, rPnt As Integer, A As String
    rPnt = 1

    ' initialize preview engine
    TempInit
    
' page 1
    ' start with a new page
    PrintHeader "DEMO", "PrvTEST - 1", 0
    ' only to print something...
    PrintInCen 9, 1.5, "Page 1 - VB Code", "Arial", 12, True, 0
    For l = 0 To List1.ListCount - 1
        PrintInLef 2, 2 + (0.5 * l), List1.List(l), "Courier New", 8, False, 0
    Next
    ' close open page
    PrintFooter "http://www.chiappori.net", "newmedia@chiappori.net", 0
    
' page 2
    ' start with a new page
    PrintHeader "DEMO", "PrvTEST - 2", 0
    ' only to print something...
    PrintInCen 9, 1.5, "Page 2 - Image", "Arial", 12, True, 0
    PrintJust 3, 3, "Something to print...", "Times New Roman", 10, False, 5, 0
    ' PrvTEST can handle also images...
    PrintImg MyImg, 2, 8, 8, 6, 0
    ' close open page
    PrintFooter "http://www.chiappori.net", "newmedia@chiappori.net", 0

End Sub

Private Sub Form_Resize()
    Command1.Left = 0
    Command1.Top = 0
    Command1.Width = frmPrintHidden.ScaleWidth
    
    Text1.Left = 0
    Text1.Top = Command1.Height
    Text1.Width = Command1.Width
    Text1.Height = frmPrintHidden.ScaleHeight - Text1.Top
End Sub

Private Sub Form_Unload(Cancel As Integer)
    TempInit
End Sub


