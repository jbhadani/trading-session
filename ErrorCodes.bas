Attribute VB_Name = "ErrorProc"
Sub ErrorMessageStr(ErrorMessage As String)
  MsgBox CheckLang(ErrorMessage), 48, CheckLang("Felmeddelande")
End Sub

Function YesNo(Content As String) As Boolean
  Dim Result As Long
  Result = MsgBox(CheckLang(Content), 36, "")
  If Result = 6 Then
    YesNo = True
  Else
    YesNo = False
  End If
End Function

Function YesNoCancel(Contents As String, Title As String) As Long
  YesNoCancel = MsgBox(CheckLang(Contents), 35, CheckLang(Title))
End Function

Sub InformationalMessage(Content As String)
  MsgBox CheckLang(Content), 64, "Information"
End Sub

Public Sub VerifyLong(ToBeTested As Object, Errorfound As Boolean)
  Dim A As Long
 
  On Error Resume Next
  A = CLng(ToBeTested.Text)
  If Err.Number <> 0 Then
    Err.Clear
    ToBeTested.SetFocus
    Errorfound = True
  End If
End Sub

Public Sub VerifyIntegerPara(ToBeTested As Object, Scroll As Object, TextMessage As String, Message As String, Errorfound As Boolean)
  Dim A As Long
  Dim Error As Boolean
  Trim Message
  On Error Resume Next
  A = CInt(ToBeTested.Text)
  If Err.Number <> 0 Then
    MsgBox CheckLang(Message) + ", " + CheckLang(TextMessage) + CheckLang(" �r inget heltal."), 16, CheckLang("Inget Heltal")
    Err.Clear
    ToBeTested.SetFocus
    ToBeTested.Selected
    Error = True
    Errorfound = True
  End If
  OutLONG = A
    If A <= Scroll.Max And A > 0 Then
      Scroll.Value = A
      ToBeTested.Text = A
    ElseIf (A > Scroll.Max Or A <= 0) And Not Error Then
      MsgBox CheckLang(TextMessage) & CheckLang(" �r fel."), 16, CheckLang("Felmeddelande")
      ToBeTested.SetFocus
      Errorfound = True
    End If
End Sub
Public Sub VerifyLongPara(ToBeTested As Object, Scroll As Object, Errorfound As Boolean)
  Dim A As Long
  Dim Error As Boolean
  Dim Message As String
  Dim Max As Long
  Dim Min As Long
  
  On Error Resume Next
  A = CLng(ToBeTested.Text)
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste mata in ett korrekt heltal."), 16, CheckLang("Inget Heltal")
    Err.Clear
    ToBeTested.SetFocus
    ToBeTested.Selected
    Error = True
    Errorfound = True
  ElseIf A > Scroll.Max Or A < Scroll.Min Then
    Min = Scroll.Min
    Max = Scroll.Max
    Message = "Du har angivit ett f�r stort eller f�r litet v�rde. Min = " + Str$(Min) + ", Max = " + Str$(Max) + "."
    MsgBox CheckLang(Message), 16, CheckLang("Felmeddelande")
    ToBeTested.SetFocus
    Errorfound = True
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          VerifySinglePara
'Input:         ToBeTested, Scroll, TextMessage, Message
'Output:        Errorfound
'
'Description:   Kontrollerar s� att v�rdet �r ett korrekt
'               decimaltal.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till funktionalitet f�r att g�ra
'                      kontroll p� Bollingerparametrar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub VerifySinglePara(ToBeTested As Object, Errorfound As Boolean)
  Dim A As Single
  Dim Error As Boolean
  
  On Error Resume Next
  A = CSng(ToBeTested.Text)
  
  If Err.Number <> 0 Then
    MsgBox CheckLang("Du m�ste ange ett korrekt decimaltaltal."), 16, CheckLang("Inget Decimaltaltal")
    Err.Clear
    ToBeTested.SetFocus
    ToBeTested.Selected
    Error = True
    Errorfound = True
  End If
  
End Sub
Public Sub VerifySingle(ToBeTested As Object, Scroll As Object, label As Object, Message As String, Errorfound As Boolean, outsingle As Single)
  Dim A As Single
  Dim Error As Boolean
  Trim Message
  On Error Resume Next
  A = CSng(ToBeTested.Text)
  If Err.Number <> 0 Then
    MsgBox CheckLang(Message) + CheckLang(" �r inget decimaltal."), 16, CheckLang("Inget Decimaltal")
    Err.Clear
    ToBeTested.SetFocus
    ToBeTested.Selected
    Error = True
    Errorfound = True
  End If
  outsingle = A
    If A <= Scroll.Max And A > 0 Then
      Scroll.Value = A * 100
      ToBeTested.Text = A
    ElseIf (A > Scroll.Max Or A <= 0) And Not Error Then
      MsgBox label & CheckLang(" �r fel."), 16, CheckLang("Felmeddelande")
      ToBeTested.SetFocus
      Errorfound = True
    End If
End Sub
Public Sub VerifyLongLONG(ToBeTested As Object, Scroll As Object, TextMessage As Object, Message As String, Errorfound As Boolean, OutLongLONG As Long, Number As Long)
  Dim A As Long
  Dim Error As Boolean
  Trim Message
  On Error Resume Next
  A = CLng(frmOptimering.tbInvBelopp.Text)
  If Err.Number <> 0 Then
    MsgBox CheckLang(Message) + CheckLang(" �r inget heltal."), 16, CheckLang("Inget Heltal")
    Err.Clear
    ToBeTested.SetFocus
    ToBeTested.Selected
    Error = True
    Errorfound = True
  End If
  OutLongLONG = A
    If A <= Scroll.Max * 100 And A > 0 Then
      Scroll.Value = A / 100
      ToBeTested.Text = A
    ElseIf (A > Scroll.Max * 100 Or A <= 0) And Not Error Then
      MsgBox CheckLang(TextMessage.Caption) & " " & CheckLang("�r fel."), 16, CheckLang("Felmeddelande")
      ToBeTested.SetFocus
      Errorfound = True
    End If
End Sub

Public Sub ErrorCheckLengthOfParameters(FormParameters As Object, Errorfound As Boolean)
  With frmParameters
    If Val(.Para_MAV_Period1) > Val(.Para_MAV_Period2) Then
      Errorfound = True
      ErrorMessageStr "Det korta medelv�rdet m�ste vara kortare �n det l�nga."
      .Para_MAV_Period1.SetFocus
    End If
    If Val(.Tb_PriceOsc_MAV1) > Val(.Tb_PriceOsc_MAV2) Then
      Errorfound = True
      ErrorMessageStr "Det korta medelv�rdet m�ste vara kortare �n det l�nga."
      .Tb_PriceOsc_MAV1.SetFocus
    End If
  End With
End Sub

Public Sub VerifySingleInput(OK As Boolean, ToBeTested As String)
  Dim temp As String
  Dim A As Single
  
  temp = PointToComma(ToBeTested)
  
  On Error Resume Next
  A = CSng(temp)
  If Err.Number <> 0 Then
    OK = False
  Else
    OK = True
  End If
  Err.Clear
  
End Sub

Public Sub VerifyDateInput(OK As Boolean, ToBeTested As String)
  Dim temp As String
  Dim A As Date
  
  temp = ToBeTested
  On Error Resume Next
  A = CDate(temp)
  If Err.Number <> 0 Then
    OK = False
  Else
    OK = True
  End If
  Err.Clear
  
End Sub
