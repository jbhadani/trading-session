VERSION 5.00
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   3090
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3090
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
  'File
Begin VB.Menu ID_NewPortfolio
	Caption         =   "NewPortfolio"
END
Begin VB.Menu ID_OpenPortfolio"
	Caption         =   "OpenPortfolio"
END
Begin VB.Menu ID_SavePortfolio"
	Caption         =   "SavePortfolio"
END
Begin VB.Menu ID_SavePortfolioAs"
	Caption         =   "SavePortfolioAs"
  
  'Tools
END
Begin VB.Menu ID_Search"    
	Caption         =   "Search"
END
Begin VB.Menu ID_Test"    
	Caption         =   "Test"
END
Begin VB.Menu ID_Optimize"    
	Caption         =   "Optimize"
  
  ' Help menu
END
Begin VB.Menu ID_HelpContents"    
	Caption         =   "HElp Contents"
END
Begin VB.Menu ID_About"    
	Caption         =   "About"
END
Begin VB.Menu ID_CheckForUpdates"    
	Caption         =   "CheckForUpdates"
  
  'Graph
END
Begin VB.Menu ID_ZoomIn"
	Caption         =   "Zoom In"
END
Begin VB.Menu ID_ZoomOut"
	Caption         =   "Zoom Out"
END
Begin VB.Menu ID_IncreaseDayWidth"
	Caption         =   "Increase Day Width"
END
Begin VB.Menu ID_DecreaseDayWidth"
	Caption         =   "Decrease Day Width"
END
Begin VB.Menu ID_Graph"
	Caption         =   "Graph"
END
Begin VB.Menu ID_Quotes"
	Caption         =   "Quotes"
END
Begin VB.Menu ID_DayQuotes"
	Caption         =   "Day Quotes"
END
Begin VB.Menu ID_DayQuotesSettings"
	Caption         =   "Day Quotes Settings"
END
Begin VB.Menu ID_ColorSettings"
	Caption         =   "Color Settings"
END
Begin VB.Menu ID_GraphSettings"
	Caption         =   "Graph Settings"
END
Begin VB.Menu ID_Report"
	Caption         =   "Report"
END
Begin VB.Menu ID_ParameterSettings"
	Caption         =   "Parameter Settings"
END
Begin VB.Menu ID_Exit"
	Caption         =   "Exit"
END
Begin VB.Menu ID_Update"
	Caption         =   "Update"
END
Begin VB.Menu ID_FishNetSettings"
	Caption         =   "Fish Net Settings"
END
Begin VB.Menu ID_FishNetEnabled
	Caption         =   "Fish Net Enabled"
END
Begin VB.Menu ID_Trigger
	Caption         =   "Trigger"
END
Begin VB.Menu ID_Daily
	Caption         =   "Daily"
END
Begin VB.Menu ID_Weekely
	Caption         =   "Weekly"
END
Begin VB.Menu ID_Monthly
	Caption         =   "Monthly"
END
Begin VB.Menu ID_Option
	Caption         =   "Option"
END
Begin VB.Menu ID_Ind1"
	Caption         =   "Ind1"
END
Begin VB.Menu ID_Ind2
	Caption         =   "Ind2"
END
Begin VB.Menu ID_ShowVolume
	Caption         =   "Show Volume"
END
Begin VB.Menu ID_ShowBollinger
	Caption         =   "Show Bollinger"
END
Begin VB.Menu ID_ShowParabolic
	Caption         =   "Show Parabolic"
END
Begin VB.Menu ID_ShowColorSignalling
	Caption         =   "Show Color Signalling"
END
Begin VB.Menu ID_ShowHLC
	Caption         =   "Show HLC"
END
Begin VB.Menu ID_Settings"
	Caption         =   "Settings"
END
Begin VB.Menu ID_TLDraw"
	Caption         =   "Draw"
END
Begin VB.Menu ID_TLDrawHor"
	Caption         =   "Draw Hor"
END
Begin VB.Menu ID_TLDrawVer"
	Caption         =   "Draw Ver"
END
Begin VB.Menu ID_TLMove"
	Caption         =   "Move"
END
Begin VB.Menu ID_TLRemove"
	Caption         =   "Remove"
END
Begin VB.Menu ID_TLDrawParallel"
	Caption         =   "Draw Parallel"
END
Begin VB.Menu ID_TLRemoveAll"
	Caption         =   "Remove All"
END
Begin VB.Menu ID_TLSave"
	Caption         =   "Save"
END
Begin VB.Menu ID_TLText"
	Caption         =   "Text"
END
Begin VB.Menu ID_TLColorRed"
	Caption         =   "Color Red"
END
Begin VB.Menu ID_TLColorYellow"
	Caption         =   "Color Yellow"
END
Begin VB.Menu ID_TLColorGreen"
	Caption         =   "Color Green"
END
Begin VB.Menu ID_TLColorBlue"
	Caption         =   "Color Blue"
END
Begin VB.Menu ID_TLColorCyan"
	Caption         =   "Color Cyan"
END
Begin VB.Menu ID_TLColorMagneta"
	Caption         =   "Color Magneta"
END
Begin VB.Menu ID_TLColorGrey"
	Caption         =   "Color Grey"
END
Begin VB.Menu ID_TLColorBlack"
	Caption         =   "Color Black"
END
Begin VB.Menu ID_GraphCloseWidthOnBar"
	Caption         =   "Graph Close Width On Bar"
END
Begin VB.Menu ID_GraphCloseWidthOne"
	Caption         =   "Graph Close Width One"
END
Begin VB.Menu ID_GraphCloseWidthTwo"
	Caption         =   "Graph Close Width Two"
END
Begin VB.Menu ID_StockTransaction"
	Caption         =   "Stock Transaction"
END
Begin VB.Menu ID_DepositWithdrawal"
	Caption         =   "Deposit Withdrawal"
END
Begin VB.Menu ID_Dividend"
	Caption         =   "Dividend"
END
Begin VB.Menu ID_ScaleLineHorizontal"
	Caption         =   "Scale Line Horizontal"
END
Begin VB.Menu ID_ScaleLineMinHorizontal"
	Caption         =   "Scale Line Min Horizontal"
END
Begin VB.Menu ID_ScaleLineVertical"
	Caption         =   "Scale Line Vertical"
END
Begin VB.Menu ID_AddObjectToPortfolio"
	Caption         =   "Add Object To Portfolio"
END
Begin VB.Menu ID_GraphScaleLinear"
	Caption         =   "Graph Scale Linear"
END
Begin VB.Menu ID_GraphScaleExponential"
	Caption         =   "Graph Scale Exponential"
END
Begin VB.Menu ID_ShowHighlight"
	Caption         =   "Show Highlight"
END
Begin VB.Menu ID_RemoveAllHighlights"
	Caption         =   "Remove All Highlights"
END
Begin VB.Menu ID_ShowMAV1"
	Caption         =   "Show MAV1"
END
Begin VB.Menu ID_ShowMAV2"
	Caption         =   "Show MAV2"
END
Begin VB.Menu ID_Information"
	Caption         =   "Information"
END
Begin VB.Menu ID_ShowKeyReversalDay"
	Caption         =   "Show Key Reversal Day"
END
Begin VB.Menu ID_ShowReversalDay"
	Caption         =   "Show Reversal Day"
END
Begin VB.Menu ID_ShowOneDayReversal"
	Caption         =   "Show One Day Reversal"
END
Begin VB.Menu ID_ShowTwoDayReversal"
	Caption         =   "Show Two Day Reversal"
END
Begin VB.Menu ID_ShowPatternGap"
	Caption         =   "Show Pattern Gap"
END
Begin VB.Menu ID_ShowReversalGap"
Caption         =   "Show Reversal Gap"
END
Begin VB.Menu ID_ShowSupportResistance"
Caption         =   "Show Support Resistance"
END
Begin VB.Menu ID_Print"
Caption         =   "Print"
END
Begin VB.Menu ID_Preview"
Caption         =   "Preview"
END
Begin VB.Menu ID_PageLayout"
Caption         =   "Page Layout"
END
Begin VB.Menu ID_NextObject"
Caption         =   "Next Object"
END
Begin VB.Menu ID_PrevObject"
Caption         =   "Prev Object"
END
Begin VB.Menu ID_ToolbarSettings"
Caption         =   "Toolbar Settings"
END
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
