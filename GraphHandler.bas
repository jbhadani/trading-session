Attribute VB_Name = "GraphHandler"
Option Explicit
  
  Dim MAV1() As Single
  Dim MAV2() As Single
  Dim SignalColorArray() As Boolean
  Dim IndSerie(1 To 2) As IndicatorSerieType
  Dim IndProp(1 To 2) As IndicatorPropertiesType
  Dim VolumeProperties As VolumePropertiesType
  Dim BollingerUpper() As Single
  Dim BollingerLower() As Single
  Dim BollingerMAV() As Single
  Dim Parabolic() As Single
  Dim ParabolicBuyOrSell() As Boolean
  Dim DrawArray() As PriceDataType
  Dim ComparePercentArray() As Single
  Dim strGainDay As String
  Dim strGainWeek As String
  Dim strGainMonth As String
  Dim strGainYear As String
  Dim lngSetupArray() As Long
 
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ProcessBollinger
'Input:
'Output:
'
'Description:   Calculate the values for the Bollinger arrays
'Author:        Fredrik Wendel
'Revision:      980104  Created
'               990727  Now uses the PDArray parameter
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
Sub ProcessBollinger(PDArray() As PriceDataType)
  BollingerBandCalc PDArray(), BollingerUpper(), BollingerLower(), BollingerMAV(), UBound(PDArray), False
  If GraphSettings.ExponentialPrice Then
      LogSingleArray BollingerUpper, UBound(PDArray)
      LogSingleArray BollingerLower, UBound(PDArray)
      LogSingleArray BollingerMAV, UBound(PDArray)
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ProcessParabolic
'Input:
'Output:
'
'Description:   Calculate the values for the Parabolic arrays
'Author:        Fredrik Wendel
'Revision:      980104  Created
'               990727  Now uses the PDArray parameter
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub ProcessParabolic(PDArray() As PriceDataType)
  ParabolicCalc PDArray(), Parabolic(), ParabolicBuyOrSell(), UBound(PDArray), False, ParameterSettings.AccelerationFactor
  If GraphSettings.ExponentialPrice Then
    LogSingleArray Parabolic, UBound(PDArray)
  End If
End Sub
Sub ProcessPriceMAVValues(PDArray() As PriceDataType)
  
  Dim Length1 As Long
  Dim Length2 As Long
  
  If GraphSettings.ShowPriceMAV1 = True Then
    ReDim MAV1(UBound(PDArray)) As Single
    If ParameterSettings.MAVUseExp Then
      ExpMAVCalc PDArray, MAV1, ParameterSettings.MAV1, UBound(PDArray)
    Else
      SimpleMAVCalc PDArray, MAV1, ParameterSettings.MAV1, UBound(MAV1), False
    End If
    If GraphSettings.ExponentialPrice Then
      LogSingleArray MAV1, UBound(MAV1)
    End If
  End If
   
  If GraphSettings.ShowPriceMAV2 = True Then
    ReDim MAV2(UBound(PDArray)) As Single
    If ParameterSettings.MAVUseExp Then
      ExpMAVCalc PDArray, MAV2, ParameterSettings.MAV2, UBound(PDArray)
    Else
      SimpleMAVCalc PDArray, MAV2, ParameterSettings.MAV2, UBound(MAV2), False
    End If
    If GraphSettings.ExponentialPrice Then
      LogSingleArray MAV2, UBound(MAV2)
    End If
  End If
  
   
   
 
End Sub
Sub ProcessPriceDataArray()
  
  Dim j As Long
  'ReDim PDArray(PriceDataArraySize)
  Select Case GraphSettings.DWM
  Case dwmDaily
    DrawArray() = PriceDataArray()
  Case dwmWeekely
    MakeWeekely PriceDataArray, DrawArray
  Case dwmMonthly
    MakeMonthly PriceDataArray, DrawArray
  End Select
  
  
  'For j = 1 To PriceDataArraySize
  '  DrawArray(j) = PriceDataArray(j)
  'Next j
  If GraphSettings.ExponentialPrice Then
    LogPriceDataArray DrawArray, UBound(DrawArray)
  End If
End Sub

#If 0 Then
Sub ProcessCompare(ByRef PDArray() As PriceDataType, ByRef ComparePercentArray() As Single, lngCompareIndex As Long)
Dim CompareArray() As PriceDataType

  R_PriceData_All DataPath & InvestInfoArray(lngCompareIndex).FileName & ".prc"
  
  'MakeSecondPriceArray PDArray, CompareArray, ComparePercentArray, 1, 100
End Sub
#End If

Sub ShowGraphForm()
  dbg.Enter "ShowGraphForm"
  If ShowGraph = False Then
    If PriceDataArraySize >= 2 Then
      If ActiveIndex <> 0 Then
        GraphInfoChanged = True
        DoUpdate = False
        ShowGraph = True
         With MainMDI
        .ID_Print.Enabled = True
        .ID_Preview.Enabled = True
        .Toolbar1.Buttons(5).Enabled = True
        .Toolbar1.Buttons(4).Enabled = True
        End With
        With Frm_OwnGraph
         .ScrollBar.Value = 1
         .top = 0
         .left = 0
         .Width = MainMDI.ScaleWidth
         .Height = MainMDI.ScaleHeight
         DoUpdate = True
        
         .Show
        End With
        
      End If
    Else
      InformationalMessage "Det m�ste finnas kurser f�r minst tv� dagar registrerade"
      MainMDI.ID_Graph.Checked = False
    End If
  Else
    Unload Frm_OwnGraph
    'ShowGraph = False
  End If
  dbg.Leave "ShowGraphForm"
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          SetIndicatorProperties
'Input:
'Output:
'
'Description:   Sets IndProp and IndSerie to appropriate values
'Author:        Fredrik Wendel
'Revision:      970905  Created
'               971218  Fixline added
'               971223  A more efficient code was implemented
'               980112  The index loop was replaced by an input
'                       parameter.
'               980227  A bug was fixed. Show both indicator 1&2
'                       will now work. ReDim statement were wrong
'               990830  Added "Volatilitet".
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub SetIndicatorProperties(Index As Byte, PDArray() As PriceDataType)

Dim strIndicator As String
ReDim IndSerie(Index).Serie1(UBound(PDArray)) As Single
ReDim IndSerie(Index).Serie2(UBound(PDArray)) As Single

  If Index = 1 Then
    strIndicator = GraphSettings.strIndicator1
  Else
    strIndicator = GraphSettings.strIndicator2
  End If

  With ParameterSettings
    IndProp(Index).ShowSerie2 = False
    IndProp(Index).Serie2name = ""
    Select Case strIndicator
    Case "Moving Average"
      If ParameterSettings.MAVUseExp Then
        ExpMAVCalc PDArray, IndSerie(Index).Serie1, .MAV1, UBound(PDArray)
        If ParameterSettings.MAVNbr = 1 Then
          IndProp(Index).ShowSerie2 = False
        Else
          ExpMAVCalc PDArray, IndSerie(Index).Serie2, .MAV2, UBound(PDArray)
          IndProp(Index).ShowSerie2 = True
          IndProp(Index).Serie2name = "Exponential Moving Average" + Str$(.MAV2) + " dagar"
        End If
        IndProp(Index).Serie1Name = "Exponential Moving Average" + Str$(.MAV1) + " dagar"
      Else
        SimpleMAVCalc PDArray, IndSerie(Index).Serie1, .MAV1, UBound(PDArray), False
        IndProp(Index).Serie1Name = "Simple Moving Average" + Str$(.MAV1) + " dagar"
        If ParameterSettings.MAVNbr = 1 Then
          IndProp(Index).ShowSerie2 = False
        Else
          SimpleMAVCalc PDArray, IndSerie(Index).Serie2, .MAV2, UBound(PDArray), False
          IndProp(Index).ShowSerie2 = True
          IndProp(Index).Serie2name = "Simple Moving Average" + Str$(.MAV2) + " dagar"
        End If
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowScale = True
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
    Case "MACD"
      MACDCalc PDArray, IndSerie(Index).Serie1, IndSerie(Index).Serie2, UBound(PDArray), False
      If .MACDMAV Then
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "MACD"
      IndProp(Index).Serie2name = "MACD Signal"
      IndProp(Index).ShowScale = True
    Case "Momentum"
      MomentumCalc PDArray, IndSerie(Index).Serie1, .MOM, UBound(PDArray), False
      If .PlotMOMandMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .MAVMOM, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = True
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).FixLine1Value = 100
      IndProp(Index).Serie1Name = "Momentum " + Str$(ParameterSettings.MOM) + " dagar"
      IndProp(Index).Serie2name = "MAV av Momentum " + Str$(ParameterSettings.MAVMOM) + " dagar"
      IndProp(Index).ShowScale = False
    Case "Money Flow Index"
      MFICalc PDArray, IndSerie(Index).Serie1, .MFILength, UBound(PDArray), False
      ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .MAVMFI, UBound(PDArray)
      If .MFIShowMAV Then
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = True
      IndProp(Index).Min = 0
      IndProp(Index).Max = 100
      IndProp(Index).ShowFixLine1 = True
      IndProp(Index).ShowFixLine2 = True
      IndProp(Index).FixLine1Value = ParameterSettings.MFILowerSignalLevel
      IndProp(Index).FixLine2Value = ParameterSettings.MFIUpperSignalLevel
      IndProp(Index).Serie1Name = "MFI " + Str$(.MFILength) + " dagar"
      IndProp(Index).Serie2name = "MAV av MFI " + Str$(.MAVMFI) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Price Oscillator"
      If ParameterSettings.OSCMAVExp Then
        ExpMAVCalc PDArray, IndSerie(Index).Serie1, .PRIOSC1, UBound(PDArray)
        ExpMAVCalc PDArray, IndSerie(Index).Serie2, .PRIOSC2, UBound(PDArray)
      Else
        SimpleMAVCalc PDArray, IndSerie(Index).Serie1, .PRIOSC1, UBound(PDArray), False
        SimpleMAVCalc PDArray, IndSerie(Index).Serie2, .PRIOSC2, UBound(PDArray), False
      End If
      IndProp(Index).ShowSerie2 = True
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Price Oscillator " + Str$(.PRIOSC1) + " dagar"
      IndProp(Index).Serie2name = "Price Oscillator " + Str$(.PRIOSC2) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Relative Strength Index"
      RSICalc PDArray, IndSerie(Index).Serie1, .RSI, UBound(PDArray), False
      If .PlotRSIandMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .MAVRSI, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = True
      IndProp(Index).Max = 100
      IndProp(Index).Min = 0
      IndProp(Index).ShowFixLine1 = True
      IndProp(Index).ShowFixLine2 = True
      IndProp(Index).FixLine1Value = ParameterSettings.RSILowerSignalLevel
      IndProp(Index).FixLine2Value = ParameterSettings.RSIUpperSignalLevel
      IndProp(Index).Serie1Name = "RSI " + Str$(.RSI) + " dagar"
      IndProp(Index).Serie2name = "MAV av RSI " + Str$(.MAVRSI) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Stochastic"
      StochasticOscCalc PDArray, IndSerie(Index).Serie1, .STO, UBound(PDArray), False, .STOSlowing
      If .PlotSTOandMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .MAVSTO, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = True
      IndProp(Index).Max = 100
      IndProp(Index).Min = 0
      IndProp(Index).ShowFixLine1 = True
      IndProp(Index).ShowFixLine2 = True
      IndProp(Index).FixLine1Value = ParameterSettings.STOLowerSignalLevel
      IndProp(Index).FixLine2Value = ParameterSettings.STOUpperSignalLevel
      IndProp(Index).Serie1Name = "STO " + Str$(.STO) + " dagar"
      IndProp(Index).Serie2name = "MAV av STO " + Str$(.MAVSTO) + " dagar"
      IndProp(Index).ShowScale = True
    'Case "Coppock"
    '  CoppockCalc PDArray, IndSerie(Index).Serie1, .COPMAV, ParameterSettings.COPDist, ParameterSettings.COPSUM, UBound(PDArray), False
    '  IndProp(Index).ShowSerie2 = False
    '  IndProp(Index).FixMaxMin = False
    '  IndProp(Index).ShowFixLine1 = False
    '  IndProp(Index).ShowFixLine2 = False
    '  IndProp(Index).Serie1Name = "Coppock"
    Case "Accumulation/Distribution"
      AccDistCalc PDArray, IndSerie(Index).Serie1, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Accumulation/Distribution"
      IndProp(Index).ShowScale = True
    Case "Average True Range"
      ATRCalc PDArray, IndSerie(Index).Serie1, .AverageTrueRangeLength, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Average True Range"
      IndProp(Index).ShowScale = True
    Case "Chaikin Oscillator"
      ChaikinOscCalc PDArray, IndSerie(Index).Serie1, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Chaikin Oscillator"
      IndProp(Index).ShowScale = True
    Case "Chaikin's Volatility"
      ChaikinsVolatilityCalc PDArray, IndSerie(Index).Serie1, .ChaikinVolatility, .ChaikinVolatilityROC, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Chaikin's Volatility"
      IndProp(Index).ShowScale = True
    Case "Detrended Price Oscillator"
      DPOCalc PDArray, IndSerie(Index).Serie1, .DetrendedPriceOscillatorLength, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Detrended Price Oscillator"
      IndProp(Index).ShowScale = True
    Case "Ease Of Movement"
      EaseOfMovementCalc PDArray, IndSerie(Index).Serie1, .EaseOfMovementLength, UBound(PDArray), False
      If .PlotEaseOfMovementAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .EaseOfMovementMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Ease Of Movement " + Str$(.EaseOfMovementLength) + " dagar"
      IndProp(Index).Serie2name = "MAV av Ease Of Movement " + Str$(.EaseOfMovementMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Negative Volume Index"
      NegativeVolumeCalc PDArray, IndSerie(Index).Serie1, UBound(PDArray), False
      If .PlotNegativeVolumeAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .NegativeVolumeMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Negative Volume Index"
      IndProp(Index).Serie2name = "MAV av Negative Volume Index " + Str$(.NegativeVolumeMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "On Balance Volume"
      OnBalanceVolumeCalc PDArray, IndSerie(Index).Serie1, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "On Balance Volume"
      IndProp(Index).ShowScale = True
    Case "Positive Volume Index"
      PositiveVolumeCalc PDArray, IndSerie(Index).Serie1, UBound(PDArray), False
      If .PlotPositiveVolumeAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .PositiveVolumeMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Positive Volume Index"
      IndProp(Index).Serie2name = "MAV av Positive Volume Index " + Str$(.PositiveVolumeMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Rate-Of-Change"
      RateOfChangeCalc PDArray, IndSerie(Index).Serie1, .RateOfChangeLength, UBound(PDArray), False
      If .PlotRateOfChangeAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .RateOfChangeMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Rate-Of-Change " + Str$(.RateOfChangeLength) + " dagar"
      IndProp(Index).Serie2name = "MAV av Rate-Of-Change " + Str$(.RateOfChangeMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Standard Deviation"
      StdDevCalc PDArray, IndSerie(Index).Serie1, .StandardDeviationLength, UBound(PDArray), False
      If .PlotStandardDeviationAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .StandardDeviationMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Standard Deviation " + Str$(.StandardDeviationLength) + " dagar"
      IndProp(Index).Serie2name = "MAV av Standard Deviation " + Str$(.StandardDeviationMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Volatilitet"
      VolatilityCalc PDArray, IndSerie(Index).Serie1
      If .PlotStandardDeviationAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .StandardDeviationMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Volatilitet " + Str$(.Volatility) + " dagar"
      IndProp(Index).Serie2name = "MAV av Volatilitet " + Str$(.VolatilityMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "TRIX"
      TRIXCalc PDArray, IndSerie(Index).Serie1, .TRIXLength, UBound(PDArray), False
      If .PlotTRIXAndMAV Then
        ExpCalc IndSerie(Index).Serie1, IndSerie(Index).Serie2, .TRIXMAVLength, UBound(PDArray)
        IndProp(Index).ShowSerie2 = True
      Else
        IndProp(Index).ShowSerie2 = False
      End If
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "TRIX " + Str$(.TRIXLength) + " dagar"
      IndProp(Index).Serie2name = "MAV av TRIX " + Str$(.TRIXMAVLength) + " dagar"
      IndProp(Index).ShowScale = True
    Case "Volume Oscillator"
      VolumeOscillatorCalc PDArray, IndSerie(Index).Serie1, .VolumeOscillatorMAV1Length, .VolumeOscillatorMAV2Length, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Volume Oscillator"
      IndProp(Index).ShowScale = True
    Case "Volume Rate-Of-Change"
      VolumeROCCalc PDArray, IndSerie(Index).Serie1, .VolumeRateOfChangeLength, UBound(PDArray), False
      IndProp(Index).FixMaxMin = False
      IndProp(Index).ShowFixLine1 = False
      IndProp(Index).ShowFixLine2 = False
      IndProp(Index).Serie1Name = "Volume Rate-Of-Change"
      IndProp(Index).ShowScale = True
    End Select
    With IndProp(Index)
     .ShowSerie1 = True
     .Serie1Color = GraphSettings.Serie1Color
     .Serie2Color = GraphSettings.Serie2Color
     .GridColor = GraphSettings.GridColor
     .AxisColor = GraphSettings.AxisColor
     .lngColorDrawArea = GraphSettings.lngDrawAreaColor
     .DayWidth = GraphSettings.lngDayWidth
    End With
  
  End With
  
End Sub
Sub SetGains(ByRef PDArray() As PriceDataType)
  Dim Status As Boolean
  Dim gain As Single
  gain = GetGain(PDArray, UBound(PDArray), ggpDay, Status)
  If Status Then
    strGainDay = FormatPercent(gain, 2)
  Else
    strGainDay = "N/A"
  End If
  gain = GetGain(PDArray, UBound(PDArray), ggpWeek, Status)
  If Status Then
    strGainWeek = FormatPercent(gain, 2)
  Else
    strGainWeek = "N/A"
  End If
  gain = GetGain(PDArray, UBound(PDArray), ggpMonth, Status)
  If Status Then
    strGainMonth = FormatPercent(gain, 2)
  Else
    strGainMonth = "N/A"
  End If
  gain = GetGain(PDArray, UBound(PDArray), ggpYear, Status)
  If Status Then
    strGainYear = FormatPercent(gain, 2)
  Else
    strGainYear = "N/A"
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          SetVolumeProperties
'Input:
'Output:
'
'Description:   Set the properties for the volume graph.
'Author:        Fredrik Wendel
'Revision:      990728  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub SetVolumeProperties()
  With VolumeProperties
    .ColorVolume = GraphSettings.lngVolumeColor
    .ColorAxis = GraphSettings.AxisColor
    .ColorGrid = GraphSettings.GridColor
    .lngColorDrawArea = GraphSettings.lngDrawAreaColor
  End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          UpdateGraph
'Input:
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      970411  Created
'               980112  A few if statements were added in the
'                       process value section.
'               990830  "Dag", "M�nad" and "�r" is now printed
'                       on the title bar.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub UpdateGraph(lngObjectIndex As Long)
  
  Static lngLastObjectIndex As Long
  Static LastDWM As DWMEnum
  Static lngLastDaySpace As Long
  Static lngLastStop As Long
  
  Dim GraphProp As GraphPropertiesType
  Dim TPX As Long
  Dim TPY As Long
  Dim ht As Long
  Dim wt As Long
  Dim I As Long
  Dim j As Long
  Dim DrawInfo As GraphDrawInfoType
  Dim GraphDrawRect As GraphDrawRectType
  Dim PriceDrawRect As GraphDrawRectType
  Dim VolumeDrawRect As GraphDrawRectType
  Dim IndicatorDrawRect(1 To 2) As GraphDrawRectType
  Dim blnShowSetup As Boolean
  dbg.Enter "UpdateGraph"
  
  
  
  If ShowGraph = True Then
    MainMDI.MousePointer = vbHourglass
  
    If DoUpdate Then
    dbg.Out "Do a real update"
      With Frm_OwnGraph
      .ScaleMode = 3
      .Cls
      Select Case GraphSettings.DWM
      Case dwmDaily
       .Caption = Trim(InvestInfoArray(ActiveIndex).Name) & " - Dag"
      Case dwmWeekely
       .Caption = Trim(InvestInfoArray(ActiveIndex).Name) & " - Vecka"
      Case dwmMonthly
       .Caption = Trim(InvestInfoArray(ActiveIndex).Name) & " - M�nad"
      End Select
      TPX = Screen.TwipsPerPixelX
      TPY = Screen.TwipsPerPixelY
      ht = Frm_OwnGraph.Height / TPY - 25
      wt = Frm_OwnGraph.Width / TPX - 5
      End With
    
      
      With GraphDrawRect
        .bottom = ht
        .Height = ht
        .left = 1
        .Width = wt
      End With
      
       'Set pricegraph size properties
      PriceDrawRect.left = 1
      PriceDrawRect.Width = wt
      VolumeDrawRect.left = 1
      VolumeDrawRect.Width = wt
      With GraphSettings
      If .ShowVolume Then
        If .ShowIndicator1 And .ShowIndicator2 Then
          PriceDrawRect.bottom = (ht - 17) * 0.45
          PriceDrawRect.Height = (ht - 17) * 0.45
          VolumeDrawRect.bottom = (ht - 17) * 0.6
          VolumeDrawRect.Height = (ht - 17) * 0.15
        ElseIf .ShowIndicator1 Xor .ShowIndicator2 Then
          PriceDrawRect.bottom = (ht - 17) * 0.55
          PriceDrawRect.Height = (ht - 17) * 0.55
          VolumeDrawRect.bottom = (ht - 17) * 0.7
          VolumeDrawRect.Height = (ht - 17) * 0.15
        Else
          PriceDrawRect.bottom = (ht - 17) * 0.85
          PriceDrawRect.Height = (ht - 17) * 0.85
          VolumeDrawRect.bottom = (ht - 17)
          VolumeDrawRect.Height = (ht - 17) * 0.15
        End If
      Else
        If .ShowIndicator1 And .ShowIndicator2 Then
          PriceDrawRect.bottom = (ht - 17) * 0.6
          PriceDrawRect.Height = (ht - 17) * 0.6
        ElseIf .ShowIndicator1 Xor .ShowIndicator2 Then
          PriceDrawRect.bottom = (ht - 17) * 0.7
          PriceDrawRect.Height = (ht - 17) * 0.7
        Else
          PriceDrawRect.bottom = (ht - 17)
          PriceDrawRect.Height = (ht - 17)
        End If
      End If
      End With
      
      With GraphProp
       .Name = Trim(InvestInfoArray(ActiveIndex).Name)

       .PriceColor = GraphSettings.PriceColor
       .PriceMAV1Color = GraphSettings.PriceMAV1Color
       .PriceMAV2Color = GraphSettings.PriceMAV2Color
       .BackGroundColor = GraphSettings.BackGroundColor
       .AxisColor = GraphSettings.AxisColor
       .GridColor = GraphSettings.GridColor
       .lngDrawAreaColor = GraphSettings.lngDrawAreaColor
       .ShowHLC = GraphSettings.ShowHLC
       .ShowPriceMAV1 = GraphSettings.ShowPriceMAV1
       .ShowPriceMAV2 = GraphSettings.ShowPriceMAV2
       '.ShowPriceMAV2 = GraphSettings.ShowPriceMAV2
       .ShowXGrid = GraphSettings.ShowXGrid
       .ShowMinXGrid = GraphSettings.ShowMinXGrid
       .ShowYGrid = GraphSettings.ShowYGrid
       .ExponentialPrice = GraphSettings.ExponentialPrice
       .BollingerColor = GraphSettings.BollingerColor
       .ShowBollinger = GraphSettings.ShowBollinger
       .ShowParabolic = GraphSettings.ShowParabolic
       .ParabolicColor = GraphSettings.ParabolicColor
       .ShowZero = GraphSettings.ShowZero
       .lngCloseColor = GraphSettings.lngCloseColor
       .lngFishColor = 33023 'GraphSettings.lngFishColor
       .lngSupportResistanceColor = GraphSettings.lngSupportResistanceColor
       .lngCloseWidth = GraphSettings.lngCloseWidth
       .strLatest = PriceDataArray(UBound(PriceDataArray)).Close
       .blnShowFish = GraphSettings.blnShowFish
       .blnShowSupportResistance = GraphSettings.blnShowSupportResistance
       For j = 1 To UBound(.Setup)
        .Setup(j) = GraphSettings.Setup(j)
       Next j
       If GraphSettings.ShowSignalColor Then
         .ShowSignalColor = True
         .SignalColorBuy = GraphSettings.SignalColorBuy
         .SignalColorSell = GraphSettings.lngSignalColorSell
         .strColorSignallingIndicator = GraphSettings.strColorSignallingIndicator
       Else
        .ShowSignalColor = False
       End If
      End With
      
      'Process valuearrays
      If GraphInfoChanged Then

        ProcessPriceDataArray
        With GraphSettings
        If .ShowPriceMAV1 Or .ShowPriceMAV2 Then ProcessPriceMAVValues DrawArray
        If .ShowBollinger Then ProcessBollinger DrawArray
        If .ShowParabolic Then ProcessParabolic DrawArray
        If .ShowSignalColor Then
          CalcSignalArray SignalColorArray(), MainMDI.ID_ColorSignallingIndicator.Text
        End If
        If .ShowIndicator1 Then SetIndicatorProperties 1, DrawArray
        If .ShowIndicator2 Then SetIndicatorProperties 2, DrawArray
        If .ShowVolume Then SetVolumeProperties
        'ProcessCompare DrawArray, ComparePercentArray, .lngCompareIndex
        SetGains PriceDataArray
        blnShowSetup = False
        For j = 1 To UBound(.Setup)
          blnShowSetup = blnShowSetup Or .Setup(j).blnShow
        Next j
        If blnShowSetup Then
          SetupArrayCreator DrawArray, lngSetupArray
          DoEvents
        End If
        GraphInfoChanged = False
        End With
      End If
      
      With GraphProp
      .strGainDay = strGainDay
      .strGainWeek = strGainWeek
      .strGainMonth = strGainMonth
      .strGainYear = strGainYear
      End With
      
      'Set indicator 1 size properties
      With IndicatorDrawRect(1)
      If GraphSettings.ShowIndicator1 And GraphSettings.ShowIndicator2 Then
        .bottom = (ht - 17) * 0.8
        .Height = (ht - 17) * 0.2
      ElseIf GraphSettings.ShowIndicator1 Xor GraphSettings.ShowIndicator2 Then
        .bottom = (ht - 17)
        .Height = (ht - 17) * 0.3
      End If
      .left = 1
      .Width = wt
      End With
      
      With DrawInfo
        .DaySpace = GraphSettings.lngDaySpace
        .DayWidth = GraphSettings.lngDayWidth
        .DWM = GraphSettings.DWM
        
        
      End With
      DrawInfo.Start = Frm_OwnGraph.ScrollBar.Value
      If (lngLastObjectIndex <> lngObjectIndex) Or (LastDWM <> DrawInfo.DWM) Then
        SetStartAndStopToLast PriceDrawRect, DrawInfo, UBound(DrawArray)
        lngLastObjectIndex = lngObjectIndex
        lngLastDaySpace = DrawInfo.DaySpace
        LastDWM = DrawInfo.DWM
      Else
        If lngLastDaySpace <> DrawInfo.DaySpace Then
          DrawInfo.Stop = lngLastStop
          SetStartAndStopToSameEnd PriceDrawRect, DrawInfo, UBound(DrawArray)
          lngLastDaySpace = DrawInfo.DaySpace
          lngLastObjectIndex = lngObjectIndex
        Else
          SetStartAndStop PriceDrawRect, DrawInfo, UBound(DrawArray)
          lngLastObjectIndex = lngObjectIndex
          lngLastDaySpace = DrawInfo.DaySpace
        End If
      End If
      lngLastStop = DrawInfo.Stop
      
      
      
      'Draw price graph
      DrawGraph Frm_OwnGraph, PriceDrawRect, DrawArray, UBound(DrawArray), _
        MAV1, MAV2, SignalColorArray, GraphProp, Frm_OwnGraph.ScrollBar.Value, _
        DrawInfo, BollingerUpper(), BollingerLower(), BollingerMAV(), _
        Parabolic(), ParabolicBuyOrSell(), lngSetupArray
        
          
      If GraphSettings.ShowVolume Then
        DrawVolume Frm_OwnGraph, DrawInfo, VolumeDrawRect, VolumeProperties, DrawArray
       End If
       
       
        
        
        
      'Draw indicator 1
      If GraphSettings.ShowIndicator1 Then
        DrawIndicator Frm_OwnGraph, IndicatorDrawRect(1), IndSerie(1).Serie1, IndSerie(1).Serie2, UBound(DrawArray), IndProp(1), DrawInfo
      End If
    
      'Set indicator 2 size properties
      With IndicatorDrawRect(2)
      If GraphSettings.ShowIndicator1 And GraphSettings.ShowIndicator2 Then
        .Height = (ht - 17) * 0.2
      End If
      If Not GraphSettings.ShowIndicator1 And GraphSettings.ShowIndicator2 Then
        .Height = (ht - 17) * 0.3
      End If
      .bottom = (ht - 17)
      .left = 1
      .Width = wt
      End With
    
      'Draw indicator 2
      If GraphSettings.ShowIndicator2 Then
        DrawIndicator Frm_OwnGraph, IndicatorDrawRect(2), IndSerie(2).Serie1, IndSerie(2).Serie2, UBound(DrawArray), IndProp(2), DrawInfo
      End If
      
      'Update scrollbar position
      SetScrollbar GraphDrawRect, DrawInfo, UBound(DrawArray)
            
      'Update trendlines
      UpdateTrendlines GraphSettings.DWM
      
      'Update labels
      If ActiveStockLabelsHandle = Empty Then
        Dim StockName As String
        StockName = RTrim(InvestInfoArray(ActiveIndex).Name)
        Select Case GraphSettings.DWM
          Case dwmDaily
            StockName = StockName & "-Day"
          Case dwmWeekely
            StockName = StockName & "-Week"
          Case dwmMonthly
            StockName = StockName & "-Month"
        End Select
        ActiveStockLabelsHandle = _
          ShowStockLabels(StockName)
      End If
      UpdateLabelsScaling
      RefreshStockLabels Frm_OwnGraph.hdc, ActiveStockLabelsHandle
        
    End If
    MainMDI.MousePointer = vbDefault
  End If
   
  dbg.Leave "UpdateGraph"
End Sub
    
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          UpdateTrendlines
'Input:
'Output:
'
'Description:   Update the positions of the trendlines
'Author:        Fredrik Wendel
'Revision:      970110  Created.
'               980106  Checks for limits were added.
'               991018  Now uses GetGraphX and GetGraphY.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  
  Sub UpdateTrendlines(DWM As DWMEnum)
  
  
  Dim j As Long
  Dim StartPrice As Single
  Dim EndPrice As Single
  Dim X1 As Long
  Dim Y1 As Long
  Dim X2 As Long
  Dim Y2 As Long
  
  
  
  
  For j = 1 To TrendlineSerieSize
    With Frm_OwnGraph.TrendLine(j - 1)
      
    If TrendLineArray(DWM, j).InUse Then
      TLWholeInView(DWM, j).End1 = True
      TLWholeInView(DWM, j).End2 = True
      .BorderColor = TrendLineArray(DWM, j).LineColor
      
      Y1 = GetGraphY(TrendLineArray(DWM, j).StartPrice)
      Y2 = GetGraphY(TrendLineArray(DWM, j).EndPrice)
    
      X1 = GetGraphX(TrendLineArray(DWM, j).StartDate)
      X2 = GetGraphX(TrendLineArray(DWM, j).EndDate)
      
      'Check if any part of the lines is within limits
      If (X1 >= LeftDrawTL Or X2 >= LeftDrawTL) And (X1 <= RightDrawTL Or X2 <= RightDrawTL) And (Y1 >= TopDrawTL Or Y2 >= TopDrawTL) And (Y1 <= BottomDrawTL Or Y2 <= BottomDrawTL) Then
        
        'Check left limits
        If X1 < LeftDrawTL Or X2 < LeftDrawTL Then
          If X1 < LeftDrawTL Then
            Y1 = Y1 + Int((Y2 - Y1) / (X2 - X1) * (LeftDrawTL - X1))
            X1 = LeftDrawTL
            TLWholeInView(DWM, j).End1 = False
          End If
          If X2 < LeftDrawTL Then
            Y2 = Y2 + Int((Y2 - Y1) / (X2 - X1) * (LeftDrawTL - X2))
            X2 = LeftDrawTL
            TLWholeInView(DWM, j).End2 = False
          End If
        End If
        
        'Check right limits
        If X1 > RightDrawTL Or X2 > RightDrawTL Then
          If X1 > RightDrawTL Then
            Y1 = Y1 + Int((Y2 - Y1) / (X2 - X1) * (RightDrawTL - X1))
            X1 = RightDrawTL
            TLWholeInView(DWM, j).End1 = False
          End If
          If X2 > RightDrawTL Then
            Y2 = Y2 + Int((Y2 - Y1) / (X2 - X1) * (RightDrawTL - X2))
            X2 = RightDrawTL
            TLWholeInView(DWM, j).End2 = False
          End If
        End If
        
        'Check bottom limits
        If Y1 > BottomDrawTL Or Y2 > BottomDrawTL Then
          If Y1 > BottomDrawTL Then
            X1 = X1 + Int((X2 - X1) / (Y2 - Y1) * (BottomDrawTL - Y1))
            Y1 = BottomDrawTL
            TLWholeInView(DWM, j).End1 = False
          End If
          If Y2 > BottomDrawTL Then
            X2 = X2 + Int((X2 - X1) / (Y2 - Y1) * (BottomDrawTL - Y2))
            Y2 = BottomDrawTL
            TLWholeInView(DWM, j).End2 = False
          End If
        End If
      
        'Check top limits
        If Y1 < TopDrawTL Or Y2 < BottomDrawTL Then
          If Y1 < TopDrawTL Then
            X1 = X1 + Int((X2 - X1) / (Y2 - Y1) * (TopDrawTL - Y1))
            Y1 = TopDrawTL
            TLWholeInView(DWM, j).End1 = False
          End If
          If Y2 < TopDrawTL Then
            X2 = X2 + Int((X2 - X1) / (Y2 - Y1) * (TopDrawTL - Y2))
            Y2 = TopDrawTL
            TLWholeInView(DWM, j).End2 = False
          End If
        End If
      
        .X1 = X1
        .X2 = X2
        .Y1 = Y1
        .Y2 = Y2
        .Visible = True
      Else
        .Visible = False
      End If

    Else
      .Visible = False
    End If
    End With
  Next j



End Sub

