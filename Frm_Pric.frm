VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Frm_Prices 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Visa kurser"
   ClientHeight    =   4350
   ClientLeft      =   510
   ClientTop       =   330
   ClientWidth     =   7485
   Icon            =   "Frm_Pric.frx":0000
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4350
   ScaleWidth      =   7485
   Begin VB.Frame Frame4 
      Caption         =   "Procentuell utveckling under �ret"
      Height          =   1095
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   2895
      Begin VB.TextBox Text2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   840
         MultiLine       =   -1  'True
         TabIndex        =   7
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Frame3 
      Height          =   1095
      Left            =   3720
      TabIndex        =   2
      Top             =   0
      Width           =   3735
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         MultiLine       =   -1  'True
         TabIndex        =   6
         Top             =   240
         Width           =   1215
      End
      Begin VB.CommandButton Command3 
         Caption         =   "N�sta �r"
         Height          =   375
         Left            =   2040
         TabIndex        =   4
         Top             =   600
         Width           =   1455
      End
      Begin VB.CommandButton Command2 
         Caption         =   "F�reg�ende �r"
         Height          =   375
         Left            =   240
         TabIndex        =   3
         Top             =   600
         Width           =   1455
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Kurser"
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   1080
      Width           =   7455
      Begin MSFlexGridLib.MSFlexGrid G_Price 
         Height          =   2895
         Left            =   120
         TabIndex        =   1
         Top             =   240
         Width           =   7215
         _ExtentX        =   12726
         _ExtentY        =   5106
         _Version        =   393216
         FixedCols       =   0
         Redraw          =   -1  'True
      End
   End
End
Attribute VB_Name = "Frm_Prices"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command2_Click()
  UpdatePriceGrid -1
End Sub

Private Sub Command3_Click()
  UpdatePriceGrid 1
End Sub

Private Sub Form_Load()
  Dim ShowYear As Date
    Frm_Prices.left = 700
    Frm_Prices.top = 100
    
    YearToShowInShowPrices = Year(PriceDataArray(PriceDataArraySize).Date)
    CalcIndex
    UpdatePriceGrid 0
    
    DisableObjectSelection
End Sub

Private Sub Form_Terminate()
  EnableObjectSelection
End Sub

Private Sub Form_Unload(Cancel As Integer)
  EnableObjectSelection
End Sub

