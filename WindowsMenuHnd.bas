Attribute VB_Name = "WindowsMenuHnd"
Option Explicit
#If 0 Then
Public Sub AddToWindowList(ByVal NewDocID As String)

' Creates a new State Button Tool by adding it to the
  ' control-level Tools collection

MainMDI.SSActiveToolBars1.Tools.Add NewDocID, ssTypeStateButton

  ' Set properties of the new State Button Tool
  With MainMDI.SSActiveToolBars1.Tools(NewDocID)
    .Name = NewDocID
    .Group = "WindowList"
    .GroupAllowAllUp = False
    .State = ssChecked
    ' Change the file path below, if necessary
    '.PictureDown = LoadPicture(App.Path & "\CHECK.BMP")
  End With

  ' Adds a copy of the new Tool as the last Tool on the
  ' Window List Menu Tool

With MainMDI.SSActiveToolBars1.Tools("ID_WindowList").Menu.Tools
    .Add NewDocID, , .Count + 1

  ' If there are only five Tools, add a Separator Tool
    If .Count = 5 Then .Add "separator", ssTypeSeparator, 5
  End With

End Sub
#End If
