VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form Frm_BuySell 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ny transaktion"
   ClientHeight    =   4155
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3180
   Icon            =   "Frm_BuySell.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4155
   ScaleWidth      =   3180
   Begin VB.Frame Frame4 
      Caption         =   "Typ"
      Height          =   855
      Left            =   0
      TabIndex        =   9
      Top             =   960
      Width           =   3135
      Begin VB.ComboBox cBuySell 
         Height          =   315
         Left            =   840
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   3360
      Width           =   3135
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Transaktion"
      Height          =   1335
      Left            =   0
      TabIndex        =   7
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox tbCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox tbPrice 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox tbVolume 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Courtage"
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Kurs"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Antal"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Datum"
      Height          =   855
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   3135
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   840
         TabIndex        =   13
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         Format          =   20643841
         CurrentDate     =   36402
      End
   End
End
Attribute VB_Name = "Frm_BuySell"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()
  Dim AllDataOk As Boolean
  With Frm_BuySell
  If .tbPrice.Text <> "" And .tbCourtage.Text <> "" And .tbVolume.Text <> "" Then
    AllDataOk = True
  Else
    AllDataOk = False
  End If
  End With
  
  If AllDataOk Then
    If BuySellOK Then
      Unload Frm_BuySell
      If ReportWindowOpen Then
        UpdateReportGrid
      End If
    Else
      MsgBox CheckLang("Du har inte s� m�nga aktier som du angivit i formul�ret i den �ppna portf�ljen."), vbOKOnly, CheckLang("Felmeddelande")
      Frm_BuySell.tbVolume.SetFocus
    End If
  Else
    MsgBox CheckLang("N�got eller n�gra av dataf�lten inneh�ller ett felaktigt v�rde. Komplettera."), vbOKOnly, CheckLang("Felaktigt v�rde")
  End If
End Sub

Private Sub Command2_Click()
  Unload Frm_BuySell
End Sub

Private Sub Form_Load()
  With Frm_BuySell
    .left = 1000
    .top = 300
    .cBuySell.AddItem "K�p"
    .cBuySell.AddItem "S�lj"
    .cBuySell.ListIndex = 0
    .DTPicker1.Value = Date
  End With
End Sub


Private Sub tbCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbCourtage_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbCourtage
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt courtagebelopp."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbCourtage.SetFocus
End Sub

Private Sub tbPrice_GotFocus()
  TextSelected
End Sub

Private Sub tbPrice_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbPrice
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange en korrekt aktiekurs."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbPrice.SetFocus
End Sub

Private Sub tbVolume_GotFocus()
  TextSelected
End Sub

Private Sub tbVolume_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbVolume
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt antal aktier."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbVolume.SetFocus
End Sub
