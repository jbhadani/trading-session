Attribute VB_Name = "Trigger"
Public Sub UpdateInfo(FileName As String)
  Dim I As Integer
  Dim PDArray() As PriceDataType
  Dim Size As Long
  Dim CloseQuote As Single
  
  R_PriceDataArray FileName, PDArray(), Size
  
  CloseQuote = PDArray(UBound(PDArray())).Close
  
  With frmTriggerRegistration
    .txtClose.Text = CloseQuote
    .txtUpperLevel.Text = FormatNumber(Int(CloseQuote * 1.2), 2)
    .txtLowerLevel.Text = FormatNumber(Int(CloseQuote * 0.8), 2)
  End With
  
End Sub

Public Sub UpdateGrid()
  Dim I As Long
  
  With frmTriggers.MSFlexGrid1
  .Redraw = False
  .Rows = UBound(Triggers()) + 1
  .Clear
  .Row = 0
  .Col = 0
  .Text = "Larm"
  .Col = 1
  .Text = "Aktie"
  .Col = 2
  .Text = "Senast"
  .Col = 3
  .Text = "�vre gr�ns"
  .Col = 4
  .Text = "Undre gr�ns"
  .Col = 5
  .Text = "Registrerad"
  
  CheckTriggers
  
  For I = 1 To UBound(Triggers())
    .Row = I
    .Col = 0
    If Triggers(I).Position = -2 Then
      .CellBackColor = QBColor(4)
    ElseIf Triggers(I).Position = -1 Then
      .CellBackColor = QBColor(12)
    ElseIf Triggers(I).Position = 0 Then
      .CellBackColor = QBColor(15)
    ElseIf Triggers(I).Position = 1 Then
      .CellBackColor = QBColor(10)
    Else
      .CellBackColor = QBColor(2)
    End If
    .Col = 1
    .Text = Trim(Triggers(I).Name)
    .Col = 2
    .Text = FormatCurrency(Triggers(I).Close, 2)
    .Col = 3
    .Text = FormatCurrency(Triggers(I).UpperTriggerLevel, 2)
    .Col = 4
    .Text = FormatCurrency(Triggers(I).LowerTriggerLevel, 2)
    .Col = 5
    .Text = Trim(Triggers(I).Date)
  Next I
  .Redraw = True
  End With
End Sub

Public Sub AddNewTrigger()
  Dim NewSize As Long
  
  NewSize = UBound(Triggers()) + 1
  
  ReDim Preserve Triggers(NewSize)
  
  With frmTriggerRegistration
    Triggers(NewSize).Name = Trim(.lblObject.Caption)
    Triggers(NewSize).Date = .DTPicker1.Value
    Triggers(NewSize).UpperTriggerLevel = CSng(PointToComma(.txtUpperLevel))
    Triggers(NewSize).LowerTriggerLevel = CSng(PointToComma(.txtLowerLevel))
  End With
  
  UpdateGrid
End Sub

Public Sub CheckTriggers()
  Dim I As Long
  Dim FileName As String
  Dim Size As Long
  Dim PDArray() As PriceDataType
  
  For I = 1 To UBound(Triggers())
    R_PriceDataArray DataPath + Trim(InvestInfoArray(GetInvestIndex(Trim(Triggers(I).Name))).FileName) + ".prc", PDArray, Size
    If PDArray(Size).Close > Triggers(I).UpperTriggerLevel Then
      Triggers(I).Position = 2
    ElseIf PDArray(Size).Close > Triggers(I).UpperTriggerLevel * 0.95 Then
      Triggers(I).Position = 1
    ElseIf PDArray(Size).Close < Triggers(I).LowerTriggerLevel Then
      Triggers(I).Position = -2
    ElseIf PDArray(Size).Close < Triggers(I).LowerTriggerLevel * 1.05 Then
      Triggers(I).Position = -1
    Else
      Triggers(I).Position = 0
    End If
    Triggers(I).Close = PDArray(Size).Close
  Next I
End Sub

Public Sub UpdateTrigger()
  Dim I As Long
  Dim Index As Long
  
  Index = frmTriggers.MSFlexGrid1.RowSel
    
  With frmTriggerRegistration
    Triggers(Index).Date = .DTPicker1.Value
    Triggers(Index).UpperTriggerLevel = CSng(PointToComma(.txtUpperLevel))
    Triggers(Index).LowerTriggerLevel = CSng(PointToComma(.txtLowerLevel))
  End With
  
  UpdateGrid
End Sub

Public Sub DeleteTrigger(Index As Long)
  Dim I As Integer
  Dim TempTriggers() As TriggerType
  Dim Found As Boolean
  Dim Position As Long
  
  ReDim TempTriggers(UBound(Triggers()) - 1)
  Found = False
  
  For I = 1 To UBound(Triggers())
    If I <> Index Then
      If Not Found Then
        Position = I
      Else
        Position = I - 1
      End If
      TempTriggers(Position).Close = Triggers(I).Close
      TempTriggers(Position).Date = Triggers(I).Date
      TempTriggers(Position).LowerTriggerLevel = Triggers(I).LowerTriggerLevel
      TempTriggers(Position).Name = Triggers(I).Name
      TempTriggers(Position).Position = Triggers(I).Position
      TempTriggers(Position).UpperTriggerLevel = Triggers(I).UpperTriggerLevel
    Else
      Found = True
    End If
  Next I
  
  ReDim Triggers(UBound(TempTriggers()))
  
  For I = 1 To UBound(Triggers())
    Triggers(I).Close = TempTriggers(I).Close
    Triggers(I).Date = TempTriggers(I).Date
    Triggers(I).LowerTriggerLevel = TempTriggers(I).LowerTriggerLevel
    Triggers(I).Name = TempTriggers(I).Name
    Triggers(I).Position = TempTriggers(I).Position
    Triggers(I).UpperTriggerLevel = TempTriggers(I).UpperTriggerLevel
  Next I
  
  UpdateGrid
  
End Sub

Sub StartUpTriggerCheck()
Dim FileName As String
Dim I As Integer
Dim Triggered As Boolean
Dim Svar As Integer

If GetSetting(App.Title, "Trigger", "StartCheck", "True") = "True" Then
  FileName = Path + "triggers.dta"
  
  R_Triggers FileName, Triggers()

  CheckTriggers
  
  Triggered = False
  For I = 1 To UBound(Triggers)
    If Triggers(I).Position = 2 Or Triggers(I).Position = -2 Then
      Triggered = True
    End If
  Next I
  
  If Triggered Then
    Svar = MsgBox(CheckLang("En eller flera larmniv�er har passerats. Vill du �ppna bevakningsf�nstret?"), vbYesNo, CheckLang("Meddelande, bevakning"))
  End If

  If Svar = vbYes Then
    frmTriggers.Show
  End If
End If
End Sub
