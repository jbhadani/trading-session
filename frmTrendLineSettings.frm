VERSION 5.00
Begin VB.Form frmTrendLineSettings 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Inst�llningar f�r trendlinjer"
   ClientHeight    =   2130
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6090
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2130
   ScaleWidth      =   6090
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   0
      TabIndex        =   7
      Top             =   1320
      Width           =   6015
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   4560
         TabIndex        =   9
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Height          =   375
         Left            =   480
         TabIndex        =   8
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Trendlinjer"
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6015
      Begin VB.ComboBox cLineType 
         Height          =   315
         ItemData        =   "frmTrendLineSettings.frx":0000
         Left            =   240
         List            =   "frmTrendLineSettings.frx":0010
         Style           =   2  'Dropdown List
         TabIndex        =   3
         Top             =   720
         Width           =   1575
      End
      Begin VB.ComboBox cLineWidth 
         Height          =   315
         ItemData        =   "frmTrendLineSettings.frx":0040
         Left            =   2040
         List            =   "frmTrendLineSettings.frx":004D
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   720
         Width           =   1575
      End
      Begin VB.PictureBox csTrendLineColor 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   4560
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   1
         Top             =   720
         Width           =   375
      End
      Begin VB.Label Label2 
         Caption         =   "F�rg p� trendlinjer"
         Height          =   255
         Left            =   4200
         TabIndex        =   6
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label3 
         Caption         =   "Linjetyp"
         Height          =   255
         Left            =   360
         TabIndex        =   5
         Top             =   360
         Width           =   975
      End
      Begin VB.Label Label6 
         Caption         =   "Linjetjocklek"
         Height          =   255
         Left            =   2160
         TabIndex        =   4
         Top             =   360
         Width           =   1335
      End
   End
End
Attribute VB_Name = "frmTrendLineSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cLineWidth_Click()
  Select Case cLineWidth.ListIndex
    Case 0
      cLineType.Enabled = True
    Case 1
      cLineType.Enabled = False
    Case 2
      cLineType.Enabled = False
  End Select
End Sub

Private Sub Command1_Click()
  UpdateTrendLineSettings
  SaveTrendLineSettingsToRegistry
  Unload Me
End Sub

Private Sub Command2_Click()
  Unload frmTrendLineSettings
End Sub

Private Sub csTrendLineColor_Click()
  MainMDI.CommonDialog1.ShowColor
  frmTrendLineSettings.csTrendLineColor.BackColor = MainMDI.CommonDialog1.Color
End Sub

Sub UpdateTrendLineSettings()
  LineSettings.LineType = cLineType.ListIndex + 1
  LineSettings.LineWidth = cLineWidth.ListIndex + 1
  LineSettings.LineColor = csTrendLineColor.BackColor
  With Frm_OwnGraph
  For i = 0 To 19
    .TrendLine(i).BorderWidth = LineSettings.LineWidth
    .TrendLine(i).BorderStyle = LineSettings.LineType
    .TrendLine(i).BorderColor = LineSettings.LineColor
  Next i
  End With
  
  If ShowGraph Then
    UpdateGraph
  End If

End Sub

Sub SaveTrendLineSettingsToRegistry()
  With LineSettings
    SaveSetting App.Title, "TrendLines", "LineType", Str$(.LineType)
    SaveSetting App.Title, "TrendLines", "LineWidth", Str$(.LineWidth)
    SaveSetting App.Title, "TrendLines", "LineColor", Str$(.LineColor)
  End With
End Sub

Private Sub Form_Load()
  cLineType.ListIndex = LineSettings.LineType - 1
  cLineWidth.ListIndex = LineSettings.LineWidth - 1
  csTrendLineColor.BackColor = LineSettings.LineColor
  Select Case cLineWidth.ListIndex
    Case 0
      cLineType.Enabled = True
    Case 1
      cLineType.Enabled = False
    Case 2
      cLineType.Enabled = False
  End Select
End Sub

