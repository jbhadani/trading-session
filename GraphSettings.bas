Attribute VB_Name = "GraphSettingsModule"
Option Explicit

Sub SaveGraphSettingsToRegistry()
  With GraphSettings
  SaveSetting App.Title, "GraphSettings", "DayWidth", CStr(.lngDayWidth)
  SaveSetting App.Title, "GraphSettings", "DaySpace", CStr(.lngDaySpace)
  SaveSetting App.Title, "GraphSettings", "CloseWidth", CStr(.lngCloseWidth)
 
  If .ShowHLC Then
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "False"
  End If
  If .ShowZero Then
    SaveSetting App.Title, "GraphSettings", "ShowZero", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowZero", "False"
  End If
    
  If .ShowXGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "False"
  End If
  If .ShowYGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "False"
  End If
  If .ShowPriceMAV1 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "False"
  End If
  If .ShowPriceMAV2 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "False"
  End If
  'SaveSetting App.Title, "GraphSettings", "PriceMAV1Color", cstr(.PriceMAV1Color)
  'SaveSetting App.Title, "GraphSettings", "PriceMAV2Color", cstr(.PriceMAV2Color)
  If .ShowIndicator1 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "False"
  End If
  If .ShowIndicator2 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "False"
  End If
  If .ShowSignalColor Then
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "False"
  End If
  If .ExponentialPrice Then
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "False"
  End If
  If .ShowVerticalColorLines Then
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "False"
  End If
  
  'Bollinger
  If .ShowBollinger Then
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "False"
  End If
  
  'Parabolic
  If .ShowParabolic Then
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "False"
  End If
  If .ShowVolume Then
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "False"
  End If
  
  SaveSetting App.Title, "GraphSettings", "ColorSignallingIndicator", .strColorSignallingIndicator
  
  If .blnShowFish Then
    SaveSetting App.Title, "GraphSettings", "ShowFish", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowFish", "False"
  End If
  
  
  End With
  
End Sub
