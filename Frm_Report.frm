VERSION 5.00
Object = "{02B5E320-7292-11CF-93D5-0020AF99504A}#1.0#0"; "MSCHART.OCX"
Begin VB.Form Frm_Report 
   BackColor       =   &H00FFFFFF&
   Caption         =   "Form1"
   ClientHeight    =   7275
   ClientLeft      =   3540
   ClientTop       =   3435
   ClientWidth     =   12285
   LinkTopic       =   "Frm_Report"
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   7275
   ScaleWidth      =   12285
   Begin VB.CommandButton Command1 
      Caption         =   "Test portgraf"
      Height          =   495
      Left            =   1680
      TabIndex        =   1
      Top             =   6000
      Width           =   1815
   End
   Begin MSChartLib.MSChart MSChart1 
      Height          =   5175
      Left            =   480
      OleObjectBlob   =   "Frm_Report.frx":0000
      TabIndex        =   0
      Top             =   120
      Width           =   7935
   End
   Begin VB.Menu M_Test 
      Caption         =   "Test"
   End
End
Attribute VB_Name = "Frm_Report"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub TabStrip1_Click()

End Sub

Private Sub Form_Click()
   With MSChart1
        ' Displays a 3d chart with 8 columns and 8 rows
        ' data.
        .chartType = VtChChartType3dBar
        .ColumnCount = 8
        .RowCount = 8
        For Column = 1 To 8
            For Row = 1 To 8
                .Column = Column
                .Row = Row
                .Data = Row * 10
            Next Row
        Next Column
        ' Use the chart as the backdrop of the legend.
        .ShowLegend = True
        .SelectPart VtChPartTypePlot, index1, index2, _
   index3, index4
        .EditCopy
        .SelectPart VtChPartTypeLegend, index1, _
        index2, index3, index4
        .EditPaste
    End With
 
End Sub

