VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Frm_Search 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "S�k k�p/s�lj signaler"
   ClientHeight    =   4455
   ClientLeft      =   2130
   ClientTop       =   1335
   ClientWidth     =   7995
   Icon            =   "Frm_Search.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4455
   ScaleWidth      =   7995
   Begin TabDlg.SSTab SSTab1 
      Height          =   4455
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   7935
      _ExtentX        =   13996
      _ExtentY        =   7858
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "S�kning"
      TabPicture(0)   =   "Frm_Search.frx":0442
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame4"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame2"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame1"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame5"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame3"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Resultat"
      TabPicture(1)   =   "Frm_Search.frx":045E
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "MSFlexGrid1"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).ControlCount=   1
      Begin VB.Frame Frame3 
         Caption         =   "Parametrar"
         Height          =   855
         Left            =   5520
         TabIndex        =   23
         Top             =   480
         Width           =   2295
         Begin VB.CommandButton Command3 
            Caption         =   "Parametrar >>"
            Height          =   375
            Left            =   480
            TabIndex        =   2
            Top             =   360
            Width           =   1215
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Vilka signaler skall s�kas"
         Height          =   1215
         Left            =   5520
         TabIndex        =   19
         Top             =   2400
         Width           =   2295
         Begin VB.OptionButton OptionBoth 
            Caption         =   "B�de K�p och S�lj"
            Height          =   255
            Left            =   360
            TabIndex        =   22
            Top             =   840
            Value           =   -1  'True
            Width           =   1695
         End
         Begin VB.OptionButton OptionOnlySell 
            Caption         =   "Endast S�ljsignaler"
            Height          =   255
            Left            =   360
            TabIndex        =   21
            Top             =   600
            Width           =   1695
         End
         Begin VB.OptionButton OptionOnlyBuy 
            Caption         =   "Endast K�psignaler"
            Height          =   255
            Left            =   360
            TabIndex        =   20
            Top             =   330
            Width           =   1695
         End
      End
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3735
         Left            =   -74880
         TabIndex        =   18
         Top             =   600
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   6588
         _Version        =   393216
         Rows            =   4
         Cols            =   15
         FixedCols       =   0
      End
      Begin VB.Frame Frame1 
         Caption         =   "Vilka Indikatorer ska ing� i s�kningen"
         Height          =   3135
         Left            =   120
         TabIndex        =   8
         Top             =   480
         Width           =   5295
         Begin VB.CheckBox chParabolic 
            Caption         =   "Parabolic, SAR"
            Height          =   255
            Left            =   2760
            TabIndex        =   24
            Top             =   2280
            Width           =   1815
         End
         Begin VB.CheckBox Check_MAV 
            Caption         =   "Glidande Medelv�rde, MAV"
            Height          =   255
            Left            =   360
            TabIndex        =   17
            Top             =   360
            Width           =   2280
         End
         Begin VB.CheckBox Check_MACD 
            Caption         =   "MACD"
            Height          =   255
            Left            =   360
            TabIndex        =   16
            Top             =   840
            Width           =   2280
         End
         Begin VB.CheckBox Check_MOM 
            Caption         =   "Momentum, MOM"
            Height          =   255
            Left            =   360
            TabIndex        =   15
            Top             =   1320
            Width           =   2280
         End
         Begin VB.CheckBox Check_MFI 
            Caption         =   "Money Flow Index, MFI"
            Height          =   255
            Left            =   360
            TabIndex        =   14
            Top             =   1800
            Width           =   2280
         End
         Begin VB.CheckBox Check_PriceOsc 
            Caption         =   "Pris Oscillator"
            Height          =   255
            Left            =   360
            TabIndex        =   13
            Top             =   2280
            Width           =   2280
         End
         Begin VB.CheckBox Check_RSI 
            Caption         =   "Relative Strength Index, RSI"
            Height          =   255
            Left            =   2760
            TabIndex        =   12
            Top             =   360
            Width           =   2400
         End
         Begin VB.CheckBox Check_STO 
            Caption         =   "Stochastic Oscillator, STO"
            Height          =   255
            Left            =   2760
            TabIndex        =   11
            Top             =   840
            Width           =   2280
         End
         Begin VB.CheckBox CheckCoppock 
            Caption         =   "Coppock"
            Height          =   255
            Left            =   2760
            TabIndex        =   10
            Top             =   1320
            Width           =   1815
         End
         Begin VB.CheckBox CheckkeyReversal 
            Caption         =   "Key Reversal"
            Height          =   255
            Left            =   2760
            TabIndex        =   9
            Top             =   1800
            Width           =   1695
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Antal dagar som genoms�ks"
         Height          =   855
         Left            =   5520
         TabIndex        =   5
         Top             =   1440
         Width           =   2295
         Begin VB.ComboBox Combo1 
            Height          =   315
            ItemData        =   "Frm_Search.frx":047A
            Left            =   1200
            List            =   "Frm_Search.frx":047C
            Style           =   2  'Dropdown List
            TabIndex        =   6
            Top             =   330
            Width           =   975
         End
         Begin VB.Label Label2 
            Caption         =   "Antal dagar"
            Height          =   255
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Left            =   120
         TabIndex        =   4
         Top             =   3600
         Width           =   7695
         Begin VB.CommandButton Command1 
            Caption         =   "S�k"
            Default         =   -1  'True
            Height          =   375
            Left            =   720
            TabIndex        =   0
            Top             =   240
            Width           =   1095
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Avbryt"
            Height          =   375
            Left            =   5760
            TabIndex        =   1
            Top             =   240
            Width           =   1215
         End
      End
   End
End
Attribute VB_Name = "Frm_Search"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
  With Frm_Search
  If .Check_MACD = 1 Or .Check_MAV = 1 Or .Check_MFI = 1 Or .Check_MOM = 1 Or .Check_PriceOsc = 1 Or .Check_RSI = 1 Or .Check_STO = 1 Or .CheckCoppock = 1 Or .CheckkeyReversal = 1 Or .chParabolic = 1 Then
    Search
  End If
  End With
End Sub

Private Sub Command2_Click()
  Frm_Search.Hide
End Sub

Private Sub Command3_Click()
  Frm_Parameters.Show
End Sub

Private Sub Form_Load()
  Width = 8085
  Height = 4830
  Top = 200
  Left = 300
 Dim i As Long
  With Frm_Search
    For i = 0 To 9
      .Combo1.AddItem i + 1
    Next i
    Frm_Search.Combo1.Text = "1"
  End With
  With Frm_Search.MSFlexGrid1
    .ColWidth(0) = 1700
    .ColWidth(1) = 900
    .ColWidth(2) = 1000
    .ColWidth(3) = 600
    .ColWidth(4) = 700
    .ColWidth(5) = 900
    .ColWidth(6) = 900
    .ColWidth(7) = 900
    .ColWidth(8) = 900
    .ColWidth(9) = 900
    .ColWidth(10) = 900
    .ColWidth(11) = 900
    .ColWidth(12) = 900
    .ColWidth(13) = 900
    .ColWidth(14) = 900
    
    .Row = 0
    .Col = 0
    .Text = "Objektnamn"
    .Col = 1
    .Text = "Antal"
    .Col = 2
    .Text = "Senaste"
    .Col = 3
    .Text = "Typ"
    .Col = 4
    .Text = "Kurs"
    .Col = 5
    .Text = "COP"
    .Col = 6
    .Text = "MACD"
    .Col = 7
    .Text = "MAV"
    .Col = 8
    .Text = "MFI"
    .Col = 9
    .Text = "MOM"
    .Col = 10
    .Text = "OSC"
    .Col = 11
    .Text = "REV"
    .Col = 12
    .Text = "RSI"
    .Col = 13
    .Text = "STO"
    .Col = 14
    .Text = "SAR"
  End With

End Sub

Private Sub Option_Date_Click()
  With Frm_Search
    .Combo1.Enabled = False
    
  End With
End Sub

Private Sub Option_Days_Click()
  With Frm_Search
    .Combo1.Enabled = True
    
  End With
End Sub

Private Sub MSFlexGrid1_DblClick()
  Dim Test As Long
  Test = MSFlexGrid1.Col
  Select Case MSFlexGrid1.Col
    Case 0
      If LastSort = 7 Then
        MSFlexGrid1.Sort = 8
        LastSort = 8
      Else
        MSFlexGrid1.Sort = 7
        LastSort = 7
      End If
    Case 2 To 3
      If LastSort = 7 Then
        MSFlexGrid1.Sort = 8
        LastSort = 8
      Else
        MSFlexGrid1.Sort = 7
        LastSort = 7
      End If
    Case 1
      If LastSort = 4 Then
        MSFlexGrid1.Sort = 3
        LastSort = 3
      Else
        MSFlexGrid1.Sort = 4
        LastSort = 4
      End If
    Case 4 To 13
      If LastSort = 4 Then
        MSFlexGrid1.Sort = 3
        LastSort = 3
      Else
        MSFlexGrid1.Sort = 4
        LastSort = 4
      End If
  End Select
End Sub

