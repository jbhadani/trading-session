VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmIndicatortestResult 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Resultat indikatortest"
   ClientHeight    =   5325
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7770
   Icon            =   "frmIndicatortestResult.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5325
   ScaleWidth      =   7770
   Begin TabDlg.SSTab SSTab1 
      Height          =   5295
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7735
      _ExtentX        =   13653
      _ExtentY        =   9340
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabHeight       =   520
      TabCaption(0)   =   "Aktietransaktioner"
      TabPicture(0)   =   "frmIndicatortestResult.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "TradesGrid"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame1(0)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "Teststatistik"
      TabPicture(1)   =   "frmIndicatortestResult.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "StatisticsGrid"
      Tab(1).Control(1)=   "Frame1(1)"
      Tab(1).ControlCount=   2
      Begin VB.Frame Frame1 
         Height          =   735
         Index           =   1
         Left            =   -74880
         TabIndex        =   6
         Top             =   4440
         Width           =   7495
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Default         =   -1  'True
            Height          =   375
            Index           =   1
            Left            =   480
            TabIndex        =   8
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdHelp 
            Caption         =   "Hj�lp"
            Height          =   375
            Index           =   1
            Left            =   6000
            TabIndex        =   7
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   3
         Top             =   4440
         Width           =   7495
         Begin VB.CommandButton cmdHelp 
            Caption         =   "Hj�lp"
            Height          =   375
            Index           =   0
            Left            =   6000
            TabIndex        =   5
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   0
            Left            =   480
            TabIndex        =   4
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid StatisticsGrid 
         Height          =   3855
         Left            =   -74880
         TabIndex        =   2
         Top             =   480
         Width           =   7495
         _ExtentX        =   13229
         _ExtentY        =   6800
         _Version        =   393216
         FixedRows       =   0
         FixedCols       =   0
         GridLines       =   0
      End
      Begin MSFlexGridLib.MSFlexGrid TradesGrid 
         Height          =   3855
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   7495
         _ExtentX        =   13229
         _ExtentY        =   6800
         _Version        =   393216
         Rows            =   16
         FixedCols       =   0
         GridLines       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
End
Attribute VB_Name = "frmIndicatortestResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdOK_Click(Index As Integer)
  Unload Me
End Sub



Private Sub Form_Load()
  top = 400
  left = 800
End Sub

