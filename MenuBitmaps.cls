VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "MenuBitmaps"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private Const IMAGE_ICON = 1
Private Const IDI_SHIELD = 32518&  'Vista+
Private Const COLOR_MENU = 4
Private Const ETO_OPAQUE = 2

Private Enum LIMG_FLAGS
    LR_DEFAULTCOLOR = 0&
    LR_LOADTRANSPARENT = &H20&
    LR_VGACOLOR = &H80&
    LR_SHARED = &H8000&
End Enum

Private Enum SM_INDEX
    SM_CXMENUCHECK = 71&
    SM_CYMENUCHECK = 72&
End Enum

Private Enum BKMODE
    OPAQUE = 2
    TRANSPARENT = 1
End Enum

Private Enum DI_FLAGS
    DI_MASK = &H1&
    DI_IMAGE = &H2&
    DI_NORMAL = &H3&
    DI_COMPAT = &H4&
    DI_DEFAULTSIZE = &H8&
    'WINVER >= 5.1
    DI_NOMIRROR = &H10&
End Enum

Private Enum MI_MASK
    MIIM_STATE = &H1&
    MIIM_ID = &H2&
    MIIM_SUBMENU = &H4&
    MIIM_CHECKMARKS = &H8&
    MIIM_TYPE = &H10&
    MIIM_DATA = &H20&
    'WINVER >= 5.0
    MIIM_STRING = &H40&
    MIIM_BITMAP = &H80&
    MIIM_FTYPE = &H100&
End Enum

Private Enum MI_TYPE
    MFT_STRING = &H0&
    MFT_BITMAP = &H4&
    MFT_MENUBARBREAK = &H20&
    MFT_MENUBREAK = &H40&
    MFT_OWNERDRAW = &H100&
    MFT_RADIOCHECK = &H200&
    MFT_SEPARATOR = &H800&
    MFT_RIGHTORDER = &H2000&
    MFT_RIGHTJUSTIFY = &H4000&
End Enum

Private Enum MI_STATE
    MFS_GRAYED = &H3&
    MFS_DISABLED = MFS_GRAYED
    MFS_CHECKED = &H8&
    MFS_HILITE = &H80&
    MFS_ENABLED = &H0&
    MFS_UNCHECKED = &H0&
    MFS_UNHILITE = &H0&
    MFS_DEFAULT = &H1000&
End Enum

Public Enum MI_STD_HBMPITEM
    'HBMMENU_CALLBACK = -1& 'Not used here.
    HBMMENU_SYSTEM = 1&
    HBMMENU_MBAR_RESTORE = 2&
    HBMMENU_MBAR_MINIMIZE = 3&
    HBMMENU_MBAR_CLOSE = 5&
    HBMMENU_MBAR_CLOSE_D = 6&
    HBMMENU_MBAR_MINIMIZE_D = 7&
    HBMMENU_POPUP_CLOSE = 8&
    HBMMENU_POPUP_RESTORE = 9&
    HBMMENU_POPUP_MAXIMIZE = 10&
    HBMMENU_POPUP_MINIMIZE = 11&
End Enum

Private Type MENUITEMINFO
    cbSize As Long
    fMask As MI_MASK
    fType As MI_TYPE
    fState As MI_STATE
    wID As Long
    hSubMenu As Long
    hbmpChecked As Long
    hbmpUnchecked As Long
    dwItemData As Long
    dwTypeData As String
    cch As Long
    'Requires Windows 98 or later:
    hbmpItem As MI_STD_HBMPITEM
End Type

Private Type RECT
    left As Long
    top As Long
    right As Long
    bottom As Long
End Type

Private Declare Function CreateCompatibleBitmap Lib "gdi32" ( _
    ByVal hdc As Long, _
    ByVal nWidth As Long, _
    ByVal nHeight As Long) As Long

Private Declare Function CreateCompatibleDC Lib "gdi32" (ByVal hdc As Long) As Long

Private Declare Function DeleteDC Lib "gdi32" (ByVal hdc As Long) As Long

Private Declare Function DeleteObject Lib "gdi32" (ByVal hObject As Long) As Long
'
'Private Declare Function DestroyIcon Lib "user32" (ByVal hIcon As Long) As Long

Private Declare Function DrawIconEx Lib "user32" ( _
    ByVal hdc As Long, _
    ByVal xLeft As Long, _
    ByVal YtoP As Long, _
    ByVal hIcon As Long, _
    ByVal cxWidth As Long, _
    ByVal cyWidth As Long, _
    ByVal istepIfAniCur As Long, _
    ByVal hbrFlickerFreeDraw As Long, _
    ByVal diFlags As DI_FLAGS) As Long

Private Declare Function ExtTextOut Lib "gdi32" Alias "ExtTextOutA" ( _
    ByVal hdc As Long, _
    ByVal X As Long, _
    ByVal Y As Long, _
    ByVal wOptions As Long, _
    ByRef lpRect As RECT, _
    ByVal lpString As String, _
    ByVal nCount As Long, _
    lpDx As Long) As Long

Private Declare Function GetMenu Lib "user32" (ByVal hWnd As Long) As Long

Private Declare Function GetSubMenu Lib "user32" ( _
    ByVal hMenu As Long, _
    ByVal nPos As Long) As Long

Private Declare Function GetSysColor Lib "user32" (ByVal nIndex As Long) As Long

Private Declare Function GetSystemMetrics Lib "user32" (ByVal nIndex As SM_INDEX) As Long

Private Declare Function GetVersion Lib "kernel32" () As Long

Private Declare Function LoadImage Lib "user32" Alias "LoadImageA" ( _
    ByVal hinst As Long, _
    ByVal lpszName As Long, _
    ByVal uType As Long, _
    ByVal cxDesired As Long, _
    ByVal cyDesired As Long, _
    ByVal fuLoad As LIMG_FLAGS) As Long

Private Declare Function SelectObject Lib "gdi32" ( _
    ByVal hdc As Long, _
    ByVal hObject As Long) As Long

Private Declare Function SetBkColor Lib "gdi32" ( _
    ByVal hdc As Long, _
    ByVal crColor As Long) As Long

Private Declare Function SetBkMode Lib "gdi32" ( _
    ByVal hdc As Long, _
    ByVal nBkMode As BKMODE) As Long

Private Declare Function SetMenuItemInfo Lib "user32" Alias "SetMenuItemInfoA" ( _
    ByVal hMenu As Long, _
    ByVal uItem As Long, _
    ByVal fByPosition As Boolean, _
    ByRef lpmii As MENUITEMINFO) As Long

Private CreatedHBmps As Collection

Private mMenuColor As Long
Private mLastCall As String
Private mLastError As Long

Public Property Get LastCall() As String
    LastCall = mLastCall
End Property

Public Property Get LastError() As Long
    LastError = mLastError
End Property

Public Property Get MenuColor() As Long
    If mMenuColor = 0 Then
        mMenuColor = GetSysColor(COLOR_MENU)
    End If
    MenuColor = mMenuColor
End Property

Public Sub Assign( _
    ByVal Form As Object, _
    ByVal hBmp As MI_STD_HBMPITEM, _
    ParamArray MenuPos() As Variant)
    'Calls must pass:
    '
    '   o Form with menu to modify.
    '
    '   o A bitmap handle (or "standard bitmap handle" value) of
    '     the image to apply.  This bitmap should have the background
    '     color COLOR_MENU (MenuColor property provided above).
    '
    '     NOTE: To clear a previously set bitmap you may pass a 0 (NULL)
    '           handle value here.
    '
    '   o A list of hierarchical menu positions and the menu position
    '     of the menuitem to modify.
    '
    'Menu "positions" are 0-based.
    '
    'If MenuPos has less than two position values then Error 5 is raised.
    '
    'If system errors occur Error &H80045800 is raised and properties
    'LastCall and LastError provide additional information.
    '
    'Example:
    '
    '   Set Form1's 1st menuitem in its 1st menu (submenu) to the standard
    '   "close" bitmap.
    '
    '       MenuBitmaps.Assign Form1, HBMMENU_MBAR_CLOSE, 0, 0
    '
    
    Dim MenuPosUB As Integer
    Dim hMenu As Long
    Dim I As Integer
    Dim MII As MENUITEMINFO
    
    mLastCall = ""
    mLastError = 0
    
    MenuPosUB = UBound(MenuPos)
    If MenuPosUB < 1 Then Err.Raise 5, TypeName(Me)
    
    hMenu = GetMenu(Form.hWnd)
    For I = 0 To MenuPosUB - 1
        hMenu = GetSubMenu(hMenu, MenuPos(I))
    Next
    With MII
        .cbSize = Len(MII)
        .fMask = MIIM_BITMAP
        .hbmpItem = hBmp
    End With
    
    If SetMenuItemInfo(hMenu, MenuPos(MenuPosUB), True, MII) = 0 Then
        mLastCall = "SetMenuItemInfo"
        mLastError = Err.LastDllError
        Err.Raise &H80045800, TypeName(Me), "System error."
    End If
End Sub

Public Function GetHBmpUacShield(ByVal Form As Object) As Long
    'Create and return a UAC Shield icon menu hBmp.
    'If running on a pre-Vista system a 0 (NULL) is
    'returned, which is also Ok to use with Assign().
    Dim hIcon As Long
    Dim cx As Long
    Dim cy As Long
    If GetVersion() And &HFF& >= 6 Then
        cx = GetSystemMetrics(SM_CXMENUCHECK)
        cy = GetSystemMetrics(SM_CYMENUCHECK)
        hIcon = LoadImage(0, _
                          IDI_SHIELD, _
                          IMAGE_ICON, _
                          cx, _
                          cy, _
                          LR_VGACOLOR Or LR_LOADTRANSPARENT Or LR_SHARED)
        GetHBmpUacShield = HIconToMenuHBitmap(Form, hIcon)
    End If
End Function

Public Function HIconToMenuHBitmap(ByVal Form As Object, ByVal hIcon As Long) As Long
    Dim RECT As RECT
    Dim hCompatDc As Long
    With RECT
        .right = GetSystemMetrics(SM_CXMENUCHECK)
        .bottom = GetSystemMetrics(SM_CYMENUCHECK)
        hCompatDc = CreateCompatibleDC(Form.hdc)
        HIconToMenuHBitmap = CreateCompatibleBitmap(Form.hdc, .right, .bottom)
        SelectObject hCompatDc, HIconToMenuHBitmap
        SetBkMode hCompatDc, TRANSPARENT
        SetBkColor hCompatDc, MenuColor
        ExtTextOut hCompatDc, 0, 0, ETO_OPAQUE, RECT, vbNullString, 0, 0
        DrawIconEx hCompatDc, 0, 0, hIcon, .right, .bottom, 0, 0, DI_NORMAL
        DeleteDC hCompatDc
    End With
    CreatedHBmps.Add HIconToMenuHBitmap
End Function

Private Sub Class_Initialize()
    Set CreatedHBmps = New Collection
End Sub

Private Sub Class_Terminate()
    Dim I As Integer
    
    With CreatedHBmps
        For I = 1 To .Count
            DeleteObject .Item(I)
        Next
    End With
End Sub


