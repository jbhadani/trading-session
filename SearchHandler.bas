Attribute VB_Name = "SearchHandler"
Public Sub Search()
  Dim Loops As Long
  Dim NbrOfInvestments As Long
  Dim NbrOfObjectsWithSignal As Single
  Dim NbrOfSignals As Single
  Dim Signals() As SignalType
  Dim FromDate As Date
  Dim NbrOFDays As Long
  Dim Filename1 As String
  Dim PDArray() As PriceDataType
  Dim Size As Long
  Dim InvestName As String
  Dim SearchDate As Date
  Dim BuySignals As Boolean
  Dim SellSignals As Boolean
  Dim OldActiveindex As Long
  Dim LastObjectWithSignal As String
  Dim Signalarray() As Long
  Dim Method As String

  LastObjectWithSignal = ""
  
  MainMDI.Enabled = False
  frmSearch.Enabled = False

  
  NbrOfInvestments = MainMDI.ID_Object.ListCount
  NbrOFDays = Val(frmSearch.Combo1.Text)

  BuySignals = False
  SellSignals = False
  With frmSearch
    If .OptionOnlyBuy = True Or .OptionBoth = True Then
      BuySignals = True
    End If
    If .OptionOnlySell = True Or .OptionBoth = True Then
      SellSignals = True
    End If
  End With
  
  Loops = 1
  NbrOfObjectsWithSignal = 0
  NbrOfSignals = 0
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "S�ker..."
  End With
   
  DoEvents
  OldActiveindex = ActiveIndex
  
  Do Until Loops > NbrOfInvestments
    
    Frm_Progress.PB_1.Value = Int(Loops / NbrOfInvestments * 100)
    Frm_Progress.Label1.Caption = Str$(Int(Loops / NbrOfInvestments * 100)) + " % �r klart."
    
    DoEvents
    
    With frmSearch
    
      Filename1 = DataPath + Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).FileName) + ".prc"
    
      If Not UseGeneralIndicatorParameters Then
        GetParameterSettingsFromInvest GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))
      End If
    
      R_PriceDataArray Filename1, PDArray(), Size
     
      If Size > 2 Then
    
        If .Check_MAV.Value = 1 Then
          Dim MAV1() As Single
          Dim MAV2() As Single
          ReDim MAV1(Size)
          ReDim MAV2(Size)
      
          If ParameterSettings.MAVUseExp Then
            ExpMAVCalc PDArray(), MAV1(), ParameterSettings.MAV1, Size
            ExpMAVCalc PDArray(), MAV2(), ParameterSettings.MAV2, Size
          Else
            SimpleMAVCalc PDArray(), MAV1(), ParameterSettings.MAV1, Size, True
            SimpleMAVCalc PDArray(), MAV2(), ParameterSettings.MAV2, Size, True
          End If
        
          MAVSignalArray Signalarray(), MAV1(), MAV2(), Size
        
          SearchForSignals Signals(), Signalarray(), PDArray(), "MAV", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
    
        If .Check_MACD.Value = 1 Then
          Dim MACD() As Single
          Dim MACDSignal() As Single
          ReDim MACD(Size)
          ReDim MACDSignal(Size)
      
          MACDCalc PDArray(), MACD(), MACDSignal(), Size, True
          
          MACDSignalArray Signalarray(), MACD(), MACDSignal(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "MACD", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
       
        If .Check_MOM = 1 Then
          Dim MOMArray() As Single
          Dim MOMMAV() As Single
          ReDim MOMArray(Size)
          ReDim MOMMAV(Size)
          
          MomentumCalc PDArray(), MOMArray(), ParameterSettings.MOM, Size, True
          ExpCalc MOMArray(), MOMMAV(), ParameterSettings.MAVMOM, Size
          
          MomentumSignalArray Signalarray(), MOMArray(), MOMMAV(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "MOM", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
    
        If .Check_MFI.Value = 1 Then
          Dim MFI() As Single
          Dim MFIMAV() As Single
          ReDim MFI(Size)
          ReDim MFIMAV(Size)
         
          MFICalc PDArray(), MFI(), ParameterSettings.MFILength, Size, True
                   
          ExpCalc MFI(), MFIMAV(), ParameterSettings.MAVMFI, Size
          
          MFISignalArray Signalarray(), MFI, MFIMAV(), Size
                   
          SearchForSignals Signals(), Signalarray(), PDArray(), "MFI", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
    
        If .Check_PriceOsc.Value = 1 Then
          Dim MAV3() As Single
          Dim MAV4() As Single
          ReDim MAV3(Size)
          ReDim MAV4(Size)
          
    
          If Not ParameterSettings.OSCMAVExp Then
            SimpleMAVCalc PDArray(), MAV3(), ParameterSettings.PRIOSC1, Size, True
            SimpleMAVCalc PDArray(), MAV4(), ParameterSettings.PRIOSC2, Size, True
          Else
            ExpMAVCalc PDArray(), MAV3(), ParameterSettings.PRIOSC1, Size
            ExpMAVCalc PDArray(), MAV4(), ParameterSettings.PRIOSC2, Size
          End If
    
          MAVSignalArray Signalarray(), MAV3(), MAV4(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "OSC", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
    
        If .Check_RSI.Value = 1 Then
          Dim RSIArray() As Single
          Dim RSIMAV() As Single
          ReDim RSIArray(Size)
          ReDim RSIMAV(Size)
    
          RSICalc PDArray(), RSIArray(), ParameterSettings.MAVRSI, Size, True
          ExpCalc RSIArray(), RSIMAV(), ParameterSettings.RSI, Size
          
          RSISignalArray Signalarray(), RSIArray(), RSIMAV(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "RSI", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
    
        If .Check_STO.Value = 1 Then
          Dim STOArray() As Single
          Dim STOMAV() As Single
          ReDim STOArray(Size)
          ReDim STOMAV(Size)
    
          StochasticOscCalc PDArray(), STOArray(), ParameterSettings.STO, Size, True, ParameterSettings.STOSlowing
          ExpCalc STOArray(), STOMAV(), ParameterSettings.MAVSTO, Size
          
          STOSignalArray Signalarray(), STOArray(), STOMAV(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "STO", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
     
        If .chParabolic.Value = 1 Then
          Dim Parabolic() As Single
          Dim ParabolicSearch() As Boolean
         
          ParabolicCalc PDArray, Parabolic(), ParabolicSearch(), Size, True, ParameterSettings.AccelerationFactor
          
          ParabolicSignalArray Signalarray(), ParabolicSearch(), Size
         
          SearchForSignals Signals(), Signalarray(), PDArray(), "SAR", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
        
        If .chROC.Value = 1 Then
          Dim ROC() As Single
          Dim ROCMAV() As Single
          
          RateOfChangeCalc PDArray(), ROC(), ParameterSettings.RateOfChangeLength, Size, True
          
          If ParameterSettings.PlotRateOfChangeAndMAV Then
            SimpleCalc ROC, ROCMAV, ParameterSettings.RateOfChangeMAVLength, Size, True
            RateOfChangeSignalArray Signalarray(), ROCMAV(), Size
          Else
            RateOfChangeSignalArray Signalarray(), ROC(), Size
          End If
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "ROC", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
        
        If .chEaseOfMovement.Value = 1 Then
          Dim EOM() As Single
          
          EaseOfMovementCalc PDArray(), EOM(), ParameterSettings.EaseOfMovementLength, Size, True
          
          EaseOfMovementSignalArray Signalarray(), EOM(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "EOM", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
        
        If .chTRIX.Value = 1 Then
          Dim TRIX() As Single
          Dim TRIXMAV() As Single
          
          ReDim TRIX(Size)
          ReDim TRIXMAV(Size)
          
          TRIXCalc PDArray(), TRIX(), ParameterSettings.TRIXLength, Size, True
          ExpCalc TRIX(), TRIXMAV(), ParameterSettings.TRIXMAVLength, Size
          
          TRIXSignalArray Signalarray(), TRIX(), TRIXMAV(), Size
          
          SearchForSignals Signals(), Signalarray(), PDArray(), "TRIX", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, BuySignals, SellSignals
        End If
        
        If .CheckkeyReversal Then
          Dim KeyReversalDay() As Boolean
          ReDim KeyReversalDay(Size)
          
          KeyReversalDayCalc PDArray, KeyReversalDay
          
          SearchForSetup Signals(), KeyReversalDay(), PDArray(), "Key Reversal Day", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
        
        If .chReversalDay Then
          Dim ReversalDay() As Boolean
          ReDim ReversalDay(Size)
          
          ReversalDayCalc PDArray, ReversalDay
          
          SearchForSetup Signals(), ReversalDay(), PDArray(), "Reversal Day", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
        
        If .chTwoDayReversal Then
          Dim TwoDayReversal() As Boolean
          ReDim TwoDayReversal(Size)
          
          TwoDayReversalCalc PDArray, TwoDayReversal
          
          SearchForSetup Signals(), TwoDayReversal(), PDArray(), "Two Day Reversal", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
        
        If .chOneDayReversal Then
          Dim OneDayReversal() As Boolean
          ReDim OneDayReversal(Size)
          
          OneDayReversalCalc PDArray, OneDayReversal
          
          SearchForSetup Signals(), OneDayReversal(), PDArray(), "One Day Reversal", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
        
        If .chPatternGap Then
          Dim PatternGap() As Boolean
          ReDim PatternGap(Size)
          
          PatternGapCalc PDArray, PatternGap
          
          SearchForSetup Signals(), PatternGap(), PDArray(), "Pattern Gap", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
        
        If .chReversalGap Then
          Dim ReversalGap() As Boolean
          ReDim ReversalGap(Size)
          
          ReversalGapCalc PDArray, ReversalGap
          
          SearchForSetup Signals(), ReversalGap(), PDArray(), "Reversal Gap", Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals
        End If
      End If
    End With
    Loops = Loops + 1
  Loop
  
  Frm_Progress.PB_1.Value = 100
  Unload Frm_Progress
  
  ReDim SearchResultArray(NbrOfObjectsWithSignal)
  SearchResultArraySize = NbrOfObjectsWithSignal
  
  For i = 1 To NbrOfObjectsWithSignal
    SearchResultArray(i) = Signals(i).Name
  Next i
  
  'Bortttaget 991209 ML.
  'If NbrOfSignals > 200 Then
  '  Svar = MsgBox(CheckLang("S�kningen gav ") & NbrOfSignals & CheckLang(" tr�ffar. Vill du skriva ut dessa p� sk�rmen?"), 35, CheckLang("Forts�ttta?"))
  '  If Svar = 2 Or Svar = 7 Then
  '    MainMDI.ID_Object.Locked = False
  '    MainMDI.SSActiveToolBars1.Tools("ID_Group").ComboBox.Locked = False
  '    Exit Sub
  '  End If
  'End If
  
  MainMDI.MousePointer = vbHourglass
  
  UpdateSearchResultGrid Signals(), NbrOfObjectsWithSignal, NbrOfSignals
  
  MainMDI.MousePointer = vbDefault
  
  MainMDI.Enabled = True
  frmSearch.Enabled = True
  
End Sub


Public Sub SearchForSignals(Signals() As SignalType, Signalarray() As Long, PDArray() As PriceDataType, Method As String, Loops As Long, NbrOfObjectsWithSignal As Single, LastObjectWithSignal As String, NbrOfSignals As Single, BuySignals As Boolean, SellSignals As Boolean)
  Dim Start As Long
  Dim Stopp As Long
  Dim i As Long
  
  Start = UBound(PDArray()) - Val(frmSearch.Combo1.Text) + 1
  
  If Start < 2 Then
    Start = 2
  End If
  
  Stopp = UBound(PDArray())
  
  
  '''''''''''''''''''''''''''''''''''''''''
  'Modified: 00-02-05
  'By: ML
  'Bugfix: S�kfunktionen gav felaktigt resultat vid s�kning. K�p
  'blev s�lj och tv�rtom. Detta �r nu avhj�lpt.
  '''''''''''''''''''''''''''''''''''''''''
  For i = Start To Stopp
    If Signalarray(i) = -1 And Signalarray(i - 1) = 1 And SellSignals Then
      LastObjectWithSignal = Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).Name)
      UpdateSignalArray Signals(), PDArray(), Method, Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, False, i
    ElseIf Signalarray(i) = 1 And Signalarray(i - 1) = -1 And BuySignals Then
      LastObjectWithSignal = Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).Name)
      UpdateSignalArray Signals(), PDArray(), Method, Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, True, i
    End If
  Next i
  
End Sub

Public Sub SearchForSetup(Signals() As SignalType, Signalarray() As Boolean, PDArray() As PriceDataType, Method As String, Loops As Long, NbrOfObjectsWithSignal As Single, LastObjectWithSignal As String, NbrOfSignals As Single)
  Dim Start As Long
  Dim Stopp As Long
  Dim i As Long
  
  Start = UBound(PDArray()) - Val(frmSearch.Combo1.Text) + 1
  
  If Start < 2 Then
    Start = 2
  End If
  
  Stopp = UBound(PDArray())
  
  For i = Start To Stopp
    If Signalarray(i) = True And Signalarray(i - 1) = False Then
      LastObjectWithSignal = Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).Name)
      UpdateSignalArray Signals(), PDArray(), Method, Loops, NbrOfObjectsWithSignal, LastObjectWithSignal, NbrOfSignals, True, i
    End If
  Next i
  
End Sub

Public Sub UpdateSignalArray(Signals() As SignalType, PDArray() As PriceDataType, Method As String, Loops As Long, NbrOfObjectsWithSignal As Single, LastObjectWithSignal As String, NbrOfSignals As Single, TypeOfSignal As Boolean, j As Long)
  If NbrOfObjectsWithSignal <> 0 Then
    NbrOfSignals = NbrOfSignals + 1
    If LastObjectWithSignal <> Trim(Signals(NbrOfObjectsWithSignal).Name) Then
      NbrOfObjectsWithSignal = NbrOfObjectsWithSignal + 1
      ReDim Preserve Signals(NbrOfObjectsWithSignal)
      Signals(NbrOfObjectsWithSignal).Name = Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).Name)
    End If
  Else
    NbrOfSignals = 1
    NbrOfObjectsWithSignal = 1
    ReDim Preserve Signals(NbrOfObjectsWithSignal)
    Signals(NbrOfObjectsWithSignal).Name = Trim(InvestInfoArray(GetInvestIndex(MainMDI.ID_Object.List(Loops - 1))).Name)
  End If
  With Signals(NbrOfObjectsWithSignal)
    If Int(PDArray(j).Date) > Int(.LastDate) Then
      .LastDate = PDArray(j).Date
      .LastType = TypeOfSignal
      .LastPrice = PDArray(j).Close
    End If
    
    If TypeOfSignal Then
      .NbrOfBuySignalsForObject = .NbrOfBuySignalsForObject + 1
      Select Case Method
        Case "COP"
          .NbrOfCOP = .NbrOfCOP + 1
        Case "MACD"
          .NbrOfMACD = .NbrOfMACD + 1
        Case "MAV"
          .NbrOfMAV = .NbrOfMAV + 1
        Case "MFI"
          .NbrOfMFI = .NbrOfMFI + 1
        Case "MOM"
          .NbrOfMOM = .NbrOfMOM + 1
        Case "OSC"
          .NbrOfOSC = .NbrOfOSC + 1
        Case "RSI"
          .NbrOfRSI = .NbrOfRSI + 1
        Case "STO"
          .NbrOfSTO = .NbrOfSTO + 1
        Case "SAR"
          .NbrOfSAR = .NbrOfSAR + 1
        Case "EOM"
          .NbrOfEOM = .NbrOfEOM + 1
        Case "ROC"
          .NbrOfROC = .NbrOfROC + 1
        Case "TRIX"
          .NbrOfTRIX = .NbrOfTRIX + 1
          
        Case "Key Reversal Day"
          .NbrOfREV = .NbrOfREV + 1
        Case "Reversal Day"
          .NbrOfReversalDay = .NbrOfReversalDay + 1
        Case "Two Day Reversal"
          .NbrOfTwoDayReversal = .NbrOfTwoDayReversal + 1
        Case "One Day Reversal"
          .NbrOfOneDayReversal = .NbrOfOneDayReversal + 1
        Case "Pattern Gap"
          .NbrOfPatternGap = .NbrOfPatternGap + 1
        Case "Reversal Gap"
          .NbrOfReversalGap = .NbrOfReversalGap + 1
      End Select
    Else
      .NbrOfSellSignalsForObject = .NbrOfSellSignalsForObject + 1
      Select Case Method
        Case "COP"
          .NbrOfCOPSell = .NbrOfCOPSell + 1
        Case "MACD"
          .NbrOfMACDSell = .NbrOfMACDSell + 1
        Case "MAV"
          .NbrOfMAVSell = .NbrOfMAVSell + 1
        Case "MFI"
          .NbrOfMFISell = .NbrOfMFISell + 1
        Case "MOM"
          .NbrOfMOMSell = .NbrOfMOMSell + 1
        Case "OSC"
          .NbrOfOSCSell = .NbrOfOSCSell + 1
        Case "RSI"
          .NbrOfRSISell = .NbrOfRSISell + 1
        Case "STO"
          .NbrOfSTOSell = .NbrOfSTOSell + 1
        Case "SAR"
          .NbrOfSARSell = .NbrOfSARSell + 1
        Case "EOM"
          .NbrOfEOMSell = .NbrOfEOMSell + 1
        Case "ROC"
          .NbrOfROCSell = .NbrOfROCSell + 1
        Case "TRIX"
          .NbrOfTRIXSell = .NbrOfTRIXSell + 1
          
        Case "Key Reversal Day"
          .NbrOfREVSell = .NbrOfREVSell + 1
        Case "Reversal Day"
          .NbrOfReversalDaySell = .NbrOfReversalDaySell + 1
        Case "Two Day Reversal"
          .NbrOfTwoDayReversalSell = .NbrOfTwoDayReversalSell + 1
        Case "One Day Reversal"
          .NbrOfOneDayReversalSell = .NbrOfOneDayReversalSell + 1
        Case "Pattern Gap"
          .NbrOfPatternGapSell = .NbrOfPatternGapSell + 1
        Case "Reversal Gap"
          .NbrOfReversalGapSell = .NbrOfReversalGapSell + 1
      End Select
    End If
     
  End With
End Sub

Public Sub UpdateSearchResultGrid(Signals() As SignalType, NbrOfObjectsWithSignal As Single, NbrOfSignals As Single)
  Dim i As Long
  Dim j As Long
  Dim NbrofMethods As Long
  Dim Krav As Long
  Dim Temp1 As Long
  Dim Temp2 As Long
  Dim UpdateOK As Boolean
  Dim TotalSignals As Long
  Dim CellBackColor As Long
  Dim Size As Long
  
  Krav = frmSearch.txtKrav
  NbrofMethods = Val(frmSearch.Label3.Caption)
  If Krav > NbrofMethods Then
    Krav = NbrofMethods
  End If
  frmSearchResult.MSFlexGrid1.Redraw = False
  CellBackColor = RGB(255, 255, 170)
  
  On Error Resume Next
    Size = UBound(Signals())
    If Err.Number <> 0 Then
      Size = 0
    End If
  On Error GoTo 0
  
  For i = 1 To Size
    UpdateOK = False
    With Signals(i)
      If Krav > 1 Then
        Temp1 = .NbrOfBuySignalsForObject
        Temp2 = .NbrOfSellSignalsForObject
        If Temp1 >= Krav Or Temp2 >= Krav Then
          UpdateOK = True
        End If
      Else
        UpdateOK = True
      End If
    End With
    
    If UpdateOK Then
      With frmSearchResult.MSFlexGrid1
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 0
        .CellFontBold = True
        .CellBackColor = CellBackColor
        .Text = Trim(Signals(i).Name)
        .Col = 1
        .CellFontBold = True
        .CellBackColor = CellBackColor
        .Text = "Antal"
        .Col = 2
        .CellFontBold = True
        .CellBackColor = CellBackColor
        TotalSignalsForObject = Signals(i).NbrOfBuySignalsForObject + Signals(i).NbrOfSellSignalsForObject
        If Signals(i).NbrOfBuySignalsForObject > Signals(i).NbrOfSellSignalsForObject Then
          .CellForeColor = QBColor(2)
          .CellFontBold = True
          .Text = TotalSignalsForObject
        Else
          .CellForeColor = QBColor(12)
          .CellFontBold = True
          .Text = TotalSignalsForObject
        End If
        .Col = 3
        .CellFontBold = True
        .CellBackColor = CellBackColor
        If Signals(i).LastType Then
          .CellForeColor = QBColor(2)
          .Text = Str$(Signals(i).LastDate)
        Else
          .CellForeColor = QBColor(12)
          .Text = Str$(Signals(i).LastDate)
        End If
        .Col = 4
        .CellFontBold = True
        .CellBackColor = CellBackColor
        .Text = FormatCurrency((Signals(i).LastPrice))
        
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 1
        .CellForeColor = QBColor(2)
        .Text = "K�psignaler"
        .Col = 2
        .CellForeColor = QBColor(2)
        .Text = Signals(i).NbrOfBuySignalsForObject
        
        .Rows = .Rows + 1
        .Row = .Rows - 1
        .Col = 1
        .CellForeColor = QBColor(12)
        .Text = "S�ljsignaler"
        .Col = 2
        .CellForeColor = QBColor(12)
        .Text = Signals(i).NbrOfSellSignalsForObject
          
        If (Signals(i).NbrOfMACD + Signals(i).NbrOfMACDSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "MACD"
          .Col = 2
          .Text = (Signals(i).NbrOfMACD + Signals(i).NbrOfMACDSell)
        End If
        
        If (Signals(i).NbrOfMAV + Signals(i).NbrOfMAVSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Medelv�rde"
          .Col = 2
          .Text = (Signals(i).NbrOfMAV + Signals(i).NbrOfMAVSell)
        End If
        
        If (Signals(i).NbrOfMFI + Signals(i).NbrOfMFISell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Money Flow"
          .Col = 2
          .Text = (Signals(i).NbrOfMFI + Signals(i).NbrOfMFISell)
        End If
        
        If (Signals(i).NbrOfMOM + Signals(i).NbrOfMOMSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Momentum"
          .Col = 2
          .Text = (Signals(i).NbrOfMOM + Signals(i).NbrOfMOMSell)
        End If
        
        If (Signals(i).NbrOfOSC + Signals(i).NbrOfOSCSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Oscillator"
          .Col = 2
          .Text = (Signals(i).NbrOfOSC + Signals(i).NbrOfOSCSell)
        End If
        
        If (Signals(i).NbrOfRSI + Signals(i).NbrOfRSISell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "RSI"
          .Col = 2
          .Text = (Signals(i).NbrOfRSI + Signals(i).NbrOfRSISell)
        End If
        
        If (Signals(i).NbrOfSAR + Signals(i).NbrOfSARSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Parabolic"
          .Col = 2
          .Text = (Signals(i).NbrOfSAR + Signals(i).NbrOfSARSell)
        End If
        
        If (Signals(i).NbrOfSTO + Signals(i).NbrOfSTOSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Stochastic"
          .Col = 2
          .Text = (Signals(i).NbrOfSTO + Signals(i).NbrOfSTOSell)
        End If
        
        If (Signals(i).NbrOfEOM + Signals(i).NbrOfEOMSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "EOM"
          .Col = 2
          .Text = (Signals(i).NbrOfEOM + Signals(i).NbrOfEOMSell)
        End If
        
        If (Signals(i).NbrOfROC + Signals(i).NbrOfROCSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "ROC"
          .Col = 2
          .Text = (Signals(i).NbrOfROC + Signals(i).NbrOfROCSell)
        End If
        
        If (Signals(i).NbrOfTRIX + Signals(i).NbrOfTRIXSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "TRIX"
          .Col = 2
          .Text = (Signals(i).NbrOfTRIX + Signals(i).NbrOfTRIXSell)
        End If
        
        If (Signals(i).NbrOfREV + Signals(i).NbrOfREVSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Key Reversal Day"
          .Col = 2
          .Text = (Signals(i).NbrOfREV + Signals(i).NbrOfREVSell)
        End If
        
        If (Signals(i).NbrOfReversalDay + Signals(i).NbrOfReversalDaySell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Reversal Day"
          .Col = 2
          .Text = (Signals(i).NbrOfReversalDay + Signals(i).NbrOfReversalDaySell)
        End If
        
        If (Signals(i).NbrOfTwoDayReversal + Signals(i).NbrOfTwoDayReversalSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Two Day Reversal"
          .Col = 2
          .Text = (Signals(i).NbrOfTwoDayReversal + Signals(i).NbrOfTwoDayReversalSell)
        End If
        
        If (Signals(i).NbrOfOneDayReversal + Signals(i).NbrOfOneDayReversalSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "One Day Reversal"
          .Col = 2
          .Text = (Signals(i).NbrOfOneDayReversal + Signals(i).NbrOfOneDayReversalSell)
        End If
        
        If (Signals(i).NbrOfPatternGap + Signals(i).NbrOfPatternGapSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Pattern Gap"
          .Col = 2
          .Text = (Signals(i).NbrOfPatternGap + Signals(i).NbrOfPatternGapSell)
        End If
        
        If (Signals(i).NbrOfReversalGap + Signals(i).NbrOfReversalGapSell) > 0 Then
          .Rows = .Rows + 1
          .Row = .Rows - 1
          .Col = 1
          .CellFontItalic = True
          .Text = "Reversal Gap"
          .Col = 2
          .Text = (Signals(i).NbrOfReversalGap + Signals(i).NbrOfReversalGapSell)
        End If
        
        .Rows = .Rows + 1
      End With
    End If
  Next i
  
  frmSearchResult.Label3.Caption = FormatNumber(NbrOfSignals, 0)
  frmSearchResult.Label4.Caption = FormatNumber(NbrOfObjectsWithSignal, 0)
  frmSearchResult.MSFlexGrid1.Redraw = True
  frmSearchResult.Show
  
End Sub

Public Sub UpdateKrav(ID As String)
  Dim i As Integer
  Dim increase As Boolean
  
  With frmSearch
  i = Val(.Label3.Caption)
  Select Case ID
    Case "MAV"
      If .Check_MAV = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "MACD"
      If .Check_MACD = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "MOM"
      If .Check_MOM = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "MFI"
      If .Check_MFI = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "OSC"
      If .Check_PriceOsc = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "RSI"
      If .Check_RSI = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "STO"
      If .Check_STO = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "REV"
      If .CheckkeyReversal = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "SAR"
      If .chParabolic = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "ROC"
      If .chROC = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "EOM"
      If .chEaseOfMovement = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "TRIX"
      If .chTRIX = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "Reversal Day"
      If .chReversalDay = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "Two Day Reversal"
      If .chTwoDayReversal = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "One Day Reversal"
      If .chOneDayReversal = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "Pattern Gap"
      If .chPatternGap = vbcheck Then
        increase = True
      Else
        increase = False
      End If
    Case "Reversal Gap"
      If .chReversalGap = vbcheck Then
        increase = True
      Else
        increase = False
      End If
  End Select
  
  If Not increase Then
    i = i + 1
  Else
    i = i - 1
  End If
  
  .Label3.Caption = i
  
  End With
  
End Sub
