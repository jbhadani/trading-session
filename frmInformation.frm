VERSION 5.00
Begin VB.Form frmInformation 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Information"
   ClientHeight    =   3390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3390
   ScaleWidth      =   2655
   Begin VB.Frame Frame2 
      Caption         =   "Antal objekt"
      Height          =   615
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   2655
      Begin VB.Label lblObjects 
         Height          =   255
         Left            =   180
         TabIndex        =   3
         Top             =   300
         Width           =   2415
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Nya objekt"
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   2655
      Begin VB.ListBox lstNewObjects 
         Height          =   2205
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   2415
      End
   End
End
Attribute VB_Name = "frmInformation"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
Dim I As Long
On Error GoTo Err
  lblObjects = CStr(UBound(InvestInfoArray))

  For I = 1 To UBound(NewObjects)
    lstNewObjects.AddItem NewObjects(I)
  Next I
Err:
    If Err.Number = 9 Then
        MsgBox CheckLang("The requested items not found")
    End If
End Sub

