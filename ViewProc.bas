Attribute VB_Name = "ViewProc"
'Sub EditPrices(index As long)
'  With Frm_EditPrice
'   .tbDate.Text = GetDateString(PriceDataArray(index).Date)
'   .tbHigh.Text = PriceDataArray(index).High
'   .tbLow.Text = PriceDataArray(index).Low
'   .tbClose.Text = PriceDataArray(index).Close
'   .tbVolume.Text = PriceDataArray(index).volume
'  End With
'  Frm_EditPrice.Show 0
'End Sub

Sub UpdatePriceGrid(Inkrement As Long)
 Dim i As Long
 Dim ShowYear As Long
 Dim StartIndex As Long
 Dim StopIndex As Long
 Dim GainActiveYear As Double
 Dim TempStr As String
 Dim sngDiff As Single
 Dim sngDiffPercent As Single
 
 If PriceDataArraySize <> 0 Then
   ShowYear = YearToShowInShowPrices + Inkrement
   If ShowYear > Year(PriceDataArray(PriceDataArraySize).Date) Then
     MsgBox CheckLang("Det finns inga kurser registrerade f�r ") + Str$(ShowYear) + ".", vbOKOnly, CheckLang("Meddelande")
     If Inkrement <> 0 Then
       Exit Sub
     Else
       ShowYear = ShowYear - 1
     End If
   End If
   If ShowYear < Year(PriceDataArray(1).Date) Then
     MsgBox CheckLang("Det finns inga kurser registrerade f�r ") + Str$(ShowYear) + ".", vbOKOnly, CheckLang("Meddelande")
     Exit Sub
   End If

   MainMDI.MousePointer = vbHourglass
   YearToShowInShowPrices = ShowYear
   Frm_Prices.Text1.Text = Str$(ShowYear)
 
   For i = 1 To StartAndStopIndexArraySize
     If ShowYear = StartAndStopIndexArray(i).Year Then
       StartIndex = StartAndStopIndexArray(i).StartIndex
       StopIndex = StartAndStopIndexArray(i).StopIndex
     End If
   Next i
 
    With Frm_Prices.G_Price
      .Redraw = False
      .Clear
      .GridLines = flexGridNone
      .Rows = StopIndex - StartIndex + 2
      '
      .Cols = 7
      .ColWidth(0) = 950
      .ColWidth(1) = 1000
      .ColWidth(2) = 1000
      .ColWidth(3) = 1000
      .ColWidth(4) = 1000
      .ColWidth(5) = 700
      .ColWidth(6) = 1200
      .ColAlignment(0) = flexAlignLeftCenter
      .ColAlignment(1) = flexAlignRightCenter
      .ColAlignment(2) = flexAlignRightCenter
      .ColAlignment(3) = flexAlignRightCenter
      .ColAlignment(4) = flexAlignRightCenter
      .ColAlignment(5) = flexAlignRightCenter
      .ColAlignment(6) = flexAlignRightCenter
      .Row = 0
      .Col = 0
      .Text = "Datum"
      .Col = 1
      .Text = "H�gsta"
      .Col = 2
      .Text = "L�gsta"
      .Col = 3
      .Text = "Senast"
      .Col = 4
      .Text = "+/-"
      .Col = 5
      .Text = "+/-%"
      .Col = 6
      .Text = "Oms�ttning"
    
      For i = 1 To StopIndex - StartIndex + 1
        If StartIndex - 2 + i > 0 Then
          If PriceDataArray(StartIndex - 2 + i).Close <> 0 Then
            sngDiff = PriceDataArray(StartIndex - 1 + i).Close _
                                     - PriceDataArray(StartIndex - 2 + i).Close
            sngDiffPercent = PriceDataArray(StartIndex - 1 + i).Close _
                                            / PriceDataArray(StartIndex - 2 + i).Close - 1
            Else
              sngDiff = 0
              sngDiffPercent = 0
            End If
          Else
              sngDiff = 0
              sngDiffPercent = 0
          End If
        .Row = StopIndex - StartIndex + 2 - i
        .Col = 0
        .Text = GetDateString(PriceDataArray(StartIndex - 1 + i).Date)
        .Col = 1
        .Text = FormatNumber(PriceDataArray(StartIndex - 1 + i).High, 2, vbTrue)
        .Col = 2
        .Text = FormatNumber(PriceDataArray(StartIndex - 1 + i).Low, 2, vbTrue)
        .Col = 3
        .Text = FormatNumber(PriceDataArray(StartIndex - 1 + i).Close, 2, vbTrue)
        .Col = 4
        If sngDiff <= 0 Then
          .Text = FormatNumber(sngDiff, 2, vbTrue)
        Else
          .Text = "+" & FormatNumber(sngDiff, 2, vbTrue)
        End If
        .Col = 5
        If sngDiff <= 0 Then
          .Text = FormatPercent(sngDiffPercent, 1, vbTrue)
        Else
          .Text = "+" & FormatPercent(sngDiffPercent, 1, vbTrue)
        End If
        .Col = 6
        .Text = PriceDataArray(StartIndex - 1 + i).Volume
      
      Next i
      If PriceDataArray(StartIndex).Close <> 0 And PriceDataArray(StopIndex).Close <> 0 Then
        GainActiveYear = (((PriceDataArray(StopIndex).Close - PriceDataArray(StartIndex).Close) / PriceDataArray(StartIndex).Close) * 10000 \ 10) / 10
        TempStr = Str$(GainActiveYear)
        Frm_Prices.Text2.Text = TempStr + " %"
      Else
        Frm_Prices.Text2.Text = "--"
      End If
      .Redraw = True
    End With
    MainMDI.MousePointer = vbDefault
  Else
    Frm_Prices.G_Price.Rows = 0
    MsgBox CheckLang("Det finns inga kurser f�r det aktiva objektet i databasen."), vbOKOnly, CheckLang("Felmeddelande")
  End If
End Sub

Public Sub CalcIndex()
  Dim i As Long
  Dim TempStartIndex As Long
  Dim TempStopIndex As Long
  Dim Continue As Boolean
  Dim StopIndexFound As Boolean
  ReDim StartAndStopIndexArray(0)
  
  StartAndStopIndexArraySize = 0
  i = 1
  Continue = True
  Do While Continue And PriceDataArraySize <> 0
    TempStartIndex = i
    StopIndexFound = False
    ThisYear = Year(PriceDataArray(i).Date)
    Do While Not StopIndexFound
      i = i + 1
      If i > PriceDataArraySize Then
        StopIndexFound = True
        If ThisYear = Year(PriceDataArray(i - 1).Date) Then
          TempStopIndex = PriceDataArraySize
          Continue = False
        End If
      ElseIf ThisYear <> Year(PriceDataArray(i).Date) Then
        StopIndexFound = True
        TempStopIndex = i - 1
      End If
    Loop
        
    StartAndStopIndexArraySize = StartAndStopIndexArraySize + 1
    ReDim Preserve StartAndStopIndexArray(StartAndStopIndexArraySize)
    
    StartAndStopIndexArray(StartAndStopIndexArraySize).StartIndex = TempStartIndex
    StartAndStopIndexArray(StartAndStopIndexArraySize).StopIndex = TempStopIndex
    StartAndStopIndexArray(StartAndStopIndexArraySize).Year = Year(PriceDataArray(TempStartIndex).Date)
    If StartAndStopIndexArray(StartAndStopIndexArraySize).Year = Year(PriceDataArray(PriceDataArraySize).Date) Then
      Continue = False
    End If
  Loop

End Sub
