Attribute VB_Name = "Diagnose"
Option Explicit
#If DIAG = 1 Then

Sub RunDiagnose()
Dim I As Long
top:
On Error GoTo Err
  Open Path & "diagnos.txt" For Output As #7
  Print #7, Delimiter
  Print #7, App.Title & " " & CStr(App.Major) & "." & CStr(App.Minor) & "." & CStr(App.Revision) & " " & CStr(Date) & " " & CStr(Time)
  Print #7, Delimiter
  Print #7, "Path:" & vbTab & vbTab & Path
  Print #7, "Datapath:" & vbTab & DataPath
  Print #7, "Antal objekt: " & vbTab & UBound(InvestInfoArray)
  Print #7, "Antal grupper: " & vbTab & UBound(CategoryArray)
  Print #7, Delimiter
  Print #7, "OBJEKT"
  Print #7, Delimiter
  For I = 1 To UBound(InvestInfoArray)
    Print #7, InvestInfoArray(I).Name & vbTab & CStr(InvestInfoArray(I).TypeOfInvest);
    DiagnosePriceData InvestInfoArray(I).FileName
  Next I
  Print #7, Delimiter
  Print #7, "GRUPPER"
  Print #7, Delimiter
  For I = 1 To UBound(CategoryArray)
    Print #7, CStr(I) & vbTab & CategoryArray(I)
  Next I
  Print #7, Delimiter
  Close #7
Err:
If Err.Number > 0 Then
    Open Path & "diagnos.txt" For Binary Access Write As #7
    Close #7
    Err.Clear
    GoTo top
End If
End Sub

Function Delimiter() As String
  Delimiter = "================================================================================"
End Function

Sub DiagnosePriceData(FileName As String)
Dim PDArray() As PriceDataType
Dim PDArraySize As Long
Dim ErrorStatus As Long
Dim I As Long
  R_PriceDataArray DataPath & Trim(FileName) & ".prc", PDArray, PDArraySize
  Print #7, vbTab & "Dagar: " & CStr(UBound(PDArray)) & vbTab & " Senaste: " & CStr(Int(PDArray(UBound(PDArray)).Date)) & vbTab;
  ErrorStatus = 0
  For I = 1 To UBound(PDArray)
    If PDArray(I).Close < 0 Then
      Print #7, ""
      Print #7, "Close < 0" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).High < 0 Then
      Print #7, ""
      Print #7, "High < 0" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).Low < 0 Then
      Print #7, ""
      Print #7, "Low < 0" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).Volume < 0 Then
      Print #7, ""
      Print #7, "Volume < 0" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).Close < 0.00000000001 And PDArray(I).Close <> 0 Then
      Print #7, ""
      Print #7, "Close < E-10" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).High < 0.00000000001 And PDArray(I).High <> 0 Then
      Print #7, ""
      Print #7, "High < E-10" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).Low < 0.00000000001 And PDArray(I).Low <> 0 Then
      Print #7, ""
      Print #7, "Low < E-10" & vbTab & "Index: " & CStr(I);
    End If
    If PDArray(I).Volume < 0.00000000001 And PDArray(I).Volume <> 0 Then
      Print #7, ""
      Print #7, "Volume < E-10" & vbTab & "Index: " & CStr(I);
    End If
  Next I
  Print #7, ""
End Sub

#End If
