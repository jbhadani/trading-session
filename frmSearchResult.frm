VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmSearchResult 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Sökresultat"
   ClientHeight    =   4860
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8940
   Icon            =   "frmSearchResult.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4860
   ScaleWidth      =   8940
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   4080
      Width           =   8895
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   7200
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Height          =   375
         Left            =   600
         TabIndex        =   4
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Information"
      Height          =   1935
      Left            =   6840
      TabIndex        =   2
      Top             =   720
      Width           =   2055
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "label4"
         Height          =   255
         Left            =   600
         TabIndex        =   9
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "label3"
         Height          =   255
         Left            =   600
         TabIndex        =   8
         Top             =   720
         Width           =   735
      End
      Begin VB.Label Label2 
         Caption         =   "Antal objekt med signal:"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1080
         Width           =   1815
      End
      Begin VB.Label Label1 
         Caption         =   "Totalt antal signaler:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   1455
      End
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   3135
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   5530
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedCols       =   0
      GridLines       =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Times New Roman"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.PictureBox Picture1 
      Height          =   680
      Left            =   0
      Picture         =   "frmSearchResult.frx":0442
      ScaleHeight     =   615
      ScaleWidth      =   8835
      TabIndex        =   0
      Top             =   0
      Width           =   8895
   End
End
Attribute VB_Name = "frmSearchResult"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
  Unload Me
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  
  Height = 5265
  Width = 9030
  Top = 200
  Left = 500
  
  With MSFlexGrid1
    
    .ColWidth(0) = 2070
    .ColWidth(1) = 1500
    .ColWidth(2) = 800
    .ColWidth(3) = 1000
    .ColWidth(4) = 1000
    
    .Row = 0
    .Col = 0
    .Text = "Objekt"
    .Col = 1
    .Text = "Signal"
    .Col = 2
    .Text = "Antal"
    .Col = 3
    .Text = "Senaste"
    .Col = 4
    .Text = "Kurs"
    
  End With
End Sub


