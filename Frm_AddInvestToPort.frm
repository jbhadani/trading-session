VERSION 5.00
Begin VB.Form Frm_AddInvestToPort 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "L�gg till objekt i portf�ljen"
   ClientHeight    =   4500
   ClientLeft      =   1710
   ClientTop       =   1740
   ClientWidth     =   6285
   Icon            =   "Frm_AddInvestToPort.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4500
   ScaleWidth      =   6285
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   3720
      Width           =   6255
      Begin VB.CommandButton CB_Ok 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   600
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton CB_Cancel 
         Cancel          =   -1  'True
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   4440
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Height          =   3735
      Left            =   2520
      TabIndex        =   3
      Top             =   0
      Width           =   1215
      Begin VB.CommandButton Command2 
         Caption         =   "<<"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1680
         Width           =   735
      End
      Begin VB.CommandButton Command1 
         Caption         =   ">>"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1320
         Width           =   735
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Objekt i portf�lj"
      Height          =   3735
      Left            =   3840
      TabIndex        =   2
      Top             =   0
      Width           =   2415
      Begin VB.ListBox List_Port 
         Height          =   2985
         ItemData        =   "Frm_AddInvestToPort.frx":0442
         Left            =   240
         List            =   "Frm_AddInvestToPort.frx":0444
         Sorted          =   -1  'True
         TabIndex        =   7
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Tillg�nliga objekt"
      Height          =   3735
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   2415
      Begin VB.ListBox List_Invest 
         Height          =   2985
         Left            =   240
         Sorted          =   -1  'True
         TabIndex        =   6
         Top             =   360
         Width           =   1935
      End
   End
End
Attribute VB_Name = "Frm_AddInvestToPort"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private Sub CB_Cancel_Click()
  Unload Frm_AddInvestToPort
End Sub

Private Sub CB_Ok_Click()
  AddInvestToPortOK
End Sub

Private Sub Command1_Click()
  MoveToPortFromInv
End Sub

Private Sub Command2_Click()
  MoveToInvFromPort
End Sub

Private Sub Form_Load()
  Frm_AddInvestToPort.Left = 500
  Frm_AddInvestToPort.Top = 200
  InitAddInvestToPort
 
End Sub


Private Sub List_Invest_DblClick()
  MoveToPortFromInv
End Sub

Private Sub List_Port_DblClick()
  MoveToInvFromPort
End Sub
