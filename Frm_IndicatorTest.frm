VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Frm_IndicatorTest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Indikatortest"
   ClientHeight    =   4620
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5850
   Icon            =   "Frm_IndicatorTest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4620
   ScaleWidth      =   5850
   Begin TabDlg.SSTab SSTab1 
      Height          =   4575
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5775
      _ExtentX        =   10186
      _ExtentY        =   8070
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Indikatortest"
      TabPicture(0)   =   "Frm_IndicatorTest.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame2"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame5"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame6"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Vinsttestresultat"
      TabPicture(1)   =   "Frm_IndicatorTest.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4"
      Tab(1).Control(0).Enabled=   0   'False
      Tab(1).Control(1)=   "Grid"
      Tab(1).Control(1).Enabled=   0   'False
      Tab(1).Control(2)=   "Label5"
      Tab(1).Control(2).Enabled=   0   'False
      Tab(1).Control(3)=   "Label4"
      Tab(1).Control(3).Enabled=   0   'False
      Tab(1).ControlCount=   4
      Begin VB.Frame Frame6 
         Caption         =   "Slutdatum"
         Height          =   735
         Left            =   120
         TabIndex        =   25
         Top             =   2880
         Width           =   2295
         Begin VB.TextBox tbSlutdatum 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   480
            MultiLine       =   -1  'True
            TabIndex        =   32
            Text            =   "Frm_IndicatorTest.frx":0182
            Top             =   300
            Width           =   1215
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Startdatum"
         Height          =   735
         Left            =   120
         TabIndex        =   24
         Top             =   2040
         Width           =   2295
         Begin VB.TextBox tbStartdatum 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   480
            MultiLine       =   -1  'True
            TabIndex        =   31
            Text            =   "Frm_IndicatorTest.frx":0188
            Top             =   300
            Width           =   1215
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Resultat"
         Height          =   1095
         Left            =   -74880
         TabIndex        =   16
         Top             =   1200
         Width           =   5535
         Begin VB.Label Label3 
            Height          =   255
            Left            =   3120
            TabIndex        =   22
            Top             =   720
            Width           =   975
         End
         Begin VB.Label Label2 
            Height          =   255
            Left            =   3120
            TabIndex        =   21
            Top             =   480
            Width           =   975
         End
         Begin VB.Label Label1 
            Height          =   255
            Left            =   3120
            TabIndex        =   20
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label7 
            Caption         =   "Innehav i tid :"
            Height          =   255
            Left            =   720
            TabIndex        =   19
            Top             =   720
            Width           =   2415
         End
         Begin VB.Label Label8 
            Caption         =   "Resultat konstant innehav :"
            Height          =   255
            Left            =   720
            TabIndex        =   18
            Top             =   480
            Width           =   2415
         End
         Begin VB.Label Label9 
            Caption         =   "Resultat med indikator :"
            Height          =   255
            Left            =   720
            TabIndex        =   17
            Top             =   240
            Width           =   2415
         End
      End
      Begin MSFlexGridLib.MSFlexGrid Grid 
         Height          =   2055
         Left            =   -74880
         TabIndex        =   15
         Top             =   2400
         Width           =   5535
         _ExtentX        =   9763
         _ExtentY        =   3625
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
      End
      Begin VB.Frame Frame2 
         Height          =   735
         Left            =   120
         TabIndex        =   13
         Top             =   3720
         Width           =   5535
         Begin VB.CommandButton Command3 
            Caption         =   "Avbryt"
            Height          =   375
            Left            =   3840
            TabIndex        =   14
            Top             =   240
            Width           =   1215
         End
         Begin VB.CommandButton Command2 
            Caption         =   "OK"
            Default         =   -1  'True
            Height          =   375
            Left            =   480
            TabIndex        =   1
            Top             =   240
            Width           =   1455
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Courtage"
         Height          =   3135
         Left            =   2640
         TabIndex        =   5
         Top             =   480
         Width           =   3015
         Begin VB.HScrollBar HScroll3 
            Height          =   135
            LargeChange     =   100
            Left            =   1620
            Max             =   10000
            Min             =   10
            TabIndex        =   28
            Top             =   2280
            Value           =   100
            Width           =   975
         End
         Begin VB.TextBox tbInvBelopp 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1680
            MultiLine       =   -1  'True
            TabIndex        =   27
            Text            =   "Frm_IndicatorTest.frx":018E
            Top             =   1920
            Width           =   855
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Anv�nd ej Courtage"
            Height          =   255
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Value           =   -1  'True
            Width           =   1815
         End
         Begin VB.OptionButton Option_ProcentCourtage 
            Caption         =   " %"
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   2640
            Width           =   615
         End
         Begin VB.OptionButton Option_MinCourtage 
            Caption         =   "Min"
            Height          =   255
            Left            =   240
            TabIndex        =   10
            Top             =   1440
            Width           =   615
         End
         Begin VB.HScrollBar HScroll2 
            Height          =   135
            LargeChange     =   10
            Left            =   1080
            Max             =   100
            TabIndex        =   9
            Top             =   2880
            Value           =   65
            Width           =   975
         End
         Begin VB.TextBox tbProcentCourtage 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1280
            MultiLine       =   -1  'True
            TabIndex        =   8
            Text            =   "Frm_IndicatorTest.frx":0194
            Top             =   2520
            Width           =   615
         End
         Begin VB.HScrollBar HScroll1 
            Height          =   135
            LargeChange     =   50
            Left            =   1620
            Max             =   2000
            TabIndex        =   7
            Top             =   1440
            Value           =   50
            Width           =   975
         End
         Begin VB.TextBox tbMinCourtage 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1800
            MultiLine       =   -1  'True
            TabIndex        =   6
            Text            =   "Frm_IndicatorTest.frx":0199
            Top             =   1080
            Width           =   615
         End
         Begin VB.Label Label10 
            Caption         =   "Investeringsbelopp"
            Height          =   255
            Left            =   1440
            TabIndex        =   30
            Top             =   1680
            Width           =   1335
         End
         Begin VB.Label Label6 
            Caption         =   "Min Courtage"
            Height          =   255
            Left            =   1620
            TabIndex        =   29
            Top             =   840
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Indikator"
         Height          =   1455
         Left            =   120
         TabIndex        =   3
         Top             =   480
         Width           =   2295
         Begin VB.ComboBox Combo1 
            Height          =   315
            ItemData        =   "Frm_IndicatorTest.frx":019C
            Left            =   240
            List            =   "Frm_IndicatorTest.frx":01BB
            Style           =   2  'Dropdown List
            TabIndex        =   0
            Top             =   360
            Width           =   1815
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Parametrar >>"
            Height          =   375
            Left            =   360
            TabIndex        =   4
            Top             =   840
            Width           =   1575
         End
      End
      Begin VB.Label Label5 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -73800
         TabIndex        =   26
         Top             =   840
         Width           =   3615
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   14.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   -73800
         TabIndex        =   23
         Top             =   480
         Width           =   3615
      End
   End
End
Attribute VB_Name = "Frm_IndicatorTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check_ConstCourtage_Click()
 ' If Check_ProcentCourtage.Value = 1 Then
 '   Check_ProcentCourtage.Value = 0
 '   tbProcentCourtage.Enabled = False
 '   HScroll2.Enabled = False
 ' End If
 ' tbMinCourtage.Enabled = True
 ' HScroll1.Enabled = True
End Sub

Private Sub Check_ProcentCourtage_Click()
 'If Check_ConstCourtage.Value = 1 Then
 '   Check_ConstCourtage.Value = 0
 '   tbMinCourtage.Enabled = False
 '   HScroll1.Enabled = False
 ' End If
 ' tbProcentCourtage.Enabled = True
 ' HScroll2.Enabled = True
End Sub
Private Sub Form_Terminate()
  IndicatortestVisible = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
  IndicatortestVisible = False
End Sub
Private Sub Command1_Click()
  Frm_Parameters.Show
End Sub

Private Sub Command2_Click()
  Dim Errorfound As Boolean
  Dim Startdatum As Date
  Dim Slutdatum As Date
  
  Errorfound = False
  If ActiveIndex = 0 Then
    Errorfound = True
  End If
  
  On Error Resume Next
  
  Startdatum = tbStartdatum
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum"
    tbStartdatum.SetFocus
    Errorfound = True
  End If
  Err.Clear
  Slutdatum = tbSlutdatum
  If Err.Number <> 0 Then
    ErrorMessageStr "Felaktigt datum"
    tbSlutdatum.SetFocus
    Errorfound = True
  End If
  Err.Clear
  On Error GoTo 0
  
  If Option_MinCourtage Then
    VerifyIntegerPara Frm_IndicatorTest.tbMinCourtage, Frm_IndicatorTest.HScroll1, "Min Courtage", "Fel", Errorfound
    VerifyIntegerPara Frm_IndicatorTest.tbInvBelopp, Frm_IndicatorTest.HScroll3, "Investeringsbeloppet", "Fel", Errorfound
  End If
  If Option_ProcentCourtage Then
    VerifySinglePara Frm_IndicatorTest.tbProcentCourtage, Errorfound
  End If
  If Not Errorfound Then
    TestHandler
  End If
End Sub

Private Sub Command3_Click()
  Unload Frm_IndicatorTest
  IndicatortestVisible = False
End Sub

Private Sub Form_Load()
If PriceDataArraySize >= 2 Then
  With Frm_IndicatorTest
  If .Option1 Then
    Option1_Click
  End If
  IndicatortestVisible = True
  Top = 50
  Left = 700
  'Dim i As LONG
  'For i = 1 To PriceDataArraySize
  '  .Combo2.AddItem PriceDataArray(i).Date
  '  .Combo3.AddItem PriceDataArray(i).Date
  'Next i
  '.Combo2.Text = PriceDataArray(1).Date
  '.Combo3.Text = PriceDataArray(i - 1).Date
  
  tbStartdatum = GetDateString(PriceDataArray(1).Date)
  tbSlutdatum = GetDateString(PriceDataArray(PriceDataArraySize).Date)
  
  End With
  With Frm_IndicatorTest.Grid
    .Row = 0
    For k = 0 To 3
      .ColWidth(k) = (.Width - 350) / 4
    Next k
    .Col = 0
    .Text = "K�p/S�lj"
    .Col = 1
    .Text = "Datum"
    .Col = 2
    .Text = "Kurs,kr"
    .Col = 3
    .Text = "Reultat,%"
  End With
  Combo1.ListIndex = 0
  
Else
  Unload Me
  'TestOK = False
  'With Frm_IndicatorTest
  ' .Combo2.Enabled = False
  ' .Combo3.Enabled = False
  ' .Combo2.Text = ""
  ' .Combo3.Text = ""
  'End With
End If
End Sub


Private Sub HScroll1_Change()
  tbMinCourtage = HScroll1.Value
End Sub

Private Sub HScroll2_Change()
  tbProcentCourtage = HScroll2.Value / 100
End Sub

Private Sub HScroll3_Change()
Dim A As Long
  A = HScroll3.Value
  A = A * 100
  tbInvBelopp = A
End Sub

Private Sub Option_MinCourtage_Click()
  If Option_ProcentCourtage.Value = False Then
    tbProcentCourtage.Enabled = False
    HScroll2.Enabled = False
  End If
  tbMinCourtage.Enabled = True
  HScroll1.Enabled = True
  tbInvBelopp.Enabled = True
  HScroll3.Enabled = True
End Sub

Private Sub Option_ProcentCourtage_Click()
  If Option_MinCourtage.Value = False Then
    tbMinCourtage.Enabled = False
    HScroll1.Enabled = False
    tbInvBelopp.Enabled = False
    HScroll3.Enabled = False
  End If
  tbProcentCourtage.Enabled = True
  HScroll2.Enabled = True
End Sub

Private Sub Option1_Click()
  tbMinCourtage.Enabled = False
  HScroll1.Enabled = False
  tbInvBelopp.Enabled = False
  HScroll3.Enabled = False
  tbProcentCourtage.Enabled = False
  HScroll2.Enabled = False
End Sub

Private Sub tbInvBelopp_GotFocus()
  TextSelected
End Sub

Private Sub tbMinCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbProcentCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbStartdatum_GotFocus()
  TextSelected
End Sub

Private Sub tbSlutdatum_GotFocus()
  TextSelected
End Sub
