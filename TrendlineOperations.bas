Attribute VB_Name = "TrendlineOperations"
Option Explicit

 Function GetFreeTrendLine(DWM As DWMEnum) As Long
 Dim j As Long
 Dim Found As Boolean
  
  j = 1
  'ReDim Preserve TrendLineArray(DWM, 31)
  Do While Not Found And j <= 31
    If Not TrendLineArray(DWM, j).InUse Then
      Found = True
    Else
      j = j + 1
    End If
  Loop
  If Not Found Then
    GetFreeTrendLine = -1
  Else
    GetFreeTrendLine = j
  End If
  
 End Function

Sub FreeTrendLineArray(DWM As DWMEnum)
  Dim j As Long
  
  For j = 1 To TrendlineSerieSize
    TrendLineArray(DWM, j).InUse = False
  Next j
End Sub

Sub HideAllTrendlines()
  Dim j As Long
  
  For j = 0 To TrendlineSerieSize - 1
    Frm_OwnGraph.TrendLine(j).Visible = False
  Next j

End Sub

Sub DeleteAllTrendlines()
  Dim j As Long
  FreeTrendLineArray GraphSettings.DWM
  For j = 0 To TrendlineSerieSize - 1
    Frm_OwnGraph.TrendLine(j).Visible = False
  Next j
  
  'Radera 'datumstämplar'
  Dim I As Integer
  Dim LabelHandle As Long
  For I = 0 To LLHPointer
    LabelHandle = GetStockLabel(Frm_OwnGraph.hDc, ActiveStockLabelsHandle, LineLabelHandleArray(I, 0), LineLabelHandleArray(I, 1))
    If LabelHandle <> 0 Then
      DeleteStockLabel ActiveStockLabelsHandle, LabelHandle
      UpdateGraph ActiveIndex
    End If
  Next I
  LLHPointer = 0
  ZIAPointer = 0
  'Slut hack
End Sub

Sub SaveTrendlines()
Dim I As Long
Dim j As Long

  If ActiveIndex > 0 Then
    For I = 1 To TrendLineSeries
      For j = 1 To TrendlineSerieSize
        dbg.Out Str(TrendLineArray(I, j).EndDate)
        WorkspaceSettingsArray(ActiveIndex).TrendLine(I, j) = TrendLineArray(I, j)
      Next j
    Next I
    
    WriteWorkspaceSettings
End If
End Sub

 Sub TLFixBorder(DWM As DWMEnum, Nbr As Long, side As Long, OtherX As Single, OtherY As Single, X As Single, Y As Single)
  
  
  Dim j As Long
  Dim Price As Single
  Dim theDate As Date
  Dim NewX As Long
  Dim NewY As Long
  
  If GraphSettings.ExponentialPrice Then
    If side = 1 Then
      Price = Log(TrendLineArray(DWM, Nbr).StartPrice)
    Else
      Price = Log(TrendLineArray(DWM, Nbr).EndPrice)
    End If
  Else
    If side = 1 Then
      Price = TrendLineArray(DWM, Nbr).StartPrice
    Else
      Price = TrendLineArray(DWM, Nbr).EndPrice
    End If
  End If
  
  If side = 1 Then
    theDate = TrendLineArray(DWM, Nbr).StartDate
  Else
    theDate = TrendLineArray(DWM, Nbr).EndDate
  End If
  
  NewY = BottomDrawTL - Int((Price - MinValueTL) * YScaleTL)
  NewX = LeftDrawTL + GetNbrOfDaysBetween(GetFirstDrawDate(), theDate) * GraphSettings.lngDaySpace
      
      
  
  
    
    'Check left limits
    If NewX < LeftDrawTL Then
      NewY = NewY + Int((OtherY - NewY) / (OtherX - NewX) * (LeftDrawTL - NewX))
      NewX = LeftDrawTL
    End If
    
    'Check right limits
    If NewX > RightDrawTL Then
      NewY = NewY + Int((OtherY - NewY) / (OtherX - NewX) * (RightDrawTL - NewX))
      NewX = RightDrawTL
    End If
  
    'Check bottom limits
    If NewY > BottomDrawTL Then
      NewX = NewX + Int((OtherX - NewX) / (OtherY - NewY) * (BottomDrawTL - NewY))
      NewY = BottomDrawTL
    End If
  
    'Check top limits
    If NewY < TopDrawTL Then
      NewX = NewX + Int((OtherX - NewX) / (OtherY - NewY) * (TopDrawTL - NewY))
      NewY = TopDrawTL
    End If
  
  X = NewX
  Y = NewY
End Sub

