VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Begin VB.Form frmSplash 
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3570
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   4785
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmSplash.frx":0000
   ScaleHeight     =   3570
   ScaleWidth      =   4785
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ProgressBar pbrSplash 
      Height          =   255
      Left            =   840
      TabIndex        =   2
      Top             =   3180
      Width           =   3255
      _ExtentX        =   5741
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.Label lblLicense 
      BackStyle       =   0  'Transparent
      ForeColor       =   &H000080FF&
      Height          =   255
      Left            =   840
      TabIndex        =   4
      Top             =   3120
      Width           =   3255
   End
   Begin VB.Label lblSplashStatus 
      BackStyle       =   0  'Transparent
      ForeColor       =   &H00C0C0C0&
      Height          =   255
      Left            =   840
      TabIndex        =   3
      Top             =   2940
      Width           =   3255
   End
   Begin VB.Label lblVersion 
      AutoSize        =   -1  'True
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Version 0.1"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   225
      Left            =   3600
      TabIndex        =   0
      Top             =   1260
      Width           =   1185
   End
   Begin VB.Label lblType 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   27.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C0C0&
      Height          =   615
      Left            =   60
      TabIndex        =   1
      Top             =   180
      Width           =   1935
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_KeyPress(KeyAscii As Integer)
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim Name As String
    Dim SerialNo As String
    frmSplash.ScaleMode = vbPixels
    Debug.Print frmSplash.ScaleWidth
    Debug.Print frmSplash.ScaleHeight
    If App.PrevInstance Then
      InformationalMessage "Det g�r inte att starta mer �n en instans av " & App.Title
      End
    End If
    
    lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision & " r1"
    If GetVersion = "std" Then
      lblType.Caption = "Demo"
      DEMO = False
      IsProVersion = False
    ElseIf GetVersion = "pro" Then
      lblType.Caption = "Pro"
      PRO = True
      IsProVersion = True
    End If
    #If BETA = 1 Then
      lblType.Caption = "Beta"
    #End If
    frmSplash.Show
    DoEvents
    DoEvents
    #If SafeSerial = 1 Then
      LicenseCheck
    #End If
    DoEvents
    Load MainMDI
    'MainMDI.MDIForm_Load
    MainMDI.Visible = True
    DoEvents
    
    'Bj�rns hack
    If ShowGraphAtStartup = 1 Then
       ShowGraphForm
    End If
    'slut hack
End Sub

Private Sub Frame1_Click()
    Me.Hide
End Sub

