VERSION 5.00
Begin VB.Form Frm_DeleteInvest 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Delete Investment"
   ClientHeight    =   4515
   ClientLeft      =   4170
   ClientTop       =   2145
   ClientWidth     =   3540
   Icon            =   "Frm_DeleteInvest.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   4515
   ScaleWidth      =   3540
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   4
      Top             =   3720
      Width           =   3495
      Begin VB.CommandButton CB_Cancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2280
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton CB_Delete 
         Caption         =   "Ta bort"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Objekt"
      Height          =   3735
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   3495
      Begin VB.ListBox List_Investments 
         Height          =   2595
         Left            =   240
         MultiSelect     =   2  'Extended
         Sorted          =   -1  'True
         TabIndex        =   0
         Top             =   480
         Width           =   2895
      End
   End
End
Attribute VB_Name = "Frm_DeleteInvest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CB_Cancel_Click()
Unload Frm_DeleteInvest
End Sub

Private Sub CB_Delete_Click()
  DeleteInvest
End Sub

Private Sub Form_Load()
  Frm_DeleteInvest.Left = (MainMDI.Width - Frm_DeleteInvest.Width) / 2
  Frm_DeleteInvest.Top = (MainMDI.Height - Frm_DeleteInvest.Height) / 4
Dim i As Integer
  For i = 1 To InvestInfoArraySize
    List_Investments.AddItem InvestInfoArray(i).Name
  Next i
End Sub

