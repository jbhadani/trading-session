VERSION 5.00
Begin VB.Form frmColorSettings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "F�rginst�llningar"
   ClientHeight    =   4470
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6750
   Icon            =   "frmColorSettings.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4470
   ScaleWidth      =   6750
   Begin VB.Frame Frame6 
      Caption         =   "K�p setup signaler"
      Height          =   2535
      Left            =   4440
      TabIndex        =   32
      Top             =   0
      Width           =   2295
      Begin VB.PictureBox picReversalGap 
         BackColor       =   &H00C00000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   38
         Top             =   2160
         Width           =   375
      End
      Begin VB.PictureBox picPatternGap 
         BackColor       =   &H00FF8080&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   37
         Top             =   1800
         Width           =   375
      End
      Begin VB.PictureBox picTwoDayReversal 
         BackColor       =   &H00C0C000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   36
         Top             =   1440
         Width           =   375
      End
      Begin VB.PictureBox picOneDayReversal 
         BackColor       =   &H00FFFF00&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   35
         Top             =   1080
         Width           =   375
      End
      Begin VB.PictureBox picReversalDay 
         BackColor       =   &H00FFFF80&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   34
         Top             =   720
         Width           =   375
      End
      Begin VB.PictureBox picKeyReversalDay 
         BackColor       =   &H00FFFFC0&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   33
         Top             =   360
         Width           =   375
      End
      Begin VB.Label Label18 
         Caption         =   "Reversal Gap"
         Height          =   255
         Left            =   720
         TabIndex        =   44
         Top             =   2160
         Width           =   1455
      End
      Begin VB.Label Label17 
         Caption         =   "Pattern Gap"
         Height          =   255
         Left            =   720
         TabIndex        =   43
         Top             =   1800
         Width           =   1455
      End
      Begin VB.Label Label16 
         Caption         =   "Two Day Reversal"
         Height          =   255
         Left            =   720
         TabIndex        =   42
         Top             =   1440
         Width           =   1455
      End
      Begin VB.Label Label15 
         Caption         =   "One Day Reversal"
         Height          =   255
         Left            =   720
         TabIndex        =   41
         Top             =   1080
         Width           =   1455
      End
      Begin VB.Label Label14 
         Caption         =   "Reversal Day"
         Height          =   255
         Left            =   720
         TabIndex        =   40
         Top             =   720
         Width           =   1455
      End
      Begin VB.Label Label13 
         Caption         =   "Key Reversal Day"
         Height          =   255
         Left            =   720
         TabIndex        =   39
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "F�rgsignalering"
      Height          =   975
      Left            =   4440
      TabIndex        =   27
      Top             =   2640
      Width           =   2295
      Begin VB.PictureBox picSignalColorSell 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   29
         Top             =   600
         Width           =   375
      End
      Begin VB.PictureBox picSignalColorBuy 
         BackColor       =   &H00008000&
         Height          =   255
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   28
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label12 
         Caption         =   "S�ljtrend"
         Height          =   255
         Left            =   600
         TabIndex        =   31
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label11 
         Caption         =   "K�ptrend"
         Height          =   255
         Left            =   600
         TabIndex        =   30
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   23
      Top             =   3720
      Width           =   6735
      Begin VB.CommandButton cbApply 
         Caption         =   "Verkst�ll"
         Height          =   375
         Left            =   5400
         TabIndex        =   26
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2940
         TabIndex        =   25
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cbOK 
         Caption         =   "OK"
         Height          =   375
         Left            =   540
         TabIndex        =   24
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Indikatordiagram"
      Height          =   975
      Left            =   2100
      TabIndex        =   18
      Top             =   2640
      Width           =   2295
      Begin VB.PictureBox picIndicatorLine2Color 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   20
         Top             =   600
         Width           =   375
      End
      Begin VB.PictureBox picIndicatorLine1Color 
         BackColor       =   &H00008000&
         Height          =   255
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   19
         Top             =   240
         Width           =   375
      End
      Begin VB.Label Label10 
         Caption         =   "Indikatorlinje 2"
         Height          =   255
         Left            =   600
         TabIndex        =   22
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label9 
         Caption         =   "Indikatorlinje 1"
         Height          =   255
         Left            =   600
         TabIndex        =   21
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Indikatorer i kursdiagram"
      Height          =   2535
      Left            =   2100
      TabIndex        =   9
      Top             =   0
      Width           =   2295
      Begin VB.PictureBox picSupportResistanceColor 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   51
         Top             =   1800
         Width           =   375
      End
      Begin VB.PictureBox picFishColor 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   47
         Top             =   2160
         Width           =   375
      End
      Begin VB.PictureBox picParabolicColor 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   13
         Top             =   1440
         Width           =   375
      End
      Begin VB.PictureBox picBollingerColor 
         BackColor       =   &H00000000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   12
         Top             =   1080
         Width           =   375
      End
      Begin VB.PictureBox picMAV2Color 
         BackColor       =   &H000000FF&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   11
         Top             =   720
         Width           =   375
      End
      Begin VB.PictureBox picMAV1Color 
         BackColor       =   &H00008000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   10
         Top             =   360
         Width           =   375
      End
      Begin VB.Label lblSupportResistanceColor 
         Caption         =   "St�d/motst�nd"
         Height          =   255
         Left            =   720
         TabIndex        =   52
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label20 
         Caption         =   "FishNet"
         Height          =   255
         Left            =   720
         TabIndex        =   48
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label8 
         Caption         =   "Parabolic"
         Height          =   255
         Left            =   720
         TabIndex        =   17
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label7 
         Caption         =   "Bollingerband"
         Height          =   255
         Left            =   720
         TabIndex        =   16
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Moving Average 2"
         Height          =   255
         Left            =   720
         TabIndex        =   15
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label5 
         Caption         =   "Moving Average 1"
         Height          =   255
         Left            =   720
         TabIndex        =   14
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Diagram"
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.PictureBox picDrawAreaColor 
         BackColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   53
         Top             =   720
         Width           =   375
      End
      Begin VB.PictureBox picVolumeColor 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   49
         Top             =   1800
         Width           =   375
      End
      Begin VB.PictureBox picCloseColor 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   45
         Top             =   1440
         Width           =   375
      End
      Begin VB.PictureBox picGridColor 
         BackColor       =   &H00C0C0C0&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   5
         Top             =   2520
         Width           =   375
      End
      Begin VB.PictureBox picAxisColor 
         BackColor       =   &H00000000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   4
         Top             =   2160
         Width           =   375
      End
      Begin VB.PictureBox picBackgroundColor 
         BackColor       =   &H00FFFFFF&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   3
         Top             =   300
         Width           =   375
      End
      Begin VB.PictureBox picDiagramColor 
         BackColor       =   &H00FF0000&
         Height          =   255
         Left            =   240
         ScaleHeight     =   195
         ScaleWidth      =   315
         TabIndex        =   1
         Top             =   1080
         Width           =   375
      End
      Begin VB.Label Label22 
         Caption         =   "Diagramyta"
         Height          =   255
         Left            =   720
         TabIndex        =   54
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label21 
         Caption         =   "Volym"
         Height          =   255
         Left            =   720
         TabIndex        =   50
         Top             =   1860
         Width           =   975
      End
      Begin VB.Label Label19 
         Caption         =   "Closepunkt"
         Height          =   255
         Left            =   720
         TabIndex        =   46
         Top             =   1500
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Rutn�t"
         Height          =   255
         Left            =   720
         TabIndex        =   8
         Top             =   2580
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "Diagramaxlar"
         Height          =   255
         Left            =   720
         TabIndex        =   7
         Top             =   2220
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Bakgrund"
         Height          =   255
         Left            =   720
         TabIndex        =   6
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label1 
         Caption         =   "Kurs"
         Height          =   255
         Left            =   720
         TabIndex        =   2
         Top             =   1155
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmColorSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cbApply_Click()
  UpdateColorGraphSettings frmColorSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
End Sub

Private Sub cbCancel_Click()
  Unload frmColorSettings
End Sub

Private Sub cbOK_Click()
  UpdateColorGraphSettings frmColorSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
  Unload Me
End Sub

Private Sub Form_Load()
If PRO = 0 Then
  Me.picFishColor.Visible = False
  Me.Label20.Visible = False
End If
  CenterForm Me
  InitColorGraphSettings
  ChangeFormLang Me
End Sub



Private Sub picAxisColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picAxisColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picAxisColor.BackColor = Color
  End If
End Sub

Private Sub picBackgroundColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picBackgroundColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picBackgroundColor.BackColor = Color
  End If
End Sub

Private Sub picBollingerColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picBollingerColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picBollingerColor.BackColor = Color
  End If
End Sub

Private Sub picCloseColor_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picCloseColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picCloseColor.BackColor = Color
  End If
End Sub

Private Sub picDiagramColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picDiagramColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picDiagramColor.BackColor = Color
  End If
End Sub

Private Sub picDrawAreaColor_Click()
 Dim Color As Long
  SelectColor frmColorSettings.picDrawAreaColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picDrawAreaColor.BackColor = Color
  End If
End Sub

Private Sub picFishColor_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picFishColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picFishColor.BackColor = Color
  End If
End Sub



Private Sub picGridColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picGridColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picGridColor.BackColor = Color
  End If
End Sub

Private Sub picIndicatorLine1Color_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picIndicatorLine1Color.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picIndicatorLine1Color.BackColor = Color
  End If
End Sub

Private Sub picIndicatorLine2Color_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picIndicatorLine2Color.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picIndicatorLine2Color.BackColor = Color
  End If
End Sub

Private Sub picKeyReversalDay_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picKeyReversalDay.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picKeyReversalDay.BackColor = Color
  End If
End Sub

Private Sub picMAV1Color_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picMAV1Color.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picMAV1Color.BackColor = Color
  End If
End Sub

Private Sub picMAV2Color_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picMAV2Color.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picMAV2Color.BackColor = Color
  End If
End Sub

Private Sub picOneDayReversal_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picOneDayReversal.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picOneDayReversal.BackColor = Color
  End If
End Sub

Private Sub picParabolicColor_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picParabolicColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picParabolicColor.BackColor = Color
  End If
End Sub

Private Sub picPatternGap_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picPatternGap.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picPatternGap.BackColor = Color
  End If
End Sub



Private Sub picReversalDay_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picReversalDay.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picReversalDay.BackColor = Color
  End If
End Sub

Private Sub picReversalGap_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picReversalGap.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picReversalGap.BackColor = Color
  End If
End Sub

Private Sub picSignalColorBuy_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picSignalColorBuy.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picSignalColorBuy.BackColor = Color
  End If
End Sub

Private Sub picSignalColorSell_DblClick()
  Dim Color As Long
  SelectColor frmColorSettings.picSignalColorSell.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picSignalColorSell.BackColor = Color
  End If
End Sub

Private Sub picSupportResistanceColor_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picSupportResistanceColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picSupportResistanceColor.BackColor = Color
  End If
End Sub

Private Sub picTwoDayReversal_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picTwoDayReversal.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picTwoDayReversal.BackColor = Color
  End If
End Sub

Private Sub picVolumeColor_Click()
  Dim Color As Long
  SelectColor frmColorSettings.picVolumeColor.BackColor, Color
  If Color <> -1 Then
    frmColorSettings.picVolumeColor.BackColor = Color
  End If
End Sub
