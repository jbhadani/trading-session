Attribute VB_Name = "ArrayOp"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetMinAndMax
'Input:         An array.
'Output:        Min,Max as single; ValueArray() as single
'Description:   This procedure gets the largest and the smallest
'               value in an array.
'Author:        Martin Lindberg
'Modified:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GetMinAndMax(ByRef ValueArray() As Single, Min As Single, Max As Single)
  Dim i As Long
  Max = ValueArray(1)
  Min = ValueArray(1)
  For i = 2 To PriceDataArraySize
    If ValueArray(i) > Max Then
      Max = ValueArray(i)
    End If
    If ValueArray(i) < Min Then
      Min = ValueArray(i)
    End If
  Next i
End Sub

Public Sub GetMinAndMaxEx(ByRef ValueArray() As Single, lngStart As Long, lngStop As Long, _
                        Min As Single, Max As Single)
  Dim i As Long
  Max = ValueArray(lngStart)
  Min = ValueArray(lngStop)
  For i = lngStart + 1 To lngStop
    If ValueArray(i) > Max Then
      Max = ValueArray(i)
    End If
    If ValueArray(i) < Min Then
      Min = ValueArray(i)
    End If
  Next i
End Sub

Function GetInvested(Name As String, QDate As Date)
Dim i As Long
Dim Invested As Single
Dim nDate As Long

nDate = Int(QDate)
Invested = 0
For i = 1 To TransactionArraySize
If Trim(Name) = Trim(TransactionArray(i).Name) Then
  If Int(TransactionArray(i).Date) <= nDate Then
    If TransactionArray(i).IsBuy Then
      Invested = Invested + TransactionArray(i).Price * TransactionArray(i).Volume + TransactionArray(i).Courtage
    Else
      Invested = Invested - TransactionArray(i).Price * TransactionArray(i).Volume + TransactionArray(i).Courtage
    End If
  End If
End If
Next i
GetInvested = Invested
End Function

Function GetTotalInvested(LatestDate As Date)
Dim i As Long
Dim Invested As Single
Dim nLatestDate As Long

nLatestDate = Int(LatestDate)
Invested = 0

For i = 1 To TransactionArraySize
  If Int(TransactionArray(i).Date) <= nLatestDate Then
    If TransactionArray(i).IsBuy Then
      Invested = Invested + TransactionArray(i).Price * TransactionArray(i).Volume + TransactionArray(i).Courtage
    Else
      Invested = Invested - TransactionArray(i).Price * TransactionArray(i).Volume + TransactionArray(i).Courtage
    End If
  End If
Next i
GetTotalInvested = Invested
End Function

Function GetAveragePrice(Name As String, LatestDate As Date, UseCourtage As Boolean) As Single
Dim i As Long
Dim TotalVolume As Single
Dim AveragePrice As Single

For i = 1 To TransactionArraySize
  If Trim(TransactionArray(i).Name) = Trim(Name) And TransactionArray(i).Date <= LatestDate Then
    If TransactionArray(i).IsBuy And TransactionArray(i).Volume > 0 Then
      TotalVolume = TotalVolume + TransactionArray(i).Volume
      If Not UseCourtage Then
        AveragePrice = ((TotalVolume - TransactionArray(i).Volume) * AveragePrice + TransactionArray(i).Price * TransactionArray(i).Volume) / TotalVolume
      Else
        AveragePrice = ((TotalVolume - TransactionArray(i).Volume) * AveragePrice + (TransactionArray(i).Price * TransactionArray(i).Volume + TransactionArray(i).Courtage)) / TotalVolume
      End If
    Else
      TotalVolume = TotalVolume - TransactionArray(i).Volume
    End If
  End If
Next i

GetAveragePrice = AveragePrice

End Function

Function GetTotalValue(LatestDate As Date) As Single

Dim j As Long
Dim TotalValue As Single
Dim Interpolated As Boolean
TotalValue = 0
For j = 1 To PortfolioArraySize
TotalValue = TotalValue + GetVolume(PortfolioArray(j).Name, LatestDate) _
             * GetPrice(LatestDate, PortfolioArray(j).Name)
Next j
If PortfolioArraySize = 0 Then
  TotalValue = 0
End If
GetTotalValue = TotalValue
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetVolume
'Input:
'Output:
'
'Description:   Get volume for given name and price
'Author:        Fredrik Wendel
'Revision:      97      Created
'               980812  Changes to cope with date type including
'                       time information.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetVolume(Name As String, QDate As Date)
  Dim i As Long
  Dim Volume As Single
  Dim nDate As Long
  
  nDate = Int(QDate)
  Volume = 0
  For i = 1 To TransactionArraySize
  If Trim(Name) = Trim(TransactionArray(i).Name) Then
    If Int(TransactionArray(i).Date) <= nDate Then
      If TransactionArray(i).IsBuy Then
        Volume = Volume + TransactionArray(i).Volume
      Else
        Volume = Volume - TransactionArray(i).Volume
      End If
    End If
  End If
  Next i
  GetVolume = Volume
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetPrice
'Input:
'Output:
'
'Description:   Get price for given name and date
'Author:        Fredrik Wendel
'Revision:      97      Created
'               980320  Search was reversed.
'               980812  Changes to cope with date type including
'                       time information.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetPrice(QDate As Date, Name As String)
  
  Dim i As Long
  Dim Ready As Boolean
  Dim Price As Single
  Dim PDArray() As PriceDataType
  Dim PDArraySize As Long
  Dim nDate As Long
  
  nDate = Int(QDate)    'Använder endast heltalsjämförelse. Snabbt!
  R_PriceDataArray DataPath + Trim(InvestInfoArray(GetInvestIndex(Trim(Name))).FileName) + ".prc", PDArray, PDArraySize
  If nDate < Int(PDArray(1).Date) Then
    Price = -1
  Else
    If nDate > Int(PDArray(PDArraySize).Date) Then
      Price = PDArray(PDArraySize).Close
    Else
      i = PDArraySize
      Ready = False
      Do While Not Ready
        If Int(PDArray(i).Date) <= nDate Then
          Ready = True
          Price = PDArray(i).Close
        Else
          i = i - 1
        End If
        If i = 0 Then Ready = True
      Loop
    End If
  End If
  GetPrice = Price
End Function
Function GetValue(Name As String, QDate As Date) As Single
  GetValue = GetPrice(QDate, Name) * GetVolume(Name, QDate)
End Function

Function GetCategoryIndex(Category As String) As Byte
  Dim i As Long
  Dim Found As Boolean
  Found = False
  i = 1
  Do While (i <= CategoryArraySize) And Not Found
    If Trim(Category) = Trim(CategoryArray(i)) Then Found = True
  i = i + 1
  Loop
  If Found = False Then
    GetCategoryIndex = -1
  Else
    GetCategoryIndex = i - 1
  End If
End Function


Public Sub CutArray(PDArray() As PriceDataType, Size As Long)
Dim TempArray() As PriceDataType
Dim i As Long
Dim j As Long
Dim k As Long
ReDim TempArray(Size)
For k = 1 To Size
  TempArray(k) = PDArray(k)
Next k
ReDim PDArray(350)
j = 1
For i = Size - 349 To Size
  PDArray(j) = TempArray(i)
  j = j + 1
Next i
Size = 350
End Sub


Function AlreadyExistInSearchResultArray(Name As String) As Boolean

  Dim i As Long
  AlreadyExistInSearchResultArray = False
  For i = 1 To SearchResultArraySize
    If Trim(Name) = Trim(SearchResultArray(i)) Then
      AlreadyExistInSearchResultArray = True
    End If
  Next i
End Function

Function GetMax(ValueArray() As Single) As Single
  
  Dim i As Long
  Dim Max As Single
  Max = -3E+38
  For i = LBound(ValueArray) To UBound(ValueArray)
    If ValueArray(i) > Max Then
      Max = ValueArray(i)
    End If
  Next i
  GetMax = Max

End Function

Function GetMin(ValueArray() As Single, ValueArraySize As Long, IgnoreZero As Boolean) As Single
  
  Dim i As Long
  Dim Min As Single
  Min = 3E+38
  For i = 1 To ValueArraySize
      If ValueArray(i) < Min Then
        If Not (IgnoreZero And ValueArray(i) = 0) Then
          Min = ValueArray(i)
        End If
      End If
  
  Next i
  GetMin = Min

End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          CalcSignalArray
'Input:         Indicator, Parameter1, Parameter2, Parameter3
'               Parameter4
'Output:        SignalArrray
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub CalcSignalArray(Signalarray() As Boolean, Indicator As String)
  Dim TempSignalArray() As Long
  Dim i As Integer
  ReDim Signalarray(PriceDataArraySize)
  ReDim TempSignalArray(PriceDataArraySize)
  
  With ParameterSettings
  
  If Trim(Indicator) = "Moving Average" Then
    Dim MAV1() As Single
    Dim MAV2() As Single
    ReDim MAV1(PriceDataArraySize)
    ReDim MAV2(PriceDataArraySize)
    
    If .MAVUseExp Then
      ExpMAVCalc PriceDataArray(), MAV1(), .MAV1, PriceDataArraySize
      ExpMAVCalc PriceDataArray(), MAV2(), .MAV2, PriceDataArraySize
    Else
      SimpleMAVCalc PriceDataArray(), MAV1(), .MAV1, PriceDataArraySize, False
      SimpleMAVCalc PriceDataArray(), MAV2(), .MAV2, PriceDataArraySize, False
    End If
    
    MAVSignalArray TempSignalArray(), MAV1, MAV2, PriceDataArraySize
        
  End If
    
  If Trim(Indicator) = "MACD" Then
    Dim MACD() As Single
    Dim MACDSignal() As Single
    ReDim MACD(PriceDataArraySize)
    ReDim MACDSignal(PriceDataArraySize)
    
    MACDCalc PriceDataArray(), MACD(), MACDSignal(), PriceDataArraySize, False
    
    MACDSignalArray TempSignalArray, MACD, MACDSignal, PriceDataArraySize
    
  End If
  
  If Trim(Indicator) = "Momentum" Then
    Dim MOMArray() As Single
    Dim MOMMAV() As Single
    ReDim MOMArray(PriceDataArraySize)
    ReDim MOMMAV(PriceDataArraySize)
    
    MomentumCalc PriceDataArray(), MOMArray(), .MOM, PriceDataArraySize, False
    
    ExpCalc MOMArray(), MOMMAV(), .MAVMOM, PriceDataArraySize
    
    MomentumSignalArray TempSignalArray, MOMArray, MOMMAV, PriceDataArraySize
    
  End If
    
  If Trim(Indicator) = "Money Flow Index" Then
    Dim MFI() As Single
    Dim MFIMAV() As Single
    ReDim MFI(PriceDataArraySize)
    ReDim MFIMAV(PriceDataArraySize)
    
    MFICalc PriceDataArray, MFI, .MFILength, PriceDataArraySize, False
        
    ExpCalc MFI, MFIMAV, .MAVMFI, PriceDataArraySize
    
    MFISignalArray TempSignalArray, MFI, MFIMAV, PriceDataArraySize
        
  End If
      
  If Trim(Indicator) = "Price Oscillator" Then
    Dim MAV3() As Single
    Dim MAV4() As Single
        
    ReDim MAV3(PriceDataArraySize)
    ReDim MAV4(PriceDataArraySize)
        
    If Not .OSCMAVExp Then
      SimpleMAVCalc PriceDataArray(), MAV3(), .PRIOSC1, PriceDataArraySize, False
      SimpleMAVCalc PriceDataArray(), MAV4(), .PRIOSC2, PriceDataArraySize, False
    Else
      ExpMAVCalc PriceDataArray(), MAV3(), .PRIOSC1, PriceDataArraySize
      ExpMAVCalc PriceDataArray(), MAV4(), .PRIOSC2, PriceDataArraySize
    End If
    
    MAVSignalArray TempSignalArray, MAV3, MAV4, PriceDataArraySize
    
  End If
    
  If Trim(Indicator) = "Relative Strength Index" Then
    Dim RSIArray() As Single
    Dim RSIMAV() As Single
    ReDim RSIArray(PriceDataArraySize)
    ReDim RSIMAV(PriceDataArraySize)
    
    RSICalc PriceDataArray(), RSIArray(), .RSI, PriceDataArraySize, False
    
    ExpCalc RSIArray(), RSIMAV(), .MAVRSI, PriceDataArraySize
    
    RSISignalArray TempSignalArray, RSIArray, RSIMAV, PriceDataArraySize
        
  End If
    
  If Trim(Indicator) = "Stochastic" Then
    Dim STOArray() As Single
    Dim STOMAV() As Single
    ReDim STOArray(PriceDataArraySize)
    ReDim STOMAV(PriceDataArraySize)
    
    StochasticOscCalc PriceDataArray(), STOArray(), .STO, PriceDataArraySize, False, .STOSlowing
    
    ExpCalc STOArray(), STOMAV(), .MAVSTO, PriceDataArraySize
    
    STOSignalArray TempSignalArray, STOArray, STOMAV, PriceDataArraySize
    
  End If
   
  If Trim(Indicator) = "Parabolic" Then
    Dim Parabolic() As Single
    ParabolicCalc PriceDataArray(), Parabolic(), Signalarray(), PriceDataArraySize, False, ParameterSettings.AccelerationFactor
  End If
  
  If Trim(Indicator) = "Ease Of Movement" Then
    Dim EOM() As Single
    Dim EOMMAV() As Single
    ReDim EOM(PriceDataArraySize)
    ReDim EOMMAV(PriceDataArraySize)
    
    EaseOfMovementCalc PriceDataArray, EOM, .EaseOfMovementLength, PriceDataArraySize, False
        
    If .PlotEaseOfMovementAndMAV Then
      SimpleCalc EOM, EOMMAV, .EaseOfMovementMAVLength, PriceDataArraySize, False
      EaseOfMovementSignalArray TempSignalArray, EOMMAV, PriceDataArraySize
    Else
      EaseOfMovementSignalArray TempSignalArray, EOM, PriceDataArraySize
    End If
    
  End If
  
  If Trim(Indicator) = "Rate-Of-Change" Then
    Dim ROC() As Single
    Dim ROCMAV() As Single
    ReDim ROC(PriceDataArraySize)
    ReDim ROCMAV(PriceDataArraySize)
    
    RateOfChangeCalc PriceDataArray, ROC, .RateOfChangeLength, PriceDataArraySize, False
    
    If .PlotRateOfChangeAndMAV Then
      SimpleCalc ROC, ROCMAV, .RateOfChangeMAVLength, PriceDataArraySize, False
      RateOfChangeSignalArray TempSignalArray, ROCMAV, PriceDataArraySize
    Else
      RateOfChangeSignalArray TempSignalArray, ROC, PriceDataArraySize
    End If
    
  End If
  
  If Trim(Indicator) = "TRIX" Then
    Dim TRIX() As Single
    Dim TRIXMAV() As Single
    ReDim TRIX(PriceDataArraySize)
    ReDim TRIXMAV(PriceDataArraySize)
    
    TRIXCalc PriceDataArray, TRIX, .TRIXLength, PriceDataArraySize, False
    
    SimpleCalc TRIX, TRIXMAV, .TRIXMAVLength, PriceDataArraySize, False
    
    TRIXSignalArray TempSignalArray, TRIX, TRIXMAV, PriceDataArraySize
    
  End If
  
  End With
  
  If Not Trim(Indicator) = "Parabolic" Then
    For i = 1 To PriceDataArraySize
      If TempSignalArray(i) = 1 Then
        Signalarray(i) = True
      Else
        Signalarray(i) = False
      End If
    Next i
  End If
End Sub

