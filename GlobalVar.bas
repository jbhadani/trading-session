Attribute VB_Name = "GlobalVar"
Option Explicit

' Version
Public IsProVersion As Boolean
Public ProVersionValidTo As Boolean

'Bj�rns Variabler
Public ShowGraphAtStartup As Integer

Public NbrVirtualDates As Integer
Public CycleLenth As Integer
Public VisitedPrefs As Boolean
Public LineLabelHandleArray(255, 1) As Single
Public LLHPointer As Integer
Public FreeDraw As Boolean

Public ZArray(10) As Long 'Motsvarar TrendLineNbr vi uppritning av Z-diagram
Public ZAIndex As Long
Public ZIndexArray(4) As Long  'Inneh�ller det f�rsta TrendLineNbr i ett Z-diagram
Public ZIAPointer As Long
'Slut Hack

' An array which contains the current investments
Public InvestInfoArray() As InvestInfoType

' Number of records in InvestInfoArray
Public InvestInfoArraySize As Long

Public PortfolioArray() As PortfolioType
Public PortfolioArraySize As Long

Public PriceDataArray() As PriceDataType
Public PriceDataArraySize As Long

Public TransactionArray() As TransactionType
Public TransactionArraySize As Long
Public IndicatorArray() As String
Public IndicatorArraySize As Long
Public NbrOfActions As Long
Public SearchResultArray() As String * 20
Public SearchResultArraySize As Long
' Environment variables

Public ShowGraph As Boolean

Public Path As String 'Added in 1.0.3
Public DataPath As String

Public UseGeneralIndicatorParameters As Boolean
Public blnUseSavedToolbarSettings As Boolean

Public ActiveIndex As Long
Public lngLastCompareObjectIndex As Long

Public ActivePortfolio As Boolean
Public ActivePortfolioFileName As String

Public CategoryArray() As String * 20
Public CategoryArraySize As Long

Public DoScroll As Boolean
Public DoUpdate As Boolean

Public GraphSettings As GraphSettings_v2_Type
Public ParameterSettings As ParameterSettingsType
Public InternetPageName As String

Public GraphInfoChanged As Boolean
Public SearchMessage As Boolean
Public IndicatortestVisible As Boolean
Public TestOK As Boolean
Public ShowIndicatorErrorMessage As Boolean
Public ErrorFoundInIndicator As Boolean

Public TLDrawMode As DrawModeType
Public AttachMode As AttachModeType
'Trendlinjer
Public NewLine As Boolean
Public LineSettings As TrendLineType

Public allowdrawlineingraph As Boolean

Public StartDate As Long



Public NbrOfDataTL As Long

Public LeftDrawTL As Long
Public RightDrawTL As Long
Public BottomDrawTL As Long
Public TopDrawTL As Long

Public LeftDrawCopy As Long
Public RightDrawCopy As Long
Public TopDrawCopy As Long
Public BottomDrawCopy As Long

Public YScaleTL As Single


Public MinValueTL As Single

Public TrendLineNbr As Long

Public LastSort As Long

Public TrendLineArray(1 To 3, 1 To 31) As TrendLineType

Public DrawSettings As DrawSettingsType

Public NewPort As Boolean
Public blnPortfolioModified 'Is true if the portfolio is modified.

Public PortfolioOverhead As PortfolioOverheadType

Public ReportWindowOpen As Boolean


Public YearToShowInShowPrices As Long
Public StartAndStopIndexArray() As StartAndStopIndexes
Public StartAndStopIndexArraySize As Long

Public TLWholeInView(1 To TrendLineSeries, 1 To 31) As TLWholeInViewType

Public WorkspaceSettingsArray() As WorkspaceSettings_v2_Type
Public WorkSpaceSettingsArraySize  As Long

Public ReplaceSem As SemaphoreType

Public gblnIncludeInStockList() As Boolean


Public PDIndicatortestArray() As PriceDataType
Public PDIndicatortestArraySize As Long
Public IndicatorTestOptions As IndicatortestOptionsType
Public IndicatorTestResult() As IndicatortestResultType
Public IndicatorTestStatistics As IndicatortestStatisticsType
Public IndicatortestObject As String

Public Triggers() As TriggerType

'#If FISH = 1 Then
   Public FishParameterSettings As FishParameterSettingsType
'#End If
Public OptimizeObject As String

Public dbg As New CDebug

Public ColorIndicatorArray() As String

Public NewObjects() As String

Public ActiveStockLabelsHandle As Long

Public Moving As Byte ' <> 0 if a trendline is moving

Public PRO As Boolean
Public DEMO As Boolean
Public FISH As Boolean

