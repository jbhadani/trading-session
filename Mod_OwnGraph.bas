Attribute VB_Name = "modGraph"
Option Explicit

Type GraphCopyType
  left As Integer
  right As Integer
  bottom As Integer
  top As Integer
  Start As Long
  Stop As Long
  Min As Single
  YScale As Single
  DaySpace As Long
  CloseWidth As Long
  DrawRect As GraphDrawRectType
  blnExponential As Boolean
End Type

Type GraphInfoType
  GraphForm As Form

  left As Long
  right As Long
  top As Long
  bottom As Long

  First As Long
  Last As Long
  
  XScale As Single
  YScale As Single
  
  DaySpace As Long
  DayWidth As Long
  
End Type

Dim mudtDrawArray() As PriceDataType
Dim mudtPriceGraphCopy As GraphCopyType


Dim TrackLabelMove As Boolean
Dim TrackLabelHandle As Long
Dim TrackLabelOriginRect As RECT
Dim TrackLabelOffsetX As Long
Dim TrackLabelOffsetY As Long
Dim TrackLabelDrawnX As Long
Dim TrackLabelDrawnY As Long

Dim RoundArray() As RoundArrayType


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          LogPriceDataArray
'Input:
'Output:
'
'Description:   Log the price data array
'Author:        Fredrik Wendel
'Revision:      970403  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub LogPriceDataArray(ByRef PDArray() As PriceDataType, PDArraySize As Long)
  Dim I As Long

  For I = 1 To PDArraySize
    If PDArray(I).Close <> 0 Then
      PDArray(I).Close = Log(PDArray(I).Close)
    End If
    If PDArray(I).High <> 0 Then
      PDArray(I).High = Log(PDArray(I).High)
    End If
    If PDArray(I).Low <> 0 Then
      PDArray(I).Low = Log(PDArray(I).Low)
    End If
  Next I
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          LogSingleDataArray
'Input:
'Output:
'
'Description:   Log the a single array
'Author:        Fredrik Wendel
'Revision:      970403  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub LogSingleArray(ByRef SingleArray() As Single, SingleArraySize)
  Dim I As Long
  
  For I = 1 To SingleArraySize
    If SingleArray(I) > 0 Then
      SingleArray(I) = Log(SingleArray(I))
    Else
      SingleArray(I) = 0
    End If
  Next I
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          SetStandardFont
'Input:
'Output:
'
'Description:   Sets the standard font
'Author:        Fredrik Wendel
'Revision:      991106 Initial version
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub SetStandardFont()
  Frm_OwnGraph.Font = "Arial"
  Frm_OwnGraph.FontSize = 8
  Frm_OwnGraph.FontBold = False
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          DrawGraph
'Input:
'Output:
'
'Description:   Draw a graph in frm_OwnGraph
'Author:        Fredrik Wendel
'Revision:      970403  Created
'               980929  Min value was added at x-axis
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub DrawGraph(TheForm As Form, _
              DrawRect As GraphDrawRectType, _
              ByRef PDArray() As PriceDataType, _
              PDArraySize As Long, _
              ByRef MAV1() As Single, _
              ByRef MAV2() As Single, _
              ByRef SignalColorArray() As Boolean, _
              GraphProp As GraphPropertiesType, _
              Start As Long, _
              DrawInfo As GraphDrawInfoType, _
              ByRef BollingerUpper() As Single, _
              ByRef BollingerLower() As Single, _
              ByRef MAV() As Single, _
              ByRef Parabolic() As Single, _
              ParabolicBuyOrSell() As Boolean, _
              ByRef lngSetupArray() As Long)

Dim GraphWidth As Long
Dim GraphHeight As Long
Dim GraphLeft As Long
Dim GraphBottom As Long

Dim j As Long
Dim YScale As Single
Dim XScale As Long

Dim ValueArray() As Single
Dim Min As Single
Dim Max As Single
Dim LastMonth As Byte
Dim GridYScale As Single

Dim NbrOfData As Long
Dim TempValue As Double
Dim TmpString As String

Dim PriceColor As Long
Dim LastColor As Long
Dim lngStart As Long
Dim lngStop As Long

Dim NbrOfDaysLastMonth As Long

Dim blnShowSetup As Boolean

Dim GraphInfo As GraphInfoType

Dim LastYLineDate As Date
Dim LastXCoord As Long
Dim MonthlyDiff As Long
Dim strXLabel As String

Dim AxisX1 As Long
Dim AxisX2 As Long
Dim AxisY1 As Long
Dim AxisY2 As Long

Dim DrawX1 As Long
Dim DrawX2 As Long
Dim DrawY1 As Long
Dim DrawY2 As Long


Dim DrawHeight As Long
Dim DrawWidth As Long

Dim lngLegendX As Long
Dim lngLegendY As Long

Dim sngNewMin As Single
Dim sngScaleStep As Single
Dim lngNbrOfSteps As Long
Dim lngNbrOfSmallSteps As Long

Dim PriceVolume() As Single
Dim XScalePriceVol As Single
Dim MaxPriceVolume As Single
Dim MinPriceVolume As Single
Dim SuccededPriceVolume As Boolean

ReDim mudtDrawArray(1 To UBound(PDArray))
mudtDrawArray = PDArray


'   | (X1,Y2)             (X2,Y2) |
'   |              |---->         |
'   |              |  +           |
'   |              \/             |
'   |                             |
'   |_____________________________|
'   /                             \
'(X1,Y1)                        (X2,Y1)
'
  With DrawRect

'#If DBUG = 1 Then
'  GraphDrawRect theForm, DrawRect
'#End If
  
  AxisX1 = .left + LeftAxisDist
  AxisX2 = .Width - RightAxisDist
  AxisY1 = .bottom - BottomAxisDist
  AxisY2 = .bottom - .Height + TopDrawDist
  
  DrawX1 = .left + LeftDrawDist
  DrawX2 = .Width - RightDrawDist
  DrawY1 = .bottom - BottomDrawDist
  DrawY2 = .bottom - .Height + TopDrawDist
  
  DrawHeight = DrawY1 - DrawY2
  DrawWidth = DrawX2 - DrawX1
  
  LeftDrawTL = AxisX1
  BottomDrawTL = AxisY1
  TopDrawTL = AxisY2

  LeftDrawTL = DrawX1
  BottomDrawTL = DrawY1
  TopDrawTL = DrawY2
  
  End With
  
  blnShowSetup = False
  For j = 1 To UBound(GraphProp.Setup)
    blnShowSetup = blnShowSetup Or GraphProp.Setup(j).blnShow
  Next j
  

  lngStart = DrawInfo.Start
With TheForm


.BackColor = GraphProp.BackGroundColor


' Axis
.DrawWidth = 2
.DrawStyle = 0

SetStandardFont

' Draw draw area background
TheForm.Line (AxisX1 + 1, AxisY1 - 1)-(AxisX2 - 1, AxisY2 + 1), GraphProp.lngDrawAreaColor, BF

' Draw axis
TheForm.Line (AxisX1, AxisY1)-(AxisX2, AxisY1), GraphProp.AxisColor
TheForm.Line (AxisX1, AxisY1)-(AxisX1, AxisY2 - 1), GraphProp.AxisColor
TheForm.Line (AxisX2, AxisY2)-(AxisX2, AxisY1), GraphProp.AxisColor
TheForm.Line (AxisX1, AxisY2)-(AxisX2, AxisY2), GraphProp.AxisColor

NbrOfData = GetNbrOfDataPoints(DrawRect, DrawInfo, UBound(PDArray))
lngStop = lngStart + NbrOfData - 1
RightDrawTL = LeftDrawTL + NbrOfData * DrawInfo.DaySpace - 1 'Korr. f�r att det skall funka. Vet ej varf�r!

GetMinAndMaxFromPriceData PDArray, lngStart, lngStop, Min, Max

If Not GraphProp.ExponentialPrice Then
  GetScale Min, Max, 10, sngNewMin, sngScaleStep, lngNbrOfSteps, lngNbrOfSmallSteps
  Min = sngNewMin
  Max = sngNewMin + (sngScaleStep) * lngNbrOfSteps
Else
  sngScaleStep = (Max - Min) / 10
  lngNbrOfSteps = 10
  lngNbrOfSmallSteps = 2
End If

' PixelPerPrice for trendlines
MinValueTL = Min
NbrOfDataTL = NbrOfData

If GraphProp.blnShowSupportResistance And Not GraphProp.ExponentialPrice Then
  MakePriceVolumeArray PDArray(), lngStart, lngStop, sngNewMin, sngScaleStep, _
                       lngNbrOfSteps, lngNbrOfSmallSteps, PriceVolume, SuccededPriceVolume
End If

  
  'Print text at the top of the price graph
  .ForeColor = GraphProp.AxisColor
  .CurrentX = DrawX1
  .CurrentY = AxisY2 - TopDrawDist
  If NbrVirtualDates <> 0 Then
    TheForm.Print Trim(GraphProp.Name) + " (" + GetDateString(PDArray(lngStart).Date); "--" & GetDateString(PDArray(lngStart + NbrOfData - NbrVirtualDates - 1).Date) + ") " + "Senast:" + FormatNumber(GraphProp.strLatest, 2); 'Modified in 1.0.3
  Else
    TheForm.Print Trim(GraphProp.Name) + " (" + GetDateString(PDArray(lngStart).Date); "--" & GetDateString(PDArray(lngStart + NbrOfData - 1).Date) + ") " + "Senast:" + FormatNumber(GraphProp.strLatest, 2); 'Modified in 1.0.3
  End If
  TheForm.Print " 1Y: " & GraphProp.strGainYear;
  TheForm.Print " 1M: " & GraphProp.strGainMonth;
  TheForm.Print " 1W: " & GraphProp.strGainWeek;
  TheForm.Print " 1D: " & GraphProp.strGainDay;
  If GraphSettings.ShowSignalColor Then
    TheForm.Print " Signal: " + GraphProp.strColorSignallingIndicator;
    .ForeColor = GraphProp.SignalColorBuy
    TheForm.Print " K�p";
    .ForeColor = GraphProp.SignalColorSell
    TheForm.Print " S�lj"
  End If
  
 

If Not (Max = 0 And Min = 0) And Not (Max = Min) Then 'Don't draw pricegraph if all high equal 0 and all low equal 0
  YScale = (DrawHeight) / (Max - Min)
  YScaleTL = YScale
  
  ' Copy draw data
  With mudtPriceGraphCopy
  .bottom = DrawY1
  .top = DrawY2
  .left = DrawX1
  .right = DrawX1 + NbrOfData * DrawInfo.DaySpace - 1
  .Start = lngStart
  .Stop = DrawInfo.Stop
  .DaySpace = DrawInfo.DaySpace
  .Min = Min
  .YScale = YScale
  .CloseWidth = GraphProp.lngCloseWidth
  .DrawRect.left = DrawRect.left
  .DrawRect.Width = DrawRect.Width
  .DrawRect.bottom = DrawRect.bottom
  .DrawRect.Height = DrawRect.Height
  .blnExponential = GraphProp.ExponentialPrice
  End With

  
  .ForeColor = GraphProp.AxisColor
  .DrawWidth = 1
  .DrawStyle = 0
  
  ' Draw min at x-axis (added 980929)
  .CurrentX = DrawRect.left + DrawRect.Width - RightLabelDist
  .CurrentY = AxisY1 - 10
  If GraphProp.ExponentialPrice Then
       TempValue = Exp(Min) 'log
  Else
     TempValue = Min
  End If
  TheForm.Print GetCurrencyString(TempValue)
  
  .ForeColor = GraphProp.GridColor
  .DrawStyle = vbDot
  GridYScale = DrawHeight / (lngNbrOfSteps * lngNbrOfSmallSteps)
  If GraphProp.blnShowSupportResistance And Not GraphProp.ExponentialPrice And SuccededPriceVolume Then
    MaxPriceVolume = GetMax(PriceVolume)
    XScalePriceVol = DrawWidth / 4 / MaxPriceVolume
  End If
  For j = 1 To lngNbrOfSteps * lngNbrOfSmallSteps - 1
    If GraphProp.blnShowSupportResistance And Not GraphProp.ExponentialPrice And SuccededPriceVolume Then
      .DrawStyle = vbSolid
      TheForm.Line (DrawX1 - 1, DrawY1 - (j - 1) * GridYScale - 4)- _
                   (DrawX1 - 1 + PriceVolume(j - 1) * XScalePriceVol, DrawY1 - (j) * GridYScale + 5) _
                   , GraphProp.lngSupportResistanceColor, BF
    End If
    If GraphProp.ShowXGrid Then
  
      If (j Mod lngNbrOfSmallSteps) = 0 Then
        .DrawStyle = vbSolid
        TheForm.Line (DrawX1, DrawY1 - Int(j * GridYScale))-(DrawX2, DrawY1 - Int(j * GridYScale))
      ElseIf GraphProp.ShowMinXGrid Then
        .DrawStyle = vbDot
        TheForm.Line (DrawX1, DrawY1 - Int(j * GridYScale))-(DrawX2, DrawY1 - Int(j * GridYScale))
      End If
    End If
    
    If (j Mod lngNbrOfSmallSteps) = 0 Then
      .CurrentX = DrawRect.left + DrawRect.Width - RightLabelDist
      .CurrentY = AxisY1 - Int(j * GridYScale) - 8
      If GraphProp.ExponentialPrice Then
        TempValue = Exp(Min + sngScaleStep * (j \ lngNbrOfSmallSteps)) 'log
      Else
        TempValue = Min + sngScaleStep * (j \ lngNbrOfSmallSteps)
      End If
      .ForeColor = GraphProp.AxisColor
      TheForm.Print GetCurrencyString(TempValue)
      .ForeColor = GraphProp.GridColor
    End If
    
   Next j
   
  XScale = DrawInfo.DaySpace

Dim atimer As Single
atimer = Timer
With GraphInfo
   Set .GraphForm = Frm_OwnGraph

  .bottom = DrawY1
  .top = DrawY2
  .left = DrawX1
  .right = DrawX2
  
  .First = lngStart
  .Last = lngStart + NbrOfData - 1
  
  .XScale = XScale
  .YScale = YScale
End With
If PRO = True Then
  If GraphProp.blnShowFish Then
    DrawFish GraphInfo, PDArray, Min, Max, GraphProp.lngFishColor
  End If
End If



  
  j = 0
  .DrawStyle = 0
  
  LastMonth = Month(PDArray(lngStart).Date)
  LastYLineDate = PDArray(lngStart).Date
  For j = 0 To NbrOfData - 1  'Points - 1
    'YGrid
    If blnShowSetup Then
      If lngSetupArray(lngStart + j) <> 0 Then
        
        .Font = "Wingdings 3"
        .FontSize = 14
        .FontBold = True
        .CurrentX = DrawX1 + j * XScale - .TextWidth("Y") / 2 + 2
        .CurrentY = Int(DrawY1 - (PDArray(j + lngStart).Low - Min) * YScale) + 2
        .ForeColor = GraphProp.Setup(lngSetupArray(lngStart + j)).lngColor
        Frm_OwnGraph.Print "h"
        SetStandardFont
      End If
    End If
    
    If GraphProp.ShowYGrid = True Then
      Select Case DrawInfo.DWM
      Case dwmDaily
        If Month(LastYLineDate) <> Month(PDArray(lngStart + j).Date) Then
          .DrawWidth = 1
          .DrawStyle = 0
          TheForm.Line (DrawX1 + j * XScale, DrawY2)-(DrawX1 + j * XScale, DrawY1), GraphProp.GridColor
          'Print the Date
          .CurrentY = .CurrentY + 2
          .ForeColor = GraphProp.AxisColor
          strXLabel = GetYearMonthString(PDArray(lngStart + j).Date)
          If .CurrentX > (LastXCoord + 10) Then
            LastXCoord = .CurrentX + .TextWidth(strXLabel)
            TheForm.Print strXLabel
          End If
          LastYLineDate = PDArray(lngStart + j).Date
        End If
      Case dwmWeekely
        If ((Month(LastYLineDate) < 7) And (Month(PDArray(lngStart + j).Date) >= 7)) Or _
        ((Month(LastYLineDate) >= 7) And (Month(PDArray(lngStart + j).Date) < 7)) Then
          .DrawWidth = 1
          .DrawStyle = 0
          TheForm.Line (DrawX1 + j * XScale, DrawY2)-(DrawX1 + j * XScale, DrawY1), GraphProp.GridColor
          'Print the Date
          .CurrentY = .CurrentY + 2
          .ForeColor = GraphProp.AxisColor
          strXLabel = GetYearMonthString(PDArray(lngStart + j).Date)
          If .CurrentX > (LastXCoord + 10) Then
            LastXCoord = .CurrentX + .TextWidth(strXLabel)
            TheForm.Print strXLabel
          End If
          LastYLineDate = PDArray(lngStart + j).Date
        End If
      Case dwmMonthly
        If Year(LastYLineDate) <> Year(PDArray(lngStart + j).Date) Then
          .DrawWidth = 1
          .DrawStyle = 0
          TheForm.Line (DrawX1 + j * XScale, DrawY2)-(DrawX1 + j * XScale, DrawY1), GraphProp.GridColor
          'Print the Date
          .CurrentY = .CurrentY + 2
          .ForeColor = GraphProp.AxisColor
          strXLabel = Mid(PDArray(lngStart + j).Date, 3, 2)
          If .CurrentX > (LastXCoord + 10) Then
            LastXCoord = .CurrentX + .TextWidth(strXLabel)
            TheForm.Print strXLabel
          End If
          LastYLineDate = PDArray(lngStart + j).Date
        End If
      End Select
    End If

    LastMonth = Month(PDArray(j + lngStart).Date)
    
    If GraphProp.ShowSignalColor Then
      .DrawWidth = 1
      .DrawStyle = vbSolid
      If SignalColorArray(j + lngStart) Then
        .ForeColor = GraphProp.SignalColorBuy
        PriceColor = GraphProp.SignalColorBuy
      Else
        .ForeColor = GraphProp.SignalColorSell
        PriceColor = GraphProp.SignalColorSell
      End If
      If GraphSettings.ShowVerticalColorLines Then
        If PriceColor <> LastColor Then
          LastColor = PriceColor
          TheForm.Line (DrawX1 + j * XScale, GraphBottom - 2)-(DrawX1 + j * XScale, GraphBottom - 2 - GraphHeight + 15)
        End If
      End If
    Else
        .ForeColor = GraphProp.PriceColor
        PriceColor = GraphProp.PriceColor
    End If

    If GraphProp.ShowHLC Then
      .DrawWidth = DrawInfo.DayWidth
      .DrawStyle = vbSolid
      If PDArray(j + lngStart).Low >= Min Then
        
        TheForm.Line (DrawX1 + j * XScale, Int(DrawY1 - (PDArray(j + lngStart).Low - Min) * YScale))-(DrawX1 + j * XScale, Int(DrawY1 - (PDArray(j + lngStart).High - Min) * YScale))
        TheForm.Line (DrawX1 + j * XScale, Int(DrawY1 - (PDArray(j + lngStart).Close - Min) * YScale))-(DrawX1 + (j * XScale) + GraphProp.lngCloseWidth + 1, Int(DrawY1 - (PDArray(j + lngStart).Close - Min) * YScale)), GraphProp.lngCloseColor
      End If
    End If
  Next j
    
  
    If Not GraphProp.ShowHLC Then
    .DrawStyle = vbSolid
    .DrawWidth = DrawInfo.DayWidth
    .CurrentY = Int(DrawY1 - (PDArray(lngStart).Close - Min) * YScale)
    .CurrentX = DrawX1
    For j = 1 To NbrOfData - 1
      If GraphProp.ShowSignalColor Then
        If SignalColorArray(j + lngStart) Then
          PriceColor = GraphProp.SignalColorBuy
        Else
          PriceColor = GraphProp.SignalColorSell
        End If
      Else
        PriceColor = GraphProp.PriceColor
      End If
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (PDArray(j + lngStart).Close - Min) * YScale)), PriceColor
    Next j
  End If

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Draws MAV in the price graph
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
  .DrawWidth = 1
  .DrawStyle = vbSolid
  
  'MAV1
  If GraphProp.ShowPriceMAV1 Then
    .CurrentY = Int(DrawY1 - (MAV1(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    .ForeColor = GraphProp.PriceMAV1Color
    For j = 1 To NbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (MAV1(j + lngStart) - Min) * YScale))
    Next j
  End If
  
  'MAV2
  If GraphProp.ShowPriceMAV2 Then
    .CurrentY = Int(DrawY1 - (MAV2(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    .ForeColor = GraphProp.PriceMAV2Color
    For j = 1 To NbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (MAV2(j + lngStart) - Min) * YScale))
    Next j
  End If
End If

'''''''''''''''''''''''''''''''''''''
'Draw Parabolic
'''''''''''''''''''''''''''''''''''''
  If GraphProp.ShowParabolic Then
      For j = 0 To NbrOfData - 1
      .CurrentX = DrawX1 + j * XScale
      .CurrentY = Int(DrawY1 - (Parabolic(lngStart + j) - Min) * YScale)
      TheForm.Circle (.CurrentX, .CurrentY), 0.5, GraphProp.ParabolicColor
    Next j
  End If
  
'''''''''''''''''''''''''''''''''''''
'Draw Bollinger
'''''''''''''''''''''''''''''''''''''
  .DrawWidth = 1
  .DrawStyle = vbSolid
  
  If GraphProp.ShowBollinger Then
    'MAV
    .CurrentY = Int(DrawY1 - (MAV(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    For j = 1 To NbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (MAV(j + lngStart) - Min) * YScale)), GraphProp.BollingerColor
    Next j
    
    'BollingerUpper
    .CurrentY = Int(DrawY1 - (BollingerUpper(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    For j = 1 To NbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (BollingerUpper(j + lngStart) - Min) * YScale)), GraphProp.BollingerColor
    Next j
    
    'BollingerLower
    .CurrentY = Int(DrawY1 - (BollingerLower(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    For j = 1 To NbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(DrawY1 - (BollingerLower(j + lngStart) - Min) * YScale)), GraphProp.BollingerColor
    Next j
  End If

'''''''''''''''''''''''''''''''''''''
'Draw legends
'''''''''''''''''''''''''''''''''''''
  With DrawRect
  lngLegendY = .bottom - .Height + TopLegendDist
  lngLegendX = .left + LeftLegendDist
  End With
  
  If GraphProp.ShowSignalColor Then
    DrawLegend TheForm, lngLegendX, lngLegendY, "Kurs", True, _
               GraphProp.AxisColor, GraphProp.SignalColorBuy, _
               GraphProp.SignalColorSell
  Else
    DrawLegend TheForm, lngLegendX, lngLegendY, "Kurs", False, GraphProp.AxisColor, GraphProp.PriceColor
  End If
  
  If GraphProp.blnShowSupportResistance Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "St�d/motst�nd", _
               False, GraphProp.AxisColor, _
               GraphProp.lngSupportResistanceColor
  End If
  
  If GraphProp.ShowPriceMAV1 Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "MAV " & CStr(ParameterSettings.MAV1), _
               False, GraphProp.AxisColor, _
               GraphProp.PriceMAV1Color
  End If
  
  If GraphProp.ShowPriceMAV2 Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "MAV " & CStr(ParameterSettings.MAV2), _
               False, GraphProp.AxisColor, _
               GraphProp.PriceMAV2Color
  End If
  If GraphProp.ShowBollinger Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "Bollinger " & CStr(ParameterSettings.BollingerMAVLength) & " " _
               & CStr(ParameterSettings.BollingerSTDFactor / 10), _
               False, GraphProp.AxisColor, GraphProp.BollingerColor
  End If
  If GraphProp.ShowParabolic Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "Parabolic " & FormatNumber(ParameterSettings.AccelerationFactor, 2, vbTrue), _
               False, GraphProp.AxisColor, GraphProp.ParabolicColor
  End If
  If GraphProp.blnShowFish Then
    lngLegendY = lngLegendY + BetweenLegendDist
    DrawLegend TheForm, lngLegendX, lngLegendY, _
               "FishNet ", _
               False, GraphProp.AxisColor, GraphProp.lngFishColor
  End If
  


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Volumegraph
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

GraphBottom = Int(GraphProp.GraphBottom)
GraphHeight = (GraphProp.GraphHeight * 0.2)

End With

Debug.Print BottomDrawTL
Debug.Print DrawY1
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          DrawFish
'Input:
'Output:
'
'Description:   Draws a FishNet graph.
'Author:        Fredrik Wendel
'Revision:      980503  Created
'               990827  Now sets the drawstyle to solid
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


'#If FISH = 1 Then
  Sub DrawFish(ByRef GraphInfo As GraphInfoType, ByRef PDArray() As PriceDataType, Min As Single, Max As Single, lngColor As Long)
  
  Dim I As Long
  Dim j As Long
  Dim tmpArr() As Single
  Dim plyFish As New Polygon
  Dim blnContainData As Boolean
    
    Set plyFish.Device = GraphInfo.GraphForm
  
    GraphInfo.GraphForm.FillStyle = vbFSTransparent
    GraphInfo.GraphForm.ForeColor = lngColor
    GraphInfo.GraphForm.DrawStyle = vbSolid
    
    ' Set the shortest MAV
    I = CLng(FishParameterSettings.lngShortest)
    Do While I <= CLng(FishParameterSettings.lngLongest)
      SimpleMAVCalc PDArray, tmpArr, I, UBound(PDArray), True
        blnContainData = False
        
        For j = 0 To (GraphInfo.Last - GraphInfo.First)
          If tmpArr(j + GraphInfo.First) > Min And tmpArr(j + GraphInfo.First) < Max Then
            plyFish.Point GraphInfo.left + j * GraphInfo.XScale, GraphInfo.bottom - (tmpArr(j + GraphInfo.First) - Min) * GraphInfo.YScale
            blnContainData = True
          Else
            If blnContainData Then
                plyFish.Draw
                plyFish.Clear
                blnContainData = False
            End If
          End If
        Next j
        I = I + FishParameterSettings.lngStep
        If blnContainData Then
          plyFish.Draw
          plyFish.Clear
        End If
    Loop
    
  
  End Sub
'#End If


Sub DrawVolume(TheForm As Form, _
               DrawInfo As GraphDrawInfoType, _
               DrawRect As GraphDrawRectType, _
               VolumeProperties As VolumePropertiesType, _
               ByRef PDArray() As PriceDataType)

Dim lngNbrOfData As Long
Dim lngStart As Long

Dim ValueArray() As Single

Dim Min As Single
Dim Max As Single

Dim YScale As Single
Dim XScale As Single

Dim AxisX1 As Long
Dim AxisX2 As Long
Dim AxisY1 As Long
Dim AxisY2 As Long

Dim DrawX1 As Long
Dim DrawX2 As Long
Dim DrawY1 As Long
Dim DrawY2 As Long

Dim DrawHeight As Long
Dim DrawWidth As Long

Dim lngLegendX As Long
Dim lngLegendY As Long

Dim j As Long


  With DrawRect
  AxisX1 = .left + LeftAxisDist
  AxisX2 = .Width - RightAxisDist
  AxisY1 = .bottom - BottomAxisDist
  AxisY2 = .bottom - .Height + TopDrawDist
  
  DrawX1 = .left + LeftDrawDist
  DrawX2 = .Width - RightDrawDist
  DrawY1 = .bottom - BottomDrawDist
  DrawY2 = .bottom - .Height + TopDrawDist
  
  DrawHeight = DrawY1 - DrawY2
  DrawWidth = DrawX2 - DrawX1
  
  


  End With
  
  lngStart = DrawInfo.Start
  lngNbrOfData = GetNbrOfDataPoints(DrawRect, DrawInfo, UBound(PDArray))
  
  
  With TheForm
  
  ' Draw draw area background
  TheForm.Line (AxisX1 + 1, AxisY1 - 1)-(AxisX2 - 1, AxisY2 + 1), VolumeProperties.lngColorDrawArea, BF

  ' Draw axis
  .DrawWidth = 2
  .DrawStyle = vbSolid
  .ForeColor = VolumeProperties.ColorAxis
  TheForm.Line (AxisX1, AxisY1)-(AxisX2, AxisY1)
  TheForm.Line (AxisX1, AxisY1)-(AxisX1, AxisY2 - 1)
  TheForm.Line (AxisX2, AxisY2)-(AxisX2, AxisY1)
  TheForm.Line (AxisX1, AxisY2)-(AxisX2, AxisY2)
  
  'Get max and min
  ReDim ValueArray(1 To UBound(PDArray))
  For j = lngStart To lngStart + lngNbrOfData - 1
    ValueArray(j - lngStart + 1) = PDArray(j).Volume
  Next j
  Min = GetMin(ValueArray, lngNbrOfData, False)
  Max = GetMax(ValueArray)

If Not (Min = 0 And Max = 0) And Not (Min = Max) Then ' Don't draw volume bars if all equal 0
  YScale = DrawHeight / (Max - Min)
  XScale = DrawInfo.DaySpace
  
  'Draw the volume bars
  .DrawStyle = vbSolid
  .DrawWidth = DrawInfo.DayWidth
  .ForeColor = VolumeProperties.ColorVolume
  For j = 0 To lngNbrOfData - 1
    TheForm.Line (DrawX1 + j * XScale, DrawY1)-(DrawX1 + j * XScale, Int(DrawY1 - (PDArray(j + lngStart).Volume - Min) * YScale))  'Modified 981228 by FW
  Next j
  
End If

'Draw Legends
With DrawRect
lngLegendY = .bottom - .Height + TopLegendDist
lngLegendX = .left + LeftLegendDist
DrawLegend TheForm, lngLegendX, lngLegendY, "Oms�ttning", False, VolumeProperties.ColorAxis, VolumeProperties.ColorVolume
End With
End With
End Sub


Sub DrawIndicator(TheForm As Form, DrawRect As GraphDrawRectType, Serie1Array() As Single, Serie2Array() As Single, SerieArraySize, IndProp As IndicatorPropertiesType, DrawInfo As GraphDrawInfoType)

Dim lngNbrOfData As Long
Dim Min As Single
Dim Max As Single
Dim Min2 As Single
Dim Max2 As Single
Dim j As Long
Dim ValueArray() As Single
Dim ZeroInt As Single
Dim GraphMin As Single
Dim NegMin As Single
Dim TmpString As String
Dim TmpValue As String

Dim lngStart As Long
Dim lngStop As Long
Dim AxisX1 As Long
Dim AxisX2 As Long
Dim AxisY1 As Long
Dim AxisY2 As Long

Dim DrawX1 As Long
Dim DrawX2 As Long
Dim DrawY1 As Long
Dim DrawY2 As Long

Dim DrawHeight As Long
Dim DrawWidth As Long

Dim YScale As Single
Dim XScale As Long

Dim lngLegendX As Long
Dim lngLegendY As Long


With DrawRect
  AxisX1 = .left + LeftAxisDist
  AxisX2 = .Width - RightAxisDist
  AxisY1 = .bottom - BottomAxisDist
  AxisY2 = .bottom - .Height + TopDrawDist
  
  DrawX1 = .left + LeftDrawDist
  DrawX2 = .Width - RightDrawDist
  DrawY1 = .bottom - BottomDrawDist
  DrawY2 = .bottom - .Height + TopDrawDist
  
  DrawHeight = DrawY1 - DrawY2
  DrawWidth = DrawX2 - DrawX1
End With
  
lngStart = DrawInfo.Start
lngStop = DrawInfo.Stop
lngNbrOfData = lngStop - lngStart + 1
  
 XScale = DrawInfo.DaySpace

With IndProp

If .FixMaxMin Then
  Max = .Max
  Min = .Min
Else
  GetMinAndMaxEx Serie1Array, lngStart, lngStop, Min, Max
  If IndProp.ShowSerie2 Then
    GetMinAndMaxEx Serie2Array, lngStart, lngStop, Min2, Max2
    If Min2 < Min Then Min = Min2
    If Max2 > Max Then Max = Max2
 End If
End If

End With
With TheForm

.DrawWidth = 2
.DrawStyle = vbSolid

' Draw draw area background
TheForm.Line (AxisX1 + 1, AxisY1 - 1)-(AxisX2 - 1, AxisY2 + 1), IndProp.lngColorDrawArea, BF


TheForm.Line (AxisX1, AxisY1)-(AxisX1, AxisY2), IndProp.AxisColor
TheForm.Line (AxisX2, AxisY1)-(AxisX2, AxisY2), IndProp.AxisColor

NegMin = 0
If (Max - Min) <> 0 Then ''
  YScale = (DrawHeight) / (Max - Min)
  If Min < 0 Then
    ZeroInt = DrawY1 - DrawHeight * Abs(Min) / (Max - Min)
    NegMin = Min
    GraphMin = 0
    Min = 0
  Else
    GraphMin = Min
    ZeroInt = DrawY1
  End If

  TheForm.Line (DrawX1, Int(ZeroInt))-(DrawX2, Int(ZeroInt)), IndProp.AxisColor
  .ForeColor = IndProp.AxisColor
  .CurrentY = .CurrentY - 6
  
  .CurrentX = DrawRect.left + DrawRect.Width - RightLabelDist
  If IndProp.ShowScale Then
  
    TheForm.Print FormatNumber(GraphMin, 2)
  End If
  .DrawWidth = 1
  TheForm.Line (DrawX1, Int(ZeroInt - (Max - Min) * YScale))-(DrawX2, Int(ZeroInt - (Max - Min) * YScale)), IndProp.GridColor
  .CurrentY = .CurrentY - 6
  .CurrentX = DrawRect.left + DrawRect.Width - RightLabelDist
  If IndProp.ShowScale Then
  
    TheForm.Print FormatNumber(Max, 2)
  End If
  If NegMin <> 0 Then
    .DrawWidth = 1
    TheForm.Line (DrawX1, DrawY1)-(DrawX2, DrawY1), IndProp.GridColor
    .CurrentY = .CurrentY - 6
    .CurrentX = DrawRect.left + DrawRect.Width - RightLabelDist
    
    If IndProp.ShowScale Then
  
      TheForm.Print FormatNumber(NegMin, 2)
    End If
  End If
    .DrawStyle = vbDot
  If IndProp.ShowFixLine1 Then
    TheForm.Line (DrawX1, Int(ZeroInt - (IndProp.FixLine1Value - Min) * YScale))-(DrawX2, Int(ZeroInt - (IndProp.FixLine1Value - Min) * YScale))
  End If
  If IndProp.ShowFixLine2 Then
    TheForm.Line (DrawX1, Int(ZeroInt - (IndProp.FixLine2Value - Min) * YScale))-(DrawX2, Int(ZeroInt - (IndProp.FixLine2Value - Min) * YScale))
  End If
  
  .DrawStyle = vbSolid
  .DrawWidth = 1
  'Draw Serie 1
  If IndProp.ShowSerie1 Then
    .CurrentY = Int(ZeroInt - (Serie1Array(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    .ForeColor = IndProp.Serie1Color
    For j = 1 To lngNbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(ZeroInt - (Serie1Array(j + lngStart) - Min) * YScale))
    Next j
  End If
  'Draw Serie 2
  If IndProp.ShowSerie2 Then
    .CurrentY = Int(ZeroInt - (Serie2Array(lngStart) - Min) * YScale)
    .CurrentX = DrawX1
    .ForeColor = IndProp.Serie2Color
    For j = 1 To lngNbrOfData - 1
      TheForm.Line Step(0, 0)-(DrawX1 + j * XScale, Int(ZeroInt - (Serie2Array(j + lngStart) - Min) * YScale))
    Next j
  End If
  
  'Draw Legends
  With DrawRect
  lngLegendY = .bottom - .Height + TopLegendDist
  lngLegendX = .left + LeftLegendDist
  If IndProp.ShowSerie1 Then
    DrawLegend TheForm, lngLegendX, lngLegendY, IndProp.Serie1Name, False, IndProp.AxisColor, IndProp.Serie1Color
  End If
  lngLegendY = lngLegendY + BetweenLegendDist
  If IndProp.ShowSerie2 Then
    DrawLegend TheForm, lngLegendX, lngLegendY, IndProp.Serie2name, False, IndProp.AxisColor, IndProp.Serie2Color
  End If
  End With

End If
End With
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  DrawLegend
'Parameters:
'Return:        None
'Description:   Draw a legend on given form.
'Created:       990728 Fredrik Wendel
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub DrawLegend(frmDraw As Form, _
               lngX As Long, _
               lngY As Long, _
               strText As String, _
               blnSplitted As Boolean, _
               lngBorderColor As Long, _
               lngFillColor1 As Long, _
               Optional lngFillColor2 As Long)

  With frmDraw
    .DrawWidth = 1
    .FillStyle = vbFSSolid
    If Not blnSplitted Then
      .FillColor = lngFillColor1
      frmDraw.Line (lngX, lngY)-Step(LegendBoxWidth, LegendBoxHeight), lngBorderColor, B
    Else
      .FillColor = lngFillColor1
      frmDraw.Line (lngX, lngY)-Step(LegendBoxWidth / 2, LegendBoxHeight), lngBorderColor, B
      .FillColor = lngFillColor2
      frmDraw.Line (lngX + LegendBoxWidth / 2, lngY)-Step(LegendBoxWidth / 2, LegendBoxHeight), lngBorderColor, B
    End If
    .ForeColor = lngBorderColor
    .CurrentX = lngX + LegendBoxWidth + BetweenLegendBoxTextDist
    .CurrentY = lngY - LegendBoxOffset
    frmDraw.Print strText
  End With
End Sub

Sub SetScrollbar(DrawRect As GraphDrawRectType, DrawInfo As GraphDrawInfoType, PDArraySize As Long)
Dim NbrOfDataPoints As Long
Dim ShowScroll As Boolean

dbg.Enter "SetScrollbar"
NbrOfDataPoints = GetNbrOfDataPoints(DrawRect, DrawInfo, PDArraySize)

If NbrOfDataPoints = PDArraySize Then
  ShowScroll = False
Else
  ShowScroll = True
End If

With Frm_OwnGraph
DoScroll = False
  If Not ShowScroll Then
    .ScrollBar.Visible = False
    .ScrollBar.Value = 1
    StartDate = 1
  Else
  .ScrollBar.Visible = True
  .ScrollBar.top = DrawRect.Height - 13
  .ScrollBar.Width = DrawRect.Width - LeftAxisDist - RightAxisDist
  .ScrollBar.left = DrawRect.left + LeftAxisDist
  .ScrollBar.Height = 10
  .ScrollBar.Min = 1
  .ScrollBar.Max = PDArraySize - NbrOfDataPoints + 1
  
  .ScrollBar.Value = DrawInfo.Start
  dbg.Out "Scrollbar set to " & CStr(DrawInfo.Start) & " " & CStr(PDArraySize - NbrOfDataPoints + 1)
  StartDate = DrawInfo.Start
  
  End If
DoScroll = True
End With
dbg.Out "The scrollbar is " & CStr(Frm_OwnGraph.ScrollBar.Value)
dbg.Leave "SetScrollbar"
End Sub

Sub SetStartAndStopToLast(DrawRect As GraphDrawRectType, ByRef DrawInfo As GraphDrawInfoType, lngArraySize As Long)
Debug.Print "SetStartAndStopToLast"
Dim lngNbrOfDataPoints As Long

  lngNbrOfDataPoints = GetNbrOfDataPoints(DrawRect, DrawInfo, lngArraySize)
  If lngArraySize > lngNbrOfDataPoints Then
    DrawInfo.Start = lngArraySize - lngNbrOfDataPoints + 1
    DrawInfo.Stop = lngArraySize
  Else
    DrawInfo.Start = 1
    DrawInfo.Stop = lngArraySize
  End If
End Sub

Sub SetStartAndStopToSameEnd(DrawRect As GraphDrawRectType, ByRef DrawInfo As GraphDrawInfoType, lngArraySize As Long)
Debug.Print "SetStartAndStopToSameEnd"
Dim lngNbrOfDataPoints As Long

  lngNbrOfDataPoints = GetNbrOfDataPoints(DrawRect, DrawInfo, lngArraySize)
  If DrawInfo.Stop - lngNbrOfDataPoints + 1 > 1 Then
    DrawInfo.Start = DrawInfo.Stop - lngNbrOfDataPoints + 1
  Else
    DrawInfo.Start = 1
    DrawInfo.Stop = lngArraySize
  End If
End Sub

Function SetStartAndStop(DrawRect As GraphDrawRectType, ByRef DrawInfo As GraphDrawInfoType, lngArraySize As Long) As Long
Debug.Print "SetStartAndStop"
Dim lngNbrOfDataPoints As Long

  lngNbrOfDataPoints = GetNbrOfDataPoints(DrawRect, DrawInfo, lngArraySize)
  If DrawInfo.Start + lngNbrOfDataPoints - 1 > lngArraySize Then
    DrawInfo.Start = lngArraySize - lngNbrOfDataPoints + 1
    DrawInfo.Stop = lngArraySize
  Else
    DrawInfo.Stop = DrawInfo.Start + lngNbrOfDataPoints - 1
  End If
  
End Function


Private Function GetNbrOfDataPoints(DrawRect As GraphDrawRectType, DrawInfo As GraphDrawInfoType, lngSize As Long) As Long
    
Dim NbrOfData  As Long
Dim NbrOfDataPoints  As Long
Dim lngDrawWidth As Long
Dim lngStart As Long
  lngDrawWidth = DrawRect.Width - DrawRect.left - RightDrawDist - LeftDrawDist
  lngStart = DrawInfo.Start
  NbrOfDataPoints = lngDrawWidth / DrawInfo.DaySpace

  If lngStart + NbrOfDataPoints - 1 > lngSize Then
    Do While (lngStart > 1) And (lngStart + NbrOfDataPoints - 1 > lngSize)
      lngStart = lngStart - 1
    Loop
  End If

  
  NbrOfData = NbrOfDataPoints
  
  If NbrOfData > lngSize Then NbrOfData = lngSize
  GetNbrOfDataPoints = NbrOfData
End Function
Sub InitRoundArray()
  ReDim RoundArray(1 To 45)
  RoundArray(1).sngScale = 0.001
  RoundArray(1).lngSmallStep = 5
  RoundArray(2).sngScale = 0.002
  RoundArray(2).lngSmallStep = 4
  RoundArray(3).sngScale = 0.0025
  RoundArray(3).lngSmallStep = 5
  RoundArray(4).sngScale = 0.005
  RoundArray(4).lngSmallStep = 5
  RoundArray(5).sngScale = 0.01
  RoundArray(5).lngSmallStep = 5
  RoundArray(6).sngScale = 0.02
  RoundArray(6).lngSmallStep = 4
  RoundArray(7).sngScale = 0.025
  RoundArray(7).lngSmallStep = 5
  RoundArray(8).sngScale = 0.05
  RoundArray(8).lngSmallStep = 5
  RoundArray(9).sngScale = 0.1
  RoundArray(9).lngSmallStep = 5
  RoundArray(10).sngScale = 0.2
  RoundArray(10).lngSmallStep = 4
  RoundArray(11).sngScale = 0.25
  RoundArray(11).lngSmallStep = 5
  RoundArray(12).sngScale = 0.5
  RoundArray(12).lngSmallStep = 5
  RoundArray(13).sngScale = 1
  RoundArray(13).lngSmallStep = 5
  RoundArray(14).sngScale = 2
  RoundArray(14).lngSmallStep = 4
  RoundArray(15).sngScale = 2.5
  RoundArray(15).lngSmallStep = 5
  RoundArray(16).sngScale = 5
  RoundArray(16).lngSmallStep = 5
  RoundArray(17).sngScale = 10
  RoundArray(17).lngSmallStep = 5
  RoundArray(18).sngScale = 20
  RoundArray(18).lngSmallStep = 4
  RoundArray(19).sngScale = 25
  RoundArray(19).lngSmallStep = 5
  RoundArray(20).sngScale = 50
  RoundArray(20).lngSmallStep = 5
  RoundArray(21).sngScale = 100
  RoundArray(21).lngSmallStep = 5
  RoundArray(22).sngScale = 200
  RoundArray(22).lngSmallStep = 4
  RoundArray(23).sngScale = 250
  RoundArray(23).lngSmallStep = 5
  RoundArray(24).sngScale = 500
  RoundArray(24).lngSmallStep = 5
  RoundArray(25).sngScale = 1000
  RoundArray(25).lngSmallStep = 5
  RoundArray(26).sngScale = 2000
  RoundArray(26).lngSmallStep = 4
  RoundArray(27).sngScale = 2500
  RoundArray(27).lngSmallStep = 5
  RoundArray(28).sngScale = 5000
  RoundArray(28).lngSmallStep = 5
  RoundArray(29).sngScale = 10000
  RoundArray(29).lngSmallStep = 5
  RoundArray(30).sngScale = 20000
  RoundArray(30).lngSmallStep = 4
  RoundArray(31).sngScale = 25000
  RoundArray(31).lngSmallStep = 5
  RoundArray(32).sngScale = 50000
  RoundArray(32).lngSmallStep = 5
  RoundArray(33).sngScale = 100000
  RoundArray(33).lngSmallStep = 5
  RoundArray(34).sngScale = 200000
  RoundArray(34).lngSmallStep = 4
  RoundArray(35).sngScale = 250000
  RoundArray(35).lngSmallStep = 5
  RoundArray(36).sngScale = 500000
  RoundArray(36).lngSmallStep = 5
  RoundArray(37).sngScale = 1000000
  RoundArray(37).lngSmallStep = 5
  RoundArray(38).sngScale = 2000000
  RoundArray(38).lngSmallStep = 4
  RoundArray(39).sngScale = 2500000
  RoundArray(39).lngSmallStep = 5
  RoundArray(40).sngScale = 5000000
  RoundArray(40).lngSmallStep = 5
  RoundArray(41).sngScale = 10000000
  RoundArray(41).lngSmallStep = 5
  RoundArray(42).sngScale = 20000000
  RoundArray(42).lngSmallStep = 4
  RoundArray(43).sngScale = 25000000
  RoundArray(43).lngSmallStep = 5
  RoundArray(44).sngScale = 50000000
  RoundArray(44).lngSmallStep = 5
  RoundArray(45).sngScale = 100000000
  RoundArray(45).lngSmallStep = 5

End Sub

Sub GetScale(ByRef Min As Single, ByRef Max As Single, lngPreferredSteps As Long, _
             ByRef sngNewMin As Single, sngScaleStep As Single, _
             ByRef lngNbrOfSteps As Long, ByRef lngNbrOfSmallSteps)




Dim Diff As Single
Dim j As Long
Dim I As Long
Dim Found As Boolean
Dim NbrOfSteps As Long
Dim lngNbrOfStepIndex As Long

lngNbrOfStepIndex = UBound(RoundArray)


Diff = Max - Min

'Hitta n�rmsta
Found = False
j = 1
Do While (j < lngNbrOfStepIndex) And Not Found
  If ((Diff / lngPreferredSteps) >= RoundArray(j).sngScale) And ((Diff / lngPreferredSteps) < RoundArray(j + 1).sngScale) Then
    Found = True
  End If
  j = j + 1
Loop

Found = False
I = 0
Do While Not Found
  If ((I * RoundArray(j).sngScale < Min) Or Min = 0) And ((I + 1) * RoundArray(j).sngScale >= Min) Then
    Found = True
  Else
    I = I + 1
  End If
Loop

lngNbrOfSteps = 1
Do While (I + lngNbrOfSteps) * RoundArray(j).sngScale < Max
  lngNbrOfSteps = lngNbrOfSteps + 1
Loop


sngNewMin = I * RoundArray(j).sngScale
sngScaleStep = RoundArray(j).sngScale
lngNbrOfSmallSteps = RoundArray(j).lngSmallStep

End Sub

#If DBUG = 1 Then
Sub GraphDrawRect(TheForm As Form, DrawRect As GraphDrawRectType)
  TheForm.DrawStyle = vbDot
  TheForm.ForeColor = RGB(200, 200, 200)
  TheForm.DrawWidth = 1
  With DrawRect
  TheForm.Line (.left, .bottom)-(.left + .Width, .bottom)
  TheForm.Line (.left, .bottom)-(.left, .bottom - .Height)
  TheForm.Line (.left, .bottom - .Height)-(.left + .Width, .bottom - .Height)
  TheForm.Line (.left + .Width, .bottom)-(.left + .Width, .bottom - .Height)
  End With
End Sub
#End If


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GraphFormMouseDown
'Input:
'Output:
'
'Description:   Handles the mouse down event.
'Author:        Fredrik Wendel
'Revision:      980503  Created
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub GraphFormMouseDown(GraphForm As Form, ByRef Button As Integer, ByRef Shift As Integer, ByRef x As Single, ByRef y As Single)
  Dim strText As String
  Dim lngTrendlineNbr As Long
  Dim LabelHandle As Long
  
  ' Handle draw text mode
  If TLDrawMode = dmText And Button = vbLeftButton And ValidCoordinate(x, y) Then
    strText = InputBox("Skriv text")
    If strText <> "" Then
      With Frm_OwnGraph
        If ActiveStockLabelsHandle <> Empty Then
            
            'MsgBox "X=" & X & ", Y=" & Y

            LabelHandle = AddStockLabel(Frm_OwnGraph.hdc, _
                          ActiveStockLabelsHandle, _
                          GetGraphDate(x), _
                          GetGraphPrice(y), _
                          strText, _
                          DrawSettings.LineColor)
                          
            'DrawStockLabel Frm_OwnGraph.hdc, _
                           ActiveStockLabelsHandle, _
                           LabelHandle, _
                           X, Y
            RefreshStockLabels Frm_OwnGraph.hdc, _
                               ActiveStockLabelsHandle

            Frm_OwnGraph.Refresh

        Else
            'Should never reach this point
            MsgBox CheckLang("Misslyckades l�gga till text")
        End If

      End With
    End If
  End If
  
  ' Handle move of text labels
  If TLDrawMode = dmMove And Button = vbLeftButton Then
    LabelHandle = GetStockLabel(Frm_OwnGraph.hdc, ActiveStockLabelsHandle, x, y)
    If LabelHandle <> 0 Then
      ' Start tracking of movement
      TrackLabelMove = True
      TrackLabelHandle = LabelHandle
      GetStockLabelRect Frm_OwnGraph.hdc, ActiveStockLabelsHandle, LabelHandle, TrackLabelOriginRect
      ' Calc offset to grabbed point from actual text-position
      TrackLabelOffsetX = TrackLabelOriginRect.left - x
      TrackLabelOffsetY = TrackLabelOriginRect.top - y
    Else
      ' Don't do any movement tracking
      TrackLabelMove = False
      TrackLabelHandle = Empty
    End If
  End If

  
  
  
  If TLDrawMode = dmDelete And Button = vbLeftButton Then
    'Bj�rn Hackar
    'Vi vill kunna ta bort ett helt Z-diagram �t g�ngen
    Dim j As Long
    Dim k As Long
    Dim l As Long
    j = 0
    Do While j <= TrendlineSerieSize - 1
    For k = 0 To ZIAPointer - 1
      If j = ZIndexArray(k) Then
        If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
          lngTrendlineNbr = TrendlineHit(GraphForm, x, y)
          For l = (lngTrendlineNbr - 1) To (lngTrendlineNbr + 4)
            If l <> -1 Then
              GraphForm.TrendLine(l).Visible = False
              TrendLineArray(GraphSettings.DWM, l + 1).InUse = False
            End If
          Next l
          Exit Do
        End If
      End If
    Next k
    j = j + 1
    Loop
    'Slut hack
    
    LabelHandle = GetStockLabel(Frm_OwnGraph.hdc, ActiveStockLabelsHandle, x, y)
    If LabelHandle <> 0 Then
      DeleteStockLabel ActiveStockLabelsHandle, LabelHandle
      UpdateGraph ActiveIndex
    End If
    ' Handle delete trendline
    lngTrendlineNbr = TrendlineHit(GraphForm, x, y)
    If lngTrendlineNbr <> -1 Then
      GraphForm.TrendLine(lngTrendlineNbr).Visible = False
      TrendLineArray(GraphSettings.DWM, lngTrendlineNbr + 1).InUse = False
    End If
  End If
  

  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GraphFormMouseUp
'Input:
'Output:
'
'Description:   Handles the mouse up event.
'Author:        Fredrik Wendel
'Revision:      980503  Created
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub GraphFormMouseUp(GraphForm As Form, ByRef Button As Integer, ByRef Shift As Integer, ByRef x As Single, ByRef y As Single)
If PRO = True Then
  If GraphSettings.blnShowFish And GraphSettings.blnShowHighlight Then
    DrawHighlight GraphForm, Button, Shift, x, y
  End If
End If

' Handle drop of label

  If TrackLabelMove Then
      
    If ValidCoordinate(x + TrackLabelOffsetX, y + TrackLabelOffsetY) Then
      
      'Move label to new position
      MoveStockLabel ActiveStockLabelsHandle, _
                     TrackLabelHandle, _
                     GetGraphDate(x + TrackLabelOffsetX), _
                     GetGraphPrice(y + TrackLabelOffsetY)

      UpdateGraph ActiveIndex

    Else
      
      'MoveStockLabel ActiveStockLabelsHandle, _
      '               TrackLabelHandle, _
      '               TrackLabelOriginRect.left, _
      '               TrackLabelOriginRect.top
                     
      Frm_OwnGraph.Refresh 'return to old gfx
    
    End If
    
    TrackLabelMove = False
    TrackLabelHandle = Empty
    TrackLabelDrawnX = Empty
    TrackLabelDrawnY = Empty

  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GraphFormMouseMove
'Input:
'Output:
'
'Description:   Handles the mouse move event.
'Author:        Fredrik Wendel
'Revision:      980503  Created
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub GraphFormMouseMove(ByRef GraphForm As Form, ByRef Button As Integer, ByRef Shift As Integer, ByRef x As Single, ByRef y As Single)
  Dim lngTrendlineNbr As Long
  Dim j As Long
  Dim LabelHandle As Long
  If OnClose(GraphForm, x, y) Then
If PRO = True Then
  If GraphSettings.blnShowFish Then
    'DrawHighlight GraphForm, Button, Shift, X, Y
  End If
End If
  End If
  
  
  If TLDrawMode = dmDelete Then
    lngTrendlineNbr = TrendlineHit(GraphForm, x, y)
    If lngTrendlineNbr <> -1 Then
      GraphForm.TrendLine(lngTrendlineNbr).BorderStyle = vbBSDash
    Else
      For j = 0 To TrendlineSerieSize - 1
        If GraphForm.TrendLine(j).Visible = True Then
          GraphForm.TrendLine(j).BorderStyle = vbBSSolid
        End If
      Next j
    End If
  End If
  
  ' Update the price data on the statusbar
  UpdateStatusBarPriceData x, y, GraphSettings.ExponentialPrice
  
    ' Handle labels
  
  If TLDrawMode = dmText Then
    
    If ValidCoordinate(x, y) Then
      MainMDI.MousePointer = MousePointerConstants.vbCrosshair
    Else
      MainMDI.MousePointer = MousePointerConstants.vbDefault
    End If
  
  ElseIf TLDrawMode = dmMove Then
    
    If Not TrackLabelMove Then
      
      LabelHandle = GetStockLabel(Frm_OwnGraph.hdc, ActiveStockLabelsHandle, x, y)
      If LabelHandle <> 0 Then
        MainMDI.MousePointer = MousePointerConstants.vbSizeAll 'vbSizeAll looks like Move cursor
      Else
        MainMDI.MousePointer = MousePointerConstants.vbDefault
      End If
    
    ElseIf TrackLabelMove Then
      
      Dim LabelRect As RECT
      
      If ValidCoordinate(x, y) Then
        MainMDI.MousePointer = MousePointerConstants.vbSizeAll 'vbSizeAll looks like Move cursor
      Else
        MainMDI.MousePointer = MousePointerConstants.vbNoDrop
      End If

      Dim Width As Long
      Dim Height As Long
      
      If TrackLabelDrawnX <> Empty Then
        GetStockLabelRect Frm_OwnGraph.hdc, ActiveStockLabelsHandle, TrackLabelHandle, LabelRect
        Width = LabelRect.right - LabelRect.left
        Height = LabelRect.bottom - LabelRect.top
        LabelRect.left = TrackLabelDrawnX
        LabelRect.top = TrackLabelDrawnY
        LabelRect.bottom = LabelRect.top + Height
        LabelRect.right = LabelRect.left + Width
        InvalidateRect Frm_OwnGraph.hWnd, LabelRect, True
      End If
      
      Frm_OwnGraph.AutoRedraw = False

      DrawStockLabel Frm_OwnGraph.hdc, _
                     ActiveStockLabelsHandle, _
                     TrackLabelHandle, _
                     x + TrackLabelOffsetX, _
                     y + TrackLabelOffsetY
                     
      TrackLabelDrawnX = x + TrackLabelOffsetX
      TrackLabelDrawnY = y + TrackLabelOffsetY

      Frm_OwnGraph.AutoRedraw = True
    End If
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ValidCoordinate
'Input:
'Output:
'
'Description:   Returns TRUE if the argument is within the chart area
'Author:        Fredrik Wendel
'Revision:      980331  Created
'               991018  Removed TL...
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function ValidCoordinate(x As Single, y As Single) As Boolean
  'ValidCoordinate = (X >= LeftDrawTL And X <= RightDrawTL And Y <= BottomDrawTL And Y >= TopDrawTL)
  With mudtPriceGraphCopy
  ValidCoordinate = (x >= .left And x <= .right And y <= .bottom And y >= .top)
  End With
End Function

Function OnClose(GraphForm As Form, ByRef x As Single, ByRef y As Single) As Boolean
  
  Dim lngIndex As Long
  Dim lngCloseYCoord As Long
  Dim lngCloseXCoord As Long
  Dim blnOnClose As Boolean
  
  
  
  
  
  blnOnClose = False
  With mudtPriceGraphCopy
  If ValidCoordinate(x, y) Then
    
    
  End If
  End With
  OnClose = True
End Function

Sub UpdateStatusBarPriceData(x As Single, y As Single, IsExponential As Boolean)

Dim lngIndex As Long
With mudtPriceGraphCopy
If ValidCoordinate(x, y) Then
  lngIndex = .Start + ((x - .left) \ .DaySpace)
  If IsExponential Then
    MainMDI.StatusBar.Panels.Item(5).Text = _
    CDate(Int(mudtDrawArray(lngIndex).Date)) & _
    " H=" & FormatNumber(Exp(mudtDrawArray(lngIndex).High), 2) & _
    " L=" & FormatNumber(Exp(mudtDrawArray(lngIndex).Low), 2) & _
    " C=" & FormatNumber(Exp(mudtDrawArray(lngIndex).Close), 2)
  Else
    MainMDI.StatusBar.Panels.Item(5).Text = CDate(Int(mudtDrawArray(lngIndex).Date)) & " H=" & mudtDrawArray(lngIndex).High & " L=" & mudtDrawArray(lngIndex).Low & " C=" & mudtDrawArray(lngIndex).Close
  End If
Else
  MainMDI.StatusBar.Panels.Item(5).Text = ""
End If
End With
End Sub

'#If FISH = 1 Then
Sub DrawHighlight(ByRef GraphForm As Form, ByRef Button As Integer, ByRef Shift As Integer, ByRef x As Single, ByRef y As Single)
Dim lngIndex As Long
Dim IncMav() As Single
Dim j As Long
Dim aHighlight As HighlightType

Dim Status As ToggleHighlightEnum
Static lngLastIndex  As Long
 
  'dbg.Enter "DrawHighLight"
  
  With mudtPriceGraphCopy
  If ValidCoordinate(x, y) Then
    lngIndex = .Start + (x - .left) \ .DaySpace
    If lngIndex <> lngLastIndex Then
      aHighlight.dtmStart = mudtDrawArray(lngIndex).Date
      Status = ToggleHighlight(aHighlight)
      Select Case Status
      Case theAdded
        SimpleMAVIncrementalCalc mudtDrawArray, IncMav, lngIndex
       
        GraphForm.ForeColor = RGB(255, 255, 255)
        GraphForm.CurrentY = Int(.bottom - (IncMav(lngIndex) - .Min) * .YScale)
        GraphForm.CurrentX = .left + (lngIndex - .Start) * .DaySpace
        For j = 1 To .Stop - lngIndex
          GraphForm.Line Step(0, 0)-(.left + (lngIndex - .Start) * .DaySpace + j * .DaySpace, Int(.bottom - (IncMav(lngIndex + j) - .Min) * .YScale))
        Next j
      
        GraphForm.CurrentX = .DrawRect.left + .DrawRect.Width - RightLabelDist
        GraphForm.CurrentY = GraphForm.CurrentY - 5
      
        GraphForm.FontBold = False
        GraphForm.Print FormatNumber(IncMav(.Stop), 2)
        GraphForm.FontBold = False
        lngLastIndex = lngIndex
        
      Case theFull
        ErrorMessageStr "Det g�r inte att rita fler strukturer."
      End Select
    End If
  End If
  End With
  'dbg.Leave "DrawHighlight"
End Sub
'#End If
#If 1 = 0 Then
Sub DrawHighligts()
  Dim Highlights() As HighlightType
  Dim I As Long
  
  GetHighlights Highlights
  For I = 1 To UBound(Highlights)
    GetDrawIndex (Highlights(I).dtmStart)
    SimpleMAVIncrementalCalc mudtDrawArray, IncMav, lngIndex
    
        GraphForm.ForeColor = RGB(255, 255, 255)
        GraphForm.CurrentY = Int(.bottom - (IncMav(lngIndex) - .Min) * .YScale)
        GraphForm.CurrentX = .left + (lngIndex - .Start) * .DaySpace
        For j = 1 To .Stop - lngIndex
          GraphForm.Line Step(0, 0)-(.left + (lngIndex - .Start) * .DaySpace + j * .DaySpace, Int(.bottom - (IncMav(lngIndex + j) - .Min) * .YScale))
        Next j
      
        GraphForm.CurrentX = .DrawRect.left + .DrawRect.Width - RightLabelDist
        GraphForm.CurrentY = GraphForm.CurrentY - 5
      
        GraphForm.FontBold = False
        GraphForm.Print FormatNumber(IncMav(.Stop), 2)
        GraphForm.FontBold = False
  Next I
End Sub
#End If

Function GetDrawIndex(theDate As Date) As Long
Dim I As Long
Dim Found As Boolean


Found = False
I = mudtPriceGraphCopy.Start
Do While Not Found And I <= mudtPriceGraphCopy.Start
  If mudtDrawArray(I).Date = theDate Then
    Found = True
  Else
    I = I + 1
  End If
Loop
GetDrawIndex = I
End Function

Function TrendlineHit(ByRef frmForm As Form, x As Single, y As Single) As Long

Dim blnFound As Boolean
Dim j As Long
  blnFound = False
  For j = 0 To TrendlineSerieSize - 1
    With frmForm.TrendLine(j)
    If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
      If (Distance(.X1, .Y1, x, y) + Distance(.X2, .Y2, x, y) - Distance(.X1, .Y1, .X2, .Y2) < 0.01) Then
        blnFound = True
        TrendlineHit = j
      End If
    End If
    End With
  Next j
  If Not blnFound Then
    TrendlineHit = -1
  End If
  
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetGraphPrice
'Input:         Y   : The y coordinate
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      991018  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetGraphPrice(y As Single) As Single

  With mudtPriceGraphCopy
  If mudtPriceGraphCopy.blnExponential Then
    GetGraphPrice = Exp(.Min + (.bottom - y) / .YScale)
  Else
    GetGraphPrice = .Min + (.bottom - y) / .YScale
  End If
  End With
End Function
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetGraphY
'Input:         Price   : The price
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      991018  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetGraphY(Price As Single) As Single
  With mudtPriceGraphCopy
  If mudtPriceGraphCopy.blnExponential Then
    GetGraphY = .bottom - CLng((Log(Price) - .Min) * .YScale)
  Else
    GetGraphY = .bottom - CLng((Price - .Min) * .YScale)
  End If
  End With
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetGraphDate
'Input:         X   : The x coordinate
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      991018  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetGraphDate(x As Single) As Date
  With mudtPriceGraphCopy
  GetGraphDate = mudtDrawArray(.Start + ((x - .left) \ .DaySpace)).Date
  End With
End Function

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetGraphX
'Input:         dtmDate   : The date
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      991018  Created
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function GetGraphX(dtmDate As Date) As Single
  With mudtPriceGraphCopy
    GetGraphX = .left + GetNbrOfDaysBetween(mudtDrawArray(.Start).Date, dtmDate) * .DaySpace
  End With
End Function


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetNbrOfDaysBetween
'Input:
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      XXXXXX  Created.
'               980812  Totally rewritten due to problem with
'                       Date type including time inforamtion.
'               991018  Now uses mudtDrawArray
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''


Function GetNbrOfDaysBetween(DateA As Date, DateB As Date) As Long
Dim Days As Long
Dim I As Long
Dim j As Long
Dim Found As Boolean
Dim nDate As Long
Dim nDateA As Long
Dim nDateB As Long


nDateA = Int(DateA) 'Ta den del av Date som �r datum dvs heltalsdelen.
nDateB = Int(DateB) 'Anv�ndning av heltal snabbar upp j�mf�relsen.

nDate = 0
j = 0
Found = False

Do While nDate <> nDateA And j < UBound(mudtDrawArray)
  j = j + 1
  nDate = Int(mudtDrawArray(j).Date)
Loop

nDate = 0
I = 0
Found = False
Do While nDate <> nDateB And I < UBound(mudtDrawArray)
  I = I + 1
  nDate = Int(mudtDrawArray(I).Date)
Loop
GetNbrOfDaysBetween = I - j

End Function

Function GetFirstDrawDate() As Date
  GetFirstDrawDate = mudtDrawArray(mudtPriceGraphCopy.Start).Date
End Function

Sub UpdateLabelsScaling()
  SetStockLabelsScaling ActiveStockLabelsHandle, _
                        mudtDrawArray(0), _
                        UBound(mudtDrawArray) + 1, _
                        mudtPriceGraphCopy
End Sub


Sub MakePriceVolumeArray(ByRef PDArray() As PriceDataType, _
                         lngStart As Long, _
                         lngStop As Long, _
                         sngNewMin As Single, _
                         sngScaleStep As Single, _
                         lngNbrOfSteps As Long, _
                         lngNbrOfSmallSteps As Long, _
                         PriceVolume() As Single, _
                         Succeded As Boolean)
  Dim Step As Long
  Dim I As Long
  Dim SmallStepPrice As Single
  Dim LowPrice  As Single
  Dim HighPrice As Single
  
  Succeded = False
  ReDim PriceVolume(0 To lngNbrOfSteps * lngNbrOfSmallSteps - 1)
  SmallStepPrice = sngScaleStep / lngNbrOfSmallSteps
  
  For Step = 0 To lngNbrOfSteps * lngNbrOfSmallSteps - 1
    LowPrice = sngNewMin + SmallStepPrice * Step
    HighPrice = sngNewMin + SmallStepPrice * (Step + 1)
    Debug.Print "Testar >= " & CStr(LowPrice) & " < " & CStr(HighPrice);
    For I = lngStart To lngStop
      With PDArray(I)
      If .Close >= LowPrice And _
          .Close < HighPrice Then
          PriceVolume(Step) = PriceVolume(Step) + .Volume
      End If
      End With
    Next I
    If PriceVolume(Step) > 0 Then Succeded = True
    Debug.Print " Volume: " & CStr(PriceVolume(Step))
  Next Step
#If DBUG = 1 Then
  Dim totvol As Single
  For I = 0 To lngNbrOfSteps * lngNbrOfSmallSteps - 1
    totvol = totvol + PriceVolume(I)
  Next I
  Debug.Print totvol
  totvol = 0
  For I = lngStart To lngStop
    totvol = totvol + PDArray(I).Volume
  Next I
  Debug.Print totvol
#End If
End Sub

Function GetGraphStartIndex() As Long
  GetGraphStartIndex = mudtPriceGraphCopy.Start
End Function

Function GetGraphStopIndex() As Long
  GetGraphStopIndex = mudtPriceGraphCopy.Stop
End Function
