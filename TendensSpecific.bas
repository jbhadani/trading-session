Attribute VB_Name = "TendensSpecific"
Option Explicit

#If TENDENS = 1 Then
Declare Function GetTempPath Lib "kernel32" Alias "GetTempPathA" (ByVal nBufferLength As Long, ByVal lpBuffer As String) As Long
Public Const gintMAX_SIZE% = 255

Public Declare Function OpenProcess Lib "kernel32" _
    (ByVal dwDesiredAccess As Long, _
     ByVal bInheritHandle As Long, _
     ByVal dwProcessId As Long) As Long
Public Declare Function GetExitCodeProcess Lib "kernel32" _
    (ByVal hProcess As Long, lpExitCode As Long) As Long
Public Declare Function CloseHandle Lib "kernel32" _
    (ByVal hObject As Long) As Long
Public Const PROCESS_QUERY_INFORMATION = &H400
Public Const STATUS_PENDING = &H103&  'new
Public Const strBMPPath As String = "d:\FrontSoft\Bmp\"

Public Function RunShell(cmdline As String) As String



  Dim hProcess As Long
  Dim ProcessId As Long
  Dim exitCode As Long
  On Error Resume Next
  ProcessId = Shell(cmdline, vbMinimizedNoFocus)
  RunShell = Trim(Err.Description)
  Err.Clear
  On Error GoTo 0
  hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, False, ProcessId)
  Do
    Call GetExitCodeProcess(hProcess, exitCode)
    DoEvents
  Loop While exitCode = STATUS_PENDING  'new
  Call CloseHandle(hProcess)
End Function

Sub SaveGraph()
  
  Dim I As Long
  
  Dim strBuf As String
  
  
  
  
  ' Get windows sytstem directory
  strBuf = Space$(gintMAX_SIZE)
  If GetTempPath(gintMAX_SIZE, strBuf) > 0 Then
    strBuf = Trim(strBuf)
    strBuf = Mid(strBuf, 1, Len(strBuf) - 1)
    strBuf = strBuf
  Else
    MsgBox CheckLang("Kunde inte finna temp mappen")
  End If
  FileCopy strBMPPath & "dither.exe", strBuf & "\dither.exe"
  FileCopy strBMPPath & "bmp2gif.exe", strBuf & "\bmp2gif.exe"
  
  
 
  
  If PortfolioArraySize > 0 Then
  Frm_Progress.PB_1.Value = 0
  Frm_Progress.PB_1.Min = 0
  Frm_Progress.PB_1.Max = PortfolioArraySize
  Frm_Progress.Label1.Caption = "Sparar bilder."
  
  Frm_Progress.Show
  For I = 1 To PortfolioArraySize
    Frm_Progress.PB_1.Value = I
    MainMDI.ID_Object.ListIndex = GetInvestIndex(PortfolioArray(I).Name) - 1
    Debug.Print "Creating " & MainMDI.ID_Object.Text & " " & PortfolioArray(I).Name & " " & CStr(GetInvestIndex(PortfolioArray(I).Name))
    ActionObjectChange
    Frm_OwnGraph.Width = 640
    Frm_OwnGraph.Height = 480
    
    SavePicture Frm_OwnGraph.Image, strBuf + DeleteTokens(PortfolioArray(I).Name, 32) & ".bmp"
    
    'Debug.Print strBuf & "dither " & DeleteTokens(PortfolioArray(i).Name, 32) & ".bmp " & strBuf & DeleteTokens(PortfolioArray(i).Name, 32) & "_d.bmp"
  Next I
  Frm_Progress.Label1.Caption = "Konverterar bilder."
  
  For I = 1 To PortfolioArraySize
    Frm_Progress.PB_1.Value = I
    RunShell strBuf & "bmpcut " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & ".bmp " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & "_c.bmp 0 0 640 480"
    RunShell strBuf & "dither " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & ".bmp " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & "_d.bmp"
    RunShell strBuf & "bmp2gif " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & "_d.bmp " & strBuf & DeleteTokens(PortfolioArray(I).Name, 32) & ".gif"
  Next I
  Unload Frm_Progress
  'Set picGraph = Frm_OwnGraph.Picture
  'Set picGraph = Frm_OwnGraph.Picture

  Else
    ErrorMessageStr "Det finns ingen portf�lj �ppnad."
  End If
  InformationalMessage "Gifs and bmps are written to " & strBuf
  Debug.Print "Picture written to " & strBuf
End Sub



Function DeleteTokens(strIn As String, token As Byte) As String
Dim strTemp As String
Dim I As Long
strTemp = ""
For I = 1 To Len(Trim(strIn))
  If Asc(Mid(strIn, I, 1)) <> token Then
    strTemp = strTemp & Mid(strIn, I, 1)
  End If
Next I
DeleteTokens = strTemp
End Function


#End If
