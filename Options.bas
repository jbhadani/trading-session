Attribute VB_Name = "Options"
Global Lang As Byte

Public Sub CheckIfAllNecessaryData(AllDataOk As Boolean)
  
  With frmOptionsInput
  If .txtInterestRate(0).Text <> "" And .txtObjectPrice(0).Text <> "" And .txtStrikePrice(0).Text <> "" Then
    If .txtOptionPrice(0).Text <> "" Or .txtVolatility(0).Text <> "" And Int(.DTPicker2.Value) - Int(.DTPicker1.Value) > 0 Then
      AllDataOk = True
    Else
      AllDataOk = False
    End If
  Else
    AllDataOk = False
  End If
  End With
  
End Sub

Public Function CheckLang(word As String, Optional b As Byte = 0)
Dim src As String
Dim I As Long
'list2- english
'list3- swedish
b = Lang
For I = 1 To 228
Dim E As String
Dim S As String
Dim wordbuf As String
E = Mid(LoadResString(I), 1, InStr(1, UCase(LoadResString(I)), "--") - 1)
S = Mid(LoadResString(I), Len(E) + 3)
If b = 2 Then           '   swedish to english
    If Trim(UCase(S)) = Trim(UCase(word)) Then
        src = E
        Exit For
    End If
ElseIf b = 1 Then                  '   english to swedish
    If Trim(UCase(E)) = Trim(UCase(word)) Then
        src = S
        Exit For
    End If
End If
Next
If src = "" Then
    CheckLang = word
Else
    CheckLang = src
End If
End Function

Public Sub CalcOptionValue()
  Dim P As Single 'Option value
  Dim v As Single 'Volatility
  Dim G As Single 'Price of underlying
  Dim E As Single 'Strike price
  Dim r As Single 'Interest rate
  Dim t1 As Long   'Days to expiration
  Dim t As Single 'Time remaining in years
  Dim d1 As Single
  Dim d2 As Single
  Dim Nd1 As Single
  Dim Nd2 As Single
  Dim PutOrCall As String
  Dim ObjectName As String
  Dim OptionName As String
  Dim cd As Date
  Dim ed As Date
  Dim d As Single
  Dim Delta As Single
  Dim Gamma As Single
  Dim Theta As Single
  Dim Vega As Single

  Const pi = 3.141592654

  With frmOptionsInput
    t1 = Int(.DTPicker2.Value) - Int(.DTPicker1.Value)
    G = CSng(PointToComma(.txtObjectPrice(0).Text))
    E = CSng(PointToComma(.txtStrikePrice(0).Text))
    r = CSng(PointToComma(.txtInterestRate(0).Text)) / 100
    v = CSng(PointToComma(.txtVolatility(0).Text)) / 100
    t = t1 / 360
    PutOrCall = .Combo1.Text
    ObjectName = .txtObjectName(0).Text
    OptionName = .txtOptionName(0).Text
    cd = .DTPicker1.Value
    ed = .DTPicker2.Value
    'd = .txtDividend(0).Text
    
    d1 = (Log(G / E) + ((r + 0.5 * v ^ 2) * t)) / (v * Sqr(t))
    d2 = d1 - v * Sqr(t)
    
    Nd1 = CalcNormalDistribution(d1)
    Nd2 = CalcNormalDistribution(d2)
    
  End With
  
  If StrComp(Trim(PutOrCall), "K�p") = 0 Then
    P = G * Nd1 - E * Exp(-1 * r * t) * Nd2
    Delta = Nd1
    Gamma = 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Exp((r * t)) / (G * v * Sqr(t))
    Theta = (-G * Exp(r * t) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * v * 1 / (2 * Sqr(t)) - r * G * Exp(r * t) * CalcNormalDistribution(d1) - r * E * Exp(-(r * t)) * CalcNormalDistribution(d2)) / 365
    Vega = G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Sqr(t) / 100
  Else
    P = E * Exp(-1 * r * t) * CalcNormalDistribution(-d2) - G * CalcNormalDistribution(-d1)
    Delta = Nd1 - 1
    Gamma = 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Exp(-(r * t)) / (G * v * Sqr(t))
    Theta = (-G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * v * 1 / (2 * Sqr(t)) + r * G * Exp(r * t) * CalcNormalDistribution(-d1) + r * E * Exp(-(r * t)) * CalcNormalDistribution(-d2)) / 365
    Vega = G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Sqr(t) / 100
  End If
  
  UpdateOptionsGrid ObjectName, OptionName, r, d, cd, ed, E, G, PutOrCall, P, v, Delta, Gamma, Theta, Vega, t1
  
End Sub

Public Sub CalcOptionVolatility()
  Dim P As Single 'Option value
  Dim v As Single 'Volatility
  Dim G As Single 'Price of underlying
  Dim E As Single 'Strike price
  Dim r As Single 'Interest rate
  Dim t1 As Long  'Days to expiration
  Dim t As Single 'Time remaining to expiration in years
  Dim d1 As Single
  Dim d2 As Single
  Dim Nd1 As Single
  Dim Nd2 As Single
  Dim UpperLimit As Single
  Dim LowerLimit As Single
  Dim ThisCalculation As Single
  Dim Pc As Single
  Dim Continue As Boolean
  Dim PutOrCall As String
  Dim ObjectName As String
  Dim OptionName As String
  Dim cd As Date
  Dim ed As Date
  Dim d As Single
  Dim Delta As Single
  Dim Gamma As Single
  Dim Theta As Single
  Dim Vega As Single
  
  Const pi = 3.141592654
  
  LowerLimit = 0.01
  UpperLimit = 0.99
  Continue = True
  
  With frmOptionsInput
    t1 = Int(.DTPicker2.Value) - Int(.DTPicker1.Value)
    G = CSng(PointToComma(.txtObjectPrice(0).Text))
    E = CSng(PointToComma(.txtStrikePrice(0).Text))
    r = CSng(PointToComma(.txtInterestRate(0).Text)) / 100
    P = CSng(PointToComma(.txtOptionPrice(0).Text))
    PutOrCall = .Combo1.Text
    ObjectName = .txtObjectName(0).Text
    OptionName = .txtOptionName(0).Text
    cd = .DTPicker1.Value
    ed = .DTPicker2.Value
    'd = .txtDividend(0).Text
  End With
  
  t = t1 / 360
  Do While Continue
    ThisCalculation = (UpperLimit + LowerLimit) / 2
    't = 10
    d1 = (Log(G / E) + ((r + 0.5 * ThisCalculation ^ 2) * t)) / (ThisCalculation * Sqr(t))
    d2 = d1 - ThisCalculation * Sqr(t)
    
    Nd1 = CalcNormalDistribution(d1)
    Nd2 = CalcNormalDistribution(d2)
    
    If StrComp(Trim(PutOrCall), "K�p") = 0 Then
      Pc = G * Nd1 - E * Exp(-1 * r * t) * Nd2
    Else
      Pc = E * Exp(-1 * r * t) * CalcNormalDistribution(-d2) - G * CalcNormalDistribution(-d1)
    End If
    
    If Abs(Pc - P) < 0.0001 Then
      Continue = False
    End If
    If Continue Then
      If Pc - P < 0 Then
        LowerLimit = ThisCalculation
      Else
        UpperLimit = ThisCalculation
      End If
    End If
  Loop
    
  v = ThisCalculation
  
  If StrComp(Trim(PutOrCall), "K�p") = 0 Then
    Delta = Nd1
    Gamma = 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Exp((r * t)) / (G * v * Sqr(t))
    Theta = (-G * Exp(r * t) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * v * 1 / (2 * Sqr(t)) - r * G * Exp(r * t) * CalcNormalDistribution(d1) - r * E * Exp(-(r * t)) * CalcNormalDistribution(d2)) / 365
    Vega = G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Sqr(t) / 100
  Else
    Delta = Nd1 - 1
    Gamma = 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Exp(-(r * t)) / (G * v * Sqr(t))
    Theta = (-G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * v * 1 / (2 * Sqr(t)) + r * G * Exp(r * t) * CalcNormalDistribution(-d1) + r * E * Exp(-(r * t)) * CalcNormalDistribution(-d2)) / 365
    Vega = G * Exp(-(r * t)) * 1 / Sqr(2 * pi) * Exp(-((d1 ^ 2) / 2)) * Sqr(t) / 100
  End If
  
  UpdateOptionsGrid ObjectName, OptionName, r, d, cd, ed, E, G, PutOrCall, P, v, Delta, Gamma, Theta, Vega, t1
  
End Sub

Function CalcNormalDistribution(d As Single) As Single
  Const a1 = 0.31938153
  Const a2 = -0.356563782
  Const a3 = 1.781477937
  Const a4 = -1.821255978
  Const a5 = 1.330274429
  Const pi = 3.141592654
  
  Dim k As Single
  Dim temp As Single
  
  k = 1 / (1 + 0.2316419 * d)
  temp = k * a1 + k ^ 2 * a2 + k * k * k * a3 + k * k * k * k * a4 + k * k * k * k * k * a5
  CalcNormalDistribution = 1 - Exp(-((d ^ 2) / 2)) * 1 / Sqr(2 * pi) * temp
  
End Function

Public Sub UpdateOptionsGrid(ObjectName As String, OptionName As String, InterestRate As Single, Dividend As Single, CalculationDate As Date, ExpirationDate As Date, StrikePrice As Single, ObjectPrice As Single, PutOrCall As String, OptionValue As Single, Volatility As Single, Delta As Single, Gamma As Single, Theta As Single, Lambda As Single, t1 As Long)
  Dim I As Integer
  
  With frmOptions.MSFlexGrid1
    I = .ColSel
    .Row = 1
    .CellAlignment = flexAlignCenterCenter
    .CellForeColor = QBColor(12)
    .TextMatrix(1, I) = Trim(ObjectName)
    .Row = 2
    .CellAlignment = flexAlignCenterCenter
    .CellForeColor = QBColor(12)
    .TextMatrix(2, I) = Trim(OptionName)
    .Row = 3
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(3, I) = FormatNumber(InterestRate * 100, 2)
    '.Row = 4
    '.CellAlignment = flexAlignCenterCenter
    '.TextMatrix(4, i) = FormatCurrency(Dividend, 2)
    .Row = 4
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(4, I) = CalculationDate
    .Row = 5
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(5, I) = ExpirationDate
    .Row = 6
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(6, I) = FormatNumber(StrikePrice, 2)
    .Row = 7
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(7, I) = FormatNumber(ObjectPrice, 2)
    .Row = 8
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(8, I) = Trim(PutOrCall)
    .Row = 10
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(10, I) = FormatNumber(OptionValue, 2)
    .Row = 11
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(11, I) = FormatNumber(Volatility * 100, 2)
    .Row = 12
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(12, I) = FormatNumber(Delta, 4)
    .Row = 13
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(13, I) = FormatNumber(Gamma, 4)
    .Row = 14
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(14, I) = FormatNumber(Theta, 4)
    .Row = 15
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(15, I) = FormatNumber(Lambda, 4)
    .Row = 16
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(16, I) = FormatNumber(t1, 0)
  End With
    
End Sub

