VERSION 5.00
Begin VB.Form frmChangeInvestment 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�ndra objekt"
   ClientHeight    =   3060
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4140
   Icon            =   "frmChangeInvestment.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3060
   ScaleWidth      =   4140
   Begin VB.Frame Frame1 
      Caption         =   "Namn"
      Height          =   1095
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   2415
      Begin VB.TextBox TB_Name 
         Height          =   285
         Left            =   240
         MaxLength       =   20
         TabIndex        =   0
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Symbol 1"
      Height          =   1095
      Left            =   2520
      TabIndex        =   9
      Top             =   0
      Width           =   1575
      Begin VB.TextBox TB_Symbol1 
         Height          =   285
         Left            =   240
         MaxLength       =   10
         TabIndex        =   2
         Top             =   480
         Width           =   1095
      End
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   2280
      Width           =   4095
      Begin VB.CommandButton CB_Ok 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton CB_Cancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2880
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Kategori"
      Height          =   1095
      Left            =   0
      TabIndex        =   7
      Top             =   1200
      Width           =   2415
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   240
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Symbol 2"
      Height          =   1095
      Left            =   2520
      TabIndex        =   6
      Top             =   1200
      Width           =   1575
      Begin VB.TextBox TB_Symbol2 
         Height          =   285
         Left            =   240
         MaxLength       =   10
         TabIndex        =   3
         Top             =   480
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmChangeInvestment"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim OldName As String * 20

Private Sub CB_Cancel_Click()
  Unload frmChangeInvestment
End Sub

Private Sub CB_Ok_Click()
  
  Dim InvestInfo As InvestInfoType
  Dim i As Integer
  Dim TypeOfInvestFound As Boolean
  Dim TempParameterSettings As ParameterSettingsType
  OldName = frmEditInvestment.List1.List(frmEditInvestment.List1.listindex)
  With InvestInfo
  .Name = frmChangeInvestment.TB_Name
  If ((Not ExistInInvestInfoArray(InvestInfo)) Or Trim(OldName) = Trim(.Name)) And (Trim(.Name) <> "") Then
    .FileName = Trim(.Name)
    .Name = frmChangeInvestment.TB_Name
    '.Symbol1 = frmChangeInvestment.TB_Symbol1
    '.Symbol2 = frmChangeInvestment.TB_Symbol2
    TypeOfInvestFound = False
    For i = 1 To CategoryArraySize
      If Trim(frmChangeInvestment.Combo1.Text) = Trim(CategoryArray(i)) Then
        TypeOfInvestFound = True
        .TypeOfInvest = i
      End If
    Next i
      If Not TypeOfInvestFound Then
        .TypeOfInvest = 0
      End If
    Name DataPath + Trim(InvestInfoArray(GetInvestIndex(OldName)).FileName) + ".prc" As DataPath + Trim(InvestInfo.FileName) + ".prc"
    
    'TempParameterSettings = InvestInfoArray(GetInvestIndex(OldName)).ParameterSettings
    DeleteInvestInfo GetInvestIndex(OldName)
    
    AddNewInvestInfo InvestInfo
    'InvestInfoArray(GetInvestIndex(InvestInfo.Name)).ParameterSettings = TempParameterSettings
    W_InvestInfo_All DataPath + "InvInfo.dat"
    InitInvestmentToolbar
    Unload frmChangeInvestment
    Unload frmEditInvestment
  Else
    ErrorMessageStr "Objektnamnet �r ogilitigt"
  End If
  End With
  
End Sub

Private Sub Form_Load()
  frmChangeInvestment.Left = (MainMDI.Width - frmChangeInvestment.Width) / 2
  frmChangeInvestment.Top = (MainMDI.Height - frmChangeInvestment.Height) / 4
Dim i As Integer
  Combo1.AddItem "Ingen"
  
For i = 1 To CategoryArraySize
  If Trim(CategoryArray(i)) <> "" Then
  Combo1.AddItem Trim(CategoryArray(i))
  End If
Next i
OldName = TB_Name.Text
End Sub
