VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_Parameters 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Indikatorparametrar"
   ClientHeight    =   6255
   ClientLeft      =   2940
   ClientTop       =   1530
   ClientWidth     =   8760
   Icon            =   "Frm_Parameters.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   PaletteMode     =   1  'UseZOrder
   ScaleHeight     =   6255
   ScaleWidth      =   8760
   Begin VB.Frame Frame37 
      Height          =   735
      Index           =   1
      Left            =   0
      TabIndex        =   140
      Top             =   5520
      Width           =   8655
      Begin VB.CommandButton btnCancel 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Index           =   0
         Left            =   2520
         TabIndex        =   144
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton btnOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Index           =   0
         Left            =   360
         TabIndex        =   143
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbApply 
         Caption         =   "Verkst�ll"
         Height          =   375
         Index           =   1
         Left            =   7080
         TabIndex        =   142
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbSave 
         Caption         =   "Spara"
         Height          =   375
         Index           =   1
         Left            =   4920
         TabIndex        =   141
         Top             =   240
         Width           =   975
      End
   End
   Begin TabDlg.SSTab Parameters1to6 
      Height          =   5415
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8655
      _ExtentX        =   15266
      _ExtentY        =   9551
      _Version        =   393216
      Tabs            =   21
      TabsPerRow      =   4
      TabHeight       =   529
      BackColor       =   -2147483644
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "Moving Average"
      TabPicture(0)   =   "Frm_Parameters.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Line1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame13"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame14"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "Frame15"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame16"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "RSI"
      TabPicture(1)   =   "Frm_Parameters.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Line2"
      Tab(1).Control(1)=   "Frame20"
      Tab(1).Control(2)=   "Frame21"
      Tab(1).Control(3)=   "Frame22"
      Tab(1).Control(4)=   "Frame3"
      Tab(1).Control(5)=   "Frame51"
      Tab(1).Control(6)=   "Frame52"
      Tab(1).ControlCount=   7
      TabCaption(2)   =   "Stochastic Oscillator"
      TabPicture(2)   =   "Frm_Parameters.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Line6"
      Tab(2).Control(1)=   "Frame17"
      Tab(2).Control(2)=   "Frame18"
      Tab(2).Control(3)=   "Frame19"
      Tab(2).Control(4)=   "Frame4"
      Tab(2).Control(5)=   "Frame55"
      Tab(2).Control(6)=   "Frame56"
      Tab(2).Control(7)=   "Frame57"
      Tab(2).ControlCount=   8
      TabCaption(3)   =   "Momentum"
      TabPicture(3)   =   "Frm_Parameters.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Line3"
      Tab(3).Control(1)=   "Frame23"
      Tab(3).Control(2)=   "Frame1"
      Tab(3).Control(3)=   "Frame5"
      Tab(3).Control(4)=   "Frame28"
      Tab(3).Control(5)=   "Frame29"
      Tab(3).ControlCount=   6
      TabCaption(4)   =   "Price Oscillator"
      TabPicture(4)   =   "Frm_Parameters.frx":01BA
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Line4"
      Tab(4).Control(1)=   "Frame24"
      Tab(4).Control(2)=   "Frame25"
      Tab(4).Control(3)=   "Frame26"
      Tab(4).ControlCount=   4
      TabCaption(5)   =   "MACD"
      TabPicture(5)   =   "Frm_Parameters.frx":01D6
      Tab(5).ControlEnabled=   0   'False
      Tab(5).Control(0)=   "Frame6"
      Tab(5).ControlCount=   1
      TabCaption(6)   =   "Money Flow Index"
      TabPicture(6)   =   "Frm_Parameters.frx":01F2
      Tab(6).ControlEnabled=   0   'False
      Tab(6).Control(0)=   "Frame54"
      Tab(6).Control(1)=   "Frame53"
      Tab(6).Control(2)=   "Frame7"
      Tab(6).Control(3)=   "Frame33"
      Tab(6).Control(4)=   "Frame32"
      Tab(6).Control(5)=   "Frame31"
      Tab(6).Control(6)=   "Line5"
      Tab(6).ControlCount=   7
      TabCaption(7)   =   "ATR"
      TabPicture(7)   =   "Frm_Parameters.frx":020E
      Tab(7).ControlEnabled=   0   'False
      Tab(7).Control(0)=   "Frame10"
      Tab(7).ControlCount=   1
      TabCaption(8)   =   "Parabolic SAR"
      TabPicture(8)   =   "Frm_Parameters.frx":022A
      Tab(8).ControlEnabled=   0   'False
      Tab(8).Control(0)=   "Frame9"
      Tab(8).ControlCount=   1
      TabCaption(9)   =   "Bollinger Band"
      TabPicture(9)   =   "Frm_Parameters.frx":0246
      Tab(9).ControlEnabled=   0   'False
      Tab(9).Control(0)=   "Frame2"
      Tab(9).Control(1)=   "Frame8"
      Tab(9).ControlCount=   2
      TabCaption(10)  =   "DPO"
      TabPicture(10)  =   "Frm_Parameters.frx":0262
      Tab(10).ControlEnabled=   0   'False
      Tab(10).Control(0)=   "Frame11"
      Tab(10).ControlCount=   1
      TabCaption(11)  =   "Ease Of Movement"
      TabPicture(11)  =   "Frm_Parameters.frx":027E
      Tab(11).ControlEnabled=   0   'False
      Tab(11).Control(0)=   "Frame12"
      Tab(11).Control(1)=   "Frame27"
      Tab(11).Control(2)=   "Frame30"
      Tab(11).ControlCount=   3
      TabCaption(12)  =   "Negative Volume"
      TabPicture(12)  =   "Frm_Parameters.frx":029A
      Tab(12).ControlEnabled=   0   'False
      Tab(12).Control(0)=   "Frame35"
      Tab(12).Control(1)=   "Frame34"
      Tab(12).ControlCount=   2
      TabCaption(13)  =   "Positive Volume"
      TabPicture(13)  =   "Frm_Parameters.frx":02B6
      Tab(13).ControlEnabled=   0   'False
      Tab(13).Control(0)=   "Frame36"
      Tab(13).Control(1)=   "Frame38"
      Tab(13).ControlCount=   2
      TabCaption(14)  =   "Rate-Of-Change"
      TabPicture(14)  =   "Frm_Parameters.frx":02D2
      Tab(14).ControlEnabled=   0   'False
      Tab(14).Control(0)=   "Frame39"
      Tab(14).Control(1)=   "Frame40"
      Tab(14).Control(2)=   "Frame41"
      Tab(14).ControlCount=   3
      TabCaption(15)  =   "Standardavvikelse"
      TabPicture(15)  =   "Frm_Parameters.frx":02EE
      Tab(15).ControlEnabled=   0   'False
      Tab(15).Control(0)=   "Frame42"
      Tab(15).Control(1)=   "Frame43"
      Tab(15).Control(2)=   "Frame44"
      Tab(15).ControlCount=   3
      TabCaption(16)  =   "TRIX"
      TabPicture(16)  =   "Frm_Parameters.frx":030A
      Tab(16).ControlEnabled=   0   'False
      Tab(16).Control(0)=   "Frame45"
      Tab(16).Control(1)=   "Frame46"
      Tab(16).Control(2)=   "Frame47"
      Tab(16).ControlCount=   3
      TabCaption(17)  =   "Volume Oscillator"
      TabPicture(17)  =   "Frm_Parameters.frx":0326
      Tab(17).ControlEnabled=   0   'False
      Tab(17).Control(0)=   "Frame48"
      Tab(17).Control(1)=   "Frame49"
      Tab(17).ControlCount=   2
      TabCaption(18)  =   "Volume ROC"
      TabPicture(18)  =   "Frm_Parameters.frx":0342
      Tab(18).ControlEnabled=   0   'False
      Tab(18).Control(0)=   "Frame50"
      Tab(18).ControlCount=   1
      TabCaption(19)  =   "Chaikin's Volatility"
      TabPicture(19)  =   "Frm_Parameters.frx":035E
      Tab(19).ControlEnabled=   0   'False
      Tab(19).Control(0)=   "Frame58"
      Tab(19).Control(1)=   "Frame59"
      Tab(19).ControlCount=   2
      TabCaption(20)  =   "Volatilitet"
      TabPicture(20)  =   "Frm_Parameters.frx":037A
      Tab(20).ControlEnabled=   0   'False
      Tab(20).Control(0)=   "Frame62"
      Tab(20).Control(1)=   "Frame61"
      Tab(20).Control(2)=   "Frame60"
      Tab(20).ControlCount=   3
      Begin VB.Frame Frame62 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   184
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton OptionVolatilityShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   186
            Top             =   600
            Width           =   735
         End
         Begin VB.OptionButton OptionVolatilityShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   185
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame61 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   181
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownVolatility 
            Height          =   285
            Left            =   1800
            TabIndex        =   183
            Top             =   360
            Width           =   240
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtVolatility"
            BuddyDispid     =   196618
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2160
            OrigBottom      =   615
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtVolatility 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   182
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame60 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   178
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownVolatilityMAVLength 
            Height          =   285
            Left            =   1800
            TabIndex        =   180
            Top             =   360
            Width           =   240
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   5
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtVolatilityMAVLength"
            BuddyDispid     =   196620
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1920
            OrigBottom      =   615
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtVolatilityMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   179
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame59 
         Caption         =   "Antal perioder f�r Rate-of-change"
         Height          =   855
         Left            =   -74880
         TabIndex        =   175
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownChaikinVolatilityROC 
            Height          =   285
            Left            =   1800
            TabIndex        =   177
            Top             =   360
            Width           =   240
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtChaikinVolatilityROC"
            BuddyDispid     =   196622
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   2040
            OrigBottom      =   645
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtChaikinVolatilityROC 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   176
            Text            =   "10"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame58 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   172
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownChaikinVolatility 
            Height          =   285
            Left            =   1800
            TabIndex        =   174
            Top             =   360
            Width           =   240
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtChaikinVolatility"
            BuddyDispid     =   196624
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   2040
            OrigBottom      =   735
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtChaikinVolatility 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   173
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame57 
         Caption         =   "�vre signalgr�ns"
         Height          =   855
         Left            =   -69960
         TabIndex        =   170
         Top             =   2100
         Width           =   2055
         Begin MSComCtl2.UpDown UpDownSTO5 
            Height          =   285
            Left            =   1440
            TabIndex        =   171
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   80
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtSTOUpperSignalLevel"
            BuddyDispid     =   196626
            OrigLeft        =   1320
            OrigTop         =   360
            OrigRight       =   1515
            OrigBottom      =   735
            Max             =   99
            Min             =   51
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtSTOUpperSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   480
            TabIndex        =   11
            Text            =   "80"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame56 
         Caption         =   "Undre signalgr�ns"
         Height          =   855
         Left            =   -72120
         TabIndex        =   168
         Top             =   2100
         Width           =   2055
         Begin MSComCtl2.UpDown UpDownSTO4 
            Height          =   285
            Left            =   1440
            TabIndex        =   169
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtSTOLowerSignalLevel"
            BuddyDispid     =   196628
            OrigLeft        =   1440
            OrigTop         =   360
            OrigRight       =   1635
            OrigBottom      =   495
            Max             =   49
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtSTOLowerSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   480
            TabIndex        =   10
            Text            =   "20"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame55 
         Caption         =   "Slowing %K"
         Height          =   855
         Left            =   -74880
         TabIndex        =   165
         Top             =   3060
         Width           =   2295
         Begin MSComCtl2.UpDown UpDownSTO2 
            Height          =   285
            Left            =   1560
            TabIndex        =   166
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   3
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtSTOSlowing"
            BuddyDispid     =   196630
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   615
            Max             =   500
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtSTOSlowing 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   600
            TabIndex        =   8
            Text            =   "3"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame54 
         Caption         =   "�vre signalgr�ns"
         Height          =   855
         Left            =   -71640
         TabIndex        =   162
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMFI4 
            Height          =   285
            Left            =   1800
            TabIndex        =   163
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   70
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtMFIUpperSignalLevel"
            BuddyDispid     =   196632
            OrigLeft        =   2040
            OrigTop         =   360
            OrigRight       =   2235
            OrigBottom      =   615
            Max             =   99
            Min             =   51
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtMFIUpperSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   19
            Text            =   "70"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame53 
         Caption         =   "Undre signalgr�ns"
         Height          =   855
         Left            =   -71640
         TabIndex        =   160
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMFI3 
            Height          =   285
            Left            =   1800
            TabIndex        =   161
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   30
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtMFILowerSignalLevel"
            BuddyDispid     =   196634
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   49
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtMFILowerSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   18
            Text            =   "30"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame52 
         Caption         =   "�vre signalgr�ns"
         Height          =   855
         Left            =   -71640
         TabIndex        =   151
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownRSI4 
            Height          =   285
            Left            =   1800
            TabIndex        =   152
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   70
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtRSIUpperSignalLevel"
            BuddyDispid     =   196636
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2115
            OrigBottom      =   640
            Max             =   99
            Min             =   51
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtRSIUpperSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   6
            Text            =   "70"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame51 
         Caption         =   "Undre signalgr�ns"
         Height          =   855
         Left            =   -71640
         TabIndex        =   149
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownRSI3 
            Height          =   285
            Left            =   1800
            TabIndex        =   150
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   30
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtRSILowerSignalLevel"
            BuddyDispid     =   196638
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   49
            Min             =   1
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtRSILowerSignalLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   5
            Text            =   "30"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame50 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   138
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownVROC 
            Height          =   285
            Left            =   1800
            TabIndex        =   139
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   12
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtVolumeRateOfChangeLength"
            BuddyDispid     =   196640
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   200
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtVolumeRateOfChangeLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   37
            Text            =   "12"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame49 
         Caption         =   "Antal perioder f�r medelv�rde 2"
         Height          =   855
         Left            =   -74880
         TabIndex        =   136
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownVO2 
            Height          =   285
            Left            =   1800
            TabIndex        =   137
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   25
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtVolumeOscillatorMAV2Length"
            BuddyDispid     =   196642
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2115
            OrigBottom      =   640
            Max             =   200
            Min             =   3
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtVolumeOscillatorMAV2Length 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   36
            Text            =   "25"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame48 
         Caption         =   "Antal perioder f�r medelv�rde 1"
         Height          =   855
         Left            =   -74880
         TabIndex        =   134
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownVO1 
            Height          =   285
            Left            =   1800
            TabIndex        =   135
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   15
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtVolumeOscillatorMAV1Length"
            BuddyDispid     =   196644
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   200
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtVolumeOscillatorMAV1Length 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   35
            Text            =   "15"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame47 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   131
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton optTRIXShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   133
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton optTRIXShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   132
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame46 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   129
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownTRIX2 
            Height          =   285
            Left            =   1800
            TabIndex        =   130
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   9
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtTRIXMAVLength"
            BuddyDispid     =   196649
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtTRIXMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   34
            Text            =   "9"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame45 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   127
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownTRIX1 
            Height          =   285
            Left            =   1800
            TabIndex        =   128
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   12
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtTRIXLength"
            BuddyDispid     =   196651
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtTRIXLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   33
            Text            =   "12"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame44 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   124
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton optSTDShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   126
            Top             =   600
            Width           =   735
         End
         Begin VB.OptionButton optSTDShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   125
            Top             =   360
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Frame Frame43 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   122
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownSTD2 
            Height          =   285
            Left            =   1800
            TabIndex        =   123
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   5
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtStandardDeviationMAVLength"
            BuddyDispid     =   196656
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2115
            OrigBottom      =   640
            Max             =   200
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtStandardDeviationMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   32
            Text            =   "5"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame42 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   120
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownSTD1 
            Height          =   285
            Left            =   1800
            TabIndex        =   121
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtStandardDeviationLength"
            BuddyDispid     =   196658
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   200
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtStandardDeviationLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   31
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame41 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   117
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton optROCShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   119
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton optROCShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   118
            Top             =   360
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Frame Frame40 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   115
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownROC2 
            Height          =   285
            Left            =   1800
            TabIndex        =   116
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   2
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtRateOfChangeMAVLength"
            BuddyDispid     =   196663
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtRateOfChangeMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   30
            Text            =   "9"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame39 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   113
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownROC1 
            Height          =   285
            Left            =   1800
            TabIndex        =   114
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   5
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtRateOfChangeLength"
            BuddyDispid     =   196665
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtRateOfChangeLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   29
            Text            =   "5"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame38 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   110
         Top             =   3060
         Width           =   2775
         Begin VB.OptionButton optPVShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   112
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton optPVShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   111
            Top             =   360
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Frame Frame36 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   108
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownPV 
            Height          =   285
            Left            =   1800
            TabIndex        =   109
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtPositiveVolumeMAVLength"
            BuddyDispid     =   196670
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   100
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtPositiveVolumeMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   28
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame35 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   105
         Top             =   3060
         Width           =   2775
         Begin VB.OptionButton optNVShowMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   1080
            TabIndex        =   107
            Top             =   600
            Width           =   735
         End
         Begin VB.OptionButton optNVShowMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   1080
            TabIndex        =   106
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame34 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   103
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownNV 
            Height          =   285
            Left            =   1800
            TabIndex        =   104
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtNegativeVolumeMAVLength"
            BuddyDispid     =   196675
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   100
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtNegativeVolumeMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   27
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame30 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   100
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton optEOMandMAVNo 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   102
            Top             =   600
            Width           =   615
         End
         Begin VB.OptionButton optEOMandMAVYes 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   101
            Top             =   360
            Value           =   -1  'True
            Width           =   615
         End
      End
      Begin VB.Frame Frame27 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   98
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownEOM2 
            Height          =   285
            Left            =   1680
            TabIndex        =   99
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   9
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtEaseOfMovementMAVLength"
            BuddyDispid     =   196680
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtEaseOfMovementMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   720
            TabIndex        =   26
            Text            =   "9"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   96
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownEOM1 
            Height          =   285
            Left            =   1680
            TabIndex        =   97
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtEaseOfMovementLength"
            BuddyDispid     =   196682
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtEaseOfMovementLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   720
            TabIndex        =   25
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   94
         Top             =   2100
         Width           =   2895
         Begin MSComCtl2.UpDown UpDownDPO 
            Height          =   285
            Left            =   1800
            TabIndex        =   95
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtDPOLength"
            BuddyDispid     =   196684
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtDPOLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   24
            Text            =   "20"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   92
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownATR 
            Height          =   285
            Left            =   1800
            TabIndex        =   93
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "txtAverageTrueRangeLength"
            BuddyDispid     =   196686
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   645
            Max             =   200
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox txtAverageTrueRangeLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            TabIndex        =   20
            Text            =   "14"
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame9 
         Caption         =   "Accelerationsfaktor"
         Height          =   855
         Left            =   -74880
         TabIndex        =   91
         Top             =   2100
         Width           =   2775
         Begin VB.TextBox tbAccelerationfactor 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   21
            Text            =   "Frm_Parameters.frx":0396
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame8 
         Caption         =   "Antal standardavvikelser"
         Height          =   855
         Left            =   -74880
         TabIndex        =   90
         Top             =   3060
         Width           =   2775
         Begin VB.TextBox tbBollingerSTD 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   23
            Text            =   "Frm_Parameters.frx":039B
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   89
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownBOL 
            Height          =   285
            Left            =   1800
            TabIndex        =   164
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "tbBollingerMAV"
            BuddyDispid     =   196692
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   615
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox tbBollingerMAV 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   22
            Text            =   "Frm_Parameters.frx":039D
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Signaler"
         Height          =   975
         Left            =   -71640
         TabIndex        =   85
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton OptionMFISignalUseMAV 
            Caption         =   "Anv�nd sk�rningar med MAV"
            Height          =   255
            Left            =   120
            TabIndex        =   87
            Top             =   600
            Width           =   2415
         End
         Begin VB.OptionButton OptionMFISignalDontUseMAV 
            Caption         =   "Anv�nd signalniv�er"
            Height          =   255
            Left            =   120
            TabIndex        =   86
            Top             =   360
            Value           =   -1  'True
            Width           =   1935
         End
      End
      Begin VB.Frame Frame33 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   84
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMFI2 
            Height          =   285
            Left            =   1800
            TabIndex        =   159
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "tbMFIMAVLength"
            BuddyDispid     =   196697
            OrigLeft        =   1800
            OrigTop         =   360
            OrigRight       =   1995
            OrigBottom      =   615
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox tbMFIMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   17
            Text            =   "Frm_Parameters.frx":03A0
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame32 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   83
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMFI1 
            Height          =   285
            Left            =   1800
            TabIndex        =   158
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   25
            AutoBuddy       =   -1  'True
            BuddyControl    =   "tbMFILength"
            BuddyDispid     =   196699
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox tbMFILength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   16
            Text            =   "Frm_Parameters.frx":03A3
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame31 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   80
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton OptionShowMFI 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   82
            Top             =   600
            Width           =   855
         End
         Begin VB.OptionButton OptionShowMFIAndMAV 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   81
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame29 
         Caption         =   "MomentumSignal"
         Height          =   975
         Left            =   -71640
         TabIndex        =   77
         Top             =   3180
         Width           =   2775
         Begin VB.OptionButton OptionMOMSignalUseMINMAX 
            Caption         =   "Anv�nd toppar och bottnar"
            Height          =   255
            Left            =   120
            TabIndex        =   79
            Top             =   600
            Width           =   2535
         End
         Begin VB.OptionButton OptionSignalUseZeroLine 
            Caption         =   "Anv�nd sk�rning med nollinjen"
            Height          =   255
            Left            =   120
            TabIndex        =   78
            Top             =   360
            Value           =   -1  'True
            Width           =   2535
         End
      End
      Begin VB.Frame Frame28 
         Caption         =   "Anv�nd medelv�rde f�r signal"
         Height          =   975
         Left            =   -71640
         TabIndex        =   74
         Top             =   2100
         Width           =   2775
         Begin VB.OptionButton OptionDontUseMAVInSignal 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   76
            Top             =   600
            Width           =   975
         End
         Begin VB.OptionButton OptionMOMSignalUseMAV 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   75
            Top             =   360
            Value           =   -1  'True
            Width           =   975
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Visa MACD signallinje"
         Height          =   975
         Left            =   -74880
         TabIndex        =   71
         Top             =   2100
         Width           =   2895
         Begin VB.OptionButton OptionShowMACDOnly 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   73
            Top             =   600
            Width           =   855
         End
         Begin VB.OptionButton Option_MACDSignal 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   72
            Top             =   360
            Value           =   -1  'True
            Width           =   855
         End
      End
      Begin VB.Frame Frame4 
         Caption         =   "Signalmetod"
         Height          =   975
         Left            =   -69960
         TabIndex        =   68
         Top             =   3900
         Width           =   2655
         Begin VB.OptionButton optSTOSignalUseMAV 
            Caption         =   "Anv�nd sk�rningar med MAV"
            Height          =   255
            Left            =   120
            TabIndex        =   70
            Top             =   600
            Width           =   2415
         End
         Begin VB.OptionButton optSTOSignalUseLevels 
            Caption         =   "Anv�nd signalniv�er"
            Height          =   255
            Left            =   120
            TabIndex        =   69
            Top             =   360
            Value           =   -1  'True
            Width           =   1740
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Signalmetod"
         Height          =   975
         Left            =   -71640
         TabIndex        =   66
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton Option_SignalRSIUseMAV 
            Caption         =   "Anv�nd sk�rningar med MAV"
            Height          =   255
            Left            =   120
            TabIndex        =   88
            Top             =   600
            Value           =   -1  'True
            Width           =   2475
         End
         Begin VB.OptionButton Option_SignalRSIDontUseMAV 
            Caption         =   "Anv�nd signalniv�er"
            Height          =   255
            Left            =   120
            TabIndex        =   67
            Top             =   360
            Width           =   2055
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Medelv�rdesl�ngd"
         Height          =   855
         Left            =   -74880
         TabIndex        =   63
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMOM2 
            Height          =   285
            Left            =   1800
            TabIndex        =   155
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_MOMMAVLength"
            BuddyDispid     =   196719
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2115
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_MOMMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   13
            Text            =   "Frm_Parameters.frx":03A6
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   62
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton Option_DontShowMOMMAV 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   65
            Top             =   600
            Width           =   975
         End
         Begin VB.OptionButton Option_ShowMOMMAV 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   64
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame26 
         Caption         =   "Typ av medelv�rde"
         Height          =   975
         Left            =   -71640
         TabIndex        =   59
         Top             =   2100
         Width           =   2775
         Begin VB.OptionButton Option_PriceOsc_Exponential 
            Caption         =   "Exponentiellt"
            Height          =   255
            Left            =   840
            TabIndex        =   61
            Top             =   480
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton Option_PriceOsc_Simple 
            Caption         =   "Ov�gt"
            Height          =   255
            Left            =   840
            TabIndex        =   60
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.Frame Frame25 
         Caption         =   "Antal perioder f�r medelv�rde 2"
         Height          =   855
         Left            =   -74880
         TabIndex        =   58
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownPriceOsc2 
            Height          =   285
            Left            =   1800
            TabIndex        =   157
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   40
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_PriceOsc_MAV2"
            BuddyDispid     =   196727
            OrigLeft        =   1800
            OrigTop         =   240
            OrigRight       =   1995
            OrigBottom      =   495
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_PriceOsc_MAV2 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   15
            Text            =   "Frm_Parameters.frx":03A9
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame24 
         Caption         =   "Antal perioder f�r medelv�rde 1"
         Height          =   855
         Left            =   -74880
         TabIndex        =   57
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownPriceOsc1 
            Height          =   285
            Left            =   1800
            TabIndex        =   156
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_PriceOsc_MAV1"
            BuddyDispid     =   196729
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   615
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_PriceOsc_MAV1 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   14
            Text            =   "Frm_Parameters.frx":03AE
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame23 
         Caption         =   "Periodl�ngd"
         Height          =   855
         Left            =   -74880
         TabIndex        =   56
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownMOM1 
            Height          =   285
            Left            =   1800
            TabIndex        =   154
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   5
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_MOMLength"
            BuddyDispid     =   196731
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_MOMLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   12
            Text            =   "Frm_Parameters.frx":03B3
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame22 
         Caption         =   "Antal perioder f�r medelv�rdet"
         Height          =   855
         Left            =   -74880
         TabIndex        =   55
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownRSI2 
            Height          =   285
            Left            =   1800
            TabIndex        =   148
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   5
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_RSIMAVLength"
            BuddyDispid     =   196733
            OrigLeft        =   1920
            OrigTop         =   240
            OrigRight       =   2115
            OrigBottom      =   520
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_RSIMAVLength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   4
            Text            =   "Frm_Parameters.frx":03B5
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame21 
         Caption         =   "Antal perioder"
         Height          =   855
         Left            =   -74880
         TabIndex        =   54
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownRSI1 
            Height          =   285
            Left            =   1800
            TabIndex        =   147
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   14
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_RSILength"
            BuddyDispid     =   196735
            OrigLeft        =   2040
            OrigTop         =   360
            OrigRight       =   2235
            OrigBottom      =   645
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_RSILength 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   3
            Text            =   "Frm_Parameters.frx":03B7
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame20 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -74880
         TabIndex        =   51
         Top             =   4020
         Width           =   2775
         Begin VB.OptionButton Option2 
            Caption         =   "Nej"
            Height          =   255
            Left            =   960
            TabIndex        =   53
            Top             =   600
            Width           =   735
         End
         Begin VB.OptionButton Option_ShowRSI 
            Caption         =   "Ja"
            Height          =   255
            Left            =   960
            TabIndex        =   52
            Top             =   360
            Value           =   -1  'True
            Width           =   735
         End
      End
      Begin VB.Frame Frame19 
         Caption         =   "Antal perioder %D"
         Height          =   855
         Left            =   -74880
         TabIndex        =   50
         Top             =   4020
         Width           =   2295
         Begin MSComCtl2.UpDown UpDownSTO3 
            Height          =   285
            Left            =   1560
            TabIndex        =   167
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   3
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_StoOscPeriodD"
            BuddyDispid     =   196740
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   735
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_StoOscPeriodD 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   600
            MultiLine       =   -1  'True
            TabIndex        =   9
            Text            =   "Frm_Parameters.frx":03BA
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame18 
         Caption         =   "Antal perioder %K"
         Height          =   855
         Left            =   -74880
         TabIndex        =   49
         Top             =   2100
         Width           =   2295
         Begin MSComCtl2.UpDown UpDownSTO1 
            Height          =   285
            Left            =   1560
            TabIndex        =   153
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Tb_StoOscPeriodK"
            BuddyDispid     =   196742
            OrigLeft        =   1680
            OrigTop         =   360
            OrigRight       =   1875
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Tb_StoOscPeriodK 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   600
            MultiLine       =   -1  'True
            TabIndex        =   7
            Text            =   "Frm_Parameters.frx":03BC
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame17 
         Caption         =   "Visa medelv�rdet i grafen"
         Height          =   975
         Left            =   -72240
         TabIndex        =   46
         Top             =   3900
         Width           =   2175
         Begin VB.OptionButton Option_ShowOsc 
            Caption         =   "Nej"
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   48
            Top             =   600
            Value           =   -1  'True
            Width           =   735
         End
         Begin VB.OptionButton Option_ShowOscAndMAV 
            Caption         =   "Ja"
            Height          =   255
            Index           =   0
            Left            =   720
            TabIndex        =   47
            Top             =   360
            Width           =   735
         End
      End
      Begin VB.Frame Frame16 
         Caption         =   "L�ngt medelv�rde"
         Height          =   855
         Left            =   120
         TabIndex        =   43
         Top             =   3060
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownParaMAVPeriod2 
            Height          =   285
            Left            =   1800
            TabIndex        =   146
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   20
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Para_MAV_Period2"
            BuddyDispid     =   196747
            OrigLeft        =   1920
            OrigTop         =   360
            OrigRight       =   2115
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Para_MAV_Period2 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   2
            Text            =   "Frm_Parameters.frx":03BF
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Frame Frame15 
         Caption         =   "Antal medelv�rden"
         Height          =   855
         Left            =   3360
         TabIndex        =   41
         Top             =   3060
         Width           =   2775
         Begin VB.OptionButton Option_TwoMAV 
            Caption         =   "Tv�"
            Height          =   255
            Left            =   120
            TabIndex        =   45
            Top             =   480
            Value           =   -1  'True
            Width           =   1335
         End
         Begin VB.OptionButton Option_OneMAV 
            Caption         =   "Ett"
            Height          =   255
            Left            =   120
            TabIndex        =   42
            Top             =   240
            Width           =   1215
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Typ av medelv�rde"
         Height          =   855
         Left            =   3360
         TabIndex        =   39
         Top             =   2100
         Width           =   2775
         Begin VB.OptionButton Option_Exp 
            Caption         =   "Exponentiellt medelv�rde"
            Height          =   255
            Left            =   120
            TabIndex        =   44
            Top             =   480
            Value           =   -1  'True
            Width           =   2415
         End
         Begin VB.OptionButton Option_Simple 
            Caption         =   "Ov�gt medelv�rde"
            Height          =   255
            Left            =   120
            TabIndex        =   40
            Top             =   240
            Width           =   2175
         End
      End
      Begin VB.Frame Frame13 
         Caption         =   "Kort medelv�rde"
         Height          =   855
         Left            =   120
         TabIndex        =   38
         Top             =   2100
         Width           =   2775
         Begin MSComCtl2.UpDown UpDownParaMAVPeriod1 
            Height          =   285
            Left            =   1800
            TabIndex        =   145
            Top             =   360
            Width           =   195
            _ExtentX        =   344
            _ExtentY        =   503
            _Version        =   393216
            Value           =   10
            AutoBuddy       =   -1  'True
            BuddyControl    =   "Para_MAV_Period1"
            BuddyDispid     =   196755
            OrigLeft        =   1560
            OrigTop         =   360
            OrigRight       =   1755
            OrigBottom      =   640
            Max             =   500
            Min             =   2
            SyncBuddy       =   -1  'True
            BuddyProperty   =   0
            Enabled         =   -1  'True
         End
         Begin VB.TextBox Para_MAV_Period1 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   840
            MultiLine       =   -1  'True
            TabIndex        =   1
            Text            =   "Frm_Parameters.frx":03C2
            Top             =   360
            Width           =   975
         End
      End
      Begin VB.Line Line6 
         X1              =   -72360
         X2              =   -72360
         Y1              =   2100
         Y2              =   4980
      End
      Begin VB.Line Line5 
         X1              =   -71880
         X2              =   -71880
         Y1              =   2100
         Y2              =   5100
      End
      Begin VB.Line Line4 
         X1              =   -71880
         X2              =   -71880
         Y1              =   2100
         Y2              =   4020
      End
      Begin VB.Line Line3 
         X1              =   -71880
         X2              =   -71880
         Y1              =   2100
         Y2              =   4980
      End
      Begin VB.Line Line2 
         X1              =   -71880
         X2              =   -71880
         Y1              =   2100
         Y2              =   5100
      End
      Begin VB.Line Line1 
         X1              =   3120
         X2              =   3120
         Y1              =   2100
         Y2              =   4020
      End
   End
End
Attribute VB_Name = "Frm_Parameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub btnCancel_Click(Index As Integer)
  Unload Frm_Parameters
End Sub

Private Sub btnOK_Click(Index As Integer)
  Dim Errorfound As Boolean
  'ControllParameters Frm_Parameters, ErrorFound
  ErrorCheckLengthOfParameters Frm_Parameters, Errorfound
  If Errorfound Then
    Exit Sub
  End If
  UpdateParameterSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
   Unload Frm_Parameters
End Sub

Private Sub Command1_Click()

End Sub


Private Sub cbApply_Click(Index As Integer)
Dim Errorfound As Boolean
  'ControllParameters Frm_Parameters, Errorfound
  ErrorCheckLengthOfParameters Frm_Parameters, Errorfound
  If Errorfound Then
    Exit Sub
  End If
  UpdateParameterSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
End Sub

Private Sub cbSave_Click(Index As Integer)
  Dim Errorfound As Boolean
  Dim Answer As Long
  'ControllParameters Frm_Parameters, ErrorFound
  ErrorCheckLengthOfParameters Frm_Parameters, Errorfound
    If Errorfound Then
      Exit Sub
    End If
  Answer = YesNoCancel("Vill du spara parametrarna som allm�nna?", "Spara?")
  Select Case Answer
  Case 6
    UpdateParameterSettings
    SaveParameterSettingsToRegistry
    MsgBox "Parametrarna �r sparade som allm�nna"
  End Select
  If Answer <> 2 Then
    Answer = YesNoCancel("Vill du spara parametrarna som objektberoende?", "Spara?")
  Select Case Answer
  Case 6
    UpdateParameterSettings
    SaveParameterSettingsToInvest ActiveIndex
    WriteWorkspaceSettings
    MsgBox "Parametrarna �r sparade som objektberoende"
  End Select
  End If
End Sub

Private Sub Form_Load()
  'MainMDI.M_Pref.Enabled = False
  Top = (MainMDI.Height - Height - 2000) / 2
  Left = (MainMDI.Width - Width) / 2
  InitParameters
  If UseGeneralIndicatorParameters Then
    Caption = "Indikatorparametrar (Allm�nna)"
  Else
    Caption = "Indikatorparametrar (Objektberoende)"
    'MainMDI.C_Category.Enabled = False
    'MainMDI.C_StockList.Enabled = False
  End If
End Sub

Private Sub Form_Terminate()
  'MainMDI.M_Pref.Enabled = True
  'MainMDI.C_Category.Enabled = True
  'MainMDI.C_StockList.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  'MainMDI.M_Pref.Enabled = True
  'MainMDI.C_Category.Enabled = True
  'MainMDI.C_StockList.Enabled = True
End Sub



Private Sub Para_MAV_Period1_GotFocus()
  TextSelected
End Sub

Private Sub Para_MAV_Period1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Para_MAV_Period1, UpDownParaMAVPeriod1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Para_MAV_Period2_GotFocus()
  TextSelected
End Sub

Private Sub Para_MAV_Period2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Para_MAV_Period1, UpDownParaMAVPeriod2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_MOMLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_MOMLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_MOMLength, UpDownMOM1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_MOMMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_MOMMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_MOMMAVLength, UpDownMOM2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_PriceOsc_MAV1_GotFocus()
  TextSelected
End Sub

Private Sub Tb_PriceOsc_MAV1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_PriceOsc_MAV1, UpDownPriceOsc1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_PriceOsc_MAV2_GotFocus()
  TextSelected
End Sub

Private Sub Tb_PriceOsc_MAV2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_PriceOsc_MAV2, UpDownPriceOsc2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_RSILength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_RSILength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_RSILength, UpDownRSI1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_RSIMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_RSIMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_RSIMAVLength, UpDownRSI2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_StoOscPeriodD_GotFocus()
  TextSelected
End Sub

Private Sub Tb_StoOscPeriodD_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_StoOscPeriodD, UpDownSTO3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_StoOscPeriodK_GotFocus()
  TextSelected
End Sub

Private Sub Tb_StoOscPeriodK_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_StoOscPeriodK, UpDownSTO1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbAccelerationfactor_GotFocus()
  TextSelected
End Sub

Private Sub tbAccelerationfactor_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySinglePara tbAccelerationfactor, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    If CSng(tbAccelerationfactor) < 0.02 Or CSng(tbAccelerationfactor) > 0.2 Then
      Min = "0,02"
      Max = "0,2"
      Message = "Du har angivit ett f�r stort eller f�r litet v�rde. Min = " + Str$(Min) + ", Max = " + Str$(Max) + ". T�nk p� att en del datorer anv�nder . som decimaltalsavskiljare."
      MsgBox Message, vbOKOnly, "Felmeddelande"
      Cancel = True
    Else
      Cancel = False
    End If
  End If
End Sub

Private Sub tbBollingerMAV_GotFocus()
  TextSelected
End Sub

Private Sub tbBollingerMAV_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbBollingerMAV, UpDownBOL, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbBollingerSTD_GotFocus()
  TextSelected
End Sub

Private Sub tbBollingerSTD_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySinglePara tbBollingerSTD, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    If CSng(tbAccelerationfactor) < 0.1 Or CSng(tbAccelerationfactor) > 5 Then
      Min = "0,1"
      Max = "5"
      Message = "Du har angivit ett f�r stort eller f�r litet v�rde. Min = " + Str$(Min) + ", Max = " + Str$(Max) + ". T�nk p� att en del datorer anv�nder . som decimaltalsavskiljare."
      MsgBox Message, vbOKOnly, "Felmeddelande"
      Cancel = True
    Else
      Cancel = False
    End If
  End If

End Sub

Private Sub tbMFILength_GotFocus()
  TextSelected
End Sub

Private Sub tbMFILength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbMFILength, UpDownMFI1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbMFIMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub tbMFIMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbMFIMAVLength, UpDownMFI2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtAverageTrueRangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtAverageTrueRangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtAverageTrueRangeLength, UpDownATR, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtChaikinVolatility_GotFocus()
  TextSelected
End Sub

Private Sub txtChaikinVolatility_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtChaikinVolatility, UpDownChaikinVolatility, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtChaikinVolatilityROC_GotFocus()
  TextSelected
End Sub

Private Sub txtChaikinVolatilityROC_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtChaikinVolatilityROC, UpDownChaikinVolatilityROC, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtDPOLength_GotFocus()
  TextSelected
End Sub

Private Sub txtDPOLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtDPOLength, UpDownDPO, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtEaseOfMovementLength_GotFocus()
  TextSelected
End Sub

Private Sub txtEaseOfMovementLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtEaseOfMovementLength, UpDownEOM1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtEaseOfMovementMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtEaseOfMovementMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtEaseOfMovementMAVLength, UpDownEOM2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtMFILowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtMFILowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtMFILowerSignalLevel, UpDownMFI3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtMFIUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtMFIUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtMFIUpperSignalLevel, UpDownMFI4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtNegativeVolumeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtNegativeVolumeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtNegativeVolumeMAVLength, UpDownNV, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtPositiveVolumeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtPositiveVolumeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtPositiveVolumeMAVLength, UpDownPV, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRateOfChangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtRateOfChangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRateOfChangeLength, UpDownROC1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRateOfChangeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtRateOfChangeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRateOfChangeMAVLength, UpDownROC2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRSILowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtRSILowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRSILowerSignalLevel, UpDownRSI3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRSIUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtRSIUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRSIUpperSignalLevel, UpDownRSI4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtStandardDeviationLength_GotFocus()
  TextSelected
End Sub

Private Sub txtStandardDeviationLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtStandardDeviationLength, UpDownSTD1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtStandardDeviationMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtStandardDeviationMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtStandardDeviationMAVLength, UpDownSTD2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOLowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOLowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOLowerSignalLevel, UpDownSTO4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOSlowing_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOSlowing_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOSlowing, UpDownSTO2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOUpperSignalLevel, UpDownSTO5, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtTRIXLength_GotFocus()
  TextSelected
End Sub

Private Sub txtTRIXLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtTRIXLength, UpDownTRIX1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtTRIXMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtTRIXMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtTRIXMAVLength, UpDownTRIX2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolatility_GotFocus()
  TextSelected
End Sub

Private Sub txtVolatility_Validate(Cancel As Boolean)
Dim Errorfound As Boolean
  VerifyLongPara txtVolatility, UpDownVolatility, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolatilityMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtVolatilityMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolatilityMAVLength, UpDownVolatilityMAVLength, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeOscillatorMAV1Length_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeOscillatorMAV1Length_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeOscillatorMAV1Length, UpDownVO1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeOscillatorMAV2Length_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeOscillatorMAV2Length_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeOscillatorMAV2Length, UpDownVO2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeRateOfChangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeRateOfChangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeRateOfChangeLength, UpDownVROC, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub


