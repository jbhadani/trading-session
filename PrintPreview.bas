Attribute VB_Name = "PrintPreview"
'frmPrintHidden
'
'1   TempInit > inizializza la routine di anteprima
'               si deve chiamare anche uscendo dal programma principale per
'               cancellare i files temporanei
'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
'    >> questo indica l'inizio della pagina
'       al momento incorpora una righetta, ma si pu� modificare il codice
'1   PrintHeader "pippo", "peppe - 1", 0
'2   PrintInCen 10, 10, "Pagina 1", "Tahoma", 12, True, 0
'2   PrintJust 2, 15, "pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo pippo", "Times New Roman", 8, False, 5, 0
'    >> controllo di appoggio per indicare le immagini da stampare
'       pu� avere il nome che si vuole e deve essere un IMAGE o un PICTURE
'2   MyImg.Picture = LoadPicture("c:\newmedia\images\nm_back.bmp")
'2   PrintImg MyImg, 2, 18, 8, 5, 0
'    >> questo indica la fine della pagina
'       al momento incorpora una righetta, ma si pu� modificare il codice
'1   PrintFooter "pippo", "peppe", 0
'
'1 > obblogatori



Option Explicit

Global Const mm = 567
Global Const cm = 567
Global Const NM_PP_Ofs = 0 '36

Global Const Gray = &HC0C0C0
Global Scala As Single

Global Const ANTEPRIMA = 0
Global Const STAMPANTE = 1

Global Const NONESCLUSIVO = 0
Global Const ESCLUSIVO = 1

Global LocPerc As String
Global Const LocName = "_$$_TEMP.TMP"

Global Ofs As Single

Global Const SistemaCoordinate = 0

Global NM_AnnullaStampa As Boolean
Global TempDemoMode As Boolean

Sub SistemaBarra(sP As Integer, eP As Integer, aP As Integer)

    ' frmPrint.sBar > max bar
    ' frmPrint.aBar > actual value
    '
    ' sP = start page
    ' eP = end page
    ' aP = actual page
    
    Static Stp As Single
    Stp = frmPrint.tBar.Width / ((eP - sP) + 1)
    
    frmPrint.pBar.Width = Stp * aP

End Sub

Function TempFileExists(MyFilename As String) As Boolean

Dim TempAttr As Double
TempFileExists = True

On Error GoTo MyErrorFileExist

    TempAttr = FileLen(MyFilename)
    GoTo MyExitFileExist
   
MyErrorFileExist:
    TempFileExists = False
    Resume MyExitFileExist
   
MyExitFileExist:
    On Error GoTo 0

End Function



Sub ContaPagine()

    frmPrintPreview.MousePointer = vbHourglass

    Static NumPag As Integer
    NumPag = 0
    Static A As String, B As String
    
    frmPrintPreview.ePag.Clear
    
    Open LocPerc + LocName For Append As #27: Close #27
    '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    Open LocPerc + LocName For Input As #27
        While Not EOF(27)
            Line Input #27, A
            If A = "#startpage" Then
                NumPag = NumPag + 1
            ElseIf A = "#endpage" Then
                frmPrintPreview.ePag.AddItem Format(NumPag)
            End If
        Wend
    Close #27
    
    If frmPrintPreview.ePag.ListCount > 0 Then
        frmPrintPreview.ePag.ListIndex = 0
    Else
        frmPrintPreview.MousePointer = vbDefault
        MsgBox "No pages to print!", vbInformation, "Preview non available"
        Unload frmPrintPreview
    End If
    
    frmPrintPreview.MousePointer = vbDefault

End Sub

Sub SistemaStatusBar()

    frmPrintPreview.aPag.Caption = frmPrintPreview.ePag.Text
    frmPrintPreview.tPag.Caption = frmPrintPreview.ePag.ListCount
    frmPrintPreview.zPag.Caption = frmPrintPreview.zVal.Text + "%"

End Sub


Function StripComma(S As String) As Single

    Static l As Integer

    For l = 1 To Len(S)
        If Mid(S, l, 1) = "," Then
            Mid(S, l, 1) = "."
        End If
    Next
    
    StripComma = Val(S)

End Function

Sub TempDelete()
    Open LocPerc + LocName For Append As #25
    Close #25
    Kill LocPerc + LocName
End Sub

Sub TempInit()

    frmPrintPreview.TmpList.Pattern = "_$$_*.TMP"
    frmPrintPreview.TmpList.Path = Left(LocPerc, Len(LocPerc) - 1)
    frmPrintPreview.TmpList.Refresh
    If frmPrintPreview.TmpList.ListCount > 0 Then
        Kill LocPerc + "_$$_*.TMP"
    End If

    Randomize 1

    Open LocPerc + LocName For Output As #25
    Close #25
    Unload frmPrintPreview
    
End Sub

Sub TempPrint(Dato As String)
    Open LocPerc + LocName For Append As #25
        Print #25, Dato
    Close #25
End Sub

Sub PrintHeader(Sin As String, Des As String, Dst As Integer)
    
    TempPrint "#startpage"
    PrintBox 2, 0.9, 18, 0.91, ANTEPRIMA
    PrintInLef 2, 0.55, Sin, "Arial", 8, False, ANTEPRIMA
    PrintInRig 18, 0.55, Des, "Arial", 8, False, ANTEPRIMA
    If TempDemoMode = True Then
        PrintCross 2, 0.9, 17.9, 26.01, ANTEPRIMA
        PrintCross 2.1, 0.9, 18, 26.01, ANTEPRIMA
    End If

End Sub


Sub PrintFooter(Sin As String, Des As String, Dst As Integer)

    PrintBox 2, 26, 18, 26.01, ANTEPRIMA
    PrintInLef 2, 26.1, Sin, "Arial", 8, False, ANTEPRIMA
    PrintInRig 18, 26.1, Des, "Arial", 8, False, ANTEPRIMA
    TempPrint "#endpage"
    ContaPagine
    
End Sub


Sub PrintRefGrid(Dst As Integer)

    Static X, Y As Integer
    
    TempPrint "#fontname"
    TempPrint "Arial"
    TempPrint "#fontsize"
    TempPrint Format(6 * Scala)
    
    For Y = 0 To 26
        TempPrint "#y"
        TempPrint Format(Y * mm)
        For X = 0 To 19
            TempPrint "#x"
            TempPrint Format(X * mm)
            TempPrint "#txt"
            TempPrint "+" & Format$(X, "#,##0") & "," & Format$(Y, "#,##0")
        Next
    Next

End Sub

Sub PrintJust(X As Single, Y As Single, Phrase As String, Fname As String, Fsize As Integer, Fbold As Integer, Larg As Single, Dst As Integer)

    ReDim aT(500) As String
    Static NumPar As Integer
    Static aP As String
    Static OaP As String
    Static lP, l As Integer
    Static VecOfs As Single
    Static Interl As Single
    
    frmPrintPreview.Prv.FontName = Fname
    frmPrintPreview.Prv.FontSize = Fsize
    frmPrintPreview.Prv.FontBold = Fbold
    
    'Interl = frmPrintPreview.Prv.TextHeight(Phrase)
    Interl = 0.4
    
    If frmPrintPreview.Prv.TextWidth(Phrase) > Larg * mm Then
        NumPar = 0
        For l = 1 To Len(Phrase)
            If Mid$(Phrase, l, 1) = " " Then
                NumPar = NumPar + 1
            Else
                aT(NumPar) = aT(NumPar) + Mid$(Phrase, l, 1)
            End If
        Next
        aP = ""
        lP = 0
        For l = 0 To NumPar
            OaP = aP
            If aP = "" Then
                aP = aT(l)
            Else
                aP = aP + " " + aT(l)
            End If
            If frmPrintPreview.Prv.TextWidth(aP) > Larg * mm Then
                aP = OaP
                PrintInLef X, Y + (Interl * lP), aP, Fname, Fsize, Fbold, ANTEPRIMA
                Ofs = Ofs + Interl
                aP = aT(l)
                lP = lP + 1
            End If
        Next
        PrintInLef X, Y + (Interl * lP), aP, Fname, Fsize, Fbold, ANTEPRIMA
        Ofs = Ofs + Interl
    Else
        PrintInLef X, Y, Phrase, Fname, Fsize, Fbold, ANTEPRIMA
        Ofs = Ofs + Interl
    End If

End Sub

Sub PrintJustS(X As Single, Y As Single, Phrase As String, Fname As String, Fsize As Integer, Fbold As Integer, Larg As Single, Dst As Integer)

    ReDim aT(500) As String
    Static NumPar As Integer
    Static aP As String
    Static OaP As String
    Static lP, l As Integer
    Static VecOfs As Single
    Static lStp As Single
    lStp = 0.3

    frmPrintPreview.Prv.FontName = Fname
    frmPrintPreview.Prv.FontSize = Fsize
    frmPrintPreview.Prv.FontBold = Fbold
    If frmPrintPreview.Prv.TextWidth(Phrase) > Larg * mm Then
        NumPar = 0
        For l = 1 To Len(Phrase)
            If Mid$(Phrase, l, 1) = " " Then
                NumPar = NumPar + 1
            Else
                aT(NumPar) = aT(NumPar) + Mid$(Phrase, l, 1)
            End If
        Next
        aP = ""
        lP = 0
        For l = 0 To NumPar
            OaP = aP
            If aP = "" Then
                aP = aT(l)
            Else
                aP = aP + " " + aT(l)
            End If
            If frmPrintPreview.Prv.TextWidth(aP) > Larg * mm Then
                aP = OaP
                PrintInLef X, Y + (lStp * lP), aP, Fname, Fsize, Fbold, ANTEPRIMA
                Ofs = Ofs + lStp
                aP = aT(l)
                lP = lP + 1
            End If
        Next
        PrintInLef X, Y + (lStp * lP), aP, Fname, Fsize, Fbold, ANTEPRIMA
        Ofs = Ofs + lStp
    Else
        PrintInLef X, Y, Phrase, Fname, Fsize, Fbold, ANTEPRIMA
        Ofs = Ofs + lStp
    End If

End Sub


Sub PrintInRig(X As Single, Y As Single, Phrase As String, Fname As String, Fsize As Integer, Fbold As Integer, Dst As Integer)

Static Tmp As String
Static Lungh As Single

    Lungh = frmPrintPreview.Prv.TextWidth(Phrase)
        
    TempPrint "#fontname"
    TempPrint Fname
    TempPrint "#fontsize"
    TempPrint Format(Fsize)
    TempPrint "#fontbold"
    TempPrint Format(Fbold)
    TempPrint "#y"
    TempPrint Format(Y * mm)
    TempPrint "#x"
    TempPrint Format(X * mm) ' - Lungh
    TempPrint "#txt_r"
    TempPrint Phrase

End Sub

Sub PrintInLef(X As Single, Y As Single, Phrase As String, Fname As String, Fsize As Integer, Fbold As Integer, Dst As Integer)

    TempPrint "#fontname"
    TempPrint Fname
    TempPrint "#fontsize"
    TempPrint Format(Fsize)
    TempPrint "#fontbold"
    TempPrint Format(Fbold)
    TempPrint "#y"
    TempPrint Format(Y * mm)
    TempPrint "#x"
    TempPrint Format(X * mm)
    TempPrint "#txt_l"
    TempPrint Phrase
    
End Sub

Sub PrintInCen(X As Single, Y As Single, Phrase As String, Fname As String, Fsize As Integer, Fbold As Integer, Dst As Integer)

    Static dX As Single
    Static tmpX As Single
'
'  X = coordinata orizzontale
'  Y = coordinata del centro della riga
'  Phrase = stringa da stampare
'
    

    dX = Int(frmPrintPreview.Prv.TextWidth(Phrase) / 2)
    tmpX = (X * mm) - (dX)

    If tmpX < 0 Then
        MsgBox "Error in coords!!!!", 16, "PrintInCen"
        Exit Sub
    End If

    TempPrint "#fontname"
    TempPrint Fname
    TempPrint "#fontsize"
    TempPrint Format(Fsize)
    TempPrint "#fontbold"
    TempPrint Format(Fbold)
    TempPrint "#y"
    TempPrint Format(Y * mm)
    TempPrint "#x"
    TempPrint Format(tmpX) '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    TempPrint "#txt_c"
    TempPrint Phrase

End Sub

Sub PrintCross(X As Single, Y As Single, X1 As Single, Y1 As Single, Dst As Integer)

    TempPrint "#fill"
    TempPrint "1"
    TempPrint "#color"
    TempPrint "0"
    TempPrint "#line"
    TempPrint Format(X * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X1 * mm)
    TempPrint Format(Y1 * mm)
    TempPrint ""
    TempPrint ""
    TempPrint "#line"
    TempPrint Format(X1 * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X * mm)
    TempPrint Format(Y1 * mm)
    TempPrint ""
    TempPrint ""


End Sub

Sub PrintBoxFill(X As Single, Y As Single, X1 As Single, Y1 As Single, MyCol As Long, Dst As Integer)

    TempPrint "#fill"
    TempPrint "1"
    TempPrint "#color"
    TempPrint "0"
    TempPrint "#line"
    TempPrint Format(X * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X1 * mm)
    TempPrint Format(Y1 * mm)
    TempPrint Format(MyCol)
    TempPrint "BF"

End Sub

Sub PrintBoxFill2(X As Single, Y As Single, X1 As Single, Y1 As Single, MyCol As Long, MyFil As Long, Dst As Integer)

    TempPrint "#fill"
    TempPrint Format(MyFil)
    TempPrint "#color"
    TempPrint Format(MyCol)
    TempPrint "#line"
    TempPrint Format(X * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X1 * mm)
    TempPrint Format(Y1 * mm)
    TempPrint ""
    TempPrint "B"

End Sub


Sub PrintBox(X As Single, Y As Single, X1 As Single, Y1 As Single, Dst As Integer)

    TempPrint "#fill"
    TempPrint "1"
    TempPrint "#color"
    TempPrint "0"
    TempPrint "#line"
    TempPrint Format(X * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X1 * mm)
    TempPrint Format(Y1 * mm)
    TempPrint ""
    TempPrint "B"

End Sub


Sub PrintImg(Nome As Control, X As Single, Y As Single, X1 As Single, Y1 As Single, Dst As Integer)

    Static RR As Single, RT As String, NI As String
    
    TempPrint "#img"
    RR = (899999 * Rnd) + 100000
    RT = Format(RR, "000000")
    NI = LocPerc + "_$$_" + RT + ".tmp"
    TempPrint NI
    SavePicture Nome, NI
    TempPrint Format(X * mm)
    TempPrint Format(Y * mm)
    TempPrint Format(X1 * mm)
    TempPrint Format(Y1 * mm)

End Sub

Sub SetA3()

    frmPrintPreview.Prv.Cls

    frmPrintPreview.Prv.Width = frmPrintPreview.Prv.Height * (29.7 / 42)
    frmPrintPreview.Prv.ScaleWidth = mm * 29.7
    frmPrintPreview.Prv.ScaleHeight = mm * 42
    
    Scala = frmPrintPreview.Prv.Height / frmPrintPreview.Prv.ScaleHeight

End Sub


Sub SetA4()

    frmPrintPreview.Prv.Cls

    frmPrintPreview.Prv.Width = frmPrintPreview.Prv.Height * (21 / 29.7)
    frmPrintPreview.Prv.ScaleWidth = mm * 21
    frmPrintPreview.Prv.ScaleHeight = mm * 29.7
    
    Scala = frmPrintPreview.Prv.Height / frmPrintPreview.Prv.ScaleHeight

End Sub


Sub SetB5()

    frmPrintPreview.Prv.Cls

    frmPrintPreview.Prv.Width = frmPrintPreview.Prv.Height * (15 / 21)
    frmPrintPreview.Prv.ScaleWidth = mm * 15
    frmPrintPreview.Prv.ScaleHeight = mm * 21
    
    Scala = frmPrintPreview.Prv.Height / frmPrintPreview.Prv.ScaleHeight

End Sub


Sub TempShow(X01 As Single, Y01 As Single, X02 As Single, Y02 As Single)

    frmPrintPreview.MousePointer = vbHourglass
    
    Static OldFill As Long, OldColo As Long
    Static l As Integer, Lung As Single, dX As Single, tmpX As Single
    Static pPnt As Integer, pRef As Integer
    pPnt = 0
    pRef = Val(frmPrintPreview.ePag.Text)
    Static A As String, B As String
    Static X As Single, Y As Single
    Static X1 As Single, Y1 As Single
    Static BoxColor As Long, BoxType As String
    
    frmPrintPreview.Prv.Cls
    'frmPrintPreview.Prv.Scale (X01, Y01)-(X02, Y02)
    frmPrintPreview.Prv.Left = (X01 * -1) + NM_PP_Ofs
    frmPrintPreview.Prv.Top = (Y01 * -1) + NM_PP_Ofs + frmPrintPreview.Cmd(0).Height
    Open LocPerc + LocName For Input As #26
        While Not EOF(26)
            Line Input #26, A
            If A = "#line" Then
                Line Input #26, A
                X = StripComma(A)
                Line Input #26, A
                Y = StripComma(A)
                Line Input #26, A
                X1 = StripComma(A)
                Line Input #26, A
                Y1 = StripComma(A)
                Line Input #26, A
                B = A
                BoxColor = StripComma(A)
                Line Input #26, A
                BoxType = A
                If pPnt = pRef Then
                    If B = "" And BoxType = "" Then
                        frmPrintPreview.Prv.Line (X, Y)-(X1, Y1)
                    ElseIf B <> "" Then
                        frmPrintPreview.Prv.Line (X, Y)-(X1, Y1), BoxColor, BF
                    Else
                        frmPrintPreview.Prv.Line (X, Y)-(X1, Y1), , B
                    End If
                End If
            ElseIf A = "#x" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.CurrentX = StripComma(A)
                End If
            ElseIf A = "#y" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.CurrentY = StripComma(A)
                End If
            ElseIf A = "#txt_c" Then
                Line Input #26, A
                If pPnt = pRef Then
                    dX = Int(frmPrintPreview.Prv.TextWidth(A) / 2)
                    tmpX = frmPrintPreview.Prv.CurrentX - dX
                    frmPrintPreview.Prv.Print A
                End If
            ElseIf A = "#txt_l" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.Print A
                End If
            ElseIf A = "#txt_r" Then
                Line Input #26, A
                If pPnt = pRef Then
                    Lung = frmPrintPreview.Prv.TextWidth(A)
                    frmPrintPreview.Prv.CurrentX = frmPrintPreview.Prv.CurrentX - Lung
                    frmPrintPreview.Prv.Print A
                End If
            ElseIf A = "#fontname" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.FontName = A
                End If
            ElseIf A = "#fontsize" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.FontSize = StripComma(A) * Scala
                End If
            ElseIf A = "#fontbold" Then
                Line Input #26, A
                If pPnt = pRef Then
                    If A = "0" Then
                        frmPrintPreview.Prv.FontBold = False
                    Else
                        frmPrintPreview.Prv.FontBold = True
                    End If
                End If
            ElseIf A = "#fill" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.FillStyle = CLng(Val(A))
                End If
            ElseIf A = "#color" Then
                Line Input #26, A
                If pPnt = pRef Then
                    frmPrintPreview.Prv.FillColor = CLng(Val(A))
                End If
            ElseIf A = "#img" Then
                Line Input #26, A
                If TempFileExists(A) = True Then
                    frmPrintPreview.Img.Picture = LoadPicture(A)
                End If
                Line Input #26, A
                X = StripComma(A)
                Line Input #26, A
                Y = StripComma(A)
                Line Input #26, A
                X1 = StripComma(A)
                Line Input #26, A
                Y1 = StripComma(A)
                If pPnt = pRef Then
                    frmPrintPreview.Prv.PaintPicture frmPrintPreview.Img.Picture, X, Y, X1, Y1
                End If
            ElseIf A = "#startpage" Then
                pPnt = pPnt + 1
            ElseIf A = "#endpage" Then
                If pPnt = pRef Then
                    GoTo BastaLeggere
                End If
            End If
        Wend
BastaLeggere:
    Close #26
    
    If frmPrintPreview.Prv.Width > frmPrintPreview.hBar.Width Then
        frmPrintPreview.hBar.Min = 0
        frmPrintPreview.hBar.Max = frmPrintPreview.Prv.Width - frmPrintPreview.hBar.Width
        frmPrintPreview.hBar.SmallChange = 20
        'frmPrintPreview.hBar.LargeChange = frmPrintPreview.hBar.Max / 10
        frmPrintPreview.hBar.LargeChange = (frmPrintPreview.hBar.Width * frmPrintPreview.hBar.Max) / frmPrintPreview.Prv.Width
    Else
        frmPrintPreview.hBar.Min = 0
        frmPrintPreview.hBar.Max = 0
    End If
    
    If frmPrintPreview.Prv.Height > frmPrintPreview.vBar.Height Then
        frmPrintPreview.vBar.Min = 0
        frmPrintPreview.vBar.Max = frmPrintPreview.Prv.Height - frmPrintPreview.vBar.Height
        frmPrintPreview.vBar.SmallChange = 20
        'frmPrintPreview.vBar.LargeChange = frmPrintPreview.vBar.Max / 10
        frmPrintPreview.vBar.LargeChange = (frmPrintPreview.vBar.Height * frmPrintPreview.vBar.Max) / frmPrintPreview.Prv.Height
    Else
        frmPrintPreview.vBar.Min = 0
        frmPrintPreview.vBar.Max = 0
    End If
    
    SistemaStatusBar
    
    frmPrintPreview.MousePointer = vbDefault
    
End Sub


Sub TempStampa(sP As Integer, eP As Integer)

    frmPrintPreview.MousePointer = vbHourglass
    frmPrint.Command2.Font.Bold = True
    DoEvents
    
    frmPrint.pBar.Width = 0
    
    Static DaStampare As Boolean
    DaStampare = False
    Static l As Integer, Lung As Single, dX As Single, tmpX As Single
    Static pPnt As Integer, pRef As Integer
    pPnt = 0
    pRef = Val(frmPrintPreview.ePag.Text)
    Static A As String, B As String
    Static X As Single, Y As Single
    Static X1 As Single, Y1 As Single
    Static BoxColor As Long, BoxType As String
    
    Open LocPerc + LocName For Input As #26
        While Not EOF(26)
            Line Input #26, A
            If A = "#line" Then
                Line Input #26, A
                X = StripComma(A)
                Line Input #26, A
                Y = StripComma(A)
                Line Input #26, A
                X1 = StripComma(A)
                Line Input #26, A
                Y1 = StripComma(A)
                Line Input #26, A
                B = A
                BoxColor = StripComma(A)
                Line Input #26, A
                BoxType = A
                If DaStampare = True Then
                    If B = "" And BoxType = "" Then
                        Printer.Line (X, Y)-(X1, Y1)
                    ElseIf B <> "" Then
                        Printer.Line (X, Y)-(X1, Y1), BoxColor, BF
                    Else
                        Printer.Line (X, Y)-(X1, Y1), , B
                    End If
                End If
            ElseIf A = "#x" Then
                Line Input #26, A
                If DaStampare = True Then
                    Printer.CurrentX = StripComma(A)
                End If
            ElseIf A = "#y" Then
                Line Input #26, A
                If DaStampare = True Then
                    Printer.CurrentY = StripComma(A)
                End If
            ElseIf A = "#txt_c" Then
                Line Input #26, A
                If DaStampare = True Then
                    dX = Int(Printer.TextWidth(A) / 2)
                    tmpX = Printer.CurrentX - dX
                    Printer.Print A
                End If
            ElseIf A = "#txt_l" Then
                Line Input #26, A
                If DaStampare = True Then
                    Printer.Print A
                End If
            ElseIf A = "#txt_r" Then
                Line Input #26, A
                If DaStampare = True Then
                    Lung = Printer.TextWidth(A)
                    Printer.CurrentX = Printer.CurrentX - Lung
                    Printer.Print A
                End If
            ElseIf A = "#fontname" Then
                Line Input #26, A
                If DaStampare = True Then
                    Printer.FontName = A
                End If
            ElseIf A = "#fontsize" Then
                Line Input #26, A
                If DaStampare = True Then
                    Printer.FontSize = StripComma(A)
                End If
            ElseIf A = "#fontbold" Then
                Line Input #26, A
                If DaStampare = True Then
                    If A = "0" Then
                        Printer.FontBold = False
                    Else
                        Printer.FontBold = True
                    End If
                End If
            ElseIf A = "#fill" Then
                Line Input #26, A
                If pPnt = pRef Then
                    Printer.FillStyle = CLng(Val(A))
                End If
            ElseIf A = "#color" Then
                Line Input #26, A
                If pPnt = pRef Then
                    Printer.FillColor = CLng(Val(A))
                End If
            ElseIf A = "#img" Then
                Line Input #26, A
                If TempFileExists(A) = True Then
                    frmPrintPreview.Img.Picture = LoadPicture(A)
                End If
                Line Input #26, A
                X = StripComma(A)
                Line Input #26, A
                Y = StripComma(A)
                Line Input #26, A
                X1 = StripComma(A)
                Line Input #26, A
                Y1 = StripComma(A)
                If DaStampare = True Then
                    Printer.PaintPicture frmPrintPreview.Img.Picture, X, Y, X1, Y1
                End If
            ElseIf A = "#startpage" Then
                If NM_AnnullaStampa = True Then GoTo BastaLeggere
                pPnt = pPnt + 1
                If pPnt > eP Then
                    GoTo BastaLeggere
                ElseIf pPnt >= sP And pPnt <= eP Then
                    DaStampare = True
                    SistemaBarra sP, eP, pPnt
                    DoEvents
                ElseIf pPnt < sP Then
                    DaStampare = False
                End If
            ElseIf A = "#endpage" Then
                If NM_AnnullaStampa = True Then GoTo BastaLeggere
                If pPnt >= eP Then GoTo BastaLeggere
                If DaStampare = True Then Printer.NewPage
            End If
        Wend
BastaLeggere:
    Printer.EndDoc
    Close #26
    
    frmPrint.Command2.Font.Bold = False
    frmPrintPreview.MousePointer = vbDefault
    
End Sub

Public Sub InitTestStatisticsPrintOut()
  Dim i As Integer
  Dim TempStr1 As String * 30
  Dim TempStr2 As String * 15
  Dim TempStr3 As String * 30
  Dim TempStr4 As String * 15
  Dim TempStr5 As String * 90
  
  frmPrintHidden.List1.Clear
  
  With frmPrintHidden
    'Rad 1
    TempStr1 = "Total vinst"
    TempStr2 = FormatCurrency(IndicatorTestStatistics.TotalNetProfit, 2)
    TempStr3 = "�ppen position"
    frmIndicatortestResult.StatisticsGrid.Row = 0
    frmIndicatortestResult.StatisticsGrid.Col = 4
    TempStr4 = frmIndicatortestResult.StatisticsGrid.Text
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 2
    TempStr1 = "Avkastning"
    TempStr2 = FormatNumber(IndicatorTestStatistics.PercentGainLoss * 100, 2) & " %"
    TempStr3 = "Startdatum"
    TempStr4 = CDate(Int(IndicatorTestOptions.StartDate))
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 3
    TempStr1 = "Startkapital"
    TempStr2 = FormatCurrency(IndicatorTestStatistics.InitialInvestment, 2)
    TempStr3 = "Slutdatum"
    TempStr4 = CDate(Int(IndicatorTestOptions.StopDate))
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 4
    .List1.AddItem ""
    
    'Rad 5
    TempStr1 = "Nuvarande position"
    frmIndicatortestResult.StatisticsGrid.Row = 4
    frmIndicatortestResult.StatisticsGrid.Col = 1
    TempStr2 = frmIndicatortestResult.StatisticsGrid.Text
    TempStr3 = "Position tagen"
    frmIndicatortestResult.StatisticsGrid.Col = 4
    TempStr4 = frmIndicatortestResult.StatisticsGrid.Text
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 6
    .List1.AddItem ""
    
    'Rad 7
    TempStr1 = "K�p och beh�ll vinst"
    TempStr2 = FormatCurrency(IndicatorTestStatistics.BuyHoldProfit, 2)
    TempStr3 = "Antal testdagar"
    TempStr4 = FormatNumber(IndicatorTestStatistics.DaysInTest)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 8
    TempStr1 = "K�p och beh�ll"
    TempStr2 = FormatNumber(IndicatorTestStatistics.BuyHoldPercentageProfit, 2) & " %"
    TempStr3 = "�rlig k�p & beh�ll avkastning"
    TempStr4 = FormatNumber(IndicatorTestStatistics.AnnualBuyHoldPercentage, 2) & " %"
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 9
    TempStr1 = "Antal avslutade aff�rer"
    TempStr2 = FormatNumber(IndicatorTestStatistics.TotalClosedTrades, 0)
    TempStr3 = "Totalt courtage"
    TempStr4 = FormatCurrency(IndicatorTestStatistics.CommissionsPaid, 2)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 10
    TempStr1 = "Medelavkastning per aff�r"
    TempStr2 = FormatCurrency(IndicatorTestStatistics.AverageProfitPerTrade, 2)
    TempStr3 = "Vinst/f�rlust f�rh�llande"
    TempStr4 = FormatNumber(IndicatorTestStatistics.AverageWinLossRatio, 2)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 11
    TempStr1 = "Antal l�nga aff�rer"
    TempStr2 = FormatNumber(IndicatorTestStatistics.TotalLongTrades, 0)
    TempStr3 = "Antal korta positioner"
    TempStr4 = FormatNumber(IndicatorTestStatistics.TotalShortTrades, 0)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 12
    TempStr1 = "Antal l�nga vinstaff�rer"
    TempStr2 = FormatNumber(IndicatorTestStatistics.WinningLongTrades, 0)
    TempStr3 = "Antal korta vinstaff�rer"
    TempStr4 = FormatNumber(IndicatorTestStatistics.WinningShortTrades, 0)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 13
    .List1.AddItem ""
    
    'Rad 14
    TempStr1 = "Antal vinstaff�rer"
    TempStr2 = FormatNumber(IndicatorTestStatistics.TotalWinningTrades, 0)
    TempStr3 = "Antal f�rlustaff�rer"
    TempStr4 = FormatNumber(IndicatorTestStatistics.TotalLosingTrades, 0)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 15
    TempStr1 = "Resultat vinstaff�rer"
    TempStr2 = FormatPercent(IndicatorTestStatistics.AmountOfWinningTrades / 100, 2)
    TempStr3 = "Resultat f�rlustaff�rer"
    TempStr4 = FormatPercent(IndicatorTestStatistics.AmountOfLosingTrades / 100, 2)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 16
    TempStr1 = "Medelvinst"
    TempStr2 = FormatPercent(IndicatorTestStatistics.AverageWin / 100, 2)
    TempStr3 = "Medelf�rlust"
    TempStr4 = FormatPercent(IndicatorTestStatistics.AverageLoss / 100, 2)
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 17
    .List1.AddItem ""
    
    'Rad 18
    TempStr1 = "Antal dagar utan position"
    TempStr2 = FormatNumber(IndicatorTestStatistics.TotalBarsOut)
    TempStr3 = ""
    TempStr4 = ""
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 19
    .List1.AddItem ""
    
    'Rad 20
    TempStr1 = "Vinst/f�rlust index"
    TempStr2 = FormatNumber(IndicatorTestStatistics.ProfitLossIndex, 2)
    TempStr3 = ""
    TempStr4 = ""
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
    'Rad 21
    TempStr1 = "K�p och beh�ll index"
    TempStr2 = FormatNumber(IndicatorTestStatistics.BuyHoldIndex, 2)
    TempStr3 = ""
    TempStr4 = ""
    TempStr5 = TempStr1 + TempStr2 + TempStr3 + TempStr4
    .List1.AddItem TempStr5
    
  End With
   
  ' local variables
  Static l As Integer, rPnt As Integer, A As String
  rPnt = 1
 
  ' initialize preview engine
  TempInit
    
  ' page 1
  ' start with a new page
  TempStr1 = Trim(Date)
  PrintHeader "Trading Session", TempStr1, 0
  ' only to print something...
  PrintInCen 9, 1.5, "Indikatortest", "Arial", 12, True, 0
  For l = 0 To frmPrintHidden.List1.ListCount - 1
    PrintInLef 2, 2 + (0.5 * l), frmPrintHidden.List1.List(l), "Courier New", 8, False, 0
  Next
  ' close open page
  PrintFooter "Trading Session", "1", 0
    
End Sub



