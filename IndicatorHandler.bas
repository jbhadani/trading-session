Attribute VB_Name = "IndicatorHandler"
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  ExpMAV
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the MAV indicator.
'Written:       970406 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ExpMAV(WhichIndicator As Integer, ScaleGraph As Integer)
  Dim Length1, Length2 As Integer
  If Frm_Parameters.Option_OneMAV = True Then
    ReDim MAV1(PriceDataArraySize) As Single
    ReDim MAV2(PriceDataArraySize) As Single
    Length1 = Val(Frm_Parameters.Para_MAV_Period1.Text)
    Length2 = Val(Frm_Parameters.Para_MAV_Period2.Text)
    If Length1 > PriceDataArraySize Then
      ErrorMessageStr "Too few records"
    Else
      ExpMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
      MAVLabels Length1, Length2, MAV1(), MAV2(), WhichIndicator, ScaleGraph
      PutDataInIndicatorOneLine MAV1(), WhichIndicator, ScaleGraph
    End If
  Else
    ReDim MAV1(PriceDataArraySize) As Single
    ReDim MAV2(PriceDataArraySize) As Single
    Length1 = Val(Frm_Parameters.Para_MAV_Period1.Text)
    Length2 = Val(Frm_Parameters.Para_MAV_Period2.Text)
    If Length1 > PriceDataArraySize Or Length2 > PriceDataArraySize Then
      ErrorMessageStr "Too few records"
    Else
      ExpMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
      ExpMAVCalc PriceDataArray(), MAV2(), Length2, PriceDataArraySize
      MAVLabels Length1, Length2, MAV1(), MAV2(), WhichIndicator, ScaleGraph
      PutDataInIndicatorTwoLines MAV1(), MAV2(), WhichIndicator, ScaleGraph
    End If
  End If
  If WhichIndicator = 1 Then
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  MACD
'Input:         WhichIndicator
'Output:
'Description:   This procedure controls the calculation and
'               plotting of the MACD indicator.
'Written:       970414 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MACD(WhichIndicator As Integer, ScaleGraph As Integer)
  ReDim MACDArray(PriceDataArraySize) As Single
  ReDim MACDSignal(PriceDataArraySize) As Single
  If PriceDataArraySize < 12 Then
    ErrorMessageStr "Too few records"
  End If
  If WhichIndicator = 1 Then
    MACDCalc PriceDataArray(), MACDArray(), MACDSignal(), PriceDataArraySize
    If Frm_Parameters.Option_MACDSignal = True Then
      PutDataInIndicatorTwoLines MACDArray(), MACDSignal(), 1, ScaleGraph
      MACDLabels MACDArray, WhichIndicator
      Frm_Graph.Indicator1.DrawMode = 3
    Else
      PutDataInIndicatorOneLine MACDArray(), 1, ScaleGraph
      MACDLabels MACDArray, WhichIndicator
      Frm_Graph.Indicator1.DrawMode = 3
    End If
  Else
    MACDCalc PriceDataArray(), MACDArray(), MACDSignal(), PriceDataArraySize
    If Frm_Parameters.Option_MACDSignal = True Then
      PutDataInIndicatorTwoLines MACDArray(), MACDSignal(), 2, ScaleGraph
      MACDLabels MACDArray, WhichIndicator
      Frm_Graph.Indicator2.DrawMode = 3
    Else
      PutDataInIndicatorOneLine MACDArray(), 2, ScaleGraph
      MACDLabels MACDArray, WhichIndicator
      Frm_Graph.Indicator2.DrawMode = 3
    End If
  End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  MoneyFlowIndex
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the MFI indicator.
'Written:       970411 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MoneyFlowIndex(WhichIndicator As Integer, ScaleGraph As Integer)
  Dim Length As Integer
  Length = 14
  Dim MFIArray() As Single
  ReDim MFIArray(PriceDataArraySize) As Single
  MFICalc PriceDataArray(), MFIArray(), Length, PriceDataArraySize
  If WhichIndicator = 1 Then
    PutDataInIndicatorOneLine MFIArray(), 1, ScaleGraph
    MFILabels WhichIndicator
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    PutDataInIndicatorOneLine MFIArray(), 2, ScaleGraph
    MFILabels WhichIndicator
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  Momentum
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the Momentum indicator.
'Written:       970406 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub Momentum(WhichIndicator As Integer, ScaleGraph As Integer)
  Dim Length As Integer
  Dim MAV() As Single
  Dim MOMArray() As Single
  ReDim MAV(PriceDataArraySize)
  ReDim MOMArray(PriceDataArraySize)
  Length = Frm_Parameters.HScroll5.Value
  If PriceDataArraySize < Length Then
    ErrorMessageStr "Too few Records"
  End If
  MomentumCalc PriceDataArray(), MOMArray, Length, PriceDataArraySize
  If Frm_Parameters.Option_ShowMOMMAV.Value = True Then
    Length = Frm_Parameters.HScroll9.Value
    ExpCalc MOMArray(), MAV(), Length, PriceDataArraySize
    If WhichIndicator = 1 Then
      MOMLabels MOMArray(), WhichIndicator
      PutDataInIndicatorTwoLines MOMArray(), MAV(), WhichIndicator, ScaleGraph
      Frm_Graph.Indicator1.DrawMode = 3
    Else
      MOMLabels MOMArray(), WhichIndicator
      PutDataInIndicatorTwoLines MOMArray(), MAV(), WhichIndicator, ScaleGraph
      Frm_Graph.Indicator2.DrawMode = 3
    End If
  Else
    If WhichIndicator = 1 Then
      MOMLabels MOMArray(), WhichIndicator
      PutDataInIndicatorOneLine MOMArray(), WhichIndicator, ScaleGraph
      Frm_Graph.Indicator1.DrawMode = 3
    Else
      MOMLabels MOMArray(), WhichIndicator
      PutDataInIndicatorOneLine MOMArray(), WhichIndicator, ScaleGraph
      Frm_Graph.Indicator2.DrawMode = 3
    End If
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  PriceOscillator
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the PriceOscillator indicator.
'Written:       970411 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub PriceOscillator(WhichIndicator As Integer, ScaleGraph As Integer)
  ReDim MAV1(PriceDataArraySize) As Single
  ReDim MAV2(PriceDataArraySize) As Single
  ReDim PriceOsc(PriceDataArraySize) As Single
  Dim Length1 As Integer
  Dim Length2 As Integer
  Length1 = Frm_Parameters.HScroll6.Value
  Length2 = Frm_Parameters.HScroll7.Value
  If Length1 > PriceDataArraySize Or Length2 > PriceDataArraySize Then
    ErrorMessageStr "Too few Records"
  End If
  If Frm_Parameters.Option_PriceOsc_Simple = True Then
    SimpleMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
    SimpleMAVCalc PriceDataArray(), MAV2(), Length2, PriceDataArraySize
  Else
    ExpMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
    ExpMAVCalc PriceDataArray(), MAV2(), Length2, PriceDataArraySize
  End If
  For i = 1 To PriceDataArraySize
    PriceOsc(i) = ((MAV1(i) - MAV2(i)) / (MAV1(i))) * 100
  Next i
  If WhichIndicator = 1 Then
    PutDataInIndicatorOneLine PriceOsc(), WhichIndicator, ScaleGraph
    PriceOscLabels PriceOsc(), WhichIndicator
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    PutDataInIndicatorOneLine PriceOsc(), WhichIndicator, ScaleGraph
    PriceOscLabels PriceOsc(), WhichIndicator
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  RSI
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the RSI indicator.
'Written:       970328 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RSI(WhichIndicator As Integer, ScaleGraph As Integer)
  ReDim RSIArray(PriceDataArraySize) As Single
  
  If Frm_Parameters.Option_ShowRSI.Value = True Then
    RSICalc PriceDataArray(), RSIArray(), PriceDataArraySize
    PutDataInIndicatorOneLine RSIArray(), WhichIndicator, ScaleGraph
    RSILabels WhichIndicator, ScaleGraph
  Else
    RSICalc PriceDataArray(), RSIArray(), PriceDataArraySize
    ReDim RSIMAVArray(PriceDataArraySize) As Single
    Length = Frm_Parameters.HScroll4.Value
    MAVCalc RSIArray(), RSIMAVArray(), Length
    RSILabels WhichIndicator, ScaleGraph
    PutDataInIndicatorTwoLines RSIArray(), RSIMAVArray(), WhichIndicator, ScaleGraph
  End If
  If WhichIndicator = 1 Then
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  SimpleMAV
'Input:         WhichIndicator
'Output:
'Description:   This procedaure controls the calculation and
'               plotting of the MAV indicator.
'Written:       970328 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SimpleMAV(WhichIndicator As Integer, ScaleGraph As Integer)
  Dim Length1, Length2, NbrOfLines As Integer
  
  If Frm_Parameters.Option_OneMAV = True Then
    ReDim MAV1(PriceDataArraySize) As Single
    ReDim MAV2(PriceDataArraySize) As Single
    Length1 = Val(Frm_Parameters.Para_MAV_Period1.Text)
    Length2 = Val(Frm_Parameters.Para_MAV_Period2.Text)
    
    If Length1 > PriceDataArraySize Then
      ErrorMessageStr "Too few records"
    Else
      SimpleMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
      MAVLabels Length1, Length2, MAV1(), MAV2(), WhichIndicator, ScaleGraph
      PutDataInIndicatorOneLine MAV1(), WhichIndicator, ScaleGraph
    End If
  Else
    ReDim MAV1(PriceDataArraySize) As Single
    ReDim MAV2(PriceDataArraySize) As Single
    Length1 = Val(Frm_Parameters.Para_MAV_Period1.Text)
    Length2 = Val(Frm_Parameters.Para_MAV_Period2.Text)
    If Length1 > PriceDataArraySize Or Length2 > PriceDataArraySize Then
      ErrorMessageStr "Too few records"
    Else
      SimpleMAVCalc PriceDataArray(), MAV1(), Length1, PriceDataArraySize
      SimpleMAVCalc PriceDataArray(), MAV2(), Length2, PriceDataArraySize
      MAVLabels Length1, Length2, MAV1(), MAV2(), WhichIndicator, ScaleGraph
      PutDataInIndicatorTwoLines MAV1(), MAV2(), WhichIndicator, ScaleGraph
    End If
  End If
  If WhichIndicator = 1 Then
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'ProcedurName:  StochasticOsc
'Input:         WhichIndicator
'Output:
'Description:   This proecdure controls the calculation and
'               plotting of the Stochastic indicator.
'Written:       970328 by Martin Lindberg
'Modified:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub StochasticOsc(WhichIndicator As Integer, ScaleGraph As Integer)
Dim Kper As Integer
Dim Dper As Integer
ReDim StochasticOscArrayK(PriceDataArraySize) As Single
ReDim StochasticOscArrayD(PriceDataArraySize) As Single

Kper = Frm_Parameters.HScroll1.Value
Dper = Frm_Parameters.HScroll2.Value

  StochasticOscCalc PriceDataArray(), StochasticOscArrayK(), Kper, PriceDataArraySize
    If WhichIndicator = 1 Then
      Frm_Graph.Indicator1.YAxisMax = 100
      Frm_Graph.Indicator1.YAxisMin = 0
    Else
      Frm_Graph.Indicator2.YAxisMax = 100
      Frm_Graph.Indicator2.YAxisMin = 0
    End If
  If Frm_Parameters.Option_ShowOsc.Value = True Then
    PutDataInIndicatorOneLine StochasticOscArrayK(), WhichIndicator, ScaleGraph
    StochasticLabels Kper, Dper, WhichIndicator, ScaleGraph
  Else
    MAVCalc StochasticOscArrayK(), StochasticOscArrayD(), Dper
    PutDataInIndicatorTwoLines StochasticOscArrayK(), StochasticOscArrayD(), WhichIndicator, ScaleGraph
    StochasticLabels Kper, Dper, WhichIndicator, ScaleGraph
  End If
  If WhichIndicator = 1 Then
    Frm_Graph.Indicator1.DrawMode = 3
  Else
    Frm_Graph.Indicator2.DrawMode = 3
  End If
End Sub


