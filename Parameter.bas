Attribute VB_Name = "Parameter"

Sub GetParameterSettingsFromInvest(Index As Long)
If Index > 0 And Index <= WorkSpaceSettingsArraySize Then
With ParameterSettings
        '.COPDist = WorkspaceSettingsArray(Index).ParameterSettings.COPDist
        '.COPMAV = WorkspaceSettingsArray(Index).ParameterSettings.COPMAV
        '.COPSUM = WorkspaceSettingsArray(Index).ParameterSettings.COPSUM
        .MACDMAV = WorkspaceSettingsArray(Index).ParameterSettings.MACDMAV
        .MAV1 = WorkspaceSettingsArray(Index).ParameterSettings.MAV1
        .MAV2 = WorkspaceSettingsArray(Index).ParameterSettings.MAV2
        .MAVUseExp = WorkspaceSettingsArray(Index).ParameterSettings.MAVUseExp
        .MAVMFI = WorkspaceSettingsArray(Index).ParameterSettings.MAVMFI
        .MAVMOM = WorkspaceSettingsArray(Index).ParameterSettings.MAVMOM
        .MAVNbr = WorkspaceSettingsArray(Index).ParameterSettings.MAVNbr
        .MAVRSI = WorkspaceSettingsArray(Index).ParameterSettings.MAVRSI
        .MAVSTO = WorkspaceSettingsArray(Index).ParameterSettings.MAVSTO
        .MFILength = WorkspaceSettingsArray(Index).ParameterSettings.MFILength
        .MFIShowMAV = WorkspaceSettingsArray(Index).ParameterSettings.MFIShowMAV
        .MFISignalUseMAV = WorkspaceSettingsArray(Index).ParameterSettings.MFISignalUseMAV
        .MOM = WorkspaceSettingsArray(Index).ParameterSettings.MOM
        .PlotMOMandMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotMOMandMAV
        .MOMSignalUseMAV = WorkspaceSettingsArray(Index).ParameterSettings.MOMSignalUseMAV
        .MOMSignalUseZeroLine = WorkspaceSettingsArray(Index).ParameterSettings.MOMSignalUseZeroLine
        .OSCMAVExp = WorkspaceSettingsArray(Index).ParameterSettings.OSCMAVExp
        .PRIOSC1 = WorkspaceSettingsArray(Index).ParameterSettings.PRIOSC1
        .PRIOSC2 = WorkspaceSettingsArray(Index).ParameterSettings.PRIOSC2
        .RSI = WorkspaceSettingsArray(Index).ParameterSettings.RSI
        .PlotRSIandMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotRSIandMAV
        .RSISignalUseMAV = WorkspaceSettingsArray(Index).ParameterSettings.RSISignalUseMAV
        .STO = WorkspaceSettingsArray(Index).ParameterSettings.STO
        .STOSlowing = WorkspaceSettingsArray(Index).ParameterSettings.STOSlowing
        .PlotSTOandMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotSTOandMAV
        .STOSignalUseSignalLevels = WorkspaceSettingsArray(Index).ParameterSettings.STOSignalUseSignalLevels
        .AccelerationFactor = WorkspaceSettingsArray(Index).ParameterSettings.AccelerationFactor
        .BollingerMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.BollingerMAVLength
        .BollingerSTDFactor = WorkspaceSettingsArray(Index).ParameterSettings.BollingerSTDFactor
        .AverageTrueRangeLength = WorkspaceSettingsArray(Index).ParameterSettings.AverageTrueRangeLength
        .ChaikinVolatility = WorkspaceSettingsArray(Index).ParameterSettings.ChaikinVolatility
        .ChaikinVolatilityROC = WorkspaceSettingsArray(Index).ParameterSettings.ChaikinVolatilityROC
        .DetrendedPriceOscillatorLength = WorkspaceSettingsArray(Index).ParameterSettings.DetrendedPriceOscillatorLength
        .EaseOfMovementLength = WorkspaceSettingsArray(Index).ParameterSettings.EaseOfMovementLength
        .EaseOfMovementMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.EaseOfMovementMAVLength
        .PlotEaseOfMovementAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotEaseOfMovementAndMAV
        .NegativeVolumeMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.NegativeVolumeMAVLength
        .PlotNegativeVolumeAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotNegativeVolumeAndMAV
        .PositiveVolumeMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.PositiveVolumeMAVLength
        .PlotPositiveVolumeAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotPositiveVolumeAndMAV
        .RateOfChangeLength = WorkspaceSettingsArray(Index).ParameterSettings.RateOfChangeLength
        .RateOfChangeMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.RateOfChangeMAVLength
        .PlotRateOfChangeAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotRateOfChangeAndMAV
        .StandardDeviationLength = WorkspaceSettingsArray(Index).ParameterSettings.StandardDeviationLength
        .StandardDeviationMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.StandardDeviationMAVLength
        .PlotStandardDeviationAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotStandardDeviationAndMAV
        .TRIXLength = WorkspaceSettingsArray(Index).ParameterSettings.TRIXLength
        .TRIXMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.TRIXMAVLength
        .PlotTRIXAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotTRIXAndMAV
        .VolumeOscillatorMAV1Length = WorkspaceSettingsArray(Index).ParameterSettings.VolumeOscillatorMAV1Length
        .VolumeOscillatorMAV2Length = WorkspaceSettingsArray(Index).ParameterSettings.VolumeOscillatorMAV2Length
        .VolumeRateOfChangeLength = WorkspaceSettingsArray(Index).ParameterSettings.VolumeRateOfChangeLength
        .MFILowerSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.MFILowerSignalLevel
        .MFIUpperSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.MFIUpperSignalLevel
        .RSILowerSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.RSILowerSignalLevel
        .RSIUpperSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.RSIUpperSignalLevel
        .STOLowerSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.STOLowerSignalLevel
        .STOUpperSignalLevel = WorkspaceSettingsArray(Index).ParameterSettings.STOUpperSignalLevel
        .VolatilityMAVLength = WorkspaceSettingsArray(Index).ParameterSettings.VolatilityMAVLength
        .Volatility = WorkspaceSettingsArray(Index).ParameterSettings.Volatility
        .PlotVolatilityAndMAV = WorkspaceSettingsArray(Index).ParameterSettings.PlotVolatilityAndMAV
        End With
Else
  ErrorMessageStr "Kunde inte h�mta objektberoende parametrar"
End If
End Sub
Sub SaveParameterSettingsToRegistry()
With ParameterSettings
    SaveSetting App.Title, "Indicator", "MAV1", Str$(.MAV1)
    SaveSetting App.Title, "Indicator", "MAV2", Str$(.MAV2)
    SaveSetting App.Title, "Indicator", "RSI", Str$(.RSI)
    SaveSetting App.Title, "Indicator", "MAVRSI", Str$(.MAVRSI)
    SaveSetting App.Title, "Indicator", "STO", Str$(.STO)
    SaveSetting App.Title, "Indicator", "STOSlowing", Str$(.STOSlowing)
    SaveSetting App.Title, "Indicator", "MAVSTO", Str$(.MAVSTO)
    SaveSetting App.Title, "Indicator", "MOM", Str$(.MOM) ' �ndrad i 1.0.6 till .MOM fr�n .MAVMOM
    SaveSetting App.Title, "Indicator", "MAVMOM", Str$(.MAVMOM)
    SaveSetting App.Title, "Indicator", "PRIOSC1", Str$(.PRIOSC1)
    SaveSetting App.Title, "Indicator", "PRIOSC2", Str$(.PRIOSC2)
    SaveSetting App.Title, "Indicator", "AF", Str$(.AccelerationFactor)
    SaveSetting App.Title, "Indicator", "BollingerMAV", Str$(.BollingerMAVLength)
    SaveSetting App.Title, "Indicator", "BollingerSTD", Str$(.BollingerSTDFactor)
    
    If .MAVUseExp Then
      SaveSetting App.Title, "Indicator", "MAVUseExp", "True"
    Else
      SaveSetting App.Title, "Indicator", "MAVUseExp", "False"
    End If
    
    SaveSetting App.Title, "Indicator", "MAVNbr", Str$(.MAVNbr)
    SaveSetting App.Title, "Indicator", "RSIM", Str$(.PRIOSC2)
    SaveSetting App.Title, "Indicator", "MAVNbr", Str$(.MAVNbr)
    
    If .PlotRSIandMAV Then
      SaveSetting App.Title, "Indicator", "PlotRSIandMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotRSIandMAV", "False"
    End If
    If .RSISignalUseMAV Then
      SaveSetting App.Title, "Indicator", "RSISignalUseMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "RSISignalUseMAV", "False"
    End If
    If .PlotSTOandMAV Then
      SaveSetting App.Title, "Indicator", "PlotSTOandMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotSTOandMAV", "False"
    End If
    
    If .STOSignalUseSignalLevels Then
      SaveSetting App.Title, "Indicator", "STOSignalUseSignalLevels", "True"
    Else
      SaveSetting App.Title, "Indicator", "STOSignalUseSignalLevels", "False"
    End If
    
    If .PlotMOMandMAV Then
      SaveSetting App.Title, "Indicator", "PlotMOMandMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotMOMandMAV", "False"
    End If
    
    If .OSCMAVExp Then
      SaveSetting App.Title, "Indicator", "OSCMAVExp", "True"
    Else
      SaveSetting App.Title, "Indicator", "OSCMAVExp", "False"
    End If
    
    If .MACDMAV Then
      SaveSetting App.Title, "Indicator", "MACDMav", "True"
    Else
      SaveSetting App.Title, "Indicator", "MACDMav", "False"
    End If
    
    If .MOMSignalUseMAV Then
      SaveSetting App.Title, "Indicator", "MOMSignalUseMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "MOMSignalUseMAV", "False"
    End If
    
    If .MOMSignalUseZeroLine Then
      SaveSetting App.Title, "Indicator", "MOMSignalUseZeroLine", "True"
    Else
      SaveSetting App.Title, "Indicator", "MOMSignalUseZeroLine", "False"
    End If
    
    If .MFIShowMAV Then
      SaveSetting App.Title, "Indicator", "MFIShowMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "MFIShowMAV", "False"
    End If
    
    SaveSetting App.Title, "Indicator", "MFILength", Str$(.MFILength)
    SaveSetting App.Title, "Indicator", "MAVMFI", Str$(.MAVMFI)
    
    If .MFISignalUseMAV Then
      SaveSetting App.Title, "Indicator", "MFISignalUseMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "MFISignalUseMAV", "False"
    End If
    
    SaveSetting App.Title, "Indicator", "MFILenghth", Str$(.COPMAV)
    'SaveSetting App.Title, "Indicator", "COPDist", Str$(.COPDist)
    'SaveSetting App.Title, "Indicator", "COPSUM", Str$(.COPSUM)
    'SaveSetting App.Title, "Indicator", "COPMAV", Str$(.COPMAV)
    
    SaveSetting App.Title, "Indicator", "AverageTrueRangeLenth", Str$(.AverageTrueRangeLength)
    SaveSetting App.Title, "Indicator", "Chaikin's Volatility", Str$(.ChaikinVolatility)
    SaveSetting App.Title, "Indicator", "Chaikin's Volatility ROC", Str$(.ChaikinVolatilityROC)
    SaveSetting App.Title, "Indicator", "DetrendedPriceOscillatorLength", Str$(.DetrendedPriceOscillatorLength)
    SaveSetting App.Title, "Indicator", "EaseOfMovementLength", Str$(.EaseOfMovementLength)
    SaveSetting App.Title, "Indicator", "EaseOfMovementMAVLength", Str$(.EaseOfMovementMAVLength)
    SaveSetting App.Title, "Indicator", "NegativeVolumeMAVLength", Str$(.NegativeVolumeMAVLength)
    SaveSetting App.Title, "Indicator", "PositiveVolumeMAVLength", Str$(.PositiveVolumeMAVLength)
    SaveSetting App.Title, "Indicator", "RateOfChangeLength", Str$(.RateOfChangeLength)
    SaveSetting App.Title, "Indicator", "RateOfChangeMAVLength", Str$(.RateOfChangeMAVLength)
    SaveSetting App.Title, "Indicator", "StandardDeviationLength", Str$(.StandardDeviationLength)
    SaveSetting App.Title, "Indicator", "StandardDeviationMAVLength", Str$(.StandardDeviationMAVLength)
    SaveSetting App.Title, "Indicator", "TRIXLength", Str$(.TRIXLength)
    SaveSetting App.Title, "Indicator", "TRIXMAVLength", Str$(.TRIXMAVLength)
    SaveSetting App.Title, "Indicator", "VolumeOscillatorMAV1Length", Str$(.VolumeOscillatorMAV1Length)
    SaveSetting App.Title, "Indicator", "VolumeOscillatorMAV2Length", Str$(.VolumeOscillatorMAV2Length)
    SaveSetting App.Title, "Indicator", "VolumeRateOfChangeLength", Str$(.VolumeRateOfChangeLength)
    
    SaveSetting App.Title, "Indicator", "MFILowerSignalLevel", Str$(.MFILowerSignalLevel)
    SaveSetting App.Title, "Indicator", "MFIUpperSignalLevel", Str$(.MFIUpperSignalLevel)
    SaveSetting App.Title, "Indicator", "RSILowerSignalLevel", Str$(.RSILowerSignalLevel)
    SaveSetting App.Title, "Indicator", "RSIUpperSignalLevel", Str$(.RSIUpperSignalLevel)
    SaveSetting App.Title, "Indicator", "STOLowerSignalLevel", Str$(.STOLowerSignalLevel)
    SaveSetting App.Title, "Indicator", "STOUpperSignalLevel", Str$(.STOUpperSignalLevel)
    
    SaveSetting App.Title, "Indicator", "VolatilityMAVLength", Str$(.VolatilityMAVLength)
    SaveSetting App.Title, "Indicator", "Volatility", Str$(.Volatility)
    
    If .PlotEaseOfMovementAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotEaseOfMovementAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotEaseOfMovementAndMAV", "False"
    End If
    If .PlotNegativeVolumeAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotNegativeVolumeAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotNegativeVolumeAndMAV", "False"
    End If
    If .PlotPositiveVolumeAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotPositiveVolumeAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotPositiveVolumeAndMAV", "False"
    End If
    If .PlotRateOfChangeAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotRateOfChangeAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotRateOfChangeAndMAV", "False"
    End If
    If .PlotStandardDeviationAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotStandardDeviationAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotStandardDeviationAndMAV", "False"
    End If
    If .PlotTRIXAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotTRIXAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotTRIXAndMAV", "False"
    End If
    
    If .PlotVolatilityAndMAV Then
        SaveSetting App.Title, "Indicator", "PlotVolatilityAndMAV", "True"
    Else
      SaveSetting App.Title, "Indicator", "PlotVolatilityAndMAV", "False"
    End If
    
    End With
    
    
End Sub

Sub SaveParameterSettingsToInvest(Index As Long)
With ParameterSettings
  'WorkspaceSettingsArray(Index).ParameterSettings.COPDist = .COPDist
  'WorkspaceSettingsArray(Index).ParameterSettings.COPMAV = .COPMAV
  'WorkspaceSettingsArray(Index).ParameterSettings.COPSUM = .COPSUM
  WorkspaceSettingsArray(Index).ParameterSettings.MACDMAV = .MACDMAV
  WorkspaceSettingsArray(Index).ParameterSettings.MAV1 = .MAV1
  WorkspaceSettingsArray(Index).ParameterSettings.MAV2 = .MAV2
  WorkspaceSettingsArray(Index).ParameterSettings.MAVUseExp = .MAVUseExp
  WorkspaceSettingsArray(Index).ParameterSettings.MAVMFI = .MAVMFI
  WorkspaceSettingsArray(Index).ParameterSettings.MAVMOM = .MAVMOM
  WorkspaceSettingsArray(Index).ParameterSettings.MAVNbr = .MAVNbr
  WorkspaceSettingsArray(Index).ParameterSettings.MAVRSI = .MAVRSI
  WorkspaceSettingsArray(Index).ParameterSettings.MAVSTO = .MAVSTO
  WorkspaceSettingsArray(Index).ParameterSettings.MFILength = .MFILength
  WorkspaceSettingsArray(Index).ParameterSettings.MFIShowMAV = .MFIShowMAV
  WorkspaceSettingsArray(Index).ParameterSettings.MFISignalUseMAV = .MFISignalUseMAV
  WorkspaceSettingsArray(Index).ParameterSettings.MOM = .MOM
  WorkspaceSettingsArray(Index).ParameterSettings.PlotMOMandMAV = .PlotMOMandMAV
  WorkspaceSettingsArray(Index).ParameterSettings.MOMSignalUseMAV = .MOMSignalUseMAV
  WorkspaceSettingsArray(Index).ParameterSettings.MOMSignalUseZeroLine = .MOMSignalUseZeroLine
  WorkspaceSettingsArray(Index).ParameterSettings.OSCMAVExp = .OSCMAVExp
  WorkspaceSettingsArray(Index).ParameterSettings.PRIOSC1 = .PRIOSC1
  WorkspaceSettingsArray(Index).ParameterSettings.PRIOSC2 = .PRIOSC2
  WorkspaceSettingsArray(Index).ParameterSettings.RSI = .RSI
  WorkspaceSettingsArray(Index).ParameterSettings.PlotRSIandMAV = .PlotRSIandMAV
  WorkspaceSettingsArray(Index).ParameterSettings.RSISignalUseMAV = .RSISignalUseMAV
  WorkspaceSettingsArray(Index).ParameterSettings.STO = .STO
  WorkspaceSettingsArray(Index).ParameterSettings.STOSlowing = .STOSlowing
  WorkspaceSettingsArray(Index).ParameterSettings.PlotSTOandMAV = .PlotSTOandMAV
  WorkspaceSettingsArray(Index).ParameterSettings.STOSignalUseSignalLevels = .STOSignalUseSignalLevels
  WorkspaceSettingsArray(Index).ParameterSettings.AccelerationFactor = .AccelerationFactor
  WorkspaceSettingsArray(Index).ParameterSettings.BollingerMAVLength = .BollingerMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.BollingerSTDFactor = .BollingerSTDFactor
  
  WorkspaceSettingsArray(Index).ParameterSettings.AverageTrueRangeLength = .AverageTrueRangeLength
  WorkspaceSettingsArray(Index).ParameterSettings.ChaikinVolatility = .ChaikinVolatility
  WorkspaceSettingsArray(Index).ParameterSettings.ChaikinVolatilityROC = .ChaikinVolatilityROC
  WorkspaceSettingsArray(Index).ParameterSettings.DetrendedPriceOscillatorLength = .DetrendedPriceOscillatorLength
  WorkspaceSettingsArray(Index).ParameterSettings.EaseOfMovementLength = .EaseOfMovementLength
  WorkspaceSettingsArray(Index).ParameterSettings.EaseOfMovementMAVLength = .EaseOfMovementMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotEaseOfMovementAndMAV = .PlotEaseOfMovementAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.NegativeVolumeMAVLength = .NegativeVolumeMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotNegativeVolumeAndMAV = .PlotNegativeVolumeAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.PositiveVolumeMAVLength = .PositiveVolumeMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotPositiveVolumeAndMAV = .PlotPositiveVolumeAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.RateOfChangeLength = .RateOfChangeLength
  WorkspaceSettingsArray(Index).ParameterSettings.RateOfChangeMAVLength = .RateOfChangeMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotRateOfChangeAndMAV = .PlotRateOfChangeAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.StandardDeviationLength = .StandardDeviationLength
  WorkspaceSettingsArray(Index).ParameterSettings.StandardDeviationMAVLength = .StandardDeviationMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotStandardDeviationAndMAV = .PlotStandardDeviationAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.TRIXLength = .TRIXLength
  WorkspaceSettingsArray(Index).ParameterSettings.TRIXMAVLength = .TRIXMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.PlotTRIXAndMAV = .PlotTRIXAndMAV
  WorkspaceSettingsArray(Index).ParameterSettings.VolumeOscillatorMAV1Length = .VolumeOscillatorMAV1Length
  WorkspaceSettingsArray(Index).ParameterSettings.VolumeOscillatorMAV2Length = .VolumeOscillatorMAV2Length
  WorkspaceSettingsArray(Index).ParameterSettings.VolumeRateOfChangeLength = .VolumeRateOfChangeLength
  
  WorkspaceSettingsArray(Index).ParameterSettings.MFILowerSignalLevel = .MFILowerSignalLevel
  WorkspaceSettingsArray(Index).ParameterSettings.MFIUpperSignalLevel = .MFIUpperSignalLevel
  WorkspaceSettingsArray(Index).ParameterSettings.RSILowerSignalLevel = .RSILowerSignalLevel
  WorkspaceSettingsArray(Index).ParameterSettings.RSIUpperSignalLevel = .RSIUpperSignalLevel
  WorkspaceSettingsArray(Index).ParameterSettings.STOLowerSignalLevel = .STOLowerSignalLevel
  WorkspaceSettingsArray(Index).ParameterSettings.STOUpperSignalLevel = .STOUpperSignalLevel
  
  WorkspaceSettingsArray(Index).ParameterSettings.VolatilityMAVLength = .VolatilityMAVLength
  WorkspaceSettingsArray(Index).ParameterSettings.Volatility = .Volatility
  WorkspaceSettingsArray(Index).ParameterSettings.PlotVolatilityAndMAV = .PlotVolatilityAndMAV
End With
End Sub

Sub GetParameterSettingsFromRegistry()
With ParameterSettings
    .MAV1 = Int(GetSetting(App.Title, "Indicator", "MAV1", "10"))
    .MAV2 = Int(GetSetting(App.Title, "Indicator", "MAV2", "20"))
    .MAVMOM = Int(GetSetting(App.Title, "Indicator", "MAVMOM", "5"))
    .MAVRSI = Int(GetSetting(App.Title, "Indicator", "MAVRSI", "5"))
    .MAVSTO = Int(GetSetting(App.Title, "Indicator", "MAVSTO", "5"))
    .MOM = Int(GetSetting(App.Title, "Indicator", "MOM", "5"))
    .PRIOSC1 = Int(GetSetting(App.Title, "Indicator", "PRIOSC1", "20"))
    .PRIOSC2 = Int(GetSetting(App.Title, "Indicator", "PRIOSC2", "40"))
    .RSI = Int(GetSetting(App.Title, "Indicator", "RSI", "14"))
    .STO = Int(GetSetting(App.Title, "Indicator", "STO", "34"))
    .STOSlowing = Int(GetSetting(App.Title, "Indicator", "STOSlowing", "3"))
    .AccelerationFactor = CSng(PointToComma(GetSetting(App.Title, "Indicator", "AF", "0,02")))
    .BollingerMAVLength = Int(GetSetting(App.Title, "Indicator", "BollingerMAV", "20"))
    .BollingerSTDFactor = CSng(PointToComma((GetSetting(App.Title, "Indicator", "BollingerSTD", "2,0"))))

    If GetSetting(App.Title, "Indicator", "MAVUseExp", "True") = "True" Then
      .MAVUseExp = True
    Else
      .MAVUseExp = False
   End If
    .MAVNbr = Int(GetSetting(App.Title, "Indicator", "MAVNbr", "2"))
    If GetSetting(App.Title, "Indicator", "PlotMOMandMAV", "True") = "True" Then
      .PlotMOMandMAV = True
    Else
      .PlotMOMandMAV = False
    End If
    If GetSetting(App.Title, "Indicator", "OSCMAVExp", "True") = "True" Then
      .OSCMAVExp = True
    Else
      .OSCMAVExp = False
    End If
    If GetSetting(App.Title, "Indicator", "PlotRSIandMAV", "True") = "True" Then
      .PlotRSIandMAV = True
    Else
      .PlotRSIandMAV = False
    End If
    If GetSetting(App.Title, "Indicator", "RSISignalUseMAV", "True") = "True" Then
      .RSISignalUseMAV = True
    Else
      .RSISignalUseMAV = False
    End If
    If GetSetting(App.Title, "Indicator", "PlotSTOandMAV", "True") = "True" Then
      .PlotSTOandMAV = True
    Else
      .PlotSTOandMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "STOSignalUseSignalLevels", "True") Then
      .STOSignalUseSignalLevels = True
    Else
      .STOSignalUseSignalLevels = False
    End If
    
    If GetSetting(App.Title, "Indicator", "MACDMAV", "True") = "True" Then
      .MACDMAV = True
    Else
      .MACDMAV = False
    End If
    If GetSetting(App.Title, "Indicator", "MOMSignalUseMAV", "True") = "True" Then
      .MOMSignalUseMAV = True
    Else
      .MOMSignalUseMAV = False
    End If
    If GetSetting(App.Title, "Indicator", "MOMSignalUseZeroLine", "True") = "True" Then
      .MOMSignalUseZeroLine = True
    Else
      .MOMSignalUseZeroLine = False
    End If
    If GetSetting(App.Title, "Indicator", "MFIShowMAV", "True") = "True" Then
      .MFIShowMAV = True
    Else
      .MFIShowMAV = False
    End If
    .MFILength = Int(GetSetting(App.Title, "Indicator", "MFILength", "25"))
    .MAVMFI = Int(GetSetting(App.Title, "Indicator", "MAVMFI", "10"))
    
    If GetSetting(App.Title, "Indicator", "MFISignalUseMAV", "True") = "True" Then
      .MFISignalUseMAV = True
    Else
      .MFISignalUseMAV = False
    End If
    
    '.COPMAV = Int(GetSetting(App.Title, "Indicator", "COPMAV", "3"))
    '.COPDist = Int(GetSetting(App.Title, "Indicator", "COPDist", "3"))
    '.COPSUM = Int(GetSetting(App.Title, "Indicator", "COPSUM", "2"))
    
    .MFILowerSignalLevel = Int(GetSetting(App.Title, "Indicator", "MFILowerSignalLevel", "30"))
    .MFIUpperSignalLevel = Int(GetSetting(App.Title, "Indicator", "MFIUpperSignalLevel", "70"))
    .RSILowerSignalLevel = Int(GetSetting(App.Title, "Indicator", "RSILowerSignalLevel", "30"))
    .RSIUpperSignalLevel = Int(GetSetting(App.Title, "Indicator", "RSIUpperSignalLevel", "70"))
    .STOLowerSignalLevel = Int(GetSetting(App.Title, "Indicator", "STOLowerSignalLevel", "20"))
    .STOUpperSignalLevel = Int(GetSetting(App.Title, "Indicator", "STOUpperSignalLevel", "80"))
    
    .AverageTrueRangeLength = Int(GetSetting(App.Title, "Indicator", "AverageTrueRangeLength", "14"))
    .ChaikinVolatility = Int(GetSetting(App.Title, "Indicator", "Chaikin's Volatility", "10"))
    .ChaikinVolatilityROC = Int(GetSetting(App.Title, "Indicator", "Chaikin's Volatility ROC", "10"))
    .DetrendedPriceOscillatorLength = Int(GetSetting(App.Title, "Indicator", "DetrendedPriceOscillatorLength", "20"))
    .EaseOfMovementLength = Int(GetSetting(App.Title, "Indicator", "EaseOfMovementLength", "14"))
    .EaseOfMovementMAVLength = Int(GetSetting(App.Title, "Indicator", "EaseOfMovementMAVLength", "9"))
    .NegativeVolumeMAVLength = Int(GetSetting(App.Title, "Indicator", "NegativeVolumeMAVLength", "14"))
    .PositiveVolumeMAVLength = Int(GetSetting(App.Title, "Indicator", "PositiveVolumeMAVLength", "14"))
    .RateOfChangeLength = Int(GetSetting(App.Title, "Indicator", "RateOfChangeLength", "5"))
    .RateOfChangeMAVLength = Int(GetSetting(App.Title, "Indicator", "RateOfChangeMAVLength", "9"))
    .StandardDeviationLength = Int(GetSetting(App.Title, "Indicator", "StandardDeviationLength", "14"))
    .StandardDeviationMAVLength = Int(GetSetting(App.Title, "Indicator", "StandardDeviationMAVLength", "5"))
    .TRIXLength = Int(GetSetting(App.Title, "Indicator", "TRIXLength", "12"))
    .TRIXMAVLength = Int(GetSetting(App.Title, "Indicator", "TRIXMAVLength", "9"))
    .VolumeOscillatorMAV1Length = Int(GetSetting(App.Title, "Indicator", "VolumeOscillatorMAV1Length", "15"))
    .VolumeOscillatorMAV2Length = Int(GetSetting(App.Title, "Indicator", "VolumeOscillatorMAV2Length", "25"))
    .VolumeRateOfChangeLength = Int(GetSetting(App.Title, "Indicator", "VolumeRateOfChangeLength", "12"))
    
    If GetSetting(App.Title, "Indicator", "PlotEaseOfMovementAndMAV", "True") = "True" Then
      .PlotEaseOfMovementAndMAV = True
    Else
      .PlotEaseOfMovementAndMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "PlotNegativeVolumeAndMAV", "True") = "True" Then
      .PlotNegativeVolumeAndMAV = True
    Else
      .PlotNegativeVolumeAndMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "PlotPositiveVolumeAndMAV", "True") = "True" Then
      .PlotPositiveVolumeAndMAV = True
    Else
      .PlotPositiveVolumeAndMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "PlotRateOfChangeAndMAV", "True") = "True" Then
      .PlotRateOfChangeAndMAV = True
    Else
      .PlotRateOfChangeAndMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "PlotStandardDeviationAndMAV", "True") = "True" Then
      .PlotStandardDeviationAndMAV = True
    Else
      .PlotStandardDeviationAndMAV = False
    End If
    
    If GetSetting(App.Title, "Indicator", "PlotTRIXAndMAV", "True") = "True" Then
      .PlotTRIXAndMAV = True
    Else
      .PlotTRIXAndMAV = False
    End If
    
    .VolatilityMAVLength = Int(GetSetting(App.Title, "Indicator", "VolatilityMAVLength", "5"))
    .Volatility = Int(GetSetting(App.Title, "Indicator", "Volatility", "20"))
    If GetSetting(App.Title, "Indicator", "PlotVolatilityAndMAV", "True") = "True" Then
      .PlotVolatilityAndMAV = True
    Else
      .PlotVolatilityAndMAV = False
    End If
  End With
  
  
  'Bj�rns Variabler
ShowGraphAtStartup = GetSetting(App.Title, "Indicator", "ShowGraphAtStartup", 1)
'NbrVirtualDates = GetSetting(App.Title, "Indicator", "NbrVirtualDates", 0)
TrendlineNotation = GetSetting(App.Title, "Indicator", "TrendlineNotation", 1)
CycleLenth = GetSetting(App.Title, "Indicator", "CycleLenth", 26)
'NotateToday = GetSetting(App.Title, "Indicator", "NotateToday", 0)

'Slut Hack
End Sub


Function GetDefaultParameterSettings() As ParameterSettingsType

 ' Setting IndicatorParameters to default values
With GetDefaultParameterSettings
  .MAV1 = 10
  .MAV2 = 20
  .MAVMOM = 5
  .MAVRSI = 5
  .MAVSTO = 5
  .MOM = 5
  .PRIOSC1 = 20
  .PRIOSC2 = 40
  .RSI = 14
  .STO = 34
  .STOSlowing = 3
  .MAVUseExp = True
  .MAVNbr = 2
  .PlotMOMandMAV = True
  .OSCMAVExp = True
  .PlotRSIandMAV = True
  .RSISignalUseMAV = False
  .PlotSTOandMAV = True
  .STOSignalUseSignalLevels = True
  .MACDMAV = True
  .MOMSignalUseMAV = True
  .MOMSignalUseZeroLine = True
  .MFIShowMAV = True
  .MFILength = 25
  .MAVMFI = 10
  .MFISignalUseMAV = False
  '.COPMAV = 3
  '.COPDist = 3
  '.COPSUM = 2
  .AccelerationFactor = 0.02
  .BollingerMAVLength = 20
  .BollingerSTDFactor = 2
  .AverageTrueRangeLength = 14
  .ChaikinVolatility = 10
  .ChaikinVolatilityROC = 10
  .DetrendedPriceOscillatorLength = 20
  .EaseOfMovementLength = 14
  .EaseOfMovementMAVLength = 9
  .PlotEaseOfMovementAndMAV = True
  .NegativeVolumeMAVLength = 14
  .PlotNegativeVolumeAndMAV = True
  .PositiveVolumeMAVLength = 14
  .PlotPositiveVolumeAndMAV = True
  .RateOfChangeLength = 5
  .RateOfChangeMAVLength = 9
  .PlotRateOfChangeAndMAV = True
  .StandardDeviationLength = 14
  .StandardDeviationMAVLength = 5
  .PlotStandardDeviationAndMAV = True
  .TRIXLength = 12
  .TRIXMAVLength = 5
  .PlotTRIXAndMAV = True
  .VolumeOscillatorMAV1Length = 15
  .VolumeOscillatorMAV2Length = 25
  .VolumeRateOfChangeLength = 12
  
  .MFILowerSignalLevel = 30
  .MFIUpperSignalLevel = 70
  .RSILowerSignalLevel = 30
  .RSIUpperSignalLevel = 70
  .STOLowerSignalLevel = 20
  .STOUpperSignalLevel = 80
  
  .VolatilityMAVLength = 5
  .Volatility = 20
  .PlotVolatilityAndMAV = True
End With
End Function

Public Sub UpdateParameterSettings()
  With frmParameters
    ParameterSettings.MAV1 = .Para_MAV_Period1
    ParameterSettings.MAV2 = .Para_MAV_Period2
    ParameterSettings.RSI = .Tb_RSILength
    ParameterSettings.MAVRSI = .Tb_RSIMAVLength
    ParameterSettings.STO = .Tb_StoOscPeriodK
    ParameterSettings.STOSlowing = .txtSTOSlowing
    ParameterSettings.MAVSTO = .Tb_StoOscPeriodD
    ParameterSettings.MOM = .Tb_MOMLength
    ParameterSettings.MAVMOM = .Tb_MOMMAVLength
    ParameterSettings.PRIOSC1 = .Tb_PriceOsc_MAV1
    ParameterSettings.PRIOSC2 = .Tb_PriceOsc_MAV2
    ParameterSettings.MAVUseExp = .Option_Exp
    
    If .Option_OneMAV = True Then
      ParameterSettings.MAVNbr = 1
    Else
      ParameterSettings.MAVNbr = 2
    End If
    
    ParameterSettings.PlotRSIandMAV = .Option_ShowRSI
    ParameterSettings.RSISignalUseMAV = .Option_SignalRSIUseMAV
    ParameterSettings.PlotSTOandMAV = .Option_ShowOscAndMAV(0)
    
    If .optSTOSignalUseLevels Then
      ParameterSettings.STOSignalUseSignalLevels = True
    Else
      ParameterSettings.STOSignalUseSignalLevels = False
    End If
    
    ParameterSettings.PlotMOMandMAV = .Option_ShowMOMMAV
    ParameterSettings.OSCMAVExp = .Option_PriceOsc_Exponential
    ParameterSettings.MACDMAV = .Option_MACDSignal
    ParameterSettings.MOMSignalUseMAV = .OptionMOMSignalUseMAV
    ParameterSettings.MOMSignalUseZeroLine = .OptionSignalUseZeroLine
    ParameterSettings.MFIShowMAV = .OptionShowMFIAndMAV
    ParameterSettings.MFILength = .tbMFILength
    ParameterSettings.MAVMFI = .tbMFIMAVLength
    ParameterSettings.MFISignalUseMAV = .OptionMFISignalUseMAV
    'ParameterSettings.COPMAV = .HScrollCOPMAV
    'ParameterSettings.COPDist = .HScrollCOPDist
    'ParameterSettings.COPSUM = .HScrollCOPSUM
    ParameterSettings.BollingerMAVLength = .tbBollingerMAV
    ParameterSettings.BollingerSTDFactor = CSng(PointToComma(.tbBollingerSTD))
    ParameterSettings.AccelerationFactor = CSng(PointToComma(.tbAccelerationfactor))
    
    ParameterSettings.AverageTrueRangeLength = .txtAverageTrueRangeLength
    ParameterSettings.ChaikinVolatility = .txtChaikinVolatility
    ParameterSettings.ChaikinVolatilityROC = .txtChaikinVolatilityROC
    ParameterSettings.DetrendedPriceOscillatorLength = .txtDPOLength
    ParameterSettings.EaseOfMovementLength = .txtEaseOfMovementLength
    ParameterSettings.EaseOfMovementMAVLength = .txtEaseOfMovementMAVLength
    ParameterSettings.MFILowerSignalLevel = .txtMFILowerSignalLevel
    ParameterSettings.MFIUpperSignalLevel = .txtMFIUpperSignalLevel
    ParameterSettings.NegativeVolumeMAVLength = .txtNegativeVolumeMAVLength
    ParameterSettings.PlotEaseOfMovementAndMAV = .optEOMandMAVYes
    ParameterSettings.PlotNegativeVolumeAndMAV = .optNVShowMAVYes
    ParameterSettings.PlotPositiveVolumeAndMAV = .optPVShowMAVYes
    ParameterSettings.PlotRateOfChangeAndMAV = .optROCShowMAVYes
    ParameterSettings.PlotStandardDeviationAndMAV = .optSTDShowMAVYes
    ParameterSettings.PlotTRIXAndMAV = .optTRIXShowMAVYes
    ParameterSettings.PositiveVolumeMAVLength = .txtPositiveVolumeMAVLength
    ParameterSettings.RateOfChangeLength = .txtRateOfChangeLength
    ParameterSettings.RateOfChangeMAVLength = .txtRateOfChangeMAVLength
    ParameterSettings.RSILowerSignalLevel = .txtRSILowerSignalLevel
    ParameterSettings.RSIUpperSignalLevel = .txtRSIUpperSignalLevel
    ParameterSettings.StandardDeviationLength = .txtStandardDeviationLength
    ParameterSettings.StandardDeviationMAVLength = .txtStandardDeviationMAVLength
    ParameterSettings.STOLowerSignalLevel = .txtSTOLowerSignalLevel
    ParameterSettings.STOUpperSignalLevel = .txtSTOUpperSignalLevel
    ParameterSettings.TRIXLength = .txtTRIXLength
    ParameterSettings.TRIXMAVLength = .txtTRIXMAVLength
    ParameterSettings.VolumeOscillatorMAV1Length = .txtVolumeOscillatorMAV1Length
    ParameterSettings.VolumeOscillatorMAV2Length = .txtVolumeOscillatorMAV2Length
    ParameterSettings.VolumeRateOfChangeLength = .txtVolumeRateOfChangeLength
    
    ParameterSettings.VolatilityMAVLength = .txtVolatilityMAVLength
    ParameterSettings.Volatility = .txtVolatility
    ParameterSettings.PlotVolatilityAndMAV = .OptionVolatilityShowMAVYes
  End With
End Sub


Public Sub InitParameters()
With ParameterSettings

    ' TODO:
    ' For some reason you can't set the value on the ones below
    ' Solve!!
    On Error Resume Next

    frmParameters.UpDownParaMAVPeriod1.Value = .MAV1
    frmParameters.UpDownParaMAVPeriod2.Value = .MAV2
    frmParameters.UpDownSTO1.Value = .STO
    frmParameters.UpDownSTO2.Value = .STOSlowing
    frmParameters.UpDownSTO3.Value = .MAVSTO
    frmParameters.UpDownRSI1.Value = .RSI
    frmParameters.UpDownRSI2.Value = .MAVRSI
    
    ' TODO:
    'MsgBox "ParameterSettings.PRIOSC1: " & .PRIOSC1
    'MsgBox "frmParameters.UpDownPriceOsc1.Value: " & frmParameters.UpDownPriceOsc1.Value
    frmParameters.UpDownPriceOsc1.Value = .PRIOSC1
    
    frmParameters.UpDownPriceOsc2.Value = .PRIOSC2
    frmParameters.UpDownMOM1.Value = .MOM
    frmParameters.UpDownMOM2.Value = .MAVMOM
    frmParameters.UpDownBOL.Value = .BollingerMAVLength
    frmParameters.UpDownMFI1.Value = .MFILength
    frmParameters.UpDownMFI2.Value = .MAVMFI
    frmParameters.tbBollingerSTD = .BollingerSTDFactor
    frmParameters.tbAccelerationfactor = .AccelerationFactor
    frmParameters.UpDownATR.Value = .AverageTrueRangeLength
    frmParameters.UpDownChaikinVolatility.Value = .ChaikinVolatility
    
    ' TODO:
    'MsgBox "ParameterSettings.ChaikinVolatilityROC: " & .ChaikinVolatilityROC
    'MsgBox "frmParameters.UpDownChaikinVolatilityROC.Value: " & frmParameters.UpDownChaikinVolatilityROC.Value
    frmParameters.UpDownChaikinVolatilityROC.Value = .ChaikinVolatilityROC
    
    frmParameters.UpDownDPO.Value = .DetrendedPriceOscillatorLength
    frmParameters.UpDownEOM1.Value = .EaseOfMovementLength
    frmParameters.UpDownEOM2.Value = .EaseOfMovementMAVLength
    frmParameters.UpDownMFI3.Value = .MFILowerSignalLevel
    frmParameters.UpDownMFI4.Value = .MFIUpperSignalLevel
    frmParameters.UpDownNV.Value = .NegativeVolumeMAVLength
    frmParameters.UpDownPV.Value = .PositiveVolumeMAVLength
    frmParameters.UpDownROC1.Value = .RateOfChangeLength
    
    ' TODO:
    frmParameters.UpDownROC2.Value = .RateOfChangeMAVLength
    
    frmParameters.UpDownRSI3.Value = .RSILowerSignalLevel
    frmParameters.UpDownRSI4.Value = .RSIUpperSignalLevel
    frmParameters.UpDownSTD1.Value = .StandardDeviationLength
    
    ' TODO:
    frmParameters.UpDownSTD2.Value = .StandardDeviationMAVLength
    
    frmParameters.UpDownSTO4.Value = .STOLowerSignalLevel
    
    ' TODO:
    frmParameters.UpDownSTO5.Value = .STOUpperSignalLevel
    
    frmParameters.UpDownTRIX1.Value = .TRIXLength
    frmParameters.UpDownTRIX2.Value = .TRIXMAVLength
    frmParameters.UpDownVO1.Value = .VolumeOscillatorMAV1Length
    
    ' TODO:
    frmParameters.UpDownVO2.Value = .VolumeOscillatorMAV2Length
    
    frmParameters.UpDownVROC.Value = .VolumeRateOfChangeLength
    
    If .MACDMAV Then
      frmParameters.Option_MACDSignal = True
    Else
      frmParameters.OptionShowMACDOnly = True
    End If
    
    If .OSCMAVExp Then
      frmParameters.Option_PriceOsc_Exponential = True
    Else
      frmParameters.Option_PriceOsc_Simple = True
    End If
    
    If .PlotMOMandMAV Then
      frmParameters.Option_ShowMOMMAV = True
    Else
      frmParameters.Option_DontShowMOMMAV = True
    End If
    
    If .RSISignalUseMAV Then
      frmParameters.Option_SignalRSIUseMAV = True
    Else
      frmParameters.Option_SignalRSIDontUseMAV = True
    End If
    
    If .MAVUseExp Then
      frmParameters.Option_Exp = True
    Else
      frmParameters.Option_Simple = True
    End If
    
    If .PlotRSIandMAV Then
      frmParameters.Option_ShowRSI = True
    Else
      frmParameters.Option2 = True
    End If
    
    If .PlotSTOandMAV Then
      frmParameters.Option_ShowOscAndMAV(0) = True
    Else
      frmParameters.Option_ShowOsc(0) = True
    End If
    
    If .MAVNbr = 1 Then
      frmParameters.Option_OneMAV = True
    Else
      frmParameters.Option_TwoMAV = True
    End If
    
    If .STOSignalUseSignalLevels Then
      frmParameters.optSTOSignalUseLevels = True
    Else
      frmParameters.optSTOSignalUseMAV = True
    End If
    
    If .MOMSignalUseMAV Then
      frmParameters.OptionMOMSignalUseMAV = True
    Else
      frmParameters.OptionDontUseMAVInSignal = True
    End If
    
    If .MOMSignalUseZeroLine Then
      frmParameters.OptionSignalUseZeroLine = True
    Else
      frmParameters.OptionMOMSignalUseMINMAX = True
    End If
    
    If .MFIShowMAV Then
      frmParameters.OptionShowMFIAndMAV = True
    Else
      frmParameters.OptionShowMFI = True
    End If
    
    If .MFISignalUseMAV Then
      frmParameters.OptionMFISignalUseMAV = True
    Else
      frmParameters.OptionMFISignalDontUseMAV = True
    End If
    
    If .PlotEaseOfMovementAndMAV Then
      frmParameters.optEOMandMAVYes = True
    Else
      frmParameters.optEOMandMAVNo = True
    End If
    
    If .PlotNegativeVolumeAndMAV Then
      frmParameters.optNVShowMAVYes = True
    Else
      frmParameters.optNVShowMAVNo = True
    End If
    
    If .PlotPositiveVolumeAndMAV Then
      frmParameters.optPVShowMAVYes = True
    Else
      frmParameters.optPVShowMAVNo = True
    End If
    
    If .PlotRateOfChangeAndMAV Then
      frmParameters.optROCShowMAVYes = True
    Else
      frmParameters.optROCShowMAVNo = True
    End If
    
    If .PlotStandardDeviationAndMAV Then
      frmParameters.optSTDShowMAVYes = True
    Else
      frmParameters.optSTDShowMAVNo = True
    End If
    
    If .PlotTRIXAndMAV Then
      frmParameters.optTRIXShowMAVYes = True
    Else
      frmParameters.optTRIXShowMAVYes = True
    End If
    
    If .MAVUseExp Then
      frmParameters.Option_Exp.Value = True
    Else
      frmParameters.Option_Simple.Value = True
    End If
    
    frmParameters.UpDownVolatilityMAVLength.Value = .VolatilityMAVLength
    frmParameters.UpDownVolatility.Value = .Volatility
    
    If .PlotVolatilityAndMAV Then
      frmParameters.OptionVolatilityShowMAVYes = True
    Else
      frmParameters.OptionVolatilityShowMAVNo = True
    End If
  End With
End Sub

'#If FISH = 1 Then
  Sub InitFishParametets()
    With frmFishSettings
    .updFishShortest = FishParameterSettings.lngShortest
    .updFishLongest = FishParameterSettings.lngLongest
    .updFishStep = FishParameterSettings.lngStep
    End With
  End Sub
  
  Sub UpdateFishParameterSettings()
    With FishParameterSettings
      .lngShortest = CLng(frmFishSettings.txtFishShortest)
      .lngLongest = CLng(frmFishSettings.txtFishLongest)
      .lngStep = CLng(frmFishSettings.txtFishStep)
    End With
  End Sub
  
  Function GetDefaulftFishParameterSettings() As FishParameterSettingsType
    With GetDefaulftFishParameterSettings
      .lngShortest = 1
      .lngLongest = 250
      .lngStep = 1
    End With
  End Function
  
  Sub GetFishParameterSettingsFromRegistry()
    With FishParameterSettings
    .lngShortest = CLng(GetSetting(App.Title, "Indicator", "FishShortest", "1"))
    .lngLongest = CLng(GetSetting(App.Title, "Indicator", "FishLongest", "250"))
    .lngStep = CLng(GetSetting(App.Title, "Indicator", "FishStep", "1"))
    End With
  End Sub
  
Sub GetFishParameterSettingsFromInvest(Index As Long)
If Index > 0 And Index <= WorkSpaceSettingsArraySize Then
  With FishParameterSettings
  .lngShortest = WorkspaceSettingsArray(Index).FishParameterSettings.lngShortest
  .lngLongest = WorkspaceSettingsArray(Index).FishParameterSettings.lngLongest
  .lngStep = WorkspaceSettingsArray(Index).FishParameterSettings.lngStep
  End With
Else
  ErrorMessageStr "Kunde inte h�mta objektberoende parametrar"
End If
End Sub
  
Sub SaveFishParameterSettingsToInvest(Index As Long)
  With FishParameterSettings
  WorkspaceSettingsArray(Index).FishParameterSettings.lngShortest = .lngShortest
  WorkspaceSettingsArray(Index).FishParameterSettings.lngLongest = .lngShortest
  WorkspaceSettingsArray(Index).FishParameterSettings.lngStep = .lngStep
  End With
End Sub

Sub SaveFishParameterSettingsToRegistry()
  With FishParameterSettings
    SaveSetting App.Title, "Indicator", "FishShortest", Str$(.lngShortest)
    SaveSetting App.Title, "Indicator", "FishLongest", Str$(.lngLongest)
    SaveSetting App.Title, "Indicator", "FishStep", Str$(.lngStep)
  End With
End Sub
'#End If
