VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "Tabctl32.ocx"
Begin VB.Form frmIndicatortestStops 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inst�llningar - Indikatortest"
   ClientHeight    =   2655
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7815
   Icon            =   "frmIndicatortestStops.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2655
   ScaleWidth      =   7815
   Begin TabDlg.SSTab SSTab1 
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   4683
      _Version        =   393216
      Tabs            =   5
      TabsPerRow      =   5
      TabHeight       =   520
      TabCaption(0)   =   "Breakeven"
      TabPicture(0)   =   "frmIndicatortestStops.frx":014A
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Frame1"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Frame3"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "Frame4(0)"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).ControlCount=   3
      TabCaption(1)   =   "Inaktivitet"
      TabPicture(1)   =   "frmIndicatortestStops.frx":0166
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "Frame4(1)"
      Tab(1).Control(1)=   "Frame7"
      Tab(1).Control(2)=   "Frame6"
      Tab(1).Control(3)=   "Frame5"
      Tab(1).ControlCount=   4
      TabCaption(2)   =   "Max f�rlust"
      TabPicture(2)   =   "frmIndicatortestStops.frx":0182
      Tab(2).ControlEnabled=   0   'False
      Tab(2).Control(0)=   "Frame10"
      Tab(2).Control(1)=   "Frame11"
      Tab(2).Control(2)=   "Frame12"
      Tab(2).Control(3)=   "Frame4(2)"
      Tab(2).ControlCount=   4
      TabCaption(3)   =   "Vinsthemtagning"
      TabPicture(3)   =   "frmIndicatortestStops.frx":019E
      Tab(3).ControlEnabled=   0   'False
      Tab(3).Control(0)=   "Frame4(3)"
      Tab(3).Control(1)=   "Frame15"
      Tab(3).Control(2)=   "Frame14"
      Tab(3).Control(3)=   "Frame13"
      Tab(3).ControlCount=   4
      TabCaption(4)   =   "Trailing"
      TabPicture(4)   =   "frmIndicatortestStops.frx":01BA
      Tab(4).ControlEnabled=   0   'False
      Tab(4).Control(0)=   "Frame4(4)"
      Tab(4).Control(1)=   "Frame19"
      Tab(4).Control(2)=   "Frame18"
      Tab(4).Control(3)=   "Frame17"
      Tab(4).ControlCount=   4
      Begin VB.Frame Frame4 
         Height          =   735
         Index           =   4
         Left            =   -74880
         TabIndex        =   59
         Top             =   1800
         Width           =   7575
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   4
            Left            =   480
            TabIndex        =   61
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   4
            Left            =   6120
            TabIndex        =   60
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Index           =   3
         Left            =   -74880
         TabIndex        =   56
         Top             =   1800
         Width           =   7575
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   3
            Left            =   480
            TabIndex        =   58
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   3
            Left            =   6120
            TabIndex        =   57
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Index           =   2
         Left            =   -74880
         TabIndex        =   53
         Top             =   1800
         Width           =   7575
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   2
            Left            =   480
            TabIndex        =   55
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   2
            Left            =   6120
            TabIndex        =   54
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Index           =   1
         Left            =   -74880
         TabIndex        =   50
         Top             =   1800
         Width           =   7575
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   1
            Left            =   480
            TabIndex        =   52
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   1
            Left            =   6120
            TabIndex        =   51
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame19 
         Caption         =   "Parametrar"
         Height          =   1215
         Left            =   -69840
         TabIndex        =   45
         Top             =   480
         Width           =   2535
         Begin VB.TextBox txtTrailingProfitRisk 
            Height          =   285
            Left            =   1560
            TabIndex        =   47
            Top             =   360
            Width           =   855
         End
         Begin VB.TextBox txtTrailingPeriods 
            Height          =   285
            Left            =   1560
            TabIndex        =   46
            Top             =   720
            Width           =   855
         End
         Begin VB.Label Label7 
            Caption         =   "Riskerad vinst"
            Height          =   255
            Left            =   120
            TabIndex        =   49
            Top             =   380
            Width           =   1095
         End
         Begin VB.Label Label6 
            Caption         =   "Ignorera dagar"
            Height          =   255
            Left            =   120
            TabIndex        =   48
            Top             =   735
            Width           =   1455
         End
      End
      Begin VB.Frame Frame18 
         Caption         =   "Metod"
         Height          =   1215
         Left            =   -72120
         TabIndex        =   42
         Top             =   480
         Width           =   2055
         Begin VB.OptionButton optTrailingPoints 
            Caption         =   "Kronor"
            Height          =   195
            Left            =   240
            TabIndex        =   44
            Top             =   720
            Width           =   1335
         End
         Begin VB.OptionButton optTrailingPercent 
            Caption         =   "Procent"
            Height          =   255
            Left            =   240
            TabIndex        =   43
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.Frame Frame17 
         Caption         =   "Vilka positioner"
         Height          =   1215
         Left            =   -74880
         TabIndex        =   39
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chkTrailingShort 
            Caption         =   "Blankningspositioner"
            Height          =   255
            Left            =   240
            TabIndex        =   41
            Top             =   720
            Width           =   2055
         End
         Begin VB.CheckBox chkTrailingLong 
            Caption         =   "L�nga positioner"
            Height          =   255
            Left            =   240
            TabIndex        =   40
            Top             =   360
            Width           =   2055
         End
      End
      Begin VB.Frame Frame15 
         Caption         =   "Parametrar"
         Height          =   1215
         Left            =   -69840
         TabIndex        =   36
         Top             =   480
         Width           =   2535
         Begin VB.TextBox txtProfitTakingTarget 
            Height          =   285
            Left            =   1560
            TabIndex        =   37
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label4 
            Caption         =   "Vinstm�l"
            Height          =   255
            Left            =   240
            TabIndex        =   38
            Top             =   500
            Width           =   1095
         End
      End
      Begin VB.Frame Frame14 
         Caption         =   "Metod"
         Height          =   1215
         Left            =   -72120
         TabIndex        =   33
         Top             =   480
         Width           =   2055
         Begin VB.OptionButton optProfitTakingPercent 
            Caption         =   "Procent"
            Height          =   255
            Left            =   240
            TabIndex        =   35
            Top             =   360
            Width           =   1455
         End
         Begin VB.OptionButton optProfitTakingPoints 
            Caption         =   "Kronor"
            Height          =   195
            Left            =   240
            TabIndex        =   34
            Top             =   720
            Width           =   1335
         End
      End
      Begin VB.Frame Frame13 
         Caption         =   "Vilka positioner"
         Height          =   1215
         Left            =   -74880
         TabIndex        =   30
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chkProfitTakingLong 
            Caption         =   "L�nga positioner"
            Height          =   255
            Left            =   240
            TabIndex        =   32
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkProfitTakingShort 
            Caption         =   "Blankningspositioner"
            Height          =   255
            Left            =   240
            TabIndex        =   31
            Top             =   720
            Width           =   2055
         End
      End
      Begin VB.Frame Frame12 
         Caption         =   "Parametrar"
         Height          =   1215
         Left            =   -69840
         TabIndex        =   27
         Top             =   480
         Width           =   2535
         Begin VB.TextBox txtMaxlossLevel 
            Height          =   285
            Left            =   1560
            TabIndex        =   28
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label5 
            Caption         =   "Maximal f�rlust"
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   495
            Width           =   1095
         End
      End
      Begin VB.Frame Frame11 
         Caption         =   "Metod"
         Height          =   1215
         Left            =   -72120
         TabIndex        =   24
         Top             =   480
         Width           =   2055
         Begin VB.OptionButton optMaxlossPoints 
            Caption         =   "Kronor"
            Height          =   195
            Left            =   240
            TabIndex        =   26
            Top             =   720
            Width           =   1335
         End
         Begin VB.OptionButton optMaxlossPercent 
            Caption         =   "Procent"
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   360
            Width           =   1455
         End
      End
      Begin VB.Frame Frame10 
         Caption         =   "Vilka positioner"
         Height          =   1215
         Left            =   -74880
         TabIndex        =   21
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chkMaxlossShort 
            Caption         =   "Blankningspositioner"
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   720
            Width           =   2055
         End
         Begin VB.CheckBox chkMaxlossLong 
            Caption         =   "L�nga positioner"
            Height          =   255
            Left            =   240
            TabIndex        =   22
            Top             =   360
            Width           =   2055
         End
      End
      Begin VB.Frame Frame7 
         Caption         =   "Parametrar"
         Height          =   1215
         Left            =   -69840
         TabIndex        =   16
         Top             =   480
         Width           =   2535
         Begin VB.TextBox txtInactivityPeriods 
            Height          =   285
            Left            =   1560
            TabIndex        =   19
            Top             =   720
            Width           =   855
         End
         Begin VB.TextBox txtInactivityChange 
            Height          =   285
            Left            =   1560
            TabIndex        =   17
            Top             =   360
            Width           =   855
         End
         Begin VB.Label Label3 
            Caption         =   "Under antal dagar"
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   735
            Width           =   1455
         End
         Begin VB.Label Label2 
            Caption         =   "Min f�r�ndring"
            Height          =   255
            Left            =   120
            TabIndex        =   18
            Top             =   380
            Width           =   1095
         End
      End
      Begin VB.Frame Frame6 
         Caption         =   "Metod"
         Height          =   1215
         Left            =   -72120
         TabIndex        =   13
         Top             =   480
         Width           =   2055
         Begin VB.OptionButton optInactivityPercent 
            Caption         =   "Procent"
            Height          =   255
            Left            =   240
            TabIndex        =   15
            Top             =   360
            Width           =   1455
         End
         Begin VB.OptionButton optInactivityPoints 
            Caption         =   "Kronor"
            Height          =   195
            Left            =   240
            TabIndex        =   14
            Top             =   720
            Width           =   1335
         End
      End
      Begin VB.Frame Frame5 
         Caption         =   "Vilka positioner"
         Height          =   1215
         Left            =   -74880
         TabIndex        =   10
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chkInactivityLong 
            Caption         =   "L�nga positioner"
            Height          =   255
            Left            =   240
            TabIndex        =   12
            Top             =   360
            Width           =   2055
         End
         Begin VB.CheckBox chkInactivityShort 
            Caption         =   "Blankningspositioner"
            Height          =   255
            Left            =   240
            TabIndex        =   11
            Top             =   720
            Width           =   2055
         End
      End
      Begin VB.Frame Frame4 
         Height          =   735
         Index           =   0
         Left            =   120
         TabIndex        =   7
         Top             =   1800
         Width           =   7575
         Begin VB.CommandButton cmdCancel 
            Caption         =   "Avbryt"
            CausesValidation=   0   'False
            Height          =   375
            Index           =   0
            Left            =   6120
            TabIndex        =   9
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cmdOK 
            Caption         =   "OK"
            Default         =   -1  'True
            Height          =   375
            Index           =   0
            Left            =   480
            TabIndex        =   8
            Top             =   240
            Width           =   975
         End
      End
      Begin VB.Frame Frame3 
         Caption         =   "Breakevenparameter"
         Height          =   1215
         Left            =   2880
         TabIndex        =   4
         Top             =   480
         Width           =   2535
         Begin VB.TextBox txtBreakevenLevel 
            Alignment       =   2  'Center
            Height          =   285
            Left            =   1200
            MaxLength       =   5
            TabIndex        =   5
            Top             =   480
            Width           =   855
         End
         Begin VB.Label Label1 
            Caption         =   "Marginal"
            Height          =   255
            Left            =   240
            TabIndex        =   6
            Top             =   480
            Width           =   735
         End
      End
      Begin VB.Frame Frame1 
         Caption         =   "Vilka positioner"
         Height          =   1215
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   2535
         Begin VB.CheckBox chkBreakevenShort 
            Caption         =   "Blankningspositioner"
            Height          =   255
            Left            =   240
            TabIndex        =   3
            Top             =   720
            Width           =   2055
         End
         Begin VB.CheckBox chkBreakevenLong 
            Caption         =   "L�nga positioner"
            Height          =   255
            Left            =   240
            TabIndex        =   2
            Top             =   360
            Width           =   2055
         End
      End
   End
End
Attribute VB_Name = "frmIndicatortestStops"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmdCancel_Click(Index As Integer)
  Unload Me
End Sub

Private Sub cmdOk_Click(Index As Integer)
Dim Checked As Long

Checked = 1
If chkBreakevenLong = Checked Then
    IndicatorTestOptions.UseBreakevenLongPositions = True
  Else
    IndicatorTestOptions.UseBreakevenLongPositions = False
  End If
  
  If chkBreakevenShort = Checked Then
    IndicatorTestOptions.UseBreakevenShortPositions = True
  Else
    IndicatorTestOptions.UseBreakevenShortPositions = False
  End If
  
  If optBreakevenPercent Then
    IndicatorTestOptions.BreakevenMethodIsPercent = True
    IndicatorTestOptions.BreakevenLevel = CSng(PointToComma(txtBreakevenLevel))
  Else
    IndicatorTestOptions.BreakevenMethodIsPercent = False
    IndicatorTestOptions.BreakevenLevel = CSng(PointToComma(txtBreakevenLevel))
  End If
  
  If chkInactivityLong = Checked Then
    IndicatorTestOptions.UseInactivityLongPositions = True
  Else
    IndicatorTestOptions.UseInactivityLongPositions = False
  End If
  
  If chkInactivityShort = Checked Then
    IndicatorTestOptions.UseInactivityShortPositions = True
  Else
    IndicatorTestOptions.UseInactivityShortPositions = False
  End If
  
  IndicatorTestOptions.InactivityMinChange = CSng(PointToComma(txtInactivityChange))
  
  If optInactivityPercent Then
    IndicatorTestOptions.InactivityMethodIsPercent = True
  Else
    IndicatorTestOptions.InactivityMethodIsPercent = False
  End If
  
  IndicatorTestOptions.InactivityPeriods = txtInactivityPeriods
  
  If chkMaxlossLong = Checked Then
    IndicatorTestOptions.UseMaxLossLongPositions = True
  Else
    IndicatorTestOptions.UseMaxLossLongPositions = False
  End If
  
  If chkMaxlossShort = Checked Then
    IndicatorTestOptions.UseMaxLossShortPositions = True
  Else
    IndicatorTestOptions.UseMaxLossShortPositions = False
  End If
  
  If optMaxlossPercent Then
    IndicatorTestOptions.MaxLossMethodIsPercent = True
  Else
    IndicatorTestOptions.MaxLossMethodIsPercent = False
  End If
  
  IndicatorTestOptions.MaxLossLevel = CSng(PointToComma(txtMaxlossLevel))
  
  If chkProfitTakingLong = Checked Then
    IndicatorTestOptions.UseProfitTakingLongPositions = True
  Else
    IndicatorTestOptions.UseProfitTakingLongPositions = False
  End If
  
  If chkProfitTakingShort = Checked Then
    IndicatorTestOptions.UseProfitTakingShortPositions = True
  Else
    IndicatorTestOptions.UseProfitTakingShortPositions = False
  End If
  
  If optProfitTakingPercent Then
    IndicatorTestOptions.ProfitTakingMethodIsPercent = True
  Else
    IndicatorTestOptions.ProfitTakingMethodIsPercent = False
  End If
  
  IndicatorTestOptions.ProfitTarget = CSng(PointToComma(txtProfitTakingTarget))
  
  If chkTrailingLong = Checked Then
    IndicatorTestOptions.UseTrailingLongPositions = True
  Else
    IndicatorTestOptions.UseTrailingLongPositions = False
  End If
  
  If chkTrailingShort = Checked Then
    IndicatorTestOptions.UseTrailingShortPositions = True
  Else
    IndicatorTestOptions.UseTrailingShortPositions = False
  End If
  
  IndicatorTestOptions.TrailingProfitRisk = CSng(PointToComma(txtTrailingProfitRisk))
  
  If optTrailingPercent Then
    IndicatorTestOptions.TrailingMethodIsPercent = True
  Else
    IndicatorTestOptions.TrailingMethodIsPercent = False
  End If
  
  IndicatorTestOptions.TrailingPeriods = txtTrailingPeriods
  
  SaveIndicatortestOptionsToRegistry
  
  Me.Hide
    
End Sub

Private Sub Form_Load()

top = 2000
left = 2000

With IndicatorTestOptions

If .UseBreakevenLongPositions Then
  chkBreakevenLong = 1
Else
  chkBreakevenLong = 0
End If

If .UseBreakevenShortPositions Then
  chkBreakevenShort = 1
Else
  chkBreakevenShort = 0
End If

If .BreakevenMethodIsPercent Then
  optBreakevenPercent = True
Else
  optBreakevenPoints = True
End If

txtBreakevenLevel = .BreakevenLevel

If .UseInactivityLongPositions Then
  chkInactivityLong = 1
Else
  chkInactivityLong = 0
End If

If .UseInactivityShortPositions Then
  chkInactivityShort = 1
Else
  chkInactivityShort = 0
End If

If .InactivityMethodIsPercent Then
  optInactivityPercent = True
Else
  optInactivityPoints = True
End If

txtInactivityChange = .InactivityMinChange
txtInactivityPeriods = .InactivityPeriods

If .UseMaxLossLongPositions Then
  chkMaxlossLong = 1
Else
  chkMaxlossLong = 0
End If

If .UseMaxLossShortPositions Then
  chkMaxlossShort = 1
Else
  chkMaxlossShort = 0
End If

If .MaxLossMethodIsPercent Then
  optMaxlossPercent = True
Else
  optMaxlossPoints = True
End If

txtMaxlossLevel = .MaxLossLevel

If .UseProfitTakingLongPositions Then
  chkProfitTakingLong = 1
Else
  chkProfitTakingLong = 0
End If

If .UseProfitTakingShortPositions Then
  chkProfitTakingShort = 1
Else
  chkProfitTakingShort = 0
End If

If .ProfitTakingMethodIsPercent Then
  optProfitTakingPercent = True
Else
  optProfitTakingPoints = True
End If

txtProfitTakingTarget = .ProfitTarget

If .UseTrailingLongPositions Then
  chkTrailingLong = 1
Else
  chkTrailingLong = 0
End If

If .UseTrailingShortPositions Then
  chkTrailingShort = 1
Else
  chkTrailingShort = 0
End If

If .TrailingMethodIsPercent Then
  optTrailingPercent = True
Else
  optTrailingPoints = True
End If

txtTrailingProfitRisk = .TrailingProfitRisk
txtTrailingPeriods = .TrailingPeriods
End With
End Sub



Private Sub txtBreakevenLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtBreakevenLevel_Validate(Cancel As Boolean)
  VerifySinglePara txtBreakevenLevel, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtBreakevenLevel.SetFocus
End Sub

Private Sub txtInactivityChange_GotFocus()
  TextSelected
End Sub

Private Sub txtInactivityChange_Validate(Cancel As Boolean)
  VerifySinglePara txtInactivityChange, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtInactivityChange.SetFocus
End Sub

Private Sub txtInactivityPeriods_GotFocus()
  TextSelected
End Sub

Private Sub txtInactivityPeriods_Validate(Cancel As Boolean)
  VerifyLong txtInactivityPeriods, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtInactivityPeriods.SetFocus
End Sub

Private Sub txtMaxlossLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtMaxlossLevel_Validate(Cancel As Boolean)
  VerifySinglePara txtMaxlossLevel, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtMaxlossLevel.SetFocus
End Sub

Private Sub txtProfitTakingTarget_GotFocus()
  TextSelected
End Sub

Private Sub txtProfitTakingTarget_Validate(Cancel As Boolean)
  VerifySinglePara txtProfitTakingTarget, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtProfitTakingTarget.SetFocus
End Sub

Private Sub txtTrailingPeriods_GotFocus()
  TextSelected
End Sub

Private Sub txtTrailingPeriods_Validate(Cancel As Boolean)
  VerifyLong txtTrailingPeriods, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtTrailingPeriods.SetFocus
End Sub

Private Sub txtTrailingProfitRisk_GotFocus()
  TextSelected
End Sub

Private Sub txtTrailingProfitRisk_Validate(Cancel As Boolean)
  VerifySinglePara txtTrailingProfitRisk, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtTrailingProfitRisk.SetFocus
End Sub
