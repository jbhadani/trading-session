VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Polygon"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'POLYGON.CLS
Option Explicit

Private Type POINTAPI
    X As Long
    Y As Long
End Type

Private Declare Function Polygon _
Lib "gdi32" ( _
    ByVal hdc As Long, _
    lpPoint As POINTAPI, _
    ByVal nCount As Long _
) As Long

Private Declare Function Polyline _
Lib "gdi32" ( _
    ByVal hdc As Long, _
    lpPoint As POINTAPI, _
    ByVal nCount As Long _
) As Long

'Module-level private variables
Private mobjDevice As Object
Private msngSX1 As Single
Private msngSY1 As Single
Private msngXRatio As Single
Private msngYRatio As Single
Private maPointArray() As POINTAPI

'~~~Device
Property Set Device(objDevice As Object)
    Dim sngSX2 As Single
    Dim sngSY2 As Single
    Dim sngPX2 As Single
    Dim sngPY2 As Single
    Dim intScaleMode As Long
    Set mobjDevice = objDevice
    With mobjDevice
        'Grab current scaling parameters
        intScaleMode = .ScaleMode
        msngSX1 = .ScaleLeft
        msngSY1 = .ScaleTop
        sngSX2 = msngSX1 + .ScaleWidth
        sngSY2 = msngSY1 + .ScaleHeight
        'Temporarily set pixels mode
        .ScaleMode = vbPixels
        'Grab pixel scaling parameters
        sngPX2 = .ScaleWidth
        sngPY2 = .ScaleHeight
        'Reset user's original scale
        If intScaleMode = 0 Then
            mobjDevice.Scale (msngSX1, msngSY1)-(sngSX2, sngSY2)
        Else
            mobjDevice.ScaleMode = intScaleMode
        End If
        'Calculate scaling ratios just once
        msngXRatio = sngPX2 / (sngSX2 - msngSX1)
        msngYRatio = sngPY2 / (sngSY2 - msngSY1)
    End With
End Property

'~~~Point X,Y
Public Sub Point(sngX As Single, sngY As Single)
    Dim lngN As Long
    lngN = UBound(maPointArray) + 1
    ReDim Preserve maPointArray(lngN)
    maPointArray(lngN).X = XtoP(sngX)
    maPointArray(lngN).Y = YtoP(sngY)
End Sub

'~~~Draw
Public Sub Draw()
  Polyline mobjDevice.hdc, maPointArray(1), UBound(maPointArray)
  ReDim maPointArray(0)
End Sub

Public Sub Clear()
  ReDim maPointArray(0)
End Sub
'Scales X value to pixel location
Private Function XtoP(sngX As Single) As Long
    XtoP = (sngX - msngSX1) * msngXRatio
End Function

'Scales Y value to pixel location
Private Function YtoP(sngY As Single) As Long
    YtoP = (sngY - msngSY1) * msngYRatio
End Function

'Initialization
Private Sub Class_Initialize()
    ReDim maPointArray(0)
End Sub


