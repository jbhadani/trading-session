VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTriggers 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Bevakningslista"
   ClientHeight    =   5670
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9300
   Icon            =   "frmTriggers.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5670
   ScaleWidth      =   9300
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame5 
      Caption         =   "Startkontroll"
      Height          =   735
      Left            =   7320
      TabIndex        =   19
      Top             =   1680
      Width           =   1935
      Begin VB.CheckBox Check1 
         Caption         =   "Starta med kontroll"
         Height          =   375
         Left            =   120
         TabIndex        =   20
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.PictureBox Picture2 
      Height          =   615
      Left            =   240
      Picture         =   "frmTriggers.frx":014A
      ScaleHeight     =   555
      ScaleWidth      =   8835
      TabIndex        =   18
      Top             =   120
      Width           =   8895
   End
   Begin VB.Frame Frame4 
      Caption         =   "F�rgkoder"
      Height          =   2295
      Left            =   7320
      TabIndex        =   7
      Top             =   2520
      Width           =   1935
      Begin VB.PictureBox Picture1 
         Height          =   255
         Index           =   4
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   195
         TabIndex        =   12
         Top             =   1800
         Width           =   255
      End
      Begin VB.PictureBox Picture1 
         Height          =   255
         Index           =   3
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   195
         TabIndex        =   11
         Top             =   1440
         Width           =   255
      End
      Begin VB.PictureBox Picture1 
         Height          =   255
         Index           =   2
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   195
         TabIndex        =   10
         Top             =   1080
         Width           =   255
      End
      Begin VB.PictureBox Picture1 
         Height          =   255
         Index           =   1
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   195
         TabIndex        =   9
         Top             =   720
         Width           =   255
      End
      Begin VB.PictureBox Picture1 
         Height          =   255
         Index           =   0
         Left            =   120
         ScaleHeight     =   195
         ScaleWidth      =   195
         TabIndex        =   8
         Top             =   360
         Width           =   255
      End
      Begin VB.Label Label1 
         Caption         =   "Undre gr�ns"
         Height          =   255
         Index           =   4
         Left            =   480
         TabIndex        =   17
         Top             =   1840
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Undre 5% zon"
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   16
         Top             =   1480
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "Neutral"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   15
         Top             =   1120
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "�vre 5% zon"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   14
         Top             =   760
         Width           =   1335
      End
      Begin VB.Label Label1 
         Caption         =   "�vre gr�ns"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   13
         Top             =   400
         Width           =   1335
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   4920
      Width           =   9255
      Begin VB.CommandButton cmbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   7440
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmbOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   600
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Funktioner"
      Height          =   735
      Left            =   7320
      TabIndex        =   1
      Top             =   840
      Width           =   1935
      Begin VB.CommandButton cmbAdd 
         Caption         =   "L�gg till..."
         Height          =   375
         Left            =   360
         TabIndex        =   4
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Bevakningslista"
      Height          =   3975
      Left            =   0
      TabIndex        =   0
      Top             =   840
      Width           =   7215
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   3615
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   6975
         _ExtentX        =   12303
         _ExtentY        =   6376
         _Version        =   393216
         Rows            =   10
         Cols            =   6
         FixedCols       =   0
         SelectionMode   =   1
      End
   End
End
Attribute VB_Name = "frmTriggers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cmbAdd_Click()
  frmTriggers.Enabled = False
  With frmTriggerRegistration
    '.cboObject.Locked = False
    .lblObject.Enabled = True
    .treObjects.Enabled = True
    .Caption = "Nytt bevakningsobjekt"
    .Show
  End With
End Sub

Private Sub cmbCancel_Click()
  Dim FileName As String
  
  FileName = Path + "triggers.dta"
  W_Triggers FileName, Triggers()
  Unload Me
End Sub

Private Sub cmbOK_Click()
  Dim FileName As String
  FileName = Path + "triggers.dta"
  W_Triggers FileName, Triggers()
  
  
  If frmTriggers.Check1 = 1 Then
    SaveSetting App.Title, "Trigger", "StartCheck", "True"
  Else
    SaveSetting App.Title, "Trigger", "StartCheck", "False"
  End If
  Unload Me
End Sub

Private Sub Form_Load()
  Dim FileName As String
  
  If GetSetting(App.Title, "Trigger", "StartCheck", "True") = "True" Then
    frmTriggers.Check1 = 1
  Else
    frmTriggers.Check1 = 0
  End If
  top = 300
  left = 500
  With MSFlexGrid1
    .ColWidth(0) = 500
    .ColWidth(1) = 2000
    .ColWidth(2) = 1000
    .ColWidth(3) = 1000
    .ColWidth(4) = 1000
    .ColWidth(5) = 1100
    .Rows = 1
    .Row = 0
    .Col = 0
    .Text = "Larm"
    .Col = 1
    .Text = "Aktie"
    .Col = 2
    .Text = "Senast"
    .Col = 3
    .Text = "�vre gr�ns"
    .Col = 4
    .Text = "Undre gr�ns"
    .Col = 5
    .Text = "Registrerad"
  End With
  Picture1(0).BackColor = QBColor(2)
  Picture1(1).BackColor = QBColor(10)
  Picture1(2).BackColor = QBColor(15)
  Picture1(3).BackColor = QBColor(12)
  Picture1(4).BackColor = QBColor(4)
  
  FileName = Path + "triggers.dta"
  
  R_Triggers FileName, Triggers()
  
  UpdateGrid
End Sub

Private Sub MSFlexGrid1_DblClick()
  Dim Index As Long
  
  
  
  Index = MSFlexGrid1.RowSel
  
  If Index >= 1 Then
    frmTriggers.Enabled = False
    With frmTriggerRegistration
      '.cboObject.Text = Triggers(Index).Name
      .lblObject.Caption = Triggers(Index).Name
      .lblObject.Enabled = False
      .treObjects.Enabled = False
      .Caption = "�ndra larmniv�"
      '.cboObject.Locked = True
      .txtClose = Triggers(Index).Close
      .DTPicker1.Value = GetDateString(Triggers(Index).Date)
      .DTPicker1.Enabled = True
      '.txtDate = Triggers(Index).Date
      '.txtDate.Locked = False
      .txtUpperLevel = Triggers(Index).UpperTriggerLevel
      .txtLowerLevel = Triggers(Index).LowerTriggerLevel
      .Show
    End With
  End If
End Sub

Private Sub MSFlexGrid1_KeyUp(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyDelete Then
    DeleteTrigger MSFlexGrid1.RowSel
  End If
End Sub
