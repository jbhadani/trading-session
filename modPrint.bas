Attribute VB_Name = "modPrint"
Option Explicit


Type ExtendedRectType
  left As Long
  right As Long
  top As Long
  bottom As Long
  Width As Long
  Height As Long
End Type

Type PrintGraphType
  Draw As ExtendedRectType
  
  LabelYLeft As Long
  
  ScaleX As Single
  ScaleY As Single
  
  IndexFirst As Long
  IndexLast As Long
  
  ValueMinY As Single
  ValueMaxY As Single
  ValueScaleStepY As Single
  ValueNbrOfStepsY As Long
  ValueNbrOfSmallStepsY As Long
  
  ShowSmallSteps As Boolean
  ShowCurrencyFomat As Boolean
End Type

Const LabelYFontSize As Long = 8
Const ObjectNameFontSize As Long = 12
Const LabelYLeftMarginDistance As Long = 950
Const DrawRightMarginDistance As Long = 1000



Sub OpenPreview()
  CreatePage True, False, True
End Sub

Sub OpenPrint()
  If frmPrint.VSPrinter1.PrintDialog(pdPrinterSetup) = True Then
    CreatePage False, True, True
  End If
  EnableAllTools
End Sub

Sub CreatePage(blnPreviewPage As Boolean, blnPrintPage As Boolean, blnSetStartAndEnd As Boolean)
Dim i As Long

Dim pg As PrintGraphType
Dim Draw As ExtendedRectType
Dim Prices As New CPriceData
On Error GoTo Err

  GetPrintSettingsFromRegistry frmPrint.VSPrinter1
  Prices.Prices = PriceDataArray
  Prices.Name = Trim(InvestInfoArray(ActiveIndex).Name)
  Prices.Index = ActiveIndex
  
  If blnSetStartAndEnd Then
    'Set DateTimePicker properties
    With frmPrint.dtpFirst
    .MinDate = 0
    .MaxDate = 200000
    .MinDate = Int(Prices.Price(GraphSettings.DWM, Prices.IndexFirst(GraphSettings.DWM)).Date)
    .MaxDate = Int(Prices.Price(GraphSettings.DWM, Prices.IndexLast(GraphSettings.DWM)).Date)
    'Set first the date visible in the graph
    .Value = Int(Prices.Price(GraphSettings.DWM, GetGraphStartIndex()).Date)
    End With
    With frmPrint.dtplast
    .MinDate = 0
    .MaxDate = 200000
    .MinDate = Int(Prices.Price(GraphSettings.DWM, Prices.IndexFirst(GraphSettings.DWM)).Date)
    .MaxDate = Int(Prices.Price(GraphSettings.DWM, Prices.IndexLast(GraphSettings.DWM)).Date)
    'Set last the date visible in the graph
    .Value = Int(Prices.Price(GraphSettings.DWM, GetGraphStopIndex()).Date)
    End With
  End If
  
  With frmPrint
  If blnPreviewPage Then
    .Caption = "Utskrift [" & Prices.Name & "]"
    .Show
  End If
  
  End With
With frmPrint.VSPrinter1

'.PaperSize = pprA4
'.Orientation = orLandscape

Draw.left = .MarginLeft
Draw.right = .PageWidth - .MarginRight - DrawRightMarginDistance
Draw.top = .MarginTop
Draw.bottom = .PageHeight - .MarginBottom
Draw.Width = Draw.right - Draw.left
Draw.Height = Draw.bottom - Draw.top
 
 
pg.IndexFirst = Prices.IndexFromDateNear(GraphSettings.DWM, frmPrint.dtpFirst.Value)
pg.IndexLast = Prices.IndexFromDateNear(GraphSettings.DWM, frmPrint.dtplast.Value)
  
Debug.Print "Print: Start= " & CStr(Prices.Price(GraphSettings.DWM, pg.IndexFirst).Date)
Debug.Print "Print: End  = " & CStr(Prices.Price(GraphSettings.DWM, pg.IndexLast).Date)

.ShowGuides = gdDesignTime 'gdShow
.Preview = Not blnPrintPage
.StartDoc
.DocName = Trim(Prices.Name) & " - " & App.Title

.FontBold = True
.TextAlign = taLeftBottom
.TextBox Prices.Name, .MarginLeft, .MarginHeader, .PageWidth - .MarginLeft - .MarginRight, .MarginTop - .MarginHeader
.TextAlign = taLeftTop

'==============================================================================
' Price graph
'==============================================================================
pg.Draw = Draw
pg.Draw.bottom = 0.79 * Draw.bottom
pg.Draw.Height = pg.Draw.bottom - pg.Draw.top

pg.LabelYLeft = .PageWidth - .MarginRight - LabelYLeftMarginDistance


Prices.GetMinAndMax GraphSettings.DWM, pg.IndexFirst, pg.IndexLast, pg.ValueMinY, pg.ValueMaxY

If GraphSettings.ExponentialPrice Then
  pg.ValueScaleStepY = (pg.ValueMaxY - pg.ValueMinY) / 10
  pg.ValueNbrOfStepsY = 10
  pg.ValueNbrOfSmallStepsY = 2
Else
  GetScale pg.ValueMinY, pg.ValueMaxY, 10, pg.ValueMinY, pg.ValueScaleStepY, pg.ValueNbrOfStepsY, pg.ValueNbrOfSmallStepsY
  pg.ValueMaxY = pg.ValueMinY + pg.ValueScaleStepY * pg.ValueNbrOfStepsY
End If

pg.ScaleY = pg.Draw.Height / (pg.ValueMaxY - pg.ValueMinY)
pg.ScaleX = pg.Draw.Width / (pg.IndexLast - pg.IndexFirst + 1)

pg.ShowCurrencyFomat = True
pg.ShowSmallSteps = GraphSettings.ShowMinXGrid

PlotGraphBorder frmPrint.VSPrinter1, pg

If GraphSettings.ShowHLC Then
  PlotGraphHLC frmPrint.VSPrinter1, pg, Prices
Else
  PlotGraphLine frmPrint.VSPrinter1, pg, Prices
End If

PlotGridHorizontal frmPrint.VSPrinter1, pg
If GraphSettings.ShowYGrid Then
  PlotGridVertical frmPrint.VSPrinter1, pg, Prices, True
End If

PlotTrendlines frmPrint.VSPrinter1, pg, Prices, TrendLineArray
PlotLabels frmPrint.VSPrinter1, pg, Prices
'==============================================================================
' Volume graph
'==============================================================================
pg.Draw = Draw
pg.Draw.top = 0.8 * Draw.bottom
pg.Draw.Height = pg.Draw.bottom - pg.Draw.top

pg.LabelYLeft = .PageWidth - .MarginRight - LabelYLeftMarginDistance

Prices.GetMinAndMaxVolume GraphSettings.DWM, pg.IndexFirst, pg.IndexLast, pg.ValueMinY, pg.ValueMaxY

If GraphSettings.ExponentialPrice Then
  pg.ValueScaleStepY = (pg.ValueMaxY - pg.ValueMinY) / 10
  pg.ValueNbrOfStepsY = 10
  pg.ValueNbrOfSmallStepsY = 2
Else
  GetScale pg.ValueMinY, pg.ValueMaxY, 5, pg.ValueMinY, pg.ValueScaleStepY, pg.ValueNbrOfStepsY, pg.ValueNbrOfSmallStepsY
  pg.ValueMaxY = pg.ValueMinY + pg.ValueScaleStepY * pg.ValueNbrOfStepsY
End If

pg.ScaleY = pg.Draw.Height / (pg.ValueMaxY - pg.ValueMinY)

pg.ShowCurrencyFomat = False
pg.ShowSmallSteps = False

PlotGraphBorder frmPrint.VSPrinter1, pg
PlotGraphVolume frmPrint.VSPrinter1, pg, Prices

PlotGridHorizontal frmPrint.VSPrinter1, pg
If GraphSettings.ShowYGrid Then
  PlotGridVertical frmPrint.VSPrinter1, pg, Prices, False
End If
.EndDoc

End With

If Not blnPreviewPage Then
  EnableAllTools
End If
Err:

End Sub

Sub PlotGraphBorder(ByRef VSP As VSPrinter, _
                  pg As PrintGraphType)

With VSP
.PenColor = 0
.PenStyle = psSolid
.PenWidth = 1
.BrushStyle = bsTransparent

.DrawRectangle pg.Draw.left, pg.Draw.top, pg.Draw.right, pg.Draw.bottom
End With
End Sub


Sub PlotGraphLine(ByRef VSP As VSPrinter, _
                  pg As PrintGraphType, _
                  Prices As CPriceData)
Dim i As Long
With pg


VSP.DrawLine .Draw.left + (0) * .ScaleX, _
          .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, .IndexFirst).Close - .ValueMinY), _
          .Draw.left + (1) * pg.ScaleX, _
          .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, .IndexFirst + 1).Close - .ValueMinY)
For i = .IndexFirst + 1 To .IndexLast
  VSP.DrawLine .Draw.left + (i - .IndexFirst) * .ScaleX, .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, i).Close - .ValueMinY)
Next i
End With
End Sub


Sub PlotGraphHLC(ByRef VSP As VSPrinter, _
                  pg As PrintGraphType, _
                  Prices As CPriceData)
Dim i As Long
With pg
VSP.BrushStyle = bsSolid
For i = .IndexFirst + 1 To .IndexLast
  VSP.DrawRectangle .Draw.left + (i - .IndexFirst) * .ScaleX, _
  .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, i).High - .ValueMinY), _
  .Draw.left + (i - .IndexFirst) * .ScaleX + (.ScaleX / 2), _
  .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, i).Low - .ValueMinY)
Next i
End With
End Sub

Sub PlotGridHorizontal(ByRef VSP As VSPrinter, _
                       pg As PrintGraphType)
Dim i As Long
Dim strTemp As String
With pg
For i = 1 To .ValueNbrOfStepsY * .ValueNbrOfSmallStepsY - 1
    If GraphSettings.ShowXGrid Then
      If (i Mod .ValueNbrOfSmallStepsY) = 0 Then
        VSP.PenStyle = psSolid
        VSP.DrawLine .Draw.left, .Draw.bottom - Int(.ValueScaleStepY * (i / .ValueNbrOfSmallStepsY) * .ScaleY), _
                  .Draw.right, .Draw.bottom - Int(.ValueScaleStepY * (i / .ValueNbrOfSmallStepsY) * .ScaleY)
      ElseIf .ShowSmallSteps Then
        VSP.PenStyle = psDot
        VSP.DrawLine .Draw.left, .Draw.bottom - Int((.ValueScaleStepY / .ValueNbrOfSmallStepsY) * i * .ScaleY), _
                  .Draw.right, .Draw.bottom - Int((.ValueScaleStepY / .ValueNbrOfSmallStepsY) * i * .ScaleY)
      End If
    End If
    
    If (i Mod .ValueNbrOfSmallStepsY) = 0 Then
      VSP.FontSize = LabelYFontSize
      VSP.FontBold = False
      If .ShowCurrencyFomat Then
        strTemp = FormatNumber(.ValueMinY + .ValueScaleStepY * (i / .ValueNbrOfSmallStepsY), 2)
      Else
        strTemp = FormatNumber(.ValueMinY + .ValueScaleStepY * (i / .ValueNbrOfSmallStepsY), 0)
      End If
      VSP.TextBox strTemp, _
               .LabelYLeft, .Draw.bottom - Int(.ValueScaleStepY * (i / .ValueNbrOfSmallStepsY) * .ScaleY) - VSP.TextHeight(strTemp) / 2, _
               1000, 1000
    End If
    
   Next i
End With
End Sub

Sub PlotGraphVolume(ByRef VSP As VSPrinter, _
                  pg As PrintGraphType, _
                  Prices As CPriceData)
Dim i As Long
With pg
VSP.BrushStyle = bsSolid
For i = .IndexFirst + 1 To .IndexLast
  VSP.DrawRectangle .Draw.left + (i - .IndexFirst) * .ScaleX, _
  .Draw.bottom, _
  .Draw.left + (i - .IndexFirst) * .ScaleX + (.ScaleX / 2), _
  .Draw.bottom - .ScaleY * (Prices.Price(GraphSettings.DWM, i).Volume - .ValueMinY)
Next i
End With
End Sub


Sub PlotGridVertical(ByRef VSP As VSPrinter, _
                     pg As PrintGraphType, _
                     Prices As CPriceData, _
                     blnText As Boolean)
Dim CurrentDate As Date
Dim LastYLineDate As Date
Dim LastXCoord As Long
Dim j As Long
Dim strXLabel As String
Dim PlotLabel As Boolean
  
  With pg
  LastYLineDate = Prices.Price(GraphSettings.DWM, .IndexFirst).Date
  VSP.PenWidth = 1
  VSP.PenStyle = psSolid
  For j = .IndexFirst To .IndexLast
    PlotLabel = False
    CurrentDate = Prices.Price(GraphSettings.DWM, j).Date

    Select Case GraphSettings.DWM
    Case dwmDaily
      If Month(LastYLineDate) <> Month(CurrentDate) Then
        PlotLabel = True
      End If
    Case dwmWeekely
      If ((Month(LastYLineDate) < 7) And (Month(CurrentDate) >= 7)) Or _
      ((Month(LastYLineDate) >= 7) And (Month(CurrentDate) < 7)) Then
        PlotLabel = True
      End If
    Case dwmMonthly
      If Year(LastYLineDate) <> Year(CurrentDate) Then
        PlotLabel = True
      End If
    End Select
    
    If PlotLabel Then
      VSP.DrawLine .Draw.left + (j - .IndexFirst) * .ScaleX, .Draw.bottom, _
                     .Draw.left + (j - .IndexFirst) * .ScaleX, .Draw.top
      If blnText Then
        'Print the Date
        VSP.CurrentY = .Draw.top + 2
        VSP.CurrentX = .Draw.left + (j - .IndexFirst) * .ScaleX + 50
        strXLabel = GetYearMonthString(CurrentDate)
        If VSP.CurrentX > (LastXCoord + 50) And (VSP.CurrentX + VSP.TextWidth(strXLabel)) < pg.Draw.right Then
         LastXCoord = VSP.CurrentX + VSP.TextWidth(strXLabel)
         VSP.Text = strXLabel
        End If
      End If
      LastYLineDate = CurrentDate
    End If
  Next j
  End With
End Sub

Sub PlotTrendlines(ByRef VSP As VSPrinter, _
                     pg As PrintGraphType, _
                     Prices As CPriceData, _
                     Trendlines() As TrendLineType)
Dim i As Long
Dim IndexFirst As Long
Dim IndexLast As Long
Dim PriceFirst As Single
Dim PriceLast As Single
Dim PricePerIndex As Double
Dim IndexPerPrice As Double
Dim PlotTrend As RECT
Dim IsVertical As Boolean
Dim IsHorizontal As Boolean

VSP.PenWidth = 1
VSP.PenStyle = psSolid
With pg
For i = 1 To UBound(Trendlines, 2)
  If Trendlines(GraphSettings.DWM, i).InUse Then
    IndexFirst = Prices.IndexFromDateNear(GraphSettings.DWM, Trendlines(GraphSettings.DWM, i).StartDate)
    IndexLast = Prices.IndexFromDateNear(GraphSettings.DWM, Trendlines(GraphSettings.DWM, i).EndDate)
    
    PriceFirst = Trendlines(GraphSettings.DWM, i).StartPrice
    PriceLast = Trendlines(GraphSettings.DWM, i).EndPrice
    
    PlotTrend.left = .Draw.left + (IndexFirst - .IndexFirst) * .ScaleX
    PlotTrend.bottom = .Draw.bottom - (PriceFirst - .ValueMinY) * .ScaleY
    PlotTrend.right = .Draw.left + (IndexLast - .IndexFirst) * .ScaleX
    PlotTrend.top = .Draw.bottom - (PriceLast - .ValueMinY) * .ScaleY
    
    IsHorizontal = False
    IsVertical = False
    If IndexLast = IndexFirst Then
      IsHorizontal = True
    Else
      PricePerIndex = (PriceLast - PriceFirst) / (IndexLast - IndexFirst)
    End If
    If PriceLast = PriceFirst Then
      IsVertical = True
    Else
      IndexPerPrice = (IndexLast - IndexFirst) / (PriceLast - PriceFirst)
    End If
    
    'Left border
    If PlotTrend.left < .Draw.left Then
      PlotTrend.left = .Draw.left
      If Not IsHorizontal Then
        PlotTrend.bottom = .Draw.bottom - (((.IndexFirst - IndexFirst) * PricePerIndex) + PriceFirst - .ValueMinY) * .ScaleY
      End If
    End If
    If PlotTrend.right < .Draw.left Then
      PlotTrend.right = .Draw.left
      If Not IsHorizontal Then
        PlotTrend.top = .Draw.bottom - (((.IndexFirst - IndexLast) * PricePerIndex) + PriceLast - .ValueMinY) * .ScaleY
      End If
    End If
    
    'Right border
    If PlotTrend.right > .Draw.right Then
      PlotTrend.right = .Draw.right
      If Not IsHorizontal Then
        PlotTrend.top = .Draw.bottom - ((-(IndexLast - .IndexLast) * PricePerIndex) + PriceLast - .ValueMinY) * .ScaleY
      End If
    End If
    If PlotTrend.left > .Draw.right Then
      PlotTrend.left = .Draw.right
      If Not IsHorizontal Then
        PlotTrend.bottom = .Draw.bottom - ((-(IndexFirst - .IndexLast) * PricePerIndex) + PriceFirst - .ValueMinY) * .ScaleY
      End If
    End If
    
    'Top border
    If PlotTrend.top < .Draw.top Then
      PlotTrend.top = .Draw.top
      If Not IsVertical Then
        PlotTrend.right = .Draw.left + (IndexLast - (PriceLast - .ValueMaxY) * IndexPerPrice - .IndexFirst) * .ScaleX
      End If
    End If
    If PlotTrend.bottom < .Draw.top Then
      PlotTrend.bottom = .Draw.top
      If Not IsVertical Then
        PlotTrend.left = .Draw.left + (IndexFirst - (PriceFirst - .ValueMaxY) * IndexPerPrice - .IndexFirst) * .ScaleX
      End If
    End If
    
    'Bottom border
    If PlotTrend.top > .Draw.bottom Then
      PlotTrend.top = .Draw.bottom
      If Not IsVertical Then
        PlotTrend.right = .Draw.left + (IndexLast - (.ValueMinY - PriceLast) * IndexPerPrice - .IndexFirst) * .ScaleX
      End If
    End If
    If PlotTrend.bottom > .Draw.bottom Then
      PlotTrend.bottom = .Draw.bottom
      If Not IsVertical Then
        PlotTrend.left = .Draw.left + (IndexFirst - (.ValueMinY - PriceFirst) * IndexPerPrice - .IndexFirst) * .ScaleX
      End If
    End If
    
    VSP.DrawLine PlotTrend.left, PlotTrend.bottom, _
                 PlotTrend.right, PlotTrend.top
    
    Debug.Print "Trendline " & CStr(i) & " is in use"
    'pg.IndexFirst
  End If
Next i
End With
End Sub

Sub PlotLabels(ByRef VSP As VSPrinter, _
                     pg As PrintGraphType, _
                     Prices As CPriceData)
  Dim LabelHandle As Long
  Dim dtDate As Date
  Dim Price As Single
  Dim Text As Variant
  Dim Index As Long
  Dim TextWidth As Double
  Dim TextLeft As Double
  LabelHandle = 0
  Do
      LabelHandle = EnumStockLabels(ActiveStockLabelsHandle, LabelHandle)
      If LabelHandle <> 0 Then
          GetStockLabelInfo ActiveStockLabelsHandle, LabelHandle, dtDate, Price, Text
          With pg
          Index = Prices.IndexFromDateNear(GraphSettings.DWM, dtDate)
          TextWidth = VSP.TextWidth(Text)
          TextLeft = .Draw.left + (Index - .IndexFirst) * .ScaleX
          'MsgBox (Text & ", " & Price & ", " & dtDate)
          
          If Index > .IndexFirst And (TextLeft + TextWidth) < .Draw.right Then
            VSP.TextBox Text, TextLeft, .Draw.bottom - (Price - .ValueMinY) * .ScaleY, TextWidth, 1000
          End If
          End With
      End If
  Loop While LabelHandle <> 0
End Sub

Sub SavePrintSettingsToRegistry(VSP As VSPrinter)
Debug.Print "SavePrintSettingsToRegistry"
With VSP
  SaveSetting App.Title, "PrintSettings", "Orientation", CStr(.Orientation)
  SaveSetting App.Title, "PrintSettings", "PaperSize", CStr(.PaperSize)
  SaveSetting App.Title, "PrintSettings", "PaperBin", CStr(.PaperBin)
End With
End Sub

Sub GetPrintSettingsFromRegistry(VSP As VSPrinter)
Debug.Print "GetPrintSettingsFromRegistry"
With VSP
  VSP.Orientation = CLng(GetSetting(App.Title, "PrintSettings", "Orientation", CStr(orLandscape)))
  VSP.PaperSize = CLng(GetSetting(App.Title, "PrintSettings", "PaperSize", CStr(pprA4)))
  VSP.PaperBin = CLng(GetSetting(App.Title, "PrintSettings", "Paperbin", CStr(binAuto)))
End With
End Sub
