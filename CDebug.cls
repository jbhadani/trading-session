VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
Dim mlngCallStackDepth As Long
Private Sub Class_Initialize()
  mlngCallStackDepth = -1
End Sub

Public Sub Enter(strText As String)
  mlngCallStackDepth = mlngCallStackDepth + 1
  Out "->" & strText, True
End Sub

Public Sub Leave(strText As String)
  Out "<-" & strText, True
  mlngCallStackDepth = mlngCallStackDepth - 1
End Sub

Public Sub Out(strText As String, Optional blnNoBlanks As Boolean)
Dim I As Long
  For I = 0 To mlngCallStackDepth
    Debug.Print "  ";
  Next I
  If Not blnNoBlanks = True Then
    Debug.Print "  ";
  End If
  
  Debug.Print strText

End Sub
