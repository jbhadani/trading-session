Attribute VB_Name = "Action"
Option Explicit
Sub ActionNewPortfolio()
  NewPortfolio
End Sub
Sub ActionOpenPortfolio()
  OpenPortfolio
End Sub
  
Sub ActionSavePortfolio()
  SavePortfolio
End Sub
  
Sub ActionSavePortfolioAs()
  SavePortfolioAs
End Sub

Sub ActionZoomOut()
  With GraphSettings
  If .lngDaySpace > 1 Then
    .lngDaySpace = .lngDaySpace - 1
    UpdateGraph ActiveIndex
  Else
    'ErrorMessageStr "Det g�r inte att zooma in mer."
  End If
  End With
End Sub

Sub ActionZoomIn()
  GraphSettings.lngDaySpace = GraphSettings.lngDaySpace + 1
  UpdateGraph ActiveIndex
End Sub

Sub ActionIncreaseDayWidth()
  GraphSettings.lngDayWidth = GraphSettings.lngDayWidth + 1
  UpdateGraph ActiveIndex
End Sub

Sub ActionDecreaseDayWidth()
  With GraphSettings
  If .lngDayWidth > 1 Then
    .lngDayWidth = .lngDayWidth - 1
    UpdateGraph ActiveIndex
  Else
    'WAS: ErrorMessageStr ""
    ErrorMessageStr "Det g�r inte att minska bredden mer."

  End If
  End With
End Sub

Sub ActionSearch()
  If PriceDataArraySize > 0 And InvestInfoArraySize > 0 Then
    frmSearch.Show
  End If

End Sub

Sub ActionTest()
  If PriceDataArraySize >= 2 And InvestInfoArraySize > 0 Then
    frmIndicatortest.Show
  Else
    'WAS: ErrorMessageStr ""
    ErrorMessageStr "Det g�r inte att g�ra ett indikatortest eftersom det finns mindre �n 2 dagskurser lagrade f�r det aktiva objektet. 'LoadResString(S0_Det_g_r_inte_att_g_)"

  End If
End Sub

Sub ActionOptimize()
  If PriceDataArraySize > 0 And InvestInfoArraySize > 0 Then
    frmOptimize.Show
  End If
End Sub

Sub ActionObjectChange()
Dim I As Long
Dim j As Long
With MainMDI.ID_Object
  
  If ActiveIndex <> GetInvestIndex(.List(.ListIndex)) Then
    ActiveIndex = GetInvestIndex(.List(.ListIndex))
    FreeTrendLineArray GraphSettings.DWM
    HideAllTrendlines
    If ActiveIndex = -1 Then ActiveIndex = 0
    
      
    
    If ActiveIndex <> 0 Then
      If Not UseGeneralIndicatorParameters Then
        GetParameterSettingsFromInvest ActiveIndex
        If frmParameters.Visible Then
          InitParameters
        End If
      End If
      R_PriceDataArray DataPath + Trim(InvestInfoArray(ActiveIndex).FileName) + ".prc", PriceDataArray, PriceDataArraySize
  
      For I = 1 To TrendLineSeries
        For j = 1 To TrendlineSerieSize
          TrendLineArray(I, j) = WorkspaceSettingsArray(ActiveIndex).TrendLine(I, j)
        Next j
      Next I
      
      'Hide labels for current stock and show for new
      If ActiveStockLabelsHandle <> Empty Then
        HideStockLabels ActiveStockLabelsHandle
        ActiveStockLabelsHandle = Empty
      End If
  
      
      GraphInfoChanged = True
      If ShowGraph = True Then
        If PriceDataArraySize > 2 Then
          UpdateGraph ActiveIndex
        Else
          'WAS: InformationalMessage "Det m�ste finnas kurser f�r minst tv� dagar registrerade."
          InformationalMessage "Det m�ste finnas kurser f�r minst tv� dagar registrerade."      'LoadResString(S2_Det_m_ste_finnas_ku)

          Unload Frm_OwnGraph
        End If
      End If
    End If
  End If
  
  ' Update indicatorform if it is visible
  If IndicatortestVisible = True Then
    UpdateIndicatortest TestOK
  End If
  End With
End Sub

Sub ActionShowGraph()
   DoEvents
   ShowGraphForm
End Sub

Sub ActionQuotes()
  If PriceDataArraySize <> 0 Then
    Frm_Prices.Show
  Else
   InformationalMessage "Det finns inga kurser f�r objektet"
  End If
End Sub

Sub ActionDayQuotes()
  frmStockList.Show
End Sub

Sub ActionDayQuotesSettings()
  frmStockListSettings.Show
End Sub

Sub ActionColorSettings()
  frmColorSettings.Show 0
End Sub

Sub ActionGraphSettings()
  'frmGraphSettings.Show
End Sub

Sub ActionReport()
  Dim LatestDate As Date
    With Frm_RepList
    .DTPicker1.Value = Date
    .Combo1.ListIndex = 0
    InitTransaktioner
    .cbChangeTransaction.Enabled = True
    .cbDeleteTransaction.Enabled = True
      
    .Show 0
  ReportWindowOpen = True
  End With
  
  On Error GoTo 0
End Sub

Sub ActionHelpContents()
  'Const HELP_CONTENTS = 3 ' HELP_CONTENTS is still kept in WinHelp for backward compatibility with 16-bit Help files
  'Const HELP_FINDER = 11 ' HELP_FINDER command is preferred for 32-bit Help files
  'Call WinHelp(MainMDI.hWnd, App.HelpFile, HELP_FINDER, 0)
  SendKeys "{F1}"
  ' Doesn't work with UAC (User Access Control)
  'SendKeys "{F1}", True
End Sub

Sub ActionAbout()
  Frm_About.Show 0
End Sub

' Checks for updates for main program
Sub ActionCheckForUpdates(ByVal CheckOnStartup As Boolean)
  '#If PRO = 0 Then
  '  InformationalMessage "Denna funktion finns f�r tillf�llet endast i PRO-versionen"
  '#Else
    CheckForUpdates CheckOnStartup
  '#End If
End Sub

Sub ActionParameterSettings()
  If InvestInfoArraySize = 0 And Not UseGeneralIndicatorParameters Then
    ErrorMessageStr "Det g�r inte att visa objektberoende parametrar."
  Else
    frmParameters.Show
  End If
End Sub

Sub ActionExit()
  ' TODO: Add error handling?
On Error GoTo Err
  'MainMDI.SSActiveToolBars1.SaveLayout Path & ACTIVETOOLBARS_LAYOUT_FILE
    MainMDI.Toolbar1.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL1"
    MainMDI.Toolbar2.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL2"
    MainMDI.Toolbar4.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL4"
  Unload MainMDI
  End
Exit Sub
Err:
    End
End Sub

Sub ActionUpdate()
  If DEMO = 0 Then
    Update
  Else
    InformationalMessage "Denna funktion finns ej i demoversionen"
  End If
End Sub

Sub ActionFishNetSettings()
  frmFishSettings.Show 0
End Sub

Sub ActionFishNetEnabled(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_FishNetEnabled.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.blnShowFish = True
    MainMDI.ID_FishNetEnabled.Checked = True
  Else
    GraphSettings.blnShowFish = False
    MainMDI.ID_FishNetEnabled.Checked = False
    'MainMDI.SSActiveToolBars1.Tools("ID_ShowHighlight").Enabled = False
    'MainMDI.Toolbar1.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL1"
    'MainMDI.Toolbar2.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL2"
    'MainMDI.Toolbar4.SaveToolbar "", "Software\VB and VBA Program Settings\Trading Session 2 2\market Access", "TL4"
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub


Sub ActionTrigger()
  frmTriggers.Show
End Sub

Sub ActionDaily()
  GraphSettings.DWM = dwmDaily
  GraphInfoChanged = True
  'Hide labels for current stock and show for new
  If ActiveStockLabelsHandle <> Empty Then
    HideStockLabels ActiveStockLabelsHandle
    ActiveStockLabelsHandle = Empty
  End If
  UpdateGraph ActiveIndex
End Sub

Sub ActionWeekely()
  GraphSettings.DWM = dwmWeekely
  GraphInfoChanged = True
  'Hide labels for current stock and show for new
  If ActiveStockLabelsHandle <> Empty Then
    HideStockLabels ActiveStockLabelsHandle
    ActiveStockLabelsHandle = Empty
  End If
  UpdateGraph ActiveIndex
End Sub

Sub ActionMonthly()
  GraphSettings.DWM = dwmMonthly
  GraphInfoChanged = True
  'Hide labels for current stock and show for new
  If ActiveStockLabelsHandle <> Empty Then
    HideStockLabels ActiveStockLabelsHandle
    ActiveStockLabelsHandle = Empty
  End If
  UpdateGraph ActiveIndex
End Sub

Sub ActionOption()
  frmOptions.Show
End Sub


Sub ActionShowIndicator1(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_Ind1.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowIndicator1 = True
    MainMDI.ID_Indicator1.Enabled = True
    MainMDI.ID_Ind1.Checked = True
  Else
    GraphSettings.ShowIndicator1 = False
    MainMDI.ID_Indicator1.Enabled = False
    MainMDI.ID_Ind1.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowIndicator2(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_Ind2.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowIndicator2 = True
    MainMDI.ID_Indicator2.Enabled = True
    MainMDI.ID_Ind2.Checked = True
  Else
    GraphSettings.ShowIndicator2 = False
    MainMDI.ID_Indicator2.Enabled = False
    MainMDI.ID_Ind2.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionShowVolume(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowVolume.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If

  If Tool.Value = tbrPressed Then
    GraphSettings.ShowVolume = True
    MainMDI.ID_ShowVolume.Checked = True
  Else
    GraphSettings.ShowVolume = False
    MainMDI.ID_ShowVolume.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionShowBollinger(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowBollinger.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowBollinger = True
    MainMDI.ID_ShowBollinger.Checked = True
  Else
    GraphSettings.ShowBollinger = False
    MainMDI.ID_ShowBollinger.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionShowParabolic(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowParabolic.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If

  If Tool.Value = tbrPressed Then
    GraphSettings.ShowParabolic = True
    MainMDI.ID_ShowParabolic.Checked = True
  Else
    GraphSettings.ShowParabolic = False
    MainMDI.ID_ShowParabolic.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionShowColorSignalling(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowColorSignalling.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If

  If Tool.Value = tbrPressed Then
    GraphSettings.ShowSignalColor = True
    MainMDI.ID_ShowColorSignalling.Checked = True
  Else
    GraphSettings.ShowSignalColor = False
    MainMDI.ID_ShowColorSignalling.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionShowHLC(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowHLC.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowHLC = True
    MainMDI.ID_ShowHLC.Checked = True
  Else
    GraphSettings.ShowHLC = False
    MainMDI.ID_ShowHLC.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionIndicatorChanged()
  GraphSettings.strIndicator1 = MainMDI.ID_Indicator1.Text
  GraphSettings.strIndicator2 = MainMDI.ID_Indicator2.Text
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionSettings()
ChangeFormLang Frm_PrefNew
  Frm_PrefNew.Show vbModal, MainMDI
End Sub

Sub ActionTLDraw(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDraw
  Else
    TLDrawMode = dmNone
  End If
End Sub
Sub ActionTLDrawDates(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDrawDates
  Else
    TLDrawMode = dmNone
  End If
End Sub
Sub ActionTLDrawZ(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDrawZ
  Else
    TLDrawMode = dmNone
  End If
End Sub

Sub ActionTLDrawVer(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDrawVertical
  Else
    TLDrawMode = dmNone
  End If
End Sub

Sub ActionTLDrawHor(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDrawHorizontal
  Else
    TLDrawMode = dmNone
  End If
End Sub

Sub ActionTLMove(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmMove
  Else
    TLDrawMode = dmNone
  End If
End Sub
Sub ActionTLMoveZ(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmMoveZ
  Else
    TLDrawMode = dmNone
  End If
End Sub
Sub ActionTLDrawParallel(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmDrawParallel
  Else
    TLDrawMode = dmNone
  End If
End Sub
Sub ActionTLRemove(ByVal Tool As Button)
  'If Tool.Value = tbrPressed Then
    TLDrawMode = dmDelete
  'Else
  '  TLDrawMode = dmNone
  'End If
End Sub

Sub ActionTLText(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    TLDrawMode = dmText
  Else
    TLDrawMode = dmNone
  End If
End Sub

Sub ActionTLRemoveAll()
  DeleteAllTrendlines
End Sub

Sub ActionTLSave()
  SaveTrendlines
End Sub

Sub ActionTLColorRed()
  DrawSettings.LineColor = vbRed
End Sub

Sub ActionTLColorYellow()
  DrawSettings.LineColor = vbYellow
End Sub

Sub ActionTLColorGreen()
  DrawSettings.LineColor = vbGreen
End Sub

Sub ActionTLColorBlue()
  DrawSettings.LineColor = vbBlue
End Sub

Sub ActionTLColorCyan()
  DrawSettings.LineColor = vbCyan
End Sub

Sub ActionTLColorMagneta()
  DrawSettings.LineColor = vbMagenta
End Sub


Sub ActionTLColorGrey()
  DrawSettings.LineColor = RGB(128, 128, 128)
End Sub

Sub ActionTLColorBlack()
  DrawSettings.LineColor = vbBlack
End Sub

Sub ActionColorSignallingIndicatorChanged(ByVal Tool As ComboBox)
  GraphSettings.strColorSignallingIndicator = Tool.Text
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionGraphCloseWidthOnBar(ByVal Tool As menu)
  If Tool.Checked = False Then
    GraphSettings.lngCloseWidth = 0
    Tool.Checked = True
    MainMDI.ID_GraphCloseWidthOne.Checked = False
    MainMDI.ID_GraphCloseWidthTwo.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionGraphCloseWidthOne(ByVal Tool As menu)
  If Tool.Checked = True Then
    GraphSettings.lngCloseWidth = 1
    Tool.Checked = True
    MainMDI.ID_GraphCloseWidthOnBar.Checked = False
    MainMDI.ID_GraphCloseWidthTwo.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
Sub ActionGraphCloseWidthTwo(ByVal Tool As menu)
  If Tool.Checked = False Then
    GraphSettings.lngCloseWidth = 2
    Tool.Checked = True
    MainMDI.ID_GraphCloseWidthOnBar.Checked = False
    MainMDI.ID_GraphCloseWidthOne.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionStockTransaction()
  If GetPortfolioIndex(MainMDI.ID_Object.Text) <> -1 Then
    Frm_BuySell.Show
    Else
  ErrorMessageStr "Du m�ste v�lja ett objekt i den aktiva portf�ljen."
  End If
End Sub

Sub ActionDividend()
  If PortfolioArraySize > 0 Then
    frmStockDividend.Show 0
  Else
    ErrorMessageStr "Den finns inga objekt i portf�ljen som kan tillskrivas utdelningen."
  End If
End Sub

Sub ActionDepositWithdrawal()
  frmCashDeposit.Show 0
End Sub

Sub ActionScaleLineHorizontal(ByVal Tool As menu)
  If Tool.Checked = False Then
    GraphSettings.ShowXGrid = True
    Tool.Checked = True
  Else
    GraphSettings.ShowXGrid = False
    Tool.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionScaleLineMinHorizontal(ByVal Tool As menu)
  If Tool.Checked = False Then
    GraphSettings.ShowMinXGrid = True
    Tool.Checked = True
  Else
    GraphSettings.ShowMinXGrid = False
    Tool.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionScaleLineVertical(ByVal Tool As menu)
  If Tool.Checked = False Then
    GraphSettings.ShowYGrid = True
    Tool.Checked = True
  Else
    GraphSettings.ShowYGrid = False
    Tool.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionAddObjectToPortfolio()
If Not ActivePortfolio Then
    MainMDI.StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj: Namnl�s.prt")
    ActivePortfolioFileName = CheckLang("Namnl�s.prt")
    ActivePortfolio = True
    'MainMDI.M_Save_Port.Enabled = True
    'MainMDI.M_Save_Port_As.Enabled = True
  End If
  Frm_AddInvestToPort.Show 0
End Sub

Sub ActionGraphScaleExponential()
  GraphSettings.ExponentialPrice = True
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionGraphScaleLinear()
  GraphSettings.ExponentialPrice = False
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowHighlight(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.blnShowHighlight = True
  Else
    GraphSettings.blnShowHighlight = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub
'#If FISH = 1 Then
Sub ActionRemoveAllHighlights()
If FISH = True Then
  FreeHighlights
  UpdateGraph ActiveIndex
End If
End Sub
'#End If

Sub ActionShowMAV1(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowMAV1.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowPriceMAV1 = True
    MainMDI.ID_ShowMAV1.Checked = True
  Else
    GraphSettings.ShowPriceMAV1 = False
    MainMDI.ID_ShowMAV1.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowMAV2(ByVal Tool As Button, Optional Direct As Boolean = True)
If Direct = False Then
   If MainMDI.ID_ShowMAV2.Checked = False Then
        Tool.Value = tbrPressed
   Else
        Tool.Value = tbrUnpressed
   End If
End If
  If Tool.Value = tbrPressed Then
    GraphSettings.ShowPriceMAV2 = True
    MainMDI.ID_ShowMAV2.Checked = True
  Else
    GraphSettings.ShowPriceMAV2 = False
    MainMDI.ID_ShowMAV2.Checked = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionInformation()
  frmInformation.Show
End Sub

Sub ActionShowKeyReversalDay(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supKeyReversalDay).blnShow = True
  Else
    GraphSettings.Setup(supKeyReversalDay).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowReversalDay(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supReversalDay).blnShow = True
  Else
    GraphSettings.Setup(supReversalDay).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowOneDayReversal(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supOneDayReversal).blnShow = True
  Else
    GraphSettings.Setup(supOneDayReversal).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowTwoDayReversal(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supTwoDayReversal).blnShow = True
  Else
    GraphSettings.Setup(supTwoDayReversal).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowPatternGap(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supPatternGap).blnShow = True
  Else
    GraphSettings.Setup(supPatternGap).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowReversalGap(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.Setup(supReversalGap).blnShow = True
  Else
    GraphSettings.Setup(supReversalGap).blnShow = False
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionShowSupportResistance(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    GraphSettings.blnShowSupportResistance = True
    MainMDI.ID_ShowSupportResistance.Checked = True
  Else
    GraphSettings.blnShowSupportResistance = False
    MainMDI.ID_ShowSupportResistance.Checked = True
  End If
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Sub ActionPrint()
  DisableAllTools
  OpenPrint
End Sub

Sub ActionPreview()
  DisableAllTools
  OpenPreview
End Sub

Sub ActionPageLayout()
  GetPrintSettingsFromRegistry frmPrint.VSPrinter1
  If frmPrint.VSPrinter1.PrintDialog(pdPageSetup) Then
    SavePrintSettingsToRegistry frmPrint.VSPrinter1
  End If
  Unload frmPrint
End Sub

Sub ActionNextObject()
With MainMDI.ID_Object
  If .ListIndex < .ListCount - 1 Then
    .ListIndex = .ListIndex + 1
  End If
End With
End Sub
Sub ActionPrevObject()
With MainMDI.ID_Object
  If .ListIndex > 0 Then
    .ListIndex = .ListIndex - 1
  End If
End With
End Sub

Sub ActionShowToolbarSettings()
  frmToolbarSettings.Show
End Sub
#If 0 Then
Sub ActionWindowListNewFocus(ByVal Tool As Button)
' Window List Tools
  Dim iForms As Integer
  
  Debug.Print Tool.Group
  ' If the Tool that was clicked belongs to the WindowList group...
  If Tool.Group = "WindowList" Then
  ' then iterate through Forms collection and set focus to document
    For iForms = 0 To Forms.Count - 1

      If Forms(iForms).Caption = Tool.ID Then
        Forms(iForms).SetFocus
        ' Once a matching form is found, stop looking
        Exit For
      End If

    Next iForms
  End If
End Sub
#End If

Sub ActionTLFreeDraw(ByVal Tool As Button)
  If Tool.Value = tbrPressed Then
    FreeDraw = True
  Else
    FreeDraw = False
  End If
End Sub

