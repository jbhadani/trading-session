Attribute VB_Name = "FileProc"
Option Explicit

Sub NewPortfolio()
  
Dim Cancel As Boolean

NewPort = True
  If ActivePortfolio Then
    PromptForSave Cancel
  End If
  If Not Cancel Then
    MainMDI.StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj: Ingen")
    ActivePortfolioFileName = ""
    ActivePortfolio = False
    ReDim PortfolioArray(0)
    PortfolioArraySize = 0
    ReDim TransactionArray(0)
    TransactionArraySize = 0
    Frm_AddInvestToPort.Show 0
  End If
  blnPortfolioModified = True
End Sub

Sub OpenPortfolio()

  Dim Cancel As Boolean
  Dim FileName As String
  Dim OK As Boolean
  If ActivePortfolio Then
    PromptForSave Cancel
  End If
  If Not Cancel Then
    With MainMDI.CommonDialog1
      .FileName = ""
      .DefaultExt = "*.prt"
      .Filter = "Portf�ljfiler (*.prt)|*.prt"
      .FLAGS = cdlOFNHideReadOnly + cdlOFNFileMustExist + cdlOFNPathMustExist + cdlOFNNoDereferenceLinks
      .CancelError = True
      On Error GoTo ErrHandler
      .ShowOpen
      On Error GoTo 0
      FileName = .FileName
    End With
    DoOpenPortfolio FileName
  
    If ReportWindowOpen Then
      UpdateReportGrid
    End If
  End If
Exit Sub
ErrHandler:
  'Do nothing
  
End Sub

Sub DoOpenPortfolio(strFileName As String)
Dim OK As Boolean
If strFileName <> "" Then
  R_Portfolio_All strFileName, OK
  If OK Then
    ActivePortfolioFileName = strFileName
    ActivePortfolio = True
    With MainMDI
    .StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj:") + strFileName
    strFileName = Mid$(strFileName, 1, Len(strFileName) - 3) & "trn"
    R_Transaction_All strFileName
    
    InitObjectCombo
    
    .ID_SavePortfolio.Enabled = True
    .ID_SavePortfolioAs.Enabled = True
    .ID_StockTransaction.Enabled = True
    .ID_DepositWithdrawal.Enabled = True
    .ID_Dividend.Enabled = True
    .ID_Report.Enabled = True
    .ID_AddObjectToPortfolio.Enabled = True
    End With
    CheckIfInvestExist
    If ReportWindowOpen Then
      UpdateReportGrid
    End If
    blnPortfolioModified = False
  Else
    ErrorMessageStr "Filen �r ingen gilitg portf�ljfil."
    ActivePortfolio = False
    ReDim PortfolioArray(0)
    PortfolioArraySize = 0
    ReDim TransactionArray(0)
    TransactionArraySize = 0
    MainMDI.StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj: Ingen")
    InitObjectCombo
    With MainMDI
    .ID_SavePortfolio.Enabled = False
    .ID_SavePortfolioAs.Enabled = False
    .ID_StockTransaction.Enabled = False
    .ID_DepositWithdrawal.Enabled = False
    .ID_Dividend.Enabled = False
    .ID_Report.Enabled = False
    .ID_AddObjectToPortfolio.Enabled = False
    End With
     If ReportWindowOpen Then
      Unload Frm_RepList
    End If
  End If
End If
End Sub

Sub SavePortfolio()
   
Dim FileName As String
  
  FileName = ActivePortfolioFileName
  
  If FileName <> "Namnl�s.prt" Then               ' "not FileName =" was replaced in 1.0.2
    If ActivePortfolio Then
      W_Portfolio_All FileName
      FileName = Mid$(FileName, 1, Len(FileName) - 3) + "trn"
      W_Transaction_All FileName
    Else
      ErrorMessageStr "Det finns ingen portf�lj."
    End If
  Else
    ' A lot of dirty code was removed here in 1.0.2
    SavePortfolioAs                                 ' Added in 1.0.2
    blnPortfolioModified = False
  End If
End Sub

Sub SavePortfolioAs()
  Dim FileName As String
  With MainMDI.CommonDialog1
  .DefaultExt = "*.prt"
  .Filter = "Portf�ljfiler (*.prt)|*.prt"
  .FLAGS = cdlOFNHideReadOnly + cdlOFNFileMustExist + cdlOFNPathMustExist + cdlOFNNoDereferenceLinks
  .CancelError = True
  On Error GoTo ErrHandler:
  .ShowSave
  On Error GoTo 0
  FileName = MainMDI.CommonDialog1.FileName
  If FileName <> "" Then
    W_Portfolio_All FileName
    MainMDI.StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj:") & " " & FileName
    FileName = Mid$(FileName, 1, Len(FileName) - 3) + "trn"
    W_Transaction_All FileName
    ActivePortfolioFileName = FileName        ' Addded in 1.0.2
    blnPortfolioModified = False
  End If
  End With
  Exit Sub
ErrHandler:
  'Do nothing
  
End Sub
Sub PromptForSave(Cancel As Boolean)
  Dim FileName As String
  Select Case YesNoCancel("Vill du spara �ndringar till den nuvarande portf�ljen.", "Spara?")
  Case vbCancel
    Cancel = True
  Case vbYes
    SavePortfolioAs
    Cancel = False
  Case vbNo
    Cancel = False
  End Select
End Sub

Sub CheckIfInvestExist()
Dim I As Long
I = 1
Do While I <= PortfolioArraySize
  If GetInvestIndex(PortfolioArray(I).Name) = -1 Then
    InformationalMessage Trim(PortfolioArray(I).Name) + " " + CheckLang("finns ej bland objekten. V�lj ett nytt objekt som ers�tter detta eller ta bort objektet ur portf�ljen.")
    ReplaceInvestInPortfolio I
  Else
    I = I + 1
  End If
Loop
End Sub

Sub ReplaceInvestInPortfolio(ReplaceIndex As Long)
  frmReplace.Show 0
  ReplaceSem.wait = True
  Do While ReplaceSem.wait
    DoEvents
  Loop
  Unload frmReplace
  If ReplaceSem.int1 = 1 Then 'Replace investment in portfolio
    ReplaceRecordinPortfolioArray ReplaceIndex, ReplaceSem.str1 ' ReplaceSem.int1 was replaced by ReplaceIndex 980824 for release 1.0.6
  ElseIf ReplaceSem.int1 = 2 Then 'Delete investment in portfolio
    DeleteRecordInPortfolioArray ReplaceIndex
  End If
End Sub

