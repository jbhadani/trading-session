Attribute VB_Name = "Settings"

Sub SaveGraphSettingsToRegistry()
  With GraphSettings
  SaveSetting App.Title, "GraphSettings", "DayWidth", CStr(.lngDayWidth)
  SaveSetting App.Title, "GraphSettings", "DaySpace", CStr(.lngDaySpace)
  SaveSetting App.Title, "GraphSettings", "CloseWidth", CStr(.lngCloseWidth)
 
  If .ShowHLC Then
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "False"
  End If
  If .ShowZero Then
    SaveSetting App.Title, "GraphSettings", "ShowZero", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowZero", "False"
  End If
    
  If .ShowXGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "False"
  End If
  If .ShowMinXGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowMinXGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowMinXGrid", "False"
  End If
  If .ShowYGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "False"
  End If
  If .ShowPriceMAV1 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "False"
  End If
  If .ShowPriceMAV2 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "False"
  End If
  If .ShowIndicator1 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "False"
  End If
  If .ShowIndicator2 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "False"
  End If
  If .ShowSignalColor Then
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "False"
  End If
  If .ExponentialPrice Then
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "False"
  End If
  If .ShowVerticalColorLines Then
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "False"
  End If
  
  'Bollinger
  If .ShowBollinger Then
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "False"
  End If
  
  'Parabolic
  If .ShowParabolic Then
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "False"
  End If
  If .ShowVolume Then
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "False"
  End If
  
  
  
  'Setup
  SaveSetting App.Title, "graphSettings", "ShowKeyReversalDay", CStr(.Setup(supKeyReversalDay).blnShow)
  SaveSetting App.Title, "graphSettings", "ShowOneDayReversal", CStr(.Setup(supOneDayReversal).blnShow)
  SaveSetting App.Title, "graphSettings", "ShowPatternGap", CStr(.Setup(supPatternGap).blnShow)
  SaveSetting App.Title, "graphSettings", "ShowReversalDay", CStr(.Setup(supReversalDay).blnShow)
  SaveSetting App.Title, "graphSettings", "ShowReversalGap", CStr(.Setup(supReversalGap).blnShow)
  SaveSetting App.Title, "graphSettings", "ShowTwoDayReversal", CStr(.Setup(supTwoDayReversal).blnShow)
  
  SaveSetting App.Title, "GraphSettings", "ShowSupportResistance", CStr(.blnShowSupportResistance)
  
  'Indicators
  SaveSetting App.Title, "GraphSettings", "ColorSignallingIndicator", .strColorSignallingIndicator
  SaveSetting App.Title, "GraphSettings", "Indicator1", .strIndicator1
  SaveSetting App.Title, "GraphSettings", "Indicator2", .strIndicator2
  
  
If PRO = True Then
  If .blnShowFish Then
    SaveSetting App.Title, "GraphSettings", "ShowFish", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowFish", "False"
  End If
  'If .blnShowHighlight Then
  '  SaveSetting App.Title, "GraphSettings", "ShowHighlight", "True"
  'Else
  '  SaveSetting App.Title, "GraphSettings", "ShowHighlight", "False"
  'End If
  
End If
  
    
  End With
  
End Sub



Public Sub UpdateColorGraphSettings(frmColorSettings As Form)
With frmColorSettings
  GraphSettings.PriceColor = .picDiagramColor.BackColor
  GraphSettings.BackGroundColor = .picBackgroundColor.BackColor
  GraphSettings.SignalColorBuy = .picSignalColorBuy.BackColor
  GraphSettings.lngSignalColorSell = .picSignalColorSell.BackColor
  GraphSettings.AxisColor = .picAxisColor.BackColor
  GraphSettings.GridColor = .picGridColor.BackColor
  GraphSettings.Serie1Color = .picIndicatorLine1Color.BackColor
  GraphSettings.Serie2Color = .picIndicatorLine2Color.BackColor
  GraphSettings.BollingerColor = .picBollingerColor.BackColor
  GraphSettings.ParabolicColor = .picParabolicColor.BackColor
  GraphSettings.PriceMAV1Color = .picMAV1Color.BackColor
  GraphSettings.PriceMAV2Color = .picMAV2Color.BackColor
  GraphSettings.Setup(supKeyReversalDay).lngColor = .picKeyReversalDay.BackColor
  GraphSettings.Setup(supReversalDay).lngColor = .picReversalDay.BackColor
  GraphSettings.Setup(supOneDayReversal).lngColor = .picOneDayReversal.BackColor
  GraphSettings.Setup(supTwoDayReversal).lngColor = .picTwoDayReversal.BackColor
  GraphSettings.Setup(supPatternGap).lngColor = .picPatternGap.BackColor
  GraphSettings.Setup(supReversalGap).lngColor = .picReversalGap.BackColor
  GraphSettings.lngCloseColor = .picCloseColor.BackColor
  GraphSettings.lngSupportResistanceColor = .picSupportResistanceColor.BackColor
  GraphSettings.lngDrawAreaColor = .picDrawAreaColor.BackColor
If PRO = True Then
  GraphSettings.lngFishColor = .picFishColor.BackColor
End If
  GraphSettings.lngVolumeColor = .picVolumeColor.BackColor
End With
End Sub

Public Sub SaveColorGraphSettingsToRegistry()
With GraphSettings
  SaveSetting App.Title, "GraphSettings", "PriceColor", CStr(.PriceColor)
  SaveSetting App.Title, "GraphSettings", "SignalColorBuy", CStr(.SignalColorBuy)
  SaveSetting App.Title, "GraphSettings", "SignalColorSell", CStr(.lngSignalColorSell)
  SaveSetting App.Title, "GraphSettings", "BackgroundColor", CStr(.BackGroundColor)
  SaveSetting App.Title, "GraphSettings", "AxisColor", CStr(.AxisColor)
  SaveSetting App.Title, "GraphSettings", "GridColor", CStr(.GridColor)
  SaveSetting App.Title, "GraphSettings", "Serie1Color", CStr(.Serie1Color)
  SaveSetting App.Title, "GraphSettings", "Serie2Color", CStr(.Serie2Color)
  SaveSetting App.Title, "GraphSettings", "PriceMAV1Color", CStr(.PriceMAV1Color)
  SaveSetting App.Title, "GraphSettings", "PriceMAV2Color", CStr(.PriceMAV2Color)
  SaveSetting App.Title, "GraphSettings", "BollingerColor", CStr(.BollingerColor)
  SaveSetting App.Title, "GraphSettings", "ParabolicColor", CStr(.ParabolicColor)
  SaveSetting App.Title, "GraphSettings", "KeyReversalDayColor", CStr(.Setup(supKeyReversalDay).lngColor)
  SaveSetting App.Title, "GraphSettings", "ReversalDayColor", CStr(.Setup(supReversalDay).lngColor)
  SaveSetting App.Title, "GraphSettings", "OneDayReversalColor", CStr(.Setup(supOneDayReversal).lngColor)
  SaveSetting App.Title, "GraphSettings", "TwoDayReversalColor", CStr(.Setup(supTwoDayReversal).lngColor)
  SaveSetting App.Title, "GraphSettings", "PatternGapColor", CStr(.Setup(supPatternGap).lngColor)
  SaveSetting App.Title, "GraphSettings", "ReversalGapCOlor", CStr(.Setup(supReversalGap).lngColor)
  SaveSetting App.Title, "GraphSettings", "CloseColor", CStr(.lngCloseColor)
  SaveSetting App.Title, "GraphSettings", "SupportResistanceColor", CStr(.lngSupportResistanceColor)
  SaveSetting App.Title, "GraphSettings", "DrawAreaColor", CStr(.lngDrawAreaColor)
  SaveSetting App.Title, "GraphSettings", "Lang", Lang
If PRO = True Then
  SaveSetting App.Title, "GraphSettings", "FishColor", CStr(.lngFishColor)
End If
  SaveSetting App.Title, "GraphSettings", "VolumeColor", CStr(.lngVolumeColor)
End With
End Sub

Sub InitColorGraphSettings()

  With frmColorSettings
  .picDiagramColor.BackColor = GraphSettings.PriceColor
  .picBackgroundColor.BackColor = GraphSettings.BackGroundColor
  .picAxisColor.BackColor = GraphSettings.AxisColor
  .picGridColor.BackColor = GraphSettings.GridColor
  .picMAV1Color.BackColor = GraphSettings.PriceMAV1Color
  .picMAV2Color.BackColor = GraphSettings.PriceMAV2Color
  .picBollingerColor.BackColor = GraphSettings.BollingerColor
  .picParabolicColor.BackColor = GraphSettings.ParabolicColor
  .picIndicatorLine1Color.BackColor = GraphSettings.Serie1Color
  .picIndicatorLine2Color.BackColor = GraphSettings.Serie2Color
  .picSignalColorBuy.BackColor = GraphSettings.SignalColorBuy
  .picSignalColorSell.BackColor = GraphSettings.lngSignalColorSell
  .picKeyReversalDay.BackColor = GraphSettings.Setup(supKeyReversalDay).lngColor
  .picReversalDay.BackColor = GraphSettings.Setup(supReversalDay).lngColor
  .picOneDayReversal.BackColor = GraphSettings.Setup(supOneDayReversal).lngColor
  .picTwoDayReversal.BackColor = GraphSettings.Setup(supTwoDayReversal).lngColor
  .picPatternGap.BackColor = GraphSettings.Setup(supPatternGap).lngColor
  .picReversalGap.BackColor = GraphSettings.Setup(supReversalGap).lngColor
  .picCloseColor.BackColor = GraphSettings.lngCloseColor
  .picSupportResistanceColor.BackColor = GraphSettings.lngSupportResistanceColor
  .picDrawAreaColor.BackColor = GraphSettings.lngDrawAreaColor
If PRO = True Then
  .picFishColor.BackColor = GraphSettings.lngFishColor
End If
  .picVolumeColor.BackColor = GraphSettings.lngVolumeColor
  End With

End Sub

Sub InitGraphSettings()
  
  With GraphSettings
  If GetSetting(App.Title, "GraphSettings", "ShowHLC", "True") = "True" Then
    .ShowHLC = True
  Else
    .ShowHLC = False
  End If
  .DWM = dwmDaily
  .PriceColor = Val(GetSetting(App.Title, "GraphSettings", "PriceColor", "16711680"))
  .PriceMAV1Color = Val(GetSetting(App.Title, "GraphSettings", "PriceMAV1Color", "255"))
  .PriceMAV2Color = Val(GetSetting(App.Title, "GraphSettings", "PriceMAV2Color", "0"))
  .SignalColorBuy = Val(GetSetting(App.Title, "GraphSettings", "SignalColorBuy", "32768"))
  .lngSignalColorSell = Val(GetSetting(App.Title, "GraphSettings", "SignalColorSell", "255"))
  .BackGroundColor = Val(GetSetting(App.Title, "GraphSettings", "BackgroundColor", "16777215"))
  .AxisColor = Val(GetSetting(App.Title, "GraphSettings", "AxisColor", "0"))
  .GridColor = Val(GetSetting(App.Title, "GraphSettings", "GridColor", "12632256"))
  .Serie1Color = Val(GetSetting(App.Title, "GraphSettings", "Serie1Color", "0"))
  .Serie2Color = Val(GetSetting(App.Title, "GraphSettings", "Serie2Color", "255"))
  .ParabolicColor = Val(GetSetting(App.Title, "GraphSettings", "ParabolicColor", "0"))
  .BollingerColor = Val(GetSetting(App.Title, "GraphSettings", "BollingerColor", "0"))
  .Setup(supKeyReversalDay).lngColor = Val(GetSetting(App.Title, "GraphSettings", "KeyReversalDayColor", "255"))
  .Setup(supReversalDay).lngColor = Val(GetSetting(App.Title, "GraphSettings", "ReversalDayColor", "255"))
  .Setup(supOneDayReversal).lngColor = Val(GetSetting(App.Title, "GraphSettings", "OneDayReversalColor", "255"))
  .Setup(supTwoDayReversal).lngColor = Val(GetSetting(App.Title, "GraphSettings", "TwoDayReversalColor", "255"))
  .Setup(supPatternGap).lngColor = Val(GetSetting(App.Title, "GraphSettings", "PatternGapColor", "255"))
  .Setup(supReversalGap).lngColor = Val(GetSetting(App.Title, "GraphSettings", "ReversalGapColor", "255"))
  .lngCloseColor = Val(GetSetting(App.Title, "GraphSettings", "CloseColor", "16711680"))
  .lngFishColor = Val(GetSetting(App.Title, "GraphSettings", "FishColor", "8453888"))
  .lngSupportResistanceColor = CLng(GetSetting(App.Title, "GraphSettings", "SupportResistanceColor", CStr(RGB(0, 255, 0))))
  .lngVolumeColor = Val(GetSetting(App.Title, "GraphSettings", "VolumeColor", "16711680"))
  .lngDrawAreaColor = Val(GetSetting(App.Title, "GraphSettings", "DrawAreaColor", "11329768"))
  
  .lngCloseWidth = Val(GetSetting(App.Title, "GraphSettings", "CloseWidth", "1"))
  
  If GetSetting(App.Title, "GraphSettings", "ShowPriceMAV1", "False") = "True" Then
    .ShowPriceMAV1 = True
  Else
    .ShowPriceMAV2 = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "ShowPriceMAV2", "False") = "True" Then
    .ShowPriceMAV2 = True
  Else
    .ShowPriceMAV2 = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "ShowMinXGrid", "True") = "True" Then
    .ShowMinXGrid = True
  Else
    .ShowMinXGrid = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "ShowXGrid", "True") = "True" Then
    .ShowXGrid = True
  Else
    .ShowXGrid = False
  End If
  Lang = GetSetting(App.Title, "GraphSettings", "Lang", 1)
  If Lang = 0 Then
    SaveSetting App.Title, "GraphSettings", "Lang", 1
  End If
  Lang = GetSetting(App.Title, "GraphSettings", "Lang", 1)
  If GetSetting(App.Title, "GraphSettings", "ShowYGrid", "True") = "True" Then
    .ShowYGrid = True
  Else
    .ShowYGrid = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "ShowSignalColor", "False") = "True" Then
    .ShowSignalColor = True
  Else
    .ShowSignalColor = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "VerticalColorLines", "False") = "True" Then
    .ShowVerticalColorLines = True
  Else
    .ShowVerticalColorLines = False
  End If
  
  .SignalIndicator = Val(GetSetting(App.Title, "GraphSettings", "SignalIndicator", "1"))
  .lngDayWidth = Val(GetSetting(App.Title, "GraphSettings", "DayWidth", "2"))
  .lngDaySpace = Val(GetSetting(App.Title, "GraphSettings", "DaySpace", "4"))
  
  If GetSetting(App.Title, "GraphSettings", "ShowIndicator1", "False") = "True" Then
    .ShowIndicator1 = True
  Else
    .ShowIndicator1 = False
  End If
  If GetSetting(App.Title, "GraphSettings", "ShowIndicator2", "False") = "True" Then
    .ShowIndicator2 = True
  Else
    .ShowIndicator2 = False
  End If
    
  If GetSetting(App.Title, "GraphSettings", "ExponentialPrice", "False") = "True" Then
    .ExponentialPrice = True
  Else
    .ExponentialPrice = False
  End If
  
  If GetSetting(App.Title, "ErrorHandling", "ShowIndicatorErrorMessage", "False") = "True" Then
    ShowIndicatorErrorMessage = True
  Else
    ShowIndicatorErrorMessage = False
  End If
  
  
  
  'Bollinger
  If GetSetting(App.Title, "GraphSettings", "ShowBollinger", "False") = "True" Then
    .ShowBollinger = True
  Else
    .ShowBollinger = False
  End If
  
  'Parabolic
  If GetSetting(App.Title, "GraphSettings", "ShowParabolic", "False") = "True" Then
    .ShowParabolic = True
  Else
    .ShowParabolic = False
  End If
  
  'ShowZero
  If GetSetting(App.Title, "GraphSettings", "ShowZero", "False") = "True" Then
    .ShowZero = True
  Else
    .ShowZero = False
  End If
  
  If GetSetting(App.Title, "GraphSettings", "ShowVolume", "True") = "True" Then
    .ShowVolume = True
  Else
    .ShowVolume = False
  End If
  
  .Setup(supKeyReversalDay).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowKeyReversalDay", "False"))
  .Setup(supOneDayReversal).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowOneDayReversal", "False"))
  .Setup(supPatternGap).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowPatternGap", "False"))
  .Setup(supReversalDay).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowReversalDay", "False"))
  .Setup(supReversalGap).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowReversalGap", "False"))
  .Setup(supTwoDayReversal).blnShow = CBool(GetSetting(App.Title, "GraphSettings", "ShowTwoDayReversal", "False"))
  
  'Indicators
  .strColorSignallingIndicator = GetSetting(App.Title, "GraphSettings", "ColorSignallingIndicator", "Moving Average")
  .strIndicator1 = GetSetting(App.Title, "GraphSettings", "Indicator1", "Moving Average")
  .strIndicator2 = GetSetting(App.Title, "GraphSettings", "Indicator2", "MACD")
  
  .blnShowSupportResistance = CBool(GetSetting(App.Title, "GraphSettings", "ShowSupportResistance", "False"))
If PRO = True Then
  If GetSetting(App.Title, "GraphSettings", "ShowFish", "False") = "True" Then
    .blnShowFish = True
  Else
    .blnShowFish = False
  End If
  'If GetSetting(App.Title, "GraphSettings", "ShowHighlight", "False") = "True" Then
  '  .blnShowHighlight = True
  'Else
    .blnShowHighlight = False
  'End If
End If
  
  End With
  
  'Trendlinjer
  With LineSettings
    .LineType = Val(GetSetting(App.Title, "TrendLines", "LineType", "1"))
    .LineWidth = Val(GetSetting(App.Title, "TrendLines", "Linewidth", "1"))
    .LineColor = Val(GetSetting(App.Title, "TrendLines", "LineColor", "255"))
  End With
  
  
  
End Sub
Sub ChangeFormLang(F As Form)
  Dim c As Object
For Each c In F
    If TypeName(c) = "Label" Or TypeName(c) = "Frame" Or TypeName(c) = "CommandButton" Or TypeName(c) = "OptionButton" Or TypeName(c) = "CheckBox" Then
        c.Caption = CheckLang(c.Caption)
    End If
Next
End Sub
