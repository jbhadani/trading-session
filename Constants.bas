Attribute VB_Name = "Constants"
Option Explicit

' Update process
Public Const STILL_ACTIVE = &H103
Public Const PROCESS_QUERY_INFORMATION = &H400

' FTP
Public Const INTERNET_OPEN_TYPE_DIRECT = 1
Public Const INTERNET_OPEN_TYPE_PROXY = 3
Public Const INTERNET_FLAG_RELOAD = &H80000000

' Active Toolbars Layout File
Public Const ACTIVETOOLBARS_LAYOUT_FILE As String = "tlo.atb"

' Message displayed on disabled features
Public Const MESSAGE_ONLY_PRO As String = ""

Public Const LeftDrawDist As Long = 6
Public Const RightDrawDist As Long = 66
Public Const TopDrawDist As Long = 16
Public Const BottomDrawDist As Long = 11

Public Const LeftAxisDist As Long = 4
Public Const RightAxisDist As Long = 64
Public Const TopAxisDist As Long = 16
Public Const BottomAxisDist As Long = 9

Public Const RightLabelDist As Long = 60

Public Const LeftLegendDist As Long = 10
Public Const TopLegendDist As Long = 20

Public Const TopLegendTextDist As Long = 18

Public Const BetweenLegendDist As Long = 12
Public Const BetweenLegendBoxTextDist As Long = 6

Public Const LegendBoxWidth As Long = 12
Public Const LegendBoxHeight As Long = 8

Public Const LegendBoxOffset As Long = 2

Public Const TrendLineSeries As Long = 3
Public Const TrendlineSerieSize As Long = 30

Public Const TextsFileName As String = "Texts.dat"
Public Const TextsFileNumber As Long = 7
Public Const TextsVersionString As String = "Trading Session"

Public Const ZArraySize As Single = 36


