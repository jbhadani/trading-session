Attribute VB_Name = "PortfolioOperations"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          ExistInPortfolioArray
'INPUT:         PortfolioType
'OUTPUT:        Function (ExistInPortfolioArray)
'LAST UPDATE:
'Returns true if Portfolio can be found in PortfolioArray
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''

Function ExistInPortfolioArray(Portfolio As PortfolioType) As Boolean
  Dim i As Long
  ExistInPortfolioArray = False
  For i = 1 To PortfolioArraySize
    If PortfolioArray(i).Name = Portfolio.Name Then
      ExistInPortfolioArray = True
    End If
  Next i
End Function

'GetPortfolioIndex
'Returns -1 if Name can't be found in PortfolioArray.Name.
'Returns the index if Name can be found in PortfolioArray
Function GetPortfolioIndex(Name As String) As Long
  Dim i As Long
  GetPortfolioIndex = -1
  For i = 1 To PortfolioArraySize
    If Trim(PortfolioArray(i).Name) = Trim(Name) Then
      GetPortfolioIndex = i
    End If
  Next i
End Function

Sub DeleteRecordInPortfolioArray(index As Long)
Dim TempPortfolioArray() As PortfolioType
Dim i, j As Long
  ReDim TempPortfolioArray(PortfolioArraySize - 1)
  j = 0
  For i = 1 To index - 1
    j = j + 1
    TempPortfolioArray(j) = PortfolioArray(i)
  Next i
  For i = index + 1 To PortfolioArraySize
    j = j + 1
    TempPortfolioArray(j) = PortfolioArray(i)
  Next i
  ReDim PortfolioArray(PortfolioArraySize - 1)
  PortfolioArraySize = PortfolioArraySize - 1
  For i = 1 To PortfolioArraySize
    PortfolioArray(i) = TempPortfolioArray(i)
  Next i
End Sub

Sub ReplaceRecordinPortfolioArray(ReplaceIndex As Long, NewName As String)
  RenameTransactions PortfolioArray(ReplaceIndex).Name, NewName
  PortfolioArray(ReplaceIndex).Name = Trim(NewName)
  SortPortfolioArray
End Sub

Sub SortPortfolioArray()
Dim i As Long
Dim j As Long
Dim MinItem As Long
Dim tmpPArray() As PortfolioType
Dim Chosen() As Boolean
Dim tmpPArraySize As Long


' Copy PortfolioArray
ReDim tmpPArray(1 To PortfolioArraySize)
ReDim Chosen(1 To PortfolioArraySize)
For i = 1 To PortfolioArraySize
  tmpPArray(i) = PortfolioArray(i)
  Chosen(i) = False
Next i


For i = 1 To PortfolioArraySize
  MinItem = 0
  j = 1
  Do While MinItem = 0
    If Not Chosen(j) Then MinItem = j   ' Chosen was missing (j) . Added 980824 for release 1.0.6
    j = j + 1
  Loop
  
  For j = 1 To PortfolioArraySize
    If Not Chosen(j) And StrComp(tmpPArray(j).Name, tmpPArray(MinItem).Name, vbTextCompare) <> 1 Then
        MinItem = j
    End If
  Next j
  
  PortfolioArray(i) = tmpPArray(MinItem)
  Chosen(MinItem) = True
  
Next i

DoEvents
End Sub

