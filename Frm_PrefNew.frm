VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form Frm_PrefNew 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inst�llningar"
   ClientHeight    =   4305
   ClientLeft      =   645
   ClientTop       =   930
   ClientWidth     =   7605
   Icon            =   "Frm_PrefNew.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4305
   ScaleWidth      =   7605
   Begin VB.Frame Frame9 
      Caption         =   "Spr�k"
      Height          =   855
      Left            =   0
      TabIndex        =   24
      Top             =   2640
      Width           =   3735
      Begin VB.OptionButton Option1 
         Caption         =   "Swedish"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   25
         Top             =   360
         Width           =   1695
      End
      Begin VB.OptionButton Option1 
         Caption         =   "English"
         Height          =   375
         Index           =   2
         Left            =   1920
         TabIndex        =   26
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame Frame8 
      Caption         =   "Uppstart"
      Height          =   855
      Left            =   0
      TabIndex        =   22
      Top             =   1800
      Width           =   3735
      Begin VB.CheckBox chkShowGraphAtStartup 
         Caption         =   "Visa graf vid uppstart"
         Height          =   375
         Left            =   360
         TabIndex        =   23
         Top             =   240
         Value           =   1  'Checked
         Width           =   2895
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Datum p� trendlinjer"
      Height          =   855
      Left            =   3840
      TabIndex        =   18
      Top             =   1800
      Width           =   3735
      Begin VB.TextBox txtCycleLenth 
         Height          =   285
         Left            =   360
         TabIndex        =   20
         Text            =   "26"
         Top             =   360
         Width           =   375
      End
      Begin MSComCtl2.UpDown UpDown2 
         Height          =   255
         Left            =   720
         TabIndex        =   19
         Top             =   360
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin VB.Label Label2 
         Caption         =   "Cykell�ngd"
         Height          =   255
         Left            =   1080
         TabIndex        =   21
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Virtuella datum"
      Height          =   855
      Left            =   3840
      TabIndex        =   14
      Top             =   2640
      Visible         =   0   'False
      Width           =   3735
      Begin VB.TextBox txtNbrVirtualDates 
         Height          =   285
         Left            =   2160
         TabIndex        =   17
         Text            =   "0"
         Top             =   360
         Width           =   375
      End
      Begin MSComCtl2.UpDown UpDown1 
         Height          =   255
         Left            =   2520
         TabIndex        =   16
         Top             =   360
         Width           =   240
         _ExtentX        =   423
         _ExtentY        =   450
         _Version        =   393216
         Enabled         =   -1  'True
      End
      Begin VB.Label Label1 
         Caption         =   "Antal dagar att l�gga till"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   360
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Verktygsrad"
      Height          =   855
      Left            =   3840
      TabIndex        =   12
      Top             =   900
      Width           =   3735
      Begin VB.CheckBox chkUseSavedToolbarSettings 
         Caption         =   "Anv�nd sparade inst�llningar f�r verktygsraderna."
         Height          =   375
         Left            =   360
         TabIndex        =   13
         Top             =   360
         Width           =   2955
      End
   End
   Begin VB.Frame Frame6 
      Height          =   735
      Left            =   0
      TabIndex        =   9
      Top             =   3480
      Width           =   7575
      Begin VB.CommandButton cbApply 
         Caption         =   "Verkst�ll"
         Height          =   375
         Left            =   6120
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   3300
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbOk 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   480
         TabIndex        =   0
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Indikatorparametrar"
      Height          =   855
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   3735
      Begin VB.OptionButton rbObjekt 
         Caption         =   "Objektberoende"
         Height          =   255
         Left            =   360
         TabIndex        =   11
         Top             =   480
         Width           =   2535
      End
      Begin VB.OptionButton rbGeneral 
         Caption         =   "Allm�nna"
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "S�kv�g f�r datafil"
      Height          =   855
      Left            =   0
      TabIndex        =   5
      Top             =   900
      Width           =   3735
      Begin VB.TextBox TB_DataPath 
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         Top             =   360
         Width           =   2295
      End
      Begin VB.Label Label3 
         Caption         =   "S�kv�g"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Felmeddelande"
      Height          =   855
      Left            =   3840
      TabIndex        =   3
      Top             =   0
      Width           =   3735
      Begin VB.CheckBox Check1 
         Caption         =   "Visa inte felmeddelande under s�kning, optimering och indikatortest."
         Height          =   375
         Left            =   360
         TabIndex        =   4
         Top             =   360
         Width           =   3255
      End
   End
End
Attribute VB_Name = "Frm_PrefNew"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bufLang As Byte

Sub UpdatePreferences()
  
  'allm�nna eller objektberoende parametrar
  If rbGeneral.Value And Not UseGeneralIndicatorParameters Then
    UseGeneralIndicatorParameters = True
    GetParameterSettingsFromRegistry
    If ShowGraph Then
      GraphInfoChanged = True
      UpdateGraph ActiveIndex
    End If
  End If
  If Not rbGeneral.Value And UseGeneralIndicatorParameters Then
    UseGeneralIndicatorParameters = False
    GetParameterSettingsFromInvest ActiveIndex
    If ShowGraph Then
      GraphInfoChanged = True
      UpdateGraph ActiveIndex
    End If
  End If
  
  'felmeddelande
  If Frm_PrefNew.Check1 = 1 Then
    ShowIndicatorErrorMessage = False
  Else
    ShowIndicatorErrorMessage = True
  End If
  
  If chkUseSavedToolbarSettings = 1 Then
    blnUseSavedToolbarSettings = True
  Else
    blnUseSavedToolbarSettings = False
  End If
  
  'Bj�rn hackar
  
  'Tills vidare...  UpdateVirtualDates Val(txtNbrVirtualDates.Text) -->
  UpdateVirtualDates 0
  
  UpdateTrendlineNotation Val(txtCycleLenth.Text)
  With MainMDI.ID_Object
    Dim foo As Long
    foo = .ListIndex
    .ListIndex = foo
  End With
  
  ShowGraphAtStartup = chkShowGraphAtStartup
  
  'SaveSetting App.Title, "Indicator", "NbrVirtualDates", NbrVirtualDates
  SaveSetting App.Title, "Indicator", "CycleLenth", CycleLenth
  
  SaveSetting App.Title, "Indicator", "ShowGraphAtStartup", ShowGraphAtStartup
  'SaveSetting App.Title, "Indicator", "NotateToday", NotateToday
  'Slut hack
  
  'Parametrar SaveSettings
  If UseGeneralIndicatorParameters Then
    SaveSetting App.Title, "Indicator", "UseGeneralIndicatorParameters", "True"
  Else
    SaveSetting App.Title, "Indicator", "UseGeneralIndicatorParameters", "False"
  End If
  
  'Felmeddelande SaveSettings
  If ShowIndicatorErrorMessage Then
    SaveSetting App.Title, "ErrorHandling", "ShowIndicatorErrorMessage", "True"
  Else
    SaveSetting App.Title, "ErrorHandling", "ShowIndicatorErrorMessage", "False"
  End If
  
  SaveSetting App.Title, "Misc", "UseSavedToolbarSettings", CStr(blnUseSavedToolbarSettings)
  
  If Trim(TB_DataPath) <> Trim(GetSetting(App.Title, "Directories", "DataPath", "")) Then
    SaveSetting App.Title, "Directories", "DataPath", Trim(TB_DataPath)
    InformationalMessage CheckLang("Du m�ste starta om programmet f�r att denna �ndring ska verka")
  End If
  
  If Option1(bufLang).Value = True Then
  Else
    Select Case Option1(1).Value
    Case True
        Option1(1).Value = True
        Lang = 1
        MainMDI.ID_Swedish_Click
        bufLang = 1
    Case False
        Lang = 2
        Option1(2).Value = True
        MainMDI.ID_English_Click
        bufLang = 2
    End Select
  End If
End Sub



Private Sub cbApply_Click()
  UpdatePreferences
End Sub

Private Sub cbCancel_Click()
  Unload Frm_PrefNew
End Sub

Private Sub cbOK_Click()
  UpdatePreferences
  Unload Frm_PrefNew
End Sub

Private Sub Form_Load()
  bufLang = Lang            '   Language buffer
  
  Frm_PrefNew.left = (MainMDI.Width - Frm_PrefNew.Width) / 2
  Frm_PrefNew.top = (MainMDI.Height - Frm_PrefNew.Height) / 4
  
  TB_DataPath = GetSetting(App.Title, "Directories", "DataPath", "")

  'inst�llning f�r att visa felmeddelande
  If ShowIndicatorErrorMessage Then
    Frm_PrefNew.Check1 = 0
  Else
    Frm_PrefNew.Check1 = 1
  End If
    
  If UseGeneralIndicatorParameters Then
    rbGeneral.Value = 1
    rbObjekt.Value = 0
  Else
    rbObjekt.Value = 1
    rbGeneral.Value = 0
  End If
  
  If blnUseSavedToolbarSettings Then
    chkUseSavedToolbarSettings.Value = 1
  Else
    chkUseSavedToolbarSettings.Value = 0
  End If
  
  'Bj�rns hack
  txtNbrVirtualDates.Text = NbrVirtualDates
  txtCycleLenth.Text = CycleLenth
  
  chkShowGraphAtStartup.Value = ShowGraphAtStartup
  'Slut hack
    
  Option1(Lang).Value = True
End Sub


Private Sub UpDown1_UpClick()
Dim cValue As Integer
cValue = Val(txtNbrVirtualDates.Text)
cValue = cValue + 1
txtNbrVirtualDates.Text = cValue
End Sub

Private Sub UpDown1_DownClick()
Dim cValue As Integer
cValue = Val(txtNbrVirtualDates.Text)
cValue = cValue - 1
txtNbrVirtualDates = cValue
End Sub

Private Sub UpDown2_DownClick()
Dim cValue As Integer
cValue = Val(txtCycleLenth.Text)
cValue = cValue - 1
txtCycleLenth.Text = cValue
End Sub

Private Sub UpDown2_UpClick()
Dim cValue As Integer
cValue = Val(txtCycleLenth.Text)
cValue = cValue + 1
txtCycleLenth.Text = cValue
End Sub
