VERSION 5.00
Begin VB.Form frmAuthorization 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ange namn och serienummer"
   ClientHeight    =   2475
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4485
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2475
   ScaleWidth      =   4485
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   7
      Top             =   1680
      Width           =   4455
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   3240
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbOk 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Height          =   1575
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   4455
      Begin VB.TextBox tbSerialNo 
         Height          =   285
         Left            =   1440
         MaxLength       =   13
         TabIndex        =   1
         Top             =   960
         Width           =   1815
      End
      Begin VB.TextBox tbName 
         Height          =   285
         Left            =   1440
         MaxLength       =   20
         TabIndex        =   0
         Top             =   360
         Width           =   2775
      End
      Begin VB.Label Label2 
         Caption         =   "Serienummer"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label1 
         Caption         =   "Namn"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   360
         Width           =   615
      End
   End
End
Attribute VB_Name = "frmAuthorization"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbCancel_Click()
  ' Terminate the application
  End
End Sub


Private Sub cbOK_Click()
If Trim$(tbName.Text) <> "" Then
  If ValidateSerialNo(Trim$(tbSerialNo.Text)) Then
    SaveSetting App.Title, "Identification", "Name", Trim$(tbName.Text)
    SaveSetting App.Title, "Identification", "SerialNo", Trim$(tbSerialNo.Text)
    'InformationalMessage "Gratulerar"
    Unload frmAuthorization
  Else
    ErrorMessageStr "Felaktigt serienummer"
    tbSerialNo.Text = ""
  End If

Else
  ErrorMessageStr "Felaktigt namn"
End If

End Sub

Private Sub Form_Load()
  With frmAuthorization
   .Top = Screen.Height / 2 - .Height / 2
   .Left = Screen.Width / 2 - .Width / 2
   End With
End Sub
