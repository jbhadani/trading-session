VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmObjectTree 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Form1"
   ClientHeight    =   8100
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   3540
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8100
   ScaleWidth      =   3540
   ShowInTaskbar   =   0   'False
   Begin MSComctlLib.TreeView TreeView1 
      Height          =   7815
      Left            =   0
      TabIndex        =   0
      Top             =   120
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   13785
      _Version        =   393217
      Style           =   7
      SingleSel       =   -1  'True
      Appearance      =   1
   End
End
Attribute VB_Name = "frmObjectTree"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  
   Dim nodX As Node
   Dim i As Long
   For i = 1 To UBound(CategoryArray)
    If Trim(CategoryArray(i)) <> "" Then
    Set nodX = TreeView1.Nodes.Add(, , "A" & CStr(i), Trim(CategoryArray(i)))
    End If
   Next i
   
   For i = 1 To UBound(InvestInfoArray)
    Set nodX = TreeView1.Nodes.Add("A" & CStr(InvestInfoArray(i).TypeOfInvest), tvwChild, "B" & CStr(i), InvestInfoArray(i).Name)
   Next i

End Sub

Private Sub Form_Resize()
  TreeView1.Width = Me.Width - 100
  TreeView1.Height = Me.Height - 400
End Sub

Private Sub TreeView1_NodeClick(ByVal Node As MSComctlLib.Node)
Dim i As Long
If left(Node.key, 1) = "B" Then
  MainMDI.ID_Group.ListIndex = CLng(right(Node.Root.key, Len(Node.Root.key) - 1)) + 2
  i = 0
  Do While Trim(MainMDI.ID_Object.List(i)) <> Trim(Node.Text)
    i = i + 1
  Loop
  MainMDI.ID_Object.ListIndex = i
End If
 Debug.Print Node.key
End Sub
