VERSION 5.00
Begin VB.Form frmToolbarSettings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Verktygsradinställningar"
   ClientHeight    =   3510
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2820
   Icon            =   "frmToolbarSettings.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3510
   ScaleWidth      =   2820
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   2760
      Width           =   2775
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   375
         Left            =   1800
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Visa verktygsrad"
      Height          =   2655
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2775
      Begin VB.CheckBox chkIndicator 
         Caption         =   "Indikatorval"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   2160
         Width           =   1695
      End
      Begin VB.CheckBox chkDraw 
         Caption         =   "Ritverktyg"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1440
         Width           =   1695
      End
      Begin VB.CheckBox chkObject 
         Caption         =   "Objekt- och gruppval"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1800
         Width           =   1935
      End
      Begin VB.CheckBox chkGraphSettings 
         Caption         =   "Diagraminställningar"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1080
         Width           =   1935
      End
      Begin VB.CheckBox chkTool 
         Caption         =   "Verktyg"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   720
         Width           =   1695
      End
      Begin VB.CheckBox chkFile 
         Caption         =   "Arkiv"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmToolbarSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Command1_Click()

End Sub

Private Sub cmdCancel_Click()
  Unload frmToolbarSettings
End Sub

Private Sub cmdOk_Click()
  With MainMDI
  If chkFile.Value = 1 Then
    .Toolbar1.Visible = True
  Else
    .Toolbar1.Visible = False
  End If
  If chkTool.Value = 1 Then
    .Toolbar1.Buttons(9).Visible = True
    .Toolbar1.Buttons(10).Visible = True
    .Toolbar1.Buttons(11).Visible = True
    .Toolbar1.Buttons(13).Visible = True
  Else
    .Toolbar1.Buttons(9).Visible = False
    .Toolbar1.Buttons(10).Visible = False
    .Toolbar1.Buttons(11).Visible = False
    .Toolbar1.Buttons(13).Visible = False
  End If
  If chkGraphSettings.Value = 1 Then
    .Toolbar2.Visible = True
  Else
    .Toolbar2.Visible = False
  End If
  If chkDraw.Value = 1 Then
    .Toolbar4.Visible = True
  Else
    .Toolbar4.Visible = False
  End If
  If chkObject.Value = 1 Then
    .ID_Group.Visible = True
    .ID_Object.Visible = True
    .ID_PrevObject.Visible = True
    .ID_NextObject.Visible = True
  Else
    .ID_Group.Visible = False
    .ID_Object.Visible = False
    .ID_PrevObject.Visible = False
    .ID_NextObject.Visible = False
  End If
  If chkIndicator.Value = 1 Then
    .ID_Indicator1.Visible = True
    .ID_Indicator2.Visible = True
  Else
    .ID_Indicator1.Visible = False
    .ID_Indicator2.Visible = False
  End If
  End With
  Unload frmToolbarSettings
End Sub

Private Sub Form_Load()
  CenterForm Me
  With MainMDI
  If .Toolbar1.Visible Then
    chkFile.Value = 1
  End If
  If .Toolbar1.Buttons(13).Visible Then
    chkTool.Value = 1
  End If
  If .Toolbar2.Visible Then
    chkGraphSettings.Value = 1
  End If
  If .Toolbar4.Visible Then
    chkDraw.Value = 1
  End If
  If .ID_Object.Visible Then
    chkObject.Value = 1
  End If
  If .ID_Indicator1.Visible Then
    chkIndicator.Value = 1
  End If
  End With
End Sub

