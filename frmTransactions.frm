VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmTransactions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Visa/redigera transaktioner"
   ClientHeight    =   3525
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8370
   Icon            =   "frmTransactions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3525
   ScaleWidth      =   8370
   Begin VB.Frame Frame1 
      Caption         =   "Transaktioner"
      Height          =   2775
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   8350
      Begin MSFlexGridLib.MSFlexGrid gTransactions 
         Height          =   2415
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Width           =   8100
         _ExtentX        =   14288
         _ExtentY        =   4260
         _Version        =   393216
         Cols            =   8
         FixedCols       =   0
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   4
      Top             =   2760
      Width           =   8350
      Begin VB.CommandButton cbExit 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   6000
         TabIndex        =   1
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cbEditTransaction 
         Caption         =   "�ndra transaktion"
         Default         =   -1  'True
         Height          =   375
         Left            =   720
         TabIndex        =   2
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cbDeleteTransaction 
         Caption         =   "Radera transaktion"
         Height          =   375
         Left            =   3360
         TabIndex        =   3
         Top             =   240
         Width           =   1575
      End
   End
End
Attribute VB_Name = "frmTransactions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbDeleteTransaction_Click()
  Dim Transaction As TransactionType
  With gTransactions
    .Col = 0
    Transaction.Date = .Text
    .Col = 1
    Transaction.Name = .Text
    If Trim(.Text) = "Ins�ttning" Or Trim(.Text) = "Uttag" Then
      Transaction.IsBuySell = False
      If Trim(.Text) = "Ins�ttning" Then
        Transaction.IsDeposit = True
      Else
        Transaction.IsDeposit = False
      End If
      .Col = 6
      Transaction.Cash = .Text
    Else
      Transaction.IsBuySell = True
      .Col = 2
      If .Text = "K�p" Then
        Transaction.IsBuy = True
      Else
        Transaction.IsBuy = False
      End If
      .Col = 3
      Transaction.Price = .Text
      .Col = 4
      Transaction.Volume = .Text
      .Col = 5
      Transaction.Courtage = .Text
    End If
  End With
  DeleteTransaction GetTransactionIndex(Transaction)
  'UpdateTransactionGrid
  If TransactionArraySize = 0 Then
    InformationalMessage "Alla transaktioner �r borttagna"
    Unload frmTransactions
  End If
End Sub

Private Sub cbEditTransaction_Click()
  With gTransactions
    .Col = 1
    If Trim(.Text) = "Ins�ttning" Or .Text = "Uttag" Then
      .Col = 0
      frmChangeCashDeposit.DTPicker1.Value = .Text
      .Col = 1
      If Trim(.Text) = "Ins�ttning" Then
        frmChangeCashDeposit.cmbChoice.ListIndex = 0
      Else
        frmChangeCashDeposit.cmbChoice.ListIndex = 1
      End If
      .Col = 6
      frmChangeCashDeposit.tbDeposit = .Text
      frmChangeCashDeposit.Show 0
    Else
      .Col = 0
      frmChangeTransaction.DTPicker1.Value = .Text
      .Col = 1
      frmChangeTransaction.Caption = "�ndra transaktion f�r " + .Text
      .Col = 2
      frmChangeTransaction.cBuySell.AddItem "K�p"
      frmChangeTransaction.cBuySell.AddItem "S�lj"
      If .Text = "K�p" Then
        frmChangeTransaction.cBuySell.ListIndex = 0
      Else
        frmChangeTransaction.cBuySell.ListIndex = 1
      End If
      .Col = 3
      frmChangeTransaction.tbPrice = .Text
      .Col = 4
      frmChangeTransaction.tbVolume = .Text
      .Col = 5
      frmChangeTransaction.tbCourtage = .Text
      frmChangeTransaction.Show
    End If
  End With
  frmTransactions.Enabled = False
End Sub

Private Sub cbExit_Click()
Unload frmTransactions
End Sub

Private Sub Form_Load()
  frmTransactions.left = (MainMDI.Width - frmTransactions.Width) / 2
  frmTransactions.top = (MainMDI.Height - frmTransactions.Height) / 4
  'UpdateTransactionGrid
End Sub

Private Sub gTransactions_DblClick()
  With gTransactions
    .Col = 1
    If Trim(.Text) = "Ins�ttning" Or .Text = "Uttag" Then
      .Col = 0
      frmChangeCashDeposit.DTPicker1.Value = .Text
      .Col = 1
      If Trim(.Text) = "Ins�ttning" Then
        frmChangeCashDeposit.cmbChoice.ListIndex = 0
      Else
        frmChangeCashDeposit.cmbChoice.ListIndex = 1
      End If
      .Col = 6
      frmChangeCashDeposit.tbDeposit = .Text
      frmChangeCashDeposit.Show 0
    Else
      .Col = 0
      frmChangeTransaction.DTPicker1.Value = .Text
      .Col = 1
      frmChangeTransaction.Caption = "�ndra transaktion f�r " + .Text
      .Col = 2
      frmChangeTransaction.cBuySell.AddItem "K�p"
      frmChangeTransaction.cBuySell.AddItem "S�lj"
      If .Text = "K�p" Then
        frmChangeTransaction.cBuySell.ListIndex = 0
      Else
        frmChangeTransaction.cBuySell.ListIndex = 1
      End If
      .Col = 3
      frmChangeTransaction.tbPrice = .Text
      .Col = 4
      frmChangeTransaction.tbVolume = .Text
      .Col = 5
      frmChangeTransaction.tbCourtage = .Text
      frmChangeTransaction.Show
    End If
  End With
  frmTransactions.Enabled = False
End Sub
