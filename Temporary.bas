Attribute VB_Name = "Temporary"
Option Explicit

#If DBUG = 1 Then

Sub PrintInvestinfoArray()
Dim I As Long
Open Path + "InvestInfoArray.txt" For Output As #12
For I = 1 To InvestInfoArraySize
With InvestInfoArray(I)
  Print #12, .Name & vbTab & .FileName & vbTab & Str(.TypeOfInvest)
End With
Next I
Close #12
End Sub

Sub CompareInvinfoWSSettings()
  Dim I As Long
  Dim Failure As Boolean
  If WorkSpaceSettingsArraySize <> InvestInfoArraySize Then
    Debug.Print Str(Time) & " Arraysizes differ! WSpSet=" & Trim(Str(WorkSpaceSettingsArraySize)) & " InvInfo=" & Trim(Str(InvestInfoArraySize))
  Else
    Debug.Print Str(Time) & " Arraysizes Ok!"
    Failure = False
    For I = 1 To InvestInfoArraySize
      If InvestInfoArray(I).Name <> WorkspaceSettingsArray(I).Name Then
        Debug.Print Str(Time) & " Name differs at index= " & Trim(Str(I)) & "! WSpSet=" & Trim(WorkspaceSettingsArray(I).Name) & " InvInfo=" & Trim(InvestInfoArray(I).Name)
        Failure = True
      End If
    Next I
    If Failure Then
      Debug.Print Str(Time) & " Names differ!"
    Else
      Debug.Print Str(Time) & " Names Ok!"
    End If
  End If
End Sub

#End If
