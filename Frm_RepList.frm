VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Frm_RepList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Portf�lj"
   ClientHeight    =   5205
   ClientLeft      =   3615
   ClientTop       =   3285
   ClientWidth     =   8925
   Icon            =   "Frm_RepList.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5205
   ScaleWidth      =   8925
   Begin VB.Frame Frame6 
      Caption         =   "Sista datum"
      Height          =   855
      Left            =   120
      TabIndex        =   14
      Top             =   0
      Width           =   2535
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   600
         TabIndex        =   16
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   20643841
         CurrentDate     =   36402
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Visa"
      Height          =   855
      Left            =   120
      TabIndex        =   12
      Top             =   960
      Width           =   2535
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "Frm_RepList.frx":014A
         Left            =   240
         List            =   "Frm_RepList.frx":0157
         Style           =   2  'Dropdown List
         TabIndex        =   13
         Top             =   360
         Width           =   2055
      End
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   4440
      Width           =   8775
      Begin VB.CommandButton cbDeleteTransaction 
         Caption         =   "Radera transaktion"
         Height          =   375
         Left            =   4800
         TabIndex        =   15
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cbChangeTransaction 
         Caption         =   "�ndra transaktion"
         Height          =   375
         Left            =   2520
         TabIndex        =   11
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   6960
         TabIndex        =   10
         Top             =   240
         Width           =   1575
      End
      Begin VB.CommandButton cbUpdate 
         Caption         =   "Uppdatera"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Aktuellt l�ge"
      Height          =   1815
      Left            =   2760
      TabIndex        =   1
      Top             =   0
      Width           =   3015
      Begin VB.Label labMarketValue 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   7
         Top             =   600
         Width           =   975
      End
      Begin VB.Label labPortfolioValue 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   6
         Top             =   960
         Width           =   975
      End
      Begin VB.Label labTotalCash 
         Alignment       =   1  'Right Justify
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   255
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Portf�ljens v�rde"
         Height          =   255
         Left            =   480
         TabIndex        =   4
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Marknadsv�rde"
         Height          =   255
         Left            =   480
         TabIndex        =   3
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Likvida medel"
         Height          =   255
         Left            =   480
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
   End
   Begin MSFlexGridLib.MSFlexGrid Grid1 
      Height          =   2535
      Left            =   0
      TabIndex        =   0
      Top             =   1920
      Width           =   8855
      _ExtentX        =   15610
      _ExtentY        =   4471
      _Version        =   393216
      Rows            =   10
      FixedCols       =   0
      ScrollTrack     =   -1  'True
      ScrollBars      =   2
   End
End
Attribute VB_Name = "Frm_RepList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbCancel_Click()
  Unload Me
  ReportWindowOpen = False
End Sub

Private Sub cbChangeTransaction_Click()
Dim Index As Long
Dim NotFound As Boolean
Dim i As Long

Frm_RepList.Enabled = False

Index = Frm_RepList.Grid1.Row

  If TransactionArray(Index).IsBuySell Then
    With frmChangeTransaction
      .Show 0
      .tbCourtage = TransactionArray(Index).Courtage
      .DTPicker1.Value = TransactionArray(Index).Date
      .tbPrice = TransactionArray(Index).Price
      .tbVolume = TransactionArray(Index).Volume
      If TransactionArray(Index).IsBuy Then
        .cBuySell.ListIndex = 0
      Else
        .cBuySell.ListIndex = 1
      End If
    End With
  ElseIf Trim(TransactionArray(Index).DepositType) <> "Utdelning" Then
    With frmChangeCashDeposit
      .Show 0
      .DTPicker1.Value = TransactionArray(Index).Date
      .tbDeposit = TransactionArray(Index).Cash
      If Trim(TransactionArray(Index).DepositType) = "Ins�ttning" Then
        .cmbChoice.ListIndex = 0
      ElseIf Trim(TransactionArray(Index).DepositType) = "Uttag" Then
        .cmbChoice.ListIndex = 1
      ElseIf Trim(TransactionArray(Index).DepositType) = "Avgifter" Then
        .cmbChoice.ListIndex = 2
      ElseIf Trim(TransactionArray(Index).DepositType) = "R�nteint�kter" Then
        .cmbChoice.ListIndex = 3
      ElseIf Trim(TransactionArray(Index).DepositType) = "R�ntekostnader" Then
        .cmbChoice.ListIndex = 4
      End If
    End With
  Else
    For i = 1 To PortfolioArraySize
      frmChangeStockDividend.Combo1.AddItem PortfolioArray(i).Name, i - 1
    Next i
    With frmChangeStockDividend
      .Show 0
      .DTPicker1.Value = TransactionArray(Index).Date
      .tbDividend = TransactionArray(Index).Cash
      NotFound = True
      i = 0
      Do While NotFound
        i = i + 1
        If Trim(PortfolioArray(i).Name) = Trim(TransactionArray(Index).Name) Then
          NotFound = False
        End If
      Loop
      .Combo1.ListIndex = i - 1
    End With
  End If

End Sub

Private Sub cbDeleteTransaction_Click()
Dim Svar As Long
  Svar = MsgBox(CheckLang("Vill du ta bort markerad transaktion?"), vbYesNo, CheckLang("Radera"))
  If Svar = 6 Then
    DeleteTransaction Frm_RepList.Grid1.Row
    UpdateReportGrid
  End If
End Sub

Private Sub cbUpdate_Click()
  UpdateReportGrid
End Sub

Private Sub Combo1_Click()
  UpdateReportGrid
End Sub

Private Sub Form_Load()
  Width = 9010
  Height = 5580
  top = 1000
  left = (MainMDI.Width - Width) / 2
  With Frm_RepList
    .Grid1.Width = 8950
    .Grid1.Height = 2535
    .DTPicker1.Value = Date
  End With
End Sub


Private Sub Form_Unload(Cancel As Integer)
  ReportWindowOpen = False
End Sub

Private Sub tbLatestdate_GotFocus()
  TextSelected
End Sub
