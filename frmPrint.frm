VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{7E00A3A2-8F5C-11D2-BAA4-04F205C10000}#1.0#0"; "VsVIEW6.ocx"
Begin VB.Form frmPrint 
   Caption         =   "Form1"
   ClientHeight    =   6450
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7605
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   6450
   ScaleWidth      =   7605
   Begin VB.CommandButton cmdPrint 
      Caption         =   "Skriv ut"
      Default         =   -1  'True
      Height          =   375
      Left            =   6360
      TabIndex        =   3
      Top             =   120
      Width           =   975
   End
   Begin VB.CommandButton cmdPrinterSetup 
      Caption         =   "Skrivarinställningar"
      Height          =   375
      Left            =   4680
      TabIndex        =   2
      Top             =   120
      Width           =   1575
   End
   Begin VB.CommandButton cmdPageSetup 
      Caption         =   "Sidinställningar"
      Height          =   375
      Left            =   3000
      TabIndex        =   1
      Top             =   120
      Width           =   1575
   End
   Begin VSVIEW6Ctl.VSPrinter VSPrinter1 
      Height          =   5775
      Left            =   0
      TabIndex        =   0
      Top             =   600
      Width           =   7575
      _cx             =   4207665
      _cy             =   4204490
      Appearance      =   1
      BorderStyle     =   1
      Enabled         =   -1  'True
      MousePointer    =   0
      BackColor       =   -2147483643
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty HdrFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _ConvInfo       =   1
      AutoRTF         =   -1  'True
      Preview         =   -1  'True
      DefaultDevice   =   0   'False
      PhysicalPage    =   -1  'True
      AbortWindow     =   -1  'True
      AbortWindowPos  =   0
      AbortCaption    =   "Skriver ut..."
      AbortTextButton =   "Avbryt"
      AbortTextDevice =   "på %s genom %s"
      AbortTextPage   =   "Skriver ut sidan %d av"
      FileName        =   ""
      MarginLeft      =   1440
      MarginTop       =   1440
      MarginRight     =   1440
      MarginBottom    =   1440
      MarginHeader    =   0
      MarginFooter    =   0
      IndentLeft      =   0
      IndentRight     =   0
      IndentFirst     =   0
      IndentTab       =   720
      SpaceBefore     =   0
      SpaceAfter      =   0
      LineSpacing     =   100
      Columns         =   1
      ColumnSpacing   =   180
      ShowGuides      =   2
      LargeChangeHorz =   300
      LargeChangeVert =   300
      SmallChangeHorz =   30
      SmallChangeVert =   30
      Track           =   0   'False
      ProportionalBars=   -1  'True
      Zoom            =   31.1051693404635
      ZoomMode        =   3
      ZoomMax         =   400
      ZoomMin         =   10
      ZoomStep        =   5
      MouseZoom       =   2
      MouseScroll     =   -1  'True
      MousePage       =   -1  'True
      EmptyColor      =   -2147483636
      TextColor       =   0
      HdrColor        =   0
      BrushColor      =   0
      BrushStyle      =   0
      PenColor        =   0
      PenStyle        =   0
      PenWidth        =   0
      PageBorder      =   0
      Header          =   ""
      Footer          =   ""
      TableSep        =   "|;"
      TableBorder     =   7
      TablePen        =   0
      TablePenLR      =   0
      TablePenTB      =   0
      HTMLStyle       =   1
   End
   Begin MSComCtl2.DTPicker dtplast 
      Height          =   375
      Left            =   1440
      TabIndex        =   4
      Top             =   120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393216
      Format          =   23527425
      CurrentDate     =   36621
   End
   Begin MSComCtl2.DTPicker dtpFirst 
      Height          =   375
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      _Version        =   393216
      Format          =   23527425
      CurrentDate     =   36621
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim CurrentFirstDate As Date
Dim CurrentLastDate As Date


Private Sub cmdPrinterSetup_Click()
  GetPrintSettingsFromRegistry frmPrint.VSPrinter1
  If VSPrinter1.PrintDialog(pdPrinterSetup) Then
    SavePrintSettingsToRegistry frmPrint.VSPrinter1
    CreatePage True, False, False
  End If
End Sub

Private Sub cmdPrint_Click()
  CreatePage True, True, False
End Sub

Private Sub dtpFirst_Change()
  CreatePage True, False, False
End Sub

Private Sub dtplast_Change()
  CreatePage True, False, False
End Sub



Private Sub Form_Resize()
  If Me.Width < 7725 Then
    Me.Width = 7725
  End If
  
  If Me.Height < 6855 Then
    Me.Height = 6855
  End If
  VSPrinter1.Width = Me.Width - 100
  VSPrinter1.Height = Me.Height - 1000
  cmdPrint.left = Me.Width - cmdPrint.Width - 250
  cmdPrinterSetup.left = cmdPrint.left - cmdPrinterSetup.Width - 150
  cmdPageSetup.left = cmdPrinterSetup.left - cmdPageSetup.Width - 150
End Sub

Private Sub cmdPageSetup_Click()
  GetPrintSettingsFromRegistry frmPrint.VSPrinter1
  If VSPrinter1.PrintDialog(pdPageSetup) Then
    SavePrintSettingsToRegistry frmPrint.VSPrinter1
    CreatePage True, False, False
  End If
End Sub

Private Sub Form_Terminate()
  EnableAllTools
End Sub

Private Sub Form_Unload(Cancel As Integer)
  EnableAllTools
End Sub
  
