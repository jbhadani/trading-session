Attribute VB_Name = "FileHandler"
Option Explicit
  'FileHandler
  'Last modified: 961229 (FW)
  'Comments:


' Writes the global array InvestInfoArray to FileName
'Sub W_InvestInfo_All(FileName As String)
'  Dim NbrOfRecords As long
'  Dim InvestInfo As InvestInfoType
'  Dim i As long
'  On Error Resume Next
'  Kill FileName
'  If Err.Number <> 0 Then ErrorMessageStr "Kan ej hitta filen med objektinformation. Skapar en ny."
'  Err.Clear
'
'  Open FileName For Random As #1 Len = Len(InvestInfo)
'  If Err.Number = 0 Then
'    For i = 1 To InvestInfoArraySize
'      Put #1, i, InvestInfoArray(i)
'    Next i
'    Close #1
'  Else
'    ErrorMessageStr "S�kv�gen finns ej."
'  End If
'  On Error GoTo 0
'End Sub


' Read all the InvestInfo records and store them
' into the global variable InvestInfoArray
Sub R_InvestInfo_All(FileName As String)
  Dim InvestInfo As InvestInfoType
  Dim NbrOfRecords As Long
  Dim i As Long
  On Error Resume Next
  Open FileName For Random As #1 Len = Len(InvestInfo)
  If Err.Number = 0 Then
    NbrOfRecords = LOF(1) \ Len(InvestInfo)
    ReDim InvestInfoArray(NbrOfRecords)
    InvestInfoArraySize = NbrOfRecords
    For i = 1 To NbrOfRecords
      Get #1, , InvestInfo
      InvestInfoArray(i) = InvestInfo
    Next i
    Close #1
    Else
      ErrorMessageStr "Kan ej hitta filen med objektinformation."
    End If
    Err.Clear
    On Error GoTo 0
End Sub


' Writes the global array PortfolioArray to FileName
Sub W_Portfolio_All(FileName As String)
  
  Dim NbrOfRecords As Long
  Dim Portfolio As PortfolioType
  Dim i As Long
  On Error Resume Next
  Kill FileName
  Err.Clear
  Open FileName For Random As #1 Len = Len(Portfolio)
  If Err.Number = 0 Then
    Portfolio.Name = "FrontTrade Portfolio"
    Put #1, 1, Portfolio.Name
    For i = 2 To PortfolioArraySize + 1
      Put #1, i, PortfolioArray(i - 1)
    Next i
    Close #1
  Else
    ErrorMessageStr "S�kv�gen �r felaktig. V�lj spara och ange en giltig s�kv�g."
  End If
  On Error GoTo 0
End Sub

' Read all the Portfolio records and store them
' into the global variable PortfolioArray
Sub R_Portfolio_All(FileName As String, OK As Boolean)
  Dim Portfolio As PortfolioType
  Dim NbrOfRecords As Long
  Dim i As Long
  Open FileName For Random As #1 Len = Len(Portfolio)
  Debug.Print Err.Description
  NbrOfRecords = LOF(1) \ Len(Portfolio)
  If NbrOfRecords <> 0 Then   ' Added in 1.0.6 to avoid error when open a non-existing file
    ReDim PortfolioArray(NbrOfRecords - 1)
    PortfolioArraySize = NbrOfRecords - 1
    Get #1, , Portfolio.Name
    If Portfolio.Name = "FrontTrade Portfolio" Then
      For i = 1 To NbrOfRecords - 1
        Get #1, , Portfolio
        PortfolioArray(i) = Portfolio
      Next i
      OK = True
    Else
      OK = False
    End If
  Else
    OK = False
  End If
  Close #1
End Sub
Sub W_Transaction_Last(FileName As String, Transaction As TransactionType)
  Dim NbrOfRecords As Long
  Open FileName For Random As #1 Len = Len(Transaction)
  NbrOfRecords = LOF(1) \ Len(Transaction)
  Put #1, NbrOfRecords + 1, Transaction
  Close #1
End Sub


'Sub W_PriceData_All(FileName As String)
'  Dim NbrOfRecords As long
'  Dim PriceData As PriceDataType
'  Dim i As long
'  Kill FileName
'  Open FileName For Random As #1 Len = Len(PriceData)
'  For i = 1 To PriceDataArraySize
'    Put #1, i, PriceDataArray(i)
'  Next i
'  Close #1
'End Sub
#If 0 Then
Sub R_PriceData_All(FileName As String)
  Dim PriceData As PriceDataType
  Dim NbrOfRecords As Long
  Dim i As Long
  Open FileName For Random As #1 Len = Len(PriceData)
  NbrOfRecords = LOF(1) \ Len(PriceData)
  ReDim PriceDataArray(NbrOfRecords)
  PriceDataArraySize = NbrOfRecords
  For i = 1 To NbrOfRecords
    Get #1, , PriceData
    PriceDataArray(i) = PriceData
  Next i
  Close #1
End Sub
#End If

'Sub W_Category_All(FileName As String)
'  Dim NbrOfRecords As long
'  Dim Category As String * 20
'  Dim i As long
'  Kill FileName
'  Open FileName For Random As #1 Len = Len(Category)
'  For i = 1 To CategoryArraySize
'    Put #1, i, CategoryArray(i)
'  Next i
'  Close #1
'End Sub

Sub R_Category_All(FileName As String)
  Dim NbrOfRecords As Long
  Dim Category As String * 20
  Dim i As Long
  On Error Resume Next
  Open FileName For Random As #1 Len = Len(Category)
  If Err.Number = 0 Then
    NbrOfRecords = LOF(1) \ Len(Category)
    ReDim CategoryArray(NbrOfRecords)
    CategoryArraySize = NbrOfRecords
    For i = 1 To NbrOfRecords
      Get #1, , Category
      CategoryArray(i) = Category
    Next i
    Close #1
  Else
    ErrorMessageStr "Kan ej hitta filen med gruppinformation"
  End If
  On Error GoTo 0
End Sub



'Sub MakeEmptyPrc(FileName As String)
'Dim PriceData As PriceDataType

'Open FileName For Random As #1 Len = Len(PriceData)
'Close #1
'End Sub


Sub W_Dividend_Last(FileName As String, Dividend As DividendType)
  Dim NbrOfRecords As Long
  Open FileName For Random As #1 Len = Len(Dividend)
  NbrOfRecords = LOF(1) \ Len(Dividend)
  Put #1, NbrOfRecords + 1, Dividend
  Close #1
End Sub



Sub W_Transaction_All(FileName As String)
  Dim NbrOfRecords As Long
  Dim Transaction As TransactionType
  Dim i As Long
  
  On Error Resume Next
  Kill FileName
  Err.Clear
  Open FileName For Random As #1 Len = Len(Transaction)
    If Err.Number = 0 Then
    For i = 1 To TransactionArraySize
      Put #1, i, TransactionArray(i)
    Next i
    Close #1
    Else
      ErrorMessageStr "S�kv�gen �r felaktig. V�lj spara igen och ange en giltig s�kv�g."
    End If
  On Error GoTo 0
End Sub

' Read all the Portfolio records and store them
' into the global variable PortfolioArray
Sub R_Transaction_All(FileName As String)
  Dim Transaction As TransactionType
  Dim NbrOfRecords As Long
  Dim i As Long
  Open FileName For Random As #1 Len = Len(Transaction)
  NbrOfRecords = LOF(1) \ Len(Transaction)
  ReDim TransactionArray(NbrOfRecords)
  TransactionArraySize = NbrOfRecords
  For i = 1 To NbrOfRecords
    Get #1, , Transaction
    TransactionArray(i) = Transaction
  Next i
  Close #1
End Sub

Sub R_PriceDataArray(FileName As String, PDArray() As PriceDataType, PDArraySize As Long)
  Dim PriceData As PriceDataType
  Dim NbrOfRecords As Long
  
  Dim i As Long
  On Error Resume Next
  Open FileName For Random As #1 Len = Len(PriceData)
  If Err.Number = 0 Then
    'NbrOfRecords = LOF(1) \ Len(PriceData) + NbrVirtualDates
    NbrOfRecords = LOF(1) \ Len(PriceData)
    ReDim PDArray(NbrOfRecords)
    PDArraySize = NbrOfRecords
    For i = 1 To NbrOfRecords
      Get #1, , PriceData
      PDArray(i) = PriceData
    Next i
    Close #1
  Else
    ErrorMessageStr "Felaktig s�kv�g."
  End If
  Err.Clear
  On Error GoTo 0
  DecryptPriceData PDArray()
  'For i = NbrOfRecords - (NbrVirtualDates - 1) To NbrOfRecords
  '  PDArray(i) = PDArray(NbrOfRecords - NbrVirtualDates)
  '  PDArray(i).Date = PDArray(NbrOfRecords - NbrVirtualDates).Date + (i - NbrOfRecords + NbrVirtualDates)
  'Next i
End Sub

'Sub W_PriceDataArray(FileName As String, PDArray() As PriceDataType, PDArraySize As long)
'
'  Dim PriceData As PriceDataType
'  Dim i As long
'  Kill FileName
'  Open FileName For Random As #1 Len = Len(PriceData)
'  For i = 1 To PDArraySize
'    Put #1, i, PDArray(i)
'  Next i
'  Close #1
'End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          ReadWorkSpaceSettings
'Input:
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      980428  Created
'               980729  Datapath in filename was changed to path
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub ReadWorkspaceSettings()

  Dim WorkspaceSetting As WorkspaceSettings_v2_Type
  Dim NbrOfRecords As Long
  Dim i As Long
  Dim temp As Long
  Dim FileName As String
  
  FileName = Path & "wspset.dat"
  On Error Resume Next
  Open FileName For Random As #1 Len = Len(WorkspaceSetting)
  If Err.Number = 0 Then
    NbrOfRecords = LOF(1) \ Len(WorkspaceSetting)
    ReDim WorkspaceSettingsArray(1 To NbrOfRecords)
    For i = 1 To NbrOfRecords
      Get #1, , WorkspaceSettingsArray(i)
    Next i
  
    Close #1
    WorkSpaceSettingsArraySize = NbrOfRecords
  Else
    ErrorMessageStr "Felaktig s�kv�g."
  End If
  Err.Clear
  On Error GoTo 0
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          WriteWorkSpaceSettings
'Input:
'Output:
'
'Description:   Writes WorkspaceSettingsArray to file
'Author:        Fredrik Wendel
'Revision:      980428  Created
'               980729  Datapath in filename was changed to path
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub WriteWorkspaceSettings()


  Dim NbrOfRecords As Long
  Dim WorkspaceSetting As WorkspaceSettings_v2_Type
  Dim i As Long
  Dim j As Long
  Dim FileName As String
  FileName = Path + "wspset.dat"
  On Error Resume Next
  Kill FileName
  Err.Clear
  Open FileName For Random As #1 Len = Len(WorkspaceSetting)
  If Err.Number = 0 Then
    For i = 1 To WorkSpaceSettingsArraySize
      Put #1, , WorkspaceSettingsArray(i)
    Next i
    Close #1
   Else
      ErrorMessageStr "S�kv�gen �r felaktig. V�lj spara igen och ange en giltig s�kv�g."
  End If
  On Error GoTo 0
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          FixAddAndRemove
'Input:
'Output:
'
'Description:   Syncronize WorkspaceSettingsArray to
'               InvestInfoArray
'Author:        Fredrik Wendel
'Revision:      98XXXX  Created
'               991019  Modifed for new TrendlineArray.
'               991105  Added NewObjects.
'               991111  Modified the Redim statement for
'                       NewObjects.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub FixAddAndRemove()
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim l As Long
  Dim Found As Boolean
  Dim tempWSArray() As WorkspaceSettings_v2_Type
  Dim tempWSArraySize As Long
  Dim ParameterSettings As ParameterSettingsType
  Dim OldWorkSpaceSettingsArraySize As Long
  ReDim NewObjects(0 To 0)
  
  frmSplash.lblSplashStatus = "Synkroniserar..."
  DoEvents
  ' A special case if Workspacesettingarray is empty.
  ' Make a new array and fill with default values
  
  If WorkSpaceSettingsArraySize = 0 Then
    ReDim WorkspaceSettingsArray(1 To InvestInfoArraySize)
    WorkSpaceSettingsArraySize = InvestInfoArraySize
    
    For i = 1 To InvestInfoArraySize
      With WorkspaceSettingsArray(i)
      .Name = InvestInfoArray(i).Name
      .ParameterSettings = GetDefaultParameterSettings
      For l = 1 To TrendLineSeries
        For j = 1 To TrendlineSerieSize
          .TrendLine(l, j).InUse = False
        Next j
      Next l
      End With
    Next i
  Else
    ' Copy WorkspaceSettingsArray
    ReDim tempWSArray(1 To WorkSpaceSettingsArraySize)
    For i = 1 To WorkSpaceSettingsArraySize
      tempWSArray(i) = WorkspaceSettingsArray(i)
    Next i
    tempWSArraySize = WorkSpaceSettingsArraySize
  
    ' Resize WorkspaceSettingsArray to InvestInfoArraySize
    ReDim WorkspaceSettingsArray(1 To InvestInfoArraySize)
    OldWorkSpaceSettingsArraySize = WorkSpaceSettingsArraySize
    WorkSpaceSettingsArraySize = InvestInfoArraySize
  
    For i = 1 To InvestInfoArraySize
      frmSplash.pbrSplash.Value = CInt(i / InvestInfoArraySize * 100)
      'Debug.Print Str(i / InvestInfoArraySize * 100) + " %"
      ' Find in tempWSArray i +- 10
      Found = False
      If i > 10 Then
        k = i - 10
      Else
        k = 1
      End If
      Do While k <= OldWorkSpaceSettingsArraySize And Not Found And k <= (i + 10)
        If Trim(tempWSArray(k).Name) = Trim(InvestInfoArray(i).Name) Then
          Found = True
        Else
          k = k + 1
        End If
      Loop
      ' Find in the rest
      If Not Found Then
        Found = False
        k = 1
        Do While k <= OldWorkSpaceSettingsArraySize And Not Found
          If Trim(tempWSArray(k).Name) = Trim(InvestInfoArray(i).Name) Then
            Found = True
          Else
            k = k + 1
          End If
        Loop
      End If
      
      If Found Then 'Already in WorkspaceSettingsArray - Copy
        WorkspaceSettingsArray(i).Name = tempWSArray(k).Name
        WorkspaceSettingsArray(i).ParameterSettings = tempWSArray(k).ParameterSettings
        For l = 1 To TrendLineSeries
          For j = 1 To TrendlineSerieSize
            WorkspaceSettingsArray(i).TrendLine(l, j) = tempWSArray(k).TrendLine(l, j)
          Next j
        Next l
      Else          'Not in WorkspaceSettingsArray - set default settings
        WorkspaceSettingsArray(i).Name = InvestInfoArray(i).Name
        WorkspaceSettingsArray(i).ParameterSettings = GetDefaultParameterSettings
        For l = 1 To TrendLineSeries
          For j = 1 To TrendlineSerieSize
            WorkspaceSettingsArray(i).TrendLine(l, j).InUse = False
          Next j
        Next l
        
        'Add to NewObjects array
        ReDim Preserve NewObjects(0 To (UBound(NewObjects) + 1))
        NewObjects(UBound(NewObjects)) = InvestInfoArray(i).Name
      End If
      'If i Mod 100 = 0 Then Debug.Print i
    Next i
  End If
  frmSplash.pbrSplash.Value = 100
End Sub

Sub R_Triggers(FileName As String, Triggers() As TriggerType)
  Dim Trigger As TriggerType
  Dim NbrOfRecords As Long
  Dim i As Long
  
  Open FileName For Random As #1 Len = Len(Trigger)
  NbrOfRecords = LOF(1) \ Len(Trigger)
  ReDim Triggers(NbrOfRecords)
  
  For i = 1 To NbrOfRecords
    Get #1, , Trigger
    Triggers(i) = Trigger
  Next i
  Close #1
  
End Sub

Public Sub W_Triggers(FileName As String, Triggers() As TriggerType)
  Dim Trigger As TriggerType
  Dim i As Long
  
  On Error Resume Next
  Kill FileName
  Err.Clear
  On Error GoTo 0
  
  Open FileName For Random As #1 Len = Len(Trigger)
    For i = 1 To UBound(Triggers())
      Put #1, i, Triggers(i)
    Next i
  Close #1

End Sub

