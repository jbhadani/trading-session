Attribute VB_Name = "SignalArrayCreator"
Option Explicit

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: EaseOfMovementSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub EaseOfMovementSignalArray(Signalarray() As Long, InputArray() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

Last = 0
Signalarray(1) = 0

For i = 2 To Size
  If InputArray(i) > 0 And InputArray(i - 1) < 0 Then
    Last = 1
  ElseIf InputArray(i) < 0 And InputArray(i - 1) > 0 Then
    Last = -1
  End If
  Signalarray(i) = Last
Next i

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MACDSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MACDSignalArray(Signalarray() As Long, MACD() As Single, MACDSignal() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

i = 1
If MACD(i) > MACDSignal(i) Then
  Last = 1
ElseIf MACD(i) < MACDSignal(i) Then
  Last = -1
End If
Signalarray(1) = Last

For i = 2 To Size
  If MACD(i) > MACDSignal(i) And MACD(i - 1) < MACDSignal(i - 1) Then
    Last = 1
  ElseIf MACD(i) < MACDSignal(i) And MACD(i - 1) > MACDSignal(i - 1) Then
    Last = -1
  End If
  Signalarray(i) = Last
Next i

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MAVSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MAVSignalArray(Signalarray() As Long, MAV1() As Single, MAV2() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

i = 1
If MAV1(i) > MAV2(i) Then
  Last = 1
ElseIf MAV1(i) < MAV2(i) Then
  Last = -1
End If

Signalarray(i) = Last

For i = 2 To Size
  If MAV1(i) > MAV2(i) And MAV1(i - 1) < MAV2(i - 1) Then
    Last = 1
  ElseIf MAV1(i) < MAV2(i) And MAV1(i - 1) > MAV2(i - 1) Then
    Last = -1
  End If
  Signalarray(i) = Last
Next i

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MFISignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MFISignalArray(Signalarray() As Long, MFI() As Single, MFIMAV() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

Last = 0
Signalarray(1) = 0

With ParameterSettings
If .MFISignalUseMAV Then
  For i = 2 To Size
    If MFI(i) > MFIMAV(i) And MFI(i - 1) < MFIMAV(i) Then
      Last = 1
    ElseIf MFI(i) < MFIMAV(i) And MFI(i - 1) > MFIMAV(i) Then
      Last = -1
    End If
    Signalarray(i) = Last
  Next i
Else
  If .MFIShowMAV Then
    For i = 2 To Size
      If MFIMAV(i) > .MFILowerSignalLevel And MFIMAV(i - 1) < .MFILowerSignalLevel Then
        Last = 1
      ElseIf MFIMAV(i) < .MFIUpperSignalLevel And MFIMAV(i - 1) > .MFIUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  Else
    For i = 2 To Size
      If MFI(i) > .MFILowerSignalLevel And MFI(i - 1) < .MFILowerSignalLevel Then
        Last = 1
      ElseIf MFI(i) < .MFIUpperSignalLevel And MFI(i - 1) > .MFIUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  End If
End If
End With

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MomentumSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub MomentumSignalArray(Signalarray() As Long, MOMArray() As Single, MOMMAVArray() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

If ParameterSettings.MOMSignalUseZeroLine Then
  If ParameterSettings.MOMSignalUseMAV Then
    If MOMMAVArray(1) > 100 Then
      Last = 1
    ElseIf MOMMAVArray(1) < 100 Then
      Last = -1
    End If
    Signalarray(1) = Last
    
    For i = 2 To Size
      If MOMMAVArray(i) > 100 And MOMMAVArray(i - 1) <= 100 Then
        Last = 1
      ElseIf MOMMAVArray(i) < 100 And MOMMAVArray(i - 1) >= 100 Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  Else
    If MOMArray(1) > 100 Then
      Last = 1
    ElseIf MOMArray(1) < 100 Then
      Last = -1
    End If
    Signalarray(1) = Last
    
    For i = 2 To Size
      If MOMArray(i) > 100 And MOMArray(i - 1) < 100 Then
        Last = 1
      ElseIf MOMArray(i) < 100 And MOMArray(i - 1) > 100 Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  End If
Else
  If ParameterSettings.MOMSignalUseMAV Then
    Last = 0
    Signalarray(1) = Last
    Signalarray(2) = Last
    
    For i = 3 To Size
      If MOMMAVArray(i) > MOMMAVArray(i - 1) And MOMMAVArray(i - 1) < MOMMAVArray(i - 2) Then
        Last = 1
      ElseIf MOMMAVArray(i) < MOMMAVArray(i - 1) And MOMMAVArray(i - 1) > MOMMAVArray(i - 2) Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  Else
    Last = 0
    Signalarray(1) = Last
    Signalarray(2) = Last
    
    For i = 3 To Size
      If MOMArray(i) > MOMArray(i - 1) And MOMArray(i - 1) < MOMArray(i - 2) Then
        Last = 1
      ElseIf MOMArray(i) < MOMArray(i - 1) And MOMArray(i - 1) > MOMArray(i - 2) Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  End If
End If

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: ParabolicSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ParabolicSignalArray(Signalarray() As Long, ParabolicArray() As Boolean, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

'ParabolicCalc PDIndicatorTestArray(), Parabolic(), TempSignal(), Size, False, ParameterSettings.AccelerationFactor

For i = 1 To Size
  If ParabolicArray(i) Then
    Signalarray(i) = 1
  Else
    Signalarray(i) = -1
  End If
Next i

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: RateOfChangeSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RateOfChangeSignalArray(Signalarray() As Long, InputArray() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

Last = 0
Signalarray(1) = 0

For i = 2 To Size
  If InputArray(i) > 0 And InputArray(i - 1) < 0 Then
    Last = 1
  ElseIf InputArray(i) < 0 And InputArray(i - 1) > 0 Then
    Last = -1
  End If
  Signalarray(i) = Last
Next i

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: RSISignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RSISignalArray(Signalarray() As Long, RSIArray() As Single, RSIMAV() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

With ParameterSettings
If .RSISignalUseMAV Then
  Last = 0
  Signalarray(1) = Last
  For i = 2 To Size
    If RSIArray(i) > RSIMAV(i) And RSIArray(i - 1) < RSIMAV(i - 1) Then
      Last = 1
    ElseIf RSIArray(i) < RSIMAV(i - 1) And RSIArray(i - 1) > RSIMAV(i - 1) Then
      Last = -1
    End If
    Signalarray(i) = Last
  Next i
Else
  If .PlotRSIandMAV Then
    Last = 0
    Signalarray(1) = Last
    For i = 2 To Size
      If RSIMAV(i) > .RSILowerSignalLevel And RSIMAV(i - 1) < .RSILowerSignalLevel Then
        Last = 1
      ElseIf RSIMAV(i) < .RSIUpperSignalLevel And RSIMAV(i - 1) > .RSIUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  Else
    Last = 0
    Signalarray(1) = Last
    For i = 2 To Size
      If RSIArray(i) > .RSILowerSignalLevel And RSIArray(i - 1) < .RSILowerSignalLevel Then
        Last = 1
      ElseIf RSIArray(i) < .RSIUpperSignalLevel And RSIArray(i - 1) > .RSIUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  End If
End If
End With
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: STOSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub STOSignalArray(Signalarray() As Long, STOArray() As Single, STOMAV() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

With ParameterSettings
If Not .STOSignalUseSignalLevels Then
  If STOArray(1) > STOMAV(1) Then
    Last = 1
  ElseIf STOArray(1) < STOMAV(1) Then
    Last = -1
  Else
    Last = 0
  End If
  Signalarray(1) = Last
  For i = 2 To Size
    If STOArray(i) > STOMAV(i) And STOArray(i - 1) < STOMAV(i - 1) Then
      Last = 1
    ElseIf STOArray(i) < STOMAV(i) And STOArray(i - 1) > STOMAV(i - 1) Then
      Last = -1
    End If
    Signalarray(i) = Last
  Next i
Else
  If .PlotSTOandMAV Then
    Last = 0
    Signalarray(1) = Last
    For i = 2 To Size
      If STOMAV(i) > .STOLowerSignalLevel And STOMAV(i - 1) < .STOLowerSignalLevel Then
        Last = 1
      ElseIf STOMAV(i) < .STOUpperSignalLevel And STOMAV(i - 1) > .STOUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  Else
    Last = 0
    Signalarray(1) = Last
    For i = 2 To Size
      If STOArray(i) > .STOLowerSignalLevel And STOArray(i - 1) < .STOLowerSignalLevel Then
        Last = 1
      ElseIf STOArray(i) < .STOUpperSignalLevel And STOArray(i - 1) > .STOUpperSignalLevel Then
        Last = -1
      End If
      Signalarray(i) = Last
    Next i
  End If
End If
End With
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: TRIXSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990203 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TRIXSignalArray(Signalarray() As Long, TRIXArray() As Single, TRIXMAVArray() As Single, Size As Long)
Dim i As Long
Dim Last As Long

ReDim Signalarray(Size)

If TRIXArray(1) > TRIXMAVArray(1) Then
  Last = 1
ElseIf TRIXArray(1) < TRIXMAVArray(1) Then
  Last = -1
Else
  Last = 0
End If

Signalarray(1) = Last
For i = 2 To Size
  If TRIXArray(i) > TRIXMAVArray(i) And TRIXArray(i - 1) < TRIXMAVArray(i - 1) Then
    Last = 1
  ElseIf TRIXArray(i) < TRIXMAVArray(i) And TRIXArray(i - 1) > TRIXMAVArray(i - 1) Then
    Last = -1
  End If
  Signalarray(i) = Last
Next i

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: REVSignalArray
'Desc: Plockar fram en array innehållande 1,0,-1. 1 indikerar
'      köpsignal och -1 indikerar säljsignal
'Chng: 990702 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub REVSignalArray(Signalarray() As Long, PDArray() As PriceDataType, Size As Long)
  Dim j As Long
  Dim Last As Long
  
  Last = 0
  Signalarray(1) = Last
  
  For j = 2 To Size
    If PDArray(j).Low <> PDArray(j).High Then
      If PDArray(j).Low < PDArray(j - 1).Low And (((PDArray(j).High - PDArray(j).Close) / (PDArray(j).High - PDArray(j).Low)) < 0.2) And PDArray(j).Close > PDArray(j - 1).Close And Not PDArray(j).Close = 0 Then
        Last = 1
      ElseIf PDArray(j).High > PDArray(j - 1).High And (((PDArray(j).Close - PDArray(j).Low) / (PDArray(j).High - PDArray(j).Low)) < 0.2) And PDArray(j).Close < PDArray(j - 1).Close And Not PDArray(j).Close = 0 Then
        Last = -1
      End If
    End If
  Next j
End Sub

Sub SetupArrayCreator(PDArray() As PriceDataType, lngSetupArray() As Long)
Dim i As Long
Dim outArray() As Boolean
ReDim outArray(UBound(PDArray))
ReDim lngSetupArray(1 To UBound(PDArray))

With GraphSettings
  If .Setup(supReversalGap).blnShow Then
    ReversalGapCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supReversalGap
      End If
    Next i
  End If
  If .Setup(supPatternGap).blnShow Then
    PatternGapCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supPatternGap
      End If
    Next i
  End If
  If .Setup(supTwoDayReversal).blnShow Then
    TwoDayReversalCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supTwoDayReversal
      End If
    Next i
  End If
  If .Setup(supOneDayReversal).blnShow Then
    OneDayReversalCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supOneDayReversal
      End If
    Next i
  End If
  If .Setup(supReversalDay).blnShow Then
    ReversalDayCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supReversalDay
      End If
    Next i
  End If
  If .Setup(supKeyReversalDay).blnShow Then
    KeyReversalDayCalc PDArray, outArray
    For i = 1 To UBound(PDArray)
      If outArray(i) Then
        lngSetupArray(i) = supKeyReversalDay
      End If
    Next i
  End If
End With
End Sub
