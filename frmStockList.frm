VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmStockList 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Börslista"
   ClientHeight    =   8760
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8850
   Icon            =   "frmStockList.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   8760
   ScaleWidth      =   8850
   Begin VB.Frame Frame4 
      Caption         =   "Börslista"
      Height          =   4215
      Left            =   0
      TabIndex        =   10
      Top             =   120
      Width           =   8775
      Begin VB.Frame Frame1 
         Height          =   735
         Index           =   0
         Left            =   0
         TabIndex        =   11
         Top             =   3480
         Width           =   8595
         Begin VB.CommandButton cbOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   0
            Left            =   720
            TabIndex        =   13
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cbCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   0
            Left            =   6600
            TabIndex        =   12
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridBörslista 
         Height          =   3255
         Left            =   0
         TabIndex        =   14
         Top             =   240
         Width           =   8595
         _ExtentX        =   15161
         _ExtentY        =   5741
         _Version        =   393216
         Rows            =   40
         Cols            =   7
         FixedCols       =   0
         ScrollBars      =   2
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Dagens vinnare"
      Height          =   4215
      Left            =   120
      TabIndex        =   5
      Top             =   4680
      Width           =   8775
      Begin VB.Frame Frame1 
         Height          =   735
         Index           =   1
         Left            =   120
         TabIndex        =   6
         Top             =   3360
         Width           =   8595
         Begin VB.CommandButton cbCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   1
            Left            =   6600
            TabIndex        =   8
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cbOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   1
            Left            =   720
            TabIndex        =   7
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridDagensVinnare 
         Height          =   2805
         Left            =   120
         TabIndex        =   9
         Top             =   240
         Width           =   8535
         _ExtentX        =   15055
         _ExtentY        =   4948
         _Version        =   393216
         Rows            =   11
         Cols            =   7
         FixedCols       =   0
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Dagens förlorare"
      Height          =   4215
      Left            =   0
      TabIndex        =   0
      Top             =   4920
      Width           =   8775
      Begin VB.Frame Frame1 
         Height          =   735
         Index           =   2
         Left            =   120
         TabIndex        =   1
         Top             =   3000
         Width           =   8595
         Begin VB.CommandButton cbOK 
            Caption         =   "OK"
            Height          =   375
            Index           =   2
            Left            =   720
            TabIndex        =   3
            Top             =   240
            Width           =   975
         End
         Begin VB.CommandButton cbCancel 
            Caption         =   "Avbryt"
            Height          =   375
            Index           =   2
            Left            =   6600
            TabIndex        =   2
            Top             =   240
            Width           =   975
         End
      End
      Begin MSFlexGridLib.MSFlexGrid GridDagensFörlorare 
         Height          =   2805
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   8595
         _ExtentX        =   15161
         _ExtentY        =   4948
         _Version        =   393216
         Rows            =   11
         Cols            =   7
         FixedCols       =   0
      End
   End
End
Attribute VB_Name = "frmStockList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cbCancel_Click(Index As Integer)
  Unload Me
End Sub

Private Sub cbOK_Click(Index As Integer)
  Unload Me
End Sub

Private Sub Form_Load()
  top = 100
  left = 200
  MainMDI.MousePointer = 11
  
  frmStockList.Width = 8970
  frmStockList.Height = 4995
  
  InitGrids frmStockList.GridBörslista
  InitGrids frmStockList.GridDagensVinnare
  InitGrids frmStockList.GridDagensFörlorare
  
  UpdateGrids
  
  MainMDI.MousePointer = 1
  
End Sub

Private Sub TabStrip1_Click()

End Sub
