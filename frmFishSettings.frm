VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmFishSettings 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "FishNetparametrar"
   ClientHeight    =   2430
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4470
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2430
   ScaleWidth      =   4470
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame5 
      Caption         =   "Steg"
      Height          =   795
      Left            =   0
      TabIndex        =   11
      Top             =   900
      Width           =   4455
      Begin VB.TextBox txtFishStep 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   420
         TabIndex        =   12
         Text            =   "1"
         Top             =   300
         Width           =   795
      End
      Begin MSComCtl2.UpDown updFishStep 
         Height          =   285
         Left            =   1215
         TabIndex        =   13
         Top             =   300
         Width           =   240
         _ExtentX        =   344
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtFishStep"
         BuddyDispid     =   196610
         OrigLeft        =   2400
         OrigTop         =   420
         OrigRight       =   2595
         OrigBottom      =   705
         Max             =   50
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Kortaste medelv�rde"
      Height          =   795
      Left            =   0
      TabIndex        =   8
      Top             =   60
      Width           =   2175
      Begin VB.TextBox txtFishShortest 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   420
         TabIndex        =   9
         Text            =   "1"
         Top             =   300
         Width           =   795
      End
      Begin MSComCtl2.UpDown updFishShortest 
         Height          =   285
         Left            =   1200
         TabIndex        =   10
         Top             =   300
         Width           =   240
         _ExtentX        =   344
         _ExtentY        =   503
         _Version        =   393216
         Value           =   1
         BuddyControl    =   "txtFishShortest"
         BuddyDispid     =   196612
         OrigLeft        =   2400
         OrigTop         =   420
         OrigRight       =   2595
         OrigBottom      =   705
         Max             =   500
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "L�ngsta medelv�rde"
      Height          =   795
      Left            =   2220
      TabIndex        =   5
      Top             =   60
      Width           =   2235
      Begin VB.TextBox txtFishLongest 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   420
         TabIndex        =   6
         Text            =   "250"
         Top             =   300
         Width           =   795
      End
      Begin MSComCtl2.UpDown updFishLongest 
         Height          =   285
         Left            =   1200
         TabIndex        =   7
         Top             =   300
         Width           =   240
         _ExtentX        =   344
         _ExtentY        =   503
         _Version        =   393216
         Value           =   250
         BuddyControl    =   "txtFishLongest"
         BuddyDispid     =   196614
         OrigLeft        =   2400
         OrigTop         =   420
         OrigRight       =   2595
         OrigBottom      =   705
         Max             =   500
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   65547
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   1680
      Width           =   4455
      Begin VB.CommandButton cmdSave 
         Caption         =   "Spara"
         Height          =   375
         Left            =   2280
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdApply 
         Caption         =   "Verkst�ll"
         Height          =   375
         Left            =   3360
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   1200
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmFishSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'#If FISH = 1 Then

'Private Sub cbCancel_Click()
'  Unload frmFishSettings
'End Sub

'Private Sub cbOK_Click()
'  GraphInfoChanged = True
'  UpdateGraph ActiveIndex
'End Sub

Private Sub cmdApply_Click()
  UpdateFishParameterSettings
  GraphInfoChanged = True
  UpdateGraph ActiveIndex
End Sub

Private Sub cmdCancel_Click()
  Unload frmFishSettings
End Sub

Private Sub cmdOk_Click()
  UpdateColorGraphSettings frmColorSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
  Unload Me
End Sub

'Private Sub cmdOK_Click(Index As Integer)
'  UpdateFishParameterSettings
'  GraphInfoChanged = True
'  UpdateGraph ActiveIndex
'  Unload frmFishSettings
'End Sub

Private Sub cmdSave_Click()
  Dim Answer As Long
  
  Answer = YesNoCancel(CheckLang("Vill du spara parametrarna som allm�nna?"), CheckLang("Spara?"))
  Select Case Answer
  Case 6
    UpdateFishParameterSettings
    SaveFishParameterSettingsToRegistry
    MsgBox CheckLang("Parametrarna �r sparade som allm�nna")
  End Select
  If Answer <> 2 Then
    Answer = YesNoCancel("Vill du spara parametrarna som objektberoende?", "Spara?")
  Select Case Answer
  Case 6
    UpdateFishParameterSettings
    SaveFishParameterSettingsToInvest ActiveIndex
    WriteWorkspaceSettings
    MsgBox CheckLang("Parametrarna �r sparade som objektberoende")
  End Select
  End If
End Sub

Private Sub Form_Load()
  CenterForm Me
  InitFishParametets
    If UseGeneralIndicatorParameters Then
    Me.Caption = "FishNet Inst�llningar (Allm�nna)"
  Else
    Me.Caption = "FishNet Inst�llningar (Objektberoende)"
     'MainMDI.C_Category.Enabled = False
     'MainMDI.C_StockList.Enabled = False
  End If
ChangeFormLang Me
End Sub

Private Sub txtFishLongest_GotFocus()
  TextSelected
End Sub

Private Sub txtFishLongest_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtFishLongest, updFishLongest, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtFishShortest_GotFocus()
  TextSelected
End Sub

Private Sub txtFishShortest_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtFishShortest, updFishShortest, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtFishStep_GotFocus()
  TextSelected
End Sub

Private Sub txtFishStep_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtFishStep, updFishStep, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

'#End If
