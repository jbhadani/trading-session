VERSION 5.00
Begin VB.Form Frm_EditTransaction 
   Caption         =   "�ndra transaktion"
   ClientHeight    =   4890
   ClientLeft      =   60
   ClientTop       =   360
   ClientWidth     =   3165
   Icon            =   "frmEditTransaction.frx":0000
   LinkTopic       =   "Form2"
   MDIChild        =   -1  'True
   ScaleHeight     =   4890
   ScaleWidth      =   3165
   Begin VB.Frame Frame1 
      Caption         =   "Datum"
      Height          =   855
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3135
      Begin VB.TextBox TB_Date 
         Height          =   285
         Left            =   240
         TabIndex        =   13
         Top             =   360
         Width           =   1455
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Transaktion"
      Height          =   2175
      Left            =   0
      TabIndex        =   5
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox TB_Volume 
         Height          =   285
         Left            =   1320
         TabIndex        =   8
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox TB_Price 
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox TB_Courtage 
         Height          =   285
         Left            =   1320
         TabIndex        =   6
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Antal"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Kurs"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Courtage"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1680
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   4080
      Width           =   3135
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   1920
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Typ"
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   960
      Width           =   3135
      Begin VB.ComboBox C_BuySell 
         Height          =   315
         Left            =   360
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   360
         Width           =   2175
      End
   End
End
Attribute VB_Name = "Frm_EditTransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Frm_EditTransaction.Left = (MainMDI.Width - Frm_EditTransaction.Width) / 2
  Frm_EditTransaction.Top = (MainMDI.Height - Frm_EditTransaction.Height) / 4
   With C_BuySell
    .AddItem "K�p"
    .AddItem "F�rs�ljning"
    .listindex = 0
  End With
    TB_Date.Text = Date
    Frm_BuySell.Caption = "Buy/Sell in " + MainMDI.C_StockList.Text
    
End Sub

