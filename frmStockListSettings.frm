VERSION 5.00
Begin VB.Form frmStockListSettings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Inst�llningar f�r b�rslistan"
   ClientHeight    =   5970
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4605
   Icon            =   "frmStockListSettings.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   5970
   ScaleWidth      =   4605
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   5160
      Width           =   4575
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   3240
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "Ok"
         Default         =   -1  'True
         Height          =   375
         Left            =   360
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Grupper som ska ing� i b�rslistan"
      Height          =   5175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   4575
      Begin VB.CheckBox chkIncludeCategory 
         Height          =   375
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   360
         Value           =   1  'Checked
         Width           =   855
      End
   End
End
Attribute VB_Name = "frmStockListSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
  Unload frmStockListSettings
End Sub

Private Sub cmdOk_Click()
  UpdateIncludeInStockList
  Unload frmStockListSettings
End Sub

Private Sub Form_Load()
  InitStockListSettingForm
End Sub

