VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmChangeTransaction 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�ndra transaktion"
   ClientHeight    =   4860
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3165
   Icon            =   "frmChangeTransaction.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4860
   ScaleWidth      =   3165
   Begin VB.Frame Frame1 
      Caption         =   "Datum"
      Height          =   855
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3135
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   840
         TabIndex        =   13
         Top             =   360
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61669377
         CurrentDate     =   36402
      End
   End
   Begin VB.Frame Frame2 
      Height          =   2175
      Left            =   0
      TabIndex        =   8
      Top             =   1920
      Width           =   3135
      Begin VB.TextBox tbVolume 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   480
         Width           =   1215
      End
      Begin VB.TextBox tbPrice 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox tbCourtage 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   1680
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Antal"
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   480
         Width           =   855
      End
      Begin VB.Label Label2 
         Caption         =   "Kurs"
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label3 
         Caption         =   "Courtage"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1680
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Height          =   735
      Left            =   0
      TabIndex        =   7
      Top             =   4080
      Width           =   3135
      Begin VB.CommandButton cbOk 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   1920
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Typ"
      Height          =   855
      Left            =   0
      TabIndex        =   6
      Top             =   960
      Width           =   3135
      Begin VB.ComboBox cBuySell 
         Height          =   315
         ItemData        =   "frmChangeTransaction.frx":014A
         Left            =   840
         List            =   "frmChangeTransaction.frx":0154
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   360
         Width           =   1455
      End
   End
End
Attribute VB_Name = "frmChangeTransaction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub cbCancel_Click()
  Unload Me
  Frm_RepList.Enabled = True
End Sub

Private Sub cbOK_Click()
Dim Transaction As TransactionType
Dim TransactionBeingEdited As Long
  
  ReadTransactionFromForm frmChangeTransaction, Transaction
  TransactionBeingEdited = Frm_RepList.Grid1.Row

  If StrComp(frmChangeTransaction.cBuySell, CheckLang("S�lj")) = 0 Then
    If CheckNecessarySharesExist(Transaction, TransactionBeingEdited) Then
      DeleteTransaction Frm_RepList.Grid1.Row
      AddTransaction Transaction
      Unload frmChangeTransaction
      UpdateReportGrid
      Frm_RepList.Enabled = True
    Else
      MsgBox CheckLang("Du har inte s� m�nga aktier som du angivit i formul�ret i den �ppna portf�ljen."), vbOKOnly, CheckLang("Felmeddelande")
      frmChangeTransaction.tbVolume.SetFocus
    End If
  Else
    DeleteTransaction Frm_RepList.Grid1.Row
    AddTransaction Transaction
    Unload frmChangeTransaction
    UpdateReportGrid
    Frm_RepList.Enabled = True
  End If

End Sub

Private Sub Form_Unload(Cancel As Integer)
  Frm_RepList.Enabled = True
End Sub

Private Sub tbCourtage_GotFocus()
  TextSelected
End Sub

Private Sub tbCourtage_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbCourtage
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt courtagebelopp."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbCourtage.SetFocus
End Sub

Private Sub tbPrice_GotFocus()
  TextSelected
End Sub

Private Sub tbPrice_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbPrice
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange en korrekt aktiekurs."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbPrice.SetFocus
End Sub

Private Sub tbVolume_GotFocus()
  TextSelected
End Sub

Private Sub tbVolume_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbVolume
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt antal aktier."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbVolume.SetFocus
End Sub
