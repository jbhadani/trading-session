VERSION 5.00
Begin VB.Form frmSearch 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "S�kning"
   ClientHeight    =   6270
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5790
   Icon            =   "frmSearch.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6270
   ScaleWidth      =   5790
   ShowInTaskbar   =   0   'False
   Begin VB.PictureBox Picture1 
      Height          =   680
      Left            =   0
      Picture         =   "frmSearch.frx":0442
      ScaleHeight     =   615
      ScaleWidth      =   5715
      TabIndex        =   25
      Top             =   60
      Width           =   5775
   End
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   22
      Top             =   5520
      Width           =   5775
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   4200
         TabIndex        =   24
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Command1 
         Caption         =   "S�k"
         Default         =   -1  'True
         Height          =   375
         Left            =   600
         TabIndex        =   23
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Kombinera signaler"
      Height          =   2415
      Left            =   2880
      TabIndex        =   17
      Top             =   3000
      Width           =   2895
      Begin VB.TextBox txtKrav 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   960
         TabIndex        =   21
         Text            =   "1"
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Minsta antal signaler som skall samspela f�r att signal skall ges:"
         Height          =   495
         Left            =   240
         TabIndex        =   20
         Top             =   960
         Width           =   2415
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   2280
         TabIndex        =   19
         Top             =   480
         Width           =   495
      End
      Begin VB.Label Label1 
         Caption         =   "Antal ing�ende indikatorer:"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   480
         Width           =   1935
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Antal dagar som genoms�ks"
      Height          =   855
      Left            =   2880
      TabIndex        =   14
      Top             =   720
      Width           =   2895
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "frmSearch.frx":75BE
         Left            =   1440
         List            =   "frmSearch.frx":75C0
         Style           =   2  'Dropdown List
         TabIndex        =   15
         Top             =   330
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Antal dagar"
         Height          =   255
         Left            =   360
         TabIndex        =   16
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Vilka signaler skall s�kas"
      Height          =   1215
      Left            =   2880
      TabIndex        =   10
      Top             =   1680
      Width           =   2895
      Begin VB.OptionButton OptionOnlyBuy 
         Caption         =   "Endast K�psignaler"
         Height          =   255
         Left            =   480
         TabIndex        =   13
         Top             =   330
         Width           =   1695
      End
      Begin VB.OptionButton OptionOnlySell 
         Caption         =   "Endast S�ljsignaler"
         Height          =   255
         Left            =   480
         TabIndex        =   12
         Top             =   600
         Width           =   1695
      End
      Begin VB.OptionButton OptionBoth 
         Caption         =   "B�de K�p och S�lj"
         Height          =   255
         Left            =   480
         TabIndex        =   11
         Top             =   840
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Indikatorer"
      Height          =   4695
      Left            =   0
      TabIndex        =   0
      Top             =   720
      Width           =   2775
      Begin VB.CheckBox chROC 
         Caption         =   "Rate-of-change, ROC"
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   1920
         Width           =   2175
      End
      Begin VB.CheckBox chReversalGap 
         Caption         =   "Reversal Gap"
         Height          =   255
         Left            =   240
         TabIndex        =   32
         Top             =   4320
         Width           =   2295
      End
      Begin VB.CheckBox chPatternGap 
         Caption         =   "Pattern Gap"
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   4080
         Width           =   2415
      End
      Begin VB.CheckBox chOneDayReversal 
         Caption         =   "One Day Reversal"
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   3840
         Width           =   2415
      End
      Begin VB.CheckBox chTwoDayReversal 
         Caption         =   "Two Day Reversal"
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   3600
         Width           =   2415
      End
      Begin VB.CheckBox chReversalDay 
         Caption         =   "Reversal Day"
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   3360
         Width           =   2415
      End
      Begin VB.CheckBox chTRIX 
         Caption         =   "TRIX"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   2640
         Width           =   2415
      End
      Begin VB.CheckBox chEaseOfMovement 
         Caption         =   "EaseOfMovement, EOM"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   240
         Width           =   2415
      End
      Begin VB.CheckBox CheckkeyReversal 
         Caption         =   "Key Reversal Day"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   3120
         Width           =   1695
      End
      Begin VB.CheckBox Check_STO 
         Caption         =   "Stochastic Oscillator, STO"
         Height          =   255
         Left            =   240
         TabIndex        =   8
         Top             =   2400
         Width           =   2280
      End
      Begin VB.CheckBox Check_RSI 
         Caption         =   "Relative Strength Index, RSI"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   2160
         Width           =   2400
      End
      Begin VB.CheckBox chParabolic 
         Caption         =   "Parabolic, SAR"
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   1440
         Width           =   1815
      End
      Begin VB.CheckBox Check_PriceOsc 
         Caption         =   "Price Oscillator, OSC"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   1680
         Width           =   2280
      End
      Begin VB.CheckBox Check_MFI 
         Caption         =   "Money Flow Index, MFI"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   1200
         Width           =   2280
      End
      Begin VB.CheckBox Check_MOM 
         Caption         =   "Momentum, MOM"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   960
         Width           =   2280
      End
      Begin VB.CheckBox Check_MACD 
         Caption         =   "MACD"
         Height          =   255
         Left            =   240
         TabIndex        =   2
         Top             =   720
         Width           =   2280
      End
      Begin VB.CheckBox Check_MAV 
         Caption         =   "Glidande Medelv�rde, MAV"
         Height          =   255
         Left            =   240
         TabIndex        =   1
         Top             =   480
         Width           =   2280
      End
      Begin VB.Line Line1 
         X1              =   120
         X2              =   2520
         Y1              =   3000
         Y2              =   3000
      End
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub chEaseOfMovement_Click()
  UpdateKrav "EOM"
End Sub

Private Sub Check_MACD_Click()
  UpdateKrav "MACD"
End Sub

Private Sub Check_MAV_Click()
  UpdateKrav "MAV"
End Sub

Private Sub Check_MFI_Click()
  UpdateKrav "MFI"
End Sub

Private Sub Check_MOM_Click()
  UpdateKrav "MOM"
End Sub

Private Sub Check_PriceOsc_Click()
  UpdateKrav "OSC"
End Sub

Private Sub Check_RSI_Click()
  UpdateKrav "RSI"
End Sub

Private Sub Check_STO_Click()
  UpdateKrav "STO"
End Sub

Private Sub CheckkeyReversal_Click()
  UpdateKrav "REV"
End Sub

Private Sub chOneDayReversal_Click()
  UpdateKrav "One Day Reversal"
End Sub

Private Sub chParabolic_Click()
  UpdateKrav "SAR"
End Sub

Private Sub chPatternGap_Click()
  UpdateKrav "Pattern Gap"
End Sub

Private Sub chReversalDay_Click()
  UpdateKrav "Reversal Day"
End Sub

Private Sub chReversalGap_Click()
  UpdateKrav "Reversal Gap"
End Sub

Private Sub chROC_Click()
  UpdateKrav "ROC"
End Sub

Private Sub chTRIX_Click()
  UpdateKrav "TRIX"
End Sub

Private Sub chTwoDayReversal_Click()
  UpdateKrav "Two Day Reversal"
End Sub

Private Sub Command1_Click()
  
  With frmSearch
    If .Check_MACD = 1 Or .Check_MAV = 1 Or .Check_MFI = 1 Or .Check_MOM = 1 Or .Check_PriceOsc = 1 Or .Check_RSI = 1 Or .Check_STO = 1 Or .CheckkeyReversal = 1 Or .chParabolic = 1 Or .chEaseOfMovement = 1 Or .chTRIX = 1 Or .chReversalDay = 1 Or .chTwoDayReversal = 1 Or .chOneDayReversal = 1 Or .chPatternGap = 1 Or .chReversalGap = 1 Or .chROC = 1 Then
      Search
    End If
  End With
  
End Sub

Private Sub Command2_Click()
  frmSearch.Hide
End Sub

Private Sub Form_Load()
  Width = 5955
  Height = 6645
  top = 50
  left = 300
 Dim I As Long
  With frmSearch
    For I = 0 To 9
      .Combo1.AddItem I + 1
    Next I
    .Combo1.Text = "1"
    .Label3.Caption = 0
  End With
End Sub

Private Sub Option_Date_Click()
  frmSearch.Combo1.Enabled = False
End Sub

Private Sub Option_Days_Click()
  frmSearch.Combo1.Enabled = True
End Sub

Private Sub txtKrav_GotFocus()
  TextSelected
End Sub

Private Sub txtKrav_Validate(Cancel As Boolean)
  VerifyLong txtKrav, Cancel
  If Cancel Then
    MsgBox CheckLang("Du m�ste ange ett korrekt heltal."), vbOKOnly, CheckLang("Felmeddelande")
  End If
  txtKrav.SetFocus
End Sub
