Attribute VB_Name = "StockListHandler"
Option Explicit


Public Sub InitGrids(StockGrid As Object)
  With StockGrid
    .Clear
    .Width = 8595
    .ColWidth(0) = 2085
    .ColWidth(1) = 1000
    .ColWidth(2) = 1000
    .ColWidth(3) = 900
    .ColWidth(4) = 900
    .ColWidth(5) = 900
    .ColWidth(6) = 1450
    .Row = 0
    .Col = 0
    .Text = CheckLang("Aktie")
    .Col = 1
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("% idag")
    .Col = 2
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("+/- idag")
    .Col = 3
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("Senast")
    .Col = 4
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("Högst")
    .Col = 5
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("Lägst")
    .Col = 6
    .CellAlignment = flexAlignCenterTop
    .Text = CheckLang("Omsatt")
  End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          UpdateGrids
'Input:
'Output:
'
'Description:   Process the data for presentation in StockList
'Author:        Martin Lindberg
'Revision:      98XXXX  Created
'               980928  Selection added
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub UpdateGrids()
  Dim I As Long
  Dim j As Long
  Dim TodayWinners(10) As StockListType
  Dim TodayLosers(10) As StockListType
  Dim StockList() As StockListType
  Dim NbrOfWinners As Long
  Dim NbrOfLosers As Long
  Dim CellColor As Long
  Dim temp As Single
  Dim LastDate As Date
  Dim tmpStr As String
  
  Dim FileName As String
  Dim NbrOfRowsInGrid As Long
  
  Dim LastPriceData As PriceDataType
  Dim SecondLastPriceData As PriceDataType
  Dim lngStatus As Long
  
  NbrOfWinners = 0
  NbrOfLosers = 0

  ReDim StockList(InvestInfoArraySize)
  NbrOfRowsInGrid = 0
  LastDate = 0
  
  For I = 1 To InvestInfoArraySize
     
    If gblnIncludeInStockList(InvestInfoArray(I).TypeOfInvest) Then
      
      'Read price data file
      FileName = DataPath + Trim(InvestInfoArray(I).FileName) + ".prc"
      
      lngStatus = Get2LastDays(FileName, LastPriceData, SecondLastPriceData)
      #If DBUG = 1 Then
'        Debug.Print "Get2LstDays reported lngStatus: " & CStr(lngStatus) & vbTab & InvestInfoArray(i).Name
      #End If
      If lngStatus = 2 Then
        NbrOfRowsInGrid = NbrOfRowsInGrid + 1
        StockList(NbrOfRowsInGrid).Name = Trim(InvestInfoArray(I).Name)
        StockList(NbrOfRowsInGrid).Close = LastPriceData.Close
        StockList(NbrOfRowsInGrid).High = LastPriceData.High
        StockList(NbrOfRowsInGrid).Low = LastPriceData.Low
        StockList(NbrOfRowsInGrid).Volume = LastPriceData.Volume
        StockList(NbrOfRowsInGrid).Date = LastPriceData.Date
        
        If Int(LastDate) < Int(LastPriceData.Date) Then
          LastDate = LastPriceData.Date
        End If
        
        If SecondLastPriceData.Close <> 0 And LastPriceData.Close <> 0 Then
          'StockList(i).UppDownToday = CInt(((LastPriceData.Close) - (SecondLastPriceData.Close)) * 10) / 10
          'StockList(i).PercentToday = CLng((StockList(i).UppDownToday / SecondLastPriceData.Close) * 10000) / 100
          StockList(NbrOfRowsInGrid).UppDownToday = LastPriceData.Close - SecondLastPriceData.Close
          StockList(NbrOfRowsInGrid).PercentToday = StockList(NbrOfRowsInGrid).UppDownToday / SecondLastPriceData.Close * 100
        Else
          StockList(NbrOfRowsInGrid).UppDownToday = 0
          StockList(NbrOfRowsInGrid).PercentToday = 0
        End If
        
        UpdateTodayWinners StockList(NbrOfRowsInGrid), NbrOfWinners, TodayWinners
        UpdateTodayLosers StockList(NbrOfRowsInGrid), NbrOfLosers, TodayLosers
      End If
      
    End If
  Next I
  With frmStockList.GridBörslista
    '.Clear
    .Rows = NbrOfRowsInGrid + 1
    
    For I = 1 To NbrOfRowsInGrid
      If StockList(I).UppDownToday > 0 Then
        CellColor = 2
      ElseIf StockList(I).UppDownToday = 0 Then
        CellColor = 0
      Else
        CellColor = 12
      End If
      .Row = I
      If Int(StockList(I).Date) < Int(LastDate) Then
        For j = 0 To 6
          .Col = j
          .CellBackColor = RGB(192, 192, 192)
        Next j
      End If
      .Col = 0
      .CellForeColor = QBColor(CellColor)
      .Text = StockList(I).Name
      .Col = 1
      .CellForeColor = QBColor(CellColor)
      .Text = FormatNumber(StockList(I).PercentToday, 2)
      .Col = 2
      .CellForeColor = QBColor(CellColor)
      .Text = FormatNumber(StockList(I).UppDownToday, 2)
      .Col = 3
      .CellForeColor = QBColor(CellColor)
      .Text = FormatNumber(StockList(I).Close, 2)
      .Col = 4
      .CellForeColor = QBColor(CellColor)
      .Text = FormatNumber(StockList(I).High, 2)
      .Col = 5
      .CellForeColor = QBColor(CellColor)
      .Text = FormatNumber(StockList(I).Low, 2)
      .Col = 6
      .CellForeColor = QBColor(CellColor)
      .Text = StockList(I).Volume
    Next I
  End With
    
  With frmStockList.GridDagensVinnare
    '.Clear
    .Rows = NbrOfWinners + 1
    If NbrOfWinners > 0 Then
      CellColor = 2
      For I = 1 To NbrOfWinners
        .Row = I
        .Col = 0
        .CellForeColor = QBColor(CellColor)
        .Text = TodayWinners(I).Name
        .Col = 1
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayWinners(I).PercentToday, 2)
        .Col = 2
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayWinners(I).UppDownToday, 2)
        .Col = 3
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayWinners(I).Close, 2)
        .Col = 4
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayWinners(I).High, 2)
        .Col = 5
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayWinners(I).Low, 2)
        .Col = 6
        .CellForeColor = QBColor(CellColor)
        .Text = TodayWinners(I).Volume
      Next I
    End If
  End With
  
  With frmStockList.GridDagensFörlorare
    '.Clear
    .Rows = NbrOfLosers + 1
    If NbrOfLosers > 0 Then
      CellColor = 12
      For I = 1 To NbrOfLosers
        .Row = I
        .Col = 0
        .CellForeColor = QBColor(CellColor)
        .Text = TodayLosers(I).Name
        .Col = 1
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayLosers(I).PercentToday, 2)
        .Col = 2
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayLosers(I).UppDownToday, 2)
        .Col = 3
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayLosers(I).Close, 2)
        .Col = 4
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayLosers(I).High, 2)
        .Col = 5
        .CellForeColor = QBColor(CellColor)
        .Text = FormatNumber(TodayLosers(I).Low, 2)
        .Col = 6
        .CellForeColor = QBColor(CellColor)
        .Text = TodayLosers(I).Volume
      Next I
    End If
  End With
  
End Sub



Public Sub UpdateTodayWinners(StockList As StockListType, NbrOfWinners As Long, TodayWinners() As StockListType)
  Dim j As Long
  
  If StockList.PercentToday > 0 Then
    For j = 1 To 10
      If StockList.PercentToday > TodayWinners(j).PercentToday Then
        If NbrOfWinners <> 10 Then
          NbrOfWinners = NbrOfWinners + 1
        End If
        InsertStock StockList, TodayWinners(), j, NbrOfWinners
        j = 10
      End If
    Next j
  End If
  
End Sub

Public Sub UpdateTodayLosers(StockList As StockListType, NbrOfLosers As Long, TodayLosers() As StockListType)
  Dim j As Long
  
  If StockList.PercentToday < 0 Then
    For j = 1 To 10
      If StockList.PercentToday < TodayLosers(j).PercentToday Then
        If NbrOfLosers <> 10 Then
          NbrOfLosers = NbrOfLosers + 1
        End If
        InsertStock StockList, TodayLosers(), j, NbrOfLosers
        j = 10
      End If
    Next j
  End If

End Sub

Public Sub InsertStock(InsertItem As StockListType, Destination() As StockListType, Position As Long, NbrOfItems As Long)
  Dim k As Long
  k = NbrOfItems
  
  If Position = 10 Then
    Destination(10) = InsertItem
    Exit Sub
  ElseIf Position > NbrOfItems Then
    Destination(Position) = InsertItem
    Exit Sub
  Else
    Do While k <> Position
      Destination(k) = Destination(k - 1)
      k = k - 1
    Loop
    Destination(k) = InsertItem
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          InitStockListSettings
'Input:
'Output:
'
'Description:   Initialize frmStockListSettings
'Author:        Fredrik Wendel
'Revision:      980928  Created
'               130603 MW changed to dynamic checkbox creation
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub InitStockListSettingForm()

Const NbrOfRows = 15 'Nbr of check rows in frmStocListForm (old value 10)

Dim I As Long
Dim NbrOfCategories As Long
  
  With frmStockListSettings
  
  NbrOfCategories = 0
  
  Dim strCaption As String
  
  For I = 1 To CategoryArraySize
    
    ' Old frmStockListSettings height: 4890
    ' Old frmStockListSettings width: 4695
    ' Old Frame1 height: 3615
    ' Old Frame1 width: 4575

    ' Create new checkboxes dynamically using
    ' chkIncludeCategory(0) as template
    Load .chkIncludeCategory(I)

    If Trim(CategoryArray(I)) <> "" Then

      strCaption = Trim(CategoryArray(I))
      
      ' Prevent ampersand from being converted to underscore
      strCaption = Replace(strCaption, "&", "&&")
      
      .chkIncludeCategory(I - 1).Caption = strCaption

      .chkIncludeCategory(I - 1).Visible = True
      
      If NbrOfCategories <= (NbrOfRows - 1) Then
        .chkIncludeCategory(I - 1).left = 240
      Else
        .chkIncludeCategory(I - 1).left = 2500
      End If
      
      .chkIncludeCategory(I - 1).Width = 2000
      
      If NbrOfCategories <= NbrOfRows - 1 Then
        .chkIncludeCategory(I - 1).top = 360 + NbrOfCategories * 300
      Else
        .chkIncludeCategory(I - 1).top = 360 + (NbrOfCategories - NbrOfRows) * 300
      End If
      
      .chkIncludeCategory(I - 1).TabStop = True
      .chkIncludeCategory(I - 1).TabIndex = NbrOfCategories
      
      NbrOfCategories = NbrOfCategories + 1
      
      If gblnIncludeInStockList(I) Then
        .chkIncludeCategory(I - 1).Value = 1
      Else
        .chkIncludeCategory(I - 1).Value = 0
      End If
    
    Else
      
      .chkIncludeCategory(I - 1).left = 10000
      .chkIncludeCategory(I - 1).TabStop = False
      .chkIncludeCategory(I - 1).Value = 0
    
    End If
  
  Next I
  
  ' TODO: No longer needed, now that checkboxes are created dynamically?
  'For i = CategoryArraySize + 1 To 20
  '  .chkIncludeCategory(i - 1).Visible = False
  'Next i
  
  End With
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          UpdateStockListSettings
'Input:
'Output:
'
'Description:   Update gblnInclude in StockList
'Author:        Fredrik Wendel
'Revision:      980928  Created
'               980929  Sving to registry was added
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Sub UpdateIncludeInStockList()
Dim I As Long

  For I = 1 To CategoryArraySize
    If frmStockListSettings.chkIncludeCategory(I - 1).Value = 1 Then
      gblnIncludeInStockList(I) = True
    Else
      gblnIncludeInStockList(I) = False
    End If
    SaveSetting App.Title, "StockList", "Include" & Trim(Str(I)), CStr(gblnIncludeInStockList(I))
  Next I
End Sub
