Attribute VB_Name = "PublicFunctions"
Option Explicit

' Decrypts Market Access-style encrypted password from the registry
Public Function DecryptPassword(strPass As String) As String

    Dim Out As String

    Dim i As Integer
    Dim c As String
    For i = 0 To Len(strPass) - 1
        c = Mid(strPass, Len(strPass) - i, 1)
        Out = Out & c
    Next i
    
    DecryptPassword = Out
    
End Function

' Checks if file exists
Public Function FileExists(strFilePath As String) As Boolean

  Dim fso As FileSystemObject

  Set fso = New FileSystemObject
  
  If fso.FileExists(strFilePath) Then
      ' TODO:
      'MsgBox strFilePath & " exists!"
      FileExists = True
  Else
      'MsgBox strFilePath & " doesn't exist!"
      FileExists = False
  End If

End Function
