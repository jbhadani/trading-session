VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmParameters 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Indikatorparametrar"
   ClientHeight    =   4290
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9240
   Icon            =   "frmParameters.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4290
   ScaleWidth      =   9240
   Begin VB.Frame Frame3 
      Height          =   795
      Index           =   0
      Left            =   60
      TabIndex        =   1
      Top             =   3420
      Width           =   9075
      Begin VB.CommandButton cmdApply 
         Caption         =   "Verkst�ll"
         Height          =   375
         Left            =   7200
         TabIndex        =   5
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   2640
         TabIndex        =   4
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdOk 
         Caption         =   "OK"
         Height          =   375
         Left            =   360
         TabIndex        =   3
         Top             =   240
         Width           =   1275
      End
      Begin VB.CommandButton cmdSave 
         Caption         =   "Spara"
         Height          =   375
         Left            =   5040
         TabIndex        =   2
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame50 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   7
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtVolumeRateOfChangeLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   8
         Text            =   "12"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownVROC 
         Height          =   285
         Left            =   1680
         TabIndex        =   9
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   12
         BuddyControl    =   "Frame23"
         BuddyDispid     =   196724
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   200
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame49 
      Caption         =   "Antal perioder f�r medelv�rde 2"
      Height          =   855
      Left            =   120
      TabIndex        =   14
      Top             =   960
      Width           =   2970
      Begin VB.TextBox txtVolumeOscillatorMAV2Length 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   15
         Text            =   "25"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownVO2 
         Height          =   285
         Left            =   1680
         TabIndex        =   16
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   25
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   645
         Max             =   200
         Min             =   3
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame48 
      Caption         =   "Antal perioder f�r medelv�rde 1"
      Height          =   855
      Left            =   120
      TabIndex        =   11
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtVolumeOscillatorMAV1Length 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   12
         Text            =   "15"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownVO1 
         Height          =   285
         Left            =   1680
         TabIndex        =   13
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   15
         BuddyControl    =   "UpDownMOM1"
         BuddyDispid     =   196799
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   640
         Max             =   200
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame47 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   24
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton optTRIXShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   26
         Top             =   480
         Width           =   810
      End
      Begin VB.OptionButton optTRIXShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   25
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
   End
   Begin VB.Frame Frame46 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   21
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtTRIXMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   22
         Text            =   "9"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownTRIX2 
         Height          =   285
         Left            =   1680
         TabIndex        =   23
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   9
         BuddyControl    =   "txtVolumeRateOfChangeLength"
         BuddyDispid     =   196615
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame45 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   18
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtTRIXLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   19
         Text            =   "12"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownTRIX1 
         Height          =   285
         Left            =   1680
         TabIndex        =   20
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   12
         BuddyControl    =   "Option_DontShowMOMMAV"
         BuddyDispid     =   196722
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame57 
      Caption         =   "�vre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   46
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtSTOUpperSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   47
         Text            =   "80"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTO5 
         Height          =   285
         Left            =   1680
         TabIndex        =   48
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   80
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   645
         Max             =   99
         Min             =   51
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame56 
      Caption         =   "Undre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   43
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtSTOLowerSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   44
         Text            =   "20"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTO4 
         Height          =   285
         Left            =   1680
         TabIndex        =   45
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "UpDownVO1"
         BuddyDispid     =   196760
         OrigLeft        =   1440
         OrigTop         =   360
         OrigRight       =   1635
         OrigBottom      =   495
         Max             =   49
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame55 
      Caption         =   "Slowing %K"
      Height          =   855
      Left            =   120
      TabIndex        =   40
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtSTOSlowing 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   41
         Text            =   "3"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTO2 
         Height          =   285
         Left            =   1680
         TabIndex        =   42
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   3
         BuddyControl    =   "Tb_MOMMAVLength"
         BuddyDispid     =   196720
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   615
         Max             =   500
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame4 
      Caption         =   "Signalmetod"
      Height          =   855
      Left            =   6120
      TabIndex        =   37
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton optSTOSignalUseMAV 
         Caption         =   "Anv�nd sk�rningar med MAV"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   480
         Width           =   2415
      End
      Begin VB.OptionButton optSTOSignalUseLevels 
         Caption         =   "Anv�nd signalniv�er"
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   240
         Value           =   -1  'True
         Width           =   1740
      End
   End
   Begin VB.Frame Frame18 
      Caption         =   "Antal perioder %K"
      Height          =   855
      Left            =   120
      TabIndex        =   34
      Top             =   0
      Width           =   2775
      Begin VB.TextBox Tb_StoOscPeriodK 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   35
         Text            =   "frmParameters.frx":030A
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTO1 
         Height          =   285
         Left            =   1680
         TabIndex        =   36
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   10
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame17 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   6120
      TabIndex        =   31
      Top             =   960
      Width           =   2775
      Begin VB.OptionButton Option_ShowOsc 
         Caption         =   "Nej"
         Height          =   255
         Index           =   0
         Left            =   720
         TabIndex        =   33
         Top             =   480
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton Option_ShowOscAndMAV 
         Caption         =   "Ja"
         Height          =   255
         Index           =   0
         Left            =   720
         TabIndex        =   32
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Frame19 
      Caption         =   "Antal perioder %D"
      Height          =   855
      Left            =   120
      TabIndex        =   28
      Top             =   1920
      Width           =   2775
      Begin VB.TextBox Tb_StoOscPeriodD 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   29
         Text            =   "5"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTO3 
         Height          =   285
         Left            =   1680
         TabIndex        =   30
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   3
         BuddyControl    =   "Frame29"
         BuddyDispid     =   196713
         OrigLeft        =   1320
         OrigTop         =   360
         OrigRight       =   1515
         OrigBottom      =   615
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame44 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   56
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton optSTDShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   58
         Top             =   480
         Width           =   735
      End
      Begin VB.OptionButton optSTDShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   57
         Top             =   240
         Value           =   -1  'True
         Width           =   615
      End
   End
   Begin VB.Frame Frame43 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   53
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtStandardDeviationMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   54
         Text            =   "5"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTD2 
         Height          =   285
         Left            =   1680
         TabIndex        =   55
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   645
         Max             =   200
         Min             =   2
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame42 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   50
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtStandardDeviationLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   51
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownSTD1 
         Height          =   285
         Left            =   1680
         TabIndex        =   52
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "UpDownTRIX2"
         BuddyDispid     =   196762
         OrigLeft        =   1500
         OrigTop         =   360
         OrigRight       =   1695
         OrigBottom      =   645
         Max             =   200
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame52 
      Caption         =   "�vre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   75
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtRSIUpperSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   76
         Text            =   "70"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownRSI4 
         Height          =   285
         Left            =   1680
         TabIndex        =   77
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   70
         BuddyControl    =   "OptionMOMSignalUseMAV"
         BuddyDispid     =   196718
         OrigLeft        =   1920
         OrigTop         =   360
         OrigRight       =   2115
         OrigBottom      =   640
         Max             =   99
         Min             =   51
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame51 
      Caption         =   "Undre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   72
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtRSILowerSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   73
         Text            =   "30"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownRSI3 
         Height          =   285
         Left            =   1680
         TabIndex        =   74
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   30
         BuddyControl    =   "txtSTOLowerSignalLevel"
         BuddyDispid     =   196630
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   49
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Signalmetod"
      Height          =   855
      Index           =   1
      Left            =   3120
      TabIndex        =   69
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton Option_SignalRSIUseMAV 
         Caption         =   "Anv�nd sk�rningar med MAV"
         Height          =   255
         Left            =   120
         TabIndex        =   71
         Top             =   480
         Value           =   -1  'True
         Width           =   2475
      End
      Begin VB.OptionButton Option_SignalRSIDontUseMAV 
         Caption         =   "Anv�nd signalniv�er"
         Height          =   255
         Left            =   120
         TabIndex        =   70
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Frame22 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   66
      Top             =   960
      Width           =   2775
      Begin VB.TextBox Tb_RSIMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   67
         Text            =   "frmParameters.frx":030D
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownRSI2 
         Height          =   285
         Left            =   1680
         TabIndex        =   68
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "Frame28"
         BuddyDispid     =   196716
         OrigLeft        =   1920
         OrigTop         =   240
         OrigRight       =   2115
         OrigBottom      =   520
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame21 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   63
      Top             =   0
      Width           =   2775
      Begin VB.TextBox Tb_RSILength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   64
         Text            =   "frmParameters.frx":030F
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownRSI1 
         Height          =   285
         Left            =   1680
         TabIndex        =   65
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "optSTOSignalUseLevels"
         BuddyDispid     =   196635
         OrigLeft        =   2040
         OrigTop         =   360
         OrigRight       =   2235
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame20 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   60
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton Option2 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   62
         Top             =   480
         Width           =   930
      End
      Begin VB.OptionButton Option_ShowRSI 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   61
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
   End
   Begin VB.Frame Frame41 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   85
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton optROCShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   87
         Top             =   480
         Width           =   615
      End
      Begin VB.OptionButton optROCShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   86
         Top             =   240
         Value           =   -1  'True
         Width           =   810
      End
   End
   Begin VB.Frame Frame40 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   82
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtRateOfChangeMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   83
         Text            =   "9"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownROC2 
         Height          =   285
         Left            =   1680
         TabIndex        =   84
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   9
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame39 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   79
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtRateOfChangeLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   80
         Text            =   "5"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownROC1 
         Height          =   285
         Left            =   1680
         TabIndex        =   81
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "UpDownSTD1"
         BuddyDispid     =   196772
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame26 
      Caption         =   "Typ av medelv�rde"
      Height          =   855
      Left            =   3120
      TabIndex        =   95
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton Option_PriceOsc_Exponential 
         Caption         =   "Exponentiellt"
         Height          =   255
         Left            =   840
         TabIndex        =   97
         Top             =   480
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton Option_PriceOsc_Simple 
         Caption         =   "Ov�gt"
         Height          =   255
         Left            =   840
         TabIndex        =   96
         Top             =   240
         Width           =   1290
      End
   End
   Begin VB.Frame Frame25 
      Caption         =   "Antal perioder f�r medelv�rde 2"
      Height          =   855
      Left            =   120
      TabIndex        =   92
      Top             =   960
      Width           =   2775
      Begin VB.TextBox Tb_PriceOsc_MAV2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   93
         Text            =   "frmParameters.frx":0312
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownPriceOsc2 
         Height          =   285
         Left            =   1680
         TabIndex        =   94
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   40
         BuddyControl    =   "Frame12"
         BuddyDispid     =   196734
         OrigLeft        =   1800
         OrigTop         =   240
         OrigRight       =   1995
         OrigBottom      =   495
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame24 
      Caption         =   "Antal perioder f�r medelv�rde 1"
      Height          =   855
      Left            =   120
      TabIndex        =   89
      Top             =   0
      Width           =   2775
      Begin VB.TextBox Tb_PriceOsc_MAV1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   90
         Text            =   "frmParameters.frx":0317
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownPriceOsc1 
         Height          =   285
         Left            =   1680
         TabIndex        =   91
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "UpDownSTO5"
         BuddyDispid     =   196765
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   615
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame38 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   102
      Top             =   960
      Width           =   2775
      Begin VB.OptionButton optPVShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   104
         Top             =   480
         Width           =   615
      End
      Begin VB.OptionButton optPVShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   103
         Top             =   240
         Value           =   -1  'True
         Width           =   810
      End
   End
   Begin VB.Frame Frame36 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   99
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtPositiveVolumeMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   100
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownPV 
         Height          =   285
         Left            =   1680
         TabIndex        =   101
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "UpDownSTO4"
         BuddyDispid     =   196766
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame9 
      Caption         =   "Accelerationsfaktor"
      Height          =   855
      Left            =   120
      TabIndex        =   106
      Top             =   0
      Width           =   2775
      Begin VB.TextBox tbAccelerationfactor 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   107
         Text            =   "frmParameters.frx":031C
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame35 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   112
      Top             =   960
      Width           =   2775
      Begin VB.OptionButton optNVShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   114
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton optNVShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   113
         Top             =   480
         Width           =   735
      End
   End
   Begin VB.Frame Frame34 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   109
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtNegativeVolumeMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   110
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownNV 
         Height          =   285
         Left            =   1680
         TabIndex        =   111
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "Frame20"
         BuddyDispid     =   196660
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame16 
      Caption         =   "L�ngt medelv�rde"
      Height          =   855
      Left            =   120
      TabIndex        =   125
      Top             =   960
      Width           =   2775
      Begin VB.TextBox Para_MAV_Period2 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   126
         Text            =   "frmParameters.frx":0321
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownParaMAVPeriod2 
         Height          =   285
         Left            =   1680
         TabIndex        =   127
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "Frame44"
         BuddyDispid     =   196643
         OrigLeft        =   1920
         OrigTop         =   360
         OrigRight       =   2115
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame15 
      Caption         =   "Antal medelv�rden"
      Height          =   855
      Left            =   3120
      TabIndex        =   122
      Top             =   960
      Width           =   2775
      Begin VB.OptionButton Option_TwoMAV 
         Caption         =   "Tv�"
         Height          =   255
         Left            =   120
         TabIndex        =   124
         Top             =   480
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton Option_OneMAV 
         Caption         =   "Ett"
         Height          =   255
         Left            =   120
         TabIndex        =   123
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Frame14 
      Caption         =   "Typ av medelv�rde"
      Height          =   855
      Left            =   3120
      TabIndex        =   119
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton Option_Exp 
         Caption         =   "Exponentiellt medelv�rde"
         Height          =   255
         Left            =   120
         TabIndex        =   121
         Top             =   480
         Value           =   -1  'True
         Width           =   2610
      End
      Begin VB.OptionButton Option_Simple 
         Caption         =   "Ov�gt medelv�rde"
         Height          =   255
         Left            =   120
         TabIndex        =   120
         Top             =   240
         Width           =   2175
      End
   End
   Begin VB.Frame Frame13 
      Caption         =   "Kort medelv�rde"
      Height          =   855
      Left            =   120
      TabIndex        =   116
      Top             =   0
      Width           =   2775
      Begin VB.TextBox Para_MAV_Period1 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   117
         Text            =   "frmParameters.frx":0324
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownParaMAVPeriod1 
         Height          =   285
         Left            =   1680
         TabIndex        =   118
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   10
         BuddyControl    =   "Tb_RSIMAVLength"
         BuddyDispid     =   196657
         OrigLeft        =   1560
         OrigTop         =   360
         OrigRight       =   1755
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame54 
      Caption         =   "�vre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   144
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtMFIUpperSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   145
         Text            =   "70"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMFI4 
         Height          =   285
         Left            =   1680
         TabIndex        =   146
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   70
         BuddyControl    =   "Frame21"
         BuddyDispid     =   196658
         OrigLeft        =   2040
         OrigTop         =   360
         OrigRight       =   2235
         OrigBottom      =   615
         Max             =   99
         Min             =   51
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame53 
      Caption         =   "Undre signalgr�ns"
      Height          =   855
      Left            =   3120
      TabIndex        =   141
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtMFILowerSignalLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   142
         Text            =   "30"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMFI3 
         Height          =   285
         Left            =   1680
         TabIndex        =   143
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   30
         BuddyControl    =   "optEOMandMAVYes"
         BuddyDispid     =   196731
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   49
         Min             =   1
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Signaler"
      Height          =   855
      Left            =   3120
      TabIndex        =   138
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton OptionMFISignalUseMAV 
         Caption         =   "Anv�nd sk�rningar med MAV"
         Height          =   255
         Left            =   120
         TabIndex        =   140
         Top             =   480
         Width           =   2415
      End
      Begin VB.OptionButton OptionMFISignalDontUseMAV 
         Caption         =   "Anv�nd signalniv�er"
         Height          =   255
         Left            =   120
         TabIndex        =   139
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
   End
   Begin VB.Frame Frame33 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   135
      Top             =   960
      Width           =   2775
      Begin VB.TextBox tbMFIMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   136
         Text            =   "frmParameters.frx":0326
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMFI2 
         Height          =   285
         Left            =   1680
         TabIndex        =   137
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   10
         BuddyControl    =   "Option_SignalRSIUseMAV"
         BuddyDispid     =   196654
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame32 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   132
      Top             =   0
      Width           =   2775
      Begin VB.TextBox tbMFILength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   133
         Text            =   "frmParameters.frx":0329
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMFI1 
         Height          =   285
         Left            =   1680
         TabIndex        =   134
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   25
         BuddyControl    =   "Frame51"
         BuddyDispid     =   196652
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame31 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   129
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton OptionShowMFI 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   131
         Top             =   480
         Width           =   1050
      End
      Begin VB.OptionButton OptionShowMFIAndMAV 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   130
         Top             =   240
         Value           =   -1  'True
         Width           =   930
      End
   End
   Begin VB.Frame Frame29 
      Caption         =   "MomentumSignal"
      Height          =   855
      Left            =   3120
      TabIndex        =   160
      Top             =   960
      Width           =   2775
      Begin VB.OptionButton OptionMOMSignalUseMINMAX 
         Caption         =   "Anv�nd toppar och bottnar"
         Height          =   255
         Left            =   120
         TabIndex        =   162
         Top             =   480
         Width           =   2490
      End
      Begin VB.OptionButton OptionSignalUseZeroLine 
         Caption         =   "Anv�nd sk�rning med nollinjen"
         Height          =   255
         Left            =   120
         TabIndex        =   161
         Top             =   240
         Value           =   -1  'True
         Width           =   2535
      End
   End
   Begin VB.Frame Frame28 
      Caption         =   "Anv�nd medelv�rde f�r signal"
      Height          =   855
      Left            =   3120
      TabIndex        =   157
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton OptionDontUseMAVInSignal 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   159
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton OptionMOMSignalUseMAV 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   158
         Top             =   240
         Value           =   -1  'True
         Width           =   1170
      End
   End
   Begin VB.Frame Frame5 
      Caption         =   "Medelv�rdesl�ngd"
      Height          =   855
      Left            =   120
      TabIndex        =   154
      Top             =   960
      Width           =   2775
      Begin VB.TextBox Tb_MOMMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   155
         Text            =   "frmParameters.frx":032C
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMOM2 
         Height          =   285
         Left            =   1680
         TabIndex        =   156
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   10
         BuddyControl    =   "txtRSIUpperSignalLevel"
         BuddyDispid     =   196651
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   151
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton Option_DontShowMOMMAV 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   153
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton Option_ShowMOMMAV 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   152
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
   End
   Begin VB.Frame Frame23 
      Caption         =   "Periodl�ngd"
      Height          =   855
      Left            =   120
      TabIndex        =   148
      Top             =   0
      Width           =   2775
      Begin VB.TextBox Tb_MOMLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   149
         Text            =   "frmParameters.frx":032F
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownMOM1 
         Height          =   285
         Left            =   1680
         TabIndex        =   150
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "Tb_PriceOsc_MAV1"
         BuddyDispid     =   196676
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Visa MACD signallinje"
      Height          =   855
      Left            =   120
      TabIndex        =   164
      Top             =   0
      Width           =   2775
      Begin VB.OptionButton OptionShowMACDOnly 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   166
         Top             =   480
         Width           =   855
      End
      Begin VB.OptionButton Option_MACDSignal 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   165
         Top             =   240
         Value           =   -1  'True
         Width           =   1050
      End
   End
   Begin VB.Frame Frame30 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   174
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton optEOMandMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   960
         TabIndex        =   176
         Top             =   480
         Width           =   615
      End
      Begin VB.OptionButton optEOMandMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   960
         TabIndex        =   175
         Top             =   240
         Value           =   -1  'True
         Width           =   810
      End
   End
   Begin VB.Frame Frame27 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   171
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtEaseOfMovementMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   172
         Text            =   "9"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownEOM2 
         Height          =   285
         Left            =   1680
         TabIndex        =   173
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   9
         BuddyControl    =   "UpDownROC1"
         BuddyDispid     =   196780
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame12 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   168
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtEaseOfMovementLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   169
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownEOM1 
         Height          =   285
         Left            =   1680
         TabIndex        =   170
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "Frame58"
         BuddyDispid     =   196740
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame11 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   178
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtDPOLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   179
         Text            =   "20"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownDPO 
         Height          =   285
         Left            =   1680
         TabIndex        =   180
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "optPVShowMAVYes"
         BuddyDispid     =   196679
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   640
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame59 
      Caption         =   "Antal perioder f�r Rate-of-change"
      Height          =   855
      Left            =   120
      TabIndex        =   185
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtChaikinVolatilityROC 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   0
         Text            =   "10"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownChaikinVolatilityROC 
         Height          =   285
         Left            =   1680
         TabIndex        =   6
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   10
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame58 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   182
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtChaikinVolatility 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   183
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownChaikinVolatility 
         Height          =   285
         Left            =   1680
         TabIndex        =   184
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "Frame38"
         BuddyDispid     =   196677
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   735
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame8 
      Caption         =   "Antal standardavvikelser"
      Height          =   855
      Left            =   120
      TabIndex        =   10
      Top             =   960
      Width           =   2775
      Begin VB.TextBox tbBollingerSTD 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   17
         Text            =   "frmParameters.frx":0331
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   27
      Top             =   0
      Width           =   2775
      Begin VB.TextBox tbBollingerMAV 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         MultiLine       =   -1  'True
         TabIndex        =   49
         Text            =   "frmParameters.frx":0333
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownBOL 
         Height          =   285
         Left            =   1680
         TabIndex        =   59
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "Frame36"
         BuddyDispid     =   196680
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame60 
      Caption         =   "Antal perioder f�r medelv�rdet"
      Height          =   855
      Left            =   120
      TabIndex        =   78
      Top             =   960
      Width           =   2775
      Begin VB.TextBox txtVolatilityMAVLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   88
         Text            =   "10"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownVolatilityMAVLength 
         Height          =   285
         Left            =   1680
         TabIndex        =   98
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   5
         BuddyControl    =   "UpDownPriceOsc2"
         BuddyDispid     =   196782
         OrigLeft        =   1680
         OrigTop         =   360
         OrigRight       =   1875
         OrigBottom      =   615
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame61 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   105
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtVolatility 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   108
         Text            =   "10"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownVolatility 
         Height          =   285
         Left            =   1680
         TabIndex        =   115
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   20
         BuddyControl    =   "Frame59"
         BuddyDispid     =   196738
         OrigLeft        =   1920
         OrigTop         =   360
         OrigRight       =   2115
         OrigBottom      =   615
         Max             =   500
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
   Begin VB.Frame Frame62 
      Caption         =   "Visa medelv�rdet i grafen"
      Height          =   855
      Left            =   120
      TabIndex        =   128
      Top             =   1920
      Width           =   2775
      Begin VB.OptionButton OptionVolatilityShowMAVYes 
         Caption         =   "Ja"
         Height          =   255
         Left            =   1080
         TabIndex        =   147
         Top             =   240
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.OptionButton OptionVolatilityShowMAVNo 
         Caption         =   "Nej"
         Height          =   255
         Left            =   1080
         TabIndex        =   163
         Top             =   480
         Width           =   930
      End
   End
   Begin VB.Frame Frame10 
      Caption         =   "Antal perioder"
      Height          =   855
      Left            =   120
      TabIndex        =   167
      Top             =   0
      Width           =   2775
      Begin VB.TextBox txtAverageTrueRangeLength 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   720
         TabIndex        =   177
         Text            =   "14"
         Top             =   360
         Width           =   975
      End
      Begin MSComCtl2.UpDown UpDownATR 
         Height          =   285
         Left            =   1680
         TabIndex        =   181
         Top             =   360
         Width           =   195
         _ExtentX        =   423
         _ExtentY        =   503
         _Version        =   393216
         Value           =   14
         BuddyControl    =   "Option_PriceOsc_Exponential"
         BuddyDispid     =   196671
         OrigLeft        =   1800
         OrigTop         =   360
         OrigRight       =   1995
         OrigBottom      =   645
         Max             =   200
         Min             =   2
         SyncBuddy       =   -1  'True
         BuddyProperty   =   0
         Enabled         =   -1  'True
      End
   End
End
Attribute VB_Name = "frmParameters"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdCancel_Click()
  Unload frmParameters
End Sub

Private Sub cmdOk_Click()
  Dim Errorfound As Boolean
  ErrorCheckLengthOfParameters frmParameters, Errorfound
  If Errorfound Then
    Exit Sub
  End If
  UpdateParameterSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
  Unload frmParameters
End Sub

Private Sub cmdApply_Click()
Dim Errorfound As Boolean
  ErrorCheckLengthOfParameters frmParameters, Errorfound
  If Errorfound Then
    Exit Sub
  End If
  UpdateParameterSettings
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph ActiveIndex
  End If
End Sub

Private Sub cmdSave_Click()
  Dim Errorfound As Boolean
  Dim Answer As Long
  ErrorCheckLengthOfParameters frmParameters, Errorfound
    If Errorfound Then
      Exit Sub
    End If
  Answer = YesNoCancel("Vill du spara parametrarna som allm�nna?", "Spara?")
  Select Case Answer
  Case 6
    UpdateParameterSettings
    SaveParameterSettingsToRegistry
    MsgBox CheckLang("Parametrarna �r sparade som allm�nna")
  End Select
  If Answer <> 2 Then
    Answer = YesNoCancel("Vill du spara parametrarna som objektberoende?", "Spara?")
  Select Case Answer
  Case 6
    UpdateParameterSettings
    SaveParameterSettingsToInvest ActiveIndex
    WriteWorkspaceSettings
    MsgBox CheckLang("Parametrarna �r sparade som objektberoende")
  End Select
  End If
End Sub

Private Sub Form_Load()
  top = (MainMDI.Height - Height - 2000) / 2
  left = (MainMDI.Width - Width) / 2
  InitParameters
  If UseGeneralIndicatorParameters Then
    Caption = CheckLang("Indikatorparametrar (Allm�nna)")
  Else
    Caption = CheckLang("Indikatorparametrar (Objektberoende)")
  End If
  SendKeys vbKeyReturn
End Sub

Private Sub Para_MAV_Period1_GotFocus()
  TextSelected
End Sub

Private Sub Para_MAV_Period1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Para_MAV_Period1, UpDownParaMAVPeriod1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Para_MAV_Period2_GotFocus()
  TextSelected
End Sub

Private Sub Para_MAV_Period2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Para_MAV_Period1, UpDownParaMAVPeriod2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_MOMLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_MOMLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_MOMLength, UpDownMOM1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_MOMMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_MOMMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_MOMMAVLength, UpDownMOM2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_PriceOsc_MAV1_GotFocus()
  TextSelected
End Sub

Private Sub Tb_PriceOsc_MAV1_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_PriceOsc_MAV1, UpDownPriceOsc1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_PriceOsc_MAV2_GotFocus()
  TextSelected
End Sub

Private Sub Tb_PriceOsc_MAV2_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_PriceOsc_MAV2, UpDownPriceOsc2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_RSILength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_RSILength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_RSILength, UpDownRSI1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_RSIMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub Tb_RSIMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_RSIMAVLength, UpDownRSI2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_StoOscPeriodD_GotFocus()
  TextSelected
End Sub

Private Sub Tb_StoOscPeriodD_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_StoOscPeriodD, UpDownSTO3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Tb_StoOscPeriodK_GotFocus()
  TextSelected
End Sub

Private Sub Tb_StoOscPeriodK_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara Tb_StoOscPeriodK, UpDownSTO1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbAccelerationfactor_GotFocus()
  TextSelected
End Sub

Private Sub tbAccelerationfactor_Validate(Cancel As Boolean)
  Dim OK As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySingleInput OK, tbAccelerationfactor
    
  If Not OK Then
    Cancel = True
  Else
    If CSng(PointToComma(tbAccelerationfactor)) < 0.02 Or CSng(PointToComma(tbAccelerationfactor)) > 0.2 Then
      Min = "0,02"
      Max = "0,2"
      Message = "Du har angivit ett f�r stort eller f�r litet v�rde. Min = " + Str$(Min) + ", Max = " + Str$(Max) + ". T�nk p� att en del datorer anv�nder . som decimaltalsavskiljare."
      MsgBox CheckLang(Message), vbOKOnly, CheckLang("Felmeddelande")
      Cancel = True
    Else
      Cancel = False
    End If
  End If
End Sub

Private Sub tbBollingerMAV_GotFocus()
  TextSelected
End Sub

Private Sub tbBollingerMAV_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbBollingerMAV, UpDownBOL, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbBollingerSTD_GotFocus()
  TextSelected
End Sub

Private Sub tbBollingerSTD_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  Dim Message As String
  Dim Min As Single
  Dim Max As Single
  
  VerifySinglePara tbBollingerSTD, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    If CSng(PointToComma(tbBollingerSTD)) < 0.1 Or CSng(PointToComma(tbBollingerSTD)) > 5 Then
      Min = "0,1"
      Max = "5"
      Message = "Du har angivit ett f�r stort eller f�r litet v�rde. Min = " + Str$(Min) + ", Max = " + Str$(Max) + ". T�nk p� att en del datorer anv�nder . som decimaltalsavskiljare."
      MsgBox CheckLang(Message), vbOKOnly, CheckLang("Felmeddelande")
      Cancel = True
    Else
      Cancel = False
    End If
  End If

End Sub

Private Sub tbMFILength_GotFocus()
  TextSelected
End Sub

Private Sub tbMFILength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbMFILength, UpDownMFI1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub tbMFIMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub tbMFIMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara tbMFIMAVLength, UpDownMFI2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub Text1_Change()

End Sub

Private Sub txtAverageTrueRangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtAverageTrueRangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtAverageTrueRangeLength, UpDownATR, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtChaikinVolatility_GotFocus()
  TextSelected
End Sub

Private Sub txtChaikinVolatility_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtChaikinVolatility, UpDownChaikinVolatility, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtChaikinVolatilityROC_GotFocus()
  TextSelected
End Sub

Private Sub txtChaikinVolatilityROC_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtChaikinVolatilityROC, UpDownChaikinVolatilityROC, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtDPOLength_GotFocus()
  TextSelected
End Sub

Private Sub txtDPOLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtDPOLength, UpDownDPO, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtEaseOfMovementLength_GotFocus()
  TextSelected
End Sub

Private Sub txtEaseOfMovementLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtEaseOfMovementLength, UpDownEOM1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtEaseOfMovementMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtEaseOfMovementMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtEaseOfMovementMAVLength, UpDownEOM2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtMFILowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtMFILowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtMFILowerSignalLevel, UpDownMFI3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtMFIUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtMFIUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtMFIUpperSignalLevel, UpDownMFI4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtNegativeVolumeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtNegativeVolumeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtNegativeVolumeMAVLength, UpDownNV, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtPositiveVolumeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtPositiveVolumeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtPositiveVolumeMAVLength, UpDownPV, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRateOfChangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtRateOfChangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRateOfChangeLength, UpDownROC1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRateOfChangeMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtRateOfChangeMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRateOfChangeMAVLength, UpDownROC2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRSILowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtRSILowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRSILowerSignalLevel, UpDownRSI3, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtRSIUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtRSIUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtRSIUpperSignalLevel, UpDownRSI4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtStandardDeviationLength_GotFocus()
  TextSelected
End Sub

Private Sub txtStandardDeviationLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtStandardDeviationLength, UpDownSTD1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtStandardDeviationMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtStandardDeviationMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtStandardDeviationMAVLength, UpDownSTD2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOLowerSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOLowerSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOLowerSignalLevel, UpDownSTO4, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOSlowing_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOSlowing_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOSlowing, UpDownSTO2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtSTOUpperSignalLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtSTOUpperSignalLevel_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtSTOUpperSignalLevel, UpDownSTO5, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtTRIXLength_GotFocus()
  TextSelected
End Sub

Private Sub txtTRIXLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtTRIXLength, UpDownTRIX1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtTRIXMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtTRIXMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtTRIXMAVLength, UpDownTRIX2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolatility_GotFocus()
  TextSelected
End Sub

Private Sub txtVolatility_Validate(Cancel As Boolean)
Dim Errorfound As Boolean
  VerifyLongPara txtVolatility, UpDownVolatility, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolatilityMAVLength_GotFocus()
  TextSelected
End Sub

Private Sub txtVolatilityMAVLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolatilityMAVLength, UpDownVolatilityMAVLength, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeOscillatorMAV1Length_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeOscillatorMAV1Length_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeOscillatorMAV1Length, UpDownVO1, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeOscillatorMAV2Length_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeOscillatorMAV2Length_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeOscillatorMAV2Length, UpDownVO2, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub

Private Sub txtVolumeRateOfChangeLength_GotFocus()
  TextSelected
End Sub

Private Sub txtVolumeRateOfChangeLength_Validate(Cancel As Boolean)
  Dim Errorfound As Boolean
  VerifyLongPara txtVolumeRateOfChangeLength, UpDownVROC, Errorfound
  If Errorfound Then
    Cancel = True
  Else
    Cancel = False
  End If
End Sub



