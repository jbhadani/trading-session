Attribute VB_Name = "DLLDeclares"
Option Explicit

' Update process
Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Declare Function OpenProcess Lib "kernel32" (ByVal dwDesiredAccess As _
    Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long

Declare Function GetExitCodeProcess Lib "kernel32" (ByVal hProcess As _
    Long, lpExitCode As Long) As Long

' Help file
Declare Function WinHelp Lib "user32" Alias "WinHelpA" (ByVal hWnd As Long, ByVal lpHelpFile As String, ByVal _
wCommand As Long, ByVal dwData As Long) As Long

' FTP functions
Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal sAgent As String, _
ByVal lAccessType As Long, ByVal sProxyName As String, ByVal sProxyBypass As String, ByVal _
lFlags As Long) As Long

Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInet As Long) As Integer

Declare Function InternetReadFile Lib "wininet.dll" (ByVal hFile As Long, ByVal sBuffer As String, _
ByVal lNumBytesToRead As Long, lNumberOfBytesRead As Long) As Integer

Declare Function InternetOpenUrl Lib "wininet.dll" Alias "InternetOpenUrlA" (ByVal hInternetSession As _
Long, ByVal lpszUrl As String, ByVal lpszHeaders As String, ByVal dwHeadersLength As Long, _
ByVal dwFlags As Long, ByVal dwContext As Long) As Long

#If DBUG Then
  
  'TS Library DLL
  Declare Function Get2LastDays Lib "tslib.dll" Alias "_G2L@12" (ByVal FileName As String, LastPriceData As PriceDataType, SecondLastPriceData As PriceDataType) As Long
  Declare Sub DeCrypt Lib "tslib.dll" Alias "_FastDateConv@12" (PriceData As PriceDataType, ByVal Start As Long, ByVal Size As Long)
  
  'TS GFX Library DLL
  Declare Sub InitStockLabels Lib "tsgfxlib.dll" (ByVal FileName As String)
  Declare Sub SetStockLabelsScaling Lib "tsgfxlib.dll" _
             (ByVal StockHandle As Long, _
              ByRef DrawArray As PriceDataType, _
              ByVal DrawArraySize As Integer, _
              ByRef PriceGraphCopy As GraphCopyType)
  Declare Function AddStockLabel Lib "tsgfxlib.dll" _
                   (ByVal hdc As Long, _
                    ByVal StockHandle As Long, _
                    ByVal dtDate As Date, _
                    ByVal Price As Single, _
                    ByVal Text As String, _
                    ByVal Color As Long) As Long
  Declare Function DeleteStockLabel Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long) As Boolean
  Declare Function GetStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal X As Long, ByVal Y As Long) As Long
  Declare Sub HideStockLabels Lib "tsgfxlib.dll" (ByVal StockHandle As Long)
  Declare Function ShowStockLabels Lib "tsgfxlib.dll" (ByVal StockName As String) As Long
  Declare Sub MoveStockLabel Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long, ByVal dtDate As Date, ByVal Price As Single)
  Declare Sub RefreshStockLabels Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long)
  Declare Sub DrawStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long, ByVal X As Long, ByVal Y As Long)
  'Declare Sub EraseStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long, ByVal X As Long, ByVal Y As Long)
  Declare Sub GetStockLabelRect Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long, DestRect As RECT)
  Declare Function EnumStockLabels Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long) As Long
  Declare Sub GetStockLabelInfo Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long, ByRef dtDate As Date, ByRef Price As Single, ByRef Text As Variant)

#Else
   
  'TS Library DLL
  Declare Function Get2LastDays Lib "tslib.dll" Alias "_G2L@12" (ByVal FileName As String, LastPriceData As PriceDataType, SecondLastPriceData As PriceDataType) As Long
  Declare Sub DeCrypt Lib "tslib.dll" Alias "_FastDateConv@12" (PriceData As PriceDataType, ByVal Start As Long, ByVal Size As Long)
  
  'TS GFX Library DLL
  Declare Sub InitStockLabels Lib "tsgfxlib.dll" (ByVal FileName As String)
  Declare Sub SetStockLabelsScaling Lib "tsgfxlib.dll" _
             (ByVal StockHandle As Long, _
              ByRef DrawArray As PriceDataType, _
              ByVal DrawArraySize As Integer, _
              ByRef PriceGraphCopy As GraphCopyType)
  Declare Function AddStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal dtDate As Date, ByVal Price As Single, ByVal Text As String, ByVal Color As Long) As Long
  Declare Function DeleteStockLabel Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long) As Boolean
   Declare Function GetStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal X As Long, ByVal Y As Long) As Long
  Declare Sub HideStockLabels Lib "tsgfxlib.dll" (ByVal StockHandle As Long)
  Declare Function ShowStockLabels Lib "tsgfxlib.dll" (ByVal StockName As String) As Long
  Declare Sub MoveStockLabel Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long, ByVal dtDate As Date, ByVal Price As Single)
  Declare Sub RefreshStockLabels Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long)
  Declare Sub DrawStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long, ByVal X As Long, ByVal Y As Long)
  'Declare Sub EraseStockLabel Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long)
  Declare Sub GetStockLabelRect Lib "tsgfxlib.dll" (ByVal hdc As Long, ByVal StockHandle As Long, ByVal LabelHandle As Long, DestRect As RECT)
  Declare Function EnumStockLabels Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long) As Long
  Declare Sub GetStockLabelInfo Lib "tsgfxlib.dll" (ByVal StockHandle As Long, ByVal LabelHandle As Long, ByRef dtDate As Date, ByRef Price As Single, ByRef Text As Variant)

#End If

Declare Function InvalidateRect Lib "user32.dll" (ByVal hWnd As Long, lpRect As Any, ByVal bErase As Boolean) As Boolean

Declare Function ShellAbout Lib "shell32.dll" Alias "ShellAboutA" (ByVal hWnd As Long, _
                                                                           ByVal szApp As String, _
                                                                           ByVal szOtherStuff As String, _
                                                                           ByVal hIcon As Long) As Long
Declare Function GetMenu Lib "user32" _
   (ByVal hWnd As Long) As Long

Declare Function GetSubMenu Lib "user32" _
   (ByVal hMenu As Long, ByVal nPos As Long) As Long

Declare Function SetMenuItemBitmaps Lib "user32" _
   (ByVal hMenu As Long, ByVal nPosition As Long, ByVal wFlags As Long, _
    ByVal hBitmapUnchecked As Long, ByVal hBitmapChecked As Long) As Long

Public Const MF_BYPOSITION = &H400&

Declare Function StretchBlt Lib "gdi32" (ByVal hdc As Long, ByVal X As Long, ByVal Y As Long, ByVal nWidth As Long, ByVal nHeight As Long, ByVal hSrcDC As Long, ByVal xSrc As Long, ByVal ySrc As Long, ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, ByVal dwRop As Long) As Long

Declare Function ShowWindow Lib "user32" (ByVal hWnd As Long, ByVal nCmdShow As Long) As Long

  Public Const SWP_NOMOVE = 2
  Public Const SWP_NOSIZE = 1
  Public Const FLAGS = SWP_NOMOVE Or SWP_NOSIZE
  Public Const HWND_TOPMOST = -1
  Public Const HWND_NOTOPMOST = -2

 Private Declare Function SetWindowPos Lib "user32" _
        (ByVal hWnd As Long, _
        ByVal hWndInsertAfter As Long, _
        ByVal X As Long, _
        ByVal Y As Long, _
        ByVal cx As Long, _
        ByVal cy As Long, _
        ByVal wFlags As Long) As Long

  Public Function SetTopMostWindow(hWnd As Long, Topmost As Boolean) _
     As Long

     If Topmost = True Then 'Make the window topmost
        SetTopMostWindow = SetWindowPos(hWnd, HWND_TOPMOST, 0, 0, 0, _
           0, FLAGS)
     Else
        SetTopMostWindow = SetWindowPos(hWnd, HWND_NOTOPMOST, 0, 0, _
           0, 0, FLAGS)
        SetTopMostWindow = False
     End If
  End Function


