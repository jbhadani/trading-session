Attribute VB_Name = "PriceDataOPerations"
Option Explicit
''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          AddPriceData
'INPUT:         PriceDataType
'OUTPUT:
'LAST UPDATE:   970323
'Adds a PriceData record to PriceDataArray
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''

'Sub AddPriceData(PriceData As PriceDataType, Added As Boolean)
'  Dim TempPriceDataArray() As PriceDataType
'  Dim i As long
'  Dim index As long
'  ReDim TempPriceDataArray(PriceDataArraySize)
'  Added = False
'  If PriceData.Date > "1998-06-01" Then
'        MsgBox$ "Testtiden �r �ver"
'        End
'  End If
'  If PriceDataArraySize > 0 Then
'    index = GetPriceDataInsertIndexReverse(PriceData) ''�ndrad till reverse
'  Else
'    ReDim PriceDataArray(1)
'    PriceDataArray(1) = PriceData
'    PriceDataArraySize = 1
'    index = -1
'    Added = True
'  End If
'
'  If (index <= PriceDataArraySize) And (index <> -1) Then
'    If (PriceData.Date = PriceDataArray(index).Date) Then
'      If OverwritePrice = True Then
'        ReplacePriceData PriceData, index
'        index = -1
'        Added = True
'      Else
'        index = -1
'      End If
'    End If
'  End If
'
'    If index > -1 Then
'    For i = 1 To PriceDataArraySize
'    TempPriceDataArray(i) = PriceDataArray(i)
'    Next i
'    PriceDataArraySize = PriceDataArraySize + 1
'    ReDim PriceDataArray(PriceDataArraySize)
'    For i = 1 To index - 1
'      PriceDataArray(i) = TempPriceDataArray(i)
'    Next i
'    PriceDataArray(index) = PriceData
'    For i = index + 1 To PriceDataArraySize
'      PriceDataArray(i) = TempPriceDataArray(i - 1)
'    Next i
'     Added = True
'   End If
'
'End Sub

''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          GetPriceDataInsertIndex
'INPUT:         PriceDataType
'OUTPUT:        Function (GetPriceDataInsertIndex)
'LAST UPDATE:   970323
'Returns the index where the new record will be
'inserted
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''


'Function GetPriceDataInsertIndex(PriceData As PriceDataType) As long
'  Dim i As long
'  Dim Content As String
'  Dim Ready As Boolean
'  i = 1
'  Ready = False
'  Do While Not Ready
'    If PriceDataArray(i).Date >= PriceData.Date Then
'      Ready = True
'    Else
'    i = i + 1
'      If i > PriceDataArraySize Then
'        Ready = True
'      End If
'    End If
'  Loop
'  GetPriceDataInsertIndex = i
'End Function

'Function GetPriceDataInsertIndexReverse(PriceData As PriceDataType) As long
'  Dim i As long
'  Dim Content As String
'  Dim Ready As Boolean
'  i = PriceDataArraySize
'  Ready = False
'  Do While Not Ready
'    If PriceDataArray(i).Date < PriceData.Date Then
'      Ready = True
'    Else
'    i = i - 1
'      If i = 0 Then
'        Ready = True
'      End If
'    End If
'  Loop
'  GetPriceDataInsertIndexReverse = i + 1
'End Function

''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          ReplacePriceData
'INPUT:         PriceDataType, Index
'OUTPUT:
'LAST UPDATE:   970328
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''
'Sub ReplacePriceData(PriceData As PriceDataType, index As long)

'  PriceDataArray(index) = PriceData

'End Sub
'Sub DeletePriceData(index As long)
  
'  Dim TempPriceDataArray() As PriceDataType
'  Dim i As long
'
'  ReDim TempPriceDataArray(PriceDataArraySize)
'  For i = 1 To PriceDataArraySize
'    TempPriceDataArray(i) = PriceDataArray(i)
'  Next i
'  PriceDataArraySize = PriceDataArraySize - 1
'  ReDim PriceDataArray(PriceDataArraySize)
'  For i = 1 To index - 1
'    PriceDataArray(i) = TempPriceDataArray(i)
'  Next i
'  For i = index To PriceDataArraySize
'    PriceDataArray(i) = TempPriceDataArray(i + 1)
'  Next i

'End Sub


Function MakeMonthly(ByRef PDArray() As PriceDataType, ByRef MonthlyArray() As PriceDataType)


Dim EndIndex As Long
Dim I As Long
Dim High As Single
Dim Low As Single
Dim Volume As Single
Dim MonthlySize As Long
EndIndex = UBound(PDArray)

Low = PDArray(1).Low
High = PDArray(1).High
Volume = PDArray(I).Volume

For I = 2 To EndIndex
  If Month(PDArray(I).Date) > Month(PDArray(I - 1).Date) _
     Or Year(PDArray(I).Date) > Year(PDArray(I - 1).Date) Or I = EndIndex Then
    If I = EndIndex Then
      Volume = Volume + PDArray(I).Volume
      If PDArray(I).High > High Then
        High = PDArray(I).High
      End If
      If PDArray(I).Low < Low Then
       Low = PDArray(I).Low
      End If
    End If
    MonthlySize = MonthlySize + 1
    ReDim Preserve MonthlyArray(MonthlySize)
    MonthlyArray(MonthlySize).Date = DateSerial(Year(PDArray(I - 1).Date), Month(PDArray(I - 1).Date), 1)
    MonthlyArray(MonthlySize).Close = PDArray(I - 1).Close
    MonthlyArray(MonthlySize).High = High
    MonthlyArray(MonthlySize).Low = Low
    MonthlyArray(MonthlySize).Volume = Volume
    Volume = 0
    Low = PDArray(I).Low
    High = PDArray(I).High
  End If
  Volume = Volume + PDArray(I).Volume
  If PDArray(I).High > High Then
    High = PDArray(I).High
  End If
  If PDArray(I).Low < Low Then
    Low = PDArray(I).Low
  End If
Next I
End Function

Function MakeWeekely(ByRef PDArray() As PriceDataType, ByRef WeeklyArray() As PriceDataType)


Dim EndIndex As Long
Dim I As Long
Dim High As Single
Dim Low As Single
Dim Volume As Single
Dim WeeklySize As Long
Dim DateOfFirstDay As Date
EndIndex = UBound(PDArray)

Low = PDArray(1).Low
High = PDArray(1).High
Volume = PDArray(I).Volume
DateOfFirstDay = PDArray(1).Date
WeeklySize = 0

For I = 2 To EndIndex
  If Weekday(PDArray(I).Date) < Weekday(PDArray(I - 1).Date) _
      Or Weekday(PDArray(I - 1).Date) = vbSunday Or I = EndIndex Then
    If I = EndIndex Then
      If PDArray(I).High > High Then
        High = PDArray(I).High
      End If
      If PDArray(I).Low < Low Then
        Low = PDArray(I).Low
      End If
      Volume = Volume + PDArray(I).Volume
      Debug.Print CStr(PDArray(I).Date) & ",";
    End If
    WeeklySize = WeeklySize + 1
    ReDim Preserve WeeklyArray(WeeklySize)
    WeeklyArray(WeeklySize).Date = DateOfFirstDay
    WeeklyArray(WeeklySize).Close = PDArray(I - 1).Close
    WeeklyArray(WeeklySize).High = High
    WeeklyArray(WeeklySize).Low = Low
    WeeklyArray(WeeklySize).Volume = Volume
    DateOfFirstDay = PDArray(I).Date
    Volume = 0
    Low = PDArray(I).Low
    High = PDArray(I).High
  End If
  Volume = Volume + PDArray(I).Volume
  If PDArray(I).High > High Then
    High = PDArray(I).High
  End If
  If PDArray(I).Low < Low Then
    Low = PDArray(I).Low
  End If
Next I
End Function
' Get the index correspondig to the last occurence of the date.
' Should also be the only one.
' Returns -1 if the date not is found
Function GetIndex(ByRef PDArray() As PriceDataType, theDate As Date) As Long
  
  Dim I As Long
  Dim IsFound  As Boolean
  IsFound = False
  I = UBound(PDArray)
  Do While Not IsFound And I > 0
    If Int(PDArray(I).Date) = Int(theDate) Then
      IsFound = True
    Else
      I = I - 1
    End If
  Loop
  If IsFound Then
    GetIndex = I
  Else
    GetIndex = -1
  End If
End Function

Function GetGain(ByRef PDArray() As PriceDataType, ByRef NowIndex As Long, GainPeriod As GetGainPeriodEnum, ByRef Status As Boolean) As Single
  
  Dim NowDate As Date
  Dim CompareDate As Date
  Dim I As Long
  Dim IsFound  As Boolean
  
  NowDate = PDArray(NowIndex).Date
  
  Select Case GainPeriod
  Case ggpDay
    CompareDate = DateSerial(Year(NowDate), Month(NowDate), Day(NowDate) - 1)
  Case ggpWeek
    CompareDate = DateSerial(Year(NowDate), Month(NowDate), Day(NowDate) - 7)
  Case ggpMonth
    CompareDate = DateSerial(Year(NowDate), Month(NowDate) - 1, Day(NowDate))
  Case ggpYear
    CompareDate = DateSerial(Year(NowDate) - 1, Month(NowDate), Day(NowDate))
  End Select
  
  
  IsFound = False
  I = UBound(PDArray)
  Do While Not IsFound And I > 0
    If Int(PDArray(I).Date) <= Int(CompareDate) Then
      IsFound = True
    Else
      I = I - 1
    End If
  Loop
  
  If IsFound And PDArray(I).Close <> 0 Then
    GetGain = PDArray(NowIndex).Close / PDArray(I).Close - 1
    Status = True
  Else
    Status = False
  End If
End Function

Sub MakeSecondPriceArray(ByRef OrgArray() As PriceDataType, ByRef secondarray() As PriceDataType, ByRef PercentArray() As Single, StartIndex As Long, EndIndex As Long)
Dim j As Long
ReDim PercentArray(1 To UBound(OrgArray))

PercentArray(1) = OrgArray(StartIndex).Close
For j = StartIndex + 1 To EndIndex
  PercentArray(j - StartIndex + 1) = OrgArray(StartIndex).Close * (secondarray(j).Close / secondarray(j - 1).Close)
Next j
End Sub


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          GetMinAndMax
'Input:         A PriceDataArray.
'Output:        Min,Max as single; PDArray() as PriceDataType
'Description:   This procedure gets the largest and the smallest
'               value in an array.
'Author:        Fredrik Wendel
'Modified:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub GetMinAndMaxFromPriceData(ByRef PDArray() As PriceDataType, _
                        ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
  Dim I As Long
  Max = PDArray(IndexFirst).Low
  Min = PDArray(IndexFirst).High
  For I = IndexFirst + 1 To IndexLast
    With PDArray(I)
    If .High > Max Then
      Max = .High
    End If
    If .Low < Min Then
      Min = .Low
    End If
    End With
  Next I
End Sub
