Attribute VB_Name = "UpdateProc"
Option Explicit
Function GetEndCol(rad As String, StartCol As Integer, Separator) As Integer
  Dim Col As Integer
    Col = StartCol
    Do While Asc(Mid$(rad, Col, 1)) <> Separator
      Col = Col + 1
    Loop
    GetEndCol = Col - 1
End Function
Function GetEndColID(StartCol As Long, Separator) As Long
  Dim Col As Long
    Col = StartCol
    Do While Asc(Mid$(InternetData, Col, 1)) <> Separator
      Col = Col + 1
    Loop
    GetEndColID = Col - 1
End Function

Sub UpdateFromInternet()
 Dim StartCol As Long
  Dim Endcol As Long
  
  Dim PriceData As PriceDataType
  Dim index As Integer
  Dim ActiveIndexBeforeUpdate
  Dim NbrOfUpdatedPrices As Long
  
  Dim Total As Single
  Dim Added As Boolean
  Dim Errorfound As Boolean
  Dim CurrentIndex As Integer
  
  Frm_Progress.Show
  Frm_Progress.PB_1.Value = 0
  Frm_Progress.Caption = "Uppdaterar..."
  NbrOfUpdatedPrices = 0
  
  ActiveIndexBeforeUpdate = ActiveIndex
  CurrentIndex = ActiveIndex
  Total = Len(InternetData)
  Errorfound = False
  StartCol = 1
  On Error Resume Next
  Do While StartCol < Len(InternetData) And Not Errorfound
    Endcol = GetEndColID(StartCol, 59)  '59 is ASCII for ";"
    index = GetInvestIndexForSymbol(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
    Frm_Progress.PB_1.Value = Int(Endcol / Total * 100)
    
    DoEvents
    If Err.Number <> 0 Then
        Errorfound = True
        ErrorMessageStr "Ett fel uppstod under uppdateringen."
    End If
    If index <> -1 Then
      If index <> CurrentIndex Then
        W_PriceData_All (DataPath + Trim(InvestInfoArray(CurrentIndex).FileName) + ".prc")
        CurrentIndex = index
        R_PriceData_All (DataPath + Trim(InvestInfoArray(index).FileName) + ".prc")
      End If
      StartCol = Endcol + 2
      Endcol = GetEndColID(StartCol, 59)
      PriceData.Date = ConvertDate2(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
      DoEvents
      
      StartCol = Endcol + 2
      Endcol = GetEndColID(StartCol, 59)
      PriceData.Close = Val(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
       
       
      StartCol = Endcol + 2
      Endcol = GetEndColID(StartCol, 59)
      PriceData.High = Val(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndColID(StartCol, 59)
      PriceData.Low = Val(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndColID(StartCol, 59)
      PriceData.Volume = Val(Mid$(InternetData, StartCol, Endcol - StartCol + 1))
      
      AddPriceData PriceData, Added
      If Err.Number = 0 Then
        If Added Then
          NbrOfUpdatedPrices = NbrOfUpdatedPrices + 1
          Frm_Progress.Label1.Caption = Str$(NbrOfUpdatedPrices) + " kursdagar �r uppdaterade."
        End If
      Else
        Errorfound = True
        ErrorMessageStr "Ett fel uppstod under uppdateringen."
      End If
    End If
    StartCol = GetEndColID(Endcol, 10) + 2
    
  Loop
  On Error GoTo 0
  Frm_Progress.PB_1.Value = 100
  W_PriceData_All (DataPath + Trim(InvestInfoArray(CurrentIndex).FileName) + ".prc")
  R_PriceData_All (DataPath + Trim(InvestInfoArray(ActiveIndexBeforeUpdate).FileName) + ".prc")
  Unload Frm_Progress
  InformationalMessage Str$(NbrOfUpdatedPrices) + " kursdagar �r uppdaterade."
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph
  End If
  AllowUser 0

End Sub

Sub UpdateFromFile(FileName As String)

  Dim StartCol As Integer
  Dim Endcol As Integer
  Dim rad As String
  Dim PriceData As PriceDataType
  Dim index As Integer
  Dim ActiveIndexBeforeUpdate
  Dim NbrOfUpdatedPrices As Long
  Dim KBRead As Single
  Dim KBTotal As Single
  Dim Added As Boolean
  Dim CurrentIndex As Integer
  Dim Errorfound As Boolean
  
  With Frm_Progress
    .Show
    .PB_1.Value = 0
    .Caption = "Uppdaterar..."
  End With
  NbrOfUpdatedPrices = 0
  CurrentIndex = ActiveIndex
  ActiveIndexBeforeUpdate = ActiveIndex
  Open FileName For Input As #2
  KBTotal = LOF(2) / 1024
  On Error Resume Next
  Errorfound = False

Do While Not EOF(2) And Not Errorfound
    Line Input #2, rad
    
    KBRead = KBRead + Len(rad) / 1024
    Frm_Progress.PB_1.Value = Int(KBRead / KBTotal * 100)
    DoEvents
    
    StartCol = 1
    Endcol = GetEndCol(rad, StartCol, 59)
    
    index = GetInvestIndexForSymbol(Mid$(rad, StartCol, Endcol - StartCol + 1))
    If index <> -1 Then
      
    
    
      If index <> CurrentIndex Then
        W_PriceData_All (DataPath + Trim(InvestInfoArray(CurrentIndex).FileName) + ".prc")
        R_PriceData_All (DataPath + Trim(InvestInfoArray(index).FileName) + ".prc")
        CurrentIndex = index
      End If
      StartCol = Endcol + 2
      Endcol = GetEndCol(rad, StartCol, 59)
      PriceData.Date = ConvertDate2(Mid$(rad, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndCol(rad, StartCol, 59)
      PriceData.Close = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndCol(rad, StartCol, 59)
      PriceData.High = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndCol(rad, StartCol, 59)
      PriceData.Low = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
      
      StartCol = Endcol + 2
      Endcol = GetEndCol(rad, StartCol, 59)
      PriceData.Volume = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
      
      AddPriceData PriceData, Added 'Add PriceData to PriceDataArray
      If Err.Number = 0 Then
        If Added Then
          NbrOfUpdatedPrices = NbrOfUpdatedPrices + 1
          Frm_Progress.Label1.Caption = Str$(NbrOfUpdatedPrices) + " kursdagar �r uppdaterade"
        End If
      Else
        Errorfound = True
        ErrorMessageStr "Ett fel uppstod under uppdateringen."
      End If
  
    End If
  Loop
  On Error GoTo 0
  Close #2
  Frm_Progress.PB_1.Value = 100
  W_PriceData_All (DataPath + Trim(InvestInfoArray(CurrentIndex).FileName) + ".prc")
  R_PriceData_All (DataPath + Trim(InvestInfoArray(ActiveIndexBeforeUpdate).FileName) + ".prc")
  Unload Frm_Progress
  InformationalMessage Str$(NbrOfUpdatedPrices) + " kursdagar �r uppdaterade"
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph
  End If
  AllowUser 0

End Sub


Sub StartTransfer()
  Frm_Status.Show
  Frm_Status.Label1.Caption = "Connecting to host"
  InternetData = ""
  With Frm_Status.Winsock1
    If Not UseProxy Then
      .RemoteHost = "hem2.passagen.se"
      .RemotePort = 80
    Else
      .RemoteHost = ProxyServer
      .RemotePort = ProxyPort
    End If
    .Connect
  End With
End Sub



Sub UpdateFromMS(FileName As String)
  Dim StartCol As Integer
  Dim Endcol As Integer
  Dim rad As String
  Dim PriceData As PriceDataType
  Dim KBRead As Single
  Dim KBTotal As Single
  Dim NbrOfUpdatedPrices As Integer
  Dim Added As Boolean
  NbrOfUpdatedPrices = 0
  MainMDI.Picture1.Enabled = False
  Frm_Progress.Show
  Frm_Progress.PB_1.Value = 0
  Frm_Progress.Caption = "Uppdaterar..."
  Open FileName For Input As #1
  KBTotal = LOF(1) / 1024
  KBRead = 0
  On Error Resume Next
  Do While Not EOF(1)
    Line Input #1, rad
    KBRead = KBRead + Len(rad) / 1024
    Frm_Progress.PB_1.Value = Int(KBRead / KBTotal * 100)
    DoEvents
    StartCol = 1
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Date = ConvertDateFromMeta(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.High = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Low = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Close = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    'Endcol = GetEndCol(rad, StartCol, 9)
    rad = Trim(Mid(rad, StartCol, 20))
    PriceData.Volume = Val(rad)
    
    
    If Err.Number = 0 Then
      AddPriceData PriceData, Added
      If Added Then
        NbrOfUpdatedPrices = NbrOfUpdatedPrices + 1
        Frm_Progress.Label1.Caption = Str$(NbrOfUpdatedPrices) + " prices are updated"
      End If
    Else
      MsgBox "Error found in imported file. Erroneous row ignored."
      
      Err.Clear
    End If
  Loop
  On Error GoTo 0
  Frm_Progress.PB_1.Value = 100
  Close #1
  Unload Frm_Progress
  W_PriceData_All (DataPath + Trim(InvestInfoArray(ActiveIndex).FileName) + ".prc")
  InformationalMessage Str$(NbrOfUpdatedPrices) + " prices are updated"
  If ShowGraph Then
    GraphInfoChanged = True
    UpdateGraph
  End If
  MainMDI.Picture1.Enabled = True

End Sub


Sub UpdateFromFrost(FileName As String)
  Dim StartCol As Integer
  Dim Endcol As Integer
  Dim rad As String
  Dim PriceData As PriceDataType
  Dim KBRead As Single
  Dim KBTotal As Single
  Dim NbrOfUpdatedPrices As Integer
  Dim Added As Boolean
  NbrOfUpdatedPrices = 0
  MainMDI.Picture1.Enabled = False
  Frm_Progress.Show
  Frm_Progress.PB_1.Value = 0
  Frm_Progress.Caption = "Uppdaterar..."
  Open FileName For Input As #1
  KBTotal = LOF(1) / 1024
  KBRead = 0
  On Error Resume Next
  Do While Not EOF(1)
    Line Input #1, rad
  
    KBRead = KBRead + Len(rad) / 1024
    Frm_Progress.PB_1.Value = Int(KBRead / KBTotal * 100)
    DoEvents
    StartCol = 1
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Date = ConvertDate(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.High = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Low = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Close = Val(Mid$(rad, StartCol, Endcol - StartCol + 1))
    
    StartCol = Endcol + 2
    Endcol = GetEndCol(rad, StartCol, 9)
    PriceData.Volume = Mid$(rad, StartCol, Endcol - StartCol + 1)
    
    
    If Err.Number = 0 Then
      AddPriceData PriceData, Added
      If Added Then
        NbrOfUpdatedPrices = NbrOfUpdatedPrices + 1
        Frm_Progress.Label1.Caption = Str$(NbrOfUpdatedPrices) + " prices are updated"
      End If
    Else
      MsgBox "Error found in imported file. Erroneous row ignored."
      
      Err.Clear
    End If
  Loop
  On Error GoTo 0
  Frm_Progress.PB_1.Value = 100
  Close #1
  Unload Frm_Progress
  W_PriceData_All (DataPath + Trim(InvestInfoArray(ActiveIndex).FileName) + ".prc")
  InformationalMessage Str$(NbrOfUpdatedPrices) + " prices are updated"
  If ShowGraph Then
    GraphInfoChanged = True
    UpdateGraph
  End If
  MainMDI.Picture1.Enabled = True

End Sub
