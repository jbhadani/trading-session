Attribute VB_Name = "InvestInfoOperations"

'Sub AddNewInvestInfo(InvestInfo As InvestInfoType)

'Dim i As long
'Dim j As long
'Dim Found As long
'Dim TempInvestInfoArray() As InvestInfoType

'  i = 1
'  Found = False
'  Do While (i <= InvestInfoArraySize) And Not Found
'    If InvestInfo.Name < InvestInfoArray(i).Name Then
'      Found = True
'    Else
'      i = i + 1
'    End If
'  Loop
'  ReDim TempInvestInfoArray(InvestInfoArraySize)
'  For j = 1 To InvestInfoArraySize
'    TempInvestInfoArray(j) = InvestInfoArray(j)
'  Next j
'  ReDim InvestInfoArray(InvestInfoArraySize + 1)

'  For j = 1 To (i - 1)
'    InvestInfoArray(j) = TempInvestInfoArray(j)
'  Next j
'    InvestInfoArray(i) = InvestInfo
'  For j = i To InvestInfoArraySize
'    InvestInfoArray(j + 1) = TempInvestInfoArray(j)
'  Next j
'  InvestInfoArraySize = InvestInfoArraySize + 1

'  If ActiveIndex >= i Then ActiveIndex = ActiveIndex + 1

'End Sub
'Sub DeleteInvestInfo(index As long)
'
'Dim i As long
'Dim TempInvestInfoArray() As InvestInfoType
'
'  ReDim TempInvestInfoArray(InvestInfoArraySize)
'  For i = 1 To InvestInfoArraySize
'    TempInvestInfoArray(i) = InvestInfoArray(i)
'  Next i
'  InvestInfoArraySize = InvestInfoArraySize - 1
'  ReDim InvestInfoArray(InvestInfoArraySize)
'  For i = 1 To index - 1
'    InvestInfoArray(i) = TempInvestInfoArray(i)
'  Next i
'  For i = index To InvestInfoArraySize
'    InvestInfoArray(i) = TempInvestInfoArray(i + 1)
'  Next i
'
'  If ActiveIndex >= index Then ActiveIndex = ActiveIndex - 1
'
'End Sub


''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          ExistInInvestInfoArray
'INPUT:         InvestInfoType
'OUTPUT:        Function (ExistInInvestInfoArray)
'LAST UPDATE:
'Returns true if InvestInfo can be found in InvestInfoArray
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''

Function ExistInInvestInfoArray(InvestInfo As InvestInfoType) As Boolean
  
Dim i As Long

  ExistInInvestInfoArray = False
  For i = 1 To InvestInfoArraySize
    If InvestInfoArray(i).Name = InvestInfo.Name Then
      ExistInInvestInfoArray = True
    End If
  Next i

End Function

'GetInvestIndex
'Returns -1 if Name can't be found in InvestInfoArray.Name.
'Returns the index if PortfolioName can be found in InvestInfoArray
Function GetInvestIndex(Name As String) As Long

Dim i As Long
  
  GetInvestIndex = -1
  For i = 1 To InvestInfoArraySize
    If Trim(InvestInfoArray(i).Name) = Trim(Name) Then
      GetInvestIndex = i
    End If
  Next i

End Function

'GetInvestIndexForSymbol
'Returns -1 if Symbol can't be found in InvestInfoArray.Name.
'Returns the index if PortfolioName can be found in InvestInfoArray
Function GetInvestIndexForSymbol(Symbol As String) As Long
  
  Dim i As Long
  
  GetInvestIndexForSymbol = -1
  For i = 1 To InvestInfoArraySize
    'If (Trim(InvestInfoArray(i).Symbol1) = Trim(Symbol)) Or (Trim(InvestInfoArray(i).Symbol2) = Trim(Symbol)) Then
    '  GetInvestIndexForSymbol = i
    'End If
  Next i

End Function
