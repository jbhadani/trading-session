VERSION 5.00
Begin VB.Form frmGraphSettings 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Diagraminställningar"
   ClientHeight    =   3855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7605
   Icon            =   "frmGraphSettings.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   7605
   Begin VB.CheckBox chkShowZero 
      Caption         =   "Visa nollkurser"
      Height          =   255
      Left            =   5280
      TabIndex        =   20
      Top             =   1800
      Width           =   1935
   End
   Begin VB.Frame Frame5 
      Caption         =   "Nollkurser"
      Height          =   615
      Left            =   5040
      TabIndex        =   23
      Top             =   1620
      Width           =   2535
   End
   Begin VB.CheckBox chExponentialPrice 
      Caption         =   "Använd logaritmisk skala"
      Height          =   255
      Left            =   5280
      TabIndex        =   21
      Top             =   1200
      Width           =   2055
   End
   Begin VB.Frame Frame1 
      Caption         =   "Linjär - Logaritmisk skala"
      Height          =   615
      Left            =   5040
      TabIndex        =   22
      Top             =   960
      Width           =   2535
   End
   Begin VB.Frame Frame4 
      Caption         =   "Visa"
      Height          =   2115
      Left            =   0
      TabIndex        =   16
      Top             =   900
      Width           =   2055
      Begin VB.CheckBox chShowPriceMAV1 
         Caption         =   "MAV1"
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   240
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox chShowIndicator1 
         Caption         =   "Visa indikator 1"
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   1240
         Width           =   1635
      End
      Begin VB.CheckBox chShowIndicator2 
         Caption         =   "Visa indikator 2"
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   1490
         Width           =   1755
      End
      Begin VB.CheckBox chkShowVolume 
         Caption         =   "Visa omsättning"
         Height          =   255
         Left            =   240
         TabIndex        =   24
         Top             =   1740
         Width           =   1575
      End
      Begin VB.CheckBox chShowPriceMAV2 
         Caption         =   "MAV2"
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   490
         Value           =   1  'Checked
         Width           =   1095
      End
      Begin VB.CheckBox chShowParabolic 
         Caption         =   "Parabolic"
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   990
         Width           =   1095
      End
      Begin VB.CheckBox chShowBollinger 
         Caption         =   "Bollinger"
         Height          =   255
         Left            =   240
         TabIndex        =   17
         Top             =   740
         Width           =   1095
      End
   End
   Begin VB.ComboBox cDayWidth 
      Height          =   315
      Left            =   5520
      Style           =   2  'Dropdown List
      TabIndex        =   13
      Top             =   360
      Width           =   1575
   End
   Begin VB.Frame Frame3 
      Caption         =   "Diagramskala"
      Height          =   855
      Left            =   5040
      TabIndex        =   14
      Top             =   0
      Width           =   2535
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Index           =   1
      Left            =   0
      TabIndex        =   9
      Top             =   3060
      Width           =   7575
      Begin VB.CommandButton cbApply 
         Caption         =   "Verkställ"
         Height          =   375
         Index           =   0
         Left            =   6240
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cbOk 
         Caption         =   "OK"
         Height          =   375
         Index           =   0
         Left            =   480
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         Height          =   375
         Index           =   0
         Left            =   3360
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame8 
      Caption         =   "Färgsignalering"
      Height          =   1815
      Left            =   2160
      TabIndex        =   6
      Top             =   960
      Width           =   2775
      Begin VB.CheckBox chVerticalLines 
         Caption         =   "Visa vertikala färgsignaler"
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1440
         Width           =   2175
      End
      Begin VB.ComboBox cSignalIndicator 
         Enabled         =   0   'False
         Height          =   315
         ItemData        =   "frmGraphSettings.frx":014A
         Left            =   240
         List            =   "frmGraphSettings.frx":014C
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   840
         Width           =   2055
      End
      Begin VB.CheckBox chSignalColor 
         Caption         =   "Använd färgsignalering"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Frame7 
      Caption         =   "Rutnät"
      Height          =   855
      Left            =   2160
      TabIndex        =   3
      Top             =   0
      Width           =   2775
      Begin VB.CheckBox chShowXGrid 
         Caption         =   "Horisontellt"
         Height          =   255
         Left            =   240
         TabIndex        =   5
         Top             =   240
         Value           =   1  'Checked
         Width           =   1215
      End
      Begin VB.CheckBox chShowYGrid 
         Caption         =   "Vertikalt"
         Height          =   255
         Left            =   240
         TabIndex        =   4
         Top             =   480
         Value           =   1  'Checked
         Width           =   1095
      End
   End
   Begin VB.Frame Frame6 
      Caption         =   "Diagramtyp"
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2055
      Begin VB.OptionButton rbShowHLC 
         Caption         =   "High-Low-Close"
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   1575
      End
      Begin VB.OptionButton rbShowLine 
         Caption         =   "Linje"
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmGraphSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Sub SaveGraphSettingsToRegistry()
  With GraphSettings
  SaveSetting App.Title, "GraphSettings", "DayWidth", CStr(.DayWidth)
  
  If .ShowHLC Then
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowHLC", "False"
  End If
  If .ShowZero Then
    SaveSetting App.Title, "GraphSettings", "ShowZero", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowZero", "False"
  End If
    
  If .ShowXGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowXGrid", "False"
  End If
  If .ShowYGrid Then
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowYGrid", "False"
  End If
  If .ShowPriceMAV1 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV1", "False"
  End If
  If .ShowPriceMAV2 Then
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowPriceMAV2", "False"
  End If
  'SaveSetting App.Title, "GraphSettings", "PriceMAV1Color", cstr(.PriceMAV1Color)
  'SaveSetting App.Title, "GraphSettings", "PriceMAV2Color", cstr(.PriceMAV2Color)
  If .ShowIndicator1 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator1", "False"
  End If
  If .ShowIndicator2 Then
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowIndicator2", "False"
  End If
  If .ShowSignalColor Then
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowSignalColor", "False"
  End If
  If .ExponentialPrice Then
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ExponentialPrice", "False"
  End If
  If .ShowVerticalColorLines Then
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "VerticalColorLines", "False"
  End If
  
  'Bollinger
  If .ShowBollinger Then
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowBollinger", "False"
  End If
  
  'Parabolic
  If .ShowParabolic Then
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowParabolic", "False"
  End If
  If .ShowVolume Then
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "True"
  Else
    SaveSetting App.Title, "GraphSettings", "ShowVolume", "False"
  End If
  
  End With
  
End Sub



Private Sub cbApply_Click(Index As Integer)
  'UpdateGraphSettings
  SaveGraphSettingsToRegistry
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph
  End If
End Sub

Private Sub cbCancel_Click(Index As Integer)
  Unload frmGraphSettings
End Sub

Private Sub cbOk_Click(Index As Integer)
  'UpdateGraphSettings
  SaveGraphSettingsToRegistry
  GraphInfoChanged = True
  If ShowGraph Then
    UpdateGraph
  End If
  Unload Me
End Sub


Private Sub Check1_Click()

End Sub

Private Sub Form_Load()
  frmGraphSettings.Left = 500
  frmGraphSettings.Top = 500
  Dim i As Long
  
  With GraphSettings
  
  'HLC
  If .ShowHLC = True Then
    rbShowHLC = True
  Else
    rbShowLine = True
  End If
  
  'Bollinger
  If .ShowBollinger = True Then
    chShowBollinger = 1
  Else
    chShowBollinger = 0
  End If
  
  'Parabolic
  If .ShowParabolic = True Then
    chShowParabolic = 1
  Else
    chShowParabolic = 0
  End If
  
  'MAV1
  If .ShowPriceMAV1 = True Then
    chShowPriceMAV1 = 1
  Else
    chShowPriceMAV1 = 0
  End If
  
  'MAV2
  If .ShowPriceMAV2 = True Then
    chShowPriceMAV2 = 1
  Else
    chShowPriceMAV2 = 0
  End If
  
  'X-Grid
  If .ShowXGrid = True Then
    chShowXGrid = 1
  Else
    chShowXGrid = 0
  End If
  
  'Y-Grid
  If .ShowYGrid = True Then
    chShowYGrid = 1
  Else
    chShowYGrid = 0
  End If
  
  'Indicator 1
  If .ShowIndicator1 = True Then
    chShowIndicator1 = 1
  Else
    chShowIndicator1 = 0
  End If
  
  'Indicator 2
  If .ShowIndicator2 = True Then
    chShowIndicator2 = 1
  Else
    chShowIndicator2 = 0
  End If
  
  If .ShowSignalColor Then
    chSignalColor = 1
  Else
    chSignalColor = 0
  End If
  
  If .ExponentialPrice Then
    chExponentialPrice.Value = 1
  Else
    chExponentialPrice.Value = 0
  End If
  
  If .ShowVerticalColorLines Then
    chVerticalLines = 1
  Else
    chVerticalLines = 0
  End If
  
  If .ShowVolume Then
    chkShowVolume = vbChecked
  Else
    chkShowVolume = vbUnchecked
  End If
  End With
  
  
  
  For i = 1 To IndicatorArraySize
    cSignalIndicator.AddItem IndicatorArray(i)
  Next i
  cSignalIndicator.ListIndex = GraphSettings.SignalIndicator - 1
  
  For i = 1 To 2
    cDayWidth.AddItem i
  Next i
    
  cDayWidth.ListIndex = GraphSettings.DayWidth - 1
      
End Sub

