Attribute VB_Name = "TransactionOperations"
Option Explicit
''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          AddTransaction
'INPUT:         TransactionType
'OUTPUT:
'LAST UPDATE:   970419
'
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''
Sub AddTransaction(Transaction As TransactionType)
  
  Dim TempTransactionArray() As TransactionType
  Dim I As Long
  Dim NotInserted As Boolean
  
  TransactionArraySize = TransactionArraySize + 1
  ReDim TempTransactionArray(TransactionArraySize)
  ReDim Preserve TransactionArray(TransactionArraySize)
  NotInserted = True
  
  If TransactionArraySize = 1 Then
    TempTransactionArray(1) = Transaction
    NotInserted = False
  Else
    I = 1
    Do While I <= TransactionArraySize
      If Transaction.Date < TransactionArray(I).Date And NotInserted Then
        TempTransactionArray(I) = Transaction
        NotInserted = False
      Else
        If NotInserted Then
          TempTransactionArray(I) = TransactionArray(I)
        Else
          TempTransactionArray(I) = TransactionArray(I - 1)
        End If
      End If
      I = I + 1
    Loop
  End If
  If NotInserted Then
    TempTransactionArray(TransactionArraySize) = Transaction
    NotInserted = False
  End If
  ReDim TransactionArray(TransactionArraySize)
  For I = 1 To TransactionArraySize
    TransactionArray(I) = TempTransactionArray(I)
  Next I
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          DeleteTransaction
'INPUT:         long
'OUTPUT:
'LAST UPDATE:   970623
'
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''
Sub DeleteTransaction(Index As Long)

Dim j As Long
Dim TempTransactionArray() As TransactionType

If TransactionArraySize > 1 Then
  ReDim TempTransactionArray(TransactionArraySize - Index)

  For j = Index + 1 To TransactionArraySize
    TempTransactionArray(j - Index) = TransactionArray(j)
  Next j

  ReDim Preserve TransactionArray(TransactionArraySize - 1)

  For j = Index To TransactionArraySize - 1
    TransactionArray(j) = TempTransactionArray(j - Index + 1)
  Next j

  TransactionArraySize = TransactionArraySize - 1
Else
  TransactionArraySize = 0
  ReDim TransactionArray(0)
End If
  
End Sub

Sub ReplaceTransaction(Transaction As TransactionType, Index As Long)
  TransactionArray(Index) = Transaction
End Sub

''''''''''''''''''''''''''''''''''''''''''''''
'NAME:          GetTransactionIndex
'INPUT:         TransactionType
'OUTPUT:
'LAST UPDATE:   970623
'
'AUTHOR:       Fredrik Wendel
''''''''''''''''''''''''''''''''''''''''''''''
Function GetTransactionIndex(Transaction As TransactionType) As Long
Dim j As Long
Dim Found As Boolean
With Transaction
Found = False
j = 1
Do While Not Found
  If .Courtage = TransactionArray(j).Courtage And .Date = TransactionArray(j).Date _
    And .IsBuy = TransactionArray(j).IsBuy And .Name = TransactionArray(j).Name _
    And .Price = TransactionArray(j).Price And .Volume = TransactionArray(j).Volume _
    And .Cash = TransactionArray(j).Cash And .IsBuySell = TransactionArray(j).IsBuySell _
    And .IsDeposit = TransactionArray(j).IsDeposit Then
    Found = True
  End If
  j = j + 1
Loop
End With
If Found Then
  GetTransactionIndex = j - 1
Else
  GetTransactionIndex = -1
End If
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: CashDeposit
'Desc: Anropar CashDepositOK som i sin tur anropar AddTransaction
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub CashDeposit(frmCashDeposit As Object, IsDividend As Boolean)
If IsDividend Then
  If CashDepositOK(IsDividend) Then
    Unload frmStockDividend
  End If
Else
  If CashDepositOK(IsDividend) Then
    Unload frmCashDeposit
  End If
End If
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: CashDepositOK
'Desc: Anropar ReadCashDepositFromForm, om OK anropas AddTransaction
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CashDepositOK(IsDividend As Boolean) As Boolean
  Dim Transaction As TransactionType
  Dim Errorfound As Boolean
  
  If IsDividend Then
    ReadDividendFromForm frmStockDividend, Transaction
  Else
    ReadCashDepositFromForm frmCashDeposit, Transaction
  End If

  If Errorfound Then
    CashDepositOK = False
  Else
    AddTransaction Transaction
    CashDepositOK = True
  End If
  
End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: RenameTransaction
'Desc: �ndrar beskrivning p� en transaktion.
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub RenameTransactions(OldName As String, NewName As String)
Dim I As Long
  
  For I = 1 To TransactionArraySize
    If Trim(TransactionArray(I).Name) = Trim(OldName) Then
      TransactionArray(I).Name = Trim(NewName)
    End If
  Next I

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: InitAddInvestToPort
'Desc: Initierar f�nstret Frm_AddInvestToPort
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub InitAddInvestToPort()
Dim I As Long
Dim Portfolio As PortfolioType
  
  For I = 0 To InvestInfoArraySize - 1
    Portfolio.Name = InvestInfoArray(I + 1).Name
    If Not ExistInPortfolioArray(Portfolio) Then
      Frm_AddInvestToPort.List_Invest.AddItem InvestInfoArray(I + 1).Name
    End If
  Next I
  
  For I = 0 To PortfolioArraySize - 1
    Frm_AddInvestToPort.List_Port.AddItem PortfolioArray(I + 1).Name, I
  Next I
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MoveToPortFromInv
'Desc: Flyttar objekt till portf�ljlistan i Frm_AddInvestToPort
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub MoveToPortFromInv()
Dim I As Long
Dim LastItem As Long

LastItem = Frm_AddInvestToPort.List_Invest.ListCount - 1
I = 0

Do Until I > LastItem
  If Frm_AddInvestToPort.List_Invest.Selected(I) = True Then
    Frm_AddInvestToPort.List_Port.AddItem Frm_AddInvestToPort.List_Invest.List(I)
    Frm_AddInvestToPort.List_Invest.RemoveItem I
    LastItem = -1
  End If
  I = I + 1
Loop

blnPortfolioModified = True

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: MoveToInvFromPort
'Desc: Flyttar objekt fr�n portf�ljlistan i Frm_AddInvestToPort
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub MoveToInvFromPort()
Dim I As Long
Dim LastItem As Long

LastItem = Frm_AddInvestToPort.List_Port.ListCount - 1
I = 0

Do Until I > LastItem
  If Frm_AddInvestToPort.List_Port.Selected(I) = True Then
    Frm_AddInvestToPort.List_Invest.AddItem Frm_AddInvestToPort.List_Port.List(I)
    Frm_AddInvestToPort.List_Port.RemoveItem I
    LastItem = -1
  End If
  I = I + 1
Loop

blnPortfolioModified = True

End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: AddInvestToPortOK
'Desc: L�gger till nya objekt i portfolioarray.
'Chng:
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub AddInvestToPortOK()
  Dim I, j As Long
  Dim Portfolio As PortfolioType
  
  PortfolioArraySize = 0
  
  For I = 0 To Frm_AddInvestToPort.List_Port.ListCount - 1
    For j = 1 To InvestInfoArraySize
      If Frm_AddInvestToPort.List_Port.List(I) = InvestInfoArray(j).Name Then
        PortfolioArraySize = PortfolioArraySize + 1
      End If
    Next j
  Next I
  
  ReDim PortfolioArray(PortfolioArraySize)
  PortfolioArraySize = 0
  
  For I = 0 To Frm_AddInvestToPort.List_Port.ListCount - 1
    For j = 1 To InvestInfoArraySize
      If Frm_AddInvestToPort.List_Port.List(I) = InvestInfoArray(j).Name Then
        Portfolio.Name = InvestInfoArray(j).Name
        PortfolioArraySize = PortfolioArraySize + 1
        PortfolioArray(PortfolioArraySize) = Portfolio
      End If
     Next j
  Next I
  
  MainMDI.StatusBar.Panels.Item(1).Text = CheckLang("Portf�lj: Namnl�s.prt")
  ActivePortfolioFileName = "Namnl�s.prt"
  ActivePortfolio = True
  'InitInvestmentToolbar
  InitObjectCombo
  
  'With MainMDI.SSActiveToolBars1
  '  .Tools("ID_SavePortfolio").Enabled = True
  '  .Tools("ID_SavePortfolioAs").Enabled = True
  '  .Tools("ID_Report").Enabled = True
  '  .Tools.Item("ID_SavePortfolio").Enabled = True
  '  .Tools.Item("ID_StockTransaction").Enabled = True
  '  .Tools.Item("ID_DepositWithdrawal").Enabled = True
  '  .Tools.Item("ID_Dividend").Enabled = True
  '  .Tools.Item("ID_AddObjectToPortfolio").Enabled = True
  'End With
  
  Unload Frm_AddInvestToPort
  
  If NewPort Then
    NewPort = False
    frmCashDeposit.Show 0
  End If
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: BuySellOK
'Desc: L�ser intrasnaktionsdata fr�n formul�r och adderar ny transaktion.
'Chng: 99-08-25 ML �ndrad.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function BuySellOK() As Boolean
  Dim Transaction As TransactionType
  
  ReadTransactionFromForm Frm_BuySell, Transaction

  If Not Transaction.IsBuy Then
    If CheckNecessarySharesExist(Transaction, 100000) Then
      AddTransaction Transaction
      BuySellOK = True
    Else
      BuySellOK = False
    End If
  Else
    AddTransaction Transaction
    BuySellOK = True
  End If

End Function

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: ReadTransactionFromForm
'Desc: L�ser in transaktionsdata fr�n formul�r.
'Chng: 99-08-25 ML Ny.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Sub ReadTransactionFromForm(InputForm As Form, Transaction As TransactionType)
   
  With InputForm
    Transaction.IsBuySell = True
    If .cBuySell.ListIndex = 0 Then Transaction.IsBuy = True
    Transaction.Name = MainMDI.ID_Object.Text
    Transaction.Date = .DTPicker1.Value
    Transaction.Volume = CSng(PointToComma(.tbVolume.Text))
    Transaction.Price = CSng(PointToComma(.tbPrice.Text))
    Transaction.Courtage = CSng(PointToComma(.tbCourtage.Text))
    Transaction.IsDeposit = False
    Transaction.Cash = 0
  End With
 
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: ReadDividendFromForm
'Desc: L�ser in utdelningsdata from formul�r.
'Chng: 99-08-25 ML Ny.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ReadDividendFromForm(frmStockDividend As Form, Transaction As TransactionType)
  
  With frmStockDividend
    Transaction.IsBuySell = False
    Transaction.DepositType = "Utdelning"
    Transaction.Name = Trim(.Combo1.Text)
    Transaction.IsDeposit = True
    Transaction.Date = .DTPicker1.Value
    Transaction.Volume = 0
    Transaction.Price = 0
    Transaction.Courtage = 0
    Transaction.Cash = CSng(PointToComma(.tbDividend))
  End With
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: ReadCashDepositFromForm
'Desc: L�ser in depositdata fr�n formul�r.
'Chng: 99-08-25 ML Ny.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub ReadCashDepositFromForm(frmCashDeposit As Form, Transaction As TransactionType)

  With frmCashDeposit
    Transaction.IsBuySell = False
    Transaction.DepositType = .cmbChoice.Text
    If Trim(Transaction.DepositType) = "Ins�ttning" Or Trim(Transaction.DepositType) = "Utdelning" Or Trim(Transaction.DepositType) = "R�nteint�kter" Then
      Transaction.IsDeposit = True
    Else
      Transaction.IsDeposit = False
    End If
    Transaction.Date = .DTPicker1.Value
    Transaction.Volume = 0
    Transaction.Price = 0
    Transaction.Courtage = 0
    Transaction.Cash = CSng(PointToComma(.tbDeposit))
  End With
  
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: CheckNecessarySharesExist
'Desc: Kontrollerar s� att tillr�ckligt m�nga aktier finns i
'      dep�n d� aktier s�ljs. F�rhindrar negativt innehav.
'Chng: 99-08-25 ML Ny.
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Function CheckNecessarySharesExist(Transaction As TransactionType, MaxNumber As Long) As Boolean
Dim I As Integer
Dim VolumeOnHand As Single

VolumeOnHand = 0
I = 1
Do While I <= TransactionArraySize
  If TransactionArray(I).Date <= Transaction.Date And MaxNumber > I Then
    If StrComp(Trim(TransactionArray(I).Name), Trim(Transaction.Name)) = 0 Then
      If TransactionArray(I).IsBuySell Then
        If TransactionArray(I).IsBuy Then
          VolumeOnHand = VolumeOnHand + TransactionArray(I).Volume
        Else
          VolumeOnHand = VolumeOnHand - TransactionArray(I).Volume
        End If
      End If
    End If
  Else
    I = TransactionArraySize
  End If
  I = I + 1
Loop
  
  If VolumeOnHand >= Transaction.Volume Then
    CheckNecessarySharesExist = True
  Else
    CheckNecessarySharesExist = False
  End If
  
End Function
