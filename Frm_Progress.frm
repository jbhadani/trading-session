VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Frm_Progress 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Updating..."
   ClientHeight    =   795
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   3660
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   795
   ScaleWidth      =   3660
   Begin MSComCtlLib.ProgressBar PB_1 
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   3420
      _ExtentX        =   6033
      _ExtentY        =   450
      _Version        =   327680
      Appearance      =   1
   End
   Begin VB.Label Label1 
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   480
      Width           =   3375
   End
End
Attribute VB_Name = "Frm_Progress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
  Frm_Progress.Left = (MainMDI.Width - Frm_Progress.Width) / 4
  Frm_Progress.Top = (MainMDI.Height - Frm_Progress.Height) / 4
End Sub
