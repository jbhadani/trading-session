VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmTriggerRegistration 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Nytt bevakningsobjekt"
   ClientHeight    =   3645
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6315
   Icon            =   "frmTriggerRegistration.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3645
   ScaleWidth      =   6315
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame3 
      Caption         =   "V�lj objekt"
      Height          =   2775
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   3615
      Begin MSComctlLib.TreeView treObjects 
         Height          =   2415
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   3375
         _ExtentX        =   5953
         _ExtentY        =   4260
         _Version        =   393217
         LabelEdit       =   1
         Style           =   7
         Appearance      =   1
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   2880
      Width           =   6255
      Begin VB.CommandButton cmbCancel 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   4920
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton cmbOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Larmdata"
      Height          =   2775
      Left            =   3720
      TabIndex        =   0
      Top             =   0
      Width           =   2535
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   1200
         TabIndex        =   11
         Top             =   840
         Width           =   1215
         _ExtentX        =   2143
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61669377
         CurrentDate     =   36402
      End
      Begin VB.TextBox txtLowerLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         TabIndex        =   10
         Top             =   2280
         Width           =   1215
      End
      Begin VB.TextBox txtUpperLevel 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         TabIndex        =   9
         Top             =   1800
         Width           =   1215
      End
      Begin VB.TextBox txtClose 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   1200
         Locked          =   -1  'True
         TabIndex        =   8
         Top             =   1320
         Width           =   1215
      End
      Begin VB.Label lblObject 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   1  'Fixed Single
         Height          =   285
         Left            =   720
         TabIndex        =   15
         Top             =   360
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Objekt"
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   360
         Width           =   495
      End
      Begin VB.Label Label5 
         Caption         =   "Senast betalt"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1320
         Width           =   975
      End
      Begin VB.Label Label4 
         Caption         =   "Undre gr�ns"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label3 
         Caption         =   "�vre gr�ns"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1800
         Width           =   975
      End
      Begin VB.Label Label2 
         Caption         =   "Datum"
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   840
         Width           =   975
      End
   End
End
Attribute VB_Name = "frmTriggerRegistration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cboObject_Click()
  UpdateInfo cboObject.ListIndex
End Sub

Private Sub cmbCancel_Click()
  Unload Me
  frmTriggers.Enabled = True
End Sub

Private Sub cmbOK_Click()
  Dim AllDataOk As Boolean
  With frmTriggerRegistration
  If .lblObject.Caption <> "" And .txtClose.Text <> "" And .txtLowerLevel.Text <> "" And .txtUpperLevel.Text <> "" Then
    AllDataOk = True
  Else
    AllDataOk = False
  End If
  End With
  
  If AllDataOk Then
    If StrComp(Trim(frmTriggerRegistration.Caption), "Nytt bevakningsobjekt") = 0 Then
      AddNewTrigger
    Else
      UpdateTrigger
    End If
    frmTriggers.Enabled = True
    Unload Me
  Else
    MsgBox CheckLang("N�got eller n�gra av dataf�lten inneh�ller felaktig data. Komplettera."), vbOKOnly, CheckLang("Felaktig data")
  End If
End Sub

Private Sub Form_Load()
  Dim I As Integer
  Dim j As Long
  Dim strObjectName As String
  Dim udtObject As InvestInfoType
  Dim nodX As Node
  
  top = 1000
  left = 1500
  
  With treObjects
    .LineStyle = tvwRootLines
    With .Nodes
      For j = 1 To CategoryArraySize
        If Trim(CategoryArray(j)) <> "" Then
          Set nodX = .Add(, , Trim(CategoryArray(j)), Trim(CategoryArray(j)))
          For I = 1 To InvestInfoArraySize
            If InvestInfoArray(I).TypeOfInvest = j Then
              Set nodX = .Add(Trim(CategoryArray(j)), tvwChild, Trim(InvestInfoArray(I).Name), Trim(InvestInfoArray(I).Name))
            End If
          Next I
        End If
      Next j
    End With
  End With

  
  If StrComp(Trim(frmTriggerRegistration.Caption), "Nytt bevakningsobjekt") = 0 Then
    DTPicker1.Value = GetDateString(Date)
    DTPicker1.Enabled = False
    'txtDate = Date
    'txtDate.Locked = True
  End If
  
  
End Sub

Private Sub Form_Terminate()
  frmTriggers.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
  frmTriggers.Enabled = True
End Sub

Private Sub txtClose_GotFocus()
  TextSelected
End Sub

Private Sub txtClose_Validate(Cancel As Boolean)
  Dim A As Single
  
  On Error Resume Next
  A = CSng(txtClose)
  If Err.Number <> 0 Then
    Cancel = True
    MsgBox CheckLang("Du har matat in ett felaktigt v�rde f�r senast betalt. F�rs�k igen."), vbOKOnly, CheckLang("Felmeddelande")
    txtClose.SetFocus
    Err.Clear
  Else
    Cancel = False
  End If
  On Error GoTo 0
End Sub

Private Sub txtDate_GotFocus()
  TextSelected
End Sub

Private Sub txtLowerLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtLowerLevel_Validate(Cancel As Boolean)
  Dim A As Single
  
  On Error Resume Next
  A = CSng(txtLowerLevel)
  If Err.Number <> 0 Then
    Cancel = True
    MsgBox CheckLang("Du har matat in ett felaktigt v�rde f�r den undre gr�nsen. F�rs�k igen."), vbOKOnly, CheckLang("Felmeddelande")
    txtLowerLevel.SetFocus
    Err.Clear
  Else
    Cancel = False
  End If
  On Error GoTo 0
  
End Sub

Private Sub txtUpperLevel_GotFocus()
  TextSelected
End Sub

Private Sub txtUpperLevel_Validate(Cancel As Boolean)
  Dim A As Single
  
  On Error Resume Next
  A = CSng(txtUpperLevel)
  If Err.Number <> 0 Then
    Cancel = True
    MsgBox CheckLang("Du har matat in ett felaktigt v�rde f�r den �vre gr�nsen. F�rs�k igen."), vbOKOnly, CheckLang("Felmeddelande")
    txtUpperLevel.SetFocus
    Err.Clear
  Else
    Cancel = False
  End If
  On Error GoTo 0
End Sub

Private Sub treObjects_NodeClick(ByVal Node As MSComctlLib.Node)
Dim FileName As String
  
  If Node.Children = 0 Then
    lblObject.Caption = Node.key
    FileName = DataPath + Trim(InvestInfoArray(GetInvestIndex(frmTriggerRegistration.lblObject.Caption)).FileName) + ".prc"
    UpdateInfo FileName
  End If
  
End Sub

