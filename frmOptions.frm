VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Optionsanalys"
   ClientHeight    =   6045
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8910
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6045
   ScaleWidth      =   8910
   Begin VB.PictureBox Picture1 
      Height          =   615
      Left            =   0
      Picture         =   "frmOptions.frx":058A
      ScaleHeight     =   555
      ScaleWidth      =   8835
      TabIndex        =   4
      Top             =   0
      Width           =   8895
   End
   Begin VB.Frame Frame1 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   5280
      Width           =   8880
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   7320
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Ok"
         Height          =   375
         Left            =   600
         TabIndex        =   1
         Top             =   240
         Width           =   975
      End
   End
   Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
      Height          =   4515
      Left            =   0
      TabIndex        =   3
      Top             =   720
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   7964
      _Version        =   393216
      Rows            =   17
      Cols            =   21
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
  Unload Me
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub Form_Load()
  Dim i, j As Integer
  Height = 6420
  Width = 9000
  top = 50
  left = 1000
  With MSFlexGrid1
    For i = 0 To 20
      For j = 0 To 16
        .CellAlignment = flexAlignCenterCenter
      Next j
    Next i
    For i = 1 To 20
      .TextMatrix(0, i) = Str$(i)
    Next i
    
    For j = 1 To 20
      For i = 10 To 11
        .Row = i
        .Col = j
        .CellBackColor = QBColor(11)
      Next i
      For i = 12 To 16
        .Row = i
        .Col = j
        .CellBackColor = QBColor(14)
      Next i
    Next j
    
    .ColWidth(0) = 1500
    .Col = 0
    .Row = 1
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(1, 0) = "Aktie"
    .Row = 2
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(2, 0) = "Option"
    .Row = 3
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(3, 0) = "R�nta"
    .Row = 4
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(4, 0) = "Ber. datum"
    .Row = 5
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(5, 0) = "L�sendag"
    .Row = 6
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(6, 0) = "L�senpris"
    .Row = 7
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(7, 0) = "Underliggande"
    .Row = 8
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(8, 0) = "Option"
    .Row = 10
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(10, 0) = "Pris"
    .Row = 11
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(11, 0) = "Volatilitet"
    .Row = 12
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(12, 0) = "Delta"
    .Row = 13
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(13, 0) = "Gamma"
    .Row = 14
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(14, 0) = "Theta"
    .Row = 15
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(15, 0) = "Vega"
    .Row = 16
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(16, 0) = "Dagar kvar"
  End With
End Sub

Private Sub MSFlexGrid1_DblClick()
Dim temp As String
  
  With MSFlexGrid1
  .Col = .ColSel
  .Row = 1
  temp = Trim(.Text)
  If .ColSel > 0 Then
  If temp = "" Then
    frmOptionsInput.Show
  Else
    frmOptionsInput.txtObjectName(0) = .Text
    .Row = 2
    frmOptionsInput.txtOptionName(0) = .Text
    .Row = 3
    frmOptionsInput.txtInterestRate(0) = .Text
    .Row = 4
    frmOptionsInput.DTPicker1 = Trim(.Text)
    .Row = 5
    frmOptionsInput.DTPicker2 = Trim(.Text)
    .Row = 6
    frmOptionsInput.txtStrikePrice(0) = .Text
    .Row = 7
    frmOptionsInput.txtObjectPrice(0) = .Text
    .Row = 8
    temp = Trim(.Text)
    If StrComp(temp, "K�p") = 0 Then
      frmOptionsInput.Combo1.ListIndex = 0
    Else
      frmOptionsInput.Combo1.ListIndex = 0
    End If
    frmOptionsInput.Show
  End If
  End If
  End With
End Sub


