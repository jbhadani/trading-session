Attribute VB_Name = "StringManipulations"
Function PointToComma(ByRef Instring As String) As String
  Dim PointPlace As Long
  PointPlace = InStr(Instring, ".")
  If PointPlace <> 0 Then
     Mid(Instring, PointPlace, 1) = ","
  End If
  PointToComma = Instring
End Function

Function GetYearMonthString(dtmDate As Date)
Dim strTemp As String
  
  '''''''''''''''''''''''''''''''''
  ' Y2K bug found here!!!!!!!!!!!
  '''''''''''''''''''''''''''''''''
  strTemp = Mid(Year(dtmDate), 3, 2)
  If Month(dtmDate) < 10 Then
    strTemp = strTemp & "0" & CStr(Month(dtmDate))
  Else
    strTemp = strTemp & CStr(Month(dtmDate))
  End If
  GetYearMonthString = Trim(strTemp)
End Function



