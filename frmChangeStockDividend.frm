VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmChangeStockDividend 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "�ndra Aktieutdelning"
   ClientHeight    =   2685
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   Icon            =   "frmChangeStockDividend.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2685
   ScaleWidth      =   3765
   Begin VB.Frame Frame4 
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   1920
      Width           =   3735
      Begin VB.CommandButton cbCancel 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   2520
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton cbOK 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   360
         TabIndex        =   6
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Utdelning"
      Height          =   855
      Left            =   1920
      TabIndex        =   2
      Top             =   960
      Width           =   1815
      Begin VB.TextBox tbDividend 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   5
         Text            =   "frmChangeStockDividend.frx":014A
         Top             =   360
         Width           =   1095
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Datum"
      Height          =   855
      Left            =   0
      TabIndex        =   1
      Top             =   960
      Width           =   1815
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   20578305
         CurrentDate     =   36402
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Aktie"
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   3735
      Begin VB.ComboBox Combo1 
         Height          =   315
         Left            =   600
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   360
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmChangeStockDividend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub cbCancel_Click()
  Unload Me
  Frm_RepList.Enabled = True
End Sub

Private Sub cbOK_Click()
Dim Transaction As TransactionType
Dim Error As Boolean

  ReadDividendFromForm frmChangeStockDividend, Transaction
  
  'Transaction.Name = Trim(frmChangeStockDividend.Combo1.Text)
  
  DeleteTransaction Frm_RepList.Grid1.Row
  AddTransaction Transaction
  'ReplaceTransaction Transaction, Frm_RepList.Grid1.Row
  Unload frmChangeStockDividend
  UpdateReportGrid
  Frm_RepList.Enabled = True
  
End Sub


Private Sub tbDividend_GotFocus()
  TextSelected
End Sub

Private Sub tbDividend_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbDividend
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt belopp."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbDividend.SetFocus
End Sub
