VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "Mscomct2.ocx"
Begin VB.Form frmCashDeposit 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Ins�ttning/Uttag likvida medel"
   ClientHeight    =   2445
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3765
   Icon            =   "frmCashDeposit.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   2445
   ScaleWidth      =   3765
   Begin VB.Frame Frame4 
      Caption         =   "Ins�ttning/Uttag"
      Height          =   855
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   1815
      Begin VB.ComboBox cmbChoice 
         Height          =   315
         ItemData        =   "frmCashDeposit.frx":014A
         Left            =   120
         List            =   "frmCashDeposit.frx":015D
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   330
         Width           =   1575
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Datum"
      Height          =   855
      Left            =   0
      TabIndex        =   6
      Top             =   840
      Width           =   1815
      Begin MSComCtl2.DTPicker DTPicker1 
         Height          =   315
         Left            =   240
         TabIndex        =   8
         Top             =   360
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   61669377
         CurrentDate     =   36402
      End
   End
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   5
      Top             =   1680
      Width           =   3735
      Begin VB.CommandButton Command2 
         Caption         =   "Avbryt"
         CausesValidation=   0   'False
         Height          =   375
         Left            =   2520
         TabIndex        =   3
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Default         =   -1  'True
         Height          =   375
         Left            =   360
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Belopp"
      Height          =   855
      Left            =   1920
      TabIndex        =   4
      Top             =   840
      Width           =   1815
      Begin VB.TextBox tbDeposit 
         Alignment       =   2  'Center
         Height          =   285
         Left            =   360
         MultiLine       =   -1  'True
         TabIndex        =   1
         Text            =   "frmCashDeposit.frx":019D
         Top             =   360
         Width           =   1095
      End
   End
End
Attribute VB_Name = "frmCashDeposit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
  CashDeposit frmCashDeposit, False
  If ReportWindowOpen Then
    UpdateReportGrid
  End If
End Sub

Private Sub Form_Load()
  Height = 2850
  Width = 3885
  DTPicker1.Value = Date
  tbDeposit.Text = 0
  cmbChoice.ListIndex = 0
End Sub

Private Sub Command2_Click()
  Unload Me
End Sub

Private Sub tbDate_GotFocus()
  TextSelected
End Sub

Private Sub tbDeposit_Validate(Cancel As Boolean)
  Dim OK As Boolean
  VerifySingleInput OK, tbDeposit
  If Not OK Then
    MsgBox CheckLang("Du m�ste ange ett korrekt belopp."), vbOKOnly, CheckLang("Felmeddelande")
    Cancel = True
  End If
  tbDeposit.SetFocus
End Sub
