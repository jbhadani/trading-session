VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CPriceData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mstrName As String
Private mlngIndex As Long

Private mudtPricesDaily() As PriceDataType
Private mudtPricesWeekly() As PriceDataType
Private mudtPricesMonthly() As PriceDataType

Private mlngIndexFirstDaily As Long
Private mlngIndexLastDaily As Long
Private mlngIndexFirstWeekly As Long
Private mlngIndexLastWeekly As Long
Private mlngIndexFirstMonthly As Long
Private mlngIndexLastMonthly As Long


Friend Property Let Name(Name As String)
  mstrName = Name
  Debug.Print "CPriceData: Name set to: " & Name
End Property

Friend Property Get Name() As String
   Name = mstrName
End Property

Friend Property Let Index(Index As Long)
  mlngIndex = Index
  Debug.Print "CPriceData: Index set to: " & CStr(Index)
End Property

Friend Property Let Prices(Prices() As PriceDataType)
On Error GoTo Err
  ReDim mudtDailyPrices(LBound(Prices) To UBound(Prices))
  mudtPricesDaily = Prices
  MakeWeekly mudtPricesDaily, mudtPricesWeekly
  MakeMonthly mudtPricesDaily, mudtPricesMonthly
  mlngIndexFirstDaily = 1 ' Should be: LBound(mudtPricesDaily)
  mlngIndexLastDaily = UBound(mudtPricesDaily)
  mlngIndexFirstWeekly = 1 ' Should be:  LBound(mudtPricesWeekly)
  mlngIndexLastWeekly = UBound(mudtPricesWeekly)
  mlngIndexFirstMonthly = 1 ' Should be:  LBound(mudtPricesMonthly)
  mlngIndexLastMonthly = UBound(mudtPricesMonthly)
  Debug.Print "CPriceData: PriceData set (" & CStr(LBound(Prices)) & "-" & CStr(UBound(Prices)) & ")"
Err:
End Property

Friend Property Get PriceDaily(Index As Long) As PriceDataType
#If DBUG Then
  If Index < LBound(mudtPricesDaily) Or Index > UBound(mudtPricesDaily) Then
    Debug.Print "CPriceData::PriceDaily - Index out of bounds"
  End If
#End If
  PriceDaily = mudtPricesDaily(Index)
End Property
Friend Property Get PriceWeekly(Index As Long) As PriceDataType
#If DBUG Then
  If Index < LBound(mudtPricesWeekly) Or Index > UBound(mudtPricesWeekly) Then
    Debug.Print "CPriceData::PriceWeekly - Index out of bounds"
  End If
#End If
  PriceWeekly = mudtPricesWeekly(Index)
End Property
Friend Property Get PriceMonthly(Index As Long) As PriceDataType
#If DBUG Then
  If Index < LBound(mudtPricesMonthly) Or Index > UBound(mudtPricesMonthly) Then
    Debug.Print "CPriceData::PricesMonthly - Index out of bounds"
  End If
#End If
  PriceMonthly = mudtPricesMonthly(Index)
End Property
Friend Property Get Price(DWM As DWMEnum, Index As Long) As PriceDataType
Select Case DWM
Case dwmDaily
    Price = mudtPricesDaily(Index)
Case dwmWeekely
    Price = mudtPricesWeekly(Index)
Case dwmMonthly
    Price = mudtPricesMonthly(Index)
End Select
End Property

Public Property Get IndexFirstDaily() As Long
  IndexFirstDaily = mlngIndexFirstDaily
End Property

Public Property Get IndexLastDaily() As Long
  IndexLastDaily = mlngIndexLastDaily
End Property
Public Property Get IndexFirstWeekly() As Long
  IndexFirstWeekly = mlngIndexFirstWeekly
End Property

Public Property Get IndexLastWeekly() As Long
  IndexLastWeekly = mlngIndexLastWeekly
End Property
Public Property Get IndexFirstMonthly() As Long
  IndexFirstMonthly = mlngIndexFirstMonthly
End Property

Public Property Get IndexLastMonthly() As Long
  IndexLastMonthly = mlngIndexLastMonthly
End Property

Friend Property Get IndexFirst(DWM As DWMEnum) As Long
Select Case DWM
Case dwmDaily
    IndexFirst = mlngIndexFirstDaily
Case dwmWeekely
    IndexFirst = mlngIndexFirstWeekly
Case dwmMonthly
    IndexFirst = mlngIndexFirstMonthly
End Select
End Property
Friend Property Get IndexLast(DWM As DWMEnum) As Long
Select Case DWM
Case dwmDaily
    IndexLast = mlngIndexLastDaily
Case dwmWeekely
    IndexLast = mlngIndexLastWeekly
Case dwmMonthly
    IndexLast = mlngIndexLastMonthly
End Select
End Property

Public Property Get IndexFromDateDaily(theDate As Date) As Long
  IndexFromDateDaily = GetIndex(mudtPricesDaily, theDate)
End Property

Public Property Get IndexFromDateWeekly(theDate As Date) As Long
  IndexFromDateWeekly = GetIndex(mudtPricesWeekly, theDate)
End Property

Public Property Get IndexFromDateMonthly(theDate As Date) As Long
  IndexFromDateMonthly = GetIndex(mudtPricesMonthly, theDate)
End Property
Friend Property Get IndexFromDate(DWM As DWMEnum, theDate As Date) As Long
Select Case DWM
Case dwmDaily
    IndexFromDate = GetIndex(mudtPricesDaily, theDate)
Case dwmWeekely
    IndexFromDate = GetIndex(mudtPricesWeekly, theDate)
Case dwmMonthly
    IndexFromDate = GetIndex(mudtPricesMonthly, theDate)
End Select
End Property
Friend Property Get IndexFromDateNear(DWM As DWMEnum, theDate As Date) As Long
Select Case DWM
Case dwmDaily
    IndexFromDateNear = GetIndexNear(mudtPricesDaily, theDate)
Case dwmWeekely
    IndexFromDateNear = GetIndexNear(mudtPricesWeekly, theDate)
Case dwmMonthly
    IndexFromDateNear = GetIndexNear(mudtPricesMonthly, theDate)
End Select
End Property
Public Sub GetMinAndMaxDaily(ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
                        
  CalcMinAndMax mudtPricesDaily, IndexFirst, IndexLast, Min, Max
End Sub
Public Sub GetMinAndMaxWeekly(ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
                        
  CalcMinAndMax mudtPricesWeekly, IndexFirst, IndexLast, Min, Max
End Sub

Public Sub GetMinAndMaxMontlhy(ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
                        
  CalcMinAndMax mudtPricesMonthly, IndexFirst, IndexLast, Min, Max
End Sub

Public Sub GetMinAndMax(ByRef DWM As DWMEnum, _
                        ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
                        
  Select Case DWM
  Case dwmDaily
    CalcMinAndMax mudtPricesDaily, IndexFirst, IndexLast, Min, Max
  Case dwmWeekely
    CalcMinAndMax mudtPricesWeekly, IndexFirst, IndexLast, Min, Max
  Case dwmMonthly
    CalcMinAndMax mudtPricesMonthly, IndexFirst, IndexLast, Min, Max
  End Select
End Sub

Public Sub GetMinAndMaxVolume(ByRef DWM As DWMEnum, _
                        ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
                        
  Select Case DWM
  Case dwmDaily
    CalcMinAndMaxVolume mudtPricesDaily, IndexFirst, IndexLast, Min, Max
  Case dwmWeekely
    CalcMinAndMaxVolume mudtPricesWeekly, IndexFirst, IndexLast, Min, Max
  Case dwmMonthly
    CalcMinAndMaxVolume mudtPricesMonthly, IndexFirst, IndexLast, Min, Max
  End Select
End Sub
Private Sub MakeMonthly(ByRef PDArray() As PriceDataType, ByRef MonthlyArray() As PriceDataType)

Dim EndIndex As Long
Dim I As Long
Dim High As Single
Dim Low As Single
Dim Volume As Single
Dim MonthlySize As Long
EndIndex = UBound(PDArray)

Low = PDArray(1).Low
High = PDArray(1).High
Volume = PDArray(I).Volume

For I = 2 To EndIndex
  If Month(PDArray(I).Date) > Month(PDArray(I - 1).Date) _
     Or Year(PDArray(I).Date) > Year(PDArray(I - 1).Date) Or I = EndIndex Then
    If I = EndIndex Then
      Volume = Volume + PDArray(I).Volume
      If PDArray(I).High > High Then
        High = PDArray(I).High
      End If
      If PDArray(I).Low < Low Then
       Low = PDArray(I).Low
      End If
    End If
    MonthlySize = MonthlySize + 1
    ReDim Preserve MonthlyArray(MonthlySize)
    MonthlyArray(MonthlySize).Date = DateSerial(Year(PDArray(I - 1).Date), Month(PDArray(I - 1).Date), 1)
    MonthlyArray(MonthlySize).Close = PDArray(I - 1).Close
    MonthlyArray(MonthlySize).High = High
    MonthlyArray(MonthlySize).Low = Low
    MonthlyArray(MonthlySize).Volume = Volume
    Volume = 0
    Low = PDArray(I).Low
    High = PDArray(I).High
  End If
  Volume = Volume + PDArray(I).Volume
  If PDArray(I).High > High Then
    High = PDArray(I).High
  End If
  If PDArray(I).Low < Low Then
    Low = PDArray(I).Low
  End If
Next I
Debug.Print "CPrice::MakeMonthly - Done"
End Sub

Private Function MakeWeekly(ByRef PDArray() As PriceDataType, ByRef WeeklyArray() As PriceDataType)


Dim EndIndex As Long
Dim I As Long
Dim High As Single
Dim Low As Single
Dim Volume As Single
Dim WeeklySize As Long
Dim DateOfFirstDay As Date
EndIndex = UBound(PDArray)

Low = PDArray(1).Low
High = PDArray(1).High
Volume = PDArray(I).Volume
DateOfFirstDay = PDArray(1).Date
WeeklySize = 0

For I = 2 To EndIndex
  If Weekday(PDArray(I).Date) < Weekday(PDArray(I - 1).Date) _
      Or Weekday(PDArray(I - 1).Date) = vbSunday Or I = EndIndex Then
    If I = EndIndex Then
      If PDArray(I).High > High Then
        High = PDArray(I).High
      End If
      If PDArray(I).Low < Low Then
        Low = PDArray(I).Low
      End If
      Volume = Volume + PDArray(I).Volume
      Debug.Print CStr(PDArray(I).Date) & ",";
    End If
    WeeklySize = WeeklySize + 1
    ReDim Preserve WeeklyArray(WeeklySize)
    WeeklyArray(WeeklySize).Date = DateOfFirstDay
    WeeklyArray(WeeklySize).Close = PDArray(I - 1).Close
    WeeklyArray(WeeklySize).High = High
    WeeklyArray(WeeklySize).Low = Low
    WeeklyArray(WeeklySize).Volume = Volume
    DateOfFirstDay = PDArray(I).Date
    Volume = 0
    Low = PDArray(I).Low
    High = PDArray(I).High
  End If
  Volume = Volume + PDArray(I).Volume
  If PDArray(I).High > High Then
    High = PDArray(I).High
  End If
  If PDArray(I).Low < Low Then
    Low = PDArray(I).Low
  End If
Next I
Debug.Print "CPrice::MakeWeekly - Done"
End Function


Private Sub CalcMinAndMax(Prices() As PriceDataType, _
                        ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
  Dim I As Long

#If DBUG Then
  If IndexFirst < LBound(Prices) Then
    Debug.Print "CPriceData::CalcMinAndMax - IndexFirst out of bounds"
  End If
  If IndexLast < UBound(Prices) Then
    Debug.Print "CPriceData::clcMinAndMax - IndexLast out of bounds"
  End If
#End If

  Max = Prices(IndexFirst).Low
  Min = Prices(IndexFirst).High

  For I = IndexFirst + 1 To IndexLast
    With Prices(I)
    If .High > Max Then
      Max = .High
    End If
    If .Low < Min Then
      Min = .Low
    End If
    End With
  Next I
End Sub

Private Sub CalcMinAndMaxVolume(Prices() As PriceDataType, _
                        ByRef IndexFirst As Long, _
                        ByRef IndexLast As Long, _
                        ByRef Min As Single, _
                        ByRef Max As Single)
  Dim I As Long

#If DBUG Then
  If IndexFirst < LBound(Prices) Then
    Debug.Print "CPriceData::CalcMinAndMax - IndexFirst out of bounds"
  End If
  If IndexLast < UBound(Prices) Then
    Debug.Print "CPriceData::clcMinAndMax - IndexLast out of bounds"
  End If
#End If

  Max = Prices(IndexFirst).Volume
  Min = Prices(IndexFirst).Volume

  For I = IndexFirst + 1 To IndexLast
    With Prices(I)
    If .Volume > Max Then
      Max = .Volume
    End If
    If .Volume < Min Then
      Min = .Volume
    End If
    End With
  Next I
End Sub

Private Function GetIndex(ByRef thePrices() As PriceDataType, theDate As Date) As Long
  
  Dim I As Long
  Dim IsFound  As Boolean
  IsFound = False
  I = UBound(thePrices)
  Do While Not IsFound And I > 0
    If Int(thePrices(I).Date) = Int(theDate) Then
      IsFound = True
    Else
      I = I - 1
    End If
  Loop
  If IsFound Then
    GetIndex = I
  Else
    GetIndex = -1
  End If
End Function

Private Function GetIndexNear(ByRef thePrices() As PriceDataType, theDate As Date) As Long
  
  Dim I As Long
  Dim IsFound  As Boolean
  IsFound = False
  
  If Int(theDate) <= Int(thePrices(1).Date) Then
    GetIndexNear = 1
    IsFound = True
  Else
    If Int(theDate) >= Int(thePrices(UBound(thePrices)).Date) Then
      GetIndexNear = UBound(thePrices)
      IsFound = True
    Else
      I = 1
      Do While Not IsFound And I <= UBound(thePrices) - 1
        If Int(theDate) >= Int(thePrices(I).Date) And Int(theDate) < Int(thePrices(I + 1).Date) Then
         IsFound = True
        Else
         I = I + 1
        End If
      Loop
      If IsFound Then
        GetIndexNear = I
      Else
        GetIndexNear = -1
      End If
    End If
  End If
  

End Function

Private Sub Class_Initialize()
Static lngNumberOfInstances As Long
  lngNumberOfInstances = lngNumberOfInstances + 1
  Debug.Print "CPriceData::Initialize (" & CStr(lngNumberOfInstances) & ")"
End Sub

Private Sub Class_Terminate()
  Debug.Print "CPriceData::Terminate"
End Sub
