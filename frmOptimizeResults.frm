VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form frmOptimizeResults 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Optimeringsresultat"
   ClientHeight    =   4845
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7950
   Icon            =   "frmOptimizeResults.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4845
   ScaleWidth      =   7950
   Begin VB.Frame Frame2 
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   4080
      Width           =   7935
      Begin VB.CommandButton Command3 
         Caption         =   "Avbryt"
         Height          =   375
         Left            =   6600
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Använd"
         Height          =   375
         Left            =   3480
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Command1 
         Caption         =   "OK"
         Height          =   375
         Left            =   360
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Optimeringsresultat"
      Height          =   3255
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   7935
      Begin MSFlexGridLib.MSFlexGrid MSFlexGrid1 
         Height          =   2895
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   7695
         _ExtentX        =   13573
         _ExtentY        =   5106
         _Version        =   393216
         Cols            =   9
         FixedCols       =   0
      End
   End
   Begin VB.PictureBox Picture1 
      Height          =   680
      Left            =   0
      Picture         =   "frmOptimizeResults.frx":014A
      ScaleHeight     =   615
      ScaleWidth      =   7875
      TabIndex        =   0
      Top             =   0
      Width           =   7935
   End
End
Attribute VB_Name = "frmOptimizeResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Dim Row As Integer
Dim Parameter1, Parameter2, Parameter3 As Long
Dim Parameter4 As Single
Dim Optimal As OptimizeType
Dim Svar As Long

  With frmOptimizeResults.MSFlexGrid1
    Row = .Row
    Parameter1 = .TextMatrix(Row, 1)
    Parameter2 = .TextMatrix(Row, 2)
    Parameter3 = .TextMatrix(Row, 3)
    Parameter4 = CSng(.TextMatrix(Row, 1))
  End With
  With Optimal
  .Parameter1 = Parameter1
  .Parameter2 = Parameter2
  .Parameter3 = Parameter3
  .sngParameter4 = Parameter4
  End With
  Svar = MsgBox(CheckLang("Vill du överföra de markerade parametrarna") & " ( " & Trim(Str$(Parameter1)) & ", " & Trim(Str$(Parameter2)) & ", " & Trim(Str$(Parameter3)) & ") till parameterinställningarna och använda dessa?", vbYesNo, CheckLang("Meddelande"))
  If Svar = vbYes Then
    CopyToParameterSettings Optimal
    GraphInfoChanged = True
    If ShowGraph Then
      UpdateGraph ActiveIndex
    End If
    DoEvents
  End If
  Unload Me
End Sub

Private Sub Command2_Click()
Dim Row As Integer
Dim Parameter1, Parameter2, Parameter3 As Long
Dim Parameter4 As Single

Dim Optimal As OptimizeType
Dim Svar As Long

  With frmOptimizeResults.MSFlexGrid1
    Row = .Row
    Parameter1 = .TextMatrix(Row, 1)
    Parameter2 = .TextMatrix(Row, 2)
    Parameter3 = .TextMatrix(Row, 3)
    Parameter4 = CSng(.TextMatrix(Row, 1))
  End With
   With Optimal
  .Parameter1 = Parameter1
  .Parameter2 = Parameter2
  .Parameter3 = Parameter3
  .sngParameter4 = Parameter4
  End With
  Svar = MsgBox(CheckLang("Vill du överföra de markerade parametrarna") & " ( " & Trim(Str$(Parameter1)) & ", " & Trim(Str$(Parameter2)) & ", " & Trim(Str$(Parameter3)) & ") till parameterinställningarna och använda dessa?", vbYesNo, CheckLang("Meddelande"))
  If Svar = vbYes Then
    CopyToParameterSettings Optimal
    If frmParameters.Visible Then
      InitParameters
    End If
    GraphInfoChanged = True
    If ShowGraph Then
      UpdateGraph ActiveIndex
    End If
  End If
End Sub

Private Sub Command3_Click()
  Unload Me
End Sub

Private Sub Form_Load()
Dim i As Integer

top = 1000
left = 1000

  With MSFlexGrid1
    .ColWidth(0) = 575
    For i = 1 To 3
     .ColWidth(i) = 1000
    Next i
    For i = 4 To 8
     .ColWidth(i) = 750
    Next i
    .Row = 0
    .Col = 1
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 1) = "Parameter 1"
    .Col = 2
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 2) = "Parameter 2"
    .Col = 3
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 3) = "Parameter 3"
    .Col = 4
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 4) = "Affärer"
    .Col = 5
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 5) = "Vinster"
    .Col = 6
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 6) = "Förluster"
    .Col = 7
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 7) = "Störst %"
    .Col = 8
    .CellAlignment = flexAlignCenterCenter
    .TextMatrix(0, 8) = "Resultat"
    .Row = 1
    .Col = 0
  End With
End Sub

Private Sub MSFlexGrid1_DblClick()
  With MSFlexGrid1
    If .ColSel <= 3 Or .ColSel = 6 Then
      .Col = .ColSel
      .Sort = 3
    Else
      .Col = .ColSel
      .Sort = 4
    End If
  End With
End Sub
