Attribute VB_Name = "Test"

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestHandler
'Input:
'Output:
'
'Description:   TestManager
'Author:        Martin Lindberg
'Revision:      980930 �ndrat s� att om aktien ligger i k�p sista
'                      testdagen r�knas resultatet p� den aff�ren
'                      till sista testdatum och ej sista dagen
'                      i pricedataarrayen.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub TestHandler()
Dim TestIndicator As String
Dim j As Long
Dim Result As Single
Dim Action As Boolean
Dim Last As Long
Dim Courtage As Single
Dim TotalTime As Long
Dim Res As String
Dim Stock As String
Dim Start As Long
Dim Stopp As Long
Dim nStartDate As Long
Dim nStopDate As Long
Dim A As Long
Dim Filename1 As String
Dim Size As Long

'''''
'Added 99-03-27
'ML
'''''
UpdateIndicatortestOptions

Filename1 = DataPath + Trim(InvestInfoArray(GetInvestIndex(IndicatortestObject)).FileName) + ".prc"
    
R_PriceDataArray Filename1, PDIndicatortestArray(), PDIndicatortestArraySize

Start = 0
Stopp = 0

nStartDate = IndicatorTestOptions.StartDate
nStopDate = IndicatorTestOptions.StopDate

If Not TestOK Then
  MsgBox CheckLang("Det g�r inte att g�ra ett indikatortest eftersom det finns mindre �n 2 dagskurser lagrade f�r det aktiva objektet."), vbOKOnly, CheckLang("Felmeddelande")
  Exit Sub
Else

  Debug.Print "Startdate: " & CDate(nStartDate)
  If CDate(PDIndicatortestArray(1).Date) >= CDate(nStartDate) Then
    Start = 1
  Else
    For I = 1 To PDIndicatortestArraySize
      If CDate(Int(PDIndicatortestArray(I).Date)) >= CDate(nStartDate) Then
        Start = I
        I = PDIndicatortestArraySize
      End If
    Next I
  End If
  Debug.Print "Startdate: " & PDIndicatortestArray(Start).Date

  Debug.Print "Stopdate: " & CDate(nStopDate)
  If Int(PDIndicatortestArray(PDIndicatortestArraySize).Date) <= nStopDate Then
    Stopp = PDIndicatortestArraySize
  Else
    I = PDIndicatortestArraySize
    Do While I > 0
      If Int(PDIndicatortestArray(I).Date) <= nStopDate Then
        Stopp = I
        I = 0
      End If
      I = I - 1
    Loop
  End If
   Debug.Print "Stoppdate: " & PDIndicatortestArray(Stopp).Date

  TotalTime = 0
  Result = 1

  TestIndicator = frmIndicatortest.cmbIndicator.Text
  Select Case TestIndicator
    Case "Ease Of Movement"
      TestEOM Start, Stopp
    Case "MACD"
      TestMACD Start, Stopp
    Case "Momentum"
      TestMomentum Start, Stopp
    Case "Money Flow Index"
      TestMFI Start, Stopp
    Case "Moving Average"
      TestMAV Start, Stopp
    Case "Parabolic"
      TestParabolic Start, Stopp
    Case "Price Oscillator"
      TestPriceOsc Start, Stopp
    Case "Rate-of-change"
      TestROC Start, Stopp
    Case "Relative Strength Index"
      TestRSI Start, Stopp
    Case "Stochastic Oscillator"
      TestStochasticOsc Start, Stopp
    Case "TRIX"
      TestTRIX Start, Stopp
  End Select

End If
End Sub

Public Sub UpdateIndicatortest(TestOK As Boolean)
  Dim I As Long
  With Frm_IndicatorTest
    If PriceDataArraySize >= 2 Then
      TestOK = True
      .tbStartdatum = GetDateString(PriceDataArray(1).Date)
      .tbSlutdatum = GetDateString(PriceDataArray(PriceDataArraySize).Date)
    Else
      TestOK = False
      .tbStartdatum = ""
      .tbSlutdatum = ""
    End If
  End With
End Sub

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: RunTest
'Desc: Letar upp k�p- och s�ljsignaler och placerar all trade-
'      info i en array, IndicatortestResult()
'Chng: 990209 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub RunTest(Signalarray() As Long, PDArray() As PriceDataType, Start As Long, Stopp As Long)
Dim I As Long
Dim j As Long
Dim OpenPosition As Boolean
Dim PositionTypeIsLong As Boolean
Dim EnterLevel As Single
Dim EnterDateIndex As Long
Dim BreakevenStopActivated As Boolean
Dim Benchmark As Single
Dim BreakevenLevel As Single

j = 0
OpenPosition = False
BreakevenStopActivated = False
With IndicatorTestOptions
For I = Start To Stopp
  If Signalarray(I) <> 0 Then
    If Signalarray(I) = 1 And Signalarray(I - 1) = -1 Then
      If IndicatorTestOptions.LongOnly Then
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, True
        OpenPosition = True
        PositionTypeIsLong = True
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      ElseIf IndicatorTestOptions.BothLongAndShort Then
        If OpenPosition Then
          SetExitData j, I, PDArray(), Stopp, 0
          BreakevenStopActivated = False
        End If
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, True
        OpenPosition = True
        PositionTypeIsLong = True
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      Else
        If OpenPosition Then
          SetExitData j, I, PDArray(), Stopp, 0
          BreakevenStopActivated = False
        End If
        OpenPosition = False
      End If
    ElseIf Signalarray(I) = -1 And Signalarray(I - 1) = 1 Then
      If IndicatorTestOptions.LongOnly Then
        If OpenPosition Then
          SetExitData j, I, PDArray(), Stopp, 0
          BreakevenStopActivated = False
        End If
        OpenPosition = False
      ElseIf IndicatorTestOptions.BothLongAndShort Then
        If OpenPosition Then
          SetExitData j, I, PDArray(), Stopp, 0
          BreakevenStopActivated = False
        End If
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, False
        OpenPosition = True
        PositionTypeIsLong = False
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      Else
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, False
        OpenPosition = True
        PositionTypeIsLong = False
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      End If
    ElseIf Signalarray(I) = 1 And Signalarray(I - 1) = 0 Then
      If IndicatorTestOptions.LongOnly Or IndicatorTestOptions.BothLongAndShort Then
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, True
        OpenPosition = True
        PositionTypeIsLong = True
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      End If
    ElseIf Signalarray(I) = -1 And Signalarray(I - 1) = 0 Then
      If IndicatorTestOptions.ShortOnly Or IndicatorTestOptions.BothLongAndShort Then
        j = j + 1
        ReDim Preserve IndicatorTestResult(j)
        SetEntryData j, I, PDArray(), Stopp, False
        OpenPosition = True
        PositionTypeIsLong = False
        EnterLevel = IndicatorTestResult(j).TradePriceAtEntry
        Benchmark = EnterLevel
        EnterDateIndex = I + IndicatorTestOptions.Delay
      End If
    End If
  End If
  
  If OpenPosition Then
    If .UseBreakevenLongPositions And PositionTypeIsLong Then
      EnterLevel = IndicatorTestResult(UBound(IndicatorTestResult())).TradePriceAtEntry
      If IndicatorTestOptions.UseCommission And IndicatorTestOptions.UsePercentageCommission Then
        If I = EnterDateIndex Then
          BreakevenLevel = EnterLevel + IndicatorTestOptions.Percentage / 100 * EnterLevel * 2 - IndicatorTestOptions.BreakevenLevel
        End If
      Else
        'Lite fusk. Sv�rt att anv�nda mincourtage...
        BreakevenLevel = EnterLevel - IndicatorTestOptions.BreakevenLevel
      End If
      If PDArray(I).Close > BreakevenLevel And Not BreakevenStopActivated Then
        BreakevenStopActivated = True
      End If
      If PDArray(I).Close < BreakevenLevel And BreakevenStopActivated Then
        SetExitData j, I, PDArray(), Stopp, 1
        OpenPosition = False
        BreakevenStopActivated = False
      End If
    ElseIf .UseBreakevenShortPositions And PositionTypeIsShort Then
      EnterLevel = IndicatorTestResult(UBound(IndicatorTestResult())).TradePriceAtEntry
      If IndicatorTestOptions.UsePercentageCommission Then
        If I = EnterDateIndex Then
          BreakevenLevel = EnterLevel - IndicatorTestOptions.Percentage / 100 * EnterLevel * 2 + IndicatorTestOptions.BreakevenLevel
        End If
      Else
        'Lite fusk. Sv�rt att anv�nda mincourtage...
        BreakevenLevel = EnterLevel + IndicatorTestOptions.BreakevenLevel
      End If
      If PDArray(I).Close < BreakevenLevel Then
        BreakevenStopActivated = True
      End If
      If PDArray(I).Close > BreakevenLevel And BreakevenStopActivated Then
        SetExitData j, I, PDArray(), Stopp, 1
        OpenPosition = False
        BreakevenStopActivated = False
      End If
    End If
    
    If .UseInactivityLongPositions And PositionTypeIsLong Then
      If .InactivityMethodIsPercent Then
        If I = .InactivityPeriods + EnterDateIndex And PDArray(I).Close < EnterLevel * (1 + .InactivityMinChange / 100) Then
          SetExitData j, I, PDArray(), Stopp, 2
          OpenPosition = False
          BreakevenStopActivated = False
        End If
      Else
        If I = .InactivityPeriods + EnterDateIndex And PDArray(I).Close < EnterLevel + .InactivityMinChange Then
          SetExitData j, I, PDArray(), Stopp, 2
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      End If
    ElseIf .UseInactivityShortPositions And PositionTypeIsShort Then
      If .InactivityMethodIsPercent Then
        If I = .InactivityPeriods + EnterDateIndex And PDArray(I).Close > EnterLevel * (1 - .InactivityMinChange / 100) Then
          SetExitData j, I, PDArray(), Stopp, 2
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      Else
        If I = .InactivityPeriods + EnterDateIndex And PDArray(I).Close > EnterLevel - .InactivityMinChange Then
          SetExitData j, I, PDArray(), Stopp, 2
          OpenPosition = False
          BreakevenStopActivated = False
        End If
      End If
    End If
    
    
    
    If .UseMaxLossLongPositions And PositionTypeIsLong Then
      If .MaxLossMethodIsPercent Then
        If PDArray(I).Close < (1 - .MaxLossLevel / 100) * EnterLevel Then
          SetExitData j, I, PDArray(), Stopp, 3
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      Else
        If PDArray(I).Close < EnterLevel - .MaxLossLevel Then
          SetExitData j, I, PDArray(), Stopp, 3
          OpenPosition = False
          BreakevenStopActivated = False
        End If
      End If
    ElseIf .UseMaxLossShortPositions And PositionTypeIsShort Then
      If .MaxLossMethodIsPercent Then
        If PDArray(I).Close > (1 + .MaxLossLevel / 100) * EnterLevel Then
          SetExitData j, I, PDArray(), Stopp, 3
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      Else
        If PDArray(I).Close > EnterLevel + .MaxLossLevel Then
          SetExitData j, I, PDArray(), Stopp, 3
          OpenPosition = False
          BreakevenStopActivated = False
        End If
      End If
    End If
    
    
    
    If .UseProfitTakingLongPositions And PositionTypeIsLong Then
      If .ProfitTakingMethodIsPercent Then
        If PDArray(I).Close >= (1 + .ProfitTarget / 100) * EnterLevel Then
          SetExitData j, I, PDArray(), Stopp, 4
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      Else
        If PDArray(I).Close >= EnterLevel + .ProfitTarget Then
          SetExitData j, I, PDArray(), Stopp, 4
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      End If
    ElseIf .UseProfitTakingShortPositions And PositionTypeIsShort Then
      If .ProfitTakingMethodIsPercent Then
        If PDArray(I).Close <= (1 - .ProfitTarget / 100) * EnterLevel Then
          SetExitData j, I, PDArray(), Stopp, 4
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      Else
        If PDArray(I).Close <= EnterLevel - .ProfitTarget Then
          SetExitData j, I, PDArray(), Stopp, 4
          BreakevenStopActivated = False
          OpenPosition = False
        End If
      End If
    End If
    
    
    
    If .UseTrailingLongPositions And PositionTypeIsLong Then
      If .TrailingMethodIsPercent Then
        If I >= .TrailingPeriods + EnterDateIndex Then
          If PDArray(I).Close < Benchmark * (1 - .TrailingProfitRisk / 100) Then
            SetExitData j, I, PDArray(), Stopp, 5
            OpenPosition = False
            BreakevenStopActivated = False
          End If
          If Benchmark < PDArray(I - .TrailingPeriods).High Then
            Benchmark = PDArray(I - .TrailingPeriods).High
          End If
        End If
      Else
        If I >= .TrailingPeriods + EnterDateIndex Then
          If PDArray(I).Close < Benchmark - .TrailingProfitRisk Then
            SetExitData j, I, PDArray(), Stopp, 5
            BreakevenStopActivated = False
            OpenPosition = False
          End If
          If Benchmark < PDArray(I - .TrailingPeriods).High Then
            Benchmark = PDArray(I - .TrailingPeriods).High
          End If
        End If
      End If
    ElseIf .UseInactivityShortPositions And PositionTypeIsShort Then
      If .InactivityMethodIsPercent Then
        If I >= .TrailingPeriods + EnterDateIndex Then
          If PDArray(I).Close > Benchmark * (1 + .TrailingProfitRisk / 100) Then
            SetExitData j, I, PDArray(), Stopp, 5
            OpenPosition = False
            BreakevenStopActivated = False
          End If
          If Benchmark > PDArray(I - .TrailingPeriods).Low Then
            Benchmark = PDArray(I - .TrailingPeriods).Low
          End If
        End If
      Else
        If I >= .TrailingPeriods + EnterDateIndex Then
          If PDArray(I).Close > Benchmark + .TrailingProfitRisk Then
            SetExitData j, I, PDArray(), Stopp, 5
            OpenPosition = False
            BreakevenStopActivated = False
          End If
          If Benchmark > PDArray(I - .TrailingPeriods).Low Then
            Benchmark = PDArray(I - .TrailingPeriods).Low
          End If
        End If
      End If
    End If
  
  End If
Next I
End With

CalcTestStatistics j, Start, Stopp, PDArray()

End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: SetEntryData
'Desc: Lagrar info om trade i IndicatortestResult()
'Chng: 990209 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SetEntryData(j As Long, I As Long, PDArray() As PriceDataType, Stopp As Long, TradeTypeIsLong As Boolean)
Dim Delay As Long

If (IndicatorTestOptions.Delay + I) > Stopp Then
  Delay = Stopp - I
Else
  Delay = IndicatorTestOptions.Delay
End If

IndicatorTestResult(j).StartIndex = I + Delay

IndicatorTestResult(j).DateOfPositionEntry = PDArray(I + Delay).Date
If TradeTypeIsLong Then
  IndicatorTestResult(j).TypeOfTrade = "Long"
Else
  IndicatorTestResult(j).TypeOfTrade = "Short"
End If
If IndicatorTestOptions.TradePrice = 3 Then
  IndicatorTestResult(j).TradePriceAtEntry = PDArray(I + Delay).Close
ElseIf IndicatorTestOptions.TradePrice = 1 Then
  IndicatorTestResult(j).TradePriceAtEntry = PDArray(I + Delay).High
ElseIf IndicatorTestOptions.TradePrice = 2 Then
  IndicatorTestResult(j).TradePriceAtEntry = PDArray(I + Delay).Low
Else
  'skall �ndras till .open
  IndicatorTestResult(j).TradePriceAtEntry = PDArray(I + Delay).Close
End If
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: SetExitData
'Desc: Lagrar info om trade i IndicatortestResult()
'Chng: 990209 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub SetExitData(j As Long, I As Long, PDArray() As PriceDataType, Stopp As Long, ReasonForClose As Long)
Dim Delay As Long

If (IndicatorTestOptions.Delay + I) > Stopp Then
  Delay = Stopp - I
Else
  Delay = IndicatorTestOptions.Delay
End If

IndicatorTestResult(j).StoppIndex = I + Delay
IndicatorTestResult(j).ReasonForClose = ReasonForClose

IndicatorTestResult(j).DateOfPositionClose = PDArray(I + Delay).Date
If IndicatorTestOptions.TradePrice = 3 Then
  IndicatorTestResult(j).TradePriceAtClose = PDArray(I + Delay).Close
ElseIf IndicatorTestOptions.TradePrice = 1 Then
  IndicatorTestResult(j).TradePriceAtClose = PDArray(I + Delay).High
ElseIf IndicatorTestOptions.TradePrice = 2 Then
  IndicatorTestResult(j).TradePriceAtClose = PDArray(I + Delay).Low
Else
  'skall �ndras till .open
  IndicatorTestResult(j).TradePriceAtClose = PDArray(I + Delay).Close
End If
End Sub


'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name: CalcTestStatistics
'Desc: Ber�knar teststatistik och lagrar denna i Indicatortest
'Chng: 990209 ML, skapad
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub CalcTestStatistics(NbrOfTrades As Long, Start As Long, Stopp As Long, PDArray() As PriceDataType)
Dim I As Long
Dim k As Long
Dim Commission As Single
Dim ThisTradeProfit As Single
Dim OldPortfolioValue As Single
Dim NewPortfolioValue As Single
Dim TotalCommission As Single
Dim NbrOfDaysWhenHoldingPosition As Long

OldPortfolioValue = IndicatorTestOptions.InitialEquity  'Capital
NewPortfolioValue = 0
Commission = 0
TotalCommission = 0
NbrOfDaysWhenHoldingPosition = 0

InitTestStatistics

If NbrOfTrades > 0 Then
  With IndicatorTestStatistics
    .InitialInvestment = IndicatorTestOptions.InitialEquity
    .DaysInTest = Stopp - Start + 1
    .CurrentPosition = Trim(IndicatorTestResult(NbrOfTrades).TypeOfTrade)
    .DateCurrentPositionEntered = IndicatorTestResult(NbrOfTrades).DateOfPositionEntry
    If PDArray(Start).Close <> 0 Then
      .BuyHoldPercentageProfit = (PDArray(Stopp).Close - PDArray(Start).Close) / PDArray(Start).Close
    Else
      For k = Start To Stopp
        If PDArray(k).Close <> 0 Then
          .BuyHoldPercentageProfit = (PDArray(Stopp).Close - PDArray(k).Close) / PDArray(k).Close
          k = Stopp
        End If
      Next k
    End If
    .BuyHoldProfit = .BuyHoldPercentageProfit * .InitialInvestment
    .BuyHoldPercentageProfit = .BuyHoldPercentageProfit * 100
    .AnnualBuyHoldPercentage = 365 / .DaysInTest * .BuyHoldPercentageProfit
    
  
    If IndicatorTestResult(NbrOfTrades).TradePriceAtClose = 0 Then
      .TotalClosedTrades = NbrOfTrades - 1
    Else
      .TotalClosedTrades = NbrOfTrades
    End If


    For I = 1 To .TotalClosedTrades
     
      If StrComp(Trim(IndicatorTestResult(I).TypeOfTrade), "Long") = 0 Then
        ThisTradeProfit = (((IndicatorTestResult(I).TradePriceAtClose - IndicatorTestResult(I).TradePriceAtEntry) / IndicatorTestResult(I).TradePriceAtEntry) + 1)
      Else
        ThisTradeProfit = (((IndicatorTestResult(I).TradePriceAtEntry - IndicatorTestResult(I).TradePriceAtClose) / IndicatorTestResult(I).TradePriceAtEntry) + 1)
      End If
    
      If IndicatorTestOptions.UseCommission Then
        If IndicatorTestOptions.UsePercentageCommission Then
          Commission = (OldPortfolioValue * (IndicatorTestOptions.Percentage / 100)) + ((OldPortfolioValue * ThisTradeProfit) * (IndicatorTestOptions.Percentage / 100))
        Else
          Commission = 2 * IndicatorTestOptions.MinCommission
        End If
      End If
    
      NewPortfolioValue = OldPortfolioValue * ThisTradeProfit - Commission
    
      TotalCommission = Commission + TotalCommission
    
      .PercentGainLoss = (NewPortfolioValue / OldPortfolioValue * (.PercentGainLoss + 1)) - 1
    
   
      If StrComp(Trim(IndicatorTestResult(I).TypeOfTrade), "Long") = 0 Then
        .TotalLongTrades = .TotalLongTrades + 1
        If IndicatorTestResult(I).TradePriceAtClose > IndicatorTestResult(I).TradePriceAtEntry Then
          .WinningLongTrades = .WinningLongTrades + 1
          .AverageWin = .AverageWin + (NewPortfolioValue / OldPortfolioValue - 1)
          If (NewPortfolioValue / OldPortfolioValue - 1) > .LargestWin Then
            .LargestWin = (NewPortfolioValue / OldPortfolioValue - 1)
          End If
        Else
          .AverageLoss = .AverageLoss + (NewPortfolioValue / OldPortfolioValue - 1)
          If (NewPortfolioValue / OldPortfolioValue - 1) < .LargestLoss Then
            .LargestLoss = (NewPortfolioValue / OldPortfolioValue - 1)
          End If
        End If
      Else
        .TotalShortTrades = .TotalShortTrades + 1
        If IndicatorTestResult(I).TradePriceAtClose < IndicatorTestResult(I).TradePriceAtEntry Then
          .WinningShortTrades = .WinningShortTrades + 1
          .AverageWin = .AverageWin + (NewPortfolioValue / OldPortfolioValue - 1)
          If (NewPortfolioValue / OldPortfolioValue - 1) > .LargestWin Then
            .LargestWin = (NewPortfolioValue / OldPortfolioValue - 1)
          End If
        Else
          .AverageLoss = .AverageLoss + (NewPortfolioValue / OldPortfolioValue - 1)
          If (NewPortfolioValue / OldPortfolioValue - 1) < .LargestLoss Then
            .LargestLoss = (NewPortfolioValue / OldPortfolioValue - 1)
          End If
        End If
      End If
    
      NbrOfDaysWhenHoldingPosition = NbrOfDaysWhenHoldingPosition + (IndicatorTestResult(I).StoppIndex - IndicatorTestResult(I).StartIndex + 1)
    
      OldPortfolioValue = NewPortfolioValue
    Next I
  
    .TotalWinningTrades = .WinningLongTrades + .WinningShortTrades
    .TotalLosingTrades = .TotalClosedTrades - .TotalWinningTrades
    .TotalNetProfit = NewPortfolioValue - IndicatorTestOptions.InitialEquity 'Capital
  
    If .DaysInTest > 0 Then
      .AnnualPercentGainLoss = 365 / .DaysInTest * .PercentGainLoss
    Else
      .AnnualPercentGainLoss = 0
    End If
   
    If .TotalClosedTrades > 0 Then
      .AverageProfitPerTrade = .TotalNetProfit / .TotalClosedTrades
    Else
      .AverageProfitPerTrade = 0
    End If
  
    .CommissionsPaid = TotalCommission
  
    .AmountOfWinningTrades = .AverageWin * 100
    .AmountOfLosingTrades = .AverageLoss * 100
  
    If .TotalWinningTrades > 0 Then
      .AverageWin = .AverageWin * 100 / .TotalWinningTrades
    Else
      .AverageWin = 0
    End If
   
    If .TotalLosingTrades > 0 Then
      .AverageLoss = .AverageLoss * 100 / .TotalLosingTrades
    Else
      .AverageLoss = 0
    End If
  
    If .AverageLoss <> 0 Then
      .AverageWinLossRatio = Abs(.AverageWin / .AverageLoss)
    Else
      .AverageWinLossRatio = 100
    End If
  
    .TotalBarsOut = .DaysInTest - NbrOfDaysWhenHoldingPosition
   
    If .PercentGainLoss <> 0 Then
      .ProfitLossIndex = (.AmountOfWinningTrades + .AmountOfLosingTrades) / .PercentGainLoss
    Else
      .ProfitLossIndex = 0
    End If
  
    If .BuyHoldPercentageProfit <> 0 Then
      .BuyHoldIndex = .PercentGainLoss / (.BuyHoldPercentageProfit / 100)
    Else
      .BuyHoldIndex = 0
    End If
  End With
End If

UpdateIndicatortestResultGrids NbrOfTrades, Start, Stopp, PDArray()

End Sub

Public Sub UpdateIndicatortestResultGrids(NbrOfTrades As Long, Start As Long, Stopp As Long, PDArray() As PriceDataType)
Dim I As Long
Dim CellColor As Long
Dim Remainder As Long

frmIndicatortestResult.Show 0

With frmIndicatortestResult.TradesGrid
.Cols = 6
.Redraw = False
If NbrOfTrades > 15 Then
  .ColWidth(0) = 600
  .ColWidth(1) = 1200
  .ColWidth(2) = 1300
  .ColWidth(3) = 1300
  .ColWidth(4) = 1500
  .ColWidth(5) = 1150
Else
  .ColWidth(0) = 600
  .ColWidth(1) = 1200
  .ColWidth(2) = 1300
  .ColWidth(3) = 1300
  .ColWidth(4) = 1500
  .ColWidth(5) = 1495
End If

.Rows = NbrOfTrades + 1
.Row = 0
.Col = 0
.Text = "Nr."
.Col = 1
.Text = "Typ"
.Col = 2
.Text = "Startdatum"
.Col = 3
.Text = "Stoppdatum"
.Col = 4
.Text = "Utstoppad"
.Col = 5
.Text = "Resultat, %"

For I = 1 To IndicatorTestStatistics.TotalClosedTrades
  .Row = I
  Remainder = I Mod 2
  If Remainder = 0 Then
    For j = 0 To 5
      .Col = j
      .CellBackColor = RGB(230, 230, 230)
    Next j
  End If
  If Trim(IndicatorTestResult(I).TypeOfTrade) = "Long" Then
    If IndicatorTestResult(I).TradePriceAtClose > IndicatorTestResult(I).TradePriceAtEntry Then
      CellColor = 2
    Else
      CellColor = 12
    End If
  Else
    If IndicatorTestResult(I).TradePriceAtClose > IndicatorTestResult(I).TradePriceAtEntry Then
      CellColor = 12
    Else
      CellColor = 2
    End If
  End If
  .Col = 0
  .CellForeColor = QBColor(CellColor)
  .Text = Str$(I)
  .Col = 1
  .CellForeColor = QBColor(CellColor)
  
  'Lagt till 991209 ML.
  If StrComp(Trim(IndicatorTestResult(I).TypeOfTrade), "Long") = 0 Then
    .Text = "L�ng"
  Else
    .Text = "Kort"
  End If
  .Col = 2
  .CellForeColor = QBColor(CellColor)
  .Text = (Int(IndicatorTestResult(I).DateOfPositionEntry))
  .Col = 3
  .CellForeColor = QBColor(CellColor)
  .Text = (Int(IndicatorTestResult(I).DateOfPositionClose))
  .Col = 4
  .CellForeColor = QBColor(CellColor)
  If IndicatorTestResult(I).ReasonForClose = 0 Then
    .Text = "Normal"
  ElseIf IndicatorTestResult(I).ReasonForClose = 1 Then
    .Text = "Breakeven"
  ElseIf IndicatorTestResult(I).ReasonForClose = 2 Then
    .Text = "Inaktivitet"
  ElseIf IndicatorTestResult(I).ReasonForClose = 3 Then
    .Text = "Maxf�rlust"
  ElseIf IndicatorTestResult(I).ReasonForClose = 4 Then
    .Text = "Vinsttagning"
  Else
    .Text = "Trailing"
  End If
  .Col = 5
  .CellForeColor = QBColor(CellColor)
  If StrComp(Trim(IndicatorTestResult(I).TypeOfTrade), "Long") = 0 Then
    .Text = FormatNumber(((IndicatorTestResult(I).TradePriceAtClose - IndicatorTestResult(I).TradePriceAtEntry) / IndicatorTestResult(I).TradePriceAtEntry) * 100, 2)
  Else
    .Text = FormatNumber(((IndicatorTestResult(I).TradePriceAtEntry - IndicatorTestResult(I).TradePriceAtClose) / IndicatorTestResult(I).TradePriceAtClose) * 100, 2)
  End If
Next I
If I = NbrOfTrades Then
  .Row = NbrOfTrades
  .Col = 0
  .Text = Str$(I)
  .Col = 1
  If StrComp(Trim(IndicatorTestResult(I).TypeOfTrade), "Long") = 0 Then
    .Text = "L�ng - �ppen"
  Else
    .Text = "Kort - �ppen"
  End If
  .Col = 2
  .Text = CDate(Int(IndicatorTestResult(I).DateOfPositionEntry))
  .Col = 5
  .Text = FormatNumber(((PDArray(Stopp).Close - IndicatorTestResult(I).TradePriceAtEntry) / IndicatorTestResult(I).TradePriceAtEntry) * 100, 2)
End If
.Redraw = True
End With

If NbrOfTrades > 0 Then
With frmIndicatortestResult.StatisticsGrid
  .Cols = 5
  .Redraw = False
  .ColWidth(0) = 2200
  .ColWidth(1) = 1325
  .ColWidth(2) = 100
  .ColWidth(3) = 2200
  .ColWidth(4) = 1325
  .Rows = 21
  .Row = 0
  .Col = 0
  .Text = "Total vinst"
  .Col = 1
  .Text = FormatCurrency(IndicatorTestStatistics.TotalNetProfit, 2)
  .Col = 3
  .Text = "�ppen position"
  .Col = 4
  If IndicatorTestResult(NbrOfTrades).TradePriceAtClose = 0 Then
    If StrComp(Trim(IndicatorTestResult(NbrOfTrades).TypeOfTrade), "Long") = 0 Then
      .Text = FormatNumber(((PDArray(Stopp).Close - IndicatorTestResult(I).TradePriceAtEntry) / IndicatorTestResult(I).TradePriceAtEntry) * 100, 2) & " %"
    Else
      .Text = FormatNumber(((IndicatorTestResult(I).TradePriceAtEntry - PDArray(Stopp).Close) / PDArray(Stopp).Close) * 100, 2) & " %"
    End If
  Else
    .CellAlignment = 6
    .Text = " --- "
  End If
  .Row = 1
  .Col = 0
  .Text = "Avkastning"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.PercentGainLoss * 100, 2) & " %"
  .Col = 3
  .Text = "Startdatum"
  .Col = 4
  .Text = CDate(Int(IndicatorTestOptions.StartDate))
  .Row = 2
  .Col = 0
  .Text = "Startkapital"
  .Col = 1
  .Text = FormatCurrency(IndicatorTestStatistics.InitialInvestment, 2)
  .Col = 3
  .Text = "Slutdatum"
  .Col = 4
  .Text = CDate(Int(IndicatorTestOptions.StopDate))
  .Row = 4
  .Col = 0
  .Text = "Nuvarande position"
  If IndicatorTestResult(NbrOfTrades).TradePriceAtClose = 0 Then
    .Col = 1
    .CellAlignment = 6
    If StrComp(Trim(IndicatorTestResult(NbrOfTrades).TypeOfTrade), "Long") = 0 Then
      .Text = "L�ng"
    Else
      .Text = "Kort"
    End If
    .Col = 4
    .CellAlignment = 6
    .Text = CDate(Int(IndicatorTestResult(NbrOfTrades).DateOfPositionEntry))
  Else
    .Col = 1
    .CellAlignment = 6
    .Text = "Ingen position"
    .Col = 4
    .CellAlignment = 6
    .Text = " --- "
  End If
  .Col = 3
  .Text = "Position tagen"
  .Row = 6
  .Col = 0
  .Text = "K�p och beh�ll vinst"
  .Col = 1
  .Text = FormatCurrency(IndicatorTestStatistics.BuyHoldProfit, 2)
  .Col = 3
  .Text = "Antal testdagar"
  .Col = 4
  .Text = IndicatorTestStatistics.DaysInTest
  .Row = 7
  .Col = 0
  .Text = "K�p och beh�ll"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.BuyHoldPercentageProfit, 2) & " %"
  .Col = 3
  .Text = "�rlig k�p & beh�ll avk."
  .Col = 4
  .Text = FormatNumber(IndicatorTestStatistics.AnnualBuyHoldPercentage, 2) & " %"
  .Row = 8
  .Col = 0
  .Text = "Antal avslutade aff�rer"
  .Col = 1
  .Text = IndicatorTestStatistics.TotalClosedTrades
  .Col = 3
  .Text = "Totalt courtage"
  .Col = 4
  .Text = FormatCurrency(IndicatorTestStatistics.CommissionsPaid, 2)
  .Row = 9
  .Col = 0
  .Text = "Medelavkastning per aff�r"
  .Col = 1
  .Text = FormatCurrency(IndicatorTestStatistics.AverageProfitPerTrade, 2)
  .Col = 3
  .Text = "Vinst/f�rlust f�rh�llande"
  .Col = 4
  .Text = FormatNumber(IndicatorTestStatistics.AverageWinLossRatio, 2)
  .Row = 10
  .Col = 0
  .Text = "Antal l�nga positioner"
  .Col = 1
  .Text = IndicatorTestStatistics.TotalLongTrades
  .Col = 3
  .Text = "Antal korta positioner"
  .Col = 4
  .Text = IndicatorTestStatistics.TotalShortTrades
  .Row = 11
  .Col = 0
  .Text = "Antal l�nga vinstaff�rer"
  .Col = 1
  .Text = IndicatorTestStatistics.WinningLongTrades
  .Col = 3
  .Text = "Antal korta vinstaff�rer"
  .Col = 4
  .Text = IndicatorTestStatistics.WinningShortTrades
  .Row = 13
  .Col = 0
  .Text = "Antal vinstaff�rer"
  .Col = 1
  .Text = IndicatorTestStatistics.TotalWinningTrades
  .Col = 3
  .Text = "Antal f�rlustaff�rer"
  .Col = 4
  .Text = IndicatorTestStatistics.TotalLosingTrades
  .Row = 14
  .Col = 0
  .Text = "Resultat vinstaff�rer"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.AmountOfWinningTrades, 2) & " %"
  .Col = 3
  .Text = "Resultat f�rlustaff�rer"
  .Col = 4
  .Text = FormatNumber(IndicatorTestStatistics.AmountOfLosingTrades, 2) & " %"
  .Row = 15
  .Col = 0
  .Text = "Medelvinst"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.AverageWin, 2) & " %"
  .Col = 3
  .Text = "Medelf�rlust"
  .Col = 4
  .Text = FormatNumber(IndicatorTestStatistics.AverageLoss, 2) & " %"
  .Row = 17
  .Col = 0
  .Text = "Antal dagar utan position"
  .Col = 1
  .Text = IndicatorTestStatistics.TotalBarsOut
  .Row = 19
  .Col = 0
  .Text = "Vinst/f�rlust index"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.ProfitLossIndex, 2)
  .Row = 20
  .Col = 0
  .Text = "K�p och beh�ll index"
  .Col = 1
  .Text = FormatNumber(IndicatorTestStatistics.BuyHoldIndex, 2)
  .Redraw = True
  
  For I = 0 To 20
  .Row = I
  Remainder = I Mod 2
    If Remainder <> 0 Then
      For j = 0 To 4
        .Col = j
        .CellBackColor = RGB(230, 230, 230)
      Next j
    End If
  Next I
  
  End With
  End If
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestMAV
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestMAV(Start As Long, Stopp As Long)
  Dim Signalarray() As Long
  Dim MAV1Array() As Single
  Dim MAV2Array() As Single
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim MAV1Array(PDIndicatortestArraySize) As Single
  ReDim MAV2Array(PDIndicatortestArraySize) As Single
  
  If Not ParameterSettings.MAVUseExp Then
    SimpleMAVCalc PDIndicatortestArray(), MAV1Array(), ParameterSettings.MAV1, PDIndicatortestArraySize, True
    SimpleMAVCalc PDIndicatortestArray(), MAV2Array(), ParameterSettings.MAV2, PDIndicatortestArraySize, True
  Else
    ExpMAVCalc PDIndicatortestArray(), MAV1Array(), ParameterSettings.MAV1, PDIndicatortestArraySize
    ExpMAVCalc PDIndicatortestArray(), MAV2Array(), ParameterSettings.MAV2, PDIndicatortestArraySize
  End If
  
  MAVSignalArray Signalarray(), MAV1Array(), MAV2Array(), PDIndicatortestArraySize
  
  RunTest Signalarray, PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestMFI
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestMFI(ByVal Start As Long, Stopp As Long)
  Dim MFIArray() As Single
  Dim MAV() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim MFIArray(PDIndicatortestArraySize)
  ReDim MAV(PDIndicatortestArraySize)
  
  MFICalc PDIndicatortestArray(), MFIArray, ParameterSettings.MFILength, PDIndicatortestArraySize, True
  
  ExpCalc MFIArray(), MAV(), ParameterSettings.MAVMFI, PDIndicatortestArraySize
  
  MFISignalArray Signalarray(), MFIArray(), MAV(), PDIndicatortestArraySize
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
  
  End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestCOP
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Public Sub TestCOP(ByVal Start As Long, Stopp As Long)
'  Dim COP() As Single
'  Dim Signalarray() As Long
'
'  ReDim Signalarray(PDIndicatortestArraySize)
'  ReDim COP(PDIndicatortestArraySize)'
'
'  CoppockCalc PriceDataArray, COP(), ParameterSettings.COPMAV, ParameterSettings.COPDist, ParameterSettings.COPSUM, PDIndicatortestArraySize, True
'
'  CoppockSignalArray Signalarray(), COP(), PDIndicatortestArraySize
'
'  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
'
'End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestRSI
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestRSI(ByVal Start As Long, Stopp As Long)
  Dim RSIArray() As Single
  Dim MAV() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim RSIArray(PDIndicatortestArraySize)
  ReDim MAV(PDIndicatortestArraySize)

  RSICalc PDIndicatortestArray(), RSIArray(), ParameterSettings.RSI, PDIndicatortestArraySize, True
  ExpCalc RSIArray(), MAV(), ParameterSettings.MAVRSI, PDIndicatortestArraySize
  
  RSISignalArray Signalarray(), RSIArray(), MAV(), PDIndicatortestArraySize
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestStochasticOsc
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
'               981112 �ndrat felaktig parameter. Parametersettings.STOMAV -> .MAVSTO.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestStochasticOsc(ByVal Start As Long, Stopp As Long)
  Dim Stochastic() As Single
  Dim MAV() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim Stochastic(PDIndicatortestArraySize)
  ReDim MAV(PDIndicatortestArraySize)

  StochasticOscCalc PDIndicatortestArray(), Stochastic(), ParameterSettings.STO, PDIndicatortestArraySize, True, ParameterSettings.STOSlowing
  ExpCalc Stochastic(), MAV(), ParameterSettings.MAVSTO, PDIndicatortestArraySize
  
  STOSignalArray Signalarray(), Stochastic(), MAV, PDIndicatortestArraySize
  
  RunTest Signalarray, PDIndicatortestArray(), Start, Stopp
  
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestPriceOsc
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestPriceOsc(ByVal Start As Long, Stopp As Long)
  Dim MAV1() As Single
  Dim MAV2() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim MAV1(PDIndicatortestArraySize) As Single
  ReDim MAV2(PDIndicatortestArraySize) As Single
    
  If Not ParameterSettings.OSCMAVExp Then
    SimpleMAVCalc PDIndicatortestArray(), MAV1(), ParameterSettings.PRIOSC1, PDIndicatortestArraySize, True
    SimpleMAVCalc PDIndicatortestArray(), MAV2(), ParameterSettings.PRIOSC2, PDIndicatortestArraySize, True
  Else
    ExpMAVCalc PDIndicatortestArray(), MAV1(), ParameterSettings.PRIOSC1, PDIndicatortestArraySize
    ExpMAVCalc PDIndicatortestArray(), MAV2(), ParameterSettings.PRIOSC2, PDIndicatortestArraySize
  End If
  
  MAVSignalArray Signalarray(), MAV1(), MAV2(), PDIndicatortestArraySize
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestMACD
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestMACD(Start As Long, Stopp As Long)
Dim MACD() As Single
Dim MACDSignal() As Single
Dim Signalarray() As Long
ReDim MACD(PDIndicatortestArraySize)
ReDim MACDSignal(PDIndicatortestArraySize)
ReDim Signalarray(PDIndicatortestArraySize)

MACDCalc PDIndicatortestArray(), MACD(), MACDSignal(), PDIndicatortestArraySize, True

MACDSignalArray Signalarray(), MACD(), MACDSignal(), PDIndicatortestArraySize

RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp

End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestMOM
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestMomentum(ByVal Start As Long, Stopp As Long)
  Dim MOMArray() As Single
  Dim MAV() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim MOMArray(PDIndicatortestArraySize)
  ReDim MAV(PDIndicatortestArraySize)
  
  MomentumCalc PDIndicatortestArray(), MOMArray(), ParameterSettings.MOM, PDIndicatortestArraySize, True
  ExpCalc MOMArray(), MAV(), ParameterSettings.MAVMOM, PDIndicatortestArraySize
  
  MomentumSignalArray Signalarray(), MOMArray(), MAV(), PDIndicatortestArraySize
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestParabolic
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestParabolic(ByVal Start As Long, Stopp As Long)
  Dim Parabolic() As Single
  Dim ParabolicSearch() As Boolean
  Dim Signalarray() As Long
  Dim TempSignal() As Boolean
  Dim Size As Long
  
  ParabolicCalc PDIndicatortestArray(), Parabolic(), TempSignal(), PDIndicatortestArraySize, False, ParameterSettings.AccelerationFactor
  
  ParabolicSignalArray Signalarray, TempSignal(), UBound(PDIndicatortestArray)
    
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestEOM
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestEOM(ByVal Start As Long, Stopp As Long)
  Dim EOM() As Single
  Dim EOMMAV() As Single
  Dim Signalarray() As Long
  ReDim EOM(PDIndicatortestArraySize)
  ReDim EOMMAV(PDIndicatortestArraySize)
  ReDim Signalarray(PDIndicatortestArraySize)
  
  EaseOfMovementCalc PDIndicatortestArray(), EOM(), ParameterSettings.EaseOfMovementLength, PDIndicatortestArraySize, True
  SimpleCalc EOM, EOMMAV, ParameterSettings.EaseOfMovementMAVLength, PDIndicatortestArraySize, True
  
  If ParameterSettings.PlotEaseOfMovementAndMAV Then
    EaseOfMovementSignalArray Signalarray(), EOMMAV(), PDIndicatortestArraySize
  Else
    EaseOfMovementSignalArray Signalarray(), EOM(), PDIndicatortestArraySize
  End If
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestROC
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:      980928 Lagt till s� att avkastning b�rjar ber�knas
'                      redan f�rsta dagen om aktien ligger i en
'                      k�ptrend.
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestROC(Start As Long, Stopp As Long)
  Dim Signalarray() As Long
  Dim ROC() As Single
  Dim ROCMAV() As Single
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim ROC(PDIndicatortestArraySize) As Single
  ReDim ROCMAV(PDIndicatortestArraySize) As Single
  
  RateOfChangeCalc PDIndicatortestArray, ROC, ParameterSettings.RateOfChangeLength, PDIndicatortestArraySize, True
  
  If ParameterSettings.PlotRateOfChangeAndMAV Then
    SimpleCalc ROC, ROCMAV, ParameterSettings.RateOfChangeMAVLength, PDIndicatortestArraySize, True
    RateOfChangeSignalArray Signalarray, ROCMAV, PDIndicatortestArraySize
  Else
    RateOfChangeSignalArray Signalarray, ROC, PDIndicatortestArraySize
  End If
  
  RunTest Signalarray, PDIndicatortestArray(), Start, Stopp
  
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          TestTRIX
'Input:         Courtagesats, Startdatum, Stoppdatum
'Output:
'
'Description:   Calculates the tradingresult for a indicator.
'Author:        Martin Lindberg
'Revision:
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Public Sub TestTRIX(ByVal Start As Long, Stopp As Long)
  Dim TRIX() As Single
  Dim TRIXMAV() As Single
  Dim Signalarray() As Long
  
  ReDim Signalarray(PDIndicatortestArraySize)
  ReDim TRIX(PDIndicatortestArraySize)
  ReDim TRIXMAV(PDIndicatortestArraySize)
  
  TRIXCalc PDIndicatortestArray(), TRIX(), ParameterSettings.TRIXLength, PDIndicatortestArraySize, True
  SimpleCalc TRIX(), TRIXMAV(), ParameterSettings.TRIXMAVLength, PDIndicatortestArraySize, True
    
  TRIXSignalArray Signalarray(), TRIX(), TRIXMAV(), PDIndicatortestArraySize
  
  RunTest Signalarray(), PDIndicatortestArray(), Start, Stopp
End Sub


Public Sub SaveIndicatortestOptionsToRegistry()
With IndicatorTestOptions

SaveSetting App.Title, "Indicatortest", "Delay", Trim(Str$(IndicatorTestOptions.Delay))
SaveSetting App.Title, "Indicatortest", "LongOnly", Trim(Str$(IndicatorTestOptions.LongOnly))
SaveSetting App.Title, "Indicatortest", "ShortOnly", Trim(Str$(IndicatorTestOptions.ShortOnly))
SaveSetting App.Title, "Indicatortest", "BothLongAndShort", Trim(Str$(IndicatorTestOptions.BothLongAndShort))
SaveSetting App.Title, "Indicatortest", "InitialEquity", Trim(Str$(IndicatorTestOptions.InitialEquity))
SaveSetting App.Title, "Indicatortest", "MarginRequirement", Trim(Str$(IndicatorTestOptions.MarginRequirement))
SaveSetting App.Title, "Indicatortest", "AnnualInterestRate", Trim(Str$(IndicatorTestOptions.AnnualInterestRate))
SaveSetting App.Title, "Indicatortest", "UseCommission", Trim(Str$(IndicatorTestOptions.UseCommission))
SaveSetting App.Title, "Indicatortest", "UsePercentageCommission", Trim(Str$(IndicatorTestOptions.UsePercentageCommission))
SaveSetting App.Title, "Indicatortest", "UseMinimumCommission", Trim(Str$(IndicatorTestOptions.UseMinimumCommission))
SaveSetting App.Title, "Indicatortest", "Percentage", Trim(Str$(IndicatorTestOptions.Percentage))
SaveSetting App.Title, "Indicatortest", "Capital", Trim(Str$(IndicatorTestOptions.Capital))
SaveSetting App.Title, "Indicatortest", "MinCommission", Trim(Str$(IndicatorTestOptions.MinCommission))
SaveSetting App.Title, "Indicatortest", "BuyColor", Trim(Str$(IndicatorTestOptions.BuyColor))
SaveSetting App.Title, "Indicatortest", "SellColor", Trim(Str$(IndicatorTestOptions.SellColor))
SaveSetting App.Title, "Indicatortest", "StopColor", Trim(Str$(IndicatorTestOptions.StopColor))
SaveSetting App.Title, "Indicatortest", "ShowArrows", Trim(Str$(IndicatorTestOptions.ShowArrows))
SaveSetting App.Title, "Indicatortest", "ShowArrowLabels", Trim(Str$(IndicatorTestOptions.ShowArrowLabels))
SaveSetting App.Title, "Indicatortest", "RemoveExistingArrows", Trim(Str$(IndicatorTestOptions.RemoveExistingArrows))
SaveSetting App.Title, "Indicatortest", "PlotEquityLine", Trim(Str$(IndicatorTestOptions.PlotEquityLine))
SaveSetting App.Title, "Indicatortest", "RemoveExistingLines", Trim(Str$(IndicatorTestOptions.RemoveExistingLines))

SaveSetting App.Title, "Indicatortest", "UseBreakevenLongPositions", Trim(Str$(.UseBreakevenLongPositions))
SaveSetting App.Title, "Indicatortest", "UseBreakevenShortPositions", Trim(Str$(.UseBreakevenShortPositions))
SaveSetting App.Title, "Indicatortest", "BreakevenMethodIsPercent", Trim(Str$(.BreakevenMethodIsPercent))
SaveSetting App.Title, "Indicatortest", "BreakevenLevel", Trim(Str$(.BreakevenLevel))
SaveSetting App.Title, "Indicatortest", "UseInactivityLongPositions", Trim(Str$(.UseInactivityLongPositions))
SaveSetting App.Title, "Indicatortest", "UseInactivityShortPositions", Trim(Str$(.UseInactivityShortPositions))
SaveSetting App.Title, "Indicatortest", "InactivityMethodIsPercent", Trim(Str$(.InactivityMethodIsPercent))
SaveSetting App.Title, "Indicatortest", "InactivityMinChange", Trim(Str$(.InactivityMinChange))
SaveSetting App.Title, "Indicatortest", "InactivityPeriods", Trim(Str$(.InactivityPeriods))
SaveSetting App.Title, "Indicatortest", "UseMaxLossLongPositions", Trim(Str$(.UseMaxLossLongPositions))
SaveSetting App.Title, "Indicatortest", "UseMaxLossShortPositions", Trim(Str$(.UseMaxLossShortPositions))
SaveSetting App.Title, "Indicatortest", "MaxLossMethodIsPercent", Trim(Str$(.MaxLossMethodIsPercent))
SaveSetting App.Title, "Indicatortest", "MaxLossLevel", Trim(Str$(.MaxLossLevel))
SaveSetting App.Title, "Indicatortest", "UseProfitTakingLongPositions", Trim(Str$(.UseProfitTakingLongPositions))
SaveSetting App.Title, "Indicatortest", "UseProfitTakingShortPositions", Trim(Str$(.UseProfitTakingShortPositions))
SaveSetting App.Title, "Indicatortest", "ProfitTakingMethodIsPercent", Trim(Str$(.ProfitTakingMethodIsPercent))
SaveSetting App.Title, "Indicatortest", "ProfitTarget", Trim(Str$(.ProfitTarget))
SaveSetting App.Title, "Indicatortest", "UseTrailingLongPositions", Trim(Str$(.UseTrailingLongPositions))
SaveSetting App.Title, "Indicatortest", "UseTrailingShortPositions", Trim(Str$(.UseTrailingShortPositions))
SaveSetting App.Title, "Indicatortest", "TrailingMethodIsPercent", Trim(Str$(.TrailingMethodIsPercent))
SaveSetting App.Title, "Indicatortest", "TrailingProfitRisk", Trim(Str$(.TrailingProfitRisk))
SaveSetting App.Title, "Indicatortest", "TrailingPeriods", Trim(Str$(.TrailingPeriods))

End With
End Sub

Public Sub UpdateIndicatortestOptions()

With IndicatorTestOptions
  .Delay = frmIndicatortestOptions.txtDelay
  
  If frmIndicatortestOptions.optLongOnly Then
    .LongOnly = True
    .ShortOnly = False
    .BothLongAndShort = False
  ElseIf frmIndicatortestOptions.optShortOnly Then
    .ShortOnly = True
    .LongOnly = False
    .BothLongAndShort = False
  Else
    .BothLongAndShort = True
    .LongOnly = False
    .ShortOnly = False
  End If
  
  .InitialEquity = Val(frmIndicatortestOptions.txtInitialEquity)
  
  .StartDate = CDate(frmIndicatortestOptions.DTPicker1.Value)
  .StopDate = CDate(frmIndicatortestOptions.DTPicker2.Value)
  
  If frmIndicatortestOptions.optUseCommission Then
    .UseCommission = True
  Else
    .UseCommission = False
  End If
  
  If frmIndicatortestOptions.optPercentage Then
    .UsePercentageCommission = True
    .UseMinimumCommission = False
    .Percentage = CSng(PointToComma(frmIndicatortestOptions.txtPercentageCommission))
  Else
    .UseMinimumCommission = True
    .UsePercentageCommission = False
    .MinCommission = CSng(PointToComma(frmIndicatortestOptions.txtMinCommission))
  End If
    
End With

SaveIndicatortestOptionsToRegistry

End Sub

Public Sub InitTestStatistics()

With IndicatorTestStatistics
  .AmountOfLosingTrades = 0
  .AmountOfWinningTrades = 0
  .AnnualBuyHoldPercentage = 0
  .AnnualPercentGainLoss = 0
  .AverageLengthOfLoss = 0
  .AverageLengthOfWin = 0
  .AverageLengthOut = 0
  .AverageLoss = 0
  .AverageProfitPerTrade = 0
  .AverageWin = 0
  .AverageWinLossRatio = 0
  .BuyHoldIndex = 0
  .BuyHoldPercentageProfit = 0
  .BuyHoldProfit = 0
  .CommissionsPaid = 0
  .CurrentPosition = 0
  .DateCurrentPositionEntered = 0
  .DaysInTest = 0
  .InitialInvestment = 0
  .InterestEarned = 0
  .LargestLoss = 0
  .LargestWin = 0
  .LongestLosingTrade = 0
  .LongestOutPeriod = 0
  .LongestWinningTrade = 0
  .MaxOpenTradeDrawDown = 0
  .MostConsecutiveLosses = 0
  .MostConsecutiveWins = 0
  .OpenPositionValue = 0
  .PercentGainLoss = 0
  .ProfitLossIndex = 0
  .RewardRiskIndex = 0
  .SystemCloseDrawDown = 0
  .SystemOpenDrawDown = 0
  .TotalBarsOut = 0
  .TotalClosedTrades = 0
  .TotalLongTrades = 0
  .TotalLosingTrades = 0
  .TotalNetProfit = 0
  .TotalShortTrades = 0
  .TotalWinningTrades = 0
  .WinningLongTrades = 0
  .WinningShortTrades = 0
End With
End Sub
