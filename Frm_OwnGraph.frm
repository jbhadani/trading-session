VERSION 5.00
Begin VB.Form Frm_OwnGraph 
   AutoRedraw      =   -1  'True
   Caption         =   "Form1"
   ClientHeight    =   11010
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14190
   BeginProperty Font 
      Name            =   "Arial"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "Frm_OwnGraph.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   NegotiateMenus  =   0   'False
   ScaleHeight     =   11010
   ScaleWidth      =   14190
   Visible         =   0   'False
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   375
      Left            =   50000
      TabIndex        =   1
      Top             =   2520
      Width           =   1455
   End
   Begin VB.HScrollBar ScrollBar 
      CausesValidation=   0   'False
      Height          =   255
      LargeChange     =   100
      Left            =   360
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   4800
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   30
      Visible         =   0   'False
      X1              =   4440
      X2              =   7680
      Y1              =   6840
      Y2              =   7200
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   29
      Visible         =   0   'False
      X1              =   3720
      X2              =   6960
      Y1              =   2160
      Y2              =   2520
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   28
      Visible         =   0   'False
      X1              =   6360
      X2              =   9600
      Y1              =   4620
      Y2              =   4980
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   27
      Visible         =   0   'False
      X1              =   3960
      X2              =   7200
      Y1              =   1080
      Y2              =   1440
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   26
      Visible         =   0   'False
      X1              =   5520
      X2              =   8760
      Y1              =   3660
      Y2              =   4020
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   25
      Visible         =   0   'False
      X1              =   1980
      X2              =   5220
      Y1              =   2520
      Y2              =   2880
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   24
      Visible         =   0   'False
      X1              =   2040
      X2              =   5280
      Y1              =   3000
      Y2              =   3360
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   23
      Visible         =   0   'False
      X1              =   5460
      X2              =   8700
      Y1              =   6420
      Y2              =   6780
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   22
      Visible         =   0   'False
      X1              =   4080
      X2              =   6960
      Y1              =   1500
      Y2              =   1860
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   21
      Visible         =   0   'False
      X1              =   480
      X2              =   3720
      Y1              =   780
      Y2              =   1140
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   20
      Visible         =   0   'False
      X1              =   720
      X2              =   3960
      Y1              =   1920
      Y2              =   2280
   End
   Begin VB.Shape shpCircle 
      BorderColor     =   &H00FF0000&
      BorderWidth     =   2
      Height          =   195
      Left            =   2040
      Shape           =   3  'Circle
      Top             =   2760
      Visible         =   0   'False
      Width           =   195
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   19
      Visible         =   0   'False
      X1              =   840
      X2              =   4080
      Y1              =   2460
      Y2              =   2820
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   18
      Visible         =   0   'False
      X1              =   600
      X2              =   3840
      Y1              =   1320
      Y2              =   1680
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   17
      Visible         =   0   'False
      X1              =   4620
      X2              =   7860
      Y1              =   1260
      Y2              =   1620
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   16
      Visible         =   0   'False
      X1              =   4200
      X2              =   7080
      Y1              =   2040
      Y2              =   2400
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   15
      Visible         =   0   'False
      X1              =   1260
      X2              =   4500
      Y1              =   1920
      Y2              =   2280
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   14
      Visible         =   0   'False
      X1              =   4200
      X2              =   7440
      Y1              =   3720
      Y2              =   4080
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   13
      Visible         =   0   'False
      X1              =   2100
      X2              =   5340
      Y1              =   3060
      Y2              =   3420
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   12
      Visible         =   0   'False
      X1              =   5640
      X2              =   8880
      Y1              =   4200
      Y2              =   4560
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   11
      Visible         =   0   'False
      X1              =   4080
      X2              =   7320
      Y1              =   1620
      Y2              =   1980
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   10
      Visible         =   0   'False
      X1              =   3900
      X2              =   7140
      Y1              =   5700
      Y2              =   6060
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   9
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   8
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   7
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   6
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   5
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   4
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   3
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   2
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   1
      Visible         =   0   'False
      X1              =   240
      X2              =   3480
      Y1              =   960
      Y2              =   1320
   End
   Begin VB.Line TrendLine 
      BorderColor     =   &H000000FF&
      Index           =   0
      Visible         =   0   'False
      X1              =   3840
      X2              =   7080
      Y1              =   2700
      Y2              =   3060
   End
End
Attribute VB_Name = "Frm_OwnGraph"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim LastX As Long
Dim LastY As Long

Dim NewZ As Boolean
Dim ZStartNbr As Long
Dim exiting As Boolean

Dim TrendlineMoveNbr As Long

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)

#If TENDENS = 1 Then
  If (Shift And vbCtrlMask) > 0 _
      And (Shift And vbShiftMask) > 0 _
      And (KeyCode = vbKeyF12) Then
      If IsProVersion = True Then
        SaveGraph
      End If
  End If
#End If
End Sub

'               991018  Now uses GetGraphPrice and GetGraphDate.

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim j As Long
Dim k As Long
Dim l As Long

ZAIndex = 0
GraphFormMouseDown Me, Button, Shift, x, y

If Button = vbRightButton Then
If IsProVersion = True Then
  MainMDI.PopupMenu MainMDI.ID_GraphMain
End If
  Exit Sub
End If

'Handle move mode
Moving = 0
If TLDrawMode = dmMove Then
  Moving = 0
  For j = 0 To TrendlineSerieSize - 1
    With TrendLine(j)
    If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
      If Distance(.X1, .Y1, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End1 = True Then
        Moving = 2
        TrendlineMoveNbr = j
      ElseIf Distance(.X2, .Y2, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End2 = True Then
        Moving = 3
        TrendlineMoveNbr = j
      ElseIf (Distance(.X1, .Y1, x, y) + Distance(.X2, .Y2, x, y) - Distance(.X1, .Y1, .X2, .Y2) < 0.01) And TLWholeInView(GraphSettings.DWM, j + 1).End1 = True And TLWholeInView(GraphSettings.DWM, j + 1).End2 = True Then
        Moving = 1
        TrendlineMoveNbr = j
        LastX = x
        LastY = y
      End If
    End If
    End With
  Next j
End If

'Bj�rn hackar
If TLDrawMode = dmMoveZ Then
  Moving = 0
  Do While j <= TrendlineSerieSize - 1
    For k = 0 To ZIAPointer - 1
      If j = ZIndexArray(k) Then
        If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
          For l = j To j + 5
            With TrendLine(l)
            If Distance(.X1, .Y1, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End1 = True Then
              Moving = 2
              TrendlineMoveNbr = ZIndexArray(k)
              LastX = x
              LastY = y
            ElseIf Distance(.X2, .Y2, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End2 = True Then
              Moving = 3
              TrendlineMoveNbr = ZIndexArray(k)
              LastX = x
              LastY = y
            ElseIf (Distance(.X1, .Y1, x, y) + Distance(.X2, .Y2, x, y) - Distance(.X1, .Y1, .X2, .Y2) < 0.02) And TLWholeInView(GraphSettings.DWM, j + 1).End1 = True And TLWholeInView(GraphSettings.DWM, j + 1).End2 = True Then
              Moving = 1
              TrendlineMoveNbr = ZIndexArray(k)
              LastX = x
              LastY = y
            End If
          End With
        Next l
      End If
      exiting = True
      Exit For
    End If
    Next k
    j = j + 1
  Loop
End If
'Slut hack


'Handle draw parallel mode
Dim lngTrendlineNbr As Long
If TLDrawMode = dmDrawParallel And Button = vbLeftButton Then
  lngTrendlineNbr = TrendlineHit(Me, x, y)
  If lngTrendlineNbr <> -1 Then
    TrendLineNbr = GetFreeTrendLine(GraphSettings.DWM)
    TrendLineArray(GraphSettings.DWM, TrendLineNbr).InUse = True
    With TrendLine(TrendLineNbr - 1)
    .BorderColor = DrawSettings.LineColor
    .BorderStyle = vbBSSolid
    .Visible = True
    .X1 = Frm_OwnGraph.TrendLine(lngTrendlineNbr).X1
    .X2 = Frm_OwnGraph.TrendLine(lngTrendlineNbr).X2
    .Y1 = Frm_OwnGraph.TrendLine(lngTrendlineNbr).Y1
    .Y2 = Frm_OwnGraph.TrendLine(lngTrendlineNbr).Y2
    End With
    TrendlineMoveNbr = TrendLineNbr - 1
    Moving = 1
    TLWholeInView(GraphSettings.DWM, TrendLineNbr).End1 = True
    TLWholeInView(GraphSettings.DWM, TrendLineNbr).End2 = True
    LastX = x
    LastY = y
  Else
    'ErrorMessageStr "Det g�r ej att rita fler trendlinjer."
  End If
End If




If TLDrawMode = dmDraw Or TLDrawMode = dmDrawDates Or TLDrawMode = dmDrawHorizontal Or TLDrawMode = dmDrawVertical Then
  If Button = 1 And ValidCoordinate(x, y) Then
    TrendLineNbr = GetFreeTrendLine(GraphSettings.DWM)
    'Load TrendLine(TrendLine.Count)
    'TrendLineNbr = GetFreeTrendLine(GraphSettings.DWM)
    If TrendLineNbr <> -1 Then
      'Set trendeline in use
      TrendLineArray(GraphSettings.DWM, TrendLineNbr).InUse = True
      'Set trendline
      With TrendLine(TrendLineNbr - 1)
      .BorderColor = DrawSettings.LineColor
      .BorderStyle = vbBSSolid
      .X1 = x
      .Y1 = y
      .X2 = x
      .Y2 = y
      .Visible = True
      End With
      NewLine = True
    Else
      'ErrorMessageStr "Det g�r ej att rita fler trendlinjer."
      NewLine = False
    End If
  End If
  
  'Hack skapad av Bj�rn Skoglund
  'Skriver ut startdatum f�r trendlinje
  
  If TLDrawMode = dmDrawDates And NewLine Then
    Dim LabelHandle As Long
    With Frm_OwnGraph
    If ActiveStockLabelsHandle <> Empty Then
      LabelHandle = AddStockLabel(Frm_OwnGraph.hdc, _
        ActiveStockLabelsHandle, _
        GetGraphDate(TrendLine(TrendLineNbr - 1).X1), _
        GetGraphPrice(TrendLine(TrendLineNbr - 1).Y1 + 5), _
        right(left(GetGraphDate(TrendLine(TrendLineNbr - 1).X1), 10), 5), _
        DrawSettings.LineColor)
          
      LineLabelHandleArray(LLHPointer, 0) = TrendLine(TrendLineNbr - 1).X1
      LineLabelHandleArray(LLHPointer, 1) = TrendLine(TrendLineNbr - 1).Y1 + 5
      LLHPointer = LLHPointer + 1
             
      RefreshStockLabels Frm_OwnGraph.hdc, _
        ActiveStockLabelsHandle
      Frm_OwnGraph.Refresh
    Else
      MsgBox CheckLang("Misslyckades l�gga till text")
    End If
    End With
  End If
  'Slut Hack
End If

'Bj�rn forts�tter of�rtrutet att hacka
If TLDrawMode = dmDrawZ And ValidCoordinate(x, y) And Button = vbLeftButton Then
  ZStartNbr = GetFreeTrendLine(GraphSettings.DWM)
  For j = ZStartNbr To ZStartNbr + 5
    If j <> -1 And (ZStartNbr + 5) <= 30 Then
      'Set trendeline in use
      TrendLineArray(GraphSettings.DWM, j).InUse = True
      'Set trendline
      With TrendLine(j - 1)
        .BorderColor = DrawSettings.LineColor
        .BorderStyle = vbBSSolid
        .X1 = x
        .Y1 = y
        .X2 = x
        .Y2 = y
        .Visible = True
      End With
    Else
      ErrorMessageStr "Det g�r inte att rita fler Z-diagram."
      NewZ = False
      Exit For
    End If
  Next j
  If ZIAPointer < 5 Then
    ZIndexArray(ZIAPointer) = ZStartNbr
    ZIAPointer = ZIAPointer + 1
  End If
  NewZ = True
End If
'Slut hack igen...
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          Form_MouseMove
'Input:
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      980331  Created.
'               990829  -Added a call to GraphFormMouseMove.
'                       -Removed the status bar update.
'               991018  Now uses GetGraphPrice and GetGraphDate.
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
  Dim j As Long
  Dim k As Long
  Dim l As Long
  Dim YC As Long
  Dim TmpString As String
  Dim NewX1 As Single
  Dim NewX2 As Single
  Dim NewY1 As Single
  Dim NewY2 As Single
  Dim Ready As Boolean
  
  Dim PriceDataIndex As Long
  
GraphFormMouseMove Me, Button, Shift, x, y

If (TLDrawMode = dmMove Or TLDrawMode = dmDrawParallel) And Moving = 0 Then
  j = 0
  Ready = False
  Do While j <= TrendlineSerieSize - 1 And Not Ready
    With TrendLine(j)
    If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
      If ((Distance(.X1, .Y1, x, y) < 2 And TLWholeInView(GraphSettings.DWM, j + 1).End1) Or _
          (Distance(.X2, .Y2, x, y) < 2 And TLWholeInView(GraphSettings.DWM, j + 1).End2)) And _
         (TLDrawMode = dmMove) Then
        Frm_OwnGraph.MousePointer = vbSizeNESW
        Ready = True
      ElseIf (Distance(.X1, .Y1, x, y) + Distance(.X2, .Y2, x, y) - Distance(.X1, .Y1, .X2, .Y2) < 0.01) And _
              (TLWholeInView(GraphSettings.DWM, j + 1).End1 = True) And _
              (TLWholeInView(GraphSettings.DWM, j + 1).End2 = True) Then
        Frm_OwnGraph.MousePointer = vbSizePointer
        Ready = True
      Else
        Frm_OwnGraph.MousePointer = vbDefault
      End If
    End If
    End With
  j = j + 1
  Loop
End If

'Bj�rn hackar
If TLDrawMode = dmMoveZ And Moving = 0 Then
  j = 0
  Ready = False
  Do While j <= TrendlineSerieSize - 1 And Not Ready
   For k = 0 To ZIAPointer - 1
    If j = ZIndexArray(k) Then
    With TrendLine(j)
    If TrendLineArray(GraphSettings.DWM, j + 1).InUse Then
      If ((Distance(.X1, .Y1, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End1) Or _
          (Distance(.X2, .Y2, x, y) < 5 And TLWholeInView(GraphSettings.DWM, j + 1).End2)) Then
        Frm_OwnGraph.MousePointer = vbSizeNESW
        Ready = True
        Exit For
      ElseIf (Distance(.X1, .Y1, x, y) + Distance(.X2, .Y2, x, y) - Distance(.X1, .Y1, .X2, .Y2) < 0.02) And _
              (TLWholeInView(GraphSettings.DWM, j + 1).End1 = True) And _
              (TLWholeInView(GraphSettings.DWM, j + 1).End2 = True) Then
        Frm_OwnGraph.MousePointer = vbSizePointer
        Ready = True
        Exit For
      Else
        Frm_OwnGraph.MousePointer = vbDefault
      End If
    End If
    End With
    End If
   Next k
  j = j + 1
  Loop
End If
'Slut hack



If (TLDrawMode = dmMove Or TLDrawMode = dmDrawParallel) And Moving <> 0 And Button = vbLeftButton And ValidCoordinate(x, y) Then
With TrendLine(TrendlineMoveNbr)
  If Moving = 1 Then
    NewX1 = .X1 + x - LastX
    NewX2 = .X2 + x - LastX
    NewY1 = .Y1 + y - LastY
    NewY2 = .Y2 + y - LastY
  ElseIf Moving = 2 Then
    NewX1 = x
    NewY1 = y
    TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 2, x, y, NewX2, NewY2
  ElseIf Moving = 3 Then
    NewX2 = x
    NewY2 = y
    TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 1, x, y, NewX1, NewY1
  End If
  
  If ValidCoordinate(NewX1, NewY1) And ValidCoordinate(NewX2, NewY2) Then
    .X1 = NewX1
    .X2 = NewX2
    .Y1 = NewY1
    .Y2 = NewY2
  End If
  End With
  LastX = x
  LastY = y
End If


If Moving = 1 And Button = vbLeftButton And TLDrawMode = dmMoveZ Then
  MoveZ x, y
End If
If Moving = 2 And Button = vbLeftButton And TLDrawMode = dmMoveZ Then
  ReSizeZ1 x, y
End If
If Moving = 3 And Button = vbLeftButton And TLDrawMode = dmMoveZ Then
  ReSizeZ2 x, y
End If

If (TLDrawMode = dmDraw Or TLDrawMode = dmDrawDates Or TLDrawMode = dmDrawHorizontal Or TLDrawMode = dmDrawVertical Or TLDrawMode = dmDrawZ) And (ValidCoordinate(x, y) Or FreeDraw) Then
  Frm_OwnGraph.MousePointer = vbCrosshair
ElseIf (TLDrawMode <> dmMove) And (TLDrawMode <> dmDrawParallel) And (TLDrawMode <> dmMoveZ) Then
  Frm_OwnGraph.MousePointer = vbDefault
End If
If TLDrawMode = dmNone Then
  Frm_OwnGraph.MousePointer = vbDefault
End If

If NewLine Then
  With TrendLine(TrendLineNbr - 1)
  If (TLDrawMode = dmDraw Or TLDrawMode = dmDrawDates) And Shift = 0 Then
    .X2 = x
    .Y2 = y
  ElseIf TLDrawMode = dmDrawHorizontal Or ((TLDrawMode = dmDrawDates Or TLDrawMode = dmDraw) And Shift = 1) Then
    .X2 = x
    .Y2 = .Y1
  ElseIf TLDrawMode = dmDrawVertical Or ((TLDrawMode = dmDrawDates Or TLDrawMode = dmDraw) And Shift = 2) Then
    .X2 = .X1
    .Y2 = y
  End If
  End With
End If

'Bj�rn hackar
If TLDrawMode = dmDrawZ And Button = vbLeftButton And NewZ Then
  'Den f�rsta horisontellen
  With TrendLine(ZStartNbr - 1)
    .X2 = x
    .Y2 = .Y1
  End With
  'Den vanliga trendlinjen
  With TrendLine(ZStartNbr)
    .X2 = x
    .Y2 = y
  End With
  'Den andra horisontellen
  With TrendLine(ZStartNbr + 1)
    .Y1 = y
    .X2 = x
    .Y2 = y
  End With
  'Mitthorisontellen
  With TrendLine(ZStartNbr + 2)
    .Y1 = (TrendLine(ZStartNbr).Y1 + TrendLine(ZStartNbr).Y2) / 2
    .Y2 = .Y1
    .X2 = x
  End With
  '38% horisontellen
  With TrendLine(ZStartNbr + 3)
    .Y1 = TrendLine(ZStartNbr).Y1 + (TrendLine(ZStartNbr).Y2 - TrendLine(ZStartNbr).Y1) * 0.38
    .Y2 = .Y1
    .X2 = x
  End With
  '62% horisontellen
  With TrendLine(ZStartNbr + 4)
    .Y1 = TrendLine(ZStartNbr).Y1 + (TrendLine(ZStartNbr).Y2 - TrendLine(ZStartNbr).Y1) * 0.62
    .Y2 = .Y1
    .X2 = x
  End With
  'Undrar om 'horisontell' �r ett korrekt substantiv?
End If
'Slut hack
End Sub

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Name:          Form_MouseUp
'Input:
'Output:
'
'Description:
'Author:        Fredrik Wendel
'Revision:      980331  Created
'               990829  Added a call to GraphFormMouseUp
'               991018  Now uses GetGraphPrice and GetGraphDate.
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Private Sub Form_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
'Dim PixelPerPrice As Single
Dim YC As Single
Dim XC As Single
Dim l As Long

GraphFormMouseUp Me, Button, Shift, x, y

If Button = vbLeftButton Then
  If TLDrawMode = dmMove And Moving <> 0 Then
    With TrendLineArray(GraphSettings.DWM, TrendlineMoveNbr + 1)
      If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End1 Then
        .StartPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y1)
      End If
      If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End2 And ValidCoordinate(x, y) Then
        .EndPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y2)
      End If
    If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End1 Then
      .StartDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X1)
    End If
    If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End2 Then
      .EndDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X2)
    End If
    End With
    Moving = 0
  End If
  
  'Bj�rn avsluar Z-F�rflyttningen
  If TLDrawMode = dmMoveZ And Moving <> 0 Then
    For l = TrendlineMoveNbr - 1 To TrendlineMoveNbr + 4
    With TrendLineArray(GraphSettings.DWM, TrendlineMoveNbr + 1)
      If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End1 Then
        .StartPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y1)
      End If
      If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End2 Then
        .EndPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y2)
      End If
    If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End1 Then
      .StartDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X1)
    End If
    If TLWholeInView(GraphSettings.DWM, TrendlineMoveNbr + 1).End2 Then
      .EndDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X2)
    End If
    End With
    Next l
    Moving = 0
  End If
  'Slut hack
  
  If TLDrawMode = dmDrawParallel And Moving <> 0 Then
    With TrendLineArray(GraphSettings.DWM, TrendlineMoveNbr + 1)
    .StartPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y1)
    .EndPrice = GetGraphPrice(TrendLine(TrendlineMoveNbr).Y2)
    .StartDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X1)
    .EndDate = GetGraphDate(TrendLine(TrendlineMoveNbr).X2)
    .LineColor = TrendLine(TrendlineMoveNbr).BorderColor
    .LineType = TrendLine(TrendlineMoveNbr).BorderStyle
    End With
    Moving = 0
  End If

  If TLDrawMode And NewLine Then
    
    If ValidCoordinate(x, y) Or FreeDraw Then
      With TrendLine(TrendLineNbr - 1)
      If (TLDrawMode = dmDraw Or TLDrawMode = dmDrawDates) And Shift = 0 Then
        XC = x
        YC = y
    Else
     If TLDrawMode = dmDrawHorizontal Or Shift = 1 Then
        XC = x
        YC = .Y1
    Else
     If TLDrawMode = dmDrawVertical Or Shift = 2 Then
        XC = .X1
        YC = y
     End If
    End If
        
  End If
  End With
     
      With TrendLineArray(GraphSettings.DWM, TrendLineNbr)
        .StartPrice = GetGraphPrice(TrendLine(TrendLineNbr - 1).Y1)
        .StartDate = GetGraphDate(TrendLine(TrendLineNbr - 1).X1)
        .EndPrice = GetGraphPrice(YC)
        If ValidCoordinate(XC, YC) Then
          .EndDate = GetGraphDate(XC)
        End If
        .LineColor = TrendLine(TrendLineNbr - 1).BorderColor
        .LineType = TrendLine(TrendLineNbr - 1).BorderStyle
      End With
      TLWholeInView(GraphSettings.DWM, TrendLineNbr).End1 = True
      TLWholeInView(GraphSettings.DWM, TrendLineNbr).End2 = True
    Else
      TrendLine(TrendLineNbr - 1).Visible = False
      TrendLineArray(GraphSettings.DWM, TrendLineNbr).InUse = False
    End If
          
    'F�r att f� datum utskrivet vid Trendlinjerna �r
    'vissa delar av Textfunktionerna inkopierade,
    'Inte vackert men det fungerar
    '/Bj�rn
    If TLDrawMode = dmDrawDates Then
      If ValidCoordinate(x, y) Then
            
      Dim dydx As Single
      Dim DaySpace As Double
        Select Case GraphSettings.DWM
          Case dwmDaily
            DaySpace = GraphSettings.lngDaySpace / (365.25 / 240)
          Case dwmWeekely
            DaySpace = GraphSettings.lngDaySpace / (365.25 / 240 * 7)
          Case dwmMonthly
            DaySpace = GraphSettings.lngDaySpace / (365.25 / 240 * 30)
        End Select
             
        'Vi vill g�rna undvika Divide by 0
        If TrendLine(TrendLineNbr - 1).X2 <> TrendLine(TrendLineNbr - 1).X1 Then
          dydx = (TrendLine(TrendLineNbr - 1).Y2 - TrendLine(TrendLineNbr - 1).Y1) / (TrendLine(TrendLineNbr - 1).X2 - TrendLine(TrendLineNbr - 1).X1)
        Else
          dydx = 0
        End If
            
        If TrendLine(TrendLineNbr - 1).X1 > TrendLine(TrendLineNbr - 1).X2 Then
          DrawDatesNegative TrendLine(TrendLineNbr - 1).X1, _
            TrendLine(TrendLineNbr - 1).X2, _
            TrendLine(TrendLineNbr - 1).Y1, _
            dydx, DaySpace
        Else
          DrawDates TrendLine(TrendLineNbr - 1).X1, _
            TrendLine(TrendLineNbr - 1).X2, _
            TrendLine(TrendLineNbr - 1).Y1, _
            dydx, DaySpace
        End If
      End If
    End If
    'Slut hack!
  End If
  
  'Bj�rn hackar
  If TLDrawMode = dmDrawZ And NewZ Then
    If ValidCoordinate(x, y) Or FreeDraw Then
      Dim j As Single
      XC = TrendLine(ZStartNbr).X2
      YC = TrendLine(ZStartNbr).X1 'En oanv�nd variabel �r en sl�sad variabel
      For j = 0 To 5
        With TrendLineArray(GraphSettings.DWM, ZStartNbr + j)
          .StartPrice = GetGraphPrice(TrendLine(ZStartNbr + j - 1).Y1)
          .StartDate = GetGraphDate(YC)
          .EndPrice = GetGraphPrice(TrendLine(ZStartNbr + j - 1).Y2)
          .EndDate = GetGraphPrice(XC)
          .LineColor = TrendLine(ZStartNbr + j - 1).BorderColor
          .LineType = TrendLine(ZStartNbr + j - 1).BorderStyle
        End With
        TLWholeInView(GraphSettings.DWM, ZStartNbr + j).End1 = True
        TLWholeInView(GraphSettings.DWM, ZStartNbr + j).End2 = True
      Next j
      NewLine = False
      NewZ = False
    Else
      For j = 0 To 5
        TrendLine(ZStartNbr + j - 1).Visible = False
        TrendLineArray(GraphSettings.DWM, ZStartNbr + j).InUse = False
      Next j
    End If
  End If
  'Slut hack
  NewLine = False
End If
End Sub


Private Sub Form_Resize()
  With Frm_OwnGraph
  If .WindowState = 0 Then
    If .Height < 5000 Then
      .Height = 5000
    End If
    If .Width < 5000 Then
      .Width = 5000
    End If
  End If
  End With
  Debug.Print "Update graph from Resize"
  UpdateGraph ActiveIndex
End Sub

Private Sub Form_Terminate()
  MainMDI.ID_Graph.Checked = False 'ssUnchecked
  ShowGraph = False
  If ActiveStockLabelsHandle <> Empty Then
    HideStockLabels ActiveStockLabelsHandle
    ActiveStockLabelsHandle = Empty
  End If
  With MainMDI
  .ID_Print.Enabled = False
  .ID_Preview.Enabled = False
  End With

End Sub

Private Sub Form_Unload(Cancel As Integer)
If IsProVersion = True Then
  MainMDI.ID_Graph.Checked = Unchecked
  ShowGraph = False
  If ActiveStockLabelsHandle <> Empty Then
    HideStockLabels ActiveStockLabelsHandle
    ActiveStockLabelsHandle = Empty
  End If
  With MainMDI
  .ID_Print.Enabled = False
  .ID_Preview.Enabled = False
  End With
End If
End Sub
Private Sub ScrollBar_Change()
dbg.Enter "Scrollbar_Change"
  
  
  If DoScroll Then
    dbg.Out "Update graph from Change " & CStr(ScrollBar.Value)
    UpdateGraph ActiveIndex
  Else
    Debug.Print "No update"
  End If
  dbg.Leave "Scrollbar_Change"
End Sub

Private Sub DrawDates(X1 As Single, X2 As Single, Y1 As Single, dydx As Single, DaySpace As Double)
Dim LabelHandle As Long
Dim k As Integer
    k = 1
Dim Xn As Single
    Xn = X1
    
If ActiveStockLabelsHandle <> Empty Then
    Do While Xn + CycleLenth * DaySpace < X2
        Xn = X1 + k * CycleLenth * DaySpace
        If ValidCoordinate(Xn, Y1 + dydx * (Xn - X1) + 5) Then
        LabelHandle = AddStockLabel(Frm_OwnGraph.hdc, _
            ActiveStockLabelsHandle, _
            GetGraphDate(Xn), _
            GetGraphPrice(Y1 + dydx * (Xn - X1) + 5), _
            right(left(GetGraphDate(Xn), 10), 5), _
            DrawSettings.LineColor)
        End If
        If LLHPointer < 256 Then
            LineLabelHandleArray(LLHPointer, 0) = Xn
            LineLabelHandleArray(LLHPointer, 1) = Y1 + dydx * (Xn - X1) + 5
            LLHPointer = LLHPointer + 1
        End If
        
        k = k + 1
    Loop
    RefreshStockLabels Frm_OwnGraph.hdc, ActiveStockLabelsHandle
    Frm_OwnGraph.Refresh

Else
    'Should never reach this point
    MsgBox CheckLang("Misslyckades l�gga till text")
End If
End Sub

Private Sub DrawDatesNegative(X1 As Single, X2 As Single, Y1 As Single, dydx As Single, DaySpace As Double)
Dim LabelHandle As Long
Dim k As Integer
    k = 1
Dim Xn As Single
    Xn = X1

If ActiveStockLabelsHandle <> Empty Then
    Do While Xn - CycleLenth * DaySpace > X2
        Xn = X1 - k * CycleLenth * DaySpace
        If ValidCoordinate(Xn, Y1 + dydx * (Xn - X1) + 5) Then
        LabelHandle = AddStockLabel(Frm_OwnGraph.hdc, _
            ActiveStockLabelsHandle, _
            GetGraphDate(Xn), _
            GetGraphPrice(Y1 + dydx * (Xn - X1) + 5), _
            right(left(GetGraphDate(Xn), 10), 5), _
            DrawSettings.LineColor)
        End If

        If LLHPointer < 256 Then
            LineLabelHandleArray(LLHPointer, 0) = Xn
            LineLabelHandleArray(LLHPointer, 1) = Y1 + dydx * (Xn - X1) + 5
            LLHPointer = LLHPointer + 1
        End If
        
        RefreshStockLabels Frm_OwnGraph.hdc, ActiveStockLabelsHandle
        Frm_OwnGraph.Refresh
        
        k = k + 1
    Loop
    
Else
    'Should never reach this point
    MsgBox CheckLang("Misslyckades l�gga till text")
End If
End Sub

Private Sub MoveZ(x As Single, y As Single)
'Bj�rn flyttar Z-diagram
Dim NewX1 As Single
Dim NewX2 As Single
Dim NewY1 As Single
Dim NewY2 As Single

With TrendLine(TrendlineMoveNbr)
  NewX1 = .X1 + x - LastX
  NewX2 = .X2 + x - LastX
  NewY1 = .Y1 + y - LastY
  NewY2 = .Y2 + y - LastY
  If (ValidCoordinate(NewX1, NewY1) And ValidCoordinate(NewX2, NewY2)) Or FreeDraw Then
    .X1 = NewX1
    .X2 = NewX2
    .Y1 = NewY1
    .Y2 = NewY2
  Else
    Exit Sub
  End If
End With
    
With TrendLine(TrendlineMoveNbr - 1)
  .X1 = .X1 + x - LastX
  .X2 = .X2 + x - LastX
  .Y1 = .Y1 + y - LastY
  .Y2 = .Y2 + y - LastY
End With
With TrendLine(TrendlineMoveNbr + 1)
 .X1 = .X1 + x - LastX
 .X2 = .X2 + x - LastX
 .Y1 = .Y1 + y - LastY
 .Y2 = .Y2 + y - LastY
End With
With TrendLine(TrendlineMoveNbr + 2)
  .X1 = .X1 + x - LastX
  .X2 = .X2 + x - LastX
  .Y1 = .Y1 + y - LastY
  .Y2 = .Y2 + y - LastY
End With
With TrendLine(TrendlineMoveNbr + 3)
  .X1 = .X1 + x - LastX
  .X2 = .X2 + x - LastX
  .Y1 = .Y1 + y - LastY
  .Y2 = .Y2 + y - LastY
End With
With TrendLine(TrendlineMoveNbr + 4)
  .X1 = .X1 + x - LastX
  .X2 = .X2 + x - LastX
  .Y1 = .Y1 + y - LastY
  .Y2 = .Y2 + y - LastY
End With
LastX = x
LastY = y
    
  'If Moving = 2 Then
  '  .X1 = X
  '  .Y1 = Y
  '  TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 2, X, Y, .X2, .Y2
  'ElseIf Moving = 3 Then
  '  .X2 = X
  ' .Y2 = Y
  '  TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 1, X, Y, .X1, .Y1
  'End If
  
  
'Slut hack
End Sub

Private Sub ReSizeZ1(x As Single, y As Single)
Dim NewX1 As Single
Dim NewY1 As Single

With TrendLine(TrendlineMoveNbr)
  NewX1 = .X1 + x - LastX
  NewY1 = .Y1 + y - LastY
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 2, X, Y, NewX1, NewY1
  If ValidCoordinate(NewX1, NewY1) Or FreeDraw Then
    .X1 = NewX1
    .Y1 = NewY1
  Else
    Exit Sub
  End If
End With

With TrendLine(TrendlineMoveNbr - 1)
  .X1 = TrendLine(TrendlineMoveNbr).X1
  .Y1 = .Y1 + y - LastY
  .Y2 = .Y1
End With
With TrendLine(TrendlineMoveNbr + 1)
  .X1 = TrendLine(TrendlineMoveNbr).X1
End With
With TrendLine(TrendlineMoveNbr + 2)
  .X1 = TrendLine(TrendlineMoveNbr).X1
  .Y1 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) / 2
  .Y2 = .Y1
End With
With TrendLine(TrendlineMoveNbr + 3)
  .X1 = TrendLine(TrendlineMoveNbr).X1
  .Y1 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) * 0.38
  .Y2 = .Y1
End With
With TrendLine(TrendlineMoveNbr + 4)
  .X1 = TrendLine(TrendlineMoveNbr).X1
  .Y1 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) * 0.62
  .Y2 = .Y1
End With
LastX = x
LastY = y
End Sub


Private Sub ReSizeZ2(x As Single, y As Single)
Dim NewX2 As Single
Dim NewY2 As Single

With TrendLine(TrendlineMoveNbr)
  NewX2 = .X2 + x - LastX
  NewY2 = .Y2 + y - LastY
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 1, 2, X, Y, NewX2, NewY2
  If ValidCoordinate(NewX2, NewY2) Or FreeDraw Then
    .X2 = NewX2
    .Y2 = NewY2
  Else
    Exit Sub
  End If
End With
    
With TrendLine(TrendlineMoveNbr - 1)
 NewX2 = TrendLine(TrendlineMoveNbr).X2
 'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr, 2, X, Y, .X2, .Y2
 .X2 = NewX2
End With

With TrendLine(TrendlineMoveNbr + 1)
 NewX2 = TrendLine(TrendlineMoveNbr).X2
 NewY2 = TrendLine(TrendlineMoveNbr).Y2
 'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 2, 2, X, Y, NewX2, NewY2
 'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 2, 2, X, Y, .X1, NewY2
 .X2 = NewX2
 .Y2 = NewY2
 .Y1 = NewY2
End With

With TrendLine(TrendlineMoveNbr + 2)
  NewX2 = TrendLine(TrendlineMoveNbr).X2
  NewY2 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) / 2
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 3, 2, X, Y, NewX2, NewY2
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 3, 2, X, Y, .X1, NewY2
  .X2 = NewX2
  .Y2 = NewY2
  .Y1 = .Y2
End With

With TrendLine(TrendlineMoveNbr + 3)
  NewX2 = TrendLine(TrendlineMoveNbr).X2
  NewY2 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) * 0.38
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 4, 2, X, Y, NewX2, NewY2
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 4, 2, X, Y, .X1, NewY2
  .X2 = NewX2
  .Y2 = NewY2
  .Y1 = .Y2
End With
With TrendLine(TrendlineMoveNbr + 4)
  NewX2 = TrendLine(TrendlineMoveNbr).X2
  NewY2 = TrendLine(TrendlineMoveNbr).Y1 + (TrendLine(TrendlineMoveNbr).Y2 - TrendLine(TrendlineMoveNbr).Y1) * 0.62
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 5, 2, X, Y, NewX2, NewY2
  'TLFixBorder GraphSettings.DWM, TrendlineMoveNbr + 5, 2, X, Y, .X1, NewY2
  .X2 = NewX2
  .Y2 = NewY2
  .Y1 = .Y2
End With
LastX = x
LastY = y
End Sub
